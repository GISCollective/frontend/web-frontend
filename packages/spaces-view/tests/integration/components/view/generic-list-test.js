/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | view/generic-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(
      hbs`<View::GenericList as |item index|>{{item}} {{index}}</View::GenericList>`,
    );

    assert.dom(this.element).hasText('');
  });

  it('renders an item when the value has one', async function (assert) {
    this.set('value', ['test']);

    await render(
      hbs`<View::GenericList @value={{this.value}} as |item index|>{{item}} {{index}}</View::GenericList>`,
    );

    expect(this.element.querySelector('.generic-list.empty')).not.to.exist;
    assert.dom(this.element).hasText('test 0');
  });

  it('renders a placeholder when the list is empty', async function (a) {
    this.set('value', []);

    await render(
      hbs`<View::GenericList @isEditor={{true}} @value={{this.value}} />`,
    );

    expect(
      this.element.querySelector('.generic-list.empty'),
    ).to.have.textContent('Empty list');
  });

  it('does not render a placeholder when it is not in edit mode', async function (a) {
    this.set('value', []);

    await render(
      hbs`<View::GenericList @isEditor={{false}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.generic-list.empty')).not.to.exist;
  });
});
