/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/blocks', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the empty state when there is no value', async function () {
    await render(hbs`<View::Blocks @isEditor={{true}} />`);

    expect(this.element.querySelector('.page-col-blocks')).to.have.classes(
      'empty',
    );

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('does not render the empty state when there is a block', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'Head',
            level: 1,
          },
        },
      ],
    });

    await render(
      hbs`<View::Blocks @isEditor={{true}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.page-col-blocks')).not.to.have.classes(
      'empty',
    );
  });

  it('renders the empty state when there is a block with no text', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: '',
            level: 1,
          },
        },
      ],
    });

    await render(
      hbs`<View::Blocks @isEditor={{true}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.page-col-blocks')).to.have.classes(
      'empty',
    );
  });

  it('renders the empty state when there is a block with no text', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'Picture',
          data: {},
        },
      ],
    });

    await render(
      hbs`<View::Blocks @isEditor={{true}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.page-col-blocks')).not.to.have.classes(
      'empty',
    );
  });

  it('does not render the empty state when there is no value and it is not an editor', async function () {
    await render(hbs`<View::Blocks @isEditor={{false}} />`);

    expect(this.element.querySelector('.page-col-blocks')).not.to.have.classes(
      'empty',
    );

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the blocks', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'Head',
            level: 1,
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'Hey. Meet the new Editor.',
          },
        },
      ],
    });

    await render(hbs`<View::Blocks @value={{this.value}} />`);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Head',
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Hey. Meet the new Editor.',
    );
  });

  it('renders the blocks wrapped in a col data', async function () {
    this.set('value', {
      data: {
        blocks: [
          {
            type: 'header',
            data: {
              text: 'Head',
              level: 1,
            },
          },
          {
            type: 'paragraph',
            data: {
              text: 'Hey. Meet the new Editor.',
            },
          },
        ],
      },
    });

    await render(hbs`<View::Blocks @value={{this.value}} />`);

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Head',
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Hey. Meet the new Editor.',
    );
  });

  it('renders and applies the options', async function () {
    this.set('value', {
      data: {
        paragraphStyle: {
          classes: ['text-center', 'lh-sm', 'ff-monospace'],
          color: 'red',
        },
        container: {
          verticalSize: 'proportional',
          proportion: '2:3',
          verticalAlign: 'top',
          classes: ['ps-2'],
          color: 'blue',
        },
        blocks: [
          {
            type: 'header',
            data: {
              text: 'Head',
              level: 1,
            },
          },
          {
            type: 'paragraph',
            data: {
              text: 'Hey. Meet the new Editor.',
            },
          },
        ],
      },
    });

    await render(hbs`<View::Blocks @deviceSize="sm" @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-blocks')).to.have.classes(
      'page-col-blocks ps-2 bg-blue container-align-top',
    );
    expect(this.element.querySelector('.page-col-blocks p')).to.have.classes(
      'text-center lh ff-monospace text-red',
    );
  });

  it('renders and applies the options', async function () {
    this.set('value', {
      data: {
        paragraphStyle: {
          classes: ['text-center', 'lh-sm', 'ff-monospace'],
          color: 'red',
        },
        container: {
          verticalSize: 'proportional',
          proportion: '2:3',
          verticalAlign: 'top',
          classes: ['ps-2'],
          color: 'blue',
        },
        content: {
          blocks: [
            {
              type: 'header',
              data: {
                text: 'Head',
                level: 1,
              },
            },
            {
              type: 'paragraph',
              data: {
                text: 'Hey. Meet the new Editor.',
              },
            },
          ],
        },
      },
    });

    await render(hbs`<View::Blocks @deviceSize="sm" @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-blocks')).to.have.classes(
      'page-col-blocks ps-2 bg-blue container-align-top',
    );
    expect(this.element.querySelector('.page-col-blocks p')).to.have.classes(
      'text-center lh ff-monospace text-red',
    );
  });

  it('renders MD string as blocks', async function () {
    this.set('value', '# Head\n\nHey. Meet the new Editor.');

    await render(hbs`<View::Blocks @value={{this.value}} />`);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Head',
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Hey. Meet the new Editor.',
    );
  });

  it('renders a plain string when preview is true', async function () {
    this.set('value', '# Head\n\nHey. Meet the new Editor.');

    await render(hbs`<View::Blocks @value={{this.value}} @preview={{true}}/>`);
    expect(this.element.querySelector('h1')).not.exist;
    expect(this.element.querySelector('p')).not.exist;
    expect(this.element.textContent.trim()).to.equal(
      'Head\n\nHey. Meet the new Editor.',
    );
  });

  it('renders the value editor fields by default', async function () {
    this.set('value', '# Head\n\nHey. Meet the new Editor.');

    await render(hbs`<View::Blocks @value={{this.value}} @preview={{true}}/>`);
    expect(this.element.querySelector('h1')).not.exist;
    expect(this.element.querySelector('p')).not.exist;
    expect(this.element.textContent.trim()).to.equal(
      'Head\n\nHey. Meet the new Editor.',
    );
  });
});
