/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, waitUntil, typeIn, click } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/geometry", function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultBaseMap();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders an input geometry component", async function () {
    await render(hbs`<Editor::Geometry />`);
    expect(this.element.querySelector(".input-geometry")).to.exist;
  });

  it("triggers a change event when the geometry is updated", async function () {
    this.set("value", { type: "Point", coordinates: [1, 2] });

    let newValue;
    this.set("change", (v) => {
      this.set("value", v);
      newValue = v;
    });

    await render(hbs`<Editor::Geometry @value={{this.value}} @onChange={{this.change}}/>`);
    await waitUntil(() => this.element.querySelector(".map").olMap);
    await click(".btn-paste");

    const editor = this.element.querySelector(".input-geo-json");

    editor.value = "";
    await typeIn(editor, `{ "type": "Point", "coordinates": [11, 12] }`);

    expect(newValue).to.deep.equal({
      type: "Point",
      coordinates: [11, 12],
    });
  });
});
