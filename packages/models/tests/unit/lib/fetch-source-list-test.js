/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { fetchSourceList } from 'models/lib/fetch-source';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Unit | Transform | fetchSourceList', function (hooks) {
  setupTest(hooks);

  let store;
  let server;
  let mapData;
  let featureData;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle();
    mapData = server.testData.storage.addDefaultMap();
    featureData = server.testData.storage.addDefaultFeature();

    store = this.owner.lookup('service:store');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('does nothing when there is no parameter', async function (a) {
    const modelData = {};
    const result = await fetchSourceList('article', {}, modelData, {}, store);

    expect(result.model).to.equal('article');
    expect(result.query).to.deep.equal({});
    expect(result.items.map((a) => a.id)).to.deep.equal(['about', '5ca78e2160780601008f69e6']);
  });

  it("sets the selected model id when the source is 'selected-model' as string", async function (a) {
    const modelData = { selectedId: 'some-id' };
    const result = await fetchSourceList('article', { parameters: { key: 'selected-model' } }, modelData, {}, store);

    expect(result.model).to.equal('article');
    a.deepEqual(result.query, {
      key: 'some-id',
    });
    expect(result.items.map((a) => a.id)).to.deep.equal(['about', '5ca78e2160780601008f69e6']);
  });

  it("sets the selected model id when the source is 'selected-model' as object", async function (a) {
    const modelData = { selectedId: 'some-id' };
    const result = await fetchSourceList(
      'article',
      { parameters: { key: { source: 'selected-model' } } },
      modelData,
      {},
      store,
    );

    expect(result.model).to.equal('article');
    a.deepEqual(result.query, {
      key: 'some-id',
    });
    expect(result.items.map((a) => a.id)).to.deep.equal(['about', '5ca78e2160780601008f69e6']);
  });

  it("sets the state value when the source is 'state-category' as string", async function (a) {
    const modelData = { 'state-category': 'some-category' };
    const result = await fetchSourceList('article', { parameters: { key: 'state-category' } }, modelData, {}, store);

    expect(result.model).to.equal('article');
    a.deepEqual(result.query, {
      key: 'some-category',
    });
    expect(result.items.map((a) => a.id)).to.deep.equal(['about', '5ca78e2160780601008f69e6']);
  });

  it("sets the state value when the source is 'state-category' as object", async function (a) {
    const modelData = { 'state-category': 'some-category' };
    const result = await fetchSourceList(
      'article',
      { parameters: { key: { source: 'state', state: 'category' } } },

      modelData,
    );

    expect(result.model).to.equal('article');
    a.deepEqual(result.query, {
      key: 'some-category',
    });
  });

  it("sets the default value when the source is 'state-category' as object and the state is not set", async function (a) {
    const modelData = {};
    const result = await fetchSourceList(
      'article',
      { parameters: { key: { source: 'state', state: 'category', value: 'some value' } } },

      modelData,
    );

    expect(result.model).to.equal('article');
    a.deepEqual(result.query, {
      key: 'some value',
    });
  });

  it('sets the query value when the source is not the current model or a state', async function (a) {
    const modelData = { 'state-category': 'some-category' };
    const result = await fetchSourceList('article', { parameters: { key: 'some value' } }, modelData);

    expect(result.model).to.equal('article');
    a.deepEqual(result.query, {
      key: 'some value',
    });
  });

  it('sets the query value when the source is fixed', async function (a) {
    const modelData = { 'state-category': 'some-category' };
    const result = await fetchSourceList(
      'article',
      { parameters: { key: { source: 'fixed', value: 'test' } } },

      modelData,
    );

    expect(result.model).to.equal('article');
    a.deepEqual(result.query, {
      key: 'test',
    });
  });

  describe('default models', function () {
    it("uses the default map when the map key is 'default'", async function (a) {
      const modelData = {};
      const result = await fetchSourceList(
        'feature',
        { parameters: { map: { source: 'default' } } },
        modelData,
        {
          defaultModels: {
            map: mapData._id,
          },
        },
        store,
      );

      expect(result.model).to.equal('feature');
      a.deepEqual(result.query, {
        map: mapData._id,
      });
      expect(result.items.map((a) => a.id)).to.deep.equal([featureData._id]);
    });
  });
});
