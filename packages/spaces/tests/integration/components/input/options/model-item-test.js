/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | input/options/model-item", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let changedValue;
  let spaceData;
  let pageData;
  let receivedPicture;
  let picture1;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle("1");
    server.testData.storage.addDefaultArticle("2");
    server.testData.storage.addDefaultCampaign("1");
    server.testData.storage.addDefaultCampaign("2");
    spaceData = server.testData.storage.addDefaultSpace("2");
    pageData = server.testData.storage.addDefaultPage();
    picture1 = server.testData.storage.addDefaultPicture("1");

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { "Content-Type": "application/json" },
        JSON.stringify({
          picture: picture1,
        }),
      ];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders the value record when is set", async function () {
    this.set("value", {
      model: "article",
      id: "1",
    });

    await render(hbs`<Input::Options::ModelItem @value={{this.value}} />`);

    const selector = this.element.querySelector(".model-name");
    expect(selector.value).to.equal("article");

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("title");
  });

  it("does not render the model selectors when useSelectedModel is true", async function () {
    this.set("value", {
      useSelectedModel: true,
    });

    await render(hbs`<Input::Options::ModelItem @value={{this.value}} />`);

    expect(this.element.querySelector(".model-id")).not.to.exist;
    expect(this.element.querySelector(".model-name")).not.to.exist;
  });

  it("can select to use the selected model", async function () {
    this.set("value", { data: { id: "1" } });

    await render(hbs`<Input::Options::ModelItem @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".use-current-model").value = "yes";
    await triggerEvent(".use-current-model", "change");

    expect(changedValue).to.deep.equal({
      useSelectedModel: true,
    });
  });

  it("can select a custom model and article from everywhere when there is no space", async function (a) {
    this.set("value", {});

    await render(hbs`<Input::Options::ModelItem @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".model-name").value = "article";
    await triggerEvent(".model-name", "change");

    a.deepEqual(changedValue, { useSelectedModel: false, useDefaultModel: false, model: "article", id: null });

    await selectSearch(".model-id", "test");
    await selectChoose(".model-id", "title");

    a.deepEqual(changedValue, { useSelectedModel: false, useDefaultModel: false, model: "article", id: "1" });
  });

  it("searches records in the own team for a non default space", async function () {
    const space = await this.store.findRecord("space", spaceData._id);
    space.visibility.isDefault = false;

    this.set("value", {});
    this.set("space", space);

    await render(
      hbs`<Input::Options::ModelItem @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.element.querySelector(".model-name").value = "article";
    await triggerEvent(".model-name", "change");

    expect(changedValue).to.deep.equal({ useSelectedModel: false, useDefaultModel: false, model: "article", id: null });

    server.history = [];
    await selectSearch(".model-id", "test");
    await selectChoose(".model-id", "title");

    expect(server.history).deep.equal(["GET /mock-server/articles?team=61292c4c7bdf9301008fd7b6&term=test"]);
  });

  it("searches records in the all teams for a default space", async function () {
    const space = await this.store.findRecord("space", spaceData._id);
    space.visibility.isDefault = true;

    this.set("value", {});
    this.set("space", space);

    await render(
      hbs`<Input::Options::ModelItem @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.element.querySelector(".model-name").value = "article";
    await triggerEvent(".model-name", "change");

    expect(changedValue).to.deep.equal({ useSelectedModel: false, useDefaultModel: false, model: "article", id: null });

    server.history = [];
    await selectSearch(".model-id", "test");
    await selectChoose(".model-id", "title");

    expect(server.history).deep.equal(["GET /mock-server/articles?term=test"]);
  });

  it("hides the model option when the model argument is set", async function () {
    this.set("value", {});

    await render(
      hbs`<Input::Options::ModelItem @model="campaign" @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    expect(this.element.querySelector(".model-name")).not.to.exist;

    await selectSearch(".model-id", "Campaign");
    await selectChoose(".model-id", "Campaign 1");

    expect(changedValue).to.deep.equal({
      useSelectedModel: false,
      useDefaultModel: false,
      model: "campaign",
      id: "1",
    });
  });

  it("does not have a picture model option", async function (a) {
    this.set("value", {});

    await render(hbs`<Input::Options::ModelItem @value={{this.value}} @onChange={{this.onChange}} />`);

    const options = [...this.element.querySelectorAll(".model-name option")].map((a) => a.textContent.trim());

    expect(options).not.to.contain("picture");
  });

  describe("when a picture is needed", async function () {
    it("allows to select the picture model", async function (a) {
      this.set("value", {});

      await render(
        hbs`<Input::Options::ModelItem @withPicture={{true}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      const options = [...this.element.querySelectorAll(".model-name option")].map((a) => a.textContent.trim());

      a.deepEqual(options, [
        "",
        "article",
        "map",
        "survey",
        "icon",
        "icon set",
        "feature",
        "team",
        "event",
        "calendar",
        "newsletter",
        "picture",
      ]);
    });

    it("allows to upload a picture when the model is a picture", async function (a) {
      this.set("value", {});

      await render(
        hbs`<Input::Options::ModelItem @withPicture={{true}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      this.element.querySelector(".model-name").value = "picture";
      await triggerEvent(".model-name", "change");

      expect(this.element.querySelector(".model-id")).not.to.exist;
      expect(this.element.querySelector(".input-picture")).to.exist;

      const blob = server.testData.create.pngBlob();
      await triggerEvent(".input-picture [type='file']", "change", {
        files: [blob],
      });

      expect(receivedPicture).to.exist;
      expect(changedValue).to.deep.equal({
        useSelectedModel: false,
        useDefaultModel: false,
        model: "picture",
        id: "1",
      });
    });

    it("renders a picture when it is set", async function (a) {
      this.set("value", { useSelectedModel: false, model: "picture", id: "1" });

      await render(
        hbs`<Input::Options::ModelItem @withPicture={{true}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      expect(this.element.querySelector(".input-picture .image")).to.have.attribute(
        "style",
        "background-image: url('/test/5d5aa72acac72c010043fb59.jpg.md.jpg?rnd=undefined')",
      );
    });
  });

  describe("preferredModel", function () {
    it("only shows the record selection when the preferred model is a page", async function () {
      this.set("value", {});
      this.set("space", { id: "1", getPagesMap: () => ({ "": "000000000000000000000001" }) });

      await render(
        hbs`<Input::Options::ModelItem @preferredModel="page" @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      expect(this.element.querySelector(".use-current-model")).not.to.exist;
      expect(this.element.querySelector(".model-name")).not.to.exist;
      expect(this.element.querySelector(".use-default-model")).not.to.exist;
      expect(this.element.querySelector(".model-name")).not.to.exist;
      expect(this.element.querySelector(".model-id")).to.exist;

      await selectSearch(".model-id", "test");
      await selectChoose(".model-id", pageData.name);

      expect(changedValue).to.deep.equal({
        useSelectedModel: false,
        useDefaultModel: false,
        model: "page",
        id: "000000000000000000000001",
      });
    });
  });
});
