/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, waitFor, fillIn, click, waitUntil, triggerEvent } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/campaignanswers/edit', function (hooks) {
  setupApplicationTest(hooks, { server: true });
  let server;
  let answer;
  let receivedAnswer;
  let receivedSound;
  let user;
  let restored;
  let campaign;
  let icon;

  hooks.beforeEach(async function () {
    server = new TestServer();
    receivedAnswer = null;

    user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    const profile = server.testData.storage.addDefaultProfile();
    server.testData.storage.addDefaultUser(false, profile._id);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultProfile('some-user-id');
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultBaseMap();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultFeature('5ca78e2160780601008f69e6');
    server.testData.storage.addDefaultFeature();

    let picture = server.testData.storage.addDefaultPicture();
    let newPicture = server.testData.storage.addDefaultPicture('12344');
    campaign = server.testData.storage.addDefaultCampaign();
    icon = server.testData.storage.addDefaultIcon();

    answer = server.testData.storage.addDefaultCampaignAnswer();
    answer.icons = [icon._id];
    answer.info.originalAuthor = profile._id;
    answer.pictures = [picture._id];
    answer.canEdit = true;
    answer.canReview = true;

    campaign.icons = [icon._id];

    server.testData.storage.addDefaultUser(answer.info.originalAuthor);

    server.put(`/mock-server/campaignanswers/:id`, (request) => {
      receivedAnswer = JSON.parse(request.requestBody);
      receivedAnswer.campaignAnswer._id = request.params.id;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({ campaignAnswer: receivedAnswer.campaignAnswer }),
      ];
    });

    server.post(`/mock-server/pictures`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: newPicture,
        }),
      ];
    });

    server.put(`/mock-server/pictures/:id`, (request) => {
      let picture = JSON.parse(request.requestBody).picture;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: {
            ...picture,
            _id: request.params.id,
          },
        }),
      ];
    });

    restored = false;
    server.post(`/mock-server/campaignanswers/${answer._id}/restore`, (request) => {
      restored = true;

      return [201, {}];
    });

    receivedSound = null;
    server.post('/mock-server/sounds', (request) => {
      receivedSound = JSON.parse(request.requestBody);
      receivedSound.sound._id = 'test';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedSound)];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('can visit /manage/campaign-surveys/edit', async function () {
    authenticateSession();

    await visit(`/manage/survey-answers/${answer._id}`);
    expect(currentURL()).to.equal(`/manage/survey-answers/${answer._id}`);
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/survey-answers/${answer._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fsurvey-answers%2F5ca78aa160780601008f6bbb');
  });

  it('shows the campaign breadcrumbs when the user is part of the team', async function (a) {
    authenticateSession();

    await visit(`/manage/survey-answers/${answer._id}`);

    a.deepEqual(PageElements.breadcrumbs(), ['Dashboard', 'Open Green Map', 'Campaign 1', 'some test']);
    a.deepEqual(PageElements.breadcrumbLinks(), [
      '/manage/dashboards',
      '/manage/dashboards/000000000000000000000001',
      '/manage/dashboards/survey/5ca78aa160780601008f6aaa',
    ]);
  });

  describe('the fields sections', function (hooks) {
    it('renders the article', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      expect(this.element.querySelector('.article-viewer h1').textContent.trim()).to.equal('some test');
      expect(this.element.querySelector('.article-viewer p').textContent.trim()).to.equal(
        'This is a test with a description',
      );

      await PageElements.wait(100);
    });

    it('can update the article', async function (a) {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      await click('.row-article .btn-edit');

      await waitFor('.editor-is-ready', { timeout: 3000 });

      await fillIn('.ce-paragraph', 'some new test description');

      await waitUntil(
        () => !this.element.querySelector('.row-article .btn-submit').attributes.getNamedItem('disabled'),
      );

      await click('.row-article .btn-submit');

      await waitUntil(() => receivedAnswer);

      expect(receivedAnswer.campaignAnswer.attributes.about.description).to.deep.equal({
        blocks: [
          { type: 'header', data: { text: 'some test', level: 1 } },
          {
            type: 'paragraph',
            data: {
              text: 'some new test description',
            },
          },
        ],
      });

      await PageElements.wait(100);
    });

    it('renders the icons section', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelectorAll('.icon-container .icon-name')).to.have.length(1);
      expect(this.element.querySelector('.icon-container .icon-name').textContent.trim()).to.equal('Healthy Dining');

      await PageElements.wait(1000);
    });

    it('can update the icons', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.row-icons .btn-edit');
      await click('.row-icons .btn-remove-icon');

      await click('.btn-submit');

      await waitUntil(() => receivedAnswer);

      expect(receivedAnswer.campaignAnswer.icons).to.deep.equal([]);

      await PageElements.wait(1000);
    });

    it('renders the geometry section', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await PageElements.waitEditorJs(this.element);

      const map = this.element.querySelector('.map').olMap;

      const features = map.getLayers().item(0).getSource().getFeatures();

      const coordinates = features[0].getGeometry().transform('EPSG:3857', 'EPSG:4326').getCoordinates();

      expect([Math.ceil(coordinates[0]), Math.ceil(coordinates[1])]).to.deep.equal([14, 53]);

      await PageElements.wait(100);
    });

    it('can change the geometry', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.row-location .btn-edit');

      const map = this.element.querySelector('.map').olMap;

      const view = map.getView();
      view.setZoom(16);
      view.setCenter([1502287.3822740193, 6886844.872352015]);

      await click('.btn-submit');

      await waitUntil(() => receivedAnswer);

      receivedAnswer.campaignAnswer.position.coordinates = receivedAnswer.campaignAnswer.position.coordinates.map((a) =>
        Math.ceil(a),
      );

      expect(receivedAnswer.campaignAnswer.position).to.deep.equal({
        type: 'Point',
        coordinates: [14, 53],
      });

      await PageElements.wait(1000);
    });

    it('renders the pictures section', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await PageElements.waitEditorJs(this.element);

      const pictures = this.element.querySelectorAll('.row-picture-list img');

      expect(pictures).to.have.length(1);
      expect(pictures[0]).to.have.attribute('src', '/test/5d5aa72acac72c010043fb59.jpg/md?rnd=');

      await PageElements.wait(100);
    });

    it('can add a picture', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.row-picture-list .btn-edit');

      const blob = server.testData.create.pngBlob();

      await triggerEvent('.input-photo-gallery-input', 'change', {
        files: [blob],
      });

      await click('.btn-submit');
      await waitUntil(() => receivedAnswer);

      expect(receivedAnswer.campaignAnswer.pictures).to.deep.equal(['5cc8dc1038e882010061545a', '12344']);

      await PageElements.wait(1000);
    });

    it('renders the answer status', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.status-badge').textContent.trim()).to.equal('pending');
      expect(this.element.querySelector('.status-user').textContent.trim()).to.equal('mr Bogdan Szabo');
      expect(this.element.querySelector('.status-message').textContent.trim()).to.equal('wants to contribute to');
      expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');

      await PageElements.wait(100);
    });

    it('can update the attributes section', async function () {
      icon.name = 'Art Spot';
      icon.attributes = [
        {
          help: '',
          isPrivate: true,
          name: 'attribute1',
          displayName: 'attribute 1',
          type: 'boolean',
          options: '',
          isInherited: false,
        },
        {
          help: '',
          isPrivate: true,
          name: 'attribute2',
          displayName: 'attribute 2',
          type: 'short text',
          options: '',
          isInherited: false,
        },
      ];
      answer.attributes['Art Spot'] = {
        attribute1: true,
        attribute2: 'some value',
      };

      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);
      await click('.row-art-spot .btn-edit');

      await click('.btn-false');
      await fillIn('.attribute-value-attribute2 input', 'some new attribute');

      await click('.btn-submit');

      await waitUntil(() => receivedAnswer);

      expect(receivedAnswer.campaignAnswer.attributes['Art Spot']).to.deep.equal({
        attribute1: false,
        attribute2: 'some new attribute',
      });
    });

    it('can add a new sound', async function (a) {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      await server.waitAllRequests();

      await click('.container-group-sound-gallery .btn-edit');

      await waitFor('.sound-list-input');

      const blob = server.testData.create.mp3Blob();
      await triggerEvent('.sound-list-input', 'change', { files: [blob] });

      await click('.btn-submit');
      await waitUntil(() => receivedSound != null, { timeout: 3000 });
      await waitUntil(() => receivedAnswer != null, { timeout: 3000 });

      expect(receivedAnswer.campaignAnswer.sounds).to.deep.equal(['test']);

      a.deepEqual(receivedSound.sound, {
        name: 'coolsound.mp3',
        sound:
          'data:audio/mpeg;base64,DcOBwrHDlcK0woHCicOJwoXCucKRw6TCgcKJwrHDlcKVw4wBUSVQw4gAAAA8AAANBcKxwrDCgcOlwr3DlcOIwoHCscK9w5nClAFRQRTDhAAAADQAAA05wqXCncKhw5HCscK9w43ClcOJw4wBUU1NFAAAADwAAA0xwoXDmcKYw5TDoMK4w4jDpMK4w4TDgMOAAQVBJQwABQXDkAAADcKlwrXChcKdwpTCvcKpw4HClQ==',
        campaignAnswer: '5ca78aa160780601008f6bbb',
        feature: null,
        _id: 'test',
      });

      await PageElements.wait(100);
    });

    it('renders an alert when the survey has no contributor questions', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      await server.waitAllRequests();

      expect(this.element.querySelector('.row-contributor .alert.alert-warning')).to.have.textContent(
        'The survey does not have contributor questions.',
      );
    });

    it('can edit the campaign contributor when exist', async function () {
      campaign.contributorQuestions = [
        {
          question: 'email?',
          type: 'email',
          name: 'email',
          isRequired: true,
          isVisible: true,
        },
        {
          question: 'name?',
          type: 'short text',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ];

      answer.contributor = {
        email: 'a@b.c',
        name: 'some name',
      };

      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      await click('.row-contributor .btn-edit');
      const items = [...this.element.querySelectorAll('input')];

      expect(items[0].value).to.equal('a@b.c');
      expect(items[1].value).to.equal('some name');

      await fillIn(items[0], 'email@a.b');

      await click('.btn-submit');

      await waitUntil(() => receivedAnswer != null, { timeout: 3000 });

      expect(receivedAnswer.campaignAnswer.contributor).to.deep.equal({ email: 'email@a.b', name: 'some name' });
    });

    it('can edit the campaign other answers when they exist', async function () {
      campaign.questions.push({
        question: 'extra info?',
        type: 'short text',
        name: 'extra',
        isRequired: true,
        isVisible: true,
      });

      answer.attributes.other = { extra: 'extra value' };

      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      await click('.row-extra-questions .btn-edit');

      expect(this.element.querySelector('.attribute-value-extra input').value).to.equal('extra value');

      await fillIn('.attribute-value-extra input', 'new extra value');

      await click('.btn-submit');

      await waitUntil(() => receivedAnswer != null, { timeout: 3000 });

      expect(receivedAnswer.campaignAnswer.attributes.other).to.deep.equal({ extra: 'new extra value' });
    });
  });

  describe('a record with canEdit = false', function (hooks) {
    hooks.beforeEach(function () {
      answer.status = 2;
      answer.canEdit = false;
    });

    it('does not render the edit buttons', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      expect(this.element.querySelector('.btn-edit')).not.to.exist;
    });

    it('does not render the article editor', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      expect(this.element.querySelector('.article-editor')).not.to.exist;
      expect(this.element.querySelector('.article-viewer')).to.exist;
      await PageElements.wait(100);
    });

    it('does not render an editable icon selector', async function () {
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      expect(this.element.querySelector('.btn-remove-icon')).not.to.exist;
      expect(this.element.querySelector('.icon-select')).not.to.exist;
      await PageElements.wait(100);
    });

    it('renders the review box', async function () {
      answer.review = {
        user: user._id,
        reviewedOn: '2022-02-12T13:32:22',
      };
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      expect(this.element.querySelector('.campaign-answer-review')).to.exist;

      await click('.btn-revert');

      expect(restored).to.be.true;
      await PageElements.wait(100);
    });

    it('renders the answer field without the map details', async function () {
      campaign.map.isEnabled = false;
      campaign.map.map = '';

      answer.review = {
        user: user._id,
        reviewedOn: '2022-02-12T13:32:22',
      };

      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      expect(this.element.querySelector('.campaign-status')).not.to.exist;
    });
  });

  describe('a record with canReview = false', function (hooks) {
    hooks.beforeEach(function () {
      answer.status = 2;
      answer.canReview = false;
    });

    it('hides the review box', async function () {
      answer.review = {
        user: user._id,
        reviewedOn: '2022-02-12T13:32:22',
      };
      authenticateSession();

      await visit(`/manage/survey-answers/${answer._id}`);

      expect(this.element.querySelector('.campaign-answer-review')).not.to.exist;
    });
  });
});
