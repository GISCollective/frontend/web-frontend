/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { Toast } from 'bootstrap';
import { service } from '@ember/service';

export default class ToastComponent extends Component {
  @service fastboot;

  @action
  setup(element) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.toast = new Toast(element, {
      delay: 10000,
    });

    this.toast.show();

    element.addEventListener('hidden.bs.toast', () => {
      if (this.isDestroyed || this.isDestroying) {
        return;
      }

      if (this.args.onClose) {
        this.args.onClose(this.args.toastId);
      }
    });
  }
}
