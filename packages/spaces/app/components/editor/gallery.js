/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "./base/modelItem";

export default class ManageEditorsGalleryComponent extends Component {
  config = [
    {
      name: "primaryPhoto",
      options: ["hidden", "unique", "same as the list"],
    },
    {
      name: "showPrimaryPhotoAttributions",
      type: "bool",
    },
    {
      name: "showPhotoList",
      type: "bool",
    },
  ];
}
