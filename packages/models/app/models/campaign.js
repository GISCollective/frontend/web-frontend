/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { hasMany, attr, belongsTo } from '@ember-data/model';
import Model from './base/primary-model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';
import { get } from '@ember/object';
import { OptionalMap } from 'models/transforms/optional-map';

export default class CampaignModel extends Model {
  @attr('string') name;
  @attr article;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr('optional-map', { defaultValue: () => new OptionalMap() }) map;

  @hasMany('icon', { async: true, inverse: null })
  icons;

  @hasMany('icon', { async: true, inverse: null })
  optionalIcons;

  @attr('geo') area;
  @attr('model-list-with-default', { model: 'base-map' }) baseMaps;
  @attr('campaign-questions') options;

  @attr('question-list') questions;
  @attr('question-list') contributorQuestions;
  @attr notifications;

  @attr('date', {
    defaultValue() {
      return new Date();
    },
  })
  startDate;

  @attr('date', {
    defaultValue() {
      let value = new Date();
      value.setFullYear(value.getFullYear() + 1);
      return value;
    },
  })
  endDate;

  @attr('boolean') canEdit;

  async getCover() {
    return await this.cover;
  }

  async loadRelations() {
    const fields = ['icons', 'optionalIcons', 'cover', ''];

    for (const field of fields) {
      try {
        // eslint-disable-next-line ember/no-get
        await get(this, field);
      } catch (err) {
        // eslint-disable-next-line no-console
        console.log(err);
      }
    }

    try {
      await this.visibility.fetchTeam();
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
    }
  }

  get title() {
    return this.name;
  }

  get contentBlocks() {
    return toContentBlocks(this.article, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  set descriptionContentBlocks(content) {
    this.description = toContentBlocks(content, this.name);
  }

  get articleContentBlocks() {
    return toContentBlocks(this.article);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async fillMetaInfo(metaInfo) {
    const cover = await this.cover;

    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;
    metaInfo.imgSrc = cover?.picture;
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }

  async removeNotifications() {
    return await this.store.adapterFor(this.constructor.modelName).createNotification(this.id);
  }

  async addNotifications() {
    return await this.store.adapterFor(this.constructor.modelName).removeNotification(this.id);
  }
}
