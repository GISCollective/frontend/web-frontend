/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { waitUntil } from '@ember/test-helpers';
import Pretender from 'pretender';
import { buildWaiter } from '@ember/test-waiters';
import TestData from './test-data';

const waiter = buildWaiter('test-server');

let tokens = [];
let prevServer = null;

export default class TestServer {
  constructor() {
    if (prevServer) {
      prevServer.shutdown();
      // throw new Error(`Prev server not stopped. ${counter}`);
    }

    this.currentRequestNumber = 0;
    this.testData = new TestData(this);

    this.history = [];
    prevServer = this;

    this.server = new Pretender(function () {
      this.post('/write-coverage', this.passthrough);
    });

    this.server.handledRequest = (verb, path) => {
      this.history.push(`${verb} ${path}`);
      this.currentRequestNumber--;

      const lastToken = tokens.pop();

      if(lastToken) {
        waiter.endAsync(lastToken);
      }
    };

    this.server.get('/mock-server/service/logo', () => {
      this.currentRequestNumber++;
      tokens.push(waiter.beginAsync());

      return [200];
    });

    this.server.get('/api-v1/tiles/:z/:x/:y', () => {
      this.currentRequestNumber++;
      tokens.push(waiter.beginAsync());

      return [200, {}];
    });

    this.server.get(`/mock-server/spaces/:id/pages`, () => {
      this.currentRequestNumber++;
      tokens.push(waiter.beginAsync());

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(this.testData.create.pagesMap)];
    });

    this.server.get(`/mock-server/spaces/:id/categories`, () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    this.spaceSummary = {
      isPublic: true,
      logo: '5cc8dc1038e882010061545a',
      name: 'My service',
    };

    this.server.get(`/mock-server/spaces/_/summary`, () => {
      this.currentRequestNumber++;
      tokens.push(waiter.beginAsync());

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({ spaceSummary: this.spaceSummary })];
    });

    this.setupModelRoutes();

    const article = this.testData.create.article('about');
    article.visibility.isPublic = false;
    this.testData.storage.addArticle(article);
  }

  waitAllRequests() {
    return waitUntil(() => this.currentRequestNumber <= 0);
  }

  groupedIcons() {
    const result = {
      icons: [],
      categories: {},
    };

    Object.keys(this.testData.storage.icons)
      .map((a) => this.testData.storage.icons[a])
      .forEach((icon) => {
        if (!result['categories'][icon.category]) {
          result['categories'][icon.category] = {
            icons: [],
            categories: {},
          };
        }

        if (!result['categories'][icon.category]['categories'][icon.subcategory]) {
          result['categories'][icon.category]['categories'][icon.subcategory] = {
            icons: [],
            categories: {},
          };
        }

        if (icon.category && icon.subcategory) {
          result['categories'][icon.category]['categories'][icon.subcategory]['icons'].push(icon);
          return;
        }

        if (icon.category) {
          result['categories'][icon.category]['icons'].push(icon);
          return;
        }

        result['icons'].push(icon);
      });

    return result;
  }

  setupModelRoutes() {
    this.server.post('/mock-server/metrics', (request) => {
      this.currentRequestNumber++;
      tokens.push(waiter.beginAsync());

      const body = JSON.parse(request.requestBody);
      body.metric._id = '1';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(body)];
    });

    this.server.get('/mock-server/:plural', (request) => {
      this.currentRequestNumber++;
      tokens.push(waiter.beginAsync());

      const plural = this.testData.plurals[request.params.plural.toLowerCase()];
      const queryName = this.testData.storage[plural][request.queryParams.name];

      if (request.queryParams.name && !queryName && plural != 'metrics') {
        // eslint-disable-next-line no-console
        console.log(`Record ${plural} was not found for query params`, request.queryParams);
        return [404];
      }

      let value = [];

      if (plural == 'icons' && request.queryParams.group) {
        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(this.groupedIcons())];
      }

      if (request.queryParams.name && plural != 'metrics') {
        value = [this.testData.storage[plural][request.queryParams.name]];
      } else {
        value = Object.keys(this.testData.storage[plural]).map((key) => this.testData.storage[plural][key]);
      }

      if (request.queryParams.type) {
        value = value.filter((a) => a.type == request.queryParams.type);
      }

      if (request.queryParams.model) {
        value = value.filter((a) => a.relatedModel == request.queryParams.model);
      }

      const response = {};
      response[plural] = value;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(response)];
    });

    this.server.get('/mock-server/:plural/:id', (request) => {
      this.currentRequestNumber++;
      tokens.push(waiter.beginAsync());

      const plural = this.testData.plurals[request.params.plural.toLowerCase()];
      const singular = this.testData.singulars[request.params.plural.toLowerCase()];

      if (singular == 'stat') {
        request.params.id +=
          '-' +
          Object.keys(request.queryParams)
            .filter((a) => a != 'id')
            .map((key) => `${key}-${request.queryParams[key]}`)
            .join('-');
      }

      if (!this.testData.storage[plural][request.params.id]) {
        // eslint-disable-next-line no-console
        console.log(plural, request.params.id, 'not found');

        return [404];
      }

      const value = this.testData.storage[plural][request.params.id];
      const response = {};
      response[singular] = value;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(response)];
    });
  }

  shutdown() {
    if (this.currentRequestNumber > 0) {
      throw new Error('There are still some pending requests');
    }

    this.server.shutdown();
    prevServer = null;
  }

  get() {
    return this.server.get(...arguments);
  }

  delete_() {
    return this.server.delete(...arguments);
  }

  delete() {
    return this.server.delete(...arguments);
  }

  post() {
    return this.server.post(...arguments);
  }

  put() {
    return this.server.put(...arguments);
  }
}
