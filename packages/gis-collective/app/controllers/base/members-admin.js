/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { debounce } from '@ember/runloop';
import { service } from '@ember/service';

export default class MembersAdminController extends Controller {
  @service router;

  performUserSearch(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject('Term is too short');
    }

    this.store
      .query('user', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  searchUsers(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performUserSearch, term, resolve, reject, 600);
    });
  }

  @action
  membersChanged(value) {
    this.model.members = value;
  }

  @action
  save() {
    const map = this.model;

    return map.get('cover').then((cover) => {
      return cover.save().then(() => {
        return map.save().then(() => {
          this.isSaving = false;
          this.router.transitionTo('maps');
        });
      });
    });
  }
}
