import Component from '@glimmer/component';
import { service } from '@ember/service';
import { GeoJson } from 'core/lib/geoJson';
import { transform, transformExtent } from 'ol/proj';
import { action } from '@ember/object';

export default class MapLocationSelectorComponent extends Component {
  @service position;

  get extent() {
    return new GeoJson(this.args.extent);
  }

  get userInExtent() {
    if(!this.position.hasPosition) {
      return false;
    }

    const extent = this.extent.toOlFeature().getGeometry();
    const point = new GeoJson(this.position.center)
      .toOlFeature()
      .getGeometry()
      .getCoordinates();

    return extent.intersectsCoordinate(point);
  }

  @action
  setupExtent(map) {
    if(this.userInExtent) {
      return;
    }

    const extentM = this.extent.toOlFeature().getGeometry().getExtent();
    const extent = transformExtent(extentM, 'EPSG:4326', 'EPSG:3857');

    map.getView().fit(extent);
  }

  @action
  handleSave(map) {
    const center = transform(
      map.getView().getCenter(),
      'EPSG:3857',
      'EPSG:4326',
    );

    return this.args.onChange(center);
  }
}
