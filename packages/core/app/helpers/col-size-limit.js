/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { helper } from '@ember/component/helper';

export default helper(function colSizeLimit(positional) {
  const size = positional[0] ?? '';
  const classes = positional[1] ?? [];

  let options = Array.isArray(classes) ? classes : (classes?.split?.(' ') ?? []);

  const def = options.filter((a) => a.indexOf('-md-') == -1 && a.indexOf('-lg-') == -1);

  const md = options.filter((a) => a.indexOf('md-') != -1).map((a) => a.replace('md-', ''));
  const lg = options.filter((a) => a.indexOf('lg-') != -1).map((a) => a.replace('lg-', ''));

  const isMobile = size == 'mobile' || size == 'sm';
  const isTablet = size == 'tablet' || size == 'md';
  const isDesktop = size == 'desktop' || size == 'lg';

  if (isMobile) {
    return def.join(' ');
  }

  if (isTablet && md.length == 0) {
    return def.join(' ');
  }

  if (isTablet && md.length > 0) {
    return md.join(' ');
  }

  if (isDesktop && md.length == 0 && lg.length == 0) {
    return def.join(' ');
  }

  if (isDesktop && lg.length == 0) {
    return md.join(' ');
  }

  if (isDesktop && lg.length > 0) {
    return lg.join(' ');
  }

  return options.join(' ');
});
