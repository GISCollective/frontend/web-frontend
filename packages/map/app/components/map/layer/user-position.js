/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import Point from 'ol/geom/Point';
import VectorSource from 'ol/source/Vector';
import Vector from 'ol/layer/Vector';
import Feature from 'ol/Feature';
import { action } from '@ember/object';
import { fromLonLat } from 'ol/proj';
import { service } from '@ember/service';

export default class MapLayerUserPositionComponent extends Component {
  @service featureStyle;

  @action
  setup() {
    if (!this.args.map) {
      return;
    }

    this.map = this.args.map;

    if (this.vectorLayer) {
      this.map.removeLayer(this.vectorLayer);
    }

    if (this.args.position) {
      const geom = new Point(fromLonLat(this.args.position));
      this.feature = new Feature();
      this.feature.setStyle(this.featureStyle?.userPointer());
      this.feature.setGeometry(geom);

      this.vectorSource = new VectorSource({});
      this.vectorSource.addFeature(this.feature);
    }

    this.vectorLayer = new Vector({
      source: this.vectorSource,
      renderMode: 'vector',
    });

    this.args.map.addLayer(this.vectorLayer);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (!this.map) {
      return;
    }

    this.map.removeLayer(this.vectorLayer);
  }
}
