/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';

export default class PictureMeta {
  @tracked renderMode = '';
  @tracked attributions = '';
  @tracked link = {};
  @tracked data = {};
  @tracked properties = {};
  @tracked width;
  @tracked height;
  @tracked mime = '';
  @tracked format = '';
  @tracked disableOptimization = false;

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (serialized.renderMode) {
      this.renderMode = serialized.renderMode;
    }

    if (serialized.attributions) {
      this.attributions = serialized.attributions;
    }

    if (serialized.data) {
      this.data = serialized.data;
    }

    if (serialized.link) {
      this.link = serialized.link;
    }

    if (serialized.width) {
      this.width = serialized.width;
    }

    if (serialized.height) {
      this.height = serialized.height;
    }

    if (serialized.mime) {
      this.mime = serialized.mime;
    }

    if (serialized.format) {
      this.format = serialized.format;
    }

    if (serialized.properties) {
      this.properties = serialized.properties;
    }

    if (serialized.disableOptimization) {
      this.disableOptimization = serialized.disableOptimization;
    }
  }

  toJSON() {
    return {
      renderMode: this.renderMode,
      attributions: this.attributions,
      link: this.link,
      data: this.data,
      disableOptimization: this.disableOptimization,
    };
  }
}
