/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { htmlSafe } from '@ember/template';
import { service } from '@ember/service';

export default class BrowseMapsMapViewFeatureController extends Controller {
  @service embed;
  @service space;
  @service fastboot;

  get featureDetailsLink() {
    return this.space.currentSpace.getLinkFor("Feature", this.model.feature.id);
  }

  get linkTarget() {
    return this.embed.isEnabled ? '_blank' : '_self';
  }

  get coverStyle() {
    const picture = this.model.cover?.get?.('picture');

    if(!picture) {
      return htmlSafe("");
    }

    const cover = `${picture}/lg`;

    return htmlSafe(`background-image: url('${cover}')`);
  }

  get enableShowMore() {
    if (!this.embed.isEnabled) {
      return true;
    }

    return this.embed.showMore;
  }

  get attributesPageColValue() {
    return {
      data: {},
      record: this.model.feature
    };
  }
}
