/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class LanguageSelectorComponent extends Component {
  @service clickOutside;
  @tracked showingOptions;

  @action
  setup(element) {
    this.element = element;
  }

  @action
  show() {
    this.clickOutside.subscribe(this.element, () => {
      this.hide();
    });

    this.showingOptions = true;
  }

  @action
  hide() {
    this.showingOptions = false;
  }

  @action
  selectLanguage(item) {
    this.args.onSelect?.(item);
    this.showingOptions = false;
  }

  @action
  handleClick() {
    this.hide();
  }
}
