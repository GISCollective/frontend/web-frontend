/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';
import { service } from '@ember/service';

export default class PageAdapter extends Adapter {
  @service store;

  get modelName() {
    return 'page';
  }

  queryRecord(store, schema, query, options) {
    if (typeof query == 'string') {
      const url = this.buildURL('page', query);
      return this.ajax(url, 'GET');
    }

    return super.queryRecord(...arguments);
  }

  getRevisions(id) {
    const url = this.buildURL('page', id) + '/revisions';
    return this.ajax(url, 'GET');
  }

  async loadRevision(id, revisionId) {
    let url = this.buildURL('page', id);

    if (revisionId) {
      url += `/revisions/${revisionId}`;
    }

    const data = await this.ajax(url, 'GET');

    this.store.pushPayload(data);

    return this.store.peekRecord('page', id);
  }

  async contact(id, message) {
    let url = this.buildURL('page', id) + `/contact`;

    await this.ajax(url, 'POST', { data: { message } });
  }

  deleteMany(query) {
    const url = [this.urlForQuery(query, 'page'), this.getParams(query)].filter((a) => a).join('?');
    return this.ajax(url, 'DELETE');
  }
}
