/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, render, triggerEvent, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/manage/data-binding/connection-info',  function (hooks) {
  setupRenderingTest(hooks);

  describe ('in view mode',  function (hooks) {
    it('renders the selected value', async function () {
      this.set('value', {
        type: 'airtable',
        config: {
          apiKey: '1',
          baseId: '2',
          tableName: '3',
          viewName: '4',
        },
      });

      await render(
        hbs`<Input::Manage::DataBinding::ConnectionInfo @title="connection info" @value={{this.value}} />`
      );

      expect(
        this.element.querySelector('.text-connection-type').textContent.trim()
      ).to.equal('airtable');

      const values = this.element.querySelectorAll('.text-value');

      expect(values).to.have.length(4);
      expect(values[0].textContent.trim()).to.equal('1');
      expect(values[1].textContent.trim()).to.equal('2');
      expect(values[2].textContent.trim()).to.equal('3');
      expect(values[3].textContent.trim()).to.equal('4');
    });

    it('renders an alert when the type is not set', async function () {
      this.set('value', {});

      await render(
        hbs`<Input::Manage::DataBinding::ConnectionInfo @title="connection info" @value={{this.value}} />`
      );

      expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
        'The connection configuration is invalid.'
      );
    });

    it('does not render values for keys that start with _', async function () {
      this.set('value', {
        type: 'airtable',
        config: {
          _apiKey: '1',
          _baseId: '2',
          _tableName: '3',
          _viewName: '4',
        },
      });

      await render(
        hbs`<Input::Manage::DataBinding::ConnectionInfo @title="connection info" @value={{this.value}} />`
      );

      expect(
        this.element.querySelector('.text-connection-type').textContent.trim()
      ).to.equal('airtable');

      const values = this.element.querySelectorAll('.text-value');

      expect(values).to.have.length(4);
      expect(values[0].textContent.trim()).to.equal('hidden');
      expect(values[1].textContent.trim()).to.equal('hidden');
      expect(values[2].textContent.trim()).to.equal('hidden');
      expect(values[3].textContent.trim()).to.equal('hidden');
    });
  });

  describe ('in edit mode',  function (hooks) {
    it('renders the selected value', async function () {
      this.set('value', {
        type: 'airtable',
        config: {
          _apiKey: '1',
          baseId: '2',
          tableName: '3',
          viewName: '4',
        },
      });

      await render(
        hbs`<Input::Manage::DataBinding::ConnectionInfo @title="connection info" @value={{this.value}} @editablePanel="connection info"/>`
      );

      expect(this.element.querySelector('.select-type').value).to.equal(
        'airtable'
      );
      expect(
        this.element.querySelector('.connection-info-_apiKey').value
      ).to.equal('1');

      expect(
        this.element.querySelector('.connection-info-baseId').value
      ).to.equal('2');

      expect(
        this.element.querySelector('.connection-info-tableName').value
      ).to.equal('3');

      expect(
        this.element.querySelector('.connection-info-viewName').value
      ).to.equal('4');
    });

    it('can change the value', async function () {
      this.set('value', {
        type: '',
        config: {},
      });

      let value;

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Manage::DataBinding::ConnectionInfo @onSave={{this.save}} @title="connection info" @value={{this.value}} @editablePanel="connection info"/>`
      );

      this.element.querySelector('.select-type').value = 'airtable';
      await triggerEvent('.select-type', 'change');

      await fillIn('.connection-info-_apiKey', 'a');
      await fillIn('.connection-info-baseId', 'b');
      await fillIn('.connection-info-tableName', 'c');
      await fillIn('.connection-info-viewName', 'd');

      await click('.btn-submit');

      expect(value.type).to.equal('airtable');
      expect(value.config).to.deep.equal({
        _apiKey: 'a',
        baseId: 'b',
        tableName: 'c',
        viewName: 'd',
      });
    });
  });
});
