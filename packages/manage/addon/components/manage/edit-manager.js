import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export class EditManager {
  @tracked editableField;

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
  }
}

export default class ManageEditManagerComponent extends Component {
  @tracked editManager = new EditManager();
}
