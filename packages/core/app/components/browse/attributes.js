/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class BrowseAttributesComponent extends Component {
  get value() {
    if (!this.args.value || !this.args.value.length) {
      return [];
    }

    return this.args.value.filter((a) => a.isPresent || a.value);
  }

  get hasIndex() {
    return typeof this.args.index == 'number';
  }

  get index() {
    return this.args.index + 1;
  }
}
