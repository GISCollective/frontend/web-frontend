/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | card/cover-title', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function () {
    await render(hbs`<Card::CoverTitle />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the picture and title when a record is set', async function () {
    this.set('campaign', {
      id: 'some-id',
      name: 'some name',
      cover: {
        picture: 'some-picture',
      },
    });

    await render(hbs`<Card::CoverTitle @record={{this.campaign}} />`);

    expect(this.element.querySelector('img')).to.have.attribute('src', 'some-picture/lg');
    expect(this.element.querySelector('.card-title').textContent.trim()).to.equal('some name');
    expect(this.element.querySelector('.card-description')).not.exist;
  });

  it('does not render the description', async function () {
    this.set('campaign', {
      id: 'some-id',
      name: 'some name',
      cover: {
        picture: 'some-picture',
      },
      firstParagraph: 'description',
    });

    await render(hbs`<Card::CoverTitle @record={{this.campaign}} />`);

    expect(this.element.textContent).not.to.contain('description');
    expect(this.element.querySelector('.card-description')).not.exist;
  });

  it('allows to edit an editable campaign', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
      constructor: {
        modelName: 'campaign',
      },
    });

    await render(hbs`<Card::CoverTitle @record={{this.campaign}} />`);

    await click('.record-options');
    expect(this.element.querySelector('.dropdown-item-edit')).to.exist;
  });
});
