/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/text-simple", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the value", async function () {
    this.set("value", "some text");

    await render(hbs`<Editor::TextSimple @value={{this.value}} />`);

    expect(this.element.querySelector(".input-text-value").value).to.equal("some text");
  });

  it("can update the value", async function () {
    this.set("value", "some text");

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::TextSimple @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".input-text-value", "new text");

    expect(value).to.equal("new text");
  });
});
