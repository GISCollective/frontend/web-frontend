/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';

export default class SpaceMenuTransform extends Transform {
  deserialize(serialized) {
    return new SpaceMenu(serialized);
  }

  serialize(deserialized) {
    return deserialized.toJSON ? deserialized.toJSON() : deserialized;
  }
}

export class SpaceMenu {
  @tracked items = A([]);

  constructor(serialized) {
    if (!Array.isArray(serialized?.items)) {
      return;
    }

    this.items = A(serialized.items.map((a) => new SpaceMenuItem(a)));
  }

  toJSON() {
    return {
      items: this.items.map((a) => (a.toJSON ? a.toJSON() : a)),
    };
  }
}

export class SpaceMenuItem {
  @tracked name;
  @tracked link;
  @tracked dropDown;
  @tracked newTab;

  constructor(serialized) {
    this.name = serialized.name;
    this.newTab = serialized.newTab;
    this.link = new MenuLink(serialized.link);
    this.dropDown = A(serialized.dropDown?.map((a) => new SpaceMenuChildItem(a)) ?? []);
  }

  copyFields(other) {
    if (other.name != this.name) {
      this.name = other.name;
    }

    if (other.link != this.link) {
      this.link = other.link;
    }

    if (other.newTab != this.newTab) {
      this.newTab = other.newTab;
    }
  }

  toJSON() {
    const result = { name: '' };

    if (this.name) {
      result.name = this.name;
    }

    if (this.newTab) {
      result.newTab = this.newTab;
    }

    if (this.link) {
      result.link = this.link.toJSON ? this.link.toJSON() : this.link;
    }

    if (this.dropDown) {
      result.dropDown = this.dropDown?.map((a) => (a.toJSON ? a.toJSON() : a));
    }

    return result;
  }
}

export class MenuLink {
  @tracked pageId;
  @tracked url;
  @tracked route;
  @tracked model;

  constructor(serialized) {
    this.pageId = serialized?.pageId;
    this.url = serialized?.url;
    this.route = serialized?.route;
    this.model = serialized?.model;
  }

  get slug() {
    let id = this.pageId ?? '';

    if (id.indexOf('/') == 0) {
      id = id.substr(1);
    }

    return id.split('/').join('--');
  }

  toJSON() {
    const result = {};

    if (this.pageId) {
      result.pageId = this.pageId;
    }

    if (this.url) {
      result.url = this.url;
    }

    if (this.route) {
      result.route = this.route;
    }

    if (this.model) {
      result.model = this.model;
    }

    return result;
  }
}

export class SpaceMenuChildItem {
  @tracked name;
  @tracked link;
  @tracked newTab;

  constructor(serialized) {
    this.name = serialized?.name;
    this.link = new MenuLink(serialized?.link);
  }

  copyFields(other) {
    if (other.name != this.name) {
      this.name = other.name;
    }

    if (other.link != this.link) {
      this.link = other.link;
    }

    if (other.newTab != this.newTab) {
      this.newTab = other.newTab;
    }
  }

  toJSON() {
    return {
      name: this.name,
      link: this.link?.toJSON ? this.link?.toJSON() : this.link,
      newTab: this.newTab,
    };
  }
}
