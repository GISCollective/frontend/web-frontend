/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from '../base/app-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class RoutesLoginRoute extends AppRoute {
  @service space;

  beforeModel(transition) {
    if (this.session.isAuthenticated) {
      this.router.transitionTo(transition.to.queryParams.redirect || 'index');
      return;
    }

    return super.beforeModel();
  }

  async getArticle() {
    if (!this.space.currentSpace?.visibility?.isDefault) {
      return null;
    }

    try {
      return await this.store.queryRecord('article', { id: 'login', team: this.space.currentSpace.visibility.teamId });
    } catch (err) {
      console.log('There is no login article.');
    }
  }

  async getGoogleClientIdPreference() {
    if (!this.space.currentSpace?.visibility?.isDefault) {
      return null;
    }

    try {
      return await this.preferences.getPreference('integrations.google.client_id');
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  }

  async model() {
    let googleClientIdPreference = await this.getGoogleClientIdPreference();
    let registrationEnabled = this.space.isDefault;
    let registerUrl;

    if(registrationEnabled) {
      registrationEnabled = (await this.preferences.getPreference('register.enabled')).value;
      registerUrl = (await this.preferences.getPreference('register.url')).value;
    }

    const name = this.space.currentSpace?.name ?? this.space.defaultSpace?.name;

    return hash({
      article: this.getArticle(),
      locale: this.locale,
      registrationEnabled: registrationEnabled === "true" || registrationEnabled === true,
      registerUrl,
      name,
      googleClientId: googleClientIdPreference?.value,
    });
  }
}
