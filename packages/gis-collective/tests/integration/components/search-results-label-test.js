/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | search-results-label', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a not found message when there are no results', async function () {
    this.set('results', []);

    await render(
      hbs`<SearchResultsLabel @term="some term" @results={{this.results}}/>`
    );
    expect(this.element.textContent.trim()).to.equal(
      'Unfortunately we could not find any results for « some term »'
    );
  });

  it('renders a found message when there are no results', async function () {
    this.set('results', [{}]);

    await render(
      hbs`<SearchResultsLabel @term="some term" @results={{this.results}}/>`
    );
    expect(this.element.textContent.trim()).to.equal(
      'Search results for « some term »'
    );
  });
});
