/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class NewRecordFormPageComponent extends Component {
  @service user;
  @tracked hasSpace;

  get canSubmit() {
    return this.editableModel?.name && this.editableModel?.slug && this.hasSpace;
  }

  get editableModel() {
    return this.args.page;
  }

  get showSpaces() {
    return this.user.isAdmin || this.args.spaces?.length > 1;
  }

  @action
  change(field, value) {
    this.editableModel?.set(field, value);
    this.hasSpace = !!this.editableModel?.space;
  }

  @action
  setup() {
    if (this.args.spaces?.length && this.args.page && !this.args.page?.space) {
      this.args.page.space = this.args.spaces[0];
      this.hasSpace = true;
    }
  }

  @action
  save() {
    this.args.page.visibility = this.args.page.space.get?.('visibility') ?? {};
    this.args.page.visibility.isPublic = false;
    this.args.page.visibility.isDefault = false;
    return this.args.onSubmit(this.args.page);
  }
}
