/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | view/card-list', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let campaign;
  let event;
  let event2;
  let event3;
  let calendar;
  let calendarData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultCampaign('1');
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultEvent('2');
    server.testData.storage.addDefaultEvent('3');
    server.testData.storage.addDefaultEvent('4');
    calendarData = server.testData.storage.addDefaultCalendar();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };

    const store = this.owner.lookup('service:store');
    campaign = await store.findRecord('campaign', '1');
    event = await store.findRecord('event', '2');
    event2 = await store.findRecord('event', '3');
    event3 = await store.findRecord('event', '4');
    calendar = await store.findRecord('calendar', calendarData._id);
  });

  it('renders the preloaded campaigns when they are set', async function () {
    server.history = [];
    this.set('value', {
      data: { query: { team: '1' } },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelectorAll('.card')).to.have.length(1);
    expect(server.history).to.deep.equal([
      'GET /mock-server/pictures/5cc8dc1038e882010061545a',
    ]);
  });

  it('uses the custom deprecated destination for links when set', async function () {
    server.history = [];
    this.set('value', {
      data: {
        query: { team: '1' },
        destination: { slug: '/custom-page/:campaign-id' },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/custom-page/1',
    );
    expect(server.history).to.deep.equal([
      'GET /mock-server/pictures/5cc8dc1038e882010061545a',
    ]);
  });

  it('allows deleting a record', async function () {
    this.set('value', {
      data: { query: { team: '1' } },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    server.server.delete(`/mock-server/campaigns/${campaign._id}`, () => {
      return [204, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    let destroyRecordCalled = false;
    campaign.canEdit = true;
    campaign.destroyRecord = () => {
      destroyRecordCalled = true;
    };

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    await waitFor('.card .btn-delete');
    await click('.card .btn-delete');

    expect(destroyRecordCalled).to.equal(true);
  });

  it('shows a message when there is no record', async function (a) {
    this.set('value', {
      data: {
        emptyMessage: {
          color: 'red',
          classes: ['text-center', 'fw-bold'],
          minLines: { sm: +0, md: +0, lg: +0 },
          text: 'this is a new message',
        },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(
      this.element.querySelector('.empty-message').textContent.trim(),
    ).to.equal('this is a new message');
    expect(this.element.querySelector('.empty-message')).to.have.classes(
      'text-with-options text-center fw-bold text-red  empty-message',
    );
  });

  it('applies the deprecated component options', async function (a) {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {
          classes: [
            'border-visibility-yes',
            'border-radius-2',
            'border-color-green-700',
          ],
          proportion: '16:26',
        },
        columns: { cardWidth: ['col-3'] },
        components: { cover: true, name: true, description: true },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    await waitFor('.loaded');

    expect(this.element.querySelector('.picture-bg')).to.have.attribute(
      'class',
      'picture-bg border-visibility-yes border-radius-2 border-color-green-700',
    );

    const style = this.element
      .querySelector('.picture-bg')
      .attributes.getNamedItem('style').value;

    expect(style).to.contain('aspect-ratio: 16/26;');
    expect(style).to.contain(
      'background-image: url(/test/5d5aa72acac72c010043fb59',
    );

    expect(
      this.element.querySelector('.card-title').textContent.trim(),
    ).to.equal('Campaign 1');

    expect(
      this.element.querySelector('.card-description').textContent.trim(),
    ).to.equal(
      "We'd like to collect urban nature sounds that characterize Berlin's natural life. Please help us find interesting places for sound recording! Here are the places and sounds we collected so far on the Berlin Sound Map.",
    );
  });

  it('applies the deprecated component column options', async function () {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {},
        columns: { classes: ['col-1', 'col-md-3', 'col-lg-4'] },
        components: { cardType: 'cover-title-description' },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.card-list')).to.have.attribute(
      'class',
      'card-list row g-4 mt-0',
    );

    expect(this.element.querySelector('.card-list .col')).to.have.classes(
      'col mt-0 col-1 col-md-3 col-lg-4',
    );
  });

  it('applies the component card options', async function () {
    this.set('value', {
      data: {
        card: {
          container: {
            classes: ['pt-1', 'col-2', 'col-md-3', 'col-lg-4'],
          },
          type: 'cover',
          destination: '/custom-page/:campaign-id',
        },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.card-title')).not.to.exist;
    expect(this.element.querySelector('.card-description')).not.to.exist;
    expect(this.element.querySelector('.card-list .col')).to.have.classes(
      'pt-1 col-2 col-md-3 col-lg-4',
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/custom-page/1',
    );
  });

  it('applies the component list container options', async function () {
    this.set('value', {
      data: {
        card: {
          containerList: {
            classes: ['justify-content-center'],
          },
        },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.card-list')).to.have.classes(
      'justify-content-center',
    );
  });

  it('uses some default column options when they are not set', async function () {
    this.set('value', {
      data: {
        source: { team: '1', model: 'campaign' },
        picture: {},
        columns: { cardWidth: [] },
        components: { cover: true, name: true, description: true },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaign]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.card-list')).to.have.attribute(
      'class',
      'card-list row g-4 mt-0',
    );
  });

  it('sets the event id as a hash for an event card with a feature destination', async function (a) {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {},
        columns: { classes: ['col-1', 'col-md-3', 'col-lg-4'] },
        components: { cardType: 'cover-title-description' },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
        destination: { slug: '/campaigns/:feature-id' },
      },
      modelKey: 'event',
    });

    event.location = {
      type: 'Feature',
      value: 'some-feature-id',
    };

    event.calendar = calendar;

    this.set('model', {
      event: { buckets: [[event]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/campaigns/some-feature-id#test-event',
    );
  });

  it('sets the card id', async function (a) {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {},
        columns: { classes: ['col-1', 'col-md-3', 'col-lg-4'] },
        components: { cardType: 'cover-title-description' },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
        destination: { slug: '/campaigns/:feature-id' },
      },
      modelKey: 'event',
    });

    this.set('model', {
      event: { buckets: [[event]] },
    });

    await render(
      hbs`<View::CardList @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.page-card')).to.have.attribute(
      'id',
      'test-event',
    );
  });

  it('shows the maxItems number of cards on lg screens (legacy)', async function (a) {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {},
        columns: { classes: ['col-1', 'col-md-3', 'col-lg-4'] },
        components: { cardType: 'cover-title-description' },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
        destination: { slug: '/campaigns/:feature-id' },
        sourceGroupStyle: {
          maxItems: {
            lg: 2,
            md: 1,
            sm: 1,
          },
        },
      },
      modelKey: 'event',
    });

    this.set('model', {
      event: { buckets: [[event, event2, event3]] },
    });

    await render(
      hbs`<View::CardList @deviceSize="lg" @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelectorAll('.page-card')).to.have.length(2);
  });

  it('shows the maxItems number of cards on lg screens', async function (a) {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {},
        columns: { classes: ['col-1', 'col-md-3', 'col-lg-4'] },
        components: { cardType: 'cover-title-description' },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
        destination: { slug: '/campaigns/:feature-id' },
        card: {
          maxItems: {
            lg: 2,
            md: 1,
            sm: 1,
          },
        },
      },
      modelKey: 'event',
    });

    this.set('model', {
      event: { buckets: [[event, event2, event3]] },
    });

    await render(
      hbs`<View::CardList @deviceSize="lg" @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelectorAll('.page-card')).to.have.length(2);
  });

  it('shows the maxItems number of cards from sm on md screens when the md value is not set (legacy)', async function (a) {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {},
        columns: { classes: ['col-1', 'col-md-3', 'col-lg-4'] },
        components: { cardType: 'cover-title-description' },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
        destination: { slug: '/campaigns/:feature-id' },
        sourceGroupStyle: {
          maxItems: {
            sm: 1,
          },
        },
      },
      modelKey: 'event',
    });

    this.set('model', {
      event: { buckets: [[event, event2, event3]] },
    });

    await render(
      hbs`<View::CardList @deviceSize="md" @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelectorAll('.page-card')).to.have.length(1);
  });

  it('shows the maxItems number of cards from sm on md screens when the md value is not set', async function (a) {
    this.set('value', {
      data: {
        query: { team: '1' },
        picture: {},
        columns: { classes: ['col-1', 'col-md-3', 'col-lg-4'] },
        components: { cardType: 'cover-title-description' },
        name: { options: [], color: '' },
        description: { options: [], color: '' },
        destination: { slug: '/campaigns/:feature-id' },
        card: {
          maxItems: {
            sm: 1,
          },
        },
      },
      modelKey: 'event',
    });

    this.set('model', {
      event: { buckets: [[event, event2, event3]] },
    });

    await render(
      hbs`<View::CardList @deviceSize="md" @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelectorAll('.page-card')).to.have.length(1);
  });
});
