/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ThemeWormholeComponent extends Component {
  @service theme;

  @action
  setup() {
    const body = window.document.querySelector('body');

    if (this.args.section != 'manage') {
      body.dataset.bsTheme = '';

      return;
    }

    body.dataset.bsTheme = this.theme.name;
  }
}
