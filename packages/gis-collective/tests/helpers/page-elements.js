/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { click, fillIn } from '@ember/test-helpers';
import Modal from './modal';

export default {
  wait(duration) {
    return new Promise((resolve) => {
      setTimeout(resolve, duration);
    });
  },

  getTextFor(selector) {
    let result = [];

    document.querySelectorAll(selector).forEach((a) => {
      result.push(a.textContent.trim());
    });

    return result;
  },

  getLinkFor(selector) {
    let result = [];

    document.querySelectorAll(selector).forEach((a) => {
      if (!a.hasAttribute('href')) {
        result.push(null);
        return;
      }

      result.push(a.attributes.getNamedItem('href').value);
    });

    return result;
  },

  getFirstLineFor(selector) {
    let result = [];

    document.querySelectorAll(selector).forEach((a) => {
      result.push(a.textContent.trim().split('\n')[0]);
    });

    return result;
  },

  breadcrumbs() {
    return this.getTextFor('.breadcrumb-item');
  },

  breadcrumbLinks() {
    return this.getLinkFor('.breadcrumb-item a');
  },

  rowTitles() {
    return this.getTextFor('.row > .col h2, .row > .col h3, .row > .col h5, .row .form-label');
  },

  containerGroupTitles() {
    return this.getFirstLineFor('.container-group .title, .panel-title');
  },

  containerGroupContents() {
    return this.getTextFor('.container-group .value');
  },

  formInputValues() {
    let result = [];

    document.querySelectorAll('.row input').forEach((a) => {
      result.push(a.value);
    });

    return result;
  },

  formSubmitButton() {
    return document.querySelector('.row-submit button.btn.btn-submit');
  },

  async editArticleName(element, name) {
    await click(element.querySelector('.btn-edit'));
    await fillIn(element.querySelector('input.input-name'), name);

    await click(element.querySelector('.btn-submit'));
  },

  async selectIconsInModal(icons) {
    await click('.attributes-icons .icon-select');

    await Modal.waitToDisplay();
    await click('.modal .card-header > button.collapsed');
    await click('.modal .card-header > button.collapsed');

    icons.forEach(async (id) => {
      await click(`.modal .btn-icon[data-id='${id}']`);
    });

    await click('.modal .btn-success');

    await Modal.waitToHide();
  },

  async waitEditorJs(element) {
    return new Promise((success) => {
      const timer = setInterval(() => {
        const editors = [...element.querySelectorAll(".editor-js")].length;
        const ready = [...element.querySelectorAll(".editor-js.editor-is-ready")].length;

        if (editors != ready) {
          return;
        }

        clearInterval(timer);
        success();
      });
    });
  },

  async openSideBar(element) {
    const openBtn = element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }
  },
};
