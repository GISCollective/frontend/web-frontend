/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { EventLocation } from 'models/transforms/event-location';

describe('Unit | Model | space', function (hooks) {
  setupTest(hooks, {}, this);

  let space;
  let server;
  let store;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    space = server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    this.set('space', space);
    store = this.owner.lookup('service:store');
    server.history = [];
  });

  it('exists', function () {
    let model = store.createRecord('space', {});
    expect(model).to.be.ok;
  });

  describe('getPagesMap', function () {
    it('queries the pages map on the server', async function () {
      let model = store.createRecord('space', { id: 'some-id' });
      const pages = await model.getPagesMap();

      expect(server.history).to.deep.equal(['GET /mock-server/spaces/some-id/pages']);
      expect(pages).to.deep.equal({ 'page--test': '000000000000000000000001' });
    });

    it('caches the queried value', async function () {
      let model = store.createRecord('space', { id: 'some-id' });

      await model.getPagesMap();
      const pages = await model.getPagesMap();

      expect(server.history).to.deep.equal(['GET /mock-server/spaces/some-id/pages']);
      expect(pages).to.deep.equal({ 'page--test': '000000000000000000000001' });
    });
  });

  describe('getPagePath', function () {
    it('returns the page path for an existing page id', async function () {
      let model = store.createRecord('space', { id: 'some-id' });

      const path = await model.getPagePath('000000000000000000000001');

      expect(path).to.equal('/page/test');
    });

    it('returns the page path for an existing page slug', async function () {
      let model = store.createRecord('space', { id: 'some-id' });

      const path = await model.getPagePath('page--test');

      expect(path).to.equal('/page/test');
    });

    it('returns undefined when the slug does not exist', async function () {
      let model = store.createRecord('space', { id: 'some-id' });

      const path = await model.getPagePath('missing');

      expect(path).to.equal(undefined);
    });
  });

  describe('getLinkFor', function () {
    it('returns the icon path when there is an icon page', async function () {
      let space = store.createRecord('space', { id: 'some-id' });

      space._getPagesMap = {
        'browse--icon--:icon-id': '000000000000000000000001',
      };

      expect(space.getLinkFor('Icon', '0002')).to.equal('/browse/icon/0002');
    });

    it('returns the path with the related ids', async function () {
      let space = store.createRecord('space', { id: 'some-id' });

      space._getPagesMap = {
        'browse--:calendar-id--:team-id--:feature-id--:event-id': '000000000000000000000001',
      };

      let calendar = store.createRecord('calendar', { id: '0005' });
      let event = store.createRecord('event', {
        id: '0003',
        calendar,
        location: new EventLocation({ type: 'Feature', value: '0004' }),
        visibility: { teamId: 'team-id' },
      });

      expect(space.getLinkFor('Event', event)).to.equal('/browse/0005/team-id/0004/0003');
    });

    it('returns the page matching a category when an article has one', async function () {
      let space = store.createRecord('space', { id: 'some-id' });
      let article = store.createRecord('article', { id: '0002', categories: ['category'] });

      space._getPagesMap = {
        'other--:article-id': '000000000000000000000002',
        'news--:article-id': '000000000000000000000001',
      };

      space._categories = {
        category: '000000000000000000000001',
      };

      expect(space.getLinkFor('Article', article)).to.equal('/news/0002');
    });

    it('uses the article slug when the article has one', async function () {
      let space = store.createRecord('space', { id: 'some-id' });
      let article = store.createRecord('article', { id: '0002', categories: ['category'], slug: 'some-slug' });

      space._getPagesMap = {
        'other--:article-id': '000000000000000000000002',
        'news--:article-id': '000000000000000000000001',
      };

      space._categories = {
        category: '000000000000000000000001',
      };

      expect(space.getLinkFor('Article', article)).to.equal('/news/some-slug');
    });

    it('returns the feature path when there is an feature page', function () {
      let space = store.createRecord('space', { id: 'some-id' });

      space._getPagesMap = {
        'browse--:feature-id': '000000000000000000000001',
      };

      expect(space.getLinkFor('Feature', '0002')).to.equal('/browse/0002');
    });

    it('ignores a _panel when a feature page is requested', function () {
      let space = store.createRecord('space', { id: 'some-id' });

      space._getPagesMap = {
        '_panel--feature--:feature-id': '00000000000000000000000a',
        'browse--:feature-id': '000000000000000000000001',
      };

      expect(space.getLinkFor('Feature', '0002')).to.equal('/browse/0002');
    });

    it('returns the legacy feature path when the space has no page defined', function () {
      let space = store.createRecord('space', { id: 'some-id' });

      expect(space.getLinkFor('Feature', '0002')).to.equal('/browse/sites/0002');
    });

    it('returns the legacy team path when the space has no page defined', function () {
      let space = store.createRecord('space', { id: 'some-id' });

      expect(space.getLinkFor('Team', '0002')).to.equal('/browse/teams/0002');
    });

    it('returns the legacy map path when the space has no page defined', function () {
      let space = store.createRecord('space', { id: 'some-id' });

      expect(space.getLinkFor('Map', '0002')).to.equal('/browse/sites?map=0002');
    });

    it('returns undefined when there is no match', function () {
      let space = store.createRecord('space', { id: 'some-id' });

      expect(space.getLinkFor('Icon', '0002')).not.to.exist;
    });
  });
});
