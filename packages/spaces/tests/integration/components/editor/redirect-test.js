/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | editor/redirect", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the current value", async function (assert) {
    this.set("value", {
      data: {
        redirect: {
          destination: {
            url: "https://giscollective.com",
          },
        },
      },
    });

    await render(hbs`<Editor::Redirect @value={{this.value}} />`);

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "https://giscollective.com",
    );
  });

  it("can change the url", async function (a) {
    this.set("value", {
      data: {
        redirect: {
          destination: {
            url: "https://giscollective.com",
          },
        },
      },
    });

    let value;

    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Redirect @value={{this.value}} @onChange={{this.change}}/>`);

    await selectSearch(".ember-power-select-trigger", "https://greenmap.com");
    await selectChoose(".ember-power-select-trigger", "https://greenmap.com");

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "https://greenmap.com",
    );
    a.deepEqual(value, {
      redirect: {
        destination: {
          url: "https://greenmap.com",
        },
      },
    });
  });
});
