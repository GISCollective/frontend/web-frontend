/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { dasherize } from '@ember/string';

export default class LayoutComponent extends Component {
  @tracked _containers;

  getContainerBgCol(index) {
    const col = this.args.cols?.find(
      (col) => col.container == index && col.row == -1 && col.col == -1,
    );

    if (!col?.type) {
      return this.getEmptyCol(index);
    }

    return col;
  }

  get visibleContainersItems() {
    return this.visibleContainers.map((a) => a.container);
  }

  get visibleContainers() {
    const list = [];

    let index = -1;
    for (const container of this.containers) {
      index++;

      if (!container.isVisible?.(this.args.conditions)) {
        continue;
      }

      const path = this.args.path
        ? `${this.args.path}.layoutContainers.${index}`
        : `layoutContainers.${index}`;

      let id = container.anchor ?? '';

      id = id.trim();

      list.push({
        container,
        index,
        path,
        id: dasherize(id || `container-${index}`),
      });
    }

    return list;
  }

  get containers() {
    if (this._containers) {
      return this._containers;
    }

    if (this.args.containers) {
      return this.args.containers;
    }

    return this.args.page?.layoutContainers ?? [];
  }

  get cols() {
    if (this.args.cols) {
      return this.args.cols;
    }

    return this.args.page?.cols ?? [];
  }

  @action
  async configContainer(index) {
    const container = this.containers[index];

    await this.args.onSelect?.({
      type: 'container',
      title: `container ${container.name}`,
      data: container,
    });

    return this.args.onConfigContainer?.(index, container);
  }
}
