/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { SpaceSearchOptions } from 'models/transforms/space-search-options';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageSearchOptionsComponent extends Component {
  options = [
    {
      id: true,
      name: 'yes',
    },
    {
      id: false,
      name: 'no',
    },
  ];

  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? new SpaceSearchOptions();
  }

  get pairs() {
    const list = [];

    for (const key of SpaceSearchOptions.fields) {
      list.push({
        key,
        value: this.args.value?.[key] ?? false,
      });
    }

    return list;
  }

  @action
  change(key, newValue) {
    let value = this.value;

    value[key] = newValue.id;

    this._value = value;
  }

  @action
  cancel() {
    this._value = null;

    return this.args.onCancel();
  }

  @action
  save() {
    return this.args.onSave?.(this.value);
  }
}
