/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AttributesGroup from '../groups/attributes';
import { tracked } from '@glimmer/tracking';

export default class AttributesContainer {
  intl;
  @tracked groups = [];

  constructor(intl) {
    this.intl = intl;
  }

  get feature() {
    return this._feature;
  }

  set feature(value) {
    this._feature = value;
    const currentGroups = this.createGroups();

    if (this.computeHash(currentGroups) != this.computeHash(this.groups)) {
      this.groups = currentGroups;
    }
  }

  computeHash(groups) {
    if (!groups) {
      return 'null';
    }

    return groups.map((a) => a.key + '.' + a.order + '.' + a.addButton).join(';');
  }

  get iconsByName() {
    const result = {};

    const icons = this.feature?.resolvedIcons ?? [];

    icons
      .filter((a) => a.attributes && a.attributes.length > 0)
      .forEach((icon) => {
        result[icon.name] = icon;
      });

    return result;
  }

  get pairs() {
    const pairs = [];

    if (!this.feature) {
      return pairs;
    }

    let createPair = (key) => {
      if (key == 'position details') {
        return;
      }

      const getDefaultValue = () => (this.iconsByName[key].allowMany ? [{}] : {});

      const item = this.feature.attributes[key] ?? getDefaultValue();
      const isArray = Array.isArray(item);

      if (isArray) {
        item.forEach((_, index) => {
          pairs.push({ key, index });
        });

        pairs.push({ key, index: 'new' });
      }

      if (!isArray && item === Object(item)) {
        pairs.push({
          key: key,
        });
      }
    };

    Object.keys(this.iconsByName).forEach((key) => {
      createPair(key);
    });

    return pairs;
  }

  createGroups() {
    const iconsByName = this.iconsByName;

    return this.pairs.map((item) => {
      const key = item.key;

      const attributeGroup = new AttributesGroup(this.intl);
      attributeGroup.key = key;
      attributeGroup.feature = this.feature;

      if (iconsByName) {
        attributeGroup.icon = iconsByName[key];
      }

      if (item.index === undefined) {
        attributeGroup.attributes = this.feature.attributes[key];
        attributeGroup.index = null;
        attributeGroup.indexed = false;
      } else if (item.index === 'new') {
        attributeGroup.addButton = true;
        attributeGroup.indexed = false;
      } else {
        attributeGroup.attributes = this.feature.attributes[key]?.[item.index] ?? {};
        attributeGroup.index = item.index;
        attributeGroup.indexed = true;
        attributeGroup.order = item.index + 1;
      }

      return attributeGroup;
    });
  }
}
