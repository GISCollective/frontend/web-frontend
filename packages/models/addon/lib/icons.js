/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export function addIconGroupToStore(group, store) {
  if (store.isDestroyed || store.isDestroying) {
    return;
  }

  if (Array.isArray(group['icons']) && group['icons'].length > 0) {
    group['icons'] = group['icons'].map((icon) => {
      return store.push(store.normalize('icon', icon));
    });
  }

  Object.keys(group['categories']).forEach((a) => addIconGroupToStore(group['categories'][a], store));
}

export function iconGroupToList(group) {
  const list = [];

  if (Array.isArray(group['icons'])) {
    for(let icon of group['icons']) {
      list.push(icon);
    }
  }

  for(let key of Object.keys(group['categories'])) {
    const groups = iconGroupToList(group['categories'][key]);

    for(let group of groups) {
      list.push(group);
    }
  }

  return list;
}
