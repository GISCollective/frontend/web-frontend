/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from './authenticated-route';
import { service } from '@ember/service';

export default class AuthenticatedAdminRoute extends AuthenticatedRoute {
  @service session;
  @service preferences;

  beforeModel() {
    if (!this.session.isAuthenticated) {
      return this.router.transitionTo('login.index');
    }

    return this.store.queryRecord('user', { me: true }).then((user) => {
      if (!user.isAdmin) {
        this.router.transitionTo('index');
      }
    });
  }
}
