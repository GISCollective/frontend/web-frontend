import DblClickDragZoom from 'ol/interaction/DblClickDragZoom';

export {
  default,
  DblClickDragZoom,
} from 'map/components/map/interaction/dbl-click-drag-zoom';
