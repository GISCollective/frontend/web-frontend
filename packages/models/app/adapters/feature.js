/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ApplicationAdapter from './application';

export default class FeatureAdapter extends ApplicationAdapter {
  get modelName() {
    return 'feature';
  }

  bulkPublish(query) {
    const url = this.urlForQuery(query, 'feature') + '/publish?' + this.getParams(query);
    return this.ajax(url, 'POST');
  }

  bulkUnpublish(query) {
    const url = this.urlForQuery(query, 'feature') + '/unpublish?' + this.getParams(query);
    return this.ajax(url, 'POST');
  }

  bulkSetPending(query) {
    const url = this.urlForQuery(query, 'feature') + '/pending?' + this.getParams(query);
    return this.ajax(url, 'POST');
  }

  bulkDelete(query) {
    const url = this.urlForQuery(query, 'feature') + '?' + this.getParams(query);
    return this.ajax(url, 'DELETE');
  }

  pendingMany(query) {
    const url = [this.urlForQuery(query, this.modelName) + '/pending', this.getParams(query)]
      .filter((a) => a)
      .join('?');

    return this.ajax(url, 'POST');
  }
}
