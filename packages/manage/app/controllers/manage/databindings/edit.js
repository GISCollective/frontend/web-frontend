/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { addIconGroupToStore } from '../../../lib/icons';

export default class ManageDataBindingsEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service store;
  @service router;

  @tracked editableField;
  @tracked savingField;
  @tracked allTeams = false;
  @tracked icons = [];

  queryParams = ['allTeams'];

  get updateFieldList() {
    return [
      { id: '', name: this.intl.t('do not update') },
      { id: '_id', name: '_id' },
      { id: 'name', name: this.intl.t('name') },
      { id: 'remoteId', name: this.intl.t('remoteId') },
    ];
  }

  get breadcrumbs_() {
    return [
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.dataBinding.visibility.teamId,
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        text: `Edit ${this.model.dataBinding.name}`,
      },
    ];
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        text: this.model.dataBinding.name,
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  @action
  async updateIcons() {
    if (!this.model.dataBinding?.destination?.modelId) {
      this.icons = [];
      return;
    }

    this.icons = await this.store.adapterFor('icon').group({ iconSetsMap: this.model.dataBinding.destination.modelId });

    addIconGroupToStore(this.icons, this.store);
  }

  @action
  async save(field, value) {
    if (field == 'updateBy') {
      value = value.id;
    }

    this.savingField = this.editableField;
    this.editableField = '';

    this.model.dataBinding.set(field, value);

    if (field == 'destination') {
      await this.updateIcons();
    }

    try {
      await this.model.dataBinding.save();
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.savingField = '';
  }

  @action
  async delete() {
    try {
      await this.model.dataBinding.destroyRecord();
      this.router.transitionTo('manage.dashboards.dashboard', this.model.dataBinding.visibility.teamId);
    } catch (err) {
      this.notifications.handleError(err);
    }
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.dataBinding.rollbackAttributes();
  }
}
