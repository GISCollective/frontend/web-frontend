/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import sanitizeHtml from 'sanitize-html';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class ArticleEditorComponent extends Component {
  @tracked _value;
  @tracked _blocks;
  @tracked isSaving;

  @service intl;

  get hasNoChanges() {
    return this._value ? false : true;
  }

  get value() {
    if (!this._value) {
      return this.args.value;
    }

    return { ...this._value, blocks: this.blocks };
  }

  get blocks() {
    if (this._blocks) {
      return this._blocks;
    }

    return this.value?.blocks ?? [];
  }

  get title() {
    return this.blocks.find((a) => a.type == 'header' && a.data.level == 1 && a.data.text.trim() != '');
  }

  get cleanTitle() {
    let title = '';

    if (this.title?.data?.text) {
      title = this.title.data.text;
    }

    const value = sanitizeHtml(title, {
      allowedTags: [],
      allowedAttributes: {},
    });

    return htmlDecode(value);
  }

  get hasMissingTitle() {
    if (this.args.allowNoHeading) {
      return false;
    }

    if (!this.value?.blocks) {
      return true;
    }

    return !this.title;
  }

  get hasNoParagraph() {
    if (this.args.allowNoParagraph) {
      return false;
    }

    if (!this.value?.blocks) {
      return true;
    }

    const paragraph = this.blocks.find((a) => a.type == 'paragraph');

    if (!paragraph) {
      return true;
    }

    return false;
  }

  get isChanged() {
    return this._value !== null || this._value !== undefined;
  }

  getKey(blocks) {
    if (!blocks) {
      return '';
    }

    const hashCode = (s) => s.split('').reduce((a, b) => ((a << 5) - a + b.charCodeAt(0)) | 0, 0);

    let result = '';

    for (const block of blocks) {
      result += `${block.type}${JSON.stringify(block.data)}`;
    }

    return hashCode(result);
  }

  @action
  change(value) {
    if (this.editor.ignoreChange) {
      return;
    }

    const currentBlocksKey = this.getKey(this.blocks);
    const newBlocksKey = this.getKey(value.blocks);

    if (currentBlocksKey == newBlocksKey) {
      return;
    }

    this._value = value;
    this._blocks = value.blocks.map((a) => ({
      type: a.type,
      data: a.data,
    }));

    this.args.onChange?.(this.cleanTitle, { blocks: this.blocks });
  }

  @action
  ready(editor) {
    this.editor = editor;

    if (this.shouldReset) {
      return this.reset();
    }
  }

  @action
  setTitlePlaceholder() {
    this.editor?.blocks?.insert(
      'header',
      {
        level: 1,
        text: this.intl.t('new title'),
      },
      undefined,
      0,
      true,
    );
  }

  @action
  setParagraphPlaceholder() {
    let pos = 0;

    if (this.blocks.length) {
      pos = this.blocks.length;
    }

    this.editor?.blocks?.insert(
      'paragraph',
      {
        text: this.intl.t('new paragraph'),
      },
      undefined,
      pos,
      true,
    );
  }

  @action
  async reset() {
    if (!this.editor) {
      this.shouldReset = true;
      return;
    }

    if (this._value == this.args.value) {
      return;
    }

    const currentBlocksKey = this.getKey(this._value?.blocks);
    const newBlocksKey = this.getKey(this.args.value?.blocks);

    if (currentBlocksKey === newBlocksKey) {
      return;
    }

    this.editor.ignoreChange = true;
    const blocks = this.args.value?.blocks ?? [];

    try {
      await this.editor.blocks.render({ blocks });
    } catch (err) {
      console.log(err);
    }

    if (this.isDestroying) {
      return;
    }

    this._value = null;
    this._blocks = null;

    later(() => {
      this.editor.ignoreChange = false;
    });
  }
}

function htmlDecode(input) {
  var e = document.createElement('textarea');
  e.innerHTML = input;
  // handle case of empty input
  return e.childNodes.length === 0 ? '' : e.childNodes[0].nodeValue.trim();
}
