import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | input/text-file', function (hooks) {
  setupRenderingTest(hooks);

  it('renders an input file', async function (assert) {
    await render(hbs`<Input::TextFile @accept="text/plain, text/csv" />`);

    expect(this.element.querySelector('input')).to.have.attribute('type', 'file');
    expect(this.element.querySelector('input')).to.have.attribute('accept', 'text/plain, text/csv');
  });

  it('triggers on change with the file content when one is selected', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::TextFile @onChange={{this.change}} @accept="text/plain, text/csv" />`);

    let utf8Encode = new TextEncoder();
    let byteArray = utf8Encode.encode('a@b.c');
    let blob = new Blob([byteArray], { type: 'text/plain' });
    blob.name = 'test.txt';

    await triggerEvent('input', 'change', { files: [blob] });

    expect(value).to.equal('a@b.c');
  });
});
