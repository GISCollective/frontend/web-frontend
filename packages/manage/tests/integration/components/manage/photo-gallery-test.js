/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, triggerEvent, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/photo-gallery', function (hooks) {
  let server;
  setupRenderingTest(hooks);

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('can add a new picture while in edit mode', async function () {
    this.set('value', []);

    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::PhotoGallery @value={{this.value}} @onSave={{this.save}}  @title="photos" @editablePanel="photos" />`,
    );

    const blob = server.testData.create.pngBlob();

    await triggerEvent("[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    await waitUntil(() => value.length);

    expect(value).to.exist;
    expect(value).to.have.length(1);
    expect(value[0].picture).to.startWith(
      'data:image/png;base64,wolQTkcNChoKAAAADUlIRFIAAABkAAAAZAgGAAAAcMOiwpVUAAAACXBIWXMAAAsSAAALE',
    );
  });

  it('shows all pictures when not in edit mode', async function () {
    this.set('value', [{ md: 'data:image/png;base64,picture1' }, { md: 'data:image/png;base64,picture2' }]);

    await render(
      hbs`<Manage::PhotoGallery @value={{this.value}} @onSave={{this.save}} @title="other" @editablePanel="photos" />`,
    );

    expect(this.element.querySelector('.input-photo-gallery')).not.to.exist;
    const images = this.element.querySelectorAll('img');

    expect(images).to.have.length(2);
    expect(images[0]).to.have.attribute('src', 'data:image/png;base64,picture1');
    expect(images[1]).to.have.attribute('src', 'data:image/png;base64,picture2');
  });
});
