import { module, test } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | input/time-tracking', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a value', async function (assert) {
    this.set('value', {
      hours: 1,
      date: new Date('2024-03-12T02:22:33Z'),
      details: 'some details',
    });

    await render(hbs`<Input::TimeTracking @value={{this.value}} />`);

    expect(this.element.querySelector('.value-hours').value).to.equal('1');
    expect(this.element.querySelector('.datepicker-input').value).to.equal('2024-03-12');
    expect(this.element.querySelector('.form-select.hour').value).to.equal('3');
    expect(this.element.querySelector('.form-select.minute').value).to.equal('22');
    expect(this.element.querySelector('.value-details').value).to.equal('some details');
  });

  it('can change the value', async function (assert) {
    this.set('value', {
      hours: 1,
      date: new Date('2024-03-12T02:22:33Z'),
      details: 'some details',
    });

    let value;

    this.set('change', (v) => {
      value = v;
      value.date = value.date?.toISOString?.();
    });

    await render(hbs`<Input::TimeTracking @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('.value-hours', '22');
    await fillIn('.value-details', 'new details');

    this.element.querySelector('.form-select.hour').value = '9';
    await triggerEvent('.form-select.hour', 'change');

    expect(value).to.deep.equal({ hours: '22', date: '2024-03-12T08:22:00.000Z', details: 'new details' });
  });
});
