/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { sizedBsProperty, setSizedBsProperty } from 'core/lib/bs-cols';
import { action } from '@ember/object';

export default class ManageEditorsPropertiesBsOptionsComponent extends Component {
  elementId = `editor-property-${guidFor(this)}`;

  allOptions = [];

  get value() {
    return sizedBsProperty(this.args.property, this.args.size, this.args.options);
  }

  @action
  change(value) {
    const options = setSizedBsProperty(this.args.property, this.args.size, value, this.args.options);

    this.args.onChange?.(options);
  }
}
