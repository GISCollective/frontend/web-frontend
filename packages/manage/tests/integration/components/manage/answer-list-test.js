/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module, test } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/answer-list', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders an alert when there are no questions', async function (assert) {
      await render(hbs`<Manage::AnswerList @title="contributor" />`);

      expect(this.element.querySelector('.alert.alert-warning')).to.have.textContent(
        'The survey does not have contributor questions.',
      );
    });

    it('renders the question answers when the survey has some defined', async function (assert) {
      this.set('questions', [
        {
          question: 'email?',
          type: 'email',
          name: 'email',
          isRequired: true,
          isVisible: true,
        },
        {
          question: 'name?',
          type: 'short text',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ]);

      this.set('value', {
        email: 'a@b.c',
        name: 'some name',
      });

      await render(hbs`<Manage::AnswerList @title="contributor" @questions={{this.questions}} @value={{this.value}}/>`);

      expect(this.element.querySelector('.alert.alert-warning')).not.to.exist;
      const items = [...this.element.querySelectorAll('.item')];

      expect(items).to.have.length(2);

      expect(items[0].querySelector('.question')).to.have.textContent('email?');
      expect(items[0].querySelector('.value')).to.have.textContent('a@b.c');

      expect(items[1].querySelector('.question')).to.have.textContent('name?');
      expect(items[1].querySelector('.value')).to.have.textContent('some name');
    });

    it('renders a stream flow value', async function (assert) {
      this.set('questions', [
        {
          question: 'stream flow',
          type: 'stream flow',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ]);

      this.set('value', {
        name: {
          type: "stream discharge",
          data: {
            velocity: 2,
            width: 3,
            depth: 4,
            result: 24
          }
        },
      });

      await render(hbs`<Manage::AnswerList @title="contributor" @questions={{this.questions}} @value={{this.value}}/>`);

      expect(this.element.querySelector(".item .value")).to.have.textContent("velocity: 2 width: 3 depth: 4 result: 24");
    });
  });

  describe('in edit mode', function (hooks) {
    hooks.beforeEach(function () {
      this.set('questions', [
        {
          question: 'email?',
          type: 'email',
          name: 'email',
          isRequired: true,
          isVisible: true,
        },
        {
          question: 'name?',
          type: 'short text',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ]);

      this.set('value', {
        email: 'a@b.c',
        name: 'some name',
      });
    });

    it('renders the values', async function () {
      await render(
        hbs`<Manage::AnswerList @title="contributor" @editablePanel="contributor" @questions={{this.questions}} @value={{this.value}}/>`,
      );

      const items = [...this.element.querySelectorAll('input')];

      expect(items[0].value).to.equal('a@b.c');
      expect(items[1].value).to.equal('some name');
    });

    it('can update the values', async function () {
      let value;

      this.set('save', (v) => {
        value = v;
      });

      await render(hbs`<Manage::AnswerList
        @title="contributor"
        @editablePanel="contributor"
        @questions={{this.questions}}
        @onSave={{this.save}}
        @value={{this.value}}/>`);

      let items = [...this.element.querySelectorAll('input')];
      await fillIn(items[0], 'b@c.d');

      items = [...this.element.querySelectorAll('input')];
      await fillIn(items[1], 'new name');

      await click('.btn-submit');

      expect(value).to.deep.equal({ email: 'b@c.d', name: 'new name' });
    });
  });
});
