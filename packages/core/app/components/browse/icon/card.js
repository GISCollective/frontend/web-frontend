/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class BrowseIconCardComponent extends Component {
  get size() {
    if (this.args.icon?.image?.value?.indexOf?.('.jpg') > 0) {
      return '.md.jpg';
    }

    return '/md';
  }
}
