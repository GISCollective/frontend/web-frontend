/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, fillIn, triggerEvent, waitFor } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | /manage/surveys/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedCampaign;
  let team;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');

    receivedCampaign = null;
  });

  hooks.afterEach(function () {
    return server.waitAllRequests();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/surveys/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fsurveys%2Fadd');
  });

  describe('when an user is authenticated and no team is available', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/surveys/add`);
      expect(currentURL()).to.equal('/manage/surveys/add');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal('New survey');

      expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to.exist;

      expect(this.element.querySelector('.row-team label').textContent.trim()).to.contain('Team');
      expect(this.element.querySelector('.row-team select')).to.not.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to.exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit(`/manage/surveys/add`);
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.campaign');
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit(`/manage/surveys/add`);
      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.campaign');
    });
  });

  describe('when an user is authenticated and a team is available', function (hooks) {
    hooks.beforeEach(function () {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser();
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit(`/manage/surveys/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'New survey']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'New survey']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
      ]);
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/surveys/add`);
      expect(currentURL()).to.equal('/manage/surveys/add');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal('New survey');

      expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to.exist;

      expect(this.element.querySelector('.row-team label').textContent.trim()).to.contain('Team');
      expect(this.element.querySelector('.row-team select')).to.exist;
      expect(this.element.querySelectorAll('.row-team  select option').length).to.equal(2);

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.have.attribute('disabled', '');
    });

    describe('when the server request is successful', function (hooks) {
      let campaign;

      hooks.beforeEach(function () {
        team = server.testData.storage.addDefaultTeam();
        campaign = server.testData.storage.addDefaultCampaign();
        server.testData.storage.addDefaultTeam('000000000000000000000001');

        server.post('/mock-server/campaigns', (request) => {
          receivedCampaign = JSON.parse(request.requestBody);
          receivedCampaign.campaign._id = campaign._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedCampaign)];
        });
      });

      it('redirects to the edit page after the request response is received', async function () {
        authenticateSession();

        await visit(`/manage/surveys/add`);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.row-name input.form-control', 'new survey name');
        await fillIn('.ce-paragraph', 'test article');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedCampaign.campaign.name).to.equal('new survey name');
        expect(receivedCampaign.campaign.article.blocks).to.deep.equal([
          {
            type: 'header',
            data: {
              text: 'new survey name',
              level: 1,
            },
          },
          {
            type: 'paragraph',
            data: {
              text: 'test article',
            },
          },
        ]);
        expect(receivedCampaign.campaign.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        });

        expect(currentURL()).to.equal(`/manage/surveys/edit/${campaign._id}`);
      });

      it('successfully adds a campaign without a description', async function () {
        authenticateSession();

        await visit(`/manage/surveys/add`);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.row-name input.form-control', 'new survey name');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedCampaign.campaign.name).to.equal('new survey name');
        expect(receivedCampaign.campaign.article.blocks).to.deep.equal([
          {
            type: 'header',
            data: {
              text: 'new survey name',
              level: 1,
            },
          },
        ]);
        expect(receivedCampaign.campaign.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        });

        expect(currentURL()).to.equal(`/manage/surveys/edit/${campaign._id}`);
      });
    });

    describe('when the server request fails', function (hooks) {
      hooks.beforeEach(function () {
        server.post('/mock-server/campaigns', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description:
                    'The `Campaign.article` field is required: {"info":{"createdOn":"2020-06-08T10:46:16Z","changeIndex":0,"lastChangeOn":"2020-06-08T10:46:16Z","originalAuthor":"5b870669796da25424540deb","author":"5b870669796da25424540deb"},"maps":["5ca89e2fef1f7e010007f50a"],"contributors":["5b870669796da25424540deb"],"name":"dsgf","isPublished":false,"position":{"type":"Point","coordinates":[5.189189189189191,29.98833891386687]}}',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function (a) {
        const service = this.owner.lookup('service:notifications');

        authenticateSession();

        await visit(`/manage/surveys/add`);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.row-name input.form-control', 'new survey name');
        await fillIn('.ce-paragraph', 'test article');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/manage/surveys/add`);

        expect(service.lastCall[0]).to.equal('handleError');
        expect(service.lastCall[1].message).to.contain('Ember Data Request POST /mock-server/campaigns returned a 400');
      });
    });
  });

  describe('when an admin user is authenticated', function (hooks) {
    hooks.beforeEach(() => {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser(true);
      server.testData.storage.addDefaultTeam('000000000000000000000001');
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/surveys/add`);
      expect(currentURL()).to.equal('/manage/surveys/add');
    });

    describe('when the server request is successfully', function (hooks) {
      let campaign;

      hooks.beforeEach(function () {
        campaign = server.testData.storage.addDefaultCampaign();
        server.post('/mock-server/campaigns', (request) => {
          receivedCampaign = JSON.parse(request.requestBody);
          receivedCampaign.campaign._id = campaign._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedCampaign)];
        });
      });

      it('queries the teams without the all query param', async function () {
        authenticateSession();

        await visit(`/manage/surveys/add`);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.row-name input.form-control', 'new survey name');
        await fillIn('.ce-paragraph', 'test article');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedCampaign.campaign.name).to.deep.equal('new survey name');
        expect(currentURL()).to.equal(`/manage/surveys/edit/${campaign._id}`);
      });

      it('queries the teams with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/manage/surveys/add`);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain('GET /mock-server/teams?all=true&edit=true');
      });
    });
  });
});
