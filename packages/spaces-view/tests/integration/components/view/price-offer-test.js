/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | view/price-offer', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::PriceOffer />`);
    assert.dom(this.element).hasText('');
  });

  it('renders all fields when set', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            currency: 'usd',
            value: '12',
            details: 'some details',
          },
        ],
        title: {
          text: 'some title',
          classes: ['text-center'],
          color: 'green',
        },
        subtitle: {
          text: 'some subtitle',
          classes: ['text-center'],
          color: 'blue',
        },
        price: {
          classes: ['text-center'],
          color: 'red',
        },
        priceDetails: {
          classes: ['text-center'],
          color: 'red',
        },
        button: {
          name: 'first button',
          link: 'https://giscollective.com/page1',
          newTab: false,
          classes: ['btn-primary', 'size-btn-lg'],
          type: 'default',
          storeType: 'apple',
        },
        container: {
          verticalSize: 'fill',
          verticalAlign: 'top',
          color: 'blue-500',
          classes: ['ps-3'],
        },
      },
    });

    await render(hbs`<View::PriceOffer @value={{this.value}} />`);

    expect(
      this.element.querySelector('.offer-title').textContent.trim(),
    ).to.equal('some title');
    expect(this.element.querySelector('.offer-title')).to.have.classes(
      'text-with-options text-center text-green offer-title',
    );

    expect(
      this.element.querySelector('.offer-subtitle').textContent.trim(),
    ).to.equal('some subtitle');
    expect(this.element.querySelector('.offer-subtitle')).to.have.classes(
      'text-with-options text-center text-blue offer-subtitle',
    );

    expect(
      this.element.querySelector('.offer-price').textContent.trim(),
    ).to.equal('$12');
    expect(this.element.querySelector('.offer-price')).to.have.classes(
      'text-with-options text-center text-red offer-price me-2',
    );

    expect(
      this.element.querySelector('.offer-button').textContent.trim(),
    ).to.equal('first button');
    expect(this.element.querySelector('.offer-button')).to.have.classes(
      'btn btn-primary size-btn-lg offer-button',
    );
  });

  it('only renders the price details when the price is not set', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            currency: 'eur',
            value: '',
            details: 'some details',
          },
        ],
        price: {
          classes: ['text-center'],
          color: 'red',
        },
        priceDetails: {
          classes: ['text-center'],
          color: 'red',
        },
      },
    });

    await render(hbs`<View::PriceOffer @value={{this.value}} />`);

    expect(this.element.querySelector('.offer-price')).not.to.exist;
    expect(
      this.element.querySelector('.offer-price-details').textContent.trim(),
    ).to.equal('some details');
  });

  it('renders an eur price', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            currency: 'eur',
            value: '12',
          },
        ],
        price: {
          classes: ['text-center'],
          color: 'red',
        },
      },
    });

    await render(hbs`<View::PriceOffer @value={{this.value}} />`);
    expect(
      this.element.querySelector('.offer-price').textContent.trim(),
    ).to.equal('12€');
  });

  it('renders an gpb price', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            currency: 'gbp',
            value: '12',
          },
        ],
        price: {
          classes: ['text-center'],
          color: 'red',
        },
      },
    });

    await render(hbs`<View::PriceOffer @value={{this.value}} />`);
    expect(
      this.element.querySelector('.offer-price').textContent.trim(),
    ).to.equal('£12');
  });

  it('renders an RON price', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            currency: 'RON',
            value: '12',
          },
        ],
        price: {
          classes: ['text-center'],
          color: 'red',
        },
      },
    });

    await render(hbs`<View::PriceOffer @value={{this.value}} />`);
    expect(
      this.element.querySelector('.offer-price').textContent.trim(),
    ).to.equal('12RON');
  });

  it('renders a button', async function (assert) {
    this.set('value', {
      data: {
        button: {
          link: {
            url: '/learn-more',
          },
          classes: [],
          name: 'Learn more',
        },
      },
    });

    this.set('space', {
      getCachedPagesMap: () => ({}),
    });

    await render(
      hbs`<View::PriceOffer @value={{this.value}} @space={{this.space}}/>`,
    );

    expect(
      this.element.querySelector('.offer-button').textContent.trim(),
    ).to.equal('Learn more');
  });

  it('applies the min lines height values', async function (assert) {
    this.set('value', {
      data: {
        subtitle: {
          text: 'subtitle',
          minLines: {
            sm: 1,
            md: 2,
            lg: 3,
          },
        },
      },
    });

    this.set('space', {
      getCachedPagesMap: () => ({}),
    });

    await render(
      hbs`<View::PriceOffer @deviceSize="lg" style="line-height: 14px" @value={{this.value}} @space={{this.space}}/>`,
    );

    const style = getComputedStyle(
      this.element.querySelector('.text-with-options'),
    );

    expect(style.minHeight).to.equal('42px');
  });

  it('inherits the height from other price offers when the price is not set', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            details: 'priceDetails',
          },
        ],
      },
    });

    this.set('space', {
      getCachedPagesMap: () => ({}),
    });

    await render(hbs`<div>
      <div class="price-offer">
        <div class="price-row">
          <div class="offer-price" style="height: 40px;">value</div>
        </div>
      </div>

      <View::PriceOffer class="tested-col" @deviceSize="lg" style="line-height: 14px" @value={{this.value}} @space={{this.space}}/>
    </div>`);

    const style = getComputedStyle(
      this.element.querySelector('.tested-col .price-row'),
    );

    /// it should be 40 but ember test applies a transformation on the view
    expect(style.minHeight).to.equal('20px');
  });

  it('renders the price based on the state currency', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            value: '1',
            currency: 'eur',
          },
          {
            value: '2',
            currency: 'usd',
          },
        ],
      },
    });

    this.set('space', {
      getCachedPagesMap: () => ({}),
    });

    this.set('state', `cu@usd`);

    await render(
      hbs`<View::PriceOffer @state={{this.state}} @value={{this.value}} @space={{this.space}}/>`,
    );

    expect(
      this.element.querySelector('.offer-price').textContent.trim(),
    ).to.equal('$2');
  });

  it('sets the justify-content classes when the text align class is set', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            value: '1',
            currency: 'eur',
          },
        ],
        price: {
          classes: [
            'text-center',
            'text-md-center',
            'text-lg-center',
            'text-size-22',
            'ff-crimson-pro',
          ],
        },
      },
    });

    this.set('space', {
      getCachedPagesMap: () => ({}),
    });

    await render(
      hbs`<View::PriceOffer @state={{this.state}} @value={{this.value}} @space={{this.space}}/>`,
    );

    expect(this.element.querySelector('.price-row')).to.have.classes(
      'd-flex align-items-center price-row justify-content-center justify-content-md-center justify-content-lg-center',
    );
  });

  it('sets the price alignment to the button container', async function (assert) {
    this.set('value', {
      data: {
        priceList: [
          {
            value: '1',
            currency: 'eur',
          },
        ],
        price: {
          classes: [
            'text-center',
            'text-md-center',
            'text-lg-center',
            'text-size-22',
            'ff-crimson-pro',
          ],
        },
        button: {
          name: 'first button',
          link: 'https://giscollective.com/page1',
          newTab: false,
          classes: ['btn-primary', 'size-btn-lg'],
          type: 'default',
          storeType: 'apple',
        },
      },
    });

    this.set('space', {
      getCachedPagesMap: () => ({}),
    });

    await render(
      hbs`<View::PriceOffer @state={{this.state}} @value={{this.value}} @space={{this.space}}/>`,
    );

    expect(this.element.querySelector('.button-container')).to.have.classes(
      'button-container text-center text-md-center text-lg-center',
    );
  });
});
