/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from './page';
import { service } from '@ember/service';

export default class IndexRoute extends Route {
  templateName = 'page';

  @service intl;
  @service router;
  @service session;
  @service space;

  beforeModel() {
    let params = this.intl.primaryLocale == 'en-us' ? '' : `?locale=${this.intl.primaryLocale}`;

    if(!this.space.summary?.isPublic && !this.space.currentSpace && this.router.currentURL?.indexOf("/login") != 0) {
      this.router.transitionTo('login.index');
      return;
    }

    if (this.space.landingPagePath != '/') {
      this.router.transitionTo(`${this.space.landingPagePath}${params}`);
      return;
    }

    return super.beforeModel();
  }
}
