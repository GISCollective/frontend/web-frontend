/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render, triggerEvent, waitFor, waitUntil } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { Selection } from "dummy/components/manage/property-panel";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | manage/property-panel", function (hooks) {
  setupRenderingTest(hooks);
  let pageData;
  let spaceData;
  let page;
  let space;
  let store;
  let server;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup("service:store");
    pageData = server.testData.storage.addDefaultPage();
    spaceData = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture("5cc8dc1038e882010061545a");
    spaceData.cols = {};

    page = await store.findRecord("page", pageData._id);
    space = await store.findRecord("space", spaceData._id);

    server.put(`/mock-server/pictures/:id`, (request) => {
      const picture = JSON.parse(request.requestBody);
      picture.picture._id = request.params.id;

      return [200, { "Content-Type": "application/json" }, JSON.stringify(picture)];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders nothing when there is no selection", async function () {
    await render(hbs`<Manage::PropertyPanel />`);

    expect(this.element.textContent.trim()).to.equal("");
  });

  it("can create a global component (legacy)", async function (a) {
    const col = page.upsertPageColByName("0.0.1");

    this.set(
      "selection",
      new Selection({
        type: "col",
        title: "0.0.3",
        data: col,
      }),
    );

    this.set("model", {
      page,
      space,
    });

    let value
    this.set("spaceChange", (v) => {
      value = v;
    })

    await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}} @onSpaceChange={{this.spaceChange}}/>`);

    await selectSearch(".ember-power-select-trigger", "article title");
    await selectChoose(".ember-power-select-trigger", "article title");

    await click(".btn-to-global-page-col");

    await fillIn(".input-global-page-col-id", "new-name");

    await click(".btn-submit");

    a.deepEqual(space.cols.map["new-name"].toJSON(), {
      col: 0,
      container: 0,
      data: {
        source: {
          model: "picture",
          id: "5cc8dc1038e882010061545a",
        },
      },
      name: "0.0.0",
      row: 0,
      type: "article/title",
    });
    a.deepEqual(value, space);
  });


  it("can create a global component", async function (a) {
    page.upsertPageColByName("0.0.1");

    const selection = {
      isHighlight: true,
      editor: "slot",
      path: "page.cols.1"
    };

    this.set(
      "selection",
      selection
    );

    this.set("model", {
      page,
      space,
    });

    let value
    this.set("spaceChange", (v) => {
      value = v;
    })

    await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}} @onSpaceChange={{this.spaceChange}}/>`);

    await selectSearch(".ember-power-select-trigger", "article title");
    await selectChoose(".ember-power-select-trigger", "article title");

    await click(".btn-to-global-page-col");

    await fillIn(".input-global-page-col-id", "new-name");

    await click(".btn-submit");

    a.deepEqual(space.cols.map["new-name"].toJSON(), {
      col: 0,
      container: 0,
      data: {
        source: {
          model: "picture",
          id: "5cc8dc1038e882010061545a",
        },
      },
      name: "0.0.0",
      row: 0,
      type: "article/title",
    });

    a.deepEqual(value, space);
  });

  it("can set a global component", async function (a) {
    const col = page.upsertPageColByName("0.0.1");
    space.setPageCol("test-global-component", "heading", {
      style: {
        color: "green",
        classes: ["fw-bold", "display-2"],
        text: "some title",
      },
    });

    this.set(
      "selection",
      new Selection({
        type: "col",
        title: "0.0.3",
        data: col,
      }),
    );

    this.set("model", {
      page,
      space,
    });

    await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>`);

    await selectSearch(".ember-power-select-trigger", "test global component");
    await selectChoose(".ember-power-select-trigger", "test global component");

    a.deepEqual(space.cols.map["test-global-component"].toJSON(), {
      col: 0,
      container: 0,
      data: {
        style: {
          classes: ["fw-bold", "display-2"],
          color: "green",
          text: "some title",
        },
      },
      name: "0.0.0",
      row: 0,
      type: "heading",
    });

    a.deepEqual(page.cols[1].toJSON(), {
      col: 1,
      container: 0,
      data: {
        style: {
          classes: ["fw-bold", "display-2"],
          color: "green",
          text: "some title",
        },
      },
      gid: "test-global-component",
      name: "0.0.1",
      row: 0,
      type: "heading",
    });
  });

  it("can change the type of a global component", async function (a) {
    space.cols.setPageCol("new-name", "heading", {
      style: {
        classes: [],
        color: "",
        heading: 1,
        text: "some test",
      },
    });

    space.cols.map["new-name"].gid = "new-name";
    space.cols.map["new-name"].name = "0.0.0";

    page.cols = [space.cols.map["new-name"]];

    const col = page.upsertPageColByName("0.0.0");

    this.set(
      "selection",
      new Selection({
        type: "col",
        title: "0.0.0",
        data: col,
      }),
    );

    this.set("model", {
      page,
      space,
    });

    await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>`);

    expect(this.element.querySelector(".ember-power-select-trigger").textContent.trim()).to.equal("new name");

    await click(".btn-change-component");

    await selectSearch(".component-selector-change-global .ember-power-select-trigger", "picture");
    await selectChoose(".component-selector-change-global .ember-power-select-trigger", "picture");

    await waitUntil(() => space.cols.map["new-name"].type == "picture");

    a.deepEqual(space.cols.map["new-name"].toJSON(), {
      col: 0,
      container: 0,
      data: {
        style: {
          classes: [],
          color: "",
          heading: 1,
          text: "some test",
        },
      },
      gid: "new-name",
      name: "0.0.0",
      row: 0,
      type: "picture",
    });
  });

  it("can change the container values for a page col", async function (a) {
    const col = page.upsertPageColByName("0.0.0");
    this.set("selection", {
      type: "col",
      title: "0.0.0",
      data: col,
      groups: [
        {
          editor: "base/container",
          property: "container",
        },
      ],
    });

    this.set("model", {
      page,
      space,
    });

    await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>`);

    expect(this.element.querySelector(".input-options-frame .header").textContent.trim()).to.equal("container");

    await click(".bs-color-picker");
    await click(".btn-color-danger");

    expect(col.gid).to.be.undefined;
    expect(col.data).to.deep.equal({
      source: { id: "5ca78e2160780601008f69e6", model: "article" },
      container: { color: "danger" },
    });
  });

  it("can change the container values for a global page col", async function (a) {
    let col = page.upsertPageColByName("0.0.0");
    col.gid = "main-title";

    this.set("selection", {
      type: "col",
      title: "0.0.0",
      data: col,
      groups: [
        {
          editor: "base/container",
          property: "container",
        },
      ],
    });

    this.set("model", {
      page,
      space,
    });

    await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>`);

    expect(this.element.querySelector(".input-options-frame .header").textContent.trim()).to.equal("container");

    await click(".bs-color-picker");
    await click(".btn-color-danger");

    expect(col.gid).to.equal("main-title");
    expect(col.data).to.deep.equal({
      source: { id: "5ca78e2160780601008f69e6", model: "article" },
      container: { color: "danger" },
    });

    expect(space?.cols?.map["main-title"].data).to.deep.equal({
      source: { id: "5ca78e2160780601008f69e6", model: "article" },
      container: { color: "danger" },
    });
  });

  describe("using a highlight selection", function () {
    it("can change the container values", async function (a) {
      this.set("selection", {
        name: "other name",
        type: "some type",
        editor: "container-options",
        path: "page.layoutContainers.0",
        gid: "",
        isHighlight: true,
      });

      this.set("model", {
        page,
        space,
      });

      await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>`);

      this.element.querySelector(".width-select").value = "fluid";
      await triggerEvent(".width-select", "change");

      const container = page.layoutContainers[0].toJSON();

      expect(container.options).to.deep.equal(["container-fluid"]);
      expect(container.rows).to.have.length(1);
    });

    it("can change the row values", async function (a) {
      this.set("selection", {
        name: "0.0",
        type: "row",
        path: "page.layoutContainers.0.rows.0.data.container",
        editor: "base/container",
        isHighlight: true,
      });

      this.set("model", {
        page,
        space,
      });

      await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>`);

      this.element.querySelector(".input-sm-border-visibility").value = "yes";
      await triggerEvent(".input-sm-border-visibility", "change");

      expect(page.shouldSave).to.equal(true);

      const container = page.layoutContainers[0].toJSON();
      expect(container.rows[0].data).to.deep.equal({ container: { classes: ["border-visibility-yes"] } });
    });

    it("triggers an event to the referenced element", async function (a) {
      this.set("model", {
        page,
        space,
      });

      let newProps;
      this.set("updateProps", (v) => {
        newProps = v.detail.value;
      });

      await render(hbs`
        <div class="element" {{on "props-update" this.updateProps}}>ELEMENT</div>
        {{#if this.selection}}
          <Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>
        {{/if}}
      `);

      this.set("selection", {
        name: "0.0",
        type: "row",
        path: "layoutContainers.0.rows.0.data.container",
        editor: "base/container",
        isHighlight: true,
        element: this.element.querySelector(".element"),
      });

      await waitFor(".input-sm-border-visibility", { timeout: 3000 });

      this.element.querySelector(".input-sm-border-visibility").value = "yes";
      await triggerEvent(".input-sm-border-visibility", "change");

      a.deepEqual(newProps, {
        classes: ["border-visibility-yes"],
      });
    });
  });

  it("can change the category button style on an icon with categories editor", async function () {
    this.set("selection", {
      name: "0.0",
      type: "row",
      path: "page.cols.0.data.subcategories.button",
      editor: "button-style",
      isHighlight: true,
    });

    this.set("model", {
      page,
      space,
    });

    await render(hbs`<Manage::PropertyPanel @selection={{this.selection}} @model={{this.model}}/>`);

    this.element.querySelector(".input-sm-btn").value = "primary";
    await triggerEvent(".input-sm-btn", "change");

    expect(page.cols[0].data).to.deep.equal({
      source: { id: "5ca78e2160780601008f69e6", model: "article" },
      subcategories: { button: { classes: ["btn-primary"] } },
    });
  });
});
