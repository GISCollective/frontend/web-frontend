/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/campaign-answer/list-item', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let server;
  let answer;
  let user;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    user = server.testData.create.user('5b8a59caef739394031a3f67');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    const profile = server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultUser(false, user._id);

    server.testData.storage.addDefaultCampaign();

    let icon = server.testData.storage.addDefaultIcon();

    answer = server.testData.storage.addDefaultCampaignAnswer();
    answer.icons = [icon._id];
    answer.info.originalAuthor = profile._id;

    let now = new Date();
    now.setSeconds(now.getSeconds() - 1);
    answer.info.createdOn = now.toISOString();

    server.testData.storage.addDefaultUser(answer.info.originalAuthor);

    const answerModel = await this.store.findRecord('campaign-answer', answer._id);
    await answerModel.info.fetch();

    this.set('answer', answerModel);
  });

  it('renders a survey name with link', async function () {
    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);

    expect(this.element.querySelector('.answer-name').textContent.trim()).to.equal('some test');
    expect(this.element.querySelector('.answer-name')).to.have.attribute(
      'href',
      '/manage/survey-answers/5ca78aa160780601008f6bbb',
    );
  });

  it('renders a campaign time', async function () {
    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);
    expect(this.element.querySelector('.relative-time').textContent.trim()).to.contain('submitted');
    expect(this.element.querySelector('.relative-time').textContent.trim()).to.contain('1 second ago');
  });

  it('renders a campaign author when it is not anonymous', async function () {
    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);
    expect(this.element.querySelector('.author').textContent.trim()).to.contain('by');
    expect(this.element.querySelector('.author').textContent.trim()).to.contain('mr Bogdan Szabo');
  });

  it('does not render a campaign author when it is anonymous', async function () {
    this.answer.info.originalAuthor = '@anonymous';
    await this.answer.info.fetch();

    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);

    expect(this.element.querySelector('.author').textContent.trim()).to.contain('by an anonymous user');
  });

  it('shows a pending icon for pending answers', async function () {
    this.answer.status = 1;

    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);

    expect(this.element.querySelector('.answer-icon')).to.have.class('fa-clock');
  });

  it('shows a rejected icon for rejected answers', async function () {
    this.answer.status = 4;

    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);

    expect(this.element.querySelector('.answer-icon')).to.have.class('fa-circle-xmark');
  });

  it('shows a check icon for published answers', async function () {
    this.answer.status = 2;

    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);

    expect(this.element.querySelector('.answer-icon')).to.have.class('fa-circle-check');
  });

  it('shows a check icon for unpublished answers', async function () {
    this.answer.status = 3;

    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);

    expect(this.element.querySelector('.answer-icon')).to.have.class('fa-circle-check');
  });

  it('shows a circle icon for processing answers', async function () {
    this.answer.status = 0;

    await render(hbs`<Manage::CampaignAnswer::ListItem @value={{this.answer}}/>`);

    expect(this.element.querySelector('.answer-icon')).to.have.class('fa-circle');
  });
});
