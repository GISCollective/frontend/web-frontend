/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { service } from "@ember/service";
import PictureMeta from "models/lib/picture-meta";

export default class EditorImageListComponent extends Component {
  @service store;
  @tracked records;

  get linkName() {
    if (this.args.gid) {
      return "Space";
    }

    return "Page";
  }

  get linkId() {
    if (this.args.gid) {
      return this.args.model?.space?.id;
    }

    return this.args.model?.page?.id;
  }

  get ids() {
    return this.args.value?.ids ?? [];
  }

  fetchRecord(id) {
    let record = this.store.peekRecord("picture", id);

    if (record) {
      return record;
    }

    return this.store.findRecord("picture", id);
  }

  @action
  async setup() {
    this.records = await Promise.all(this.ids.map((id) => this.fetchRecord(id)));
  }

  @action
  createImage() {
    return this.store.createRecord("picture", {
      name: "",
      picture: "",
      meta: new PictureMeta({}),
    });
  }

  @action
  async update(value) {
    await Promise.all(value.map((a) => (a.isNew ? a.save() : a)));
    const ids = value.map((a) => a.id);

    this.records = value;

    return this.args.onChange({
      model: "picture",
      ids,
    });
  }
}
