/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | answer-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no question', async function (assert) {
    await render(hbs`<AnswerList />`);
    assert.dom(this.element).hasText('');
  });

  it('renders a question with a notice when it has no value', async function (assert) {
    this.set('questions', [
      {
        question: 'email?',
        type: 'email',
        name: 'email',
        isRequired: true,
        isVisible: true,
      },
    ]);

    this.set('value', {});

    await render(hbs`<AnswerList @value={{this.value}} @questions={{this.questions}} />`);

    const items = [...this.element.querySelectorAll('.item')];
    expect(items).to.have.length(1);

    expect(items[0].querySelector('.question')).to.have.textContent('email?');
    expect(items[0].querySelector('.value')).not.to.exist;
    expect(items[0].querySelector('.no-value')).to.have.textContent('No answer.');
  });

  it('renders a question with a value when set', async function (assert) {
    this.set('questions', [
      {
        question: 'email?',
        type: 'email',
        name: 'email',
        isRequired: true,
        isVisible: true,
      },
    ]);

    this.set('value', {
      email: 'a@b.c',
    });

    await render(hbs`<AnswerList @value={{this.value}} @questions={{this.questions}} />`);

    const items = [...this.element.querySelectorAll('.item')];
    expect(items).to.have.length(1);

    expect(items[0].querySelector('.question')).to.have.textContent('email?');
    expect(items[0].querySelector('.no-value')).not.to.exist;
    expect(items[0].querySelector('.value')).to.have.textContent('a@b.c');
  });

  it('renders a question with a block value when set', async function (assert) {
    this.set('questions', [
      {
        question: 'email?',
        type: 'long text',
        name: 'test',
        isRequired: true,
        isVisible: true,
      },
    ]);

    this.set('value', {
      test: {
        blocks: [
          {
            type: 'paragraph',
            data: {
              text: 'long',
            },
          },
        ],
      },
    });

    await render(hbs`<AnswerList @value={{this.value}} @questions={{this.questions}} />`);

    const items = [...this.element.querySelectorAll('.item')];
    expect(items).to.have.length(1);

    expect(items[0].querySelector('.question')).to.have.textContent('email?');
    expect(items[0].querySelector('.no-value')).not.to.exist;
    expect(items[0].querySelector('.blocks-value')).to.have.textContent('long');
  });

  it('renders two questions', async function (assert) {
    this.set('questions', [
      {
        question: 'email?',
        type: 'email',
        name: 'email',
        isRequired: true,
        isVisible: true,
      },
      {
        question: 'name?',
        type: 'short text',
        name: 'name',
        isRequired: true,
        isVisible: true,
      },
    ]);

    this.set('value', {
      email: 'a@b.c',
      name: 'name',
    });

    await render(hbs`<AnswerList @value={{this.value}} @questions={{this.questions}} />`);

    const items = [...this.element.querySelectorAll('.item')];
    expect(items).to.have.length(2);

    expect(items[0].querySelector('.question')).to.have.textContent('email?');
    expect(items[0].querySelector('.value')).to.have.textContent('a@b.c');

    expect(items[1].querySelector('.question')).to.have.textContent('name?');
    expect(items[1].querySelector('.value')).to.have.textContent('name');
  });

  it('does not render hidden questions', async function (assert) {
    this.set('questions', [
      {
        question: 'email?',
        type: 'email',
        name: 'email',
        isRequired: true,
        isVisible: false,
      },
      {
        question: 'name?',
        type: 'short text',
        name: 'name',
        isRequired: true,
        isVisible: true,
      },
    ]);

    this.set('value', {
      email: 'a@b.c',
      name: 'name',
    });

    await render(hbs`<AnswerList @value={{this.value}} @questions={{this.questions}} />`);

    const items = [...this.element.querySelectorAll('.item')];
    expect(items).to.have.length(1);

    expect(items[0].querySelector('.question')).to.have.textContent('name?');
    expect(items[0].querySelector('.value')).to.have.textContent('name');
  });
});
