/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { A } from 'models/lib/array';

export default class ListTransform extends Transform {
  deserialize(serialized) {
    return A(serialized);
  }

  serialize(deserialized) {
    return deserialized;
  }
}
