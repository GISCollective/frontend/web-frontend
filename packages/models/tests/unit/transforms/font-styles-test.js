/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';

import { setupTest, it } from 'dummy/tests/helpers';
import { expect } from 'chai';

module('Unit | Transform | font styles', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:font-styles');
  });

  // Replace this with your real tests.
  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function (a) {
    let deserialized = transform.deserialize(null);

    a.deepEqual(deserialized.toJSON(), {
      h1: [],
      h2: [],
      h3: [],
      h4: [],
      h5: [],
      h6: [],
      paragraph: [],
    });
  });

  it('can deserialize the style classes', function (a) {
    let deserialized = transform.deserialize({
      h1: ['a1'],
      h2: ['a2'],
      h3: ['a3'],
      h4: ['a4'],
      h5: ['a5'],
      h6: ['a6'],
      paragraph: ['a7'],
    });

    a.deepEqual(deserialized.toJSON(), {
      h1: ['a1'],
      h2: ['a2'],
      h3: ['a3'],
      h4: ['a4'],
      h5: ['a5'],
      h6: ['a6'],
      paragraph: ['a7'],
    });
  });
});
