/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | model-actions/map',  function (hooks) {
  setupRenderingTest(hooks, { server: true });

  hooks.beforeEach(async function() {
    const spaceService = this.owner.lookup('service:space');
    const spaceData = this.server.testData.storage.addDefaultSpace();

    const space = await this.store.findRecord("space", spaceData._id);
    spaceService.currentSpace = space;
  });

  it('renders', async function () {
    this.set('map', {
      id: 'id',
      canEdit: true,
    });
    await render(hbs`<ModelActions::Map @record={{this.map}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map(
      (element) => element.textContent.trim()
    );

    expect(labels).to.deep.equal([
      'View features',
      'Edit the map',
      'Map files',
      'Map sheet',
    ]);
  });

  it('renders the small version', async function () {
    this.set('map', {
      id: 'id',
      canEdit: true,
    });
    await render(
      hbs`<ModelActions::Map @isSmall={{true}} @record={{this.map}} />`
    );

    let labels = [];

    expect(this.element.querySelector('.dropdown-item')).not.to.exist;

    this.element.querySelectorAll('.btn').forEach((element) => {
      labels.push(element.textContent.trim());
    });

    expect(labels).to.deep.equal(['', '', '', '']);
  });
});
