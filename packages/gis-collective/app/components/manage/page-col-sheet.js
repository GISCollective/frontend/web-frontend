/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { Offcanvas } from 'bootstrap';
import { tracked } from '@glimmer/tracking';

export default class ManagePageColSheetComponent extends Component {
  @tracked isVisible;

  @tracked _value = null;
  @tracked newValue = null;
  @tracked newType = null;

  elementId = `editor-${guidFor(this)}`;

  get hasUpdate() {
    return this.newType !== null || this.newValue !== null;
  }

  get value() {
    const type = this.newType || this.args.type;
    let data = this.newValue || this._value;

    if ((data === undefined || data === null) && !this.args.value?.then) {
      data = this.args.value;
    }

    return {
      type,
      data,
    };
  }

  @action
  change(value, type) {
    this.newValue = value;
    this.newType = type;
  }

  @action
  close() {
    this.isVisible = false;
    this.offcanvas.hide();
  }

  @action
  async setup(element) {
    this.offcanvas = new Offcanvas(element);

    element.addEventListener('hidden.bs.offcanvas', () => {
      this.args.onHide?.();
    });

    this.setStates();
    this.updateValue();
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.offcanvas?.dispose();
  }

  @action
  async updateValue() {
    this._value = await this.args.value;
  }

  @action
  setStates() {
    if (this.args.show) {
      this.offcanvas.show();
      this.isVisible = true;
    }

    if (!this.args.show && this.isVisible) {
      this.close();
    }
  }

  @action
  async save() {
    await this.args.onChange(this.newValue, this.newType);
    this.close();
  }
}
