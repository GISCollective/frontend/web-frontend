/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { fromPageState } from '../../lib/page-state';
import { service } from '@ember/service';
import PositionDetails from '../../lib/positionDetails';

export default class AttributesGroupPositionFeatureComponent extends Component {
  @service store;
  @tracked feature;

  get state() {
    return fromPageState(this.args.state);
  }

  get featureId() {
    return this.state.fid;
  }

  get cardStyle() {
    return {
      data: {
        picture: { proportion: '3:1', classes: ['border-radius-2'] },
      },
    };
  }

  triggerChange() {
    const hasSameId = this.featureId == this.args.details?.id;

    if (this.args?.details?.type == 'feature' && hasSameId) {
      return;
    }

    let position = this.feature?.position;
    let details = new PositionDetails();
    details.type = 'feature';
    details.id = this.feature?.id;

    this.args.onChange?.(position, details);
  }

  @action
  async setup() {
    if (!this.featureId) {
      return;
    }

    let feature = this.store.peekRecord('feature', this.featureId);

    if (!feature) {
      feature = await this.store.findRecord('feature', this.featureId);
    }

    this.feature = feature;

    this.triggerChange();
  }
}
