/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action, set } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { onPictureUpload } from '../../../lib/editor-js-picture';

export default class ManageEventsEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service store;
  @service router;

  @tracked editableField;
  @tracked savingField;
  @tracked allTeams = false;
  @tracked _icons;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.calendar',
        model: this.model.calendar.get('id'),
        text: this.model.calendar.name,
      },
      {
        text: this.model.event.name,
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  get isSchedule() {
    return this.model.calendar.type == 'schedules';
  }

  @action
  onPictureUpload(value, type) {
    return onPictureUpload(value, type, this.store, { model: 'Event', modelId: this.model.event?.id }, false);
  }

  @action
  async saveField(field, value) {
    set(this.model.event, field, value);

    return this.save();
  }

  @action
  saveArticle(name, article) {
    this.model.event.name = name;
    this.model.event.article = article;

    return this.save();
  }

  @action
  async save() {
    this.savingField = this.editableField;

    let result;

    try {
      result = await this.model.event.save();
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.editableField = '';
    this.savingField = '';

    return result;
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.event.rollbackAttributes();
  }
}
