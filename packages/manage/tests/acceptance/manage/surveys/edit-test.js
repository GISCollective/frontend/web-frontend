/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  triggerEvent,
  fillIn,
  triggerKeyEvent,
  settled,
} from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';
import typeIn from '@ember/test-helpers/dom/type-in';

describe('Acceptance | manage/surveys/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let campaign;
  let receivedCampaign;
  let defaultPicture;
  let notificationCreated;
  let receivedPicture;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    const user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();
    campaign = server.testData.storage.addDefaultCampaign();
    server.testData.storage.addDefaultMap('5ca89e37ef1f7e010007f54c');

    receivedCampaign = null;

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    notificationCreated = false;
    server.post(`/mock-server/campaigns/5ca78aa160780601008f6aaa/createNotification`, () => {
      notificationCreated = true;
      return [201, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    server.put(`/mock-server/campaigns/${campaign._id}`, (request) => {
      receivedCampaign = JSON.parse(request.requestBody);
      receivedCampaign.campaign._id = campaign._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedCampaign)];
    });
  });

  it('should contain all the browse links', async function () {
    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Campaign 1', 'Edit']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/000000000000000000000001',
      '/manage/dashboards/survey/5ca78aa160780601008f6aaa',
    ]);
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/surveys/edit/${campaign._id}`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fsurveys%2Fedit%2F5ca78aa160780601008f6aaa');
  });

  it('should be able to change the team', async function () {
    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    this.element.querySelector('.value.editing select').value = '000000000000000000000001';

    await triggerEvent('.value.editing select', 'change');
    await waitUntil(() => receivedCampaign != null, { timeout: 5000 });

    expect(receivedCampaign.campaign.visibility.team).to.equal('000000000000000000000001');
  });

  it('should be able to create the add notification', async function () {
    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-notifications .btn-edit');
    await click('.row-notifications .btn-switch');

    await click('.btn-submit');

    await waitUntil(() => notificationCreated, { timeout: 3000 });

    await settled();
  });

  it('should be able to change the `isPublic` flag', async function () {
    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('#toggle-is-public');

    await waitUntil(() => receivedCampaign != null, { timeout: 5000 });

    expect(receivedCampaign.campaign.visibility.isPublic).to.equal(false);
  });

  it('should be able to change the `original author`', async function () {
    authenticateSession();

    await visit(`/manage/surveys/edit/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-original-author .btn-edit');
    await click('.ember-power-select-trigger');
    await fillIn('.ember-power-select-search-input', 'some user');
    await click('.ember-power-select-option');

    await waitUntil(() => receivedCampaign, { timeout: 10000 });

    expect(receivedCampaign.campaign.info.originalAuthor).to.equal('some-user-id');
  });

  it('can change the restrict to registered users', async function () {
    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('#toggle-mandatory-registration');

    await waitUntil(() => receivedCampaign, { timeout: 10000 });

    expect(receivedCampaign.campaign.options.registrationMandatory).to.equal(true);
  });

  it('can change the feature name prefix', async function () {
    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.row-feature-name-prefix .btn-edit');
    await typeIn('.row-feature-name-prefix input', 'new value');
    await click('.row-feature-name-prefix .btn-submit');

    await waitUntil(() => receivedCampaign, { timeout: 10000 });

    expect(receivedCampaign.campaign.options.featureNamePrefix).to.equal('new value');
  });

  it('can change the name', async function () {
    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-article .btn-edit');
    await PageElements.waitEditorJs(this.element);

    await click('.ce-header');
    await fillIn('.ce-header', 'new name');
    await triggerKeyEvent('.ce-header', 'keyup', 'Enter');

    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));
    await click('.btn-submit');

    await waitUntil(() => receivedCampaign, { timeout: 3000 });
    expect(receivedCampaign.campaign.name).to.equal('new name');
  });

  it('can change the article', async function () {
    authenticateSession();

    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-article .btn-edit');
    await PageElements.waitEditorJs(this.element);

    await fillIn('.ce-paragraph', 'new description');
    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
    await click('.btn-submit');

    await waitUntil(() => receivedCampaign, { timeout: 3000 });

    expect(receivedCampaign.campaign.article.blocks).to.deep.equal([
      { type: 'header', data: { text: campaign.name, level: 1 } },
      { type: 'paragraph', data: { text: 'new description' } },
    ]);
  });

  it('can change the cover', async function () {
    authenticateSession();

    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-cover .btn-edit');

    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Campaign', id: campaign._id },
      data: {},
      disableOptimization: false,
    });
    expect(receivedCampaign.campaign.cover).to.equal('1');

    await server.waitAllRequests();
    await PageElements.wait(1000);
  });

  it('can change the start date', async function () {
    authenticateSession();

    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-date-interval .btn-edit');
    await click('.col-start-date .form-switch input');

    await fillIn('.datepicker-input', '2020-03-20');

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);

    expect(receivedCampaign.campaign.startDate).to.startWith('2020-03-20');
  });

  it('can change the end date', async function () {
    authenticateSession();

    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-date-interval .btn-edit');
    await click('.col-end-date .form-switch input');

    await fillIn('.col-end-date .datepicker-input', '2020-03-20');

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.endDate).to.startWith('2020-03-20');
  });

  it('can change the contributor questions', async function (a) {
    authenticateSession();

    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-about-contributor-questions .btn-edit');

    await click('.btn-toggle-visibility');
    await click('.btn-add-question');

    const questions = [...this.element.querySelectorAll('.input-question')];

    await fillIn(questions[1].querySelector('.text-question'), 'new question');
    await fillIn(questions[1].querySelector('.text-name'), 'question');

    questions[1].querySelector('.select-type').value = 'short text';
    await triggerEvent(questions[1].querySelector('.select-type'), 'change');

    await click('.btn-submit');

    expect(this.element.querySelectorAll('.row-about-contributor-questions .question')).to.have.length(2);

    await waitUntil(() => receivedCampaign);
    a.deepEqual(receivedCampaign.campaign.contributorQuestions, [
      {
        question: 'What is your email?',
        help: 'We need your email to let you know if we have questions about your contribution and to let you know about the progress.',
        type: 'email',
        name: 'email',
        isRequired: true,
        isVisible: true,
        options: {},
      },
      {
        help: '',
        question: 'new question',
        name: 'question',
        type: 'short text',
        isRequired: false,
        isVisible: true,
        options: {},
      },
    ]);
  });

  it('should be able to enable a map campaign', async function () {
    let map = server.testData.create.map('campaign-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-map .btn-edit');
    this.element.querySelector('.row-map input.enable-map').checked = true;
    await triggerEvent('.row-map input.enable-map', 'change');

    this.element.querySelector('.row-map select').value = 'campaign-map';
    await triggerEvent('select', 'change');
    await click('.row-map .btn-submit');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedCampaign.campaign.map).to.deep.equal({
      isEnabled: true,
      map: 'campaign-map',
    });
  });

  it('should allow selecting icons for the campaign', async function () {
    let map = server.testData.create.map('campaign-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-default-icons .btn-edit');

    await click('.icon-container');

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.icons).to.deep.equal(['5ca7bfc0ecd8490100cab980']);
  });

  it('should allow selecting optional icons for the campaign', async function () {
    let map = server.testData.create.map('campaign-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-questions .btn-edit');

    await click('.icon-list .icon-container');

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.questions.find((a) => a.name == 'icons')).to.deep.equal({
      question: 'Please select an icon from the list',
      help: '',
      type: 'icons',
      name: 'icons',
      isRequired: true,
      isVisible: true,
      options: ['5ca7bfc0ecd8490100cab980'],
    });
    await Modal.waitToHide();
  });

  it('can set the campaign questions as optional', async function () {
    let map = server.testData.create.map('campaign-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/surveys/edit/${campaign._id}`);

    await click('.row-questions .btn-edit');

    const checkboxes = [...this.element.querySelectorAll('.chk-is-required')];

    await Promise.all(checkboxes.map((a) => click(a)));

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);

    expect(receivedCampaign.campaign.questions.map((a) => ({ name: a.name, isRequired: a.isRequired }))).to.deep.equal([
      { name: 'sounds', isRequired: false },
      { name: 'position', isRequired: false },
      { name: 'name', isRequired: false },
      { name: 'description', isRequired: false },
      { name: 'icons', isRequired: false },
      { name: 'pictures', isRequired: false },
    ]);
  });

  describe('a survey without a map', function () {
    it('allows selecting default icons', async function (a) {
      campaign.map = null;

      authenticateSession();
      await visit(`/manage/surveys/edit/${campaign._id}`);

      await click('.row-default-icons .btn-edit');

      expect(server.history).to.contain('GET /mock-server/iconsets?default=true');
      expect(this.element.querySelector('.row-default-icons .icon-container')).to.exist;
    });

    it('queries the new icon sets when a map is selected', async function (a) {
      campaign.map = null;

      let map = server.testData.create.map('2');
      map.iconSets = {
        useCustomList: true,
        list: ['22'],
      };

      server.testData.storage.addMap(map);
      server.testData.storage.addDefaultIconSet('22');

      authenticateSession();
      await visit(`/manage/surveys/edit/${campaign._id}`);

      await click('.row-map .btn-edit');
      await click('.enable-map');

      this.element.querySelector('.row-map select').value = '2';
      await triggerEvent('.row-map select', 'change');

      await click('.btn-submit');

      await wait(100);

      await click(this.element.querySelector('.row-default-icons .btn-edit'));
      expect(this.element.querySelector('.row-default-icons .icon-container')).to.exist;
    });
  });
});
