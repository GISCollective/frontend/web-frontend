/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { A } from 'core/lib/array';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | layout-rows',  function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultPicture('1');
    server.testData.storage.addDefaultArticle('2');
    server.testData.storage.addDefaultPicture('2');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when page and cols are not set', async function () {
    await render(hbs`<LayoutRows/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('.row')).not.to.exist;
    expect(this.element.querySelector('.col')).not.to.exist;
  });

  describe ('when there is a layout with 2 cols',  function (hooks) {
    hooks.beforeEach(function () {
      this.set('cols', [
        {
          col: 0,
          row: 0,
          type: 'article',
          data: {
            id: '1',
          },
        },
        {
          col: 1,
          row: 0,
          type: 'picture',
          data: {
            id: '2',
          },
        },
      ]);

      this.set('layout', A([
        {
          options: [],
          cols: [
            {
              type: 'type',
              data: {},
              options: [],
              name: '0.0.0',
            },
            {
              type: 'type',
              data: {},
              options: [],
              name: '0.0.1',
            },
          ],
        },
      ]));
    });

    it('renders the layout frame', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} as | col |>
        <View::Slot @value={{col.value}} />
      </LayoutRows>`);

      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelectorAll('.row .col')).to.have.length(2);
    });

    it('renders nothing wen the rows are removed', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} as | col |>
        <View::Slot @value={{col.value}} />
      </LayoutRows>`);

      this.set('layout', []);
      expect(this.element.querySelector('.row')).not.to.exist;
    });

    it('renders nothing wen the cols are removed', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} as | col |>
        <View::Slot @value={{col.value}} />
      </LayoutRows>`);

      this.set('layout', A([
        {
          options: [],
          cols: [],
        },
      ]));
      expect(this.element.querySelector('.col')).not.to.exist;
    });

    it('renders another row when is added', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} as | col |>
        <View::Slot @value={{col.value}} />
      </LayoutRows>`);

      this.layout.push({ cols: [] });

      await waitUntil(() => this.element.querySelectorAll('.row').length > 1);

      expect(this.element.querySelectorAll('.row')).to.have.length(2);
      expect(this.element.querySelectorAll('.col')).to.have.length(2);
    });

    it('renders the column names', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} as | col |>
        {{col.name}}
      </LayoutRows>`);

      const cols = this.element.querySelectorAll('.row .col');

      expect(cols[0].textContent).to.contain('0.0.0');
      expect(cols[1].textContent).to.contain('0.0.1');
    });
  });
});
