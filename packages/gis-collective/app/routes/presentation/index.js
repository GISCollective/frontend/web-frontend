/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from '../base/app-route';
import { hash } from 'rsvp';
import { fetch } from 'models/lib/fetch-source';

export default class PresentationIndexRoute extends AppRoute {
  async model(params) {
    const presentation = await this.store.findRecord('presentation', params.name);

    const modelData = {
      presentation,
    };

    try {
      for (const col of presentation.cols) {
        const promise = await fetch(modelData, col, {}, this.store);
        const key = col.modelKey;

        if (promise && key) {
          modelData[key] = promise;
        }
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }

    return hash(modelData);
  }
}
