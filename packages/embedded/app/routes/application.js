import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class MapViewRoute extends Route {
  @service embeddedClient;
  @service router;
  @service intl;

  async beforeModel() {
    window.router = this.router;
    window.intl = this.intl;

    this.intl.setLocale(['en-us']);
    await this.embeddedClient.setup();

    const route = this.embeddedClient.config.route;

    if (route) {
      console.log('Transition to', route);
      try {
        this.router.transitionTo(route);
      } catch (err) {
        console.error(err);
      }
    }
  }
}
