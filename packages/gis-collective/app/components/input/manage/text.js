/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageTextComponent extends Component {
  @tracked value = '';

  @action
  setupValue() {
    this.value = this.args.value;
  }

  @action
  updateValue(newValue) {
    if (typeof newValue.id == 'string') {
      this.value = newValue.id;
      return;
    }

    this.value = newValue;
  }

  @action
  save() {
    return this.args.onSave(this.value);
  }
}
