/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageGeometryComponent extends Component {
  @tracked tmpValue;

  get value() {
    if (this.tmpValue) {
      return this.tmpValue;
    }

    return this.args.value;
  }

  @action
  change(value) {
    this.tmpValue = value;
  }

  @action
  save() {
    let result;

    result = this.args.onSave?.(this.value);
    this.tmpValue = null;

    return result;
  }

  @action
  cancel() {
    this.tmpValue = null;
    return this.args.onCancel?.(...arguments);
  }

  @action
  edit() {
    this.tmpValue = null;
    return this.args.onEdit?.(...arguments);
  }
}
