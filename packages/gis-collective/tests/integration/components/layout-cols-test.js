/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { LayoutCol } from 'models/transforms/layout-row-list';

describe ('Integration | Component | layout-cols',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the layout list is empty', async function () {
    this.set('cols', []);
    await render(hbs`<LayoutCols @layout={{this.cols}} as |col|>
  {{col.name}}
</LayoutCols>`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a column when the layout list has a column', async function () {
    const col = new LayoutCol({
      name: 'test',
    });

    this.set('cols', [col]);

    await render(hbs`<LayoutCols @layout={{this.cols}} as |col|>
  {{col.name}}
</LayoutCols>`);

    expect(this.element.textContent.trim()).to.equal('test');
  });

  it('renders two columns when the layout list has two columns', async function () {
    const col1 = new LayoutCol({
      name: 'test 1',
    });
    const col2 = new LayoutCol({
      name: 'test 2',
    });

    this.set('cols', [col1, col2]);

    await render(hbs`<LayoutCols @layout={{this.cols}} as |col|>
  {{col.name}}
</LayoutCols>`);

    expect(this.element.textContent.trim()).to.contain('test 1');
    expect(this.element.textContent.trim()).to.contain('test 2');
  });

  it('renders two columns when the layout list has a column with component count 2', async function () {
    const col = new LayoutCol({
      name: 'test 1',
      componentCount: 2,
    });

    this.set('cols', [col]);

    await render(hbs`<LayoutCols @layout={{this.cols}} as |col|>
  {{col.name}}
</LayoutCols>`);

    expect(this.element.textContent.trim()).to.contain('test 1');
    expect(this.element.textContent.trim()).to.contain('test 1.1');
  });
});
