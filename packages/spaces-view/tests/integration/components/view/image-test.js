/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/image', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Image />`);
    expect(this.element.querySelector('img')).not.to.exist;

    expect(this.element.querySelector('.page-col-image')).not.to.have.class(
      'image-tool--stretched',
    );
    expect(this.element.querySelector('.page-col-image')).not.to.have.class(
      'image-tool--withBorder',
    );
    expect(this.element.querySelector('.page-col-image')).not.to.have.class(
      'image-tool--withBackground',
    );
  });

  it('renders an image when the file is set', async function () {
    this.set('value', {
      type: 'image',
      data: {
        withBorder: false,
        file: {
          url: 'some url',
        },
        stretched: true,
        withBackground: false,
        caption: '',
      },
    });

    await render(hbs`<View::Image @value={{this.value}} />`);

    expect(this.element.querySelector('img')).to.exist;
    expect(this.element.querySelector('img')).to.exist;
    expect(this.element.querySelector('.caption')).not.to.exist;
    expect(this.element.querySelector('img')).to.have.attribute(
      'src',
      'some url',
    );
  });

  it('renders a caption when is set', async function () {
    this.set('value', {
      type: 'image',
      data: {
        withBorder: false,
        file: {
          url: 'some url',
        },
        stretched: true,
        withBackground: false,
        caption: 'some caption',
      },
    });

    await render(hbs`<View::Image @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-image-caption')).to.exist;
    expect(
      this.element.querySelector('.page-col-image-caption').textContent.trim(),
    ).to.equal('some caption');
  });

  it('sets all classes when all attributes are set to true', async function () {
    this.set('value', {
      type: 'image',
      data: {
        withBorder: true,
        file: {
          url: 'some url',
        },
        stretched: true,
        withBackground: true,
        caption: '',
      },
    });

    await render(hbs`<View::Image @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--stretched',
    );
    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--filled',
    );
    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--withBorder',
    );
    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--withBackground',
    );
  });
});
