/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, fillIn, waitFor, triggerEvent, waitUntil } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import { selectChoose, selectSearch } from 'ember-power-select/test-support';
import { wait } from 'dummy/tests/helpers';

describe('Acceptance | manage/articles/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedArticle;
  let team;
  let receivedPicture;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultTeam('5ca78e2160780601008f69e6');
    server.testData.storage.addDefaultPicture();

    receivedArticle = null;

    server.get(`/mock-server/articles/_/categories`, () => {
      return [200, { 'Content-Type': 'application/json' }, '{ "categories": ["category 1", "category 2"] }'];
    });

    receivedPicture = null;
    server.post('/mock-server/pictures', (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = '1';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPicture)];
    });
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/articles/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Farticles%2Fadd');
  });

  describe('when an user is authenticated and no team is available', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser();
      server.testData.storage.teams = {};
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/articles/add`);

      expect(currentURL()).to.equal('/manage/articles/add');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal('New article');

      expect(this.element.querySelector('.row-categories label').textContent.trim()).to.contain('Categories');
      expect(this.element.querySelector('.input-categories')).to.exist;

      expect(this.element.querySelector('.row-team label').textContent.trim()).to.contain('Team');
      expect(this.element.querySelector('.row-team select')).to.not.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to.exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit(`/manage/articles/add`);
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.article');
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit(`/manage/articles/add`);
      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.article');
    });
  });

  describe('when an user is authenticated and a team is available', function (hooks) {
    hooks.beforeEach(function () {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser();
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();

      await visit(`/manage/articles/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'New article']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      await waitUntil(() => PageElements.breadcrumbs().length == 3);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'New article']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
      ]);
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/articles/add`);
      expect(currentURL()).to.equal('/manage/articles/add');

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.have.attribute('disabled', '');
    });

    it('can add a picture in the article', async function (a) {
      authenticateSession();

      await visit(`/manage/articles/add`);

      await PageElements.waitEditorJs(this.element);

      await click('.ce-paragraph');
      await click('.ce-toolbar__plus');
      await click(".ce-popover-item[data-item-name='image']");

      await waitUntil(() => document.querySelector("body > [type='file']"));

      const blob = server.testData.create.pngBlob();
      await triggerEvent(document.querySelector("body > [type='file']"), 'change', { files: [blob] });

      await waitUntil(() => receivedPicture);

      a.deepEqual(receivedPicture.picture.meta, {
        renderMode: '',
        attributions: '',
        link: {},
        data: {},
        disableOptimization: false,
      });
    });

    describe('when the server request is successful', function (hooks) {
      let page;

      hooks.beforeEach(function () {
        team = server.testData.storage.addDefaultTeam();
        page = server.testData.storage.addDefaultPage();
        server.testData.storage.addDefaultTeam('000000000000000000000001');
        server.testData.storage.addDefaultArticle('000000000000000000000001');

        server.post('/mock-server/articles', (request) => {
          receivedArticle = JSON.parse(request.requestBody);
          receivedArticle.article._id = page._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedArticle)];
        });
      });

      it('redirects to the edit page after the request response is received', async function () {
        authenticateSession();

        await visit(`/manage/articles/add`);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await click('.add-title-link');

        await fillIn('.ce-paragraph', 'test article');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await click('.input-categories .btn-add');

        await wait(100);
        await selectSearch('.text-value-0', 'cate');
        await selectChoose('.text-value-0', 'category 2');

        await wait(600);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.content.blocks).to.deep.equal([
          { type: 'header', data: { text: 'New title', level: 1 } },
          { type: 'paragraph', data: { text: 'test article' } },
        ]);
        expect(receivedArticle.article.categories).to.deep.equal(['category 2']);
        expect(receivedArticle.article.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        });

        expect(currentURL()).to.equal(`/manage/articles/${page._id}`);
      });
    });

    describe('when the server request fails', function (hooks) {
      hooks.beforeEach(function () {
        server.post('/mock-server/articles', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description: 'The `page.slug` field is required.',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function (a) {
        const service = this.owner.lookup('service:notifications');

        authenticateSession();

        await visit(`/manage/articles/add`);

        await waitFor('.editor-is-ready', { timeout: 3000 });

        await click('.add-title-link');
        await fillIn('.ce-paragraph', 'test article');

        await click('.input-categories .btn-add');
        await selectSearch('.input-categories .text-value-0', 'Blog');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/manage/articles/add`);

        a.equal(service.lastCall[0], 'handleError');
        a.equal(
          service.lastCall[1].message,
          'Ember Data Request POST /mock-server/articles returned a 400\nPayload (application/json)\n[object Object]',
        );
      });
    });
  });

  describe('when an admin user is authenticated', function (hooks) {
    hooks.beforeEach(() => {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser(true);
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/articles/add`);
      expect(currentURL()).to.equal('/manage/articles/add');
    });

    describe('when the server request is successful', function (hooks) {
      let article;

      hooks.beforeEach(function () {
        article = server.testData.storage.addDefaultArticle();
        server.post('/mock-server/articles', (request) => {
          receivedArticle = JSON.parse(request.requestBody);
          receivedArticle.article._id = article._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedArticle)];
        });
      });

      it('queries the teams without the all query param', async function () {
        authenticateSession();

        await visit(`/manage/articles/add`);

        await waitFor('.editor-is-ready', { timeout: 3000 });

        await click('.add-title-link');
        await fillIn('.ce-paragraph', 'test article');

        await click('.input-categories .btn-add');
        await selectSearch('.input-categories .text-value-0', 'Blog');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));
        expect(currentURL()).to.equal(`/manage/articles/${article._id}`);
      });

      it('queries the teams with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/manage/articles/add`);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain('GET /mock-server/teams?all=true&edit=true');
      });
    });
  });

  describe('when the team and category are set in the url', function (hooks) {
    hooks.beforeEach(() => {
      server.testData.storage.addDefaultUser(true);

      team = server.testData.storage.addDefaultTeam('1');
      team = server.testData.storage.addDefaultTeam('2');
    });

    describe('when the server request is successful', function (hooks) {
      let article;

      hooks.beforeEach(function () {
        article = server.testData.storage.addDefaultArticle();
        server.post('/mock-server/articles', (request) => {
          receivedArticle = JSON.parse(request.requestBody);
          receivedArticle.article._id = article._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedArticle)];
        });
      });

      it('sets the team and category', async function () {
        authenticateSession();

        await visit(`/manage/articles/add?teamId=2&category=test`);

        await waitFor('.editor-is-ready', { timeout: 3000 });

        await click('.add-title-link');
        await fillIn('.ce-paragraph', 'test article');

        expect(this.element.querySelector('.input-categories .text-value-0').textContent.trim()).to.equal('test');
        expect(this.element.querySelector('.row-team select').value).to.equal('2');

        await PageElements.wait(1000);

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedArticle.article.visibility).to.deep.equal({ isPublic: false, isDefault: false, team: '2' });
        expect(receivedArticle.article.categories).to.deep.equal(['test']);
      });
    });
  });
});
