/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class LinkMapTransform extends Transform {
  deserialize(serialized) {
    return new LinkMap(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON() ?? deserialized;
  }
}

export class LinkMap {
  @tracked value = {};

  constructor(serialized) {
    if (typeof serialized != 'object' || !serialized) {
      return;
    }

    for (const key in serialized) {
      this.value[key] = new Link(serialized[key]);
    }
  }

  toJSON() {
    const result = {};

    for (const key in this.value) {
      result[key] = this.value[key].toJSON();
    }

    return result;
  }
}

export class Link {
  @tracked route;
  @tracked model;
  @tracked path;
  @tracked url;
  @tracked anchor;

  keys = ['route', 'model', 'path', 'url', 'anchor'];

  constructor(serialized) {
    for (const key of this.keys) {
      this[key] = serialized[key];
    }
  }

  get niceString() {
    let result = [];

    for (const key of this.keys) {
      if (this[key]) {
        result.push(`${key} ${this[key]}`);
      }
    }

    return result.join(', ');
  }

  toJSON() {
    const result = {};

    for (const key of this.keys) {
      if (this[key]) {
        result[key] = this[key];
      }
    }

    return result;
  }
}
