/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class CampaignQuestionsTransform extends Transform {
  deserialize(serialized) {
    return new CampaignQuestions(serialized);
  }

  serialize(deserialized) {
    if (deserialized?.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class CampaignQuestions {
  @tracked name = new CampaignLabelQuestion();
  @tracked description = new CampaignLabelQuestion();
  @tracked icons = new CampaignLabelQuestion();
  @tracked location = new CampaignLocationQuestion();
  @tracked pictures = new CampaignPicturesQuestion();

  @tracked registrationMandatory = false;
  @tracked featureNamePrefix = '';

  constructor(value) {
    if (!value || typeof value != 'object') {
      return;
    }

    if (value.featureNamePrefix) {
      this.featureNamePrefix = value.featureNamePrefix;
    }

    if (value.registrationMandatory) {
      this.registrationMandatory = value.registrationMandatory;
    }

    if (value.name) {
      this.name = new CampaignLabelQuestion(value.name);
    }

    if (value.description) {
      this.description = new CampaignLabelQuestion(value.description);
    }

    if (value.icons) {
      this.icons = new CampaignLabelQuestion(value.icons);
    }

    if (value.location) {
      this.location = new CampaignLocationQuestion(value.location);
    }

    if (value.pictures) {
      this.pictures = new CampaignPicturesQuestion(value.pictures);
    }

    if (value.showNameQuestion) {
      this.name.customQuestion = value.showNameQuestion;
    }

    if (value.nameLabel) {
      this.name.label = value.nameLabel;
    }

    if (value.showDescriptionQuestion) {
      this.description.customQuestion = value.showDescriptionQuestion;
    }

    if (value.descriptionLabel) {
      this.description.label = value.descriptionLabel;
    }

    if (value.iconsLabel) {
      this.icons.label = value.iconsLabel;
    }
  }

  get showNameQuestion() {
    return this.name.customQuestion;
  }

  get nameLabel() {
    return this.name.label;
  }

  get showDescriptionQuestion() {
    return this.description.customQuestion;
  }

  get descriptionLabel() {
    return this.description.label;
  }

  get iconsLabel() {
    return this.icons.label;
  }

  toJSON() {
    const result = {
      registrationMandatory: this.registrationMandatory,
      featureNamePrefix: this.featureNamePrefix,
      name: this.name.toJSON(),
      description: this.description.toJSON(),
      icons: this.icons.toJSON(),
      location: this.location.toJSON(),
      pictures: this.pictures.toJSON(),
    };

    return result;
  }
}

export class CampaignLabelQuestion {
  @tracked customQuestion = false;
  @tracked label = '';

  constructor(value) {
    if (!value || typeof value != 'object') {
      return;
    }

    if (value.customQuestion) {
      this.customQuestion = value.customQuestion;
    }

    if (value.label) {
      this.label = value.label;
    }
  }

  toJSON() {
    return {
      customQuestion: this.customQuestion,
      label: this.label,
    };
  }
}

export class CampaignLocationQuestion {
  @tracked customQuestion = false;
  @tracked label = '';
  @tracked allowGps = true;
  @tracked allowManual = true;
  @tracked allowAddress = true;
  @tracked allowExistingFeature = false;

  constructor(value) {
    if (!value || typeof value != 'object') {
      return;
    }

    if (value.customQuestion) {
      this.customQuestion = value.customQuestion;
    }

    if (value.label) {
      this.label = value.label;
    }

    if (typeof value.allowGps == 'boolean') {
      this.allowGps = value.allowGps;
    }

    if (typeof value.allowManual == 'boolean') {
      this.allowManual = value.allowManual;
    }

    if (typeof value.allowAddress == 'boolean') {
      this.allowAddress = value.allowAddress;
    }

    if (typeof value.allowExistingFeature == 'boolean') {
      this.allowExistingFeature = value.allowExistingFeature;
    }
  }

  toJSON() {
    return {
      customQuestion: this.customQuestion,
      label: this.label,
      allowGps: this.allowGps,
      allowManual: this.allowManual,
      allowAddress: this.allowAddress,
      allowExistingFeature: this.allowExistingFeature,
    };
  }
}

export class CampaignPicturesQuestion {
  @tracked customQuestion = false;
  @tracked isMandatory = false;
  @tracked label = '';

  constructor(value) {
    if (!value || typeof value != 'object') {
      return;
    }

    if (value.customQuestion) {
      this.customQuestion = value.customQuestion;
    }

    if (value.label) {
      this.label = value.label;
    }

    if (typeof value.isMandatory == 'boolean') {
      this.isMandatory = value.isMandatory;
    }
  }

  toJSON() {
    return {
      customQuestion: this.customQuestion,
      label: this.label,
      isMandatory: this.isMandatory,
    };
  }
}
