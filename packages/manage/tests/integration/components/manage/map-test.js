/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, triggerEvent, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/map', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders', async function () {
      await render(hbs`<Manage::Map @title="title" />`);

      expect(this.element.textContent.trim()).to.contain('Answers from this survey will not be added to a map.');
    });

    it('renders the map name when it is enabled', async function () {
      this.set('value', {
        isEnabled: true,
        map: {
          name: 'custom map',
        },
      });

      await render(hbs`<Manage::Map @title="title" @value={{this.value}}/>`);

      expect(this.element.querySelector('h4').textContent.trim()).to.contain(
        'Answers from this survey will be added on',
      );
      expect(this.element.querySelector('a').textContent.trim()).to.contain('custom map');
    });
  });

  describe('in edit mode', function (hooks) {
    it('renders a disabled switch when the map is not enabled', async function () {
      await render(hbs`<Manage::Map @title="title" @editablePanel="title" />`);

      expect(this.element.querySelector('.enable-map')).exist;
      expect(this.element.querySelector('.enable-map').checked).to.equal(false);
    });

    it('renders an enabled switch when the map is enabled', async function () {
      this.set('value', {
        isEnabled: true,
        map: {
          name: 'custom map',
        },
      });

      await render(hbs`<Manage::Map @value={{this.value}} @title="title" @editablePanel="title" />`);

      expect(this.element.querySelector('.enable-map')).exist;
      expect(this.element.querySelector('.enable-map').checked).to.equal(true);
    });

    it('allows choosing a new map', async function () {
      this.set('value', {
        isEnabled: true,
        map: {
          id: 'map-1',
          name: 'custom map',
        },
      });

      this.set('maps', [
        {
          id: 'map-1',
          name: 'map 1',
        },
        {
          id: 'map-2',
          name: 'map 2',
        },
      ]);

      await render(
        hbs`<Manage::Map @maps={{this.maps}} @value={{this.value}} @title="title" @editablePanel="title" />`,
      );

      expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');
      expect(this.element.querySelector('.map-select').value).to.equal('map-1');

      this.element.querySelector('select').value = 'map-2';
      await triggerEvent('select', 'change');

      expect(this.element.querySelector('.map-select').value).to.equal('map-2');
    });

    it('allows enabling a map', async function () {
      this.set('value', {});

      this.set('maps', [
        {
          id: 'map-1',
          name: 'map 1',
        },
        {
          id: 'map-2',
          name: 'map 2',
        },
      ]);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Map @maps={{this.maps}} @onSave={{this.save}} @value={{this.value}} @title="title" @editablePanel="title" />`,
      );

      await click('.enable-map');
      this.element.querySelector('select').value = 'map-2';
      await triggerEvent('select', 'change');

      await click('.btn-submit');

      expect(value.isEnabled).to.equal(true);
      expect(value.mapId).to.equal('map-2');
    });
  });
});
