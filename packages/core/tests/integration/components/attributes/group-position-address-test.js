/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, click, fillIn, waitUntil, triggerKeyEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | attributes/group-position-address', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let details;
  let position;

  describe('when there are matching geocodings', (hooks) => {
    hooks.before(function () {
      server = new TestServer();
      server.testData.storage.addDefaultGeocoding();

      const polygon = server.testData.create.geocoding();
      polygon._id = '2';
      polygon.name = 'address 2';
      polygon.geometry = {
        type: 'Polygon',
        coordinates: [
          [
            [3.86, 36.03],
            [31.28, 36.03],
            [31.28, 48.92],
            [3.86, 48.92],
            [3.86, 36.03],
          ],
        ],
      };
      server.testData.storage.addGeocoding(polygon);
    });

    hooks.beforeEach(async function () {
      details = null;

      this.set('change', (p, d) => {
        details = d;
        position = p;
      });
    });

    hooks.after(function () {
      server.shutdown();
    });

    it('renders an empty search field', async function () {
      await render(hbs`<Attributes::GroupPositionAddress @details={{this.details}} @onChange={{this.change}}/>`);

      const attributesValues = this.element.querySelectorAll('.attribute-value .form-control');

      expect(this.element.querySelector('label').textContent.trim()).to.equal('search an address');

      expect(attributesValues).to.have.length(1);
      expect(attributesValues[0].value).to.equal('');
    });

    it('shows the selected search values', async function () {
      this.set('details', {
        type: 'address',
        address: 'address 1',
        searchTerm: 'address',
      });

      await render(
        hbs`<Attributes::GroupPositionAddress
        @details={{this.details}}
        @onChange={{this.change}} />`,
      );

      const options = this.element.querySelectorAll('.value-options');
      expect(options[0].querySelector('.name-list').textContent.trim()).to.equal('address 1');
      expect(this.element.querySelector('.attribute-value input').value).to.equal('address');
    });

    it('triggers a change with the selected geometry', async function () {
      this.set('details', {
        type: 'address',
        searchTerm: 'address',
      });

      await render(
        hbs`<Attributes::GroupPositionAddress
        @details={{this.details}}
        @onChange={{this.change}}
      />`,
      );

      await click('.btn-add');
      const options = this.element.querySelectorAll('.value-options .list-group-item');

      await click(options[0]);
      await click('.btn-hide');

      expect(details).to.deep.equal({
        type: 'address',
        searchTerm: 'address',
        address: 'address 2',
      });
      expect(position).to.deep.contain({
        type: 'Point',
        coordinates: [17.57, 42.475],
      });
    });

    it('converts a selected polygon to a point', async function () {
      this.set('details', {
        type: 'address',
        searchTerm: 'address',
      });

      await render(
        hbs`<Attributes::GroupPositionAddress
        @details={{this.details}}
        @onChange={{this.change}} />`,
      );

      await click('.btn-add');
      const options = this.element.querySelectorAll('.value-options .list-group-item');

      await click(options[1]);

      expect(details).to.deep.equal({
        type: 'address',
        searchTerm: 'address',
        address: 'Berlin, Coos County, New Hampshire, 03570, United States',
      });
      expect(position).to.deep.contain({
        type: 'Point',
        coordinates: [13.433576, 52.495781],
      });
    });

    it('triggers a change with the search term on enter', async function () {
      await render(
        hbs`<Attributes::GroupPositionAddress
        @details={{this.details}}
        @onChange={{this.change}} />`,
      );

      await fillIn('.attribute-value .form-control', 'search term');
      await triggerKeyEvent('.attribute-value .form-control', 'keyup', 'Enter');

      expect(details).to.deep.equal({
        searchTerm: 'search term',
        address: 'address 2',
        type: 'address',
      });
      expect(`${position.coordinates[0]}`).to.equal('17.57');
      expect(`${position.coordinates[1]}`).to.equal('42.475');
    });

    it('triggers a change with the search term on search click', async function () {
      await render(
        hbs`<Attributes::GroupPositionAddress
        @details={{this.details}}
        @onChange={{this.change}} />`,
      );

      await fillIn('.attribute-value .form-control', 'search term');
      await click('.btn-search-address');

      expect(details).to.deep.equal({
        searchTerm: 'search term',
        address: 'address 2',
        type: 'address',
      });
      expect(`${position.coordinates[0]}`).to.equal('17.57');
      expect(`${position.coordinates[1]}`).to.equal('42.475');
    });

    it('shows a loading icon while searching', async function () {
      this.set('change', () => {
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve();
          }, 300);
        });
      });

      await render(
        hbs`<Attributes::GroupPositionAddress
        @details={{this.details}}
        @onChange={{this.change}} />`,
      );

      await fillIn('.attribute-value .form-control', 'search term');
      click('.btn-search-address');

      await waitUntil(() => this.element.querySelector('.fa-spin'));

      await waitUntil(() => !this.element.querySelector('.fa-spin'));
    });
  });

  describe('when there are no matching geocodings', (hooks) => {
    hooks.before(function () {
      server = new TestServer();
    });

    hooks.after(function () {
      server.shutdown();
    });

    it('shows a nice message when the address was not found', async function () {
      this.set('details', {});

      await render(
        hbs`<Attributes::GroupPositionAddress
        @details={{this.details}}
        @onChange={{this.change}}
      />`,
      );

      await fillIn('.form-control', 'some address');
      await click('.btn-search-address');

      expect(this.element.querySelector('.alert-no-address')).to.exist;
    });
  });
});
