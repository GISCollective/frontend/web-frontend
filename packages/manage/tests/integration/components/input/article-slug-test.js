/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { fillIn, render, typeIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | input/article-slug', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let article;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.after(async function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    article = server.testData.storage.addDefaultArticle('taken-slug');
    article.slug = 'taken-slug';
  });

  it('renders an empty value', async function (assert) {
    await render(hbs`<Input::ArticleSlug />`);

    expect(this.element.querySelector('.input-article-slug').value).to.equal('');
  });

  it('renders a valid value', async function (assert) {
    await render(hbs`<Input::ArticleSlug @value="some-slug"/>`);

    expect(this.element.querySelector('.input-article-slug').value).to.equal('some-slug');
    expect(this.element.querySelector('.invalid-feedback')).not.to.exist;
  });

  it('can change a value', async function (assert) {
    let value;

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Input::ArticleSlug @value="some-slug" @onChange={{this.onChange}} @timeout={{100}}/>`);

    await fillIn('.input-article-slug', 'new-value');

    expect(this.element.querySelector('.input-article-slug').value).to.equal('new-value');
    expect(value).to.equal('new-value');
    expect(this.element.querySelector('.invalid-feedback')).not.to.exist;
  });

  it('shows a validation message when the slug contains a space', async function (assert) {
    let value;

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Input::ArticleSlug @value="some-slug" @onChange={{this.onChange}} @timeout={{100}}/>`);

    await fillIn('.input-article-slug', 'new value');

    expect(this.element.querySelector('.input-article-slug').value).to.equal('new value');
    expect(value).to.equal('new value');
    expect(this.element.querySelector('.invalid-feedback')).to.have.textContent(
      'The slug should not contain spaces. You can only use lower case characters, numbers and dashes.',
    );
  });

  it('shows a validation message when the slug contains double dashes', async function (assert) {
    let value;

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Input::ArticleSlug @value="some-slug" @onChange={{this.onChange}} @timeout={{100}}/>`);

    await fillIn('.input-article-slug', 'new--value');

    expect(this.element.querySelector('.input-article-slug').value).to.equal('new--value');
    expect(value).to.equal('new--value');
    expect(this.element.querySelector('.invalid-feedback')).to.have.textContent(
      'The slug should not contain two consecutive dashes. You can only use lower case characters, numbers and dashes.',
    );
  });

  it('shows a validation message when the slug contains an upper case char', async function (assert) {
    let value;

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Input::ArticleSlug @value="some-slug" @onChange={{this.onChange}} @timeout={{100}}/>`);

    await fillIn('.input-article-slug', 'new-Value');

    expect(this.element.querySelector('.input-article-slug').value).to.equal('new-Value');
    expect(value).to.equal('new-Value');
    expect(this.element.querySelector('.invalid-feedback')).to.have.textContent(
      'The slug should not contain upper case chars. You can only use lower case characters, numbers and dashes.',
    );
  });

  it('shows a validation message when the slug contains non alphanumeric chars', async function (assert) {
    let value;

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Input::ArticleSlug @value="some-slug" @onChange={{this.onChange}} @timeout={{100}}/>`);

    await fillIn('.input-article-slug', 'new+value');

    expect(this.element.querySelector('.input-article-slug').value).to.equal('new+value');
    expect(value).to.equal('new+value');
    expect(this.element.querySelector('.invalid-feedback')).to.have.textContent(
      'The slug contains invalid chars. You can only use lower case characters, numbers and dashes.',
    );
  });

  it('shows a validation error when an article with the same slug is found', async function () {
    let value;

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Input::ArticleSlug @value="some-slug" @onChange={{this.onChange}} @timeout={{100}}/>`);

    await fillIn('.input-article-slug', 'taken-slug');

    expect(this.element.querySelector('.input-article-slug').value).to.equal('taken-slug');
    expect(value).to.equal('taken-slug');
    expect(this.element.querySelector('.invalid-feedback')).to.have.textContent(
      'The slug is already used by another article. You can only use lower case characters, numbers and dashes.',
    );
  });

  it('does not show a validation error when the article matches its own slug', async function () {
    let value;

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Input::ArticleSlug @value="taken-slug" @onChange={{this.onChange}} @timeout={{100}} />`);

    await fillIn('.input-article-slug', 'other-slug');
    await fillIn('.input-article-slug', 'taken-slug');

    expect(this.element.querySelector('.invalid-feedback')).not.to.exist;
  });
});
