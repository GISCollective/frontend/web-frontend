/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/dashboard/newsletter-stats', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let team;
  let newsletter;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function() {
    server.shutdown();
  })

  hooks.beforeEach(async function () {
    team = server.testData.storage.addDefaultTeam();
    team.id = team._id;
    newsletter = server.testData.storage.addDefaultNewsletter();

    server.testData.storage.addStat(`newsletter-subscribers-newsletter-${newsletter._id}`, 20);
    server.testData.storage.addStat(`newsletter-emails-newsletter-${newsletter._id}`, 30);
    server.testData.storage.addStat(`newsletter-open-newsletter-${newsletter._id}`, 40);
    server.testData.storage.addStat(`newsletter-interactions-newsletter-${newsletter._id}`, 50);

    this.set('team', team);
    this.set('newsletter', await this.store.queryRecord('newsletter', newsletter._id));
  });

  it('renders the subscriber number', async function () {
    await render(hbs`<Manage::Dashboard::NewsletterStats @newsletter={{this.newsletter}} />`);

    const titles = this.element.querySelectorAll('.stat-title');
    const values = this.element.querySelectorAll('.stat-value');

    expect(titles[0].textContent.trim()).to.equal('Subscribers');
    expect(values[0].textContent.trim()).to.equal('20');
  });

  it('renders the sent emails count', async function () {
    await render(hbs`<Manage::Dashboard::NewsletterStats @newsletter={{this.newsletter}} />`);

    const titles = this.element.querySelectorAll('.stat-title');
    const values = this.element.querySelectorAll('.stat-value');

    expect(titles[1].textContent.trim()).to.equal('Emails sent');
    expect(values[1].textContent.trim()).to.equal('30');
  });

  it('renders the open emails count', async function () {
    await render(hbs`<Manage::Dashboard::NewsletterStats @newsletter={{this.newsletter}} />`);

    const titles = this.element.querySelectorAll('.stat-title');
    const values = this.element.querySelectorAll('.stat-value');

    expect(titles[2].textContent.trim()).to.equal('Emails opened');
    expect(values[2].textContent.trim()).to.equal('40');
  });

  it('renders the interactions emails count', async function () {
    await render(hbs`<Manage::Dashboard::NewsletterStats @newsletter={{this.newsletter}} />`);

    const titles = this.element.querySelectorAll('.stat-title');
    const values = this.element.querySelectorAll('.stat-value');

    expect(titles[3].textContent.trim()).to.equal('Emails interactions');
    expect(values[3].textContent.trim()).to.equal('50');
  });
});
