/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { InheritedStyle } from '../../../transforms/inherited-style';
import AlertMessage from '../../../lib/alert-message';

export default class EditIconManageController extends Controller {
  @service notifications;
  @service intl;
  @service store;
  @service fastboot;
  @service router;

  @tracked savingField;
  @tracked editableField;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.icon-set',
        model: this.model.iconSet.get('id'),
        text: this.model.iconSet.get('name'),
      },
      { text: this.intl.t('edit-model.title', { name: this.model.icon.name }) },
    ];
  }

  get alertList() {
    const list = [];
    const iconSet = this.model.icon.get('iconSet');
    const parentIconSet = this.model.parentIconSet;

    if (iconSet && !iconSet?.get('visibility')?.isPublic) {
      list.push(new AlertMessage(this.intl.t('message-alert-icon-set-private'), 'warning', 'alert-icon-set-private'));
    }

    const isParentIconSetVisible = parentIconSet?.get('visibility')?.isPublic;
    if (parentIconSet && !isParentIconSetVisible) {
      list.push(
        new AlertMessage(this.intl.t('message-alert-parent-icon-private'), 'warning', 'alert-parent-icon-private'),
      );
    }

    return list;
  }

  get isSavingIcon() {
    return this.savingField == 'icon';
  }

  get isSavingAttributes() {
    return this.savingField == 'attributes';
  }

  get categories() {
    var result = [];

    this.model.icons.forEach((element) => {
      const category = element.get('category');

      if (result.indexOf(category) == -1) {
        result.push(category);
      }
    });

    return result;
  }

  get subcategories() {
    var result = [];
    const category = this.model.icon.category;

    this.model.icons
      .filter((a) => a != category)
      .forEach((element) => {
        const subcategory = element.get('subcategory');

        if (result.indexOf(subcategory) == -1) {
          result.push(subcategory);
        }
      });

    return result;
  }

  get attributes() {
    return this.model.icon.attributes;
  }

  get otherNames() {
    if (!this.model.icon.otherNames?.length) {
      return '';
    }

    return this.model.icon.otherNames.map((a) => a.trim()).join(', ');
  }

  performIconSearch(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject('Term is too short');
    }

    this.store
      .query('icon', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  saveField(field, value) {
    this.model.icon[field] = value;

    return this.save();
  }

  @action
  saveOtherNames(value) {
    this.model.icon.set(
      'otherNames',
      value
        .split(',')
        .map((a) => a.trim())
        .filter((a) => a != ''),
    );

    return this.save();
  }

  @action
  parentIconChanged(icon) {
    this.model.icon.set('parent', icon?.id ?? '');
    this.model.parentIcon = icon;

    return this.save();
  }

  @action
  changeIconSet(iconSet) {
    this.model.icon.set('iconSet', iconSet);
    return this.save();
  }

  @action
  changeIconImage(image) {
    this.editableField = 'icon';
    this.model.icon.image = image;
    return this.save();
  }

  @action
  saveAttributes(value) {
    this.model.icon.attributes = value;

    return this.save();
  }

  @action
  saveCategory(value) {
    this.model.icon.category = value;

    return this.save();
  }

  @action
  saveSubcategory(value) {
    this.model.icon.subcategory = value;

    return this.save();
  }

  @action
  save() {
    this.savingField = this.editableField;

    later(() => {
      this.editableField = '';

      this.model.icon.save().then(
        () => {
          this.savingField = '';
        },
        (err) => {
          this.notifications.handleError(err);
          this.savingField = '';
        },
      );
    }, 200);
  }

  @action
  saveAllowMany(newValue) {
    this.model.icon.set('allowMany', newValue);
    this.editableField = this.intl.t('allow many instances');

    return this.save();
  }

  @action
  saveKeepWhenSmall(newValue) {
    this.model.icon.keepWhenSmall = newValue;
    this.editableField = this.intl.t('keep it on map when it is small');

    return this.save();
  }

  @action
  saveZoom(minZoom, maxZoom) {
    this.model.icon.set('minZoom', minZoom);
    this.model.icon.set('maxZoom', maxZoom);

    return this.save();
  }

  @action
  saveArticle(name, description) {
    this.model.icon.set('name', name);
    this.model.icon.set('description', description);

    return this.save();
  }

  @action
  saveStyles(value) {
    this.model.icon.styles = new InheritedStyle(value);
    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.icon.rollbackAttributes();
  }

  @action
  async delete() {
    await this.model.icon.destroyRecord();

    this.router.transitionTo('browse.icons.set', this.model.iconSet.id);
  }
}
