/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, fillIn, triggerEvent, click, waitUntil } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/basemaps/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedBaseMap;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();

    server.get('/mock-server/basemaps/1', () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedBaseMap)];
    });

    receivedBaseMap = null;
  });


  describe('when there is a team available', function (hooks) {
    let team;

    hooks.beforeEach(function () {
      team = server.testData.storage.addDefaultTeam();
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/basemaps/add');
      expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fbasemaps%2Fadd');
    });

    it('should not redirect to login without an auth token', async function () {
      authenticateSession();

      await visit('/manage/basemaps/add');

      expect(currentURL()).to.equal('/manage/basemaps/add');
      expect(this.element.querySelector('.input-base-map-name')).to.exist;
      expect(this.element.querySelector('.input-base-map-team')).to.exist;
      expect(this.element.querySelector('.btn.btn-primary')).to.exist;

      expect(this.element.querySelector('.alert-danger .btn-add-team')).not.to.exist;
      expect(this.element.querySelector('.alert-danger')).not.to.exist;
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit('/manage/basemaps/add');

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'New base map']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'New base map']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
      ]);
    });

    it('should disable the add button by default', async function () {
      authenticateSession();

      await visit('/manage/basemaps/add');

      expect(currentURL()).to.equal('/manage/basemaps/add');

      expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute('disabled', '');
    });

    it('should enable the add button when all fields are filled', async function () {
      authenticateSession();

      await visit('/manage/basemaps/add');
      expect(currentURL()).to.equal('/manage/basemaps/add');

      await fillIn('.input-base-map-name', 'test');

      this.element.querySelector('.input-base-map-team').value = '5ca78e2160780601008f69e6';
      await triggerEvent('.input-base-map-team', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.attribute('disabled', '');
    });

    it('should send all values to the server and redirect to the index page', async function () {
      server.post('/mock-server/basemaps', (request) => {
        receivedBaseMap = JSON.parse(request.requestBody);
        receivedBaseMap['baseMap']['_id'] = '1';

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedBaseMap)];
      });

      authenticateSession();

      await visit('/manage/basemaps/add');

      await fillIn('.input-base-map-name', 'test');

      this.element.querySelector('.input-base-map-team').value = '5ca78e2160780601008f69e6';
      await triggerEvent('.input-base-map-team', 'change');

      await waitUntil(() => this.element.querySelectorAll('.btn.btn-primary.disabled').length == 0);

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(currentURL()).to.equal('/manage/basemaps/edit/1');

      expect(receivedBaseMap).to.deep.equal({
        baseMap: {
          name: 'test',
          icon: '',
          layers: [],
          visibility: {
            isPublic: false,
            isDefault: false,
            team: '5ca78e2160780601008f69e6',
          },
          defaultOrder: null,
          cover: null,
          _id: '1',
        },
      });
    });

    it('should show a modal with an error on failure', async function (a) {
      server.post('/mock-server/basemaps', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                title: 'Some error',
                description: 'some message',
                status: 400,
              },
            ],
          }),
        ];
      });

      authenticateSession();

      let error;

      const notifications = this.owner.lookup('service:notifications');
      notifications.handleError = (e) => {
        error = e['errors'][0];
      };

      await visit('/manage/basemaps/add');

      await fillIn('.input-base-map-name', 'test');

      this.element.querySelector('.input-base-map-team').value = '5ca78e2160780601008f69e6';
      await triggerEvent('.input-base-map-team', 'change');

      await waitUntil(() => this.element.querySelectorAll('.btn.btn-primary.disabled').length == 0);
      await click(this.element.querySelector('.btn.btn-primary'));

      a.deepEqual(error, {
        description: 'some message',
        status: 400,
        title: 'Some error',
      });
    });
  });

  describe('when there is no team available', function (hooks) {
    it('should show a button to guide the user to create a team first', async function () {
      authenticateSession();
      await visit('/manage/basemaps/add');

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to.exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/basemaps/add');
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.base-map');
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/basemaps/add');

      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.base-map');
    });
  });
});
