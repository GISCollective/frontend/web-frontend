import { module, test } from 'qunit';
import { visit, currentURL, settled } from '@ember/test-helpers';
import { setupApplicationTest } from 'embedded/tests/helpers';
import { transform } from 'ol/proj';
import { expect } from 'chai';

module('Acceptance | geo-json', function (hooks) {
  setupApplicationTest(hooks);
  let service;

  hooks.beforeEach(function () {
    service = this.owner.lookup('service:embedded-client');
  });

  hooks.afterEach(function () {
    service.setConfig('apiUrl', '/mock-server');
  });

  test('visiting / when the route is "geo-json"', async function (assert) {
    service.setConfig('route', 'geo-json');
    service.setConfig('apiUrl', 'https://app.giscollective.com/api-v1');
    await visit('/');

    assert.strictEqual(currentURL(), '/geo-json');
  });

  test('setting the value updates the component', async function (assert) {
    service.setConfig('route', 'geo-json');
    service.setConfig('apiUrl', 'https://app.giscollective.com/api-v1');
    await visit('/');

    const center = transform(
      [23.369218111038208, 45.43939480420836],
      'EPSG:4326',
      'EPSG:3857',
    );

    window.setValue({
      type: 'Point',
      coordinates: [23.369218111038208, 45.43939480420836],
    });

    await settled();

    const map = this.element.querySelector('.map').olMap;
    const view = map.getView();

    expect(view.getZoom()).to.equal(18);
    expect(view.getCenter()).to.deep.equal(center);

    expect(this.element.querySelector('.map-overlay')).to.have.attribute(
      'data-lon',
      `${center[0]}`,
    );
    expect(this.element.querySelector('.map-overlay')).to.have.attribute(
      'data-lat',
      `${center[1]}`,
    );
    expect(this.element.querySelector('.map-overlay .point-icon')).to.exist;
  });

  test('setting a null value shows a message', async function (assert) {
    service.setConfig('route', 'geo-json');
    service.setConfig('apiUrl', 'https://app.giscollective.com/api-v1');
    await visit('/');

    window.setValue(null);

    await settled();

    expect(this.element.querySelector('.map')).not.to.exist;
    expect(this.element.querySelector('.message').textContent.trim()).to.equal(
      'Select a feature to view it on the map.',
    );
  });
});
