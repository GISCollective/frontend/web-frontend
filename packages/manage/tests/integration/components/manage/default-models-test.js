import { module, test } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | manage/default-models', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let map;
  let campaign;
  let calendar;
  let newsletter;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    map = server.testData.storage.addDefaultMap();
    campaign = server.testData.storage.addDefaultCampaign();
    calendar = server.testData.storage.addDefaultCalendar();
    newsletter = server.testData.storage.addDefaultNewsletter();

    server.testData.storage.addDefaultMap('1');
  });

  describe('view mode', function () {
    it('renders no values when it is not set', async function (a) {
      await render(hbs`<Manage::DefaultModels @title="test" />`);

      const models = [...this.element.querySelectorAll('.model')].map((a) => a.textContent.trim());
      const values = [...this.element.querySelectorAll('.value')].map((a) => a.textContent.trim());

      expect(models).to.have.length(4);
      expect(values).to.have.length(4);

      a.deepEqual(models, ['map', 'survey', 'calendar', 'newsletter']);
      a.deepEqual(values, ['not set', 'not set', 'not set', 'not set']);
    });

    it('renders the model names when they are set', async function (a) {
      this.set('value', {
        map: map._id,
        campaign: campaign._id,
        calendar: calendar._id,
        newsletter: newsletter._id,
      });

      await render(hbs`<Manage::DefaultModels @title="test" @value={{this.value}} />`);

      const models = [...this.element.querySelectorAll('.model')].map((a) => a.textContent.trim());
      const values = [...this.element.querySelectorAll('.value')].map((a) => a.textContent.trim());

      expect(models).to.have.length(4);
      expect(values).to.have.length(4);

      a.deepEqual(models, ['map', 'survey', 'calendar', 'newsletter']);
      a.deepEqual(values, ['Harta Verde București', 'Campaign 1', 'test calendar', 'my first newsletter']);
    });

    it('renders nothing when the models are not found', async function (a) {
      this.set('value', {
        map: '2',
        campaign: '1',
        calendar: '1',
        newsletter: '1',
      });

      await render(hbs`<Manage::DefaultModels @title="test" @value={{this.value}} />`);

      const models = [...this.element.querySelectorAll('.model')].map((a) => a.textContent.trim());
      const values = [...this.element.querySelectorAll('.value')].map((a) => a.textContent.trim());

      expect(models).to.have.length(4);
      expect(values).to.have.length(4);

      a.deepEqual(models, ['map', 'survey', 'calendar', 'newsletter']);
      a.deepEqual(values, ['not set', 'not set', 'not set', 'not set']);
    });
  });

  describe('edit mode', function () {
    it('renders the model names when they are set', async function (a) {
      this.set('value', {
        map: map._id,
        campaign: campaign._id,
        calendar: calendar._id,
        newsletter: newsletter._id,
      });

      await render(hbs`<Manage::DefaultModels @title="test" @editablePanel="test" @value={{this.value}} />`);

      const selects = [...this.element.querySelectorAll('select')].map((a) => a.value);

      expect(selects).to.deep.equal([map._id, campaign._id, calendar._id, newsletter._id]);
    });

    it('can change the value', async function (a) {
      this.set('value', {
        map: map._id,
        campaign: campaign._id,
        calendar: calendar._id,
        newsletter: newsletter._id,
      });

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::DefaultModels @title="test" @editablePanel="test" @value={{this.value}} @onSave={{this.save}} />`,
      );

      const selects = [...this.element.querySelectorAll('select')];

      selects[0].value = '1';
      await triggerEvent(selects[0], 'change');

      await click('.btn-submit');

      expect(value.toJSON().map).to.equal('1');
    });
  });
});
