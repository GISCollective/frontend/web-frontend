/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr } from '@ember-data/model';

export default class TranslationModel extends Model {
  @attr('string') name;
  @attr('string') locale;
  @attr('string') file;
  @attr customTranslations;

  @attr('boolean') canEdit;
}
