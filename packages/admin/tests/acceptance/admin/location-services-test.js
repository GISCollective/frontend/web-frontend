/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, triggerEvent, click } from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';

describe ('Acceptance | admin/location-services',  function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultSpace();
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('Should redirect to index if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit('/admin/location-services');
    expect(currentURL()).to.equal('/');
  });

  it('Should redirect to index if the user is not authenticated', async function () {
    await visit('/admin/location-services');
    expect(currentURL()).to.equal('/login');
  });

  it('Should let administrators visit /admin/location-services', async function () {
    authenticateSession();
    server.testData.storage.addPreference(
      'locationServices.maskingPrecision',
      ''
    );

    await visit('/admin/location-services');

    expect(currentURL()).to.equal('/admin/location-services');

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;
  });

  it('should have a disabled submit button', async function () {
    authenticateSession();
    server.testData.storage.addPreference(
      'locationServices.maskingPrecision',
      ''
    );

    await visit('/admin/location-services');

    expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
      'disabled',
      ''
    );
  });

  it('should have the server values', async function () {
    authenticateSession();

    server.testData.storage.addPreference(
      'locationServices.maskingPrecision',
      '0'
    );

    await visit('/admin/location-services');
    expect(currentURL()).to.equal('/admin/location-services');

    expect(
      this.element.querySelector('.input-masking-precision').value
    ).to.equal('0');
  });

  describe ('failing put request',  function (hooks) {
    hooks.beforeEach(function () {
      server.put('/mock-server/preferences/:id', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                title: 'Some error',
                description: 'some message',
                status: 400,
              },
            ],
          }),
        ];
      });

      server.testData.storage.addPreference(
        'locationServices.maskingPrecision',
        ''
      );
    });

    it('should show a modal with the error', async function () {

      authenticateSession();

      await visit('/admin/location-services');
      expect(currentURL()).to.equal('/admin/location-services');

      this.element.querySelector('select').value = '1';
      await triggerEvent('select', 'change');

      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));
      await Modal.waitToDisplay(4000);

      expect(
        document.querySelector('.modal-title').textContent.trim()
      ).to.equal('Some error');
      expect(document.querySelector('.modal-body').textContent.trim()).to.equal(
        'some message'
      );

      await click('.btn-resolve');
      await Modal.waitToHide(4000);
    });
  });

  describe ('successful put request',  function (hooks) {
    let receivedPreference;

    hooks.beforeEach(function () {
      server.put('/mock-server/preferences/:id', (request) => {
        receivedPreference = JSON.parse(request.requestBody);
        receivedPreference.preference._id = receivedPreference.preference.name;

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedPreference),
        ];
      });

      server.testData.storage.addPreference(
        'locationServices.maskingPrecision',
        ''
      );
    });

    it('Changing the masking precision should enable the save', async function () {
      authenticateSession();

      await visit('/admin/location-services');
      expect(currentURL()).to.equal('/admin/location-services');

      this.element.querySelector('select').value = '2';
      await triggerEvent('select', 'change');

      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'locationServices.maskingPrecision', _id: 'locationServices.maskingPrecision', value: '2' },
      });
    });
  });
});
