export { A } from 'models/lib/array';
import { A as Arr } from 'models/lib/array';

export function uniqBy(list, field) {
  const set = Array.from(new Set(list.map((obj) => obj[field])));

  return Arr(set.map((id) => list.find((obj) => obj[field] === id)));
}

export function without(list, value) {
  return Arr(list.filter((a) => a != value));
}
