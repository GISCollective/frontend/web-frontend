/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/menu', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let pictureData;
  let store;
  let page;
  let space;
  let spaceData;
  let picture;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIconSet();
    pictureData = server.testData.storage.addDefaultPicture();
    page = server.testData.storage.addDefaultPage();
    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    const spaceService = this.owner.lookup('service:store');
    picture = await store.findRecord('picture', pictureData._id);

    space = await this.store.findRecord('space', spaceData._id);

    space.logo = picture;
    space.landingPageId = page._id;
    this.set('space', space);

    space._getPagesMap = {
      [page._id]: page.slug,
    };
    spaceService.currentSpace = space;
  });

  it('renders the logo with the provided size', async function (a) {
    this.set('value', {
      data: {
        showLogo: true,
        logo: {
          style: {
            lg: { height: '12px' },
            md: { height: '12px' },
            sm: { height: '12px' },
          },
        },
      },
    });

    this.set('space', space);

    await render(
      hbs`<View::Menu @value={{this.value}} @space={{this.space}} />`,
    );

    expect(this.element.querySelector('.logo').offsetHeight).to.equal(12);
  });

  it('renders a link to the homepage on the logo', async function (a) {
    this.set('value', {
      data: {
        showLogo: true,
        style: {
          lg: { height: '12px' },
          md: { height: '12px' },
          sm: { height: '12px' },
        },
      },
    });

    await render(
      hbs`<View::Menu @value={{this.value}} @space={{this.space}} />`,
    );

    expect(this.element.querySelector('.logo-link')).to.have.attribute(
      'href',
      '/000000000000000000000001',
    );
  });

  it('renders the loading spinner as big as the logo', async function (a) {
    await store.findRecord('picture', pictureData._id);
    const loading = this.owner.lookup('service:loading');

    this.set('value', {
      data: {
        showLogo: true,
        logo: {
          style: {
            lg: { height: '62px' },
            md: { height: '62px' },
            sm: { height: '62px' },
          },
        },
      },
    });

    this.set('space', space);

    await render(
      hbs`<View::Menu @value={{this.value}} @space={{this.space}} />`,
    );

    setTimeout(() => {
      loading.isLoading = true;
    }, 500);

    await waitFor('.spinner-container');

    expect(
      this.element.querySelector('.spinner-container').offsetHeight,
    ).to.equal(16);
    expect(
      this.element.querySelector('.spinner-container').offsetWidth,
    ).to.equal(16);

    loading.isLoading = false;
  });
});
