/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | filters/issues',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Filters::Issues />`);
    expect(this.element.textContent.trim()).to.equal('with issues');
  });

  it('triggers the onChange action with true when the button is pressed', async function () {
    let newValue;
    this.set('onChange', function (value) {
      newValue = value;
    });

    await render(hbs`<Filters::Issues @onChange={{this.onChange}}/>`);
    await click('.btn-pill-with-issues');

    expect(newValue).to.equal('true');
  });

  it('triggers the onChange action with null when the reset button is pressed', async function () {
    let newValue;
    this.set('onChange', function (value) {
      newValue = value;
    });

    await render(
      hbs`<Filters::Issues @onChange={{this.onChange}} @value="true"/>`
    );
    await click('.btn-reset');

    expect(newValue).to.equal(null);
  });
});
