/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { helper } from '@ember/component/helper';
import {
  isPropertyWithoutSize,
  hasBsSize,
  validBsSizes,
  withoutBsSize,
  extractProperty,
  isAnyOfBsProperty,
} from 'core/lib/bs-cols';
import { getSizeIndexFromBs } from '../lib/responsive';

export default helper(function pageColClass(positional, named) {
  let color = '';

  if (typeof positional?.toArray == 'function') {
    positional = positional.toArray();
  }

  let allClasses = Array.isArray(positional[0]) ? positional[0] : positional[0]?.classes;

  if (positional?.[0]?.color) {
    color = `text-${positional[0].color}`;
  }

  if (!Array.isArray(allClasses)) {
    return color;
  }

  if (typeof named.deviceSize != 'string') {
    return [...allClasses, color].filter((a) => a).join(' ');
  }

  let bySize = {};

  for (const size of validBsSizes) {
    const index = getSizeIndexFromBs(size);
    bySize[index] = allClasses.filter((a) => hasBsSize(a, size));
  }
  bySize[0] = allClasses.filter((a) => isPropertyWithoutSize(a));

  const sizeIndex = getSizeIndexFromBs(named.deviceSize);

  let result = [];
  let added = [];
  let existingProperties = [];
  for (let index = sizeIndex; index >= 0; index--) {
    const availableOptions = bySize[index].filter((a) => !isAnyOfBsProperty(a, existingProperties));
    result = [...result, ...availableOptions];

    const properties = availableOptions.map((a) => extractProperty(a)).filter((a) => a);
    existingProperties = [...existingProperties, ...properties];
    added = result.map((a) => withoutBsSize(a));
  }

  return [...added, color].filter((a) => a).join(' ');
});
