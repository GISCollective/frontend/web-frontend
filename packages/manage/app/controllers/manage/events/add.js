/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddCalendarController extends Controller {
  @service intl;
  @service router;
  @tracked team;

  destination = 'manage.events.edit';

  get editableModel() {
    return this.model.event;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.event.get('visibility.team');
    let calendar = this.model.event.calendar;

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    if (calendar?.get('id')) {
      pieces.push({
        route: 'manage.dashboards.calendar',
        text: calendar.get('name'),
        model: calendar.get('id'),
      });
    }

    return pieces;
  }

  @action
  async save(event, cover) {
    let result;

    if (cover) {
      result = await cover.save();
      event.cover = result;
    }

    result = await this.editableModel.save();

    this.router.transitionTo('manage.events.edit', result.id);
  }
}
