/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from "qunit";
import { setupRenderingTest, it } from "dummy/tests/helpers";
import { render, triggerEvent, click, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

module("Integration | Component | editor/list", function (hooks) {
  let changedValue;
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders the selected text style values", async function () {
    this.set("value", {
      data: {
        textStyle: {
          classes: ["text-center"],
          color: "green",
        },
      },
    });

    await render(hbs`<Editor::List @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".text-style .text-color-selector .selected-color").textContent.trim()).to.equal(
      "green",
    );
    expect(this.element.querySelector(".text-style select.input-sm-text").value).to.equal("center");
  });

  it("can change the text style values", async function () {
    this.set("value", {
      data: {},
    });

    await render(hbs`<Editor::List @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input--ff").value = "monospace";
    await triggerEvent(".input--ff", "change");

    expect(changedValue.textStyle).to.deep.equal({
      color: "",
      classes: ["ff-monospace"],
      minLines: { lg: 0, md: 0, sm: 0 },
    });
  });

  it("renders the text list values", async function () {
    this.set("value", {
      data: {
        textList: ["category 1", "category 2"],
        textStyle: {
          classes: ["text-center"],
          color: "green",
        },
        icon: {
          color: "red",
          name: "check",
          size: "2",
        },
      },
    });

    await render(hbs`<Editor::List @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".text-list .text-value-0").value).to.equal("category 1");
    expect(this.element.querySelector(".text-list .text-value-1").value).to.equal("category 2");
  });

  it("can change the text list values", async function () {
    this.set("value", {
      data: {},
    });

    await render(hbs`<Editor::List @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".text-list .btn-add");
    await fillIn(".text-list .text-value-0", "new category");

    expect(changedValue.textList).to.deep.equal(["new category"]);
  });

  it("renders the icon values", async function () {
    this.set("value", {
      data: {
        iconStyle: {
          color: "red",
          name: "check",
          size: "2",
        },
      },
    });

    await render(hbs`<Editor::List @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".icon-style .selected-color").textContent.trim()).to.equal("red");
    expect(this.element.querySelector(".icon-style select.name").value).to.equal("check");
    expect(this.element.querySelector(".icon-style select.size").value).to.equal("2");
  });

  it("can change the icon style values", async function () {
    this.set("value", {
      data: {},
    });

    await render(hbs`<Editor::List @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".icon-style select.size").value = "3";
    await triggerEvent(".icon-style select.size", "change");

    expect(changedValue.iconStyle).to.deep.equal({ name: "circle-check", size: "3" });
  });
});
