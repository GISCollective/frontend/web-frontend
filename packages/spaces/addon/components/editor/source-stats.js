/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class EditorSourceStatsComponent extends Component {
  statsList = [
    {
      id: "published-campaigns",
      name: "published survey",
    },
    {
      id: "campaign-answer-contributors",
      name: "survey answer contributors",
    },
    {
      id: "campaign-contributions",
      name: "survey contributions",
    },
  ];

  @action
  sourceChanged(value) {
    this.args.onChange({
      model: "stat",
      id: value.id,
    });
  }
}
