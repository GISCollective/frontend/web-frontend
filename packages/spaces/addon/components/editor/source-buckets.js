/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class EditorSourceBucketsComponent extends Component {
  get preferredModel() {
    return this.args.element?.attributes?.getNamedItem?.("data-preferred-model")?.value;
  }
}
