/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputSelectComponent extends Component {
  @tracked _value;
  @tracked selectionList = [];

  get value() {
    if (this._value || this._value === 0) {
      return this._value;
    }

    if (this.args.value === 0) {
      return 0;
    }

    return this.getValueFromObject(this.args.value);
  }

  getValueFromObject(obj) {
    let value = obj || '';

    if (value) {
      if (obj.get && obj.get('id')) {
        value = this.args.value.get('id');
      }

      if (!obj.get && typeof obj.id != 'undefined') {
        value = obj.id;
      }
    }

    if (typeof obj == 'boolean') {
      value = obj;
    }

    return value;
  }

  get hasNoSelectedValues() {
    return typeof this.args.value == 'undefined' || this.args.value === null;
  }

  createSelectionList() {
    const result = [];

    if (this.hasNoSelectedValues || this.args.allowEmpty) {
      result.push({ name: '' });
    }

    if (!this.args.list || !this.args.list.length) {
      return result;
    }

    this.args.list.map((value) => {
      let name = value;
      let id = value;

      if (typeof value?.id != 'undefined' && value?.name) {
        id = value.id;
        name = value.name;
      }

      result.push({ id, name, value });
    });

    return result;
  }

  @action
  change(event) {
    this._value = event.target.value;

    this.args.onChange?.(this.getById(this._value));
  }

  getById(id) {
    return this.selectionList?.find((a) => a == id || `${a.id}` == id)?.value;
  }

  @action
  setup(element) {
    this.element = element;

    this.updateOptions();
  }

  @action
  updateOptions() {
    this.selectionList = this.createSelectionList();

    while (this.element.firstChild) {
      this.element.removeChild(this.element.lastChild);
    }

    for (var i = 0; i < this.selectionList.length; i++) {
      var option = document.createElement('option');

      if (this.selectionList[i].id !== undefined) {
        option.value = this.selectionList[i].id;
      }

      option.text = this.selectionList[i].name;
      this.element.appendChild(option);
    }

    this.element.value = this.value;
  }
}
