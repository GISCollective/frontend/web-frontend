/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/template';

export default class TeamsGalleryController extends Controller {
  @tracked index;

  get arePicturesLoaded() {
    return this.model.pictures.isLoaded;
  }

  get currentSlide() {
    return this.index;
  }

  set currentSlide(val) {
    this.index = val;
  }

  pictures() {
    return this.model.pictures.map((a) => htmlSafe(`background-image: url("${a.picture}")`));
  }
}
