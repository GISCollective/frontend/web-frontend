/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ManageLanguagesAddController from './add';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageLanguagesEditController extends ManageLanguagesAddController {
  @service intl;

  get breadcrumbs() {
    return [
      {
        route: 'manage.languages',
        text: this.intl.t('languages'),
        capitalize: true,
      },
      { text: this.intl.t('edit-model.title', { name: this.model.name }) },
    ];
  }

  @action
  onChangeCustomTranslations(value) {
    this.model.customTranslations = value;
  }
}
