import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | manage/import-emails', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };
  });

  it('renders the file picker by default', async function (assert) {
    await render(hbs`<Manage::ImportEmails />`);

    expect(this.element.querySelector('.col-file-upload')).to.have.class('selected');
    expect(this.element.querySelector('.chk-file-upload').checked).to.equal(true);

    expect(this.element.querySelector('.col-copy-paste')).not.to.have.class('selected');
    expect(this.element.querySelector('.chk-copy-paste').checked).to.equal(false);

    expect(this.element.querySelector('.input-text-file')).to.exist;
    expect(this.element.querySelector('.input-copy-paste')).not.to.exist;
  });

  it('shows the copy paste input when the radio is pressed', async function (assert) {
    await render(hbs`<Manage::ImportEmails />`);

    await click('.chk-copy-paste');

    expect(this.element.querySelector('.col-copy-paste')).to.have.class('selected');
    expect(this.element.querySelector('.chk-copy-paste').checked).to.equal(true);

    expect(this.element.querySelector('.col-file-upload')).not.to.have.class('selected');
    expect(this.element.querySelector('.chk-file-upload').checked).to.equal(false);

    expect(this.element.querySelector('.input-text-file')).not.to.exist;
    expect(this.element.querySelector('.input-copy-paste')).to.exist;
  });

  it('shows the copy paste input when the col is pressed', async function (assert) {
    await render(hbs`<Manage::ImportEmails />`);

    await click('.col-copy-paste');

    expect(this.element.querySelector('.col-copy-paste')).to.have.class('selected');
    expect(this.element.querySelector('.chk-copy-paste').checked).to.equal(true);

    expect(this.element.querySelector('.col-file-upload')).not.to.have.class('selected');
    expect(this.element.querySelector('.chk-file-upload').checked).to.equal(false);

    expect(this.element.querySelector('.input-text-file')).not.to.exist;
    expect(this.element.querySelector('.input-copy-paste')).to.exist;
  });

  it('shows the upload input when the radio is pressed', async function (assert) {
    await render(hbs`<Manage::ImportEmails />`);

    await click('.chk-copy-paste');
    await click('.chk-file-upload');

    expect(this.element.querySelector('.col-copy-paste')).not.to.have.class('selected');
    expect(this.element.querySelector('.chk-copy-paste').checked).to.equal(false);

    expect(this.element.querySelector('.col-file-upload')).to.have.class('selected');
    expect(this.element.querySelector('.chk-file-upload').checked).to.equal(true);

    expect(this.element.querySelector('.input-text-file')).to.exist;
    expect(this.element.querySelector('.input-copy-paste')).not.to.exist;
  });

  it('shows the upload input when the col is pressed', async function (assert) {
    await render(hbs`<Manage::ImportEmails />`);

    await click('.chk-copy-paste');
    await click('.col-file-upload');

    expect(this.element.querySelector('.col-copy-paste')).not.to.have.class('selected');
    expect(this.element.querySelector('.chk-copy-paste').checked).to.equal(false);

    expect(this.element.querySelector('.col-file-upload')).to.have.class('selected');
    expect(this.element.querySelector('.chk-file-upload').checked).to.equal(true);

    expect(this.element.querySelector('.input-text-file')).to.exist;
    expect(this.element.querySelector('.input-copy-paste')).not.to.exist;
  });

  it('can import a file', async function (assert) {
    let value;
    let replace;

    this.set('newsletter', {
      importEmails: (v, r) => {
        value = v;
        replace = r;
      },
    });

    await render(hbs`<Manage::ImportEmails @newsletter={{this.newsletter}} />`);

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');

    let utf8Encode = new TextEncoder();
    let byteArray = utf8Encode.encode('a@b.c');
    let blob = new Blob([byteArray], { type: 'text/plain' });
    blob.name = 'test.txt';

    await triggerEvent('input[type=file]', 'change', { files: [blob] });

    await click('.btn-submit');

    expect(value).to.equal('a@b.c');
    expect(replace).to.equal(false);
  });

  it('can import a file and replace the emails', async function (assert) {
    let value;
    let replace;

    this.set('newsletter', {
      importEmails: (v, r) => {
        value = v;
        replace = r;
      },
    });

    await render(hbs`<Manage::ImportEmails @newsletter={{this.newsletter}} />`);

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');

    let utf8Encode = new TextEncoder();
    let byteArray = utf8Encode.encode('a@b.c');
    let blob = new Blob([byteArray], { type: 'text/plain' });
    blob.name = 'test.txt';

    await triggerEvent('input[type=file]', 'change', { files: [blob] });

    await click('.chk-replace');

    await click('.btn-submit');

    expect(value).to.equal('a@b.c');
    expect(replace).to.equal(true);
  });

  it('can import a text value', async function (assert) {
    let value;
    let replace;

    this.set('newsletter', {
      importEmails: (v, r) => {
        value = v;
        replace = r;
      },
    });

    await render(hbs`<Manage::ImportEmails @newsletter={{this.newsletter}} />`);

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');

    await click('.label-copy-paste');

    await fillIn('.input-copy-paste', 'a@b.c,c@d.e');

    await click('.btn-submit');

    expect(value).to.deep.equal(['a@b.c', 'c@d.e']);
    expect(replace).to.equal(false);
  });
});
