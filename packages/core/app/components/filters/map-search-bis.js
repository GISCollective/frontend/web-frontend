/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later, cancel } from '@ember/runloop';
import { service } from '@ember/service';

export default class FiltersMapSearchBisComponent extends Component {
  @service clickOutside;

  @tracked isHiding = false;

  get term() {
    return this.args.term ?? '';
  }

  set term(value) {
    this.args.onChange?.(value);
  }

  get isVisible() {
    if (this.isHiding) {
      return false;
    }

    return this.args.isActive;
  }

  @action
  setupSearchContainer(element) {
    this.searchContainer = element;
  }

  @action
  setupComponentContainer(element) {
    this.componentContainer = element;
  }

  @action
  inputFocus() {
    this.componentContainer.querySelector('.input-search').focus();

    this.clickOutside.subscribe(this.componentContainer, async () => {
      try {
        await this.args.onDeactivate?.();
      } catch (err) {
        console.error('ignoring deactivate error', err);
      }
    });

    cancel(this.blurTimer);

    this.args.onActivate?.();
  }

  @action
  hideContent() {
    this.isHiding = true;

    return new Promise((resolve, reject) => {
      if (this.hideEvent) {
        return reject(new Error('There is already a running close event!'));
      }

      this.hideEvent = later(() => {
        resolve();

        this.isHiding = false;
        this.hideEvent = null;
      }, 500);
    });
  }

  @action
  setupContent(element) {
    this.elementContent = element;
  }

  @action
  async close() {
    await this.args.onChange?.('', true);

    try {
      await this.args.onDeactivate?.();
    } catch (err) {
      console.error('ignoring deactivate error', err);
    }

    await this.hideContent();
  }

  @action
  backToSearch() {
    this.args.onBackToSearch?.();
  }

  @action
  filterIcons() {
    return this.args.onFilterIcons?.();
  }

  @action
  async backToResults() {
    await this.args.onBackToResults?.();
  }
}
