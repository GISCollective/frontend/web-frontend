/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class AdminUsersIndexController extends Controller {
  queryParams = ['search'];
  @tracked search = '';
  @service user;

  get csvLink() {
    return `${this.user.apiUrl}/users?format=csv`;
  }

  @action
  performSearch(value) {
    this.search = value;
  }
}
