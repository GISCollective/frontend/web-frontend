/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | sheet/title',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders empty string when the title is not set', async function () {
    await render(hbs`<Sheet::Title />`);
    expect(
      this.element.querySelector('.sheet-title').textContent.trim()
    ).to.equal('');
  });

  it('renders the title when is set', async function () {
    await render(hbs`<Sheet::Title @title="some title"/>`);
    expect(
      this.element.querySelector('.sheet-title').textContent.trim()
    ).to.equal('some title');
  });
});
