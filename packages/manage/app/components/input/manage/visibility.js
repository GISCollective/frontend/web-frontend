/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageVisibilityComponent extends Component {
  @service intl;
  @tracked _value;

  get values() {
    return [
      { id: '0', name: this.intl.t('visibility-private') },
      { id: '1', name: this.intl.t('visibility-public') },
      { id: '-1', name: this.intl.t('visibility-pending') },
    ];
  }

  get value() {
    return `${this._value || this.args.value || 0}`;
  }

  @action
  change(value) {
    this._value = value.id;
  }

  @action
  save() {
    const result = this.args.onSave?.(parseInt(this.value));
    this._value = undefined;

    return result;
  }

  @action
  cancel() {
    const result = this.args.onCancel?.();
    this._value = undefined;

    return result;
  }
}
