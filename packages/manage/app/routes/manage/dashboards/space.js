/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

class SpaceModels {
  @tracked space;
  @tracked team;
  @tracked pages;

  constructor(data) {
    this.space = data.space;
    this.team = data.team;
    this.pages = data.pages;
  }
}

export default class ManageDashboardsSpaceRoute extends AuthenticatedRoute {
  async model(params) {
    const space = await this.store.findRecord('space', params.id);
    const team = space.visibility.fetchTeam();

    const data = await hash({
      space,
      team,
      pages: this.store.query('page', { space: params.id }),
    });

    return new SpaceModels(data);
  }
}
