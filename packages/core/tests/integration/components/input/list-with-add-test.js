/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { expect } from 'chai';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | input/list-with-add', function (hooks) {
  setupRenderingTest(hooks);
  let value;

  hooks.beforeEach(function () {
    value = undefined;
    this.set('change', function (v) {
      value = v;
    });
  });

  it('renders an add button when there is no value', async function (assert) {
    await render(hbs`<Input::ListWithAdd @onChange={{this.change}} />`);

    expect(this.element.querySelector('.btn-add')).to.exist;
  });

  it('renders the value when there is a value', async function (assert) {
    this.set('value', ['value']);

    await render(hbs`<Input::ListWithAdd @value={{this.value}} @onChange={{this.change}} as |value|>
      <span class="value">{{value}}</span>
    </Input::ListWithAdd>`);

    expect(this.element.querySelector('.value').textContent.trim()).to.equal('value');
    expect(this.element.querySelector('li')).not.to.exist;
  });

  it('renders 2 values when there are 2 values', async function (assert) {
    this.set('value', ['value1', 'value2']);

    await render(hbs`<Input::ListWithAdd @value={{this.value}} @onChange={{this.change}} as |value|>
      <span class="value">{{value}}</span>
    </Input::ListWithAdd>`);

    const values = this.element.querySelectorAll('.value');

    expect(values[0].textContent.trim()).to.equal('value1');
    expect(values[1].textContent.trim()).to.equal('value2');

    expect(this.element.querySelectorAll('li')).to.have.length(2);
  });

  it('can delete a value', async function (a) {
    this.set('value', ['value1', 'value2']);

    await render(hbs`<Input::ListWithAdd @value={{this.value}} @onChange={{this.change}} as |value|>
      <span class="value">{{value}}</span>
    </Input::ListWithAdd>`);

    await click('.btn-delete');

    const values = this.element.querySelectorAll('.value');
    expect(values[0].textContent.trim()).to.equal('value2');

    a.deepEqual(value, ['value2']);
  });

  it('can add a value', async function (a) {
    this.set('value', ['value1', 'value2']);

    await render(hbs`<Input::ListWithAdd @value={{this.value}} @onChange={{this.change}} as |value|>
      <span class="value">{{value}}</span>
    </Input::ListWithAdd>`);

    await click('.btn-add');

    const values = this.element.querySelectorAll('.value');
    expect(values[0].textContent.trim()).to.equal('value1');
    expect(values[1].textContent.trim()).to.equal('value2');
    expect(values[2].textContent.trim()).to.equal('');

    a.deepEqual(value, ['value1', 'value2', undefined]);
  });

  it('calls the factory value when a new item is added', async function (a) {
    this.set('value', ['value1', 'value2']);

    this.set('factoryValue', () => 'new value');

    await render(hbs`<Input::ListWithAdd @value={{this.value}} @factoryValue={{this.factoryValue}} @onChange={{this.change}} as |value|>
      <span class="value">{{value}}</span>
    </Input::ListWithAdd>`);

    await click('.btn-add');

    const values = this.element.querySelectorAll('.value');
    expect(values[0].textContent.trim()).to.equal('value1');
    expect(values[1].textContent.trim()).to.equal('value2');
    expect(values[2].textContent.trim()).to.equal('new value');

    a.deepEqual(value, ['value1', 'value2', 'new value']);
  });
});
