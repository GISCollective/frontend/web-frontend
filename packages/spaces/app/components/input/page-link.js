/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { service } from "@ember/service";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class InputPageLinkComponent extends Component {
  @service store;
  @tracked internalPages;
  @tracked isExternal;
  elementId = "input-page-link-" + guidFor(this);

  getPath(slug) {
    return `/` + slug.split("--").join("/");
  }

  get value() {
    return this.pages.find((a) => a.path == this.args.value);
  }

  get textValue() {
    return this.args.value ?? "";
  }

  set textValue(value) {
    this.isExternal = true;
    this.args.onChange?.(value);
  }

  get pages() {
    let pageList =
      this.internalPages?.map((a) => ({
        id: a.id,
        path: this.getPath(a.slug),
        name: `${this.getPath(a.slug)}`,
      })) ?? [];

    return pageList.sort((a, b) => a.path.localeCompare(b.path));
  }

  get hasExternalLink() {
    if (this.isExternal === true || this.isExternal === false) {
      return this.isExternal;
    }

    const value = this.args.value ?? "";

    return value.indexOf?.("http") == 0;
  }

  set hasExternalLink(value) {
    this.isExternal = value;
  }

  @action
  async setup() {
    const pages = (await this.args.space?.getPagesMap(this.args.space.id)) ?? {};

    this.internalPages = Object.keys(pages).map((slug) => ({
      id: pages[slug],
      slug,
    }));
  }

  @action
  change(value) {
    this.args.onChange?.(value?.path);
  }
}
