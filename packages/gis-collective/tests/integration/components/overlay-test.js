/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | overlay',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`
      <Overlay>
        template block text
      </Overlay>
    `);

    expect(this.element.querySelector('.content').textContent.trim()).to.equal(
      'template block text'
    );
  });

  it('triggers the onClose event when the btn-close button is pressed', async function () {
    let called;

    this.set('close', () => {
      called = true;
    });

    await render(hbs`
      <Overlay @onClose={{this.close}} />
    `);

    await click('.btn-modal-close');

    expect(called).to.equal(true);
  });
});
