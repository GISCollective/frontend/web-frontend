/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, fillIn, waitUntil } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/teams/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedTeam;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultSpace();

    server.post('/mock-server/teams', (request) => {
      receivedTeam = JSON.parse(request.requestBody);
      receivedTeam.team['_id'] = '5ca78e2160780601008f69e6';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedTeam)];
    });

    receivedTeam = null;
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit('/manage/teams/add');
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fteams%2Fadd');
  });

  it('can visit /manage/teams/add', async function () {
    authenticateSession();
    await visit('/manage/teams/add');
    await PageElements.waitEditorJs(this.element);

    expect(currentURL()).to.equal('/manage/teams/add');
    expect(this.element.querySelector('h1').textContent.trim()).to.equal('New team');

    expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
    expect(this.element.querySelector('.row-name input.form-control')).to.exist;

    expect(this.element.querySelector('.row-about label').textContent.trim()).to.contain('About');
    expect(this.element.querySelector('.row-about .editor-js')).to.exist;
    expect(this.element.querySelector('.ce-paragraph').textContent).to.contain(
      'This is our team, dedicated to map all our projects.',
    );

    expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();
    await visit('/manage/teams/add');

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'New team']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);
  });

  it('can create a team', async function (a) {
    authenticateSession();
    await visit('/manage/teams/add');
    await PageElements.waitEditorJs(this.element);

    expect(currentURL()).to.equal('/manage/teams/add');
    await fillIn('.row-name input.form-control', 'test name');
    await fillIn('.ce-paragraph', 'test description');

    await PageElements.wait(1000);

    await click('.btn-submit');
    await PageElements.waitEditorJs(this.element);

    await waitUntil(() => receivedTeam);

    a.deepEqual(receivedTeam.team.about.blocks, [
      { type: 'header', data: { text: 'test name', level: 1 } },
      { type: 'paragraph', data: { text: 'test description' } },
    ]);

    a.deepEqual(receivedTeam, {
      team: {
        _id: '5ca78e2160780601008f69e6',
        about: {
          blocks: [
            { type: 'header', data: { text: 'test name', level: 1 } },
            { type: 'paragraph', data: { text: 'test description' } },
          ],
        },
        guests: [],
        isPublic: false,
        isPublisher: false,
        leaders: [],
        logo: null,
        members: [],
        categories: [],
        name: 'test name',
        allowCustomDataBindings: false,
        allowNewsletters: false,
        allowEvents: false,
        allowReports: false,
        owners: [],
        invitations: [],
        contactEmail: "",
        socialMediaLinks: {}
      },
    });

    expect(currentURL()).to.equal(`/manage/teams/${receivedTeam.team._id}`);
  });

  it('has the submit button disabled when there is no name', async function () {
    authenticateSession();
    await visit('/manage/teams/add');
    await PageElements.waitEditorJs(this.element);

    expect(currentURL()).to.equal('/manage/teams/add');

    expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');
  });

  it('redirects the user to the route provided as query param value if the `next` query param is present', async function () {
    authenticateSession();
    await visit('/manage/teams/add?next=manage.maps.add');
    await PageElements.waitEditorJs(this.element);

    await fillIn('.row-name input.form-control', 'test name');
    await click('.btn-submit');
    await PageElements.waitEditorJs(this.element);

    await waitUntil(() => receivedTeam);

    expect(currentURL()).to.equal(`/manage/maps/add`);
  });
});
