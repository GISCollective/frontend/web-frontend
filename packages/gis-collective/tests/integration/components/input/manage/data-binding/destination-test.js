/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | input/manage/data-binding/destination',  function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let team;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultMap('1');
    server.testData.storage.addDefaultMap('2');
    team = server.testData.storage.addDefaultTeam();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe ('in view mode',  function (hooks) {
    it('renders a message when the value is not set', async function () {
      await render(
        hbs`<Input::Manage::DataBinding::Destination @title="destination" />`
      );

      expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
        'The destination is not set.'
      );
    });

    it('renders a map when the destination is a map', async function () {
      this.set('value', {
        type: 'Map',
        modelId: '1',
      });

      await render(
        hbs`<Input::Manage::DataBinding::Destination @title="destination" @value={{this.value}}/>`
      );

      expect(
        this.element.querySelector('.btn-map-link').textContent.trim()
      ).to.equal('Harta Verde București');
    });
  });

  describe ('in edit mode',  function (hooks) {
    it('renders the selected map', async function () {
      this.set('value', {
        type: 'Map',
        modelId: '1',
      });

      team.id = team._id;
      this.set('team', team);

      await render(
        hbs`<Input::Manage::DataBinding::Destination @title="destination" @editablePanel="destination" @team={{this.team}} @value={{this.value}}/>`
      );

      expect(this.element.querySelector('.select-map').value).to.equal('1');
    });

    it('can change the selected map', async function () {
      let value;
      this.set('value', {
        type: 'Map',
        modelId: '1',
      });

      team.id = team._id;
      this.set('team', team);

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Manage::DataBinding::Destination  @onSave={{this.save}} @title="destination" @editablePanel="destination" @team={{this.team}} @value={{this.value}}/>`
      );

      this.element.querySelector('.select-map').value = '2';
      await triggerEvent('.select-map', 'change');

      await click('.btn-submit');

      expect(value.toJSON()).to.deep.equal({
        type: 'Map',
        modelId: '2',
        deleteNonSyncedRecords: false,
      });
    });
  });
});
