/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/map/embed-code', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    const space = this.owner.lookup('service:space');
    space.currentSpace = { domain: 'giscollective.com' };
  });

  it('renders', async function () {
    this.set('map', {
      id: 1,
      name: 'name',
    });

    await render(hbs`<Manage::EmbedMapCode @map={{this.map}} />`);

    const code = this.element
      .querySelector('textarea')
      .textContent.replace('\n', '')
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a)
      .join(' ');

    expect(code).to.equal(
      `<iframe src="https://giscollective.com/browse/maps/1/map-view?embed=true&mapInfo=true&showMore=true&filterManyIcons=false&allIcons=false" allowFullScreen="true" allow="fullscreen; geolocation;" title="name" frameborder="0" width="100%" height="500"></iframe>`,
    );
  });

  it('allows toggling "enable the show more button" option', async function () {
    this.set('map', {
      id: 1,
      name: 'name',
    });

    await render(hbs`<Manage::EmbedMapCode @map={{this.map}} />`);

    await click('.chk-show-more');

    const code = this.element
      .querySelector('textarea')
      .textContent.replace('\n', '')
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a)
      .join(' ');

    expect(code).to.equal(
      `<iframe src="https://giscollective.com/browse/maps/1/map-view?embed=true&mapInfo=true&showMore=false&filterManyIcons=false&allIcons=false" allowFullScreen="true" allow="fullscreen; geolocation;" title="name" frameborder="0" width="100%" height="500"></iframe>`,
    );
  });

  it('allows toggling "filter with many icons" option', async function () {
    this.set('map', {
      id: 1,
      name: 'name',
    });

    await render(hbs`<Manage::EmbedMapCode @map={{this.map}} />`);

    await click('.chk-filter-many-icons');

    const code = this.element
      .querySelector('textarea')
      .textContent.replace('\n', '')
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a)
      .join(' ');

    expect(code).to.equal(
      `<iframe src="https://giscollective.com/browse/maps/1/map-view?embed=true&mapInfo=true&showMore=true&filterManyIcons=true&allIcons=false" allowFullScreen="true" allow="fullscreen; geolocation;" title="name" frameborder="0" width="100%" height="500"></iframe>`,
    );
  });

  it('allows toggling "show the map info box" option', async function () {
    this.set('map', {
      id: 1,
      name: 'name',
    });

    await render(hbs`<Manage::EmbedMapCode @map={{this.map}} />`);

    await click('.chk-show-map-info');

    const code = this.element
      .querySelector('textarea')
      .textContent.replace('\n', '')
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a)
      .join(' ');

    expect(code).to.equal(
      `<iframe src="https://giscollective.com/browse/maps/1/map-view?embed=true&mapInfo=false&showMore=true&filterManyIcons=false&allIcons=false" allowFullScreen="true" allow="fullscreen; geolocation;" title="name" frameborder="0" width="100%" height="500"></iframe>`,
    );
  });

  it('allows toggling "show all icons" option', async function () {
    this.set('map', {
      id: 1,
      name: 'name',
    });

    await render(hbs`<Manage::EmbedMapCode @map={{this.map}} />`);

    await click('.chk-show-all-icons');

    const code = this.element
      .querySelector('textarea')
      .textContent.replace('\n', '')
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a)
      .join(' ');

    expect(code).to.equal(
      `<iframe src="https://giscollective.com/browse/maps/1/map-view?embed=true&mapInfo=true&showMore=true&filterManyIcons=false&allIcons=true" allowFullScreen="true" allow="fullscreen; geolocation;" title="name" frameborder="0" width="100%" height="500"></iframe>`,
    );
  });
});
