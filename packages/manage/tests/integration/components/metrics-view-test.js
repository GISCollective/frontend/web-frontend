import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | metrics-view', function (hooks) {
  let server;
  let metrics;

  setupRenderingTest(hooks);

  hooks.before(function () {
    server = new TestServer();

    metrics = [];

    for (let i = 1; i < 10; i++) {
      metrics.push({
        _id: `${i}`,
        name: '1',
        reporter: '',
        labels: {},
        time: `2020-12-0${i}T00:00:00Z`,
        type: 'mapView',
        value: i,
      });
    }
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no metric', async function (assert) {
    await render(hbs`<MetricsView />`);

    assert.dom().hasText('');
  });

  it('renders the value', async function (assert) {
    this.set('metrics', metrics);

    await render(
      hbs`<MetricsView @name="1" @type="mapView" @value={{this.metrics}} @start="2020-12-01T00:00:00Z" @end="2020-12-09T00:00:00Z" />`,
    );

    const canvas = this.element.querySelector('canvas');

    const meta = canvas.chart.getDatasetMeta(0);

    expect(meta._dataset.data).to.deep.equal([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });

  it('fills in the gaps with 0 values', async function (assert) {
    this.set('metrics', [
      {
        _id: `1`,
        name: '1',
        reporter: '',
        labels: {},
        time: `2020-12-01T00:00:00Z`,
        type: 'mapView',
        value: 1,
      },
      {
        _id: `2`,
        name: '1',
        reporter: '',
        labels: {},
        time: `2020-12-09T00:00:00Z`,
        type: 'mapView',
        value: 9,
      },
    ]);

    await render(
      hbs`<MetricsView @name="1" @type="mapView" @value={{this.metrics}} @start="2020-12-01T00:00:00Z" @end="2020-12-09T00:00:00Z" />`,
    );

    const canvas = this.element.querySelector('canvas');

    const meta = canvas.chart.getDatasetMeta(0);

    expect(meta._dataset.data).to.deep.equal([1, 0, 0, 0, 0, 0, 0, 0, 9]);
  });
});
