/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { set, action } from '@ember/object';
import { toContentBlocks } from 'models/lib/content-blocks';
import { tracked } from '@glimmer/tracking';
import { addIconGroupToStore } from '../../lib/icons';
import { onPictureUpload } from 'core/lib/editor-js-picture';

export default class NewRecordFormEventComponent extends Component {
  @service intl;
  @service store;

  @tracked canSubmit;
  @tracked cover;
  @tracked icons;
  @tracked iconSets;
  @tracked iconSetModels;

  pictures = [];

  get recordName() {
    return this.args.event?.get('calendar.recordName') ?? 'event';
  }

  get namePlaceholder() {
    return this.intl.t('what is the event name?', { name: this.recordName });
  }

  get breadcrumbs() {
    const result = [
      ...(this.args.breadcrumbs ?? []),
      {
        text: this.title,
        capitalize: true,
      },
    ];

    return result;
  }

  get title() {
    return this.intl.t('new record', { name: this.recordName });
  }

  get article() {
    const value = this.intl.t('article-placeholders.add-event', { name: this.recordName });

    return toContentBlocks(value);
  }

  get isSchedule() {
    return this.args.event?.calendar?.get('type') == 'schedules';
  }

  get showCalendar() {
    if (!this.args.event?.calendar) {
      return true;
    }

    return !this.args.hideCalendar;
  }

  updateCanSubmit() {
    const model = this.args.event;

    this.canSubmit = model?.name?.trim?.() && model?.visibility?.team && model.calendar;
  }

  @action
  onPictureUpload(value, type) {
    return onPictureUpload(value, type, this.store, {}, false, (picture) => {
      this.pictures.push(picture);
    });
  }

  @action
  async updateIcons() {
    this.iconSets = this.args.event?.get?.('calendar.iconSets.idList') ?? [];
    this.iconSetModels = await this.args.event?.get?.('calendar.iconSets')?.fetch();
  }

  @action
  changeCover(index, value) {
    return this.changeField('cover', value);
  }

  @action
  async changeField(name, value) {
    if (!this.args.event) {
      return;
    }

    set(this.args.event, name, value);

    if (name == 'calendar' && value?.visibility) {
      this.args.event.visibility = value.visibility;
      await this.updateIcons();
    }

    if (name == 'cover') {
      this.cover = value;
    }

    this.updateCanSubmit();
  }

  @action
  changeAttributes(key, value) {
    if (!this.args.event.attributes) {
      this.args.event.attributes = {};
    }

    this.args.event.attributes[key] = value;
  }

  @action
  changeArticle(title, body) {
    return this.changeField('article', body);
  }

  @action
  async setup() {
    await this.updateIcons();
    return this.changeField('article', this.article);
  }

  @action
  async save() {
    const event = await this.args.onSubmit(this.args.event, this.cover);

    const allPictures = [this.cover, ...this.pictures].filter((a) => a);

    for (const picture of allPictures) {
      picture.meta.link.model = 'Event';
      picture.meta.link.modelId = event.id;
    }

    await Promise.all(allPictures.map((a) => a.save()));
  }
}
