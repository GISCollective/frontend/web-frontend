/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageIconsAddSetRoute extends AuthenticatedRoute {
  model() {
    if (this.isFastBoot) {
      return;
    }

    this.modelsToClear = ['iconSet'];

    const teamParams = {
      edit: true,
      all: false,
    };

    const iconSet = this.store.createRecord('iconSet', {
      name: '',
      description: '',
      visibility: {
        team: null,
        isPublic: false,
      },
    });

    return hash({
      teams: this.store.query('team', teamParams),
      iconSet: iconSet,
    });
  }
}
