/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { fromPageState, toPageState } from 'core/lib/page-state';

export default class PageColCategoriesComponent extends Component {
  get path() {
    const path = this.args.path ?? '';

    if (path.indexOf('.data') != -1) {
      return path;
    }

    return path + '.data';
  }

  get state() {
    return fromPageState(this.args.state);
  }

  get stateKey() {
    return this.args.stateKey ?? 'cn';
  }

  get stateValue() {
    return this.state[this.stateKey] ?? '';
  }

  get selectedGroup() {
    if (this.args.isEditor) {
      return this.categories?.[0]?.toLowerCase();
    }

    if (this.stateValue) {
      return this.stateValue.toLowerCase();
    }

    const modelCategories = Object.values(this.args.model ?? {})
      .map((a) => a?.query?.category)
      .filter((a) => a)
      .map((a) => a.toLowerCase());

    for (const category of this.categories.map((a) => a?.toLowerCase())) {
      if (modelCategories.includes(category)) {
        return category;
      }
    }
  }

  get value() {
    return this.args.value?.data ?? this.args.value ?? {};
  }

  get categories() {
    return this.value.categoryList ?? [];
  }

  get valueName() {
    return this.args.name ?? this.args.value.name;
  }

  get valueGid() {
    return this.args.gid ?? this.args.value.gid;
  }

  @action
  setup() {
    if (this.selectedGroup || this.args.skipSetup) {
      return;
    }

    if (!this.value.defaultCategory?.value) {
      return;
    }

    this.selectGroup(this.value.defaultCategory.value);
  }

  @action
  selectGroup(name) {
    if (this.args.isEditor) {
      return false;
    }

    const state = this.state;

    if (state[this.stateKey] == name) {
      return;
    }

    state[this.stateKey] = name;

    later(() => {
      this.args.onChange?.(name);
      this.args.onChangeState?.(toPageState(state));
    });
  }
}
