import { module, test } from 'qunit';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | question-view-icons', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<QuestionViewIcons />`);

    assert
      .dom(this.element)
      .hasText("You haven't picked any icons. You need to select at least one icon to make the question relevant.");
  });
});
