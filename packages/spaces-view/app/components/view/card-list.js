/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BasePageColListComponent from './base/modelList';
import { service } from '@ember/service';
import { getBsFromSizeIndex, getSizeIndexFromBs } from 'core/lib/responsive';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class PageColCampaignsComponent extends BasePageColListComponent {
  @service pageCols;

  @tracked size;
  @tracked _card;
  @tracked _value;

  modelName = 'campaign';

  get containerClasses() {
    const options = this.value?.data?.columns?.containerClasses?.join?.(' ');

    return options ?? '';
  }

  get value() {
    return this._value ?? this.args.value;
  }

  @action
  changeSize(value) {
    this.size = value;
  }

  @action
  updateCardProps(event) {
    const value = this.value;
    value.data.card = event.detail.value;

    this._card = event.detail.value;
    this._value = value;
  }

  get card() {
    return this._card ?? this.args.value?.data?.card;
  }

  get maxItems() {
    const maxItems =
      this.args.value?.data?.card?.maxItems ??
      this.args.value?.data?.sourceGroupStyle?.maxItems;

    if (!maxItems) {
      return null;
    }

    let size = this.args.deviceSize ?? this.size ?? 'lg';
    let sizeIndex = getSizeIndexFromBs(size);

    let result = null;

    while (!result && sizeIndex > -1) {
      size = getBsFromSizeIndex(sizeIndex);
      result = maxItems[size];

      sizeIndex--;
    }

    return result;
  }

  get cardContainer() {
    if (this.card) {
      return this.card.container;
    }

    return this.args.value?.data?.columns;
  }
}
