/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { click, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { DateTime } from 'luxon';
import { expect } from 'chai';

describe('Integration | Component | day-selector', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a calendar with the today selected', async function (a) {
    await render(hbs`<DaySelector />`);

    const timestamp = parseInt(
      this.element.querySelector('.datepicker-cell.day.focused.selected').attributes.getNamedItem('data-date').value,
    );
    const date = DateTime.fromMillis(timestamp);
    const now = DateTime.now();

    expect(date.day).to.equal(now.day);
    expect(date.month).to.equal(now.month);
    expect(date.year).to.equal(now.year);
  });

  it('renders a calendar with no selection when allowClear is true', async function (a) {
    await render(hbs`<DaySelector @allowClear={{true}}/>`);

    expect(this.element.querySelector('.datepicker-cell.day.focused.selected')).not.to.exist;
  });

  it('renders a calendar with a string value selected', async function (a) {
    await render(hbs`<DaySelector @value="2022-02-03"/>`);

    const timestamp = parseInt(
      this.element.querySelector('.datepicker-cell.day.focused.selected').attributes.getNamedItem('data-date').value,
    );
    const date = DateTime.fromMillis(timestamp);

    expect(date.day).to.equal(3);
    expect(date.month).to.equal(2);
    expect(date.year).to.equal(2022);
  });

  it('renders a calendar with a date value selected', async function () {
    this.set('value', DateTime.fromISO('2022-02-03T12:00:00').toJSDate());

    await render(hbs`<DaySelector @value={{this.value}}/>`);

    const timestamp = parseInt(
      this.element.querySelector('.datepicker-cell.day.focused.selected').attributes.getNamedItem('data-date').value,
    );
    const date = DateTime.fromMillis(timestamp);

    expect(date.day).to.equal(3);
    expect(date.month).to.equal(2);
    expect(date.year).to.equal(2022);
  });

  it('renders the updated value', async function (a) {
    this.set('value', '2022-02-03');
    await render(hbs`<DaySelector @value={{this.value}} />`);

    this.set('value', '2023-03-04');

    const timestamp = parseInt(
      this.element.querySelector('.datepicker-cell.day.focused.selected').attributes.getNamedItem('data-date').value,
    );
    const date = DateTime.fromMillis(timestamp);

    expect(date.day).to.equal(4);
    expect(date.month).to.equal(3);
    expect(date.year).to.equal(2023);
  });

  it('triggers a change event when a new day is selected', async function (a) {
    this.set('value', '2022-02-03');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<DaySelector @onChange={{this.change}} @value={{this.value}} />`);

    const days = this.element.querySelectorAll('.day');

    await click(days[11]);

    const timestamp = parseInt(
      this.element.querySelector('.datepicker-cell.day.focused.selected').attributes.getNamedItem('data-date').value,
    );
    const date = DateTime.fromMillis(timestamp);

    expect(date.day).to.equal(10);
    expect(date.month).to.equal(2);
    expect(date.year).to.equal(2022);

    expect(value.day).to.equal(10);
    expect(value.month).to.equal(2);
    expect(value.year).to.equal(2022);
  });

  it('renders an input when the showInput is true', async function (a) {
    await render(hbs`<DaySelector @value="2022-02-03" @showInput={{true}}/>`);

    await click('input');

    const timestamp = parseInt(
      this.element.querySelector('.datepicker-cell.day.focused.selected').attributes.getNamedItem('data-date').value,
    );
    const date = DateTime.fromMillis(timestamp);

    expect(date.day).to.equal(3);
    expect(date.month).to.equal(2);
    expect(date.year).to.equal(2022);
  });
});
