/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | bs-col-widths', function (hooks) {
  setupRenderingTest(hooks);

  it('renders dashes when the options are not set', async function () {
    await render(hbs`<BsColWidths />`);

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('-');
  });

  it('renders the mobile col size when is set', async function () {
    this.set('col', { options: ['col-2'] });
    await render(hbs`<BsColWidths @col={{this.col}} />`);

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('2');
  });

  it('renders the col name when set', async function () {
    this.set('col', { options: ['col-2'], name: 'main menu' });
    await render(hbs`<BsColWidths @col={{this.col}} />`);

    expect(this.element.querySelector('.bs-col-name').textContent.trim()).to.equal('main menu (?.?.?)');
  });

  it('renders the col indexes as name when the name is not set', async function () {
    this.set('col', { options: ['col-2'] });
    await render(hbs`<BsColWidths @col={{this.col}} @containerIndex={{1}} @colIndex={{2}} @rowIndex={{3}} />`);

    expect(this.element.querySelector('.bs-col-name').textContent.trim()).to.equal('1.3.2');
  });

  it('renders the mobile px size when is set', async function () {
    this.set('col', { options: ['wpx-20'] });
    await render(hbs`<BsColWidths @col={{this.col}} />`);

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('20px');
  });

  it('renders the tablet col size when is set', async function () {
    this.set('col', { options: ['col-md-2'] });
    await render(hbs`<BsColWidths @col={{this.col}} />`);

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('2');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('-');
  });

  it('renders the tablet px size when is set', async function () {
    this.set('col', { options: ['wpx-md-20'] });
    await render(hbs`<BsColWidths @col={{this.col}} />`);

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('20px');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('-');
  });

  it('renders the desktop col size when is set', async function () {
    this.set('col', { options: ['col-lg-2'] });
    await render(hbs`<BsColWidths @col={{this.col}} />`);

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('2');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('-');
  });

  it('renders the desktop px size when is set', async function () {
    this.set('col', { options: ['wpx-lg-20'] });
    await render(hbs`<BsColWidths @col={{this.col}} />`);

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('20px');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('-');
  });

  it('does not show any size as active by default', async function () {
    await render(hbs`<BsColWidths />`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).not.to.exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).not.to.exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).not.to.exist;
  });

  it('activates the desktop size when size is desktop', async function () {
    await render(hbs`<BsColWidths @size="desktop"/>`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).to.exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).not.to.exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).not.to.exist;
  });

  it('activates the tablet size when size is tablet', async function () {
    await render(hbs`<BsColWidths @size="tablet"/>`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).not.to.exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).to.exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).not.to.exist;
  });

  it('activates the mobile size when size is mobile', async function () {
    await render(hbs`<BsColWidths @size="mobile"/>`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).not.to.exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).not.to.exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).to.exist;
  });
});
