/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent, typeIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/attribute", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let value;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultFeature("1");

    const feature = server.testData.storage.addDefaultFeature("2");
    feature.name = "name 2";
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    value = null;

    this.set("onChange", (v) => {
      value = v;
    });
  });

  it("renders the values", async function (a) {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        sourceAttribute: {
          field: "icon.property",
        },
        title: {
          text: "some title",
          heading: 2,
          color: "green",
          classes: ["display-3", "fw-bold"],
        },
        attributeStyle: {
          color: "red",
          classes: ["fw-light"],
        },
      },
    });

    await render(hbs`<Editor::Attribute @value={{this.value}} />`);

    expect(this.element.querySelector(".input-attribute-field").value).to.equal("icon.property");

    expect(this.element.querySelector(".model-name").value).to.equal("feature");
    expect(this.element.querySelector(".model-id .ember-power-select-selected-item").textContent.trim()).to.equal(
      "Nomadisch Grün - Local Urban Food",
    );

    expect(this.element.querySelector(".title-style .input-title-text").value).to.equal("some title");
    expect(this.element.querySelector(".title-style .input-sm-display").value).to.equal("3");

    expect(this.element.querySelector(".attribute-style .input-sm-fw").value).to.equal("light");
    expect(this.element.querySelector(".attribute-style .text-color-selector .selected-value")).to.have.class("bg-red");
  });

  it("can change the source values", async function (a) {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        sourceAttribute: {
          field: "icon.property",
        },
        title: {
          text: "some title",
          heading: 2,
          color: "green",
          classes: ["display-3", "fw-bold"],
        },
        attributeStyle: {
          color: "red",
          classes: ["fw-light"],
        },
      },
    });

    await render(hbs`<Editor::Attribute @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".input-attribute-field", "new value");

    this.element.querySelector(".use-current-model").value = "yes";
    await triggerEvent(".use-current-model", "change");

    expect(value).to.deep.equal({
      source: { useSelectedModel: true },
      sourceAttribute: { field: "new value" },
      title: { text: "some title", heading: 2, color: "green", classes: ["display-3", "fw-bold"] },
      attributeStyle: { color: "red", classes: ["fw-light"] },
    });
  });

  it("can change the title values", async function (a) {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        sourceAttribute: {
          field: "icon.property",
        },
        title: {
          text: "some title",
          heading: 2,
          color: "green",
          classes: ["display-3", "fw-bold"],
        },
        attributeStyle: {
          color: "red",
          classes: ["fw-light"],
        },
      },
    });

    await render(hbs`<Editor::Attribute @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".input-title-text", "new value");

    expect(value).to.deep.equal({
      source: { model: "feature", id: "1" },
      sourceAttribute: { field: "icon.property" },
      title: { color: "green", classes: ["display-3", "fw-bold"], text: "new value" },
      attributeStyle: { color: "red", classes: ["fw-light"] },
    });
  });

  it("can change the attribute style values", async function (a) {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        sourceAttribute: {
          field: "icon.property",
        },
        title: {
          text: "some title",
          heading: 2,
          color: "green",
          classes: ["display-3", "fw-bold"],
        },
        attributeStyle: {
          color: "red",
          classes: ["fw-light"],
        },
      },
    });

    await render(hbs`<Editor::Attribute @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".attribute-style .input-sm-fw").value = "bold";
    await triggerEvent(".attribute-style .input-sm-fw", "change");

    expect(value).to.deep.equal({
      source: { model: "feature", id: "1" },
      sourceAttribute: { field: "icon.property" },
      title: { text: "some title", heading: 2, color: "green", classes: ["display-3", "fw-bold"] },
      attributeStyle: { color: "red", classes: ["fw-bold"], minLines: { sm: +0, md: +0, lg: +0 } },
    });
  });
});
