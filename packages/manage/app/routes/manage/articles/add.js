/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';
import { Visibility } from '../../../transforms/visibility';

export default class AddArticleRoute extends AuthenticatedRoute {
  modelsToClear = ['article'];

  queryParams = {
    all: { refreshModel: true },
    teamId: { refreshModel: true },
    category: { refreshModel: true },
  };

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    let team = null;
    let categories = [];
    const teamParams = { edit: true };

    if (params.teamId) {
      team = params.teamId;
    }

    if (params.all) {
      teamParams.all = params.all;
    }

    if (params.category) {
      categories = params.category.split(',');
    }

    const article = this.store.createRecord('article', {
      title: '',
      content: {},
      categories,
      info: new ModelInfo(),
      visibility: new Visibility(this.store, false, false, team),
    });

    await article.visibility.fetchTeam();

    return hash({
      article,
      categories,
      pictures: [],
      teams: this.store.query('team', teamParams),
    });
  }
}
