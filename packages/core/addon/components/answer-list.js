/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

function toStringValue(value) {
  if (!value) {
    return null;
  }

  if (typeof value?.data == 'object' && value.data) {
    return Object.keys(value.data)
      .map((a) => `${a}: ${value.data[a]}`)
      .join(' ');
  }

  return value;
}

export default class AnswerListComponent extends Component {
  get questions() {
    return this.args?.questions?.filter((a) => a.isVisible) ?? [];
  }

  get questionsAndValues() {
    return this.questions.map((a) => ({
      question: a.question,
      value: toStringValue(this.args.value?.[a.name]),
      hasBlocksValue: Array.isArray(this.args.value?.[a.name]?.blocks),
      hasValue: this.args.value?.[a.name] !== undefined && this.args.value?.[a.name] !== null,
    }));
  }
}
