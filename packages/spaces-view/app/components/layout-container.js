/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/template';
import { service } from '@ember/service';

export default class LayoutContainerComponent extends Component {
  @service fastboot;

  @tracked height;
  @tracked _container;

  get container() {
    if (this._container) {
      return this._container;
    }

    return this.args.container;
  }

  get heightClass() {
    if (this.container?.height != 'fill') {
      return '';
    }

    return 'height-fill';
  }

  get style() {
    if (!this.height) {
      return htmlSafe(`100%`);
    }

    return htmlSafe(`height: ${this.height}px;`);
  }

  get backgroundCol() {
    return this.col?.type || 'background/empty';
  }

  get col() {
    return this.layers?.[0];
  }

  get hasLayers() {
    return this.layers.length > 1;
  }

  get layers() {
    const name = this.args.container?.name ?? '-';

    let result = [this.args.cols?.find((col) => col.name == name)];

    for (let i = 1; i < this.args.container?.layers?.count; i++) {
      result.push(
        this.args.cols?.find((col) => col.name == `${name}.layer-${i}`),
      );
    }

    return result;
  }

  get effect() {
    return this.args.container?.layers?.effect ?? 'none';
  }

  @action
  updateProps(event) {
    this._container = event.detail.value;
  }

  @action
  watchSize(element) {
    this.rootElement = element;

    this.onResize();
    this.spyResize();
  }

  spyResize() {
    try {
      this.sizeObserver?.disconnect?.();

      this.sizeObserver = new ResizeObserver(() => {
        window.requestAnimationFrame(() => {
          this.onResize();
        });
      });
      this.sizeObserver.observe(this.rootElement);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  }

  onResize() {
    const rect = this.rootElement?.getBoundingClientRect?.();

    this.height = rect?.height;
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.sizeObserver?.disconnect?.();
  }
}
