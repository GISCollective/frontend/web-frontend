/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { toDateTime } from './calendar-entry-list';
import { A } from '../lib/array';

export default class SubscriptionDetailsTransform extends Transform {
  deserialize(serialized) {
    return new SubscriptionDetails(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() || deserialized;
  }
}

export class TimeTracking {
  @tracked hours = 0;
  @tracked date = new Date('0001-01-01T00:00:00Z');
  @tracked details = '';

  constructor(serialized) {
    if (serialized?.details) {
      this.details = serialized.details;
    }

    if (serialized?.date) {
      this.date = new Date(serialized.date);
    }

    if (!isNaN(serialized?.hours)) {
      this.hours = parseFloat(serialized.hours);
    }
  }

  toJSON() {
    return {
      hours: parseFloat(this.hours),
      date: toDateTime(this.date, 'UTC').toISO(),
      details: this.details,
    };
  }
}

export class SubscriptionDetails {
  @tracked name = '';
  @tracked expire = new Date('0001-01-01T00:00:00Z');
  @tracked domains = A();
  @tracked comment = '';
  @tracked details = '';
  @tracked monthlySupportHours = 0;
  @tracked support = A();
  @tracked invoices = A();

  constructor(serialized) {
    if (serialized?.name) {
      this.name = serialized.name;
    }

    if (serialized?.comment) {
      this.comment = serialized.comment;
    }

    if (Array.isArray(serialized?.domains)) {
      for(let domain of serialized.domains) {
        this.domains.push(domain)
      }
    }

    if (Array.isArray(serialized?.support)) {
      for(let a of serialized.support) {
        this.support.push(new TimeTracking(a))
      }
    }

    if (Array.isArray(serialized?.invoices)) {
      for(let a of serialized.invoices) {
        this.invoices.push(new TimeTracking(a))
      }
    }

    if (serialized?.expire) {
      this.expire = new Date(serialized.expire);
    }

    if (serialized?.details) {
      this.details = serialized.details;
    }

    if (!isNaN(serialized?.monthlySupportHours)) {
      this.monthlySupportHours = parseFloat(serialized.monthlySupportHours);
    }
  }

  toJSON() {
    const result = {
      name: this.name,
      domains: this.domains,
      comment: this.comment,
      expire: this.expire.toISOString(),
      details: this.details ?? '',
      monthlySupportHours: isNaN(this.monthlySupportHours) ? 0 : parseFloat(this.monthlySupportHours),
      invoices: this.invoices?.map((a) => (a.toJSON ? a.toJSON() : a)) ?? [],
      support: this.support?.map((a) => (a.toJSON ? a.toJSON() : a)) ?? [],
    };

    return result;
  }
}
