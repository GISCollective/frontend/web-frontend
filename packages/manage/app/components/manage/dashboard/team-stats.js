/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageDashboardTeamStatsComponent extends Component {
  @service store;

  @tracked mapViews;
  @tracked campaignContributions;
  @tracked campaignAnswerContributors;

  @action
  async setup() {
    this.mapViews = this.store.queryRecord('stat', {
      id: 'map-views',
      team: this.args.team?.id,
    });

    this.campaignContributions = this.store.queryRecord('stat', {
      id: 'campaign-contributions',
      team: this.args.team?.id,
    });

    this.campaignAnswerContributors = this.store.queryRecord('stat', {
      id: 'campaign-answer-contributors',
      team: this.args.team?.id,
    });
  }
}
