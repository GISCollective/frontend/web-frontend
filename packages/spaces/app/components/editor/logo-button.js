/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class ManageEditorsLogoButtonComponent extends Component {
  @tracked _style;
  @tracked _classes;

  get classes() {
    if (this._classes) {
      return this._classes;
    }

    return this.args.value?.style?.classes ?? [];
  }

  get style() {
    if (this._style) {
      return this._style;
    }

    return this.args?.value.pictureOptions?.style ?? {};
  }

  triggerChange() {
    return this.args.onChange?.({
      style: { classes: this.classes },
      pictureOptions: {
        style: this.style,
      },
    });
  }

  @action
  changeClasses(value) {
    this._classes = value;

    return this.triggerChange();
  }

  @action
  changePictureStyle(value) {
    this._style = value;

    return this.triggerChange();
  }
}
