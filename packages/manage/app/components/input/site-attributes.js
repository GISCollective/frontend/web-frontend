/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';
import { A } from 'core/lib/array';

export default class InputSiteAttributesComponent extends Component {
  @tracked value = {};
  @tracked groups = A([]);
  @tracked listGroupNames = A([]);
  @tracked iconGroupNames = A([]);
  @tracked icons;

  handleChange(group) {
    group.update(this.value, this.groups);

    const availableGroups = A(this.groups.filter((a) => !a.isDeleted));

    if (availableGroups.length != this.groups.length) {
      this.groups = availableGroups;
    }

    this.updateListGroupNames();
    this.updateIconGroupNames();
  }

  updateGroups() {
    this.groups = A();
    const handleChange = (group) => {
      this.handleChange(group);
    };

    for (const key of Object.keys(this.value)) {
      if (Array.isArray(this.value[key])) {
        for (let index = 0; index < this.value[key].length; index++) {
          const item = new AttributeListGroup(key, index, handleChange, this.value[key][index]);

          this.groups.push(item);
        }

        continue;
      }

      this.groups.push(new AttributeGroup(key, handleChange, this.value[key]));
    }

    this.updateListGroupNames();
    this.matchGroupIcons();
    this.updateIconGroupNames();
  }

  updateListGroupNames() {
    this.listGroupNames = A();

    for (const key of Object.keys(this.value)) {
      if (Array.isArray(this.value[key])) {
        this.listGroupNames.push(key);
      }
    }
  }

  matchGroupIcons() {
    const iconMap = {};

    for (const icon of this.icons) {
      iconMap[icon.name] = icon;
    }

    for (const group of this.groups) {
      group.icon = iconMap[group.name];
    }
  }

  updateIconGroupNames() {
    const result = A(this.icons.filter((a) => a.allowMany));
    let groups = this.icons.filter((a) => !a.allowMany && !this.value[a.name]);

    for(let group of groups) {
      result.push(group);
    }

    this.iconGroupNames = A(result);
  }

  @action
  async setup() {
    const icons = (await this.args.icons) ?? [];

    this.icons = icons.slice?.() ?? icons;

    later(() => {
      this.value = JSON.parse(JSON.stringify(this.args.attributes ?? {})) ?? {};
      this.updateGroups();
    });
  }

  @action
  deleteGroup(group) {
    group.isDeleted = true;
  }

  @action
  addAttribute(group) {
    group.addAttribute();
  }

  @action
  addAnonymousGroup() {
    const key = 'New Group';
    let index = 1;

    while (this.value[`${key} ${index}`]) {
      index++;
    }

    this.value[`${key} ${index}`] = {};
    this.updateGroups();
  }

  @action
  addListGroupItem(name) {
    this.value[name] = A(this.value[name]);

    this.value[name].push({});
    this.updateGroups();
  }

  @action
  addIconGroup(icon) {
    const name = icon.name;

    if (!this.value[name] && !icon.allowMany) {
      this.value[name] = {};
    }

    if (!this.value[name] && icon.allowMany) {
      this.value[name] = [];
    }

    if (this.value[name] && icon.allowMany) {
      return this.addListGroupItem(name);
    }

    this.updateGroups();
  }

  @action
  onSave() {
    return this.args.onSave(this.value);
  }

  @action
  onCancel() {
    this.value = {};
    this.groups = A([]);
    this.setup();

    return this.args.onCancel?.();
  }
}

class AttributeGroup {
  @tracked _name;
  @tracked properties = A();
  @tracked _icon;
  @tracked _isDeleted;

  originalName = '';

  constructor(name, onChange, defaultProps) {
    this._name = name;
    this.originalName = name;
    this.onChange = onChange;

    if (defaultProps) {
      this.properties = A(
        Object.keys(defaultProps).map(
          (a) =>
            new Attribute(a, defaultProps[a], null, () => {
              this.onChange(this);
            }),
        ),
      );
    }
  }

  get icon() {
    return this._icon;
  }

  set icon(value) {
    this._icon = value;

    const sortedProperties = A();
    const propertyMap = {};

    for (const property of this.properties) {
      propertyMap[property.name] = property;
    }

    if (Array.isArray(value?.attributes)) {
      for (let attribute of value?.attributes) {
        if (!propertyMap[attribute.name]) {
          propertyMap[attribute.name] = new Attribute(attribute.name, '', null, () => {
            this.onChange(this);
          });
        }

        propertyMap[attribute.name].isRequired = attribute.isRequired;
        propertyMap[attribute.name].displayName = attribute.displayName;
        propertyMap[attribute.name].help = attribute.help;

        sortedProperties.push(propertyMap[attribute.name]);

        delete propertyMap[attribute.name];
      }
    }

    Object.keys(propertyMap).forEach((name) => {
      sortedProperties.push(propertyMap[name]);
    });

    this.properties = A(sortedProperties);
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
    this.onChange(this);
  }

  get isDeleted() {
    return this._isDeleted;
  }

  set isDeleted(value) {
    this._isDeleted = value;
    this.onChange(this);
  }

  @action
  addAttribute(name = 'new attribute') {
    const exists = this.properties.find((a) => a.name == name);

    if (exists) {
      return;
    }

    this.properties.push(
      new Attribute(name, '', null, () => {
        this.onChange(this);
      }),
    );

    this.onChange(this);
  }

  updateProperties(value) {
    for (const property of this.properties) {
      if (property.originalName != property.name || property.isDeleted) {
        delete value[property.originalName];
        property.originalName = property.name;
      }

      if (!property.isDeleted) {
        value[property.name] = property.value;
      }
    }

    const allExistingProps = this.properties.filter((a) => !a.isDeleted);
    if (allExistingProps.length != this.properties.length) {
      this.properties = allExistingProps;
    }

    return value;
  }

  update(value) {
    if (this.originalName != this.name) {
      value[this.name] = value[this.originalName];
      delete value[this.originalName];
      this.originalName = this.name;
    }

    if (this.isDeleted && this.name) {
      delete value[this.name];
    }

    if (this.isDeleted) {
      return;
    }

    value[this.name] = this.updateProperties(value[this.name]);
  }
}

class AttributeListGroup extends AttributeGroup {
  @tracked index;

  constructor(name, index, onChange, defaultProps) {
    super(name, onChange, defaultProps);
    this.index = index;
  }

  get hasIndex() {
    return true;
  }

  update(value, allGroups) {
    if (this.originalName != this.name) {
      value[this.name] = A(value[this.originalName]);
      delete value[this.originalName];

      const originalName = this.originalName;
      for (const group of allGroups) {
        if (group.originalName == originalName) {
          group.originalName = this.name;
          group._name = this.name;
        }
      }
    }

    if (this.isDeleted && this.name) {
      value[this.name].splice(this.index, 1);

      for (const group of allGroups) {
        if (group.name == this.name && group.index > this.index) {
          group.index -= 1;
        }
      }
    }

    if (this.isDeleted) {
      return;
    }

    value[this.name][this.index] = this.updateProperties(value[this.name][this.index]);
  }
}

class Attribute {
  @tracked _name;
  @tracked _value;
  @tracked _isDeleted = false;
  @tracked isRequired;
  @tracked displayName;
  @tracked help;

  originalName = '';

  constructor(name, value, index, onChange) {
    this._name = name;
    this._value = value;
    this.originalName = name;
    this.onChange = onChange;

    this.onChange = onChange;
  }

  get isBlocks() {
    return Array.isArray(this._value?.blocks);
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
    this.onChange();
  }

  get value() {
    return this._value;
  }

  set value(value) {
    this._value = value;
    this.onChange();
  }

  get isDeleted() {
    return this._isDeleted;
  }

  set isDeleted(value) {
    this._isDeleted = value;
    this.onChange();
  }
}
