/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { Style } from 'ol/style';

export default class StyleCache {
  styles = {};

  constructor() {
    this.emptyStyle = new Style();
  }
}
