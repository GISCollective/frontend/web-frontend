/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/selectable-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a message when there is no value', async function () {
    await render(hbs`<Manage::SelectableList>template block text</Manage::SelectableList>`);

    expect(this.element.textContent.trim()).to.equal(
      'Looks like there’s nothing here. Add a new record to get started!',
    );
    expect(this.element.querySelector('.btn-load-more')).not.to.exist;
  });

  it('renders an item when it is set', async function () {
    this.set('value', [{ id: 1 }]);

    await render(hbs`<Manage::SelectableList @value={{this.value}} as |item| >{{item.id}}</Manage::SelectableList>`);

    expect(this.element.textContent.trim()).to.equal('1');
  });

  it('renders an item when it is set in the items property of an object', async function () {
    this.set('value', { items: [{ id: 1 }] });

    await render(hbs`<Manage::SelectableList @value={{this.value}} as |item| >{{item.id}}</Manage::SelectableList>`);

    expect(this.element.textContent.trim()).to.equal('1');
  });

  it('renders a load more button when the value has canLoadMore=true', async function () {
    let nextLoaded;

    this.set('value', { items: [{ id: 1 }], canLoadMore: true, loadNext: () => (nextLoaded = true) });

    await render(hbs`<Manage::SelectableList @value={{this.value}} as |item| >{{item.id}}</Manage::SelectableList>`);

    await click('.btn-load-more');

    expect(nextLoaded).to.equal(true);
  });

  it('renders 2 items when they are set', async function () {
    this.set('value', [{ id: 1 }, { id: 2 }]);

    await render(
      hbs`<Manage::SelectableList @value={{this.value}} as |item| ><span>{{item.id}}</span></Manage::SelectableList>`,
    );

    expect([...this.element.querySelectorAll('span')].map((a) => a.textContent).join('')).to.equal('12');
  });

  it('passes the on select function', async function () {
    this.set('value', [{ id: 1 }, { id: 2 }]);

    await render(hbs`<Manage::SelectableList @value={{this.value}} as |item selection onSelect| >
      <button type="button" {{on "click" (fn onSelect "all")}}>btn {{item.id}}</button>
    </Manage::SelectableList>`);

    expect(this.element.querySelector('.count-message')).not.to.exist;

    await click('button');

    expect(this.element.querySelector('.count-message').textContent.trim()).to.equal('all items are selected');
  });

  it('renders the select all chackbox when there are 2 items', async function () {
    this.set('value', [{ id: 1 }, { id: 2 }]);

    await render(hbs`<Manage::SelectableList @value={{this.value}} as |item selection onSelect| >
      <button type="button" {{on "click" (fn onSelect "all")}}>btn {{item.id}}</button>
    </Manage::SelectableList>`);

    expect(this.element.querySelector('.count-message')).not.to.exist;

    await click('.chk-select-all');

    expect(this.element.querySelector('.count-message').textContent.trim()).to.equal('all items are selected');
  });
});
