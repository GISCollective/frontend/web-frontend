/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class ManageNewsletterUnsubscribeRoute extends Route {
  @service store;

  async model(params) {
    return {
      email: params.email_id,
      newsletter: await this.store.findRecord('newsletter', params.id),
    };
  }
}
