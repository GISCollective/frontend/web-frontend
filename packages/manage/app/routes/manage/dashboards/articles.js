/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

class ArticlesModel {
  @tracked team;
  @tracked articles;
  @tracked categoryList;
  @tracked params;
  @tracked store;

  constructor(store, params) {
    this.store = store;
    this.params = params;
  }

  @action
  async reload() {
    const teamCategory = this.params.teamCategory ?? '';

    this.categoryList = await this.store.adapterFor('article').categories(this.params.id, teamCategory);

    let category = [];
    if (this.categoryList.categories.includes(this.params.category)) {
      category.push(this.params.category);
    }

    if (teamCategory) {
      category.push(teamCategory);
    }

    if (this.categoryList.categories) {
      this.categoryList = this.categoryList.categories.slice();
    }

    this.team = await this.store.findRecord('team', this.params.id);
    this.articles = await this.store.query('article', {
      team: this.params.id,
      type: 'any',
      category: category.length ? category.join(',') : undefined,
    });
  }
}

export default class ManageDashboardsArticlesRoute extends AuthenticatedRoute {
  queryParams = {
    category: {
      refreshModel: true,
    },
    teamCategory: {
      refreshModel: true,
    },
  };

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const model = new ArticlesModel(this.store, params);

    await model.reload();

    return model;
  }
}
