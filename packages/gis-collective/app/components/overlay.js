/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { later, cancel } from '@ember/runloop';

export default class OverlayComponent extends Component {
  @service fullscreen;
  @tracked _isPresentable;

  get isPresentable() {
    return this._isPresentable;
  }

  set isPresentable(value) {
    if (value) {
      this.fullscreen.freezeBody();
    } else {
      this.fullscreen.unfreezeBody();
    }

    this._isPresentable = value;
  }

  @action
  setup(element) {
    this.overlayElement = element;

    if (this.args.isVisible) {
      later(() => {
        this.isPresentable = true;
      }, 10);
    }
  }

  @action
  update() {
    this.isPresentable = this.args.isVisible;
  }

  @action
  onClose() {
    later(() => {
      this.isPresentable = false;
    }, 10);

    this.closeTimer = later(() => {
      this.args.onClose?.();
    }, 30);
  }

  @action
  startTransition(event) {
    if (this.overlayElement != event.target || event.propertyName != 'opacity') {
      return;
    }

    cancel(this.closeTimer);
  }

  @action
  finishTransition(event) {
    if (this.overlayElement != event.target || event.propertyName != 'opacity') {
      return;
    }

    if (this.isPresentable == false) {
      cancel(this.closeTimer);
      this.args.onClose?.();
    }
  }

  willDestroy() {
    super.willDestroy(...arguments);

    if (this.isPresentable) {
      this.fullscreen.unfreezeBody();
    }
  }
}
