import { module, test } from 'qunit';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | manage/list-row', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders nothing when there is no row', async function (assert) {
    await render(hbs`<Manage::ListRow />`);

    // assert.dom(this.element).hasText('');
  });
});
