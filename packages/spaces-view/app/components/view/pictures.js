/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './base-list';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class PageColPictureListComponent extends Component {
  @service fastboot;
  @tracked _recordsWithLinks = [];

  elementId = `page-col-picture-list-${guidFor(this)}`;
  elements = {};

  get recordType() {
    return 'picture';
  }

  get recordsWithLinks() {
    if (!this.fastboot.isFastBoot) {
      return this._recordsWithLinks;
    }

    return this.prepareRecords();
  }

  prepareRecords() {
    const result = [];
    const records = this.records;
    const links = this.args.value?.data?.links ?? [];

    let i = 0;
    for (let picture of records) {
      const destination =
        Object.keys(links[i] ?? {}).length > 0 ? links[i] : null;
      result.push({ picture, destination });
      i++;
    }

    return result;
  }

  @action
  prepareData() {
    later(() => {
      this._recordsWithLinks = this.prepareRecords();
    });
  }

  @action
  async updateProps(event) {
    const source = event.detail.value;

    this.ids = source.ids;
    await this.loadIds();
    this.prepareData();
  }

  @action
  setupLink(index, element) {
    this.elements[index] = element;
  }

  @action
  handleClick(index) {
    this.elements[index]?.click?.();
  }
}
