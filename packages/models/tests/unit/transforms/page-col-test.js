/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { PageCol } from 'models/transforms/page-col-list';

describe('Unit | Transform | page col class', function (hooks) {
  setupTest(hooks);

  describe('modelKey', function () {
    it('returns null when there is no data', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      expect(pageCol.modelKey).not.to.exist;
    });

    it('returns a default key when a default model is used', function () {
      const pageCol = new PageCol({
        name: '0.0.0',
        data: {
          source: {
            model: 'map',
            useDefaultModel: true,
            useSelectedModel: false,
          },
        },
      });

      expect(pageCol.modelKey).to.equal('map_default');
    });
  });
});
