/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/space-template-selector', function (hooks) {
  setupRenderingTest(hooks, { server: true });

  let space;
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no space', async function () {
    await render(hbs`<Manage::SpaceTemplateSelector />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  describe('when there is a space', function (hooks) {
    hooks.beforeEach(async function () {
      space = server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultPicture();
    });

    it('renders a card with a preview button', async function () {
      await render(hbs`<Manage::SpaceTemplateSelector />`);

      expect(this.element.querySelector('.space-name').textContent.trim()).to.equal('GISCollective');
      expect(this.element.querySelector('.space-cover')).to.have.attribute(
        'style',
        "background-image: url('/test/5d5aa72acac72c010043fb59.jpg/md');",
      );

      expect(this.element.querySelector('.preview-link').textContent.trim()).to.equal('preview');
      expect(this.element.querySelector('.preview-link')).to.have.attribute('href', 'https://test.giscollective.com');
      expect(this.element.querySelector('.preview-link')).to.have.attribute('target', '_blank');
    });

    it('renders a card with a select button', async function () {
      await render(hbs`<Manage::SpaceTemplateSelector />`);

      expect(this.element.querySelector('.btn-select')).to.exist;
      expect(this.element.querySelector('.btn-select')).to.have.class('btn-outline-secondary');
    });

    it('renders a card with a selected button when the value matches the space id', async function () {
      this.set('id', space._id);

      await render(hbs`<Manage::SpaceTemplateSelector @value={{this.id}} />`);

      expect(this.element.querySelector('.btn-select')).to.exist;
      expect(this.element.querySelector('.btn-select')).to.have.class('btn-success');
    });

    it('triggers onChange with the space id when a space is selected', async function () {
      let value;

      this.set('change', (v) => {
        value = v;
      });

      await render(hbs`<Manage::SpaceTemplateSelector @onChange={{this.change}} />`);

      await click('.btn-select');

      expect(value).to.equal(space._id);
    });

    it('triggers onChange with undefined when the selected space is selected again', async function () {
      this.set('id', space._id);
      let value = space._id;

      this.set('change', (v) => {
        value = v;
      });

      await render(hbs`<Manage::SpaceTemplateSelector @value={{this.id}} @onChange={{this.change}} />`);

      await click('.btn-select');

      expect(value).to.be.undefined;
    });
  });
});
