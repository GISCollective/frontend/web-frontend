/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';

describe.skip('Acceptance | browse/maps/map-view/spotlight', function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPage('000000000000000000000001');
    server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('on the world view', function (hooks) {
    describe('when the server returns no features, icons or maps', function (hooks) {
      it('shows a list of icon sets', async function () {
        await visit('/browse/maps/_/map-view/spotlight');

        expect(this.element.querySelector('h5.icon-set-title')).not.to.exist;
        expect(this.element.querySelector('.no-data')).to.exist;
      });
    });

    describe('when the server returns one icon set with icons', function (hooks) {
      hooks.beforeEach(function () {
        server.testData.storage.addDefaultIconSet('5ca7b702ecd8490100cab96f');

        server.testData.storage.addDefaultIcon('1');
        server.testData.storage.addDefaultIcon('2');
        server.testData.storage.addDefaultIcon('3');
        server.testData.storage.addDefaultIcon('4');
        server.testData.storage.addDefaultIcon('5');
        server.testData.storage.addDefaultIcon('6');
        server.testData.storage.addDefaultIcon('7');
        server.testData.storage.addDefaultIcon('8');
        server.testData.storage.addDefaultIcon('9');
        server.testData.storage.addDefaultIcon('10');
      });

      it('renders all icons', async function () {
        await visit('/browse/maps/_/map-view/spotlight');

        expect(this.element.querySelector('.btn-outline-primary')).not.to.exist;
        expect(this.element.querySelectorAll('.btn-icon')).to.have.length(10);
      });
    });

    describe('when the server returns two icon sets with icons', function (hooks) {
      hooks.beforeEach(function () {
        server.testData.storage.addDefaultIconSet('1');
        server.testData.storage.addDefaultIconSet('2');
        server.testData.storage.addDefaultIconSet('5ca7b702ecd8490100cab96f');

        server.testData.storage.addDefaultIcon('1');
        server.testData.storage.addDefaultIcon('2');
      });

      it('shows a list of icon sets', async function () {
        await visit('/browse/maps/_/map-view/spotlight');

        expect(this.element.querySelector('h5.icon-set-title').textContent.trim()).to.equal(
          'Green Map® Icons Version 3',
        );
        expect(this.element.querySelector('.no-data')).not.to.exist;

        expect(this.element.querySelector('.icon-name')).to.exist;
      });

      it('does not render the show more button when the set has <= 8 icons', async function () {
        server.testData.storage.addDefaultIcon('3');
        server.testData.storage.addDefaultIcon('4');
        server.testData.storage.addDefaultIcon('5');
        server.testData.storage.addDefaultIcon('6');
        server.testData.storage.addDefaultIcon('7');
        server.testData.storage.addDefaultIcon('8');

        await visit('/browse/maps/_/map-view/spotlight');

        expect(this.element.querySelector('.btn-view-all-icons')).not.to.exist;
      });

      it('renders the show more button when the set has > 8 icons', async function () {
        server.testData.storage.addDefaultIcon('3');
        server.testData.storage.addDefaultIcon('4');
        server.testData.storage.addDefaultIcon('5');
        server.testData.storage.addDefaultIcon('6');
        server.testData.storage.addDefaultIcon('7');
        server.testData.storage.addDefaultIcon('8');
        server.testData.storage.addDefaultIcon('9');

        await visit('/browse/maps/_/map-view/spotlight');

        expect(this.element.querySelector('.btn-view-all-icons')).to.exist;
      });

      it('selects an icon when clicked', async function () {
        await visit('/browse/maps/_/map-view/spotlight');

        await click('.btn-icon');

        expect(currentURL()).to.contain('?icons=1');
      });
    });
  });
});
