/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | view/map-list-text',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::MapNameList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a list of maps with links when the value is set', async function () {
    this.set('value', [
      {
        id: '1',
        name: 'map 1',
      },
      {
        id: '2',
        name: 'map 2',
      },
    ]);

    await render(hbs`<View::MapNameList @value={{this.value}}/>`);
    const links = this.element.querySelectorAll('a');

    expect(links[0].textContent.trim()).to.equal('map 1');
    expect(links[1].textContent.trim()).to.equal('map 2');
  });
});
