/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputLayoutContainersComponent extends Component {
  @tracked _containers;

  get containers() {
    if (this._containers) {
      return this._containers;
    }

    return this.args.page?.layoutContainers ?? [];
  }

  get selectedContainer() {
    const path = this.args.selectionId ?? '';
    const pieces = path.split('.').map((a) => parseInt(a));

    return pieces[0];
  }

  @action
  removeContainer(index) {
    this.args.page?.deleteContainer(index);

    this._containers = this.args.page?.layoutContainers ?? [];
  }

  @action
  configContainer(index) {
    this.args.onSelect?.('container', [index], this.containers[index]);
  }

  @action
  addContainerBefore(index) {
    this.args.page?.addLayoutContainerBefore(index);
    this.args.page?.addLayout(index, 0, 0);

    this._containers = this.args.page?.layoutContainers;
  }

  @action
  addContainerEnd() {
    this.args.page?.addLayout(this.containers.length, 0, 0);

    this._containers = this.args.page?.layoutContainers;
  }
}
