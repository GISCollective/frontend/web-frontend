/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import AttributeDefinition from '../../lib/attribute-definition';
import { guidFor } from '@ember/object/internals';
import { A } from 'core/lib/array';

export default class ManageIconAttributesComponent extends Component {
  elementId = 'icon-attributes-' + guidFor(this);
  @tracked value = A();
  @tracked types = [
    'short text',
    'long text',
    'integer',
    'decimal',
    'boolean',
    'date',
    'options',
    'options with other',
  ];

  @action
  attributeTypeChanged(index, value) {
    if (this.value[index].type == value) {
      return;
    }

    const newItem = new AttributeDefinition(this.value[index]);
    newItem.type = value;

    this.value.splice(index, 1, newItem);
  }

  @action
  attributeOptionsChanged(index, value) {
    if (this.value[index].options == value) {
      return;
    }

    const newItem = new AttributeDefinition(this.value[index]);
    newItem.options = value;

    this.value.splice(index, 1, newItem);
  }

  @action
  addAttribute() {
    this.value.push(
      new AttributeDefinition({
        name: 'new attribute',
        type: 'short text',
      }),
    );
  }

  @action
  setup() {
    const originalList = this.args.value || [];

    this.value = A(originalList.map((a) => new AttributeDefinition(a)));
  }

  @action
  cancel() {
    return this.args.onCancel(...arguments);
  }

  @action
  save() {
    return this.args.onSave(this.value || []);
  }

  @action
  deleteItem(index) {
    this.value.splice(index, 1);
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);
  }
}
