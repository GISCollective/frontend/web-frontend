import Component from '@glimmer/component';
import { action } from '@ember/object';
import { GeoJson } from 'core/lib/geoJson';
import PositionDetails from 'core/lib/positionDetails';

export default class GroupMapComponent extends Component {
  get position() {
    return JSON.stringify(this.args.position);
  }

  @action
  select() {
    this.args.onChange(new GeoJson({ type: 'Point', coordinates: [10, 20] }), new PositionDetails({ type: 'manual' }));
  }
}
