/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorAttributeComponent extends Component {
  @tracked _source;
  @tracked _title;
  @tracked _attributeStyle;
  @tracked _attributeField;

  get source() {
    if (this._source) {
      return this._source;
    }

    return this.args.value?.data?.source ?? {};
  }

  get title() {
    if (this._title) {
      return this._title;
    }

    return this.args.value?.data?.title ?? {};
  }

  get attributeStyle() {
    if (this._attributeStyle) {
      return this._attributeStyle;
    }

    return this.args.value?.data?.attributeStyle ?? {};
  }

  get attributeField() {
    if (this._attributeField) {
      return this._attributeField;
    }

    return this.args.value?.data?.sourceAttribute?.field ?? "";
  }

  triggerChange() {
    return this.args.onChange?.({
      source: this.source,
      sourceAttribute: {
        field: this.attributeField,
      },
      title: this.title,
      attributeStyle: this.attributeStyle,
    });
  }

  @action
  change(key, value) {
    this[`_${key}`] = value;

    this.triggerChange();
  }
}
