/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class BrowsePrimaryPictureComponent extends Component {
  @service fastboot;

  get pictures() {
    if (!this.args.pictures || this.args.pictures.length == 0) {
      return [];
    }

    if (this.args.pictures.toArray) {
      return this.args.pictures.toArray();
    }

    return this.args.pictures;
  }

  get primaryImage() {
    if (this.pictures.length == 0) {
      return null;
    }

    return this.pictures[0];
  }
}
