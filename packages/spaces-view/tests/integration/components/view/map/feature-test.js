/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

let testJsonPoint = {
  type: 'Point',
  coordinates: [102.0, 10.0],
};

let testJsonLine = {
  type: 'LineString',
  coordinates: [
    [102.0, 50.0],
    [103.0, 51.0],
    [104.0, 50.0],
    [105.0, 51.0],
  ],
};

describe('Integration | Component | view/map/feature', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let map;
  let baseMap1;
  let baseMap2;
  let store;
  let feature;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIcon();

    map = server.testData.storage.addDefaultMap();
    baseMap1 = server.testData.storage.addDefaultBaseMap('1');
    baseMap2 = server.testData.storage.addDefaultBaseMap('2');
    server.testData.storage.addDefaultPicture();

    baseMap1.attributions = [
      {
        name: '© OpenStreetMap contributors',
        url: 'https://www.openstreetmap.org/copyright',
      },
    ];

    baseMap1['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    baseMap2['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    map.id = map._id;
    map.baseMaps.list = [baseMap1._id, baseMap2._id];
    map.license = {
      name: 'CC BY-NC 4.0',
      url: 'https://creativecommons.org/licenses/by-nc/4.0/',
    };
  });

  hooks.after(async function () {
    await server.shutdown();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    await store.findAll('icon');
  });

  describe('when the selectedModel is a point feature', function (hooks) {
    hooks.beforeEach(async function () {
      const featureData = server.testData.storage.addDefaultFeature();
      featureData.position = testJsonPoint;
      feature = await store.findRecord('feature', featureData._id);
    });

    it('can render all elements', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
          elements: {
            fullscreen: true,
            zoom: true,
            mapLicense: true,
            baseMapLicense: true,
            download: true,
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();

      expect(features.length).to.equal(1);

      expect(
        this.element.querySelector('.map-license').textContent.trim(),
      ).to.contain('CC BY-NC 4.0');
      expect(
        this.element.querySelector('.base-map-license').textContent.trim(),
      ).to.contain('© OpenStreetMap contributors');
      expect(this.element.querySelector('.btn-zoom-in')).to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
      expect(this.element.querySelector('.btn-fullscreen')).to.exist;
    });

    it('does not render the download gpx button', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.btn-get-gpx')).not.to.exist;
    });
  });

  describe('when the selectedModel is a line feature', function (hooks) {
    hooks.beforeEach(async function () {
      const featureData = server.testData.storage.addDefaultFeature();
      featureData.position = testJsonLine;
      feature = await store.findRecord('feature', featureData._id);
    });

    it('renders the download gpx button', async function () {
      this.set('value', {
        data: {
          source: {
            useSelectedModel: false,
            model: 'feature',
            id: '1',
          },
          elements: {
            fullscreen: true,
            zoom: true,
            license: true,
            download: true,
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.btn-get-gpx')).to.exist;
      expect(
        this.element
          .querySelector('.btn-get-gpx')
          .attributes.getNamedItem('href').value,
      ).to.contain('/mock-server/features/5ca78e2160780601008f69e6?format=gpx');
    });
  });

  describe('when the border options are set', function (hooks) {
    hooks.beforeEach(async function () {
      const featureData = server.testData.storage.addDefaultFeature();
      featureData.position = testJsonLine;
      feature = await store.findRecord('feature', featureData._id);
    });

    it('adds the border classes', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
          border: { classes: ['border-visibility-yes'] },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.fullscreen-container')).to.have.class(
        'border-visibility-yes',
      );
    });
  });

  describe('map elements', function (hooks) {
    hooks.beforeEach(async function () {
      const featureData = server.testData.storage.addDefaultFeature();
      featureData.position = testJsonLine;
      feature = await store.findRecord('feature', featureData._id);
    });

    it('does not render the fullscreen button when the element.fullscreen is false', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
          border: { classes: ['border-visibility-yes'] },
          elements: {
            fullscreen: false,
            zoom: true,
            mapLicense: true,
            baseMapLicense: true,
            download: true,
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.btn-fullscreen')).not.to.exist;
      expect(this.element.querySelector('.btn-zoom-in')).to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
      expect(this.element.querySelector('.btn-get-gpx')).to.exist;
      expect(this.element.querySelector('.map-license')).to.exist;
      expect(this.element.querySelector('.base-map-license')).to.exist;
    });

    it('does not render the zoom buttons when the element.zoom is false', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
          border: { classes: ['border-visibility-yes'] },
          elements: {
            fullscreen: true,
            zoom: false,
            mapLicense: true,
            baseMapLicense: true,
            download: true,
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.btn-zoom-in')).not.to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).not.to.exist;
      expect(this.element.querySelector('.btn-fullscreen')).to.exist;
      expect(this.element.querySelector('.btn-get-gpx')).to.exist;
      expect(this.element.querySelector('.map-license')).to.exist;
      expect(this.element.querySelector('.base-map-license')).to.exist;
    });

    it('does not render the download buttons when the element.download is false', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
          border: { classes: ['border-visibility-yes'] },
          elements: {
            fullscreen: true,
            zoom: true,
            mapLicense: true,
            baseMapLicense: true,
            download: false,
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.btn-get-gpx')).not.to.exist;
      expect(this.element.querySelector('.btn-zoom-in')).to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
      expect(this.element.querySelector('.btn-fullscreen')).to.exist;
      expect(this.element.querySelector('.map-license')).to.exist;
      expect(this.element.querySelector('.base-map-license')).to.exist;
    });

    it('does not render the map license when the element.mapLicense is false', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
          border: { classes: ['border-visibility-yes'] },
          elements: {
            fullscreen: true,
            zoom: true,
            mapLicense: false,
            baseMapLicense: true,
            download: true,
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.map-license')).not.to.exist;
      expect(this.element.querySelector('.base-map-license')).to.exist;
      expect(this.element.querySelector('.btn-get-gpx')).to.exist;
      expect(this.element.querySelector('.btn-zoom-in')).to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
      expect(this.element.querySelector('.btn-fullscreen')).to.exist;
    });

    it('does not render the base map license when the element.mapLicense is false', async function () {
      this.set('value', {
        data: {
          source: { useSelectedModel: false, model: 'feature', id: '1' },
          border: { classes: ['border-visibility-yes'] },
          elements: {
            fullscreen: true,
            zoom: true,
            mapLicense: true,
            baseMapLicense: false,
            download: true,
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('model', {
        selectedModel: feature,
      });

      await render(
        hbs`<View::Map::Feature @value={{this.value}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('.map-license')).to.exist;
      expect(this.element.querySelector('.base-map-license')).not.to.exist;
      expect(this.element.querySelector('.btn-get-gpx')).to.exist;
      expect(this.element.querySelector('.btn-zoom-in')).to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
      expect(this.element.querySelector('.btn-fullscreen')).to.exist;
    });
  });
});
