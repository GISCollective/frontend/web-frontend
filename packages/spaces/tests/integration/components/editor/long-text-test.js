/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/long-text", function (hooks) {
  setupRenderingTest(hooks);

  it("renders an empty value by default", async function () {
    await render(hbs`<Editor::LongText />`);
    expect(this.element.textContent.trim()).to.equal("");
  });

  it("renders the provided value", async function () {
    this.set("value", "some value");
    await render(hbs`<Editor::LongText @value={{this.value}}/>`);

    expect(this.element.querySelector(".input-long-text-field").value).to.equal("some value");
  });

  it("triggers the on change event when a new value is typed", async function () {
    let value;

    this.set("onChange", (v) => {
      value = v;
    });

    this.set("value", "some value");
    await render(hbs`<Editor::LongText @onChange={{this.onChange}} @value={{this.value}}/>`);

    await fillIn(".input-long-text-field", "new value");

    expect(this.element.querySelector(".input-long-text-field").value).to.equal("new value");
    expect(value).to.equal("new value");
  });
});
