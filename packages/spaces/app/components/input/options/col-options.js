/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";
import { defaultBsProperty, sizedBsProperty, setDefaultBsProperty } from "core/lib/bs-cols";
import { tracked } from "@glimmer/tracking";

export default class InputColOptionsComponent extends Component {
  elementId = `input-col-options-${guidFor(this)}`;

  @tracked _componentCount;
  @tracked _name;
  @tracked _options;

  availableWidths = ["default", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
  allDisplayValues = ["default", "block", "inline-block"];
  availableMargins = ["default", "0", "1", "2", "3", "4", "5"];

  get componentCount() {
    if (!isNaN(this._componentCount)) {
      return this._componentCount;
    }

    return this.args.col?.componentCount ?? 1;
  }

  set componentCount(value) {
    this._componentCount = value;
    this.triggerChange();
  }

  get displayMode() {
    return defaultBsProperty("display-content", this.options) ?? this.allDisplayValues[0];
  }

  get mobileWidth() {
    return defaultBsProperty("col", this.options);
  }

  get tabletWidth() {
    return sizedBsProperty("col", "md", this.options);
  }

  get desktopWidth() {
    return sizedBsProperty("col", "lg", this.options);
  }

  get componentMargin() {
    return defaultBsProperty("component-m", this.options);
  }

  get options() {
    if (this._options) {
      return this._options;
    }

    return this.args.col?.options ?? [];
  }

  get name() {
    if (this._name !== undefined) {
      return this._name;
    }

    return this.args.col?.name ?? "";
  }

  set name(value) {
    this._name = value;

    this.triggerChange();
  }

  triggerChange() {
    let componentCount = parseInt(this.componentCount);

    if (isNaN(componentCount) || componentCount < 1) {
      componentCount = 1;
    }

    this.args.onChangeOptions?.({
      options: this.options,
      name: this.name,
      componentCount,
    });
  }

  @action
  reset() {
    this._name = undefined;
    this._options = undefined;
    this._componentCount = undefined;
  }

  @action
  componentMarginChange(value) {
    this._options = setDefaultBsProperty("component-m", value, this.options);
    this.triggerChange();
  }

  @action
  changeDisplayMode(value) {
    this._options = setDefaultBsProperty("display-content", value, this.options);
    this.triggerChange();
  }

  @action
  changeOptions(value) {
    this._options = value;

    this.triggerChange();
  }
}
