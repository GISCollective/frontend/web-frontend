/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { A } from '../lib/array';

export default class LayerTransform extends Transform {
  deserialize(serialized) {
    const list = A([]);

    serialized.forEach((element) => {
      list.push(new BaseMapLayer(element));
    });

    return list;
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class BaseMapLayer {
  @tracked type = '';
  @tracked options = {};

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (serialized.type) {
      this.type = serialized.type;
    }

    if (serialized.options) {
      this.options = serialized.options;
    }
  }

  toJSON() {
    return {
      type: this.type,
      options: this.options,
    };
  }
}
