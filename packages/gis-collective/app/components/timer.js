/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later, cancel } from '@ember/runloop';

export default class TimerComponent extends Component {
  @action
  setup() {
    if (this.isDestroying || this.isDestroyed || !this.args.interval) {
      return;
    }

    this.timerId = later(async () => {
      if (this.isDestroying || this.isDestroyed) {
        return;
      }

      await this.args.onTick?.();
      this.setup();
    }, this.args.interval);
  }

  willDestroy() {
    super.willDestroy(...arguments);

    cancel(this.timerId);
  }
}
