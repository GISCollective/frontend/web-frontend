/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageBaseMapsEditController extends Controller {
  @service store;
  @service notifications;
  @service ol;
  @service intl;
  @service user;
  @service router;

  @tracked editableField;
  @tracked savingField;
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        text: this.model.baseMap.name,
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  get attributions() {
    try {
      return this.model.baseMap.attributions.slice();
    } catch (err) {
      return [];
    }
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.baseMap.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      },
    );
  }

  @action
  saveAttributions(value) {
    this.model.baseMap.set('attributions', value);
    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.model.baseMap.rollbackAttributes();
    this.editableField = '';
  }

  @action
  delete() {
    return this.model.baseMap.destroyRecord().then(() => {
      return this.router.transitionTo('manage.dashboards.dashboard', this.model.team.id);
    });
  }

  @action
  saveCover(value) {
    this.model.baseMap.set('cover', value);
    return this.save();
  }

  @action
  saveLayers(value) {
    this.model.baseMap.set('layers', value);

    return this.save();
  }

  @action
  saveIsPublic(newValue) {
    this.model.baseMap.visibility.isPublic = newValue;
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.baseMap.visibility.team = newValue;
    this.editableField = this.intl.t('team');

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.baseMap.visibility.isDefault = newValue;
    this.editableField = this.intl.t('is default');

    return this.save();
  }

  @action
  saveName(newValue) {
    this.model.baseMap.set('name', newValue);

    return this.save();
  }
}
