/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import {
  click,
  render,
  fillIn,
  triggerEvent,
  waitFor,
  waitUntil,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { DateTime } from 'luxon';

describe('Integration | Component | view/filters', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let mapData;
  let store;
  let state;
  let teamData;
  let team;
  let events;
  let calendar;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIconSet();
    mapData = server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultGeocoding();
    server.testData.storage.addDefaultFeature();
    server.testData.storage.addDefaultTeam('5ca78e2160780601008f69e6');
    teamData = server.testData.storage.addDefaultTeam();
    calendar = server.testData.storage.addDefaultCalendar();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };

    store = this.owner.lookup('service:store');
    server.history = [];

    state = '';
    this.set('state', '');
    this.set('onChangeState', (s) => {
      state = s;

      this.set('state', s);
    });

    team = await store.findRecord('team', teamData._id);

    events = [];

    server.get(
      `/mock-server/calendars/5cc8dc1038e882010061221/attributes`,
      (request) => {
        events.push(request.queryParams);
        return [200, {}, '["value1", "value2"]'];
      },
    );

    server.get(
      `/mock-server/maps/5ca89e37ef1f7e010007f54c/attributes`,
      (request) => {
        events.push(request.queryParams);
        return [200, {}, '["value1", "value2"]'];
      },
    );
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Filters />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the icons and location filters when they are set', async function () {
    this.set('value', {
      data: {
        filters: [{ type: 'icons' }, { type: 'location' }],
        style: {},
      },
    });

    await render(hbs`<View::Filters @value={{this.value}} />`);

    expect(this.element.querySelector('.state-filters-icons')).to.exist;
    expect(this.element.querySelector('.state-filters-location')).to.exist;
  });

  it('renders the icons with the requested icon set', async function (a) {
    this.set('value', {
      data: {
        filters: [
          {
            type: 'icons',
            options: {
              label: 'new value',
              'icon-set': '3',
              source: 'icon-set',
              sourceModelKey: 'selected_model',
            },
          },
        ],
        style: {},
      },
    });

    server.history = [];
    await render(hbs`<View::Filters @value={{this.value}} />`);

    expect(this.element.querySelector('.btn-pill').textContent.trim()).to.equal(
      'new value',
    );
    expect(server.history).to.deep.equal([
      'GET /mock-server/icons?grupset=true&iconSet=3&group=true',
    ]);
  });

  it('renders the classes (legacy)', async function () {
    this.set('value', {
      data: {
        filters: [{ type: 'icons' }, { type: 'location' }],
        style: {
          classes: ['justify-content-center'],
        },
      },
    });

    await render(hbs`<View::Filters @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-filters')).to.have.class(
      'justify-content-center',
    );
  });

  it('renders the classes ', async function () {
    this.set('value', {
      data: {
        filters: [{ type: 'icons' }, { type: 'location' }],
        container: {
          classes: ['justify-content-center'],
        },
      },
    });

    await render(hbs`<View::Filters @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-filters')).to.have.class(
      'justify-content-center',
    );
  });

  it('triggers the onChangeState event when an icon is selected', async function () {
    const map = await store.findRecord('map', mapData._id);

    this.set('value', {
      data: {
        filters: [
          {
            type: 'icons',
            options: { source: 'model', sourceModelKey: 'map' },
          },
          { type: 'location' },
        ],
        style: {},
      },
    });
    let value;

    this.set('state', '');
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    this.set('model', {
      map,
    });

    await render(
      hbs`<View::Filters @value={{this.value}} @state={{this.state}} @model={{this.model}} @onChangeState={{this.onChangeState}} />`,
    );

    await click('.btn-pill-icon');
    await waitFor('.btn-icon');
    await click('.btn-icon');

    expect(value).to.equal('if@5ca7bfc0ecd8490100cab980');
  });

  it('triggers the onChangeState event when a location is selected', async function () {
    this.set('value', {
      data: {
        filters: [{ type: 'icons' }, { type: 'location' }],
        style: {},
      },
    });
    let value;

    this.set('state', '');
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    await render(
      hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    await click('.btn-pill-location');

    await fillIn('.input-filter', 'some place');
    await triggerEvent('.input-filter', 'change');

    await click('.list-group-item');

    expect(value).to.equal(
      'l@Berlin, Coos County, New Hampshire, 03570, United States',
    );
  });

  describe('the map feature filter', function () {
    it('can search a feature by name when the map is an id', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'map feature',
              options: {
                label: 'some label',
                'placeholder-text': 'placeholder',
                map: '2',
                'with-search': true,
              },
            },
          ],
          style: {},
        },
      });

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('some label');

      await click('.btn-pill');
      expect(
        this.element.querySelector('.popover-body label').textContent.trim(),
      ).to.equal('placeholder');

      server.history = [];
      await fillIn('.input-filter', 'some message');
      await triggerEvent('.input-filter', 'change');

      await click('.list-group-item');

      await waitUntil(() => state);

      expect(server.history).to.contain(
        'GET /mock-server/features?limit=10&map=2&sortBy=name&sortOrder=asc&term=some%20message',
      );

      expect(state).to.equal('ffid@5ca78e2160780601008f69e6');
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
    });

    it('can search a feature by name when the map is an object with id', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'map feature',
              options: {
                label: 'some label',
                'placeholder-text': 'placeholder',
                map: { id: '2' },
                'with-search': true,
              },
            },
          ],
          style: {},
        },
      });

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      await click('.btn-pill');

      server.history = [];
      await fillIn('.input-filter', 'some message');
      await triggerEvent('.input-filter', 'change');

      await click('.list-group-item');

      await waitUntil(() => state);

      expect(server.history).to.contain(
        'GET /mock-server/features?limit=10&map=2&sortBy=name&sortOrder=asc&term=some%20message',
      );

      expect(state).to.equal('ffid@5ca78e2160780601008f69e6');
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
    });

    it('can not search a feature when with-search is false', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'map feature',
              options: {
                label: 'some label',
                'placeholder-text': 'placeholder',
                map: { id: '2' },
                'with-search': false,
              },
            },
          ],
          style: {},
        },
      });

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      await click('.btn-pill');

      server.history = [];

      await click('.list-group-item');

      await waitUntil(() => state);

      expect(server.history).to.contain(
        'GET /mock-server/features?map=2&sortBy=name&sortOrder=asc',
      );

      expect(state).to.equal('ffid@5ca78e2160780601008f69e6');
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
    });

    it('shows the feature name when the state is set', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'map feature',
              options: {
                label: 'some label',
                'placeholder-text': 'placeholder',
                map: '2',
                'with-search': true,
              },
            },
          ],
          style: {},
        },
      });

      this.set('state', 'ffid@5ca78e2160780601008f69e6');

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
    });
  });

  describe('the event repetition filter', function () {
    it('can select an event repetition', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'event repetition',
              options: {
                label: 'some label',
                norepeat: true,
                daily: true,
                weekly: true,
                monthly: true,
                yearly: true,
              },
            },
          ],
          style: {},
        },
      });

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('some label');

      await click('.btn-pill');

      await click('.btn-value-daily');

      await waitUntil(() => state);

      expect(state).to.equal('er@daily');
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('daily');
    });

    it('can show only 2 options', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'event repetition',
              options: {
                label: 'some label',
                norepeat: true,
                daily: false,
                weekly: true,
                monthly: false,
                yearly: false,
              },
            },
          ],
          style: {},
        },
      });

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      await click('.btn-pill');

      const values = [
        ...this.element.querySelectorAll('.popover-body .btn'),
      ].map((a) => a.textContent.trim());

      expect(values).to.deep.equal(['One off', 'Weekly']);
    });
  });

  describe('the date filter', function () {
    it('can select a date', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'date',
              options: {
                label: 'some label',
              },
            },
          ],
          style: {},
        },
      });

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      const now = DateTime.now();

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('some label');

      await click('.btn-pill');
      await click('.day.focused');

      await waitUntil(() => state);

      expect(state).to.equal(
        `da@${now.toISODate()}T12:00:00.000+0${now.offset / 60}:00`,
      );
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal(now.toLocaleString());
    });
  });

  describe('the day of week filter', function () {
    it('can select a date', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'day of week',
              options: {
                label: 'some label',
              },
            },
          ],
          style: {},
        },
      });

      await render(
        hbs`<View::Filters @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      await click('.btn-pill');
      const btns = this.element.querySelectorAll('.btn-day');
      await click(btns[3]);

      await waitUntil(() => state);

      expect(state).to.equal(`dow@3`);
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('Thursday');
    });
  });

  describe('the calendar attributes filter', function () {
    it('can select a value', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'calendar attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
                calendar: calendar._id,
                'with-search': true,
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      this.set('state', 'at@name|other value');

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('some label');

      await click('.btn-pill');
      expect(
        this.element.querySelector('.popover-body label').textContent.trim(),
      ).to.equal('some placeholder');

      expect(events).to.deep.equal([
        { attribute: 'icon.name', term: '', limit: '5' },
      ]);

      await click('.list-group-item');

      await waitUntil(() => state);

      expect(state).to.equal(`at@name|other value|icon.name|value1`);
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('value1');
    });

    it('can show the selected value', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'calendar attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      this.set('state', `at@name|other value|icon.name|value2`);

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('value2');
    });

    it('can reset the selected value', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'calendar attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      this.set('state', `at@name|other value|icon.name|value2`);

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      await click('.btn-reset');

      await waitUntil(() => state);

      expect(state).to.equal(`at@name|other value`);
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('some label');
    });

    it('can search for a value when the with-search option is enabled', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'calendar attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
                calendar: calendar._id,
                'with-search': true,
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      server.history = [];
      await click('.btn-pill');

      await fillIn('.input-filter', 'some value');
      await waitUntil(() => server.history.length);

      expect(server.history).to.deep.equal([
        'GET /mock-server/calendars/5cc8dc1038e882010061221/attributes?attribute=icon.name&term=some value&limit=10',
      ]);
    });
  });

  describe('the feature attributes filter', function () {
    it('can select a value', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'feature attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
                map: mapData._id,
                'with-search': true,
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      this.set('state', 'at@name|other value');

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('some label');

      await click('.btn-pill');

      expect(
        this.element.querySelector('.popover-body label').textContent.trim(),
      ).to.equal('some placeholder');

      expect(events).to.deep.equal([
        { attribute: 'icon.name', term: '', limit: '5' },
      ]);

      await click('.list-group-item');

      await waitUntil(() => state);

      expect(state).to.equal(`at@name|other value|icon.name|value1`);
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('value1');
    });

    it('can show the selected value', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'feature attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      this.set('state', `at@name|other value|icon.name|value2`);

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('value2');
    });

    it('can reset the selected value', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'feature attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      this.set('state', `at@name|other value|icon.name|value2`);

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      await click('.btn-reset');

      await waitUntil(() => state);

      expect(state).to.equal(`at@name|other value`);
      expect(
        this.element.querySelector('.btn-pill').textContent.trim(),
      ).to.equal('some label');
    });

    it('can search for a value when the with-search option is enabled', async function (a) {
      this.set('value', {
        data: {
          filters: [
            {
              type: 'feature attribute',
              options: {
                label: 'some label',
                'placeholder-text': 'some placeholder',
                'attribute-name': 'icon.name',
                map: mapData._id,
                'with-search': true,
              },
            },
          ],
          style: {},
        },
      });

      this.set('model', { team });

      await render(
        hbs`<View::Filters @value={{this.value}} @model={{this.model}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
      );

      server.history = [];
      await click('.btn-pill');

      await fillIn('.input-filter', 'some value');

      await waitUntil(() => server.history.length);

      expect(server.history).to.deep.equal([
        `GET /mock-server/maps/${mapData._id}/attributes?attribute=icon.name&term=some value&limit=10`,
      ]);
    });
  });
});
