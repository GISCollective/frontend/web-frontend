/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';
import { action } from '@ember/object';

export default class WindowService extends Service {
  @tracked width;
  @tracked height;
  @tracked documentWidth;
  @tracked documentHeight;
  @tracked scrollX;
  @tracked scrollY;
  @tracked scrollDirection;

  @tracked scrollPercentageX;
  @tracked scrollPercentageY;

  @tracked scrollPageX;
  @tracked scrollPageY;

  updatePercentage() {
    this.scrollPercentageY = this.scrollY / this.documentHeight;
    this.scrollPercentageX = this.scrollX / this.documentWidth;

    this.scrollPageY = this.scrollY / this.height;
    this.scrollPageX = this.scrollX / this.width;
  }

  updateSize() {
    this.width = this.window.innerWidth;
    this.height = this.window.innerHeight;
    this.documentWidth = document.body.scrollWidth;
    this.documentHeight = document.body.scrollHeight;
  }

  @action
  setScrollProperties() {
    if (this.window.scrollY < this.scrollY) {
      this.scrollDirection = 'up';
    }

    if (this.window.scrollY > this.scrollY) {
      this.scrollDirection = 'down';
    }

    this.scrollY = this.window.scrollY;
    this.scrollX = this.window.scrollX;
  }

  updateScroll() {
    debounce(this, this.setScrollProperties, 10);
  }

  setup(window) {
    this.window = window;
    this.updateSize();
    this.updateScroll();
    this.updatePercentage();

    this.resizeObserver = new ResizeObserver(() => {
      window?.requestAnimationFrame(() => {
        this.updateSize();
        this.updateScroll();
        this.updatePercentage();
      });
    });

    this.resizeObserver.observe(document.body);

    window.addEventListener('resize', () => {
      window.requestAnimationFrame(() => {
        this.updateSize();
        this.updatePercentage();
      });
    });

    window.addEventListener('scroll', () => {
      window.requestAnimationFrame(() => {
        this.updateScroll();
        this.updatePercentage();
      });
    });
  }
}
