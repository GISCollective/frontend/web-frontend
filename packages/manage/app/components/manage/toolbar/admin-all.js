/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
export default class ManageToolbarAdminAllComponent extends Component {
  @action
  change() {
    if (this.args.change) {
      this.args.change(!this.args.value);
    }

    if (this.args.onChange) {
      this.args.onChange(!this.args.value);
    }
  }
}
