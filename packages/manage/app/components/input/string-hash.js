/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { A } from 'core/lib/array';

export default class InputStringHashComponent extends Component {
  @tracked pairs;

  @action
  setup() {
    this.pairs = A(
      Object.keys(this.args.value ?? {}).map(
        (key) =>
          new TranslationPairs(key, this.args.value[key], () => {
            this.change();
          }),
      ),
    );
  }

  change() {
    const values = {};

    this.pairs.forEach((pair) => {
      values[pair.key] = pair.value;
    });

    this.args.onChange?.(values);
  }

  @action
  addItem() {
    this.pairs.push(
      new TranslationPairs('', '', () => {
        this.change();
      }),
    );
    this.change();
  }
}

class TranslationPairs {
  constructor(key, value, changeEvent) {
    this._key = key;
    this._value = value;
    this.changeEvent = changeEvent;
  }

  get key() {
    return this._key;
  }

  set key(value) {
    this._key = value;
    this.changeEvent?.();
  }

  get value() {
    return this._value;
  }

  set value(value) {
    this._value = value;
    this.changeEvent?.();
  }
}
