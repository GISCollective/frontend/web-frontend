/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { LayoutRow, LayoutCol } from 'models/transforms/layout-row-list';

describe('Unit | Transform | layout row list', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:layout-row-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize([
      {
        options: ['option 1', 'option 2'],
        cols: [
          {
            type: 'type',
            data: {},
            options: ['option 1', 'option 2'],
            name: '',
          },
        ],
      },
    ]);

    expect(deserialized.length).to.deep.equal(1);
    expect(deserialized[0].cols.length).to.deep.equal(1);
  });

  it('can serialize an object', function (a) {
    let deserialized = transform.deserialize([
      {
        cols: [
          {
            type: 'type',
            data: {},
            name: '',
            componentCount: 1,
            options: ['option 1', 'option 2'],
          },
        ],
        options: ['option 1', 'option 2'],
      },
    ]);

    a.deepEqual(transform.serialize(deserialized), [
      {
        name: '',
        data: {},
        cols: [
          {
            type: 'type',
            data: {},
            name: '',
            componentCount: 1,
            options: ['option 1', 'option 2'],
          },
        ],
        options: ['option 1', 'option 2'],
      },
    ]);
  });

  describe('LayoutRow', function () {
    describe('updateIndex', function () {
      it('does nothing when the name is empty', function () {
        const row = new LayoutRow();

        row.updateIndex(1);

        expect(row.name).to.equal('');
      });

      it('updates the index when it has a default name', function () {
        const row = new LayoutRow({ name: '1.2' });

        row.updateIndex(3);

        expect(row.name).to.equal('1.3');
      });

      it('does not update the index when the name has letters', function () {
        const row = new LayoutRow({ name: 'a.b' });

        row.updateIndex(3);

        expect(row.name).to.equal('a.b');
      });

      it('updates the col indexes', function () {
        const row = new LayoutRow({
          name: '1.2',
          cols: [new LayoutCol({ name: '1.2.0' }), new LayoutCol({ name: '1.2.1' })],
        });

        row.updateIndex(3);

        expect(row.cols[0].name).to.equal('1.3.0');
        expect(row.cols[1].name).to.equal('1.3.1');
      });
    });

    describe('updateContainerIndex', function () {
      it('does nothing when the name is empty', function () {
        const row = new LayoutRow();

        row.updateContainerIndex(1);

        expect(row.name).to.equal('');
      });

      it('updates the index when it has a default name', function () {
        const row = new LayoutRow({ name: '1.2' });

        row.updateContainerIndex(3);

        expect(row.name).to.equal('3.2');
      });

      it('does not update the index when the name has letters', function () {
        const row = new LayoutRow({ name: 'a.b' });

        row.updateContainerIndex(3);

        expect(row.name).to.equal('a.b');
      });

      it('updates the col indexes', function () {
        const row = new LayoutRow({
          name: '1.2',
          cols: [new LayoutCol({ name: '1.2.0' }), new LayoutCol({ name: '1.2.1' })],
        });

        row.updateContainerIndex(3);

        expect(row.cols[0].name).to.equal('3.2.0');
        expect(row.cols[1].name).to.equal('3.2.1');
      });
    });
  });

  describe('LayoutCol', function () {
    describe('updateIndex', function () {
      it('does nothing when the name is empty', function () {
        const row = new LayoutCol();

        row.updateIndex(1);

        expect(row.name).to.equal('');
      });

      it('updates the index when it has a default name', function () {
        const row = new LayoutCol({ name: '1.2.3' });

        row.updateIndex(4);

        expect(row.name).to.equal('1.2.4');
      });

      it('does not update the index when the name has letters', function () {
        const row = new LayoutCol({ name: 'a.b.c' });

        row.updateIndex(3);

        expect(row.name).to.equal('a.b.c');
      });
    });

    describe('updateRowIndex', function () {
      it('does nothing when the name is empty', function () {
        const row = new LayoutCol();

        row.updateRowIndex(1);

        expect(row.name).to.equal('');
      });

      it('updates the index when it has a default name', function () {
        const row = new LayoutCol({ name: '1.2.3' });

        row.updateRowIndex(4);

        expect(row.name).to.equal('1.4.3');
      });

      it('does not update the index when the name has letters', function () {
        const row = new LayoutCol({ name: 'a.b.c' });

        row.updateRowIndex(3);

        expect(row.name).to.equal('a.b.c');
      });
    });

    describe('updateContainerIndex', function () {
      it('does nothing when the name is empty', function () {
        const row = new LayoutCol();

        row.updateContainerIndex(1);

        expect(row.name).to.equal('');
      });

      it('updates the index when it has a default name', function () {
        const row = new LayoutCol({ name: '1.2.3' });

        row.updateContainerIndex(4);

        expect(row.name).to.equal('4.2.3');
      });

      it('does not update the index when the name has letters', function () {
        const row = new LayoutCol({ name: 'a.b.c' });

        row.updateContainerIndex(3);

        expect(row.name).to.equal('a.b.c');
      });
    });

    describe('names', function () {
      it('returns a list with an empty string when the col has no name', function () {
        const col = new LayoutCol();
        expect(col.names).to.deep.equal(['']);
      });

      it('returns a list with the col name when the col has a name', function () {
        const col = new LayoutCol();
        col.name = 'name';
        expect(col.names).to.deep.equal(['name']);
      });

      it('returns the sub component names when there componentCount > 1', function () {
        const col = new LayoutCol();
        col.name = 'name';
        col.componentCount = 3;
        expect(col.names).to.deep.equal(['name', 'name.1', 'name.2']);
      });
    });
  });
});
