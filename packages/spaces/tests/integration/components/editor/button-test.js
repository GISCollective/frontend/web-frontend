/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/button", function (hooks) {
  setupRenderingTest(hooks);

  describe("the link buttons", function (hooks) {
    let changedValue;

    hooks.beforeEach(function () {
      changedValue = null;

      this.set("onChange", (v) => {
        changedValue = v;
      });

      this.set("value", {
        data: {
          style: {
            classes: ["btn-primary", "size-btn-lg"],
          },
          content: {
            name: "first button",
            type: "default",
            link: "https://giscollective.com/page1",
          },
        },
      });
    });

    it("does not render the link properties when the selected element has data-no-link=true", async function () {
      const element = document.createElement("div");
      element.dataset.noLink = "true";

      this.set("testElement", element);

      await render(
        hbs`<Editor::Button @element={{this.testElement}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      expect(this.element.querySelector(".input-button-type")).not.to.exist;

      expect(this.element.querySelector(".input-button-name")).to.exist;

      expect(this.element.querySelector(".manage-editors-properties-link")).not.to.exist;
    });

    it("renders the right button type", async function () {
      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      expect(this.element.querySelector(".input-button-type").value).to.equal("default");

      expect(this.element.querySelector(".input-button-name").value).to.equal("first button");

      expect(this.element.querySelector(".input-sm-btn").value).to.equal("primary");
    });

    it("can change the style", async function (a) {
      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      expect(this.element.querySelector(".input-button-type").value).to.equal("default");

      expect(this.element.querySelector(".input-button-name").value).to.equal("first button");

      this.element.querySelector(".input-sm-btn").value = "danger";
      await triggerEvent(".input-sm-btn", "change");

      a.deepEqual(changedValue, {
        content: {
          name: "first button",
          type: "default",
          link: "https://giscollective.com/page1",
        },
        pictureOptions: {},
        style: { classes: ["size-btn-lg", "btn-danger"] },
        containerStyle: {},
      });
    });

    it("can change to a color style", async function (a) {
      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      expect(this.element.querySelector(".input-button-type").value).to.equal("default");
      expect(this.element.querySelector(".input-button-name").value).to.equal("first button");

      this.element.querySelector(".input-sm-btn").value = "color";
      await triggerEvent(".input-sm-btn", "change");

      const colors = this.element.querySelectorAll(".input-bs-color");
      await click(colors[0]);
      await click(colors[0].querySelector(".btn-color-primary"));

      await click(colors[1]);
      await click(colors[1].querySelector(".btn-color-secondary"));

      expect(changedValue.style).to.deep.equal({
        classes: ["size-btn-lg", "btn-color", "bg-primary", "text-secondary"],
      });
    });

    it("can change from a color style", async function (a) {
      this.set("value", {
        data: {
          style: {
            classes: ["size-btn-lg", "btn-color", "bg-primary", "text-secondary"],
          },
          content: {
            name: "first button",
            type: "default",
            link: "https://giscollective.com/page1",
          },
        },
      });

      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      expect(this.element.querySelector(".input-button-type").value).to.equal("default");
      expect(this.element.querySelector(".input-button-name").value).to.equal("first button");

      this.element.querySelector(".input-sm-btn").value = "color";
      await triggerEvent(".input-sm-btn", "change");

      const colors = this.element.querySelectorAll(".selected-value");

      expect(colors[0]).to.have.class("bg-primary");
      expect(colors[1]).to.have.class("bg-secondary");

      this.element.querySelector(".input-sm-btn").value = "primary";
      await triggerEvent(".input-sm-btn", "change");

      expect(changedValue.style).to.deep.equal({ classes: ["size-btn-lg", "btn-primary"] });
    });

    it("can change the alignment", async function () {
      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-sm-text").value = "center";
      await triggerEvent(".input-sm-text", "change");

      expect(changedValue).to.deep.equal({
        content: {
          name: "first button",
          type: "default",
          link: "https://giscollective.com/page1",
        },
        pictureOptions: {},
        style: { classes: ["btn-primary", "size-btn-lg"] },
        containerStyle: {
          classes: ["text-center"],
        },
      });
    });

    it("can change the display mode", async function () {
      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-sm-w").value = "50";
      await triggerEvent(".input-sm-w", "change");

      expect(changedValue).to.deep.equal({
        content: {
          name: "first button",
          type: "default",
          link: "https://giscollective.com/page1",
        },
        pictureOptions: {},
        style: { classes: ["btn-primary", "size-btn-lg", "w-50"] },
        containerStyle: {},
      });
    });

    it("can change the open in new tab to false", async function (a) {
      this.set("value", {
        data: {
          style: {},
          content: {
            name: "first button",
            type: "default",
            link: "https://giscollective.com/page1",
            newTab: true,
          },
        },
      });

      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".select-option-in-new-tab").value = "no";
      await triggerEvent(".select-option-in-new-tab", "change");

      a.deepEqual(changedValue, {
        content: {
          name: "first button",
          type: "default",
          link: "https://giscollective.com/page1",
          newTab: false,
          storeType: undefined,
          enablePicture: undefined,
        },
        style: {},
        pictureOptions: {},
        containerStyle: {},
      });
    });

    it("can enable the picture", async function (a) {
      this.set("value", {
        data: {
          style: {},
          pictureOptions: {},
          content: {
            enablePicture: true,
            link: "https://giscollective.com/page1",
            name: "first button",
            newTab: true,
            storeType: undefined,
            type: "default",
          },
        },
      });

      await render(hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".select-option-enable-picture").value = "yes";
      await triggerEvent(".select-option-enable-picture", "change");

      expect(this.element.querySelector(".picture-section")).to.exist;

      a.deepEqual(changedValue, {
        content: {
          enablePicture: true,
          link: "https://giscollective.com/page1",
          name: "first button",
          newTab: true,
          storeType: undefined,
          type: "default",
        },
        pictureOptions: {},
        style: {},
        containerStyle: {},
      });
    });

    it("can set a custom color style", async function (a) {
      this.set("value", {
        data: {
          style: {},
          pictureOptions: {},
          content: {
            enablePicture: true,
            link: "https://giscollective.com/page1",
            name: "first button",
            newTab: true,
            storeType: undefined,
            type: "default",
          },
        },
      });

      this.set("space", {
        getPagesMap: () => [],
        colorPalette: {
          customColors: [{
            name: "Color 1",
            lightValue: "",
            darkValue: ""
          }, {
            name: "Color £ 2",
            lightValue: "",
            darkValue: ""
          }]
        }
      });

      await render(hbs`<Editor::Button @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`);

      let options = [...this.element.querySelectorAll(".input-sm-btn option")].map(a => a.textContent.trim());

      expect(options).to.contain("color-1");
      expect(options).to.contain("color-2");

      this.element.querySelector(".input-sm-btn").value = "color-2";
      await triggerEvent(".input-sm-btn", "change");

      expect(this.element.querySelector(".picture-section")).to.exist;

      a.deepEqual(changedValue, {
        content: {
          enablePicture: true,
          link: "https://giscollective.com/page1",
          name: "first button",
          newTab: true,
          storeType: undefined,
          type: "default",
        },
        pictureOptions: {},
        style: {
          "classes": [
            "btn-color-2"
          ]
        },
        containerStyle: {},
      });
    });
  });

  // describe('the store buttons', function () {
  //   let changedValue;

  //   hooks.beforeEach(function () {
  //     changedValue = null;

  //     this.set('onChange', (v) => {
  //       changedValue = v;
  //     });

  //     this.set('value', {
  //       data: {
  //         style: {
  //           classes: ['btn-primary', 'size-btn-lg'],
  //         },
  //         content: {
  //           name: 'first button',
  //           type: 'app-store',
  //           storeType: 'apple',
  //           link: 'https://giscollective.com/page1',
  //         },
  //       },
  //     });
  //   });

  //   it('renders the right button type', async function () {
  //     await render(hbs`<Editor::Button @value={{this.value}} />`);

  //     expect(this.element.querySelector('.input-button-type').value).to.equal(
  //       'app-store'
  //     );
  //     expect(this.element.querySelector('.input-store-type').value).to.equal(
  //       'apple'
  //     );
  //   });

  //   it('can change the button type', async function () {
  //     await render(
  //       hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`
  //     );

  //     this.element.querySelector('.input-button-type').value = 'default';
  //     await triggerEvent('.input-button-type', 'change');

  //     expect(changedValue).to.deep.equal({
  //       content: {
  //         name: 'first button',
  //         type: 'default',
  //         storeType: 'apple',
  //         link: 'https://giscollective.com/page1',
  //         newTab: undefined
  //       },
  //       style: { classes: ['btn-primary', 'size-btn-lg'] },
  //       containerStyle: {}
  //     });
  //   });

  //   it('can change the store type', async function () {
  //     await render(
  //       hbs`<Editor::Button @value={{this.value}} @onChange={{this.onChange}} />`
  //     );

  //     this.element.querySelector('.input-store-type').value = 'android';
  //     await triggerEvent('.input-store-type', 'change');

  //     expect(changedValue).to.deep.equal({
  //       content: {
  //         name: 'first button',
  //         type: 'app-store',
  //         storeType: 'android',
  //         link: 'https://giscollective.com/page1',
  //         newTab: undefined
  //       },
  //       style: { classes: ['btn-primary', 'size-btn-lg'] },
  //       containerStyle: {},
  //     });
  //   });
  // });
});
