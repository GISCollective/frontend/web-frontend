import { expect } from 'chai';
import { setupTest, describe, it } from 'embedded/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Unit | adapters | application', function (hooks) {
  setupTest(hooks);
  let adapter;
  let embeddedClient;
  let server;
  let baseMap;

  hooks.beforeEach(function () {
    embeddedClient = this.owner.lookup('service:embedded-client');
    adapter = this.owner.lookup('adapter:application');
    server = new TestServer();
  });

  hooks.afterEach(function () {
    server.shutdown();
    window.hasEmbeddedStore = undefined;
  });

  it('exists', function (assert) {
    assert.ok(adapter);
  });

  describe('when window.hasEmbeddedStore = true', function (hooks) {
    let name;
    let args;
    let mockedResponse;

    hooks.beforeEach(function () {
      window.hasEmbeddedStore = true;

      name = null;
      args = null;
      embeddedClient.sendMessage = (a, b) => {
        name = a;
        args = b;

        return mockedResponse;
      };
    });

    it('uses a serialized object instead of urls', function (a) {
      const result = adapter.buildURL('base-map', 'some id', {}, 'query');

      expect(result).to.equal(
        `{"modelName":"BaseMap","id":"some id","requestType":"query"}`,
      );
    });

    it('passes a message to the host app inside the ajax method', async function (a) {
      mockedResponse = 'hello';
      const result = await adapter.ajax(
        `{"modelName":"BaseMap","id":"some id","requestType":"query"}`,
        'type',
        'options',
      );

      expect(name).to.equal(`storeFetch`);
      a.deepEqual(args, {
        info: {
          id: 'some id',
          modelName: 'BaseMap',
          requestType: 'query',
        },
        options: 'options',
        type: 'type',
      });
      expect(result).to.deep.equal(`hello`);
    });
  });

  describe('when window.hasEmbeddedStore = false', function (hooks) {
    let name;
    let args;

    hooks.beforeEach(function () {
      window.hasEmbeddedStore = false;

      name = null;
      args = null;
      embeddedClient.sendMessage = (a, b) => {
        name = a;
        args = b;
      };

      baseMap = server.testData.storage.addDefaultBaseMap();
    });

    it('uses urls pointing to the configured host', function (a) {
      const result = adapter.buildURL('base-map', 'some id', {}, 'query');

      expect(result).to.equal(`/mock-server/basemaps`);
    });

    it('passes a message to the host app inside the ajax method', async function (a) {
      console.log('adapter', adapter.hosts);
      const result = await adapter.ajax(`/mock-server/basemaps`, 'GET', {});

      expect(name).to.equal(null);
      a.deepEqual(result, { baseMaps: [baseMap] });
    });
  });
});
