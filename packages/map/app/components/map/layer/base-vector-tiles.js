/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { Map } from 'maplibre-gl';
import { toLonLat } from 'ol/proj';

export default class MapLayerBaseVectorTilesComponent extends Component {
  @tracked mapLibreInstance;

  change() {
    if (!this.mapLibreInstance) {
      return;
    }

    const view = this._map.getView();
    const center = view.getCenter();
    const zoom = view.getZoom();
    const rotation = view.getRotation();

    this.mapLibreInstance.jumpTo({
      center: toLonLat(center),
      zoom: zoom - 1,
      bearing: (-rotation * 180) / Math.PI,
      animate: false,
    });
  }

  setupMapLibre(container) {
    this.mapLibreInstance = new Map({
      container,
      style: this.args.style,
      attributionControl: false,
      boxZoom: false,
      doubleClickZoom: false,
      dragPan: false,
      dragRotate: false,
      interactive: false,
      keyboard: false,
      pitchWithRotate: false,
      scrollZoom: false,
      touchZoomRotate: false,
    });

    const view = this.args.map.getView();
    this._change = () => this.change(...arguments);

    view.on('change', this._change);
    this.args.map.on('precompose', this._change);
    this._change();

    this.resizeObserver = new ResizeObserver(() => {
      window?.requestAnimationFrame(() => {
        this.mapBoxInstance.resize();
      });
    });

    this.resizeObserver.observe(container);
  }

  @action
  update() {
    if (this.args.style === this._style && this._map === this.args.map) {
      return;
    }

    if (!this.args.style || !this.args.map) {
      return;
    }

    this.destroyMap();

    this._style = this.args.style;
    this._map = this.args.map;
    this.mapBack = this.args.map
      .getTargetElement()
      .parentElement.querySelector('.map-back');
    this.setupMapLibre(this.mapBack);
  }

  destroyMap() {
    this.mapLibreInstance?.remove?.();

    if (!this._map) {
      return;
    }

    const view = this._map.getView();
    view.on('change', this._change);
    this._map.on('precompose', this._change);

    this.resizeObserver?.disconnect?.();
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.destroyMap();
  }
}
