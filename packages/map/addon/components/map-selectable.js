import Component from '@glimmer/component';
import { service } from '@ember/service';
import { transformExtent } from 'ol/proj';
import { GeoJson } from 'core/lib/geoJson';
import { action } from '@ember/object';
import { FeatureSelection } from '../lib/map/feature-selection';

export default class MapSelectable extends Component {
  @service tiles;
  @service mapStyles;
  @service position;

  selection = new FeatureSelection();

  get tilesUrl() {
    return `${this.tiles.url}/{z}/{x}/{y}?format=vt&map=${this.args.id}`;
  }

  get extent() {
    return new GeoJson(this.args.extent);
  }

  get userInExtent() {
    if(!this.position.hasPosition) {
      return false;
    }

    const extent = this.extent.toOlFeature().getGeometry();
    const point = new GeoJson(this.position.center)
      .toOlFeature()
      .getGeometry()
      .getCoordinates();

    return extent.intersectsCoordinate(point);
  }

  @action
  setupExtent(map) {
    if(this.userInExtent) {
      return;
    }

    const extentM = this.extent.toOlFeature().getGeometry().getExtent();
    const extent = transformExtent(extentM, 'EPSG:4326', 'EPSG:3857');

    map.getView().fit(extent);
  }

  @action
  handleSelection() {
    this.selection.update(...arguments);

    if(this.selection.group?.getProperties) {
      let ids = this.selection.group.getProperties()["idList"];

      if(typeof ids != "string") {
        ids = "";
      }

      return this.args.onSelect(ids.split(","));
    }

    return this.args.onSelect([this.selection.featureId]);
  }
}
