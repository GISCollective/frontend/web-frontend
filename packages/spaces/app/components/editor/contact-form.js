/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class ManageEditorsContactFormComponent extends Component {
  formConfig = [
    {
      name: "sendTo",
      type: "email",
    },
    {
      name: "floatingLabels",
      type: "bool",
    },
    {
      name: "emailLabel",
      type: "text",
    },
    {
      name: "nameLabel",
      type: "text",
    },
    {
      name: "messageLabel",
      type: "text",
    },
    {
      name: "subjectLabel",
      type: "text",
    },
    {
      name: "labelSuccess",
      type: "textarea",
    },
    {
      name: "submitButtonLabel",
      type: "text",
    },
    {
      name: "subject",
      type: "text",
    },
  ];
}
