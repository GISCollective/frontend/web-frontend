/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageCampaignStatusComponent extends Component {
  @tracked profile;
  @tracked feature;

  get info() {
    return this.args.value?.info ?? {};
  }

  get message() {
    if (this.args.value?.status == 2 || this.args.value?.status == 3) {
      return 'survey-contribute-map-published-message';
    }

    if (this.args.value?.status == 4) {
      return 'survey-contribute-map-rejected-message';
    }

    return 'survey-contribute-map-pending-message';
  }

  @action
  async setup() {
    await this.info?.fetch?.();

    this.profile = this.info.originalAuthorProfile;

    this.feature = await this.args.value?.featureId;
  }
}
