import { module, test } from 'qunit';
import { setupTest, it, describe } from 'dummy/tests/helpers';

module('Unit | Transform | default models', function (hooks) {
  setupTest(hooks);
  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:default-models');
  });

  // Replace this with your real tests.
  test('it exists', function (assert) {
    assert.ok(transform);
  });

  it('transforms an undefined', function (a) {
    let deserialized = transform.deserialize(undefined);

    a.deepEqual(deserialized.toJSON(), {
      map: '',
      campaign: '',
      calendar: '',
      newsletter: '',
    });
  });

  it('deserializes an object with ids', function (a) {
    let deserialized = transform.deserialize({
      map: '1',
      campaign: '2',
      calendar: '3',
      newsletter: '4',
    });

    a.deepEqual(deserialized.toJSON(), {
      map: '1',
      campaign: '2',
      calendar: '3',
      newsletter: '4',
    });
  });

  it('serializes an empty object', function (a) {
    let deserialized = transform.deserialize({});

    a.deepEqual(deserialized.toJSON(), {
      map: '',
      campaign: '',
      calendar: '',
      newsletter: '',
    });
  });

  it('serializes an object with values', function (a) {
    let deserialized = transform.deserialize({
      map: '1',
      campaign: '2',
      calendar: '3',
      newsletter: '4',
    });

    a.deepEqual(transform.serialize(), {
      map: '',
      campaign: '',
      calendar: '',
      newsletter: '',
    });
  });
});
