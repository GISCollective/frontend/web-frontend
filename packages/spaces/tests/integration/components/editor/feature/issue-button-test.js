/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/feature/issue-button", function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultFeature("1");
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the selected values", async function () {
    this.set("value", {
      label: "some label",
      issueType: "photo",
    });

    await render(hbs`<Editor::Feature::IssueButton @value={{this.value}} />`);

    expect(this.element.querySelector("select").value).to.equal("photo");
  });

  it("can change the the issue type", async function () {
    this.set("value", {});

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Feature::IssueButton @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector("select").value = "photo";
    await triggerEvent("select", "change");

    expect(value).to.deep.equal({
      issueType: "photo",
    });
  });
});
