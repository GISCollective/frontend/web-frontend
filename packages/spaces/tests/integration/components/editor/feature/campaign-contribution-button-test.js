/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent, waitFor } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/feature/campaign-contribution-button", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let spaceData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultCampaign("1");
    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.testData.create.pagesMap = {
      campaigns: "1",
      "campaigns--:campaign_id": "2",
      "surveys--:campaign_id": "3",
      "other--:campaign_id": "4",
      "help--terms": "5",
    };

    this.set("space", await this.store.findRecord("space", spaceData._id));
  });

  it("renders the selected value", async function () {
    this.set("value", {
      data: {
        source: { model: "campaign", id: "1" },
        button: {
          label: "some label",
          destination: "/campaigns/:campaign_id",
        },
        style: {
          classes: ["btn-primary", "size-btn-lg"],
        },
      },
    });

    await render(hbs`<Editor::Feature::CampaignContributionButton @space={{this.space}} @value={{this.value}}/>`);
    expect(this.element.querySelector(".use-current-model")).not.to.exist;

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("Campaign 1");
    expect(this.element.querySelector(".form-control.label").value).to.equal("some label");
    expect(this.element.querySelector(".form-select.destination").value).to.equal("/campaigns/:campaign_id");
    expect(this.element.querySelector(".input-sm-btn").value).to.equal("primary");

    expect(this.element.querySelector(".input-sm-size-btn").value).to.equal("lg");
  });

  it("can change the style options", async function () {
    this.set("value", {
      data: {
        style: {
          classes: ["btn-primary", "size-btn-lg"],
        },
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Feature::CampaignContributionButton @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.element.querySelector(".input-sm-btn").value = "link";
    await triggerEvent(".input-sm-btn", "change");

    this.element.querySelector(".input-sm-size-btn").value = "sm";
    await triggerEvent(".input-sm-size-btn", "change");

    expect(value.style).to.deep.equal({
      classes: ["btn-link", "size-btn-sm"],
    });
  });

  it("can change the label", async function () {
    this.set("value", {
      data: {},
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Feature::CampaignContributionButton @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    await fillIn(".form-control.label", "some label");

    expect(value.button).to.deep.equal({
      label: "some label",
    });
  });

  it("can change the destination", async function (a) {
    this.set("value", {
      data: {},
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Feature::CampaignContributionButton @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    await waitFor(".form-select.destination");

    this.element.querySelector(".form-select.destination").value = "/surveys/:campaign_id";

    await triggerEvent(".form-select.destination", "change");

    expect(value.button).to.deep.equal({
      destination: "/surveys/:campaign_id",
    });
  });
});
