import { module, test } from 'qunit';
import {
  visit,
  currentURL,
  settled,
  waitUntil,
  waitFor,
} from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { setupApplicationTest } from 'embedded/tests/helpers';
import { expect } from 'chai';

module('Acceptance | application', function (hooks) {
  setupApplicationTest(hooks);
  let service;
  let lastMessage;

  hooks.beforeEach(function () {
    lastMessage = null;

    window.ReactNativeWebView = {
      postMessage: (message) => (lastMessage = message),
    };

    service = this.owner.lookup('service:embedded-client');
  });

  hooks.afterEach(function () {
    service.setConfig('apiUrl', '/mock-server');
  });

  test('visiting / when the route is "map-view"', async function (assert) {
    service.setConfig('route', 'map-view');
    service.setConfig('apiUrl', 'https://app.giscollective.com/api-v1');
    await visit('/');

    assert.strictEqual(currentURL(), '/map-view');
  });

  test('visiting / when the route is "user-location"', async function (assert) {
    service.setConfig('route', 'user-location');
    service.setConfig('apiUrl', 'https://app.giscollective.com/api-v1');
    await visit('/');

    assert.strictEqual(currentURL(), '/user-location');
  });

  test('visiting / when the route is "map-selectable"', async function (assert) {
    service.setConfig('route', 'map-selectable');
    service.setConfig('apiUrl', 'https://app.giscollective.com/api-v1');
    await visit('/');

    assert.strictEqual(currentURL(), '/map-selectable');
  });

  test('setting the route after the setup shourd do the transition', async function (assert) {
    service.setConfig('route', '/');
    service.setConfig('apiUrl', 'https://app.giscollective.com/api-v1');
    await visit('/');

    service.setConfig('route', 'user-location');
    await settled();

    assert.strictEqual(currentURL(), '/user-location');
  });

  module('map-selectable', function (hooks) {
    let server;
    let tileHeaders;
    let map;
    let feature;

    hooks.beforeEach(function () {
      server = new TestServer();
      tileHeaders = null;
      server.get('/mock-server/tiles/:z/:x/:y', (request) => {
        tileHeaders = request.requestHeaders;
        return [200, {}, ''];
      });

      server.testData.storage.addDefaultBaseMap();
      map = server.testData.storage.addDefaultMap();
      feature = server.testData.storage.addDefaultFeature();
    });

    hooks.afterEach(function () {
      server.shutdown();
    });

    test('forwards the map id and bearer', async function (assert) {
      service.setConfig('route', 'map-selectable');
      service.setConfig('apiUrl', '/mock-server');
      service.setConfig('bearer', 'some-token');
      service.setValue(`{ "mapId": "${map._id}" }`);

      await visit('/map-selectable');
      await waitUntil(() => tileHeaders);

      expect(
        this.element.querySelector('.d-none.vector-tiles'),
      ).to.have.textContent(
        '/mock-server/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c',
      );
      expect(tileHeaders).to.deep.equal({ Authorization: 'Bearer some-token' });
    });

    test('sends a message when a feature is selected', async function (a) {
      service.setConfig('route', 'map-selectable');
      service.setConfig('apiUrl', '/mock-server');
      service.setConfig('bearer', 'some-token');
      service.setValue(`{ "mapId": "${map._id}" }`);

      await visit('/map-selectable');

      await waitFor('.d-none.vector-tiles');
      await waitUntil(
        () => this.element.querySelector('.d-none.vector-tiles').selectFeature,
      );

      const layer = this.element.querySelector('.d-none.vector-tiles');
      layer.selectFeature({
        getProperties: () => ({
          _id: feature._id,
        }),
      });

      a.equal(
        lastMessage,
        JSON.stringify({
          name: 'selection',
          args: [[feature._id]],
          messageIndex: 0,
        }),
      );
    });
  });
});
