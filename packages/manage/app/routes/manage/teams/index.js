/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { service } from '@ember/service';
import { hash } from 'rsvp';
import LazyList from 'models/lib/lazy-list';

export default class ManageTeams extends AuthenticatedRoute {
  @service user;
  @service session;

  queryParams = {
    search: {
      refreshModel: true,
    },
  };

  beforeModel() {
    if (!this.session.isAuthenticated) {
      return this.router.transitionTo('login.index');
    }

    if (!this.user.isAdmin) {
      this.router.replaceWith('index');
    }

    return super.beforeModel();
  }

  async model(params) {
    const teamsParams = {
      edit: true,
      all: true,
      term: params['search'],
    };

    const teams = new LazyList('team', teamsParams, this.store);
    await teams.loadNext();

    return hash({
      teams,
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);
    controller.setupFilters();
  }
}
