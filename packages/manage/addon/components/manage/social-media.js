import Component from '@glimmer/component';
import { SocialMediaLinks, SocialMediaList } from 'models/transforms/social-media-links';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageSocialMedia extends Component {
  @tracked _value;

  get hasPairs() {
    return this.pairs?.filter(a => a.value.trim()).length > 0;
  }

  get pairs() {
    let result = [];

    for(const field of SocialMediaList) {
      result.push({
        key: field,
        value: this.args.value?.[field]?.trim?.() ?? ""
      });
    }

    return result;
  }

  @action
  change(key, value) {
    value = value.trim();

    if(value.indexOf("http") != "0" && value != "") {
      value = `https://${value}`;
    }

    if(!this._value) {
      this._value = new SocialMediaLinks(this.args.value);
    }

    this._value[key] = encodeURI(value);
  }

  @action
  save() {
    if(!this._value) {
      return;
    }

    return this.args.onSave?.(this._value);
  }
}
