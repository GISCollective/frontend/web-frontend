/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/title-with-buttons', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let pictureData;
  let picture;

  hooks.beforeEach(async function () {
    server = new TestServer();
    pictureData = server.testData.storage.addDefaultPicture();
    picture = await this.store.findRecord('picture', pictureData._id);
  });

  it('renders a level 2 title with a primary large button', async function () {
    this.set('value', {
      data: {
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        buttons: [
          {
            name: 'first button',
            classes: ['btn-primary', 'size-btn-lg'],
            link: { url: 'https://giscollective.com/page1' },
            type: 'default',
          },
        ],
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);

    expect(this.element.querySelector('h2').textContent.trim()).to.equal(
      'some title',
    );

    expect(this.element.querySelector('h2')).to.have.class('display-3');
    expect(this.element.querySelector('h2')).to.have.class('fw-bold');
    expect(this.element.querySelector('h2')).to.have.class('text-green');

    expect(this.element.querySelectorAll('a')).to.have.length(1);
    expect(this.element.querySelector('a')).to.have.class('btn');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'https://giscollective.com/page1',
    );
    expect(this.element.querySelector('a')).to.have.class('btn-primary');
    expect(this.element.querySelector('a')).to.have.class('size-btn-lg');
  });

  it('renders a level 1 title', async function () {
    this.set('value', {
      data: {
        title: {
          text: 'some title',
          heading: 1,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);
    expect(this.element.querySelector('h2')).not.to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'some title',
    );

    expect(this.element.querySelector('h1')).to.have.class('display-3');
    expect(this.element.querySelector('h1')).to.have.class('fw-bold');
    expect(this.element.querySelector('h1')).to.have.class('text-green');

    expect(this.element.querySelectorAll('a')).to.have.length(0);
    expect(this.element.querySelectorAll('p')).to.have.length(0);
  });

  it('renders a picture', async function (a) {
    this.set('value', {
      data: {
        source: {
          model: 'picture',
          id: pictureData._id,
        },
        picture: {
          size: {
            sizing: 'fixed height',
            proportion: '4:3',
            customStyle: {
              sm: {
                height: '100px',
                width: 'auto',
              },
              md: {
                height: '200px',
                width: 'auto',
              },
              lg: {
                height: '300px',
                width: 'auto',
              },
            },
          },
        },
      },
      modelKey: 'picture',
    });

    this.set('model', {
      picture,
    });

    await render(
      hbs`<View::TitleWithButtons @model={{this.model}} @value={{this.value}} />`,
    );

    await waitFor('.title-with-buttons img');

    const id = this.element
      .querySelector('.picture-with-options')
      .attributes.getNamedItem('id').textContent;

    expect(
      this.element
        .querySelector(`style[data-selector*="#${id} img"]`)
        .textContent.trim(),
    ).to.contain(`{height: 100px;width: auto}`);
  });

  it('renders two buttons', async function () {
    this.set('value', {
      data: {
        buttons: [
          {
            name: 'first button',
            classes: ['btn-primary', 'size-btn-lg'],
            link: 'https://giscollective.com/page1',
            type: 'default',
          },
          {
            name: 'second button',
            classes: ['btn-secondary', 'size-btn-sm'],
            link: 'https://giscollective.com/page2',
            type: 'default',
          },
        ],
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;

    const buttons = this.element.querySelectorAll('a');
    expect(buttons).to.have.length(2);
    expect(buttons[0]).not.to.have.class('btn-app-store');
    expect(buttons[1]).not.to.have.class('btn-app-store');
    expect(buttons[1]).not.to.have.attribute('target');
  });

  it('renders a button with a _blank target', async function () {
    this.set('value', {
      data: {
        buttons: [
          {
            name: 'first button',
            classes: ['btn-primary', 'size-btn-lg'],
            link: 'https://giscollective.com/page1',
            newTab: true,
            type: 'default',
          },
        ],
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;

    const buttons = this.element.querySelectorAll('a');
    expect(buttons[0]).to.have.attribute('target', '_blank');
  });

  it('renders two app store buttons', async function () {
    this.set('value', {
      data: {
        buttons: [
          {
            name: 'first button',
            classes: ['btn-primary', 'size-btn-lg'],
            link: { url: 'https://giscollective.com/page1' },
            type: 'app-store',
            storeType: 'android',
          },
          {
            name: 'second button',
            classes: ['btn-secondary', 'size-btn-sm'],
            link: { url: 'https://giscollective.com/page2' },
            type: 'app-store',
            storeType: 'apple',
          },
        ],
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;

    const buttons = this.element.querySelectorAll('a');
    expect(buttons).to.have.length(2);
    expect(buttons[0]).to.have.class('btn-app-store');
    expect(buttons[1]).to.have.class('btn-app-store');
  });

  it('renders buttons on center when the paragraph is centered', async function () {
    this.set('value', {
      data: {
        paragraph: {
          classes: ['text-center'],
        },
        buttons: [
          {
            name: 'button',
            classes: ['btn-primary', 'size-btn-lg'],
            link: { url: 'https://giscollective.com/page1' },
            type: 'default',
          },
        ],
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);

    expect(this.element.querySelector('.button-list')).to.have.class(
      'text-center',
    );
  });

  it('renders a paragraph when set', async function () {
    this.set('value', {
      data: {
        paragraph: {
          text: 'this is a paragraph',
          classes: ['text-center', 'fw-bold'],
          color: 'red',
        },
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;
    expect(this.element.querySelectorAll('a')).to.have.length(0);
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'this is a paragraph',
    );
    expect(this.element.querySelector('p')).to.have.class('text-center');
    expect(this.element.querySelector('p')).to.have.class('fw-bold');
    expect(this.element.querySelector('p')).to.have.class('text-red');
  });

  it('renders the container options', async function () {
    this.set('value', {
      data: {
        container: {
          color: 'pink-300',
          verticalAlign: 'center',
          verticalSize: 'fill',
          classes: ['pe-3'],
        },
      },
    });

    await render(hbs`<View::TitleWithButtons @value={{this.value}} />`);

    expect(this.element.querySelector('.title-with-buttons')).to.have.class(
      'bg-pink-300',
    );
    expect(this.element.querySelector('.title-with-buttons')).to.have.class(
      'pe-3',
    );
    expect(this.element.querySelector('.title-with-buttons')).to.have.class(
      'container-align-center',
    );
  });
});
