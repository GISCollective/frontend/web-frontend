/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class BrowsePlacesPreviewComponent extends Component {
  @action
  onClick() {
    return this.args.onClick?.(this.args.place);
  }
}
