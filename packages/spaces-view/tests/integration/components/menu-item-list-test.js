/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render, triggerEvent, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | menu-item-list', function (hooks) {
  setupRenderingTest(hooks);

  describe('a menu with two items and a dropdown', function (hooks) {
    hooks.beforeEach(async function () {
      this.set('value', [
        {
          name: 'name1',
          dropDown: [
            {
              name: 'other name',
              link: { url: 'https://giscollective.com' },
            },
          ],
        },
        {
          name: 'name2',
          link: { url: 'https://giscollective.com' },
        },
      ]);

      await render(hbs`<MenuItemList @value={{this.value}} />`);
    });

    it('renders the menu item as button', async function () {
      expect(
        this.element.querySelector('button.nav-link').textContent.trim(),
      ).to.equal('name1');
    });

    it('renders the sub menu item as link', async function () {
      expect(
        this.element.querySelector('a.nav-link').textContent.trim(),
      ).to.equal('other name');
      expect(this.element.querySelector('a.nav-link')).to.have.attribute(
        'href',
        'https://giscollective.com',
      );
    });

    it('shows the dropdown on mouseenter', async function () {
      await triggerEvent('.nav-item-level-1', 'mouseenter');

      expect(
        this.element
          .querySelector('.nav-item-level-1.show-dropdown button')
          .textContent.trim(),
      ).to.equal('name1');
    });

    it('hides the dropdown on mouseleave from navbar-nav', async function () {
      await triggerEvent('.nav-item-level-1', 'mouseenter');
      await triggerEvent('.navbar-nav', 'mouseleave');

      expect(
        this.element.querySelector('.nav-item-level-1.show-dropdown button'),
      ).not.to.exist;
    });

    it('hides the dropdown on mouseleave from dropdown-menu-list', async function () {
      await triggerEvent('.nav-item-level-1', 'mouseenter');
      await triggerEvent('.dropdown-menu-list', 'mouseleave');

      expect(
        this.element.querySelector('.nav-item-level-1.show-dropdown button'),
      ).not.to.exist;
    });
  });

  describe('when ignoreHover is true', function (hooks) {
    hooks.beforeEach(async function () {
      this.set('value', [
        {
          name: 'name1',
          link: { url: 'https://giscollective.com' },
          dropDown: [
            {
              name: 'other name',
              link: { url: 'https://giscollective.com' },
            },
          ],
        },
        {
          name: 'name2',
          link: { url: 'https://giscollective.com' },
          dropDown: [
            {
              name: 'other name',
              link: { url: 'https://giscollective.com' },
            },
          ],
        },
      ]);

      await render(
        hbs`<MenuItemList @ignoreHover={{true}} @value={{this.value}} />`,
      );
    });

    it('does not show the dropdown on mouseenter', async function () {
      await triggerEvent('.nav-item-level-1', 'mouseenter');

      expect(
        this.element.querySelector('.nav-item-level-1.show-dropdown button'),
      ).not.to.exist;
    });

    it('shows the dropdown on click', async function () {
      const buttons = this.element.querySelectorAll('.nav-item-level-1 button');
      await click(buttons[0]);

      expect(
        this.element
          .querySelector('.nav-item-level-1.show-dropdown button')
          .textContent.trim(),
      ).to.equal('name1');

      await click(buttons[1]);

      expect(
        this.element
          .querySelector('.nav-item-level-1.show-dropdown button')
          .textContent.trim(),
      ).to.equal('name2');
    });
  });
});
