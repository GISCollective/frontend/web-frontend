/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | view/profile-list',  function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let profile1;
  let profile2;

  hooks.before(function () {
    server = new TestServer();
    profile1 = server.testData.storage.addDefaultProfile('1');
    profile2 = server.testData.storage.addDefaultProfile('2');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::ProfileList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the profile name and a link when the profile exists', async function () {
    this.set('value', [profile1._id, profile2._id]);

    await render(hbs`<View::ProfileList @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.contain('mr Bogdan Szabo');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/browse/profiles/1'
    );
  });
});
