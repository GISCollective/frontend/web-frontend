/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';
import LineStyleProperties from './line-style-properties';

export default class PolygonStyleProperties extends LineStyleProperties {
  @tracked showAsLineAfterZoom = 99;

  constructor(value) {
    super(value);

    if (!value || typeof value != 'object') {
      value = {};
    }

    if (!isNaN(value.showAsLineAfterZoom)) {
      this.showAsLineAfterZoom = value.showAsLineAfterZoom;
    }
  }

  clone() {
    return new PolygonStyleProperties(this);
  }

  toJSON() {
    const result = super.toJSON();
    result.showAsLineAfterZoom = this.showAsLineAfterZoom;

    return result;
  }
}
