/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../../base/app-route';
import { hash } from 'rsvp';

export default class BrowseProfilesProfileRoute extends Route {
  async model(params) {
    const profile = await this.store.findRecord('user-profile', params.id);
    const teams = this.store.query('team', { user: params.id, all: true });

    return hash({
      profile,
      teams,
      contributions: profile.contributions,
    });
  }
}
