/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageIconsAddSetController extends Controller {
  @service notifications;
  @service intl;
  @service router;

  @tracked _description = null;
  @tracked team;
  @tracked _name = '';

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
    this.model.iconSet.name = value;
  }

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-icon-set'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  get isInvalid() {
    return this.name.trim() == '' || !this.team;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.iconSet.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new icon set'),
      capitalize: true,
    });

    return pieces;
  }

  @action
  changeTeam(team) {
    this.model.iconSet.visibility.team = team;
    this.team = team;
  }

  @action
  changeDescriptionValue(title, description) {
    this.description = description;
  }

  @action
  async save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.iconSet.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];
    this.model.iconSet.description = {
      blocks,
    };

    try {
      const result = await this.model.iconSet.save();
      this.router.transitionTo('manage.icons.editset', result.id);
    } catch (err) {
      this.notifications.handleError(err);
    }
  }
}
