/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupTest, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { Question } from 'models/transforms/question-list';
import { A } from 'models/lib/array';

module('Unit | Transform | question list', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:question-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty array', function () {
    let deserialized = transform.deserialize([]);

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function (a) {
    let deserialized = transform.deserialize([
      {
        question: 'custom question',
        help: 'custom help',
        type: 'email',
        name: 'email',
        isVisible: false,
        isRequired: true,
        options: { a: 'b' },
      },
    ]);

    a.deepEqual(deserialized[0].toJSON(), {
      question: 'custom question',
      help: 'custom help',
      type: 'email',
      name: 'email',
      isRequired: true,
      isVisible: false,
      options: { a: 'b' },
    });
  });

  it('can serialize an ember array with a Question with all fields', function (a) {
    let serialized = transform.serialize(
      A([
        new Question({
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'email',
          isRequired: true,
          isVisible: true,
          options: '',
        }),
      ]),
    );

    a.deepEqual(serialized, [
      {
        question: 'custom question',
        help: 'custom help',
        type: 'email',
        name: 'email',
        isVisible: true,
        isRequired: true,
        options: '',
      },
    ]);
  });

  it('can serialize an array with an object with missing fields', function (a) {
    let serialized = transform.serialize([
      {
        question: 'custom question',
        help: 'custom help',
        type: 'email',
        name: 'email',
        options: {},
      },
    ]);

    a.deepEqual(serialized, [
      {
        question: 'custom question',
        help: 'custom help',
        type: 'email',
        name: 'email',
        isRequired: false,
        isVisible: true,
        options: {},
      },
    ]);
  });
});
