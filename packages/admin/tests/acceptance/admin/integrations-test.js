/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, fillIn, click } from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';

describe ('Acceptance | admin/integrations', function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultSpace();
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to index if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit('/admin/integrations');
    expect(currentURL()).to.equal('/');
  });

  it('should redirect to index if the user is not authenticated', async function () {
    await visit('/admin/integrations');
    expect(currentURL()).to.equal('/login');
  });

  it('should show a disabled submit button', async function () {
    authenticateSession();

    server.testData.storage.addPreference('integrations.slack.webhook', '');
    server.testData.storage.addPreference(
      'integrations.google.client_id',
      'google-client-secret'
    );
    server.testData.storage.addPreference(
      'integrations.google.client_secret',
      'google-client-secret'
    );

    await visit('/admin/integrations');

    expect(currentURL()).to.equal('/admin/integrations');

    expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
      'disabled',
      ''
    );
  });

  it('should have the server values', async function () {
    authenticateSession();

    server.testData.storage.addPreference('integrations.slack.webhook', 'some-key');
    server.testData.storage.addPreference(
      'integrations.google.client_id',
      'google-client-id'
    );
    server.testData.storage.addPreference(
      'integrations.google.client_secret',
      'google-client-secret'
    );

    await visit('/admin/integrations');
    expect(currentURL()).to.equal('/admin/integrations');

    expect(this.element.querySelector('.input-slack-webhook').value).to.equal(
      'some-key'
    );
    expect(
      this.element.querySelector('.input-google-client-id').value
    ).to.equal('google-client-id');
    expect(
      this.element.querySelector('.input-google-client-secret').value
    ).to.equal('google-client-secret');
  });

  describe('successful put request', function (hooks) {
    let receivedPreferences = [];

    hooks.beforeEach(function () {
      receivedPreferences = [];

      server.put('/mock-server/preferences/:id', (request) => {
        let receivedPreference = JSON.parse(request.requestBody);
        receivedPreference.preference._id = receivedPreference.preference.name;
        receivedPreferences.push(receivedPreference);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedPreference),
        ];
      });
      server.testData.storage.addPreference(
        'integrations.google.client_id',
        'google-client-id'
      );
      server.testData.storage.addPreference(
        'integrations.google.client_secret',
        'google-client-secret'
      );
      server.testData.storage.addPreference(
        'integrations.slack.webhook',
        ''
      );
    });

    it('updates the values on submit', async function () {
      authenticateSession();

      await visit('/admin/integrations');

      await fillIn('.input-google-client-id', 'new value 4');
      await fillIn('.input-google-client-secret', 'new value 5');
      await fillIn('.input-slack-webhook', 'new value 6');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreferences).to.deep.contain({
        preference: {
          _id: 'integrations.google.client_id',
          name: 'integrations.google.client_id',
          value: 'new value 4',
        },
      });

      expect(receivedPreferences).to.deep.contain({
        preference: {
          _id: 'integrations.google.client_secret',
          name: 'integrations.google.client_secret',
          value: 'new value 5',
        },
      });

      expect(receivedPreferences).to.deep.contain({
        preference: {
          _id: 'integrations.slack.webhook',
          name: 'integrations.slack.webhook',
          value: 'new value 6',
        },
      });
    });
  });
});
