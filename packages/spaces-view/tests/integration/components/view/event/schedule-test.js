/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';
import { CalendarEntry } from 'models/transforms/calendar-entry-list';

module('Integration | Component | view/event/schedule', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let eventData;

  hooks.before(function () {
    server = new TestServer();
    eventData = server.testData.storage.addDefaultEvent('1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::Event::Schedule />`);

    assert.dom(this.element).hasText('');
  });

  it('renders the schedule when the event has one', async function (assert) {
    this.set('value', {
      data: {
        source: { model: 'event', id: '1' },
        style: {
          color: 'green',
          classes: ['fw-bold'],
        },
      },
      modelKey: 'event',
    });

    eventData.entries = [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'CET',
      }),
      new CalendarEntry({
        begin: '2023-01-03T13:00:00Z',
        end: '2023-01-03T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'CET',
      }),
    ].map((a) => a.toJSON());

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Schedule @model={{this.model}} @value={{this.value}} />`,
    );

    const items = this.element.querySelectorAll('.time-table-day');
    expect(
      items[0].textContent
        .split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('Mon: 11:00 - 13:00');
    expect(
      items[1].textContent
        .split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('Tue: 14:00 - 19:00');
  });
});
