/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class PageController extends Controller {
  preserveScrollPosition = false;

  @service space;
  @service headData;
  @service user;

  @tracked s = '';

  queryParams = ['s'];

  get conditions() {
    const result = [];

    if (this.user.id) {
      result.push("user:authenticated");
    }

    if (!this.user.id) {
      result.push("user:unauthenticated");
    }

    return result;
  }

  @action
  changeState(s) {
    this.s = s;
  }
}
