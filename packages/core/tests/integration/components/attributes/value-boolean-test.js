/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/value-boolean', function (hooks) {
  setupRenderingTest(hooks);

  it('renders unknown by default', async function () {
    await render(hbs`<Attributes::ValueBoolean />`);

    expect(this.element.querySelector('.btn-unknown')).to.have.class('btn-success');
    expect(this.element.querySelector('.btn-true')).not.to.have.class('btn-success');
    expect(this.element.querySelector('.btn-false')).not.to.have.class('btn-success');
  });

  it('renders true value when the value is true', async function () {
    await render(hbs`<Attributes::ValueBoolean @value={{true}} />`);

    expect(this.element.querySelector('.btn-unknown')).not.to.have.class('btn-success');
    expect(this.element.querySelector('.btn-true')).to.have.class('btn-success');
    expect(this.element.querySelector('.btn-false')).not.to.have.class('btn-success');
  });

  it('renders false value when the value is false', async function () {
    await render(hbs`<Attributes::ValueBoolean @value={{false}} />`);

    expect(this.element.querySelector('.btn-unknown')).not.to.have.class('btn-success');
    expect(this.element.querySelector('.btn-true')).not.to.have.class('btn-success');
    expect(this.element.querySelector('.btn-false')).to.have.class('btn-success');
  });

  it('triggers on change action when an option is clicked', async function () {
    let value;
    this.set('changed', (v) => {
      value = v;
    });

    await render(hbs`<Attributes::ValueBoolean @value={{false}} @onChange={{this.changed}}/>`);
    await click('.btn-true');

    expect(value).to.equal(true);
  });
});
