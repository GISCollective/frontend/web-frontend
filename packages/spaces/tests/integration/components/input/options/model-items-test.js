/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { click, fillIn, render, triggerEvent, waitFor } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { selectSearch, selectChoose } from "ember-power-select/test-support";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | input/options/model-items", function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let mapData;
  let iconSetData;
  let teamData;
  let campaignData;
  let icon1;
  let icon2;
  let spaceData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam("1");
    server.testData.storage.addDefaultTeam("2");
    spaceData = server.testData.storage.addDefaultSpace("2");

    campaignData = server.testData.storage.addDefaultCampaign("1");
    server.testData.storage.addDefaultCampaign("2");
    server.testData.storage.addDefaultCampaign("3");

    server.testData.storage.addDefaultFeature("1");

    icon1 = server.testData.storage.addDefaultIcon();

    icon2 = server.testData.storage.addDefaultIcon("1");
    icon2.name = "test icon";
    icon2.localName = "test icon";

    mapData = server.testData.storage.addDefaultMap();
    teamData = server.testData.storage.addDefaultTeam();
    iconSetData = server.testData.storage.addDefaultIconSet();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    store = this.owner.lookup("service:store");
  });

  it("renders the all option when there is no source", async function () {
    this.set("value", {});

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} />`);

    expect(this.element.querySelector(".model-name").value).to.equal("");
  });

  it("does not show the selectedModel option when the model does not have one", async function () {
    this.set("value", { team: "", model: "campaign" });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    const options = [...this.element.querySelectorAll(".model-name option")].map((a) => a.value);

    expect(options).to.deep.equal([
      "article",
      "map",
      "campaign",
      "icon",
      "icon-set",
      "feature",
      "team",
      "event",
      "calendar",
    ]);
  });

  it("shows the selected selected-model when set", async function () {
    const feature = await store.findRecord("feature", "1");

    this.set("value", {
      useSelectedModel: true,
      property: "maps",
    });
    this.set("model", {
      selectedModel: feature,
    });

    await render(
      hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
    );

    expect(this.element.querySelector(".model-name").value).to.equal("selected-model");
    expect(this.element.querySelector(".model-property").value).to.equal("maps");
  });

  it("can select the selected-model option when the model does has one", async function () {
    const store = this.owner.lookup("service:store");
    const feature = await store.findRecord("feature", "1");

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    this.set("value", {});
    this.set("model", {
      selectedModel: feature,
    });

    await render(
      hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
    );

    this.element.querySelector(".model-name").value = "selected-model";
    await triggerEvent(".model-name", "change");

    expect(this.element.querySelector(".btn-group")).not.to.exist;

    this.element.querySelector(".model-property").value = "maps";
    await triggerEvent(".model-property", "change");

    expect(value).to.deep.equal({
      useSelectedModel: true,
      property: "maps",
    });
  });

  it("shows the selected ids", async function () {
    this.set("value", { ids: ["1"], model: "campaign" });

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    const values = [...this.element.querySelectorAll(".model-id .ember-power-select-selected-item")].map((a) =>
      a.textContent.trim(),
    );
    expect(values).to.deep.equal(["Campaign 1"]);
  });

  it("allows selecting a new model", async function () {
    this.set("value", { team: { source: "fixed", value: "1" }, model: "campaign" });

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".model-name").value).to.equal("campaign");

    this.element.querySelector(".model-name").value = "map";
    await triggerEvent(".model-name", "change");

    expect(value).to.deep.equal({ model: "map" });
  });

  it("allows changing to the `all` value", async function () {
    this.set("value", { ids: [], model: "campaign" });

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    await waitFor(".btn-check-ids");

    expect(this.element.querySelector(".btn-check-ids").checked).to.equal(true);

    await click(".btn-check-all");

    expect(this.element.querySelector(".select-source-team")).not.to.exist;

    expect(value).to.deep.equal({ model: "campaign" });
  });

  it("allows changing to the `ids` value", async function () {
    this.set("value", { team: { source: "fixed", value: "1" }, model: "campaign" });

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".btn-check-ids");
    await click(".btn-add-item");

    await selectSearch(".model-id-0", campaignData.name);
    await selectChoose(".model-id-0", campaignData.name);

    expect(value).to.deep.equal({ ids: ["1"], model: "campaign" });
  });

  it("selects only records from the current team when the space is not default", async function () {
    const space = await this.store.findRecord("space", spaceData._id);
    space.visibility.isDefault = false;

    this.set("value", { team: { source: "fixed", value: "1" }, model: "campaign" });
    this.set("space", space);

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(
      hbs`<Input::Options::ModelItems @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    await click(".btn-check-ids");
    await click(".btn-add-item");

    server.history = [];
    await selectSearch(".model-id-0", campaignData.name);
    await selectChoose(".model-id-0", campaignData.name);

    expect(server.history).deep.equal(["GET /mock-server/campaigns?team=61292c4c7bdf9301008fd7b6&term=Campaign%201"]);
  });

  it("selects any records from the any team when the space is default", async function () {
    const space = await this.store.findRecord("space", spaceData._id);
    space.visibility.isDefault = true;

    this.set("value", { team: { source: "fixed", value: "1" }, model: "campaign" });
    this.set("space", space);

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(
      hbs`<Input::Options::ModelItems @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    await click(".btn-check-ids");
    await click(".btn-add-item");

    server.history = [];
    await selectSearch(".model-id-0", campaignData.name);
    await selectChoose(".model-id-0", campaignData.name);

    expect(server.history).deep.equal(["GET /mock-server/campaigns?term=Campaign%201"]);
  });

  it("allows changing to the `groups` value", async function () {
    this.set("value", { team: { source: "fixed", value: "1" }, model: "campaign" });

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".btn-check-groups");
    await click(".btn-groups-add");
    await click(".btn-groups-add");

    expect(this.element.querySelectorAll(".model-group")).to.have.length(2);

    await click(".btn-add-item");

    await selectSearch(".model-id", "test");
    await selectChoose(".model-id", "Campaign 1");

    expect(value).to.deep.equal({
      model: "campaign",
      groups: [
        { name: "new group", ids: ["1"] },
        { name: "new group", ids: [] },
      ],
    });
  });

  it("allows changing to the `groups` names", async function () {
    this.set("value", {
      model: "campaign",
      groups: [
        { name: "new group", ids: ["1"] },
        { name: "new group", ids: ["2"] },
      ],
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    await waitFor(".input-group-name-0");

    await fillIn(".input-group-name-0", "group 1");
    await fillIn(".input-group-name-0", "group 1");
    await fillIn(".input-group-name-1", "group 2");

    expect(value).to.deep.equal({
      model: "campaign",
      groups: [
        { name: "group 1", ids: ["1"] },
        { name: "group 2", ids: ["2"] },
      ],
    });
  });

  it("allows deleting a group", async function () {
    this.set("value", {
      model: "campaign",
      groups: [
        { name: "new group", ids: ["1"] },
        { name: "new group", ids: ["2"] },
      ],
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
      this.set("value", v);
    });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".btn-delete-group-0");

    expect(value).to.deep.equal({
      model: "campaign",
      groups: [{ name: "new group", ids: ["2"] }],
    });
  });

  describe("using the filter option", function () {
    describe("when the list renders features", function (hooks) {
      let value;
      let map;

      hooks.beforeEach(async function () {
        value = null;
        this.set("value", { model: "feature" });
        map = await store.findRecord("map", mapData._id);

        this.set("model", {
          selectedModel: map,
          selectedType: "map",
        });

        this.set("onChange", (v) => {
          value = v;
          this.set("value", v);
        });
      });

      it("allows to filter features by the current map", async function () {
        await render(
          hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
        );

        await waitFor(".model-name");
        await click(".btn-check-all");
        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[0].value = "selected model";
        await triggerEvent(selects[0], "change");

        expect(value).to.deep.equal({
          parameters: { map: { source: "selected-model", value: "" } },
          model: "feature",
        });
      });

      it("allows to filter features by the icon from the current state", async function () {
        await render(
          hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
        );

        await waitFor(".model-name");
        await click(".btn-check-all");
        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[1].value = "state";
        await triggerEvent(selects[1], "change");

        expect(value).to.deep.equal({
          parameters: { icons: { source: "state", value: "", state: "icon" } },
          model: "feature",
        });
      });

      it("allows to filter features by a custom map from any team when the space is default", async function () {
        const space = await this.store.findRecord("space", spaceData._id);
        space.visibility.isDefault = true;

        this.set("space", space);

        await render(
          hbs`<Input::Options::ModelItems @space={{this.space}} @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
        );

        await waitFor(".model-name");
        await click(".btn-check-all");
        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");
        selects[0].value = "fixed";
        await triggerEvent(selects[0], "change");

        server.history = [];
        await selectSearch(".model-parameter-record-id", map.name);
        await selectChoose(".model-parameter-record-id", map.name);

        expect(server.history).to.deep.equal(["GET /mock-server/maps?term=Harta%20Verde%20Bucure%C8%99ti"]);

        expect(value).to.deep.equal({
          parameters: { map: { source: "fixed", value: "5ca89e37ef1f7e010007f54c" } },
          model: "feature",
        });
      });

      it("allows to filter features by a custom map from own team when the space is not default", async function () {
        const space = await this.store.findRecord("space", spaceData._id);
        space.visibility.isDefault = false;

        this.set("space", space);

        await render(
          hbs`<Input::Options::ModelItems @space={{this.space}} @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
        );

        await waitFor(".model-name");
        await click(".btn-check-all");
        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");
        selects[0].value = "fixed";
        await triggerEvent(selects[0], "change");

        server.history = [];
        await selectSearch(".model-parameter-record-id", map.name);
        await selectChoose(".model-parameter-record-id", map.name);

        expect(server.history).to.deep.equal([
          "GET /mock-server/maps?team=61292c4c7bdf9301008fd7b6&term=Harta%20Verde%20Bucure%C8%99ti",
        ]);

        expect(value).to.deep.equal({
          parameters: { map: { source: "fixed", value: "5ca89e37ef1f7e010007f54c" } },
          model: "feature",
        });
      });

      it("allows to filter features by an icon list", async function () {
        await render(
          hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
        );

        await waitFor(".model-name");
        await click(".btn-check-all");
        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");
        selects[1].value = "fixed";
        await triggerEvent(selects[1], "change");

        await click(".model-group-id-list .btn-add-item");

        await selectSearch(".model-id-0", icon1.name);
        await selectChoose(".model-id-0", icon1.name);

        await click(".model-group-id-list .btn-add-item");

        await selectSearch(".model-id-1", icon2.name);
        await selectChoose(".model-id-1", icon2.name);

        expect(value).to.deep.equal({
          parameters: { icons: { source: "fixed", value: "5ca7bfc0ecd8490100cab980,1" } },
          model: "feature",
        });
      });

      it("renders the current value with selected-model", async function () {
        this.set("value", {
          model: "feature",
          parameters: {
            map: "selected-model",
            icons: { source: "fixed", value: "5ca7bfc0ecd8490100cab980,1" },
          },
        });
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} />`);

        const selects = this.element.querySelectorAll(".select-parameter-source");

        expect(selects[0].value).to.equal("selected model");

        expect(this.element.querySelectorAll(".model-id")).to.have.length(2);
      });

      it("renders the current value without selected-model", async function () {
        this.set("value", {
          model: "feature",
          parameters: {
            map: "5ca89e37ef1f7e010007f54c",
            icons: "5ca7bfc0ecd8490100cab980,1",
          },
        });

        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} />`);

        const selects = this.element.querySelectorAll(".select-parameter-source");

        expect(selects[0].value).to.equal("fixed");
        expect(
          this.element.querySelector(".model-parameter-record-id .ember-power-select-selected-item").textContent.trim(),
        ).to.equal("Harta Verde București");
        expect(this.element.querySelectorAll(".model-id")).to.have.length(2);
      });
    });

    describe("when the list renders campaigns", function (hooks) {
      let value;
      let map;
      let team;

      hooks.beforeEach(async function () {
        value = null;
        this.set("value", { model: "campaign" });
        map = await store.findRecord("map", mapData._id);
        team = await store.findRecord("team", teamData._id);

        this.set("model", {
          selectedModel: map,
          selectedType: "map",
        });

        this.set("onChange", (v) => {
          value = v;
          this.set("value", v);
        });
      });

      it("allows selecting to filter by teams", async function () {
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[0].value = "fixed";
        await triggerEvent(selects[0], "change");

        expect(this.element.querySelector(".model-name").value).to.equal("campaign");

        await selectSearch(".model-parameter-record-id", team.name);
        await selectChoose(".model-parameter-record-id", team.name);

        expect(value).to.deep.equal({
          parameters: { team: { source: "fixed", value: "1" } },
          model: "campaign",
        });
      });
    });

    describe("when the list renders maps", function (hooks) {
      let value;
      let map;
      let team;

      hooks.beforeEach(async function () {
        value = null;
        this.set("value", { model: "map" });
        map = await store.findRecord("map", mapData._id);
        team = await store.findRecord("team", teamData._id);

        this.set("model", {
          selectedModel: map,
          selectedType: "map",
        });

        this.set("onChange", (v) => {
          value = v;
          this.set("value", v);
        });
      });

      it("allows selecting to filter by teams", async function () {
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

        await click(".btn-check-parameters");

        expect(this.element.querySelector(".model-name").value).to.equal("map");
        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[0].value = "fixed";
        await triggerEvent(selects[0], "change");

        await selectSearch(".model-parameter-record-id", team.name);
        await selectChoose(".model-parameter-record-id", team.name);

        expect(value).to.deep.equal({
          parameters: { team: { source: "fixed", value: "1" } },
          model: "map",
        });
      });
    });

    describe("when the list renders icon sets", function (hooks) {
      let value;
      let map;
      let team;

      hooks.beforeEach(async function () {
        value = null;
        this.set("value", { model: "icon-set" });
        map = await store.findRecord("map", mapData._id);
        team = await store.findRecord("team", teamData._id);

        this.set("model", {
          selectedModel: map,
          selectedType: "map",
        });

        this.set("onChange", (v) => {
          value = v;
          this.set("value", v);
        });
      });

      it("allows selecting to filter by teams", async function () {
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

        await click(".btn-check-parameters");

        expect(this.element.querySelector(".model-name").value).to.equal("icon-set");
        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[0].value = "fixed";
        await triggerEvent(selects[0], "change");

        await selectSearch(".model-parameter-record-id", team.name);
        await selectChoose(".model-parameter-record-id", team.name);

        expect(value).to.deep.equal({
          parameters: { team: { source: "fixed", value: "1" } },
          model: "icon-set",
        });
      });
    });

    describe("when the list renders icons", function (hooks) {
      let value;
      let map;
      let iconSet;

      hooks.beforeEach(async function () {
        value = null;
        this.set("value", { model: "icon" });
        map = await store.findRecord("map", mapData._id);
        iconSet = await store.findRecord("iconSet", iconSetData._id);

        this.set("model", {
          selectedModel: map,
          selectedType: "map",
        });

        this.set("onChange", (v) => {
          value = v;
          this.set("value", v);
        });
      });

      it("allows to filter by map", async function () {
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

        await click(".btn-check-parameters");

        expect(this.element.querySelector(".model-name").value).to.equal("icon");
        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[0].value = "fixed";
        await triggerEvent(selects[0], "change");

        await selectSearch(".model-parameter-record-id", map.name);
        await selectChoose(".model-parameter-record-id", map.name);

        expect(value).to.deep.equal({
          parameters: {
            map: { source: "fixed", value: "5ca89e37ef1f7e010007f54c" },
          },
          model: "icon",
        });
      });

      it("allows to filter by icon set", async function () {
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

        await click(".btn-check-parameters");

        expect(this.element.querySelector(".model-name").value).to.equal("icon");

        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[1].value = "fixed";
        await triggerEvent(selects[1], "change");

        await selectSearch(".model-parameter-record-id", iconSet.name);
        await selectChoose(".model-parameter-record-id", iconSet.name);

        expect(value).to.deep.equal({
          parameters: {
            iconSet: { source: "fixed", value: "5ca7b702ecd8490100cab96f" },
          },
          model: "icon",
        });
      });

      it("allows to filter by the selected icon-set", async function () {
        this.set("model", {
          selectedModel: iconSet,
          selectedType: "icon-set",
        });

        await render(
          hbs`<Input::Options::ModelItems @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
        );

        await click(".btn-check-parameters");

        expect(this.element.querySelector(".model-name").value).to.equal("icon");

        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[1].value = "selected model";
        await triggerEvent(selects[1], "change");

        expect(value).to.deep.equal({
          parameters: {
            iconSet: { source: "selected-model", value: "" },
          },
          model: "icon",
        });
      });

      it("allows to filter by category", async function () {
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[2].value = "fixed";
        await triggerEvent(selects[2], "change");

        await fillIn(".model-text", "some category");

        expect(value).to.deep.equal({
          parameters: {
            category: { source: "fixed", value: "some category" },
          },
          model: "icon",
        });
      });

      it("allows to filter by subcategory", async function () {
        await render(hbs`<Input::Options::ModelItems @value={{this.value}} @onChange={{this.onChange}} />`);

        await click(".btn-check-parameters");

        const selects = this.element.querySelectorAll(".select-parameter-source");

        selects[3].value = "fixed";
        await triggerEvent(selects[3], "change");

        await fillIn(".model-text", "some subcategory");

        expect(value).to.deep.equal({
          parameters: {
            subcategory: { source: "fixed", value: "some subcategory" },
          },
          model: "icon",
        });
      });
    });
  });
});
