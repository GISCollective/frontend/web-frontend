/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class TransitionContainerComponent extends Component {
  @tracked isFirstTime = true;
  @tracked state = 'init';

  get divStyle() {
    return htmlSafe(`
      #${this.args.id}.to-toggled { animation-name: ${
        this.animationName
      }; animation-duration: 1s; animation-iteration-count: 1; }
      #${this.args.id}.to-init { animation-name: ${
        this.animationName
      }; animation-duration: 1s; animation-iteration-count: 1; animation-direction: reverse; }
      #${this.args.id}.toggled { ${this.toStyles(this.args.transitionSteps?.[100])} }`);
  }

  toStyles(properties) {
    if (!properties) {
      return '';
    }

    return Object.keys(properties)
      .map((key) => `${key}: ${properties[key]};`)
      .join(' ');
  }

  get steps() {
    const content = Object.keys(this.args.transitionSteps)
      .map((a) => `${a}% { ${this.toStyles(this.args.transitionSteps[a])} }`)
      .join('\n');

    return `@keyframes ${this.animationName} { ${content} }`;
  }

  get animationName() {
    return `${this.args.id}-animation`;
  }

  @action
  unlockAnimation() {
    if (!this.args.transition && this.state == 'toggled') {
      this.state = 'to-init';
      return;
    }

    if (this.args.transition && this.state == 'init') {
      this.state = 'to-toggled';
      return;
    }

    this.setup();
  }

  @action
  setup() {
    if (this.args.transition) {
      this.state = 'toggled';
    }
  }

  @action
  handleAnimationEnd() {
    if (this.state == 'to-toggled') {
      this.state = 'toggled';
    }

    if (this.state == 'to-init') {
      this.state = 'init';
    }

    this.args.onTransitionEnd?.(this.state);
  }

  @action
  handleAnimationStart() {
    this.args.onTransitionStart?.(this.state);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (this.state == 'to-toggled' || this.state == 'to-init') {
      this.handleAnimationEnd();
    }
  }
}
