/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module, test } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | time-interval', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<TimeInterval />`);

    assert.dom(this.element).hasText('');
  });

  it('renders a nice time interval when it is on the same day', async function (a) {
    this.set('value', {
      begin: new Date('2021-01-02T12:00:00Z'),
      end: new Date('2021-01-02T13:00:00Z'),
    });

    await render(hbs`<TimeInterval @value={{this.value}} />`);

    expect(this.element.querySelector('.time-interval-date').textContent.trim()).to.equal('on 02/01/2021');
    expect(this.element.querySelector('.time-interval-time').textContent.trim()).to.equal('13:00 - 14:00');
  });

  it('renders a nice time interval when it is not on the same day', async function (a) {
    this.set('value', {
      begin: new Date('2021-01-02T12:00:00Z'),
      end: new Date('2021-01-03T13:00:00Z'),
    });

    await render(hbs`<TimeInterval @value={{this.value}} />`);

    expect(this.element.querySelector('.time-interval-date').textContent.trim()).to.equal(
      'between 02/01/2021 13:00 and 03/01/2021 14:00',
    );
    expect(this.element.querySelector('.time-interval-time')).not.to.exist;
  });
});
