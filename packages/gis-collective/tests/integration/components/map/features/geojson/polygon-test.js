/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import VectorSource from 'ol/source/Vector';

describe ('Integration | Component | map/features/geojson/polygon',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing', async function () {
    await render(hbs`<Map::Features::Geojson::Polygon />`);
    expect(this.element.textContent.trim()).to.equal('');

    await render(hbs`
      <Map::Features::Geojson::Polygon>
        template block text
      </Map::Features::Geojson::Polygon>
    `);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('adds a polygon to the source', async function () {
    this.set('visible', true);
    this.set('source', new VectorSource());
    this.set('value', {
      type: 'Polygon',
      coordinates: [
        [
          [100.0, 0.0],
          [101.0, 0.0],
          [101.0, 1.0],
          [100.0, 1.0],
          [100.0, 0.0],
        ],
      ],
    });

    expect(this.source.getFeatures().length).to.equal(0);

    await render(
      hbs`{{#if this.visible}}<Map::Features::Geojson::Polygon @source={{this.source}} @value={{this.value}} />{{/if}}`
    );

    let features = this.source.getFeatures();

    expect(features.length).to.equal(1);
    expect(
      JSON.stringify(
        features[0]
          .getGeometry()
          .transform('EPSG:3857', 'EPSG:4326')
          .getCoordinates()
      )
    ).to.equal(
      JSON.stringify([
        [
          [100.0, 0.0],
          [101.0, 0.0],
          [101.0, 1.0],
          [100.0, 1.0],
          [100.0, 0.0],
        ],
      ])
    );

    this.set('visible', false);
    expect(this.source.getFeatures().length).to.equal(0);
  });
});
