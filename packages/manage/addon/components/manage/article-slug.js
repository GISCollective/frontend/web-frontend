/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageArticleSlugComponent extends Component {
  @tracked _value;
  @tracked _validate;

  get value() {
    if (typeof this._value == 'string') {
      return this._value;
    }

    return this.args.value;
  }

  get isDisabled() {
    return typeof this._value != 'string' || this._validate;
  }

  @action
  validate(value) {
    this._validate = value;
  }

  @action
  onChange(value) {
    this._value = value;
  }

  @action
  save() {
    this.args.onSave?.(this.value);
  }

  @action
  cancel() {
    this._value = null;
    this._validate = null;
    this.args.onCancel?.();
  }
}
