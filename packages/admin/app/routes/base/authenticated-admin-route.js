/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { service } from '@ember/service';


export default class AuthenticatedAdminRoute extends Route {
  @service session;
  @service preferences;
  @service fastboot;
  @service store;
  @service router;
  @service user;

  get isFastBoot() {
    return false;
  }

  beforeModel() {
    if (!this.session.isAuthenticated) {
      return this.router.transitionTo('login.index');
    }

    if (!this.user.isAdmin) {
      this.router.replaceWith('index');
    }
  }
}
