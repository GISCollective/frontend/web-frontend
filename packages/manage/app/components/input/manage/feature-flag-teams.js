/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputManageFeatureFlagTeamsComponent extends Component {
  @service user;
  @service store;
  @service fastboot;

  @tracked _team;
  @tracked teams = [];
  @tracked all;

  get team() {
    if (this._team) {
      return this._team;
    }

    return this.teamList[0];
  }

  get showTeamSection() {
    return this.teamList.length > 1;
  }

  get teamList() {
    let list = this.teams;

    if (this.args.flag) {
      list = list.filter((a) => a[this.args.flag]);
    }

    return list;
  }

  @action
  changeTeam(value) {
    this._team = value;
    this.args.onChange?.(this._team);
  }

  @action
  async setup() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    await this.loadTeams();
  }

  async loadTeams() {
    const teamParams = {};

    if (this.all) {
      teamParams.all = this.all;
    } else {
      teamParams.user = this.user.id;
    }

    this.teams = (await this.store.query('team', teamParams)).slice();

    const list = this.teamList;

    if (list.length == 0) {
      return;
    }

    this.args.onChange?.(list[0]);
  }
}
