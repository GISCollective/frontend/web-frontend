/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class AttributesGroupPositionTypeComponent extends Component {
  @service intl;
  @service position;

  typeOptionsWithValues = {
    gps: 'use my current location',
    manual: 'choose manually on the map',
    address: 'by address',
    feature: 'select an existing feature',
  };

  allow = {
    gps: 'allowGps',
    manual: 'allowManual',
    address: 'allowAddress',
    feature: 'allowExistingFeature',
  };

  get options() {
    const options = this.args.options ?? {};

    return Object.keys(this.typeOptionsWithValues)
      .filter((a) => a != 'gps')
      .filter((a) => options[this.allow[a]]);
  }

  get type() {
    return this.args.details?.type;
  }

  @action
  selectType(value) {
    if (this.args.isEditor) {
      return;
    }

    return this.args.onChange?.(value);
  }

  get typeOptions() {
    return Object.keys(this.typeOptionsWithValues)
      .map((a) => this.intl.t(this.typeOptionsWithValues[a]))
      .join(',');
  }
}
