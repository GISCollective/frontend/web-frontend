import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ViewMenuLight extends Component {
  @tracked _logo;
  @tracked _login;
  @tracked _components;
  @service space;
  @service session;

  get currentSpace() {
    return this.args.model?.space;
  }

  get link() {
    return this.space?.landingPagePath ?? '/';
  }

  get components() {
    return this._components ?? this.args.value?.data?.components ?? {};
  }

  get login() {
    return this._login ?? this.args.value?.data?.login ?? {};
  }

  get btnClass() {
    return this.login.style?.classes?.join(' ');
  }

  get logo() {
    const logoId = this.currentSpace?.logo?.id;
    const logo = this._logo ?? this.args.value?.data?.logo ?? {};

    return {
      pictureOptions: {
        picture: logoId,
        position: 'start',
        style: logo?.pictureOptions?.style,
      },
      style: logo.style,
    };
  }

  @action
  updateLogoProps(event) {
    this._logo = event.detail.value;
  }

  @action
  updateComponents(event) {
    this._components = event.detail.value;
  }

  @action
  updateLoginProps(event) {
    this._login = event.detail.value;
  }
}
