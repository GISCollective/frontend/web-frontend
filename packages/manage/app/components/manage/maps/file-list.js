/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { A } from 'core/lib/array';

export default class ManageMapsFileListComponent extends Component {
  @service toaster;
  @service intl;

  interval = 3000;
  viewMode = 'table';
  queryParams = ['viewMode'];
  cachedFiles = A([]);

  get files() {
    const tmp = [];

    if (this.args.files?.length) {
      tmp.push(this.args.files);
    }

    this.cachedFiles = A(tmp);

    return this.cachedFiles;
  }

  get fileActions() {
    return [];
  }

  updateFileMetadata() {
    if (!this.args.files) {
      return;
    }

    Promise.all(this.args.files.map((a) => a.reloadMeta()))
      .then(() => {
        this.fileWatchInterval = setTimeout(() => {
          if (this.isDestroyed) {
            return;
          }

          this.updateFileMetadata();
        }, this.interval);
      })
      .catch((err) => this.toaster.handleError(err));
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.stopFileWatch();
  }

  @action
  setupTimer() {
    this.stopFileWatch();
    this.updateFileMetadata();
  }

  @action
  stopFileWatch() {
    if (this.fileWatchInterval) {
      clearTimeout(this.fileWatchInterval);
    }

    this.fileWatchInterval = null;
  }

  @action
  import() {
    this.args.onImport(...arguments);
  }

  @action
  showLog() {
    this.args.onShowLog(...arguments);
  }

  @action
  delete() {
    this.args.onDelete(...arguments);
  }

  @action
  fileAction(key, items) {
    if (key == 'import') {
      return Promise.all(items.map((a) => this.import(a)));
    }
  }
}
