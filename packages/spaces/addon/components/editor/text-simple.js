/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";

export default class EditorTextSimpleComponent extends Component {
  @tracked _value;

  get value() {
    if (typeof this._value == "string") {
      return this._value;
    }

    return this.args.value;
  }

  set value(newValue) {
    this._value = newValue;

    this.args.onChange?.(newValue);
  }
}
