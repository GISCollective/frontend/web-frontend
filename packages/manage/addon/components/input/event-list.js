import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class InputEventList extends Component {
  @action
  async factoryValue() {
    const list = typeof this.args.list?.toArray ? (await this.args.list).toArray() : this.args.list;

    return list[0] ?? null;
  }
}
