/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';

import { service } from '@ember/service';
import { later, cancel } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';
import Map from 'ol/Map';
import View from 'ol/View';
import * as olInteraction from 'ol/interaction';

export default class OlService extends Service {
  @service fastboot;

  @tracked map;
  @tracked oldW;
  @tracked oldH;
  @tracked sizeObserver;
  @tracked resizeTimer;

  screenshot() {
    return new Promise((resolve, reject) => {
      const exportOptions = {
        filter: function (element) {
          return element.className ? element.className.indexOf('ol-control') === -1 : true;
        },
      };

      this.map.once('rendercomplete', () => {
        // toPng(this.map.getTargetElement(), exportOptions).then(resolve).catch(reject);
      });

      this.map.renderSync();
    });
  }

  onMapResize(map) {
    map.updateSize();
  }

  spyResizeWithTimer(element, map) {
    this.oldW = element.offsetWidth;
    this.oldH = element.offsetHeight;
    cancel(this.resizeTimer);
    const _this = this;

    function sizeCheck() {
      const resizeTimer = later(() => {
        sizeCheck();
      }, 1000);

      _this.resizeTimer = resizeTimer;

      if (_this.oldW == element.offsetWidth && _this.oldH == element.offsetHeight) {
        return;
      }

      _this.oldW = element.offsetWidth;
      _this.oldH = element.offsetHeight;

      _this.onMapResize(map);
    }

    const resizeTimer = later(() => {
      sizeCheck();
    }, 1000);

    _this.resizeTimer = resizeTimer;
  }

  spyResize(element, map) {
    try {
      if (this.sizeObserver) {
        this.sizeObserver.disconnect();
      }

      this.sizeObserver = new ResizeObserver(() => {
        window?.requestAnimationFrame(() => {
          this.onMapResize(map);
        });
      });

      this.sizeObserver.observe(element);
    } catch (err) {
      return this.spyResizeWithTimer(element, map);
    }
  }

  insert(element) {
    const map = this.createMap();
    this.map = map;

    map.setTarget(element);
    this.spyResize(element, map);

    return map;
  }

  createMap() {
    return new Map({
      controls: [],
      loadTilesWhileAnimating: true,
      loadTilesWhileInteracting: true,
      view: new View({
        zoom: 1,
        center: [0, 0],
      }),
      interactions: olInteraction.defaults({
        doubleClickZoom: false,
      }),
    });
  }
}
