/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class InputQuestionComponent extends Component {
  @service store;
  @service intl;

  baseTypes = [
    'short text',
    'long text',
    'integer',
    'decimal',
    'boolean',
    'date',
    'time',
    'email',
    'options',
    'options with other',
    'stream discharge'
  ];

  extendedTypes = ['pictures', 'icons', 'sounds', 'geo json'];
  measurements = ['', 'pH', 'Temperature', 'Dissolved Oxygen', 'Turbidity', 'Conductivity', 'Salinity'];

  @tracked _question;
  @tracked _help;
  @tracked _name;
  @tracked _type;
  @tracked _isRequired;
  @tracked _options;
  @tracked selectedIcons;

  get label() {
    if (this.value.disableDelete) {
      return `question-${this.value.name}`;
    }

    return 'question';
  }

  get types() {
    let result = this.baseTypes;

    if (this.args.extendedTypes) {
      result = [...result, ...this.extendedTypes];
    }

    return result;
  }

  get hasNoQuestion() {
    return !this.question?.trim?.();
  }

  get hasNoName() {
    return !this.name?.trim?.();
  }

  get hasNoType() {
    return !this.type?.trim?.();
  }

  get hasGeoJson() {
    return this.type == 'geo json';
  }

  get hasIcons() {
    return this.type == 'icons';
  }

  get value() {
    return this.args.value ?? {};
  }

  get question() {
    if (this._question !== undefined) {
      return this._question;
    }

    return this.value.question;
  }

  set question(value) {
    this._question = value;
    this.triggerChange();
  }

  get help() {
    if (this._help !== undefined) {
      return this._help;
    }

    return this.value.help;
  }

  set help(value) {
    this._help = value;
    this.triggerChange();
  }

  get name() {
    if (this._name !== undefined) {
      return this._name;
    }

    return this.value.name;
  }

  set name(value) {
    this._name = value;
    this.triggerChange();
  }

  get type() {
    if (this._type !== undefined) {
      return this._type;
    }

    return this.value.type;
  }

  get isRequired() {
    if (this._isRequired !== undefined) {
      return this._isRequired;
    }

    return this.value.isRequired;
  }

  set isRequired(value) {
    this._isRequired = value;

    this.triggerChange();
  }

  get options() {
    if (this._options !== undefined) {
      return this._options;
    }

    return this.value.options;
  }

  set options(value) {
    this._options = value;

    later(() => {
      this.triggerChange();
    });
  }

  get measurement() {
    return this.options?.measurement ?? "";
  }

  @action
  measurementChanged(measurement) {
    this._options = { measurement }

    this.triggerChange();
  }

  @action
  changeIcons(value) {
    this.selectedIcons = value;

    this.triggerChange();
  }

  @action
  changeOptions(options) {
    later(() => {
      this.options = options;
      this.triggerChange();
    });
  }

  get hasOptions() {
    return ['options', 'options with other'].includes(this.type);
  }

  @action
  typeChanged(value) {
    this._type = value;

    if (value == "options" && typeof this.options != "string") {
      this._options = "";
    }

    if (value == "decimal" && typeof this.options != "object") {
      this._options = {};
    }

    this.triggerChange();
  }

  triggerChange() {
    let newValue = this.value;

    newValue.question = this.question;
    newValue.name = this.name;
    newValue.help = this.help;
    newValue.type = this.type;
    newValue.isRequired = this.isRequired;
    newValue.options = this.options;

    if (this.type == 'icons') {
      newValue.options = this.selectedIcons?.map((a) => a.id) ?? [];
    }

    return this.args?.onChange(newValue);
  }

  @action
  async setupIcons() {
    if (!Array.isArray(this.options)) {
      return;
    }

    const result = [];

    for (const id of this.options) {
      let icon = this.store.peekRecord('icon', id);

      if (!icon) {
        icon = await this.store.findRecord('icon', id);
      }

      if (icon) {
        result.push(icon);
      }
    }

    this.selectedIcons = result;
  }
}
