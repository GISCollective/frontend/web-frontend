/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class DecoratorsTransform extends Transform {
  deserialize(serialized) {
    return new Decorators(serialized);
  }

  serialize(deserialized) {
    if (deserialized?.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class Decorators {
  @tracked showAsLineAfterZoom = 99;
  @tracked useDefault = true;
  @tracked changeIndex = 0;
  @tracked maxZoom = 20;
  @tracked minZoom = 0;
  @tracked showLineMarkers = false;
  @tracked keepWhenSmall = true;
  @tracked center = [];

  constructor(value) {
    if (typeof value != 'object' || value === null) {
      return;
    }

    if (!isNaN(value.showAsLineAfterZoom)) {
      this.showAsLineAfterZoom = value.showAsLineAfterZoom;
    }

    if (!isNaN(value.changeIndex)) {
      this.changeIndex = value.changeIndex;
    }

    if (!isNaN(value.maxZoom)) {
      this.maxZoom = value.maxZoom;
    }

    if (!isNaN(value.minZoom)) {
      this.minZoom = value.minZoom;
    }

    if (value.useDefault === true || value.useDefault === false) {
      this.useDefault = value.useDefault;
    }

    if (value.showLineMarkers === true || value.showLineMarkers === false) {
      this.showLineMarkers = value.showLineMarkers;
    }

    if (value.keepWhenSmall === true || value.keepWhenSmall === false) {
      this.keepWhenSmall = value.keepWhenSmall;
    }

    if (Array.isArray(value.center)) {
      this.center = value.center;
    }
  }

  toJSON() {
    return {
      showAsLineAfterZoom: this.showAsLineAfterZoom,
      useDefault: this.useDefault,
      changeIndex: this.changeIndex,
      maxZoom: this.maxZoom,
      minZoom: this.minZoom,
      showLineMarkers: this.showLineMarkers,
      keepWhenSmall: this.keepWhenSmall,
      center: this.center,
    };
  }
}
