/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import config from '../../config/environment';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class LoginIndexController extends Controller {
  @service session;
  @service intl;
  @service fastboot;
  @service store;
  @service user;
  @service preferences;
  @service router;
  @service space;
  @service loading;

  @tracked identification = '';
  @tracked password = '';
  @tracked _isLoading = false;
  @tracked errorMessage;

  host = config.apiUrl;
  queryParams = ['redirect'];

  @tracked redirect = null;

  get isLoading() {
    if(this.loading.isDestroyed) {
      return false;
    }

    return this.loading.isLoading || this._isLoading;
  }

  get hasArticle() {
    if(this.space?.currentSpace?.domain != "new.opengreenmap.org") {
      return false;
    }

    if (this.model.article?.title?.trim() == '') {
      return false;
    }

    if (!this.model.article?.contentBlocks?.blocks?.length) {
      return false;
    }

    return true;
  }

  get redirectPath() {
    const profile = this.user.profileData;

    if (profile?.showWelcomePresentation && this.preferences.showWelcomePresentation) {
      this.store.findRecord('user-profile', this.user.id).then((profile) => {
        profile.showWelcomePresentation = false;
        profile.save();
      });

      return `/presentation/welcome`;
    }

    return `${this.redirect ?? '/'}`;
  }

  @action
  async googleLogin(token) {
    this.errorMessage = null;
    this._isLoading = true;

    try {
      await this.session.authenticate('authenticator:google', token);

      await this.user.getUserData();
      await this.user.loadProfile();

      this.router.replaceWith(`${this.redirectPath}?locale=${this.intl.primaryLocale}`);
    } catch (reason) {
      this.errorMessage = reason?.responseJSON?.error || reason;
    }

    this._isLoading = false;

    return false;
  }

  @action
  async authenticate() {
    this.errorMessage = null;
    this._isLoading = true;

    try {
      let expirationTime = 14 * 24 * 60 * 60;
      this.session.store.cookieExpirationTime = expirationTime;
      await this.session.authenticate('authenticator:oauth2', this.identification.toLowerCase(), this.password);

      await this.user.getUserData();
      await this.user.loadProfile();

      this.router.replaceWith(`${this.redirectPath}?locale=${this.intl.primaryLocale}`);
    } catch (reason) {
      this.errorMessage = reason?.responseJSON?.error || reason;
    }

    this._isLoading = false;

    return false;
  }
}
