/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | calendar-entry-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders an info alert when there is no value', async function (assert) {
    await render(hbs`<CalendarEntryList />`);

    expect(this.element.querySelector('ol')).not.to.exist;
    expect(this.element.querySelector('.alert.alert-info').textContent.trim()).to.equal(
      'Unfortunately, there are no scheduled dates for this event at the moment.',
    );
  });

  it('renders an entry when the list has one', async function (assert) {
    this.set('value', [
      {
        begin: '2023-01-01T00:00:00.000Z',
        end: '2023-01-01T01:00:00.000Z',
        intervalEnd: '2024-05-01T01:00:00.000Z',
        repetition: 'norepeat',
        timezone: 'UTC',
      },
    ]);

    await render(hbs`<CalendarEntryList @value={{this.value}} />`);

    expect(this.element.querySelector('ol')).not.to.exist;
    expect(this.element.querySelector('.alert.alert-info')).not.to.exist;
    expect(
      this.element.textContent
        .split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('on 01/01/2023 00:00 - 01:00 UTC');
  });

  it('renders a list with two entries when they are set', async function (assert) {
    this.set('value', [
      {
        begin: '2023-01-01T00:00:00.000Z',
        end: '2023-01-01T01:00:00.000Z',
        intervalEnd: '2024-05-01T01:00:00.000Z',
        repetition: 'norepeat',
        timezone: 'UTC',
      },
      {
        begin: '2024-01-01T00:00:00.000Z',
        end: '2024-01-01T01:00:00.000Z',
        intervalEnd: '2025-05-01T01:00:00.000Z',
        repetition: 'norepeat',
        timezone: 'UTC',
      },
    ]);

    await render(hbs`<CalendarEntryList @value={{this.value}} />`);

    expect(this.element.querySelector('ol')).to.exist;
    expect(this.element.querySelector('.alert.alert-info')).not.to.exist;

    const listItems = [...this.element.querySelectorAll('ol li')];

    expect(
      listItems[0].textContent
        .split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('on 01/01/2023 00:00 - 01:00 UTC');
    expect(
      listItems[1].textContent
        .split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('on 01/01/2024 00:00 - 01:00 UTC');
  });
});
