/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { sizedBsProperty, setSizedBsProperty } from "core/lib/bs-cols";

export default class ManageEditorsPropertiesBsOptionsComponent extends Component {
  @tracked _type;

  get hasColor() {
    return this.args.value?.includes("btn-color");
  }

  get bgColor() {
    return sizedBsProperty("bg", undefined, this.args.value);
  }

  get textColor() {
    return sizedBsProperty("text", undefined, this.args.value);
  }

  @action
  changeBgColor(value) {
    const options = setSizedBsProperty("bg", undefined, value, this.args.value);

    return this.args.onChange(options);
  }

  @action
  changeTextColor(value) {
    const options = setSizedBsProperty("text", undefined, value, this.args.value);
    return this.args.onChange(options);
  }

  @action
  changeBtn(value) {
    if (!value.includes("btn-color")) {
      value = setSizedBsProperty("bg", undefined, "default", value);
      value = setSizedBsProperty("text", undefined, "default", value);
    }

    return this.args.onChange?.(value);
  }
}
