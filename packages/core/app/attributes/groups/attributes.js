/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import TextValue from '../values/text';
import OptionsValue from '../values/options';
import OptionsOther from '../values/options-other';
import { tracked } from '@glimmer/tracking';

export default class AttributesGroup {
  intl;
  _list = [];
  @tracked key;
  @tracked feature;
  @tracked attributes = {};
  @tracked index;
  @tracked indexed;
  @tracked order;
  @tracked icon;
  @tracked isLoading;

  constructor(intl) {
    this.intl = intl;
  }

  get canBeRemoved() {
    return true;
  }

  get name() {
    if (!this.icon) {
      return this.key;
    }

    if (!this.icon.localName) {
      return this.icon.name;
    }

    return this.icon.localName;
  }

  get key() {
    return this.key;
  }

  get type() {
    return 'list';
  }

  get list() {
    if (!this.icon) {
      return [];
    }

    return this.icon.attributes.map((a) => this.getAttribute(a));
  }

  getAttribute(iconAttribute) {
    let attribute;
    const attributeValue = this.attributes?.[iconAttribute.name];

    switch (iconAttribute.type) {
      case 'options':
        attribute = new OptionsValue(
          iconAttribute.displayName,
          iconAttribute.name,
          iconAttribute.options,
          attributeValue,
        );
        break;

      case 'options with other':
        attribute = new OptionsOther(
          iconAttribute.displayName,
          iconAttribute.name,
          iconAttribute.options,
          attributeValue,
        );
        break;

      default:
        attribute = new TextValue(iconAttribute.displayName, iconAttribute.name, iconAttribute.type, attributeValue);
    }

    if (iconAttribute.help) {
      attribute.help = iconAttribute.help;
    }

    if (iconAttribute.isRequired) {
      attribute.isRequired = iconAttribute.isRequired;
    }

    return attribute;
  }
}
