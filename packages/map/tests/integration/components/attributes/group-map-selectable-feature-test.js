/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/group-map-selectable-feature', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Attributes::GroupMapSelectableFeature />`);

    expect(
      this.element.querySelector('.picker-options').textContent.trim(),
    ).to.equal('Pick a location on the map');
  });
});
