/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PageColNewsletterComponent extends Component {
  elementId = `newsletter-${guidFor(this)}`;

  @tracked _form;
  @tracked _label;
  @tracked email;
  @tracked showEmailInput = true;

  get form() {
    return this._form ?? this.args.value?.data?.form ?? {};
  }

  get label() {
    return this._label ?? this.args.value?.data?.label ?? {};
  }

  get formLabel() {
    return this.label.text || this.form.label;
  }

  get formLabelSuccess() {
    return (
      this.form.labelSuccess ?? 'You are now subscribed to our newsletter.'
    );
  }

  get inputGroup() {
    return this.form.inputGroup;
  }

  get floatingLabels() {
    return this.form.floatingLabels;
  }

  @action
  updateLabelProps(event) {
    this._label = event.detail.value;
  }

  @action
  updateFormProps(event) {
    this._form = event.detail.value;
  }

  @action
  setup(value) {
    this.newsletter = value;
  }

  @action
  async submit() {
    await this.newsletter.subscribe(this.email);
    this.showEmailInput = false;
  }
}
