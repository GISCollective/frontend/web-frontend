import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { fromPageState } from 'core/lib/page-state';
import { GeoJson, GeoJsonGps } from 'core/lib/geoJson';
import PositionDetails, { PositionDetailsGps } from 'core/lib/positionDetails';
import GeocodingSearch from 'core/lib/geocoding-search';
import { later } from '@ember/runloop';

export default class InputGeoJsonComponent extends Component {
  @service position;
  @service store;

  @tracked geocodingSearch;
  @tracked _value;
  @tracked _positionDetails;

  get state() {
    return fromPageState(this.args.state);
  }

  get stateFeatureId() {
    return this.state?.fid;
  }

  get preferredPositionDetails() {
    if (this.stateFeatureId) {
      return new PositionDetails({ type: 'feature', id: this.stateFeatureId });
    }

    return new PositionDetails({ type: '' });
  }

  get positionDetails() {
    if (this._positionDetails) {
      return this._positionDetails;
    }

    return this.preferredPositionDetails;
  }

  get value() {
    return this._value;
  }

  get positionTypeOptions() {
    return (
      this.args.typeOptions ?? {
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      }
    );
  }

  @action
  async searchGeocoding(term) {
    this.hasNoResults = false;

    if (!term) {
      return this.geocodingSearch.clear();
    }

    return await this.geocodingSearch.search(term);
  }

  @action
  setup() {
    later(() => {
      this.triggerChange();
    });
  }

  @action
  triggerChange() {
    this.args.onChange?.(this.value);
    this.args.onChange?.('positionDetails', this.positionDetails);
    this.geocodingSearch = new GeocodingSearch(this.store);
  }

  @action
  async changePositionType(type) {
    if (type == 'gps') {
      this._value = new GeoJsonGps(this.position, this.args.campaign?.area?.center?.coordinates);
      this._positionDetails = new PositionDetailsGps(this.position);

      try {
        await this.position.watchPosition();
      } catch (err) {}

      await this.triggerChange();
    }

    this._positionDetails = new PositionDetails({ type });
  }

  @action
  async changePosition(position, details) {
    this._value = new GeoJson({
      type: 'Point',
      coordinates: [0, 0],
    });

    if (position?.center?.type && position?.center?.coordinates) {
      this._value.coordinates = position.center.coordinates;
    }

    this._positionDetails = details;

    return this.triggerChange();
  }
}
