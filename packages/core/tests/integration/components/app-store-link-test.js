/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from '../../helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | app-store-link', function (hooks) {
  setupRenderingTest(hooks);

  it('renders an apple link', async function () {
    await render(hbs`<AppStoreLink
      @to="https://giscollective.com"
      @storeType="apple"
      class="btn-primary"
    />`);

    expect(this.element.querySelector('.svg-inline--fa')).to.have.class('fa-apple');
    expect(this.element.querySelector('.small-text').textContent.trim()).to.contain('Download on the');
    expect(this.element.querySelector('.large-text').textContent.trim()).to.contain('App Store');
  });

  it('renders an play store link', async function () {
    await render(hbs`<AppStoreLink
      @to="https://giscollective.com"
      @storeType="android"
      class="btn-primary"
    />`);

    expect(this.element.querySelector('.svg-inline--fa')).to.have.class('fa-google-play');
    expect(this.element.querySelector('.small-text').textContent.trim()).to.contain('GET IT ON');
    expect(this.element.querySelector('.large-text').textContent.trim()).to.contain('Google Play');
  });
});
