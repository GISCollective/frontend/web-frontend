/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './group-map-base';
import { action } from '@ember/object';
import { transformExtent, transform } from 'ol/proj';
import { getCenter } from 'ol/extent';
import { GeoJson } from 'core/lib/geoJson';
import PositionDetails from 'core/lib/positionDetails';
import {service} from '@ember/service';
import { later } from "@ember/runloop";

export default class AttributesGroupMapSelectableFeatureComponent extends Component {
  @service store;

  @action
  checkEditMode() {
    const diff = new Date().getTime() - this.dismissedAt;

    if (diff < 100) {
      return;
    }

    if (this.args.details?.type != 'feature') {
      return this.edit();
    }

    if (!this.args.details?.id) {
      return this.edit();
    }
  }

  @action
  async mapSetup(map) {
    super.mapSetup(map);

    if(this.args.details?.type != "feature" || !this.args.details?.id) {
      return;
    }

    let feature = this.store.peekRecord('feature', this.args.details.id);

    if (!feature) {
      feature = await this.store.findRecord('feature', this.args.details.id);
    }


    let center = transform(
      feature.position.center.coordinates ?? [0, 0],
      'EPSG:4326',
      'EPSG:3857',
    );

    later(() => {
      this._map.getView().setZoom(15);
      this._map.getView().setCenter(center);
    }, 100)
  }

  @action
  confirm() {
    if (!this.selectedPosition || !this.selectedDetails) {
      return;
    }

    this.args.onChange(this.selectedPosition, this.selectedDetails);
    super.confirm();
  }

  @action
  selectFeature(value) {
    const properties = value?.getProperties?.() ?? {};
    const id = properties._id || properties.id;

    if (!id) {
      return;
    }

    let extent = value.getGeometry().getExtent();

    if (!this.editMode) {
      this._map.getView().setZoom(15);
      this._map.getView().setCenter(getCenter(extent));
    }

    extent = transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
    const center = getCenter(extent);

    const sameId = id == this.args.details?.id;
    const sameCenter =
      JSON.stringify(center) == JSON.stringify(this.position?.coordinates);

    if (sameId && sameCenter) {
      return;
    }

    this.selectedPosition = new GeoJson({ type: 'Point', coordinates: center });
    this.selectedDetails = new PositionDetails({ type: 'feature', id });
  }
}
