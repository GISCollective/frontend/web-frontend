/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AddPageController extends Controller {
  @service store;
  @service intl;
  @service router;

  @tracked all = false;

  destination = 'manage.spaces.edit';
  queryParams = ['all', 'next'];

  get editableModel() {
    return this.model.space;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    if (this.model.space.visibility?.team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: this.model.space.visibility?.team?.name,
        model: this.model.space.visibility?.team?.id,
      });
    }

    pieces.push({
      text: this.intl.t('new space'),
      capitalize: true,
    });

    return pieces;
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  async save() {
    let result;

    result = await this.editableModel.save();

    this.router.transitionTo(this.destination, result.id);

    return result;
  }
}
