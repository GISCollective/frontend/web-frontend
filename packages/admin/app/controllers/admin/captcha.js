/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class AdminCaptchaController extends Controller {
  @service notifications;
  @tracked isSaving;
  @tracked isError;

  status = [
    { name: 'disabled', value: 'false' },
    { name: 'reCaptcha', value: 'reCAPTCHA' },
    { name: 'mtCaptcha', value: 'mtCAPTCHA' },
  ];

  get isDirty() {
    return Object.keys(this.model).filter((a) => this.model[a].hasDirtyAttributes).length > 0;
  }

  get isDisabled() {
    return !this.isDirty || this.isSaving;
  }

  @action
  save() {
    return Promise.all(
      Object.keys(this.model)
        .filter((a) => this.model[a].hasDirtyAttributes)
        .map((a) => this.model[a].save())
    )
  }
}
