/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, settled, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/source-field", function (hooks) {
  setupRenderingTest(hooks);
  let mapData;
  let map;
  let store;
  let server;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup("service:store");
    mapData = server.testData.storage.addDefaultMap();

    map = await store.findRecord("map", mapData._id);
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders an empty value and name when the col is not set", async function (a) {
    await render(hbs`<Editor::SourceField />`);

    expect(this.element.textContent.trim()).to.equal("");
  });

  it("renders the options for a map", async function (a) {
    this.set("value", { name: "tagLine" });
    this.set("model", { record: map });
    this.set("valueElement", null);

    await render(
      hbs`<div class="elm" data-model-key="record"></div><Editor::SourceField @model={{this.model}} @value={{this.value}} @element={{this.valueElement}}/>`,
    );

    this.set("valueElement", this.element.querySelector(".elm"));

    await settled();

    expect(this.element.querySelector(".source-field").value).to.equal("tagLine");
  });

  it("can change the options for a map", async function (a) {
    this.set("value", { name: "tagLine" });
    this.set("model", { record: map });
    this.set("valueElement", null);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(
      hbs`<div class="elm" data-model-key="record"></div><Editor::SourceField @onChange={{this.change}} @model={{this.model}} @value={{this.value}} @element={{this.valueElement}}/>`,
    );

    this.set("valueElement", this.element.querySelector(".elm"));
    await settled();

    this.element.querySelector(".source-field").value = "description";
    await triggerEvent(".source-field", "change");

    expect(value).to.deep.equal({
      name: "description",
    });
  });
});
