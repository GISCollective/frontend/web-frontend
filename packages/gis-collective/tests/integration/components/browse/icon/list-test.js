/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | browse/icon/list',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Browse::Icon::List />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders large icons', async function () {
    await render(hbs`<Browse::Icon::List @size="lg" />`);
    expect(this.element.querySelector('.size-lg')).to.exist;
  });

  it('renders a list of 2 icons', async function () {
    this.set('icons', [{}, {}]);
    await render(hbs`<Browse::Icon::List @icons={{this.icons}} />`);

    expect(this.element.querySelectorAll('.text-danger').length).to.equal(2);
  });

  it('updates the rendered icons if they change', async function () {
    this.set('icons', [{}, {}]);
    await render(hbs`<Browse::Icon::List @icons={{this.icons}} />`);

    this.set('icons', []);
    expect(this.element.querySelectorAll('.text-danger').length).to.equal(0);
  });

  it('resolves a promise', async function () {
    this.set(
      'icons',
      new Promise((resolve) => {
        resolve([{}, {}]);
      })
    );

    await render(hbs`<Browse::Icon::List @icons={{this.icons}} />`);

    expect(this.element.querySelectorAll('.text-danger').length).to.equal(2);
  });
});
