/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/authenticated-admin-route';
import { service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminLocationServicesRoute extends Route {
  @service preferences;

  model() {
    const fields = ['maskingPrecision'];
    const items = {};

    fields.forEach((key) => {
      items[key] = this.preferences.requestPreference(`locationServices.${key}`);
    });

    return hash(items);
  }
}
