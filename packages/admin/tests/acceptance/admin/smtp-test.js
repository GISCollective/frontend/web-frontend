/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import {
  visit,
  currentURL,
  fillIn,
  triggerEvent,
  click,
} from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';

describe ('Acceptance | admin/smtp',  function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultSpace();
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to index if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/');
  });

  it('redirects to index if the user is not authenticated', async function () {
    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/login');
  });

  it('lets administrators visit /admin/smtp', async function () {
    authenticateSession();

    server.testData.storage.addPreference('secret.mail.type', '');
    server.testData.storage.addPreference('secret.mailersend.key', 'key');
    server.testData.storage.addPreference('secret.mailersend.from', 'from');
    server.testData.storage.addPreference('secret.smtp.authType', '');
    server.testData.storage.addPreference('secret.smtp.connectionType', '');
    server.testData.storage.addPreference('secret.smtp.host', '');
    server.testData.storage.addPreference('secret.smtp.port', '');
    server.testData.storage.addPreference('secret.smtp.password', '');
    server.testData.storage.addPreference('secret.smtp.username', '');
    server.testData.storage.addPreference('secret.smtp.from', '');

    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/admin/smtp');

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;
  });

  describe ('using the smtp mail settings',  function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addPreference('secret.mail.type', 'smtp');
      server.testData.storage.addPreference('secret.smtp.authType', 'login');
    });

    it('shows a disabled save button by default', async function () {
      authenticateSession();

      server.testData.storage.addPreference('secret.smtp.authType', '');
      server.testData.storage.addPreference('secret.mailersend.key', 'key');
      server.testData.storage.addPreference('secret.mailersend.from', 'from');
      server.testData.storage.addPreference('secret.smtp.connectionType', '');
      server.testData.storage.addPreference('secret.smtp.host', '');
      server.testData.storage.addPreference('secret.smtp.port', '');
      server.testData.storage.addPreference('secret.smtp.password', '');
      server.testData.storage.addPreference('secret.smtp.username', '');
      server.testData.storage.addPreference('secret.smtp.from', '');

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
        'disabled',
        ''
      );
    });

    it('hides the mailer send fields', async function () {
      authenticateSession();

      server.testData.storage.addPreference('secret.smtp.authType', '');
      server.testData.storage.addPreference('secret.mailersend.key', 'key');
      server.testData.storage.addPreference('secret.mailersend.from', 'from');
      server.testData.storage.addPreference('secret.smtp.connectionType', '');
      server.testData.storage.addPreference('secret.smtp.host', '');
      server.testData.storage.addPreference('secret.smtp.port', '');
      server.testData.storage.addPreference('secret.smtp.password', '');
      server.testData.storage.addPreference('secret.smtp.username', '');
      server.testData.storage.addPreference('secret.smtp.from', '');

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      expect(this.element.querySelector('.input-mailersend-key')).not.to.exist;
      expect(this.element.querySelector('.input-mailersend-from')).not.to.exist;
    });

    it('shows the server values in the inputs', async function () {
      authenticateSession();

      server.testData.storage.addPreference('secret.mailersend.key', 'key');
      server.testData.storage.addPreference('secret.mailersend.from', 'from');
      server.testData.storage.addPreference('secret.smtp.authType', 'login');
      server.testData.storage.addPreference(
        'secret.smtp.connectionType',
        'tls'
      );
      server.testData.storage.addPreference('secret.smtp.host', 'test.host');
      server.testData.storage.addPreference('secret.smtp.port', '213');
      server.testData.storage.addPreference('secret.smtp.password', 'secret');
      server.testData.storage.addPreference('secret.smtp.username', 'me');
      server.testData.storage.addPreference(
        'secret.smtp.from',
        'test@gmail.com'
      );

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      expect(this.element.querySelector('.input-mail-type').value).to.equal(
        'smtp'
      );
      expect(
        this.element.querySelector('.input-smtp-auth-type').value
      ).to.equal('login');
      expect(
        this.element.querySelector('.input-smtp-connection-type').value
      ).to.equal('tls');
      expect(this.element.querySelector('.input-smtp-host').value).to.equal(
        'test.host'
      );
      expect(this.element.querySelector('.input-smtp-port').value).to.equal(
        '213'
      );
      expect(this.element.querySelector('.input-smtp-password').value).to.equal(
        'secret'
      );
      expect(this.element.querySelector('.input-smtp-username').value).to.equal(
        'me'
      );
      expect(this.element.querySelector('.input-smtp-from').value).to.equal(
        'test@gmail.com'
      );
    });

    it('triggers a test email when the test button is pressed', async function () {
      let called;
      server.post(
        '/mock-server/users/5b870669796da25424540deb/testNotification',
        () => {
          called = true;
          return [200, { 'Content-Type': 'application/json' }];
        }
      );

      authenticateSession();

      server.testData.storage.addPreference('secret.mailersend.key', 'key');
      server.testData.storage.addPreference('secret.mailersend.from', 'from');
      server.testData.storage.addPreference('secret.smtp.authType', 'login');
      server.testData.storage.addPreference(
        'secret.smtp.connectionType',
        'tls'
      );
      server.testData.storage.addPreference('secret.smtp.host', 'test.host');
      server.testData.storage.addPreference('secret.smtp.port', '213');
      server.testData.storage.addPreference('secret.smtp.password', 'secret');
      server.testData.storage.addPreference('secret.smtp.username', 'me');
      server.testData.storage.addPreference(
        'secret.smtp.from',
        'test@gmail.com'
      );

      await visit('/admin/smtp');

      await click('.btn-test-message');

      expect(called).to.be.true;
    });
  });

  describe ('failing put request',  function (hooks) {
    hooks.beforeEach(function () {
      server.put('/mock-server/preferences/:id', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                title: 'Some error',
                description: 'some message',
                status: 400,
              },
            ],
          }),
        ];
      });

      server.testData.storage.addPreference('secret.mailersend.key', 'key');
      server.testData.storage.addPreference('secret.mailersend.from', 'from');
      server.testData.storage.addPreference('secret.mail.type', 'smtp');
      server.testData.storage.addPreference('secret.smtp.authType', '');
      server.testData.storage.addPreference('secret.smtp.connectionType', '');
      server.testData.storage.addPreference('secret.smtp.host', '');
      server.testData.storage.addPreference('secret.smtp.port', '');
      server.testData.storage.addPreference('secret.smtp.password', '');
      server.testData.storage.addPreference('secret.smtp.username', '');
      server.testData.storage.addPreference(
        'secret.smtp.from',
        'test@gmail.com'
      );
    });

    it('should show a modal with the error', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-host', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));
      await Modal.waitToDisplay(4000);

      expect(
        document.querySelector('.modal-title').textContent.trim()
      ).to.equal('Some error');
      expect(document.querySelector('.modal-body').textContent.trim()).to.equal(
        'some message'
      );

      await click('.btn-resolve');
      await Modal.waitToHide(4000);
    });
  });

  describe ('successful put request',  function (hooks) {
    let receivedPreference;

    hooks.beforeEach(function () {
      server.put('/mock-server/preferences/:id', (request) => {
        receivedPreference = JSON.parse(request.requestBody);
        receivedPreference.preference._id = receivedPreference.preference.name;

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedPreference),
        ];
      });

      server.testData.storage.addPreference('secret.mail.type', 'smtp');
      server.testData.storage.addPreference('secret.mailersend.key', 'key');
      server.testData.storage.addPreference('secret.mailersend.from', 'from');
      server.testData.storage.addPreference('secret.smtp.authType', '');
      server.testData.storage.addPreference('secret.smtp.connectionType', '');
      server.testData.storage.addPreference('secret.smtp.host', '');
      server.testData.storage.addPreference('secret.smtp.port', '');
      server.testData.storage.addPreference('secret.smtp.password', '');
      server.testData.storage.addPreference('secret.smtp.username', '');
      server.testData.storage.addPreference('secret.smtp.from', '');
    });

    it('Changing the mail type should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-mail-type', 'mailersend.com');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.mail.type', _id: 'secret.mail.type', value: 'mailersend.com' },
      });
    });

    it('Changing the mailersend key should enable the save', async function () {
      authenticateSession();

      server.testData.storage.addPreference(
        'secret.mail.type',
        'mailersend.com'
      );

      await visit('/admin/smtp');

      expect(
        this.element.querySelector('.input-mailersend-key').value
      ).to.equal('key');

      await fillIn('.input-mailersend-key', 'some-key');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.mailersend.key', _id: 'secret.mailersend.key', value: 'some-key' },
      });
    });

    it('Changing the mailersend from should enable the save', async function () {
      authenticateSession();

      server.testData.storage.addPreference(
        'secret.mail.type',
        'mailersend.com'
      );

      await visit('/admin/smtp');

      expect(
        this.element.querySelector('.input-mailersend-from').value
      ).to.equal('from');

      await fillIn('.input-mailersend-from', 'some-from');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.mailersend.from', _id: 'secret.mailersend.from', value: 'some-from' },
      });
    });

    it('Changing the host should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-host', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.host', _id: 'secret.smtp.host', value: 'new value' },
      });
    });

    it('Changing the port should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-port', '42');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.port', _id: 'secret.smtp.port', value: '42' },
      });
    });

    it('Changing the password should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-password', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.password', _id: 'secret.smtp.password', value: 'new value' },
      });
    });

    it('Changing the username should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-username', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: {
          name: 'secret.smtp.username', _id: 'secret.smtp.username', value: 'new value'
        },
      });
    });

    it('Changing the from field should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-from', 'new_value@gmail.com');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.from', _id: 'secret.smtp.from', value: 'new_value@gmail.com' },
      });
    });

    it('Changing the auth-type value should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      this.element.querySelector('.input-smtp-auth-type').value = 'plain';
      await triggerEvent('.input-smtp-auth-type', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.authType', _id: 'secret.smtp.authType', value: 'plain' },
      });
    });

    it('Changing the connection-type value should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      this.element.querySelector('.input-smtp-connection-type').value = 'plain';
      await triggerEvent('.input-smtp-connection-type', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.connectionType', _id: 'secret.smtp.connectionType', value: 'plain' },
      });
    });
  });
});
