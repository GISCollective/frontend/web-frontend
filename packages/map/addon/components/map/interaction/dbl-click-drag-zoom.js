/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import DblClickDragZoom from 'ol/interaction/DblClickDragZoom';
import { action } from '@ember/object';

export default class MapInteractionDblClickDragZoomComponent extends Component {
  @action
  setupOl() {
    if (!this.args.map) {
      return;
    }

    this.interaction = new DblClickDragZoom({
      duration: this.args.duration ?? 0,
      delta: this.args.delta ?? 20,
      stopDown: this.args.stopDown,
    });

    this.args.map.addInteraction(this.interaction);
    this.currentInteractions = [this.interaction];
  }

  willDestroy() {
    super.willDestroy(...arguments);

    if (this.currentInteractions && this.args.map) {
      this.currentInteractions.forEach((item) => {
        this.args.map.removeInteraction(item);
      });
    }
  }
}
