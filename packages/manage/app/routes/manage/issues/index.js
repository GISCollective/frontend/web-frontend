/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import LazyList from 'models/lib/lazy-list';
import { hash } from 'rsvp';

export default class ManageIssuesRoute extends AuthenticatedRoute {
  queryParams = {
    feature: {
      refreshModel: true,
    },
  };

  async model(params) {
    const issues = new LazyList('issue', { feature: params.feature }, this.store);
    await issues.loadNext();

    return hash({
      feature: this.store.findRecord('feature', params.feature),
      issues,
    });
  }
}
