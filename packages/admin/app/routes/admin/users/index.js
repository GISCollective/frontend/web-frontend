/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../../base/authenticated-admin-route';
import LazyList from 'models/lib/lazy-list';
import { service } from '@ember/service';

export default class AdminUsersIndexRoute extends Route {
  @service searchStorage;

  queryParams = {
    search: {
      refreshModel: true,
      replace: true,
    },
  };

  async model(params) {
    const queryParams = {
      edit: true,
    };

    params['edit'] = true;
    if (params['search'] && params['search'] != '') {
      this.searchStorage.term = params['search'];
      queryParams['term'] = params['search'];
    }

    const userList = new LazyList("user", queryParams, this.store);
    await userList.loadNext();

    return userList;
  }
}
