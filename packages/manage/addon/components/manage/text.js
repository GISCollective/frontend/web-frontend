/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

const validateEmail = (email) => {
  return email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
};


export default class ManageTextComponent extends Component {
  @tracked value = '';

  get isInvalid() {
    if(this.args.type != "email" || this.value == "") {
      return false;
    }

    return !validateEmail(this.value);
  }

  get validationMessage() {
    if(!this.isInvalid) {
      return "";
    }

    return "manage-section.invalid-email";
  }

  @action
  change(newValue) {
    this.value = newValue;
  }

  @action
  setupValue() {
    this.value = this.args.value;
  }

  @action
  updateValue(newValue) {
    if (typeof newValue.id == 'string') {
      this.value = newValue.id;
      return;
    }

    this.value = newValue;
  }

  @action
  save() {
    return this.args.onSave(this.value);
  }
}
