import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ViewAttributesGroup extends Component {
  @tracked attributes;

  @action
  loaded(record) {
    let result = {};

    const keys = this.args.value?.data?.sourceAttribute?.groups;

    if (!keys.length) {
      return;
    }

    for (let key of keys) {
      result = { ...(record.attributes?.[key] ?? {}) };
    }

    this.attributes = result;
  }
}
