/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, fillIn, typeIn, click, waitUntil, waitFor, triggerEvent, settled } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';

describe('Integration | Component | new-record-form/article-message', function (hooks) {
  setupRenderingTest(hooks);

  let receivedArticle;

  let server;
  let newsletterData;
  let receivedPicture;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultTeam();
    newsletterData = server.testData.storage.addDefaultNewsletter();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    receivedArticle = null;

    server.post('/mock-server/articles', (request) => {
      receivedArticle = JSON.parse(request.requestBody);
      receivedArticle.article._id = '1';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedArticle)];
    });

    receivedPicture = null;
    server.post('/mock-server/pictures', (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = '1';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPicture)];
    });
  });

  it('renders a disabled add button by default', async function () {
    await render(hbs`<NewRecordForm::ArticleMessage />`);

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');
  });

  it('can submit a new article when the fields are filled in', async function () {
    this.set('newsletter', await this.store.findRecord('newsletter', newsletterData._id));

    await render(hbs`<NewRecordForm::ArticleMessage @newsletter={{this.newsletter}} />`);

    await waitFor('.editor-is-ready', { timeout: 3000 });

    await click('.add-title-link');

    await waitUntil(() => !this.element.querySelector('.alert-danger'));

    await click('.add-paragraph-link');

    await waitUntil(() => !this.element.querySelector('.add-paragraph-link'));

    await fillIn('.input-subject', 'new-subject');

    await fillIn('.ce-paragraph', '');
    await typeIn('.ce-paragraph', 'test message');
    await PageElements.wait(510);

    await fillIn('.datepicker-input', '2025-04-20');

    this.element.querySelector('.form-select.hour').value = '12';
    await triggerEvent('.form-select.hour', 'change');

    this.element.querySelector('.form-select.minute').value = '30';
    await triggerEvent('.form-select.minute', 'change');

    await waitUntil(() => !this.element.querySelector('.btn-submit').attributes.getNamedItem('disabled'));

    await click('.btn-submit');

    await waitUntil(() => receivedArticle);

    expect(receivedArticle).to.deep.equal({
      article: {
        title: 'New title',
        content: {
          blocks: [
            { type: 'header', data: { text: 'New title', level: 1 } },
            { type: 'paragraph', data: { text: 'test message' } },
          ],
        },
        slug: '',
        subject: 'new-subject',
        type: 'newsletter-article',
        relatedId: '5ca78e2160780601008f69ff',
        status: 'draft',
        releaseDate: '2025-04-20T10:30:00.000Z',
        categories: [],
        order: 999,
        visibility: { isPublic: false, isDefault: false, team: '5ca78e2160780601008f69e6' },
        cover: null,
        _id: '1',
      },
    });
  });

  it('can add a picture', async function (a) {
    this.set('newsletter', await this.store.findRecord('newsletter', newsletterData._id));

    await render(hbs`<NewRecordForm::ArticleMessage @newsletter={{this.newsletter}} />`);

    await waitFor('.editor-is-ready', { timeout: 3000 });

    await click('.ce-paragraph');
    await click('.ce-toolbar__plus');
    await click(".ce-popover-item[data-item-name='image']");

    await waitUntil(() => document.querySelector("[type='file']"));

    const blob = server.testData.create.pngBlob();
    await triggerEvent(document.querySelector("[type='file']"), 'change', { files: [blob] });

    await waitUntil(() => receivedPicture);

    a.deepEqual(receivedPicture.picture.meta, {
      renderMode: '',
      attributions: '',
      link: {},
      data: {},
      disableOptimization: true,
    });
  });
});
