/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class BrowseSiteCoverComponent extends Component {
  @tracked isLoaded;
  @tracked pictures;

  get route() {
    return this.args.route ?? 'browse.sites.site';
  }

  get picture() {
    return this.args.feature?.cover;
  }

  get size() {
    return `${this.picture.picture}`.indexOf('.jpg') > 0 ? '.md.jpg' : '/md';
  }

  get pictureUrl() {
    if (!this.hasPicture || !this.picture?.picture) {
      return '';
    }

    return `${this.picture.picture}${this.size}`;
  }

  get hashStyle() {
    if (this.args.feature?.cover) {
      return htmlSafe(this.args.feature.cover.hashBg ?? '');
    }

    return htmlSafe(this.args.feature?.hashBg ?? '');
  }

  get style() {
    if (!this.hasPicture) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${this.pictureUrl}')`);
  }

  get hasPicture() {
    if (typeof this.pictures?.[0] != 'undefined') {
      return true;
    }

    if (typeof this.args.feature?.cover != 'undefined') {
      return true;
    }

    return !!this.pictures?.[0];
  }

  @action
  async setup() {
    this.pictures = await this.args.feature?.pictures;
  }

  @action
  linkRendered(element) {
    this.link = element;
  }

  @action
  transitionToSite(event) {
    event.preventDefault();

    if (!this.args.onClick) {
      this.link.click();
      return;
    }

    if (this.args.onClick) {
      later(() => {
        this.args.onClick?.(this.args.feature);
      });
    }

    return false;
  }

  @action
  loaded() {
    this.isLoaded = true;
  }
}
