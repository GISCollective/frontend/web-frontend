/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/fragment', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let pageData;

  hooks.before(function () {
    server = new TestServer();
    pageData = server.testData.storage.addDefaultPage('1');
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::Fragment />`);

    assert.dom(this.element).hasText('');
  });

  it('renders a page with 2 cols when the page is set as value', async function () {
    this.set('value', {
      source: {},
      modelKey: 'page_1',
    });

    const page = await this.store.findRecord('page', '1');
    this.set('model', {
      page_1: page,
    });

    await render(
      hbs`<View::Fragment @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelectorAll('.page-col')).to.have.length(2);
    expect(
      this.element.querySelector('.page-col-0-0-0 h1'),
    ).to.have.textContent('title');
    expect(this.element.querySelector('.page-col-0-0-0 p')).to.have.textContent(
      'some content',
    );
    expect(this.element.querySelector('img').getAttribute('src')).to.contain(
      '/test/5d5aa72acac72c010043fb59.jpg.',
    );
  });

  it('does not render a fragment from the page', async function () {
    this.set('value', {
      source: {},
      modelKey: 'page_1',
    });

    const page = await this.store.findRecord('page', '1');
    page.cols[0].data = {
      source: {
        model: 'page',
        id: '1',
      },
    };
    page.cols[0].type = 'fragment';

    this.set('model', {
      page_1: page,
    });

    await render(
      hbs`<View::Fragment @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelector("[data-name='0.0.0']")).to.exist;
    expect(this.element.querySelector("[data-name='0.0.0'] .view-fragment")).not
      .to.exist;
  });
});
