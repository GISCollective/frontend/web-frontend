import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ViewContentPickerComponent extends Component {
  @tracked _options;

  get options() {
    if (this._options) {
      return this._options;
    }

    return this.args.options ?? {};
  }

  get value() {
    if (!this.args.value) {
      return { blocks: [] };
    }

    if (this.args.options?.mode == 'paragraph') {
      let index = parseInt(this.options.paragraphIndex);
      if (isNaN(index)) {
        index = 0;
      }

      const blocks = this.args.value.blocks?.filter(
        (a) => a.type == 'paragraph',
      );

      return { blocks: [blocks[index]] };
    }

    return this.args.value;
  }

  @action
  updateProps(event) {
    this._options = event.detail.value;
  }
}
