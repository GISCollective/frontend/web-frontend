/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { debounce } from '@ember/runloop';

export default class InputArticleSlugComponent extends Component {
  @service store;

  @tracked _value;
  @tracked loading = 0;
  @tracked article;

  get timeout() {
    return this.args.timeout ?? 1000;
  }

  get value() {
    if (typeof this._value == 'string') {
      return this._value;
    }

    return this.args.value ?? '';
  }

  set value(newValue) {
    this._value = newValue;

    debounce(this, this.validate, this.timeout);
    this.args.onChange?.(this.value);
    this.args.onValidate?.(this.validation);
  }

  get validation() {
    if (!this.value) {
      return;
    }

    if (this.value.indexOf(' ') != -1) {
      return 'slug-no-space';
    }

    if (this.value.indexOf('--') != -1) {
      return 'slug-double-dash';
    }

    if (this.value.toLowerCase() != this.value) {
      return 'slug-no-uppercase';
    }

    if (!this.value.match(/^[0-9a-z\-]+$/)) {
      return 'slug-invalid-chars';
    }

    if (this.article?.slug == this.value) {
      return 'slug-taken';
    }

    return '';
  }

  @action
  setup() {
    this.initialValue = this.args.value;
  }

  @action
  async validate() {
    if (this.isDestroying) {
      return;
    }

    if (!this.value || this.value == this.initialValue) {
      this.article = null;
      this.args.onValidate?.(this.validation);
      return;
    }

    const id = this.value;
    const props = {
      id,
    };

    if (this.args.teamId) {
      props.team = this.args.teamId;
    }

    let result;

    try {
      this.loading++;
      result = await this.store.queryRecord('article', props);
    } catch (err) {
      result = null;
    } finally {
      this.loading--;
    }

    if (id == this.value) {
      this.article = result;
    }

    this.args.onValidate?.(this.validation);
  }
}
