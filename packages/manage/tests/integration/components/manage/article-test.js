/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, fillIn, render, waitFor, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { describe } from 'dummy/tests/helpers';
import { expect } from 'chai';

module('Integration | Component | manage/article', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the value on view mode', async function (assert) {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    await render(hbs`<Manage::Article @value={{this.value}} @title="title" />`);

    expect(this.element.querySelector('.view-container h1').textContent.trim()).to.equal('title1');
  });

  describe('in edit mode', function (hooks) {
    hooks.beforeEach(function () {
      this.set('editablePanel', 'some title');
      this.set('onEdit', () => {});
    });

    it('renders', async function () {
      await render(hbs`<Manage::Article @editablePanel={{this.editablePanel}} @title="some title" />`);

      expect(this.element.querySelector('.alert-danger')).to.exist;
      expect(this.element.querySelector('.alert-danger').textContent.trim()).to.contain(
        'You need to provide a level 1 Heading for the article title',
      );
    });

    it('renders a heading block value', async function () {
      this.set('value', {
        blocks: [
          {
            type: 'header',
            data: {
              text: 'title1',
              level: 1,
            },
          },
        ],
      });

      await render(
        hbs`<Manage::Article @editablePanel={{this.editablePanel}} @title="some title" @onCancel={{this.onCancel}} @value={{this.value}}/>`,
      );
      await waitFor('.editor-is-ready', { timeout: 2000 });

      expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.alert-danger')).not.to.exist;
      expect(this.element.querySelector('h1')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title1');
    });

    it('does not trigger a publish on change', async function () {
      this.set('value', {
        blocks: [
          {
            type: 'header',
            data: {
              text: 'title1',
              level: 1,
            },
          },
        ],
      });

      let title;
      let value;
      this.set('publish', (t, v) => {
        title = t;
        value = v;
      });

      await render(
        hbs`<Manage::Article @editablePanel={{this.editablePanel}} @title="some title" @value={{this.value}} @onSave={{this.save}} @onCancel={{this.onCancel}}/>`,
      );
      await waitFor('.editor-is-ready', { timeout: 2000 });

      await fillIn('h1', 'newValue');

      waitFor('.editor-changed');

      await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));

      expect(title).not.to.exist;
      expect(value).not.to.exist;
      expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', null);
      expect(this.element.querySelector('.btn-cancel')).to.have.attribute('disabled', null);
    });

    it('resets the value on click on reset', async function () {
      this.set('value', {
        blocks: [
          {
            type: 'header',
            data: {
              text: 'title1',
              level: 1,
            },
          },
        ],
      });

      let title;
      let value;
      this.set('publish', (t, v) => {
        title = t;
        value = v;
      });

      await render(
        hbs`<Manage::Article @editablePanel={{this.editablePanel}} @title="some title" @value={{this.value}} @onPublish={{this.publish}}/>`,
      );
      await waitFor('.editor-is-ready', { timeout: 2000 });

      await fillIn('h1', 'newValue');

      waitFor('.editor-changed');

      await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));

      await click('.btn-cancel');

      await waitFor('.editor-is-ready', { timeout: 2000 });

      expect(this.element.querySelector('h1').textContent).to.equal('title1');

      expect(title).not.to.exist;
      expect(value).not.to.exist;
      expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');
    });

    it('publishes the value on click on publish', async function () {
      this.set('value', {
        blocks: [
          {
            type: 'header',
            data: {
              text: 'title1',
              level: 1,
            },
          },
        ],
      });

      let title;
      let value;
      this.set('save', (t, v) => {
        title = t;
        value = v;
      });

      await render(
        hbs`<Manage::Article @editablePanel={{this.editablePanel}} @title="some title" @value={{this.value}} @onSave={{this.save}} @onEdit={{this.onEdit}}/>`,
      );
      await waitFor('.editor-is-ready', { timeout: 2000 });

      await fillIn('h1', 'newValue');

      waitFor('.editor-changed');

      await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));

      await click('.btn-submit');

      expect(title).to.equal('newValue');
      expect(value.blocks).to.deep.equal([
        {
          type: 'header',
          data: {
            text: 'newValue',
            level: 1,
          },
        },
      ]);
      expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', null);
    });
  });
});
