/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AddController from '../../base/add-controller';

export default class AddPresentationController extends AddController {
  destination = 'manage.presentations.edit';

  get canSubmit() {
    return this.editableModel.name && this.editableModel.slug && this.hasTeam;
  }

  get editableModel() {
    return this.model.presentation;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.presentation.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new presentation'),
      capitalize: true,
    });

    return pieces;
  }
}
