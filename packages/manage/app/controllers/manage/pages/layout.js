/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from './base';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class ManagePagesLayoutController extends Controller {
  @service store;

  @tracked configureLayout = true;
  @tracked title = 'page';

  @tracked location = [];
  @tracked selection = null;
  @tracked hasVisibleContainers = true;
  @tracked pageRevisions;

  async reset() {
    this.configureLayout = true;
    this.title = 'page';
    this.location = [];
    this.selection = null;
    this.hasVisibleContainers = true;
    this.pageRevisions = await this.model.page?.getRevisions();
  }

  get containers() {
    return this.model.page.layoutContainers;
  }

  get selectedCol() {
    if (!this.selection || this.location.length != 3) {
      return null;
    }

    return this.selection;
  }

  get selectedRow() {
    if (!this.selection || this.location.length != 2) {
      return null;
    }

    return this.selection;
  }

  get selectedContainer() {
    if (!this.selection || this.location.length != 1) {
      return null;
    }

    return this.selection;
  }

  get selectionId() {
    return this.location.join('.');
  }

  resetSelection() {
    this.location = [];
    this.selection = null;

    this.configureLayout = null;
  }

  @action
  configLayout() {
    this.resetSelection();

    this.title = `page`;
    this.configureLayout = false;
    this.configureLayout = true;
    this.location = [];
  }

  @action
  async saveLayout() {
    if (this.model.space.layoutContainers?.isDirty) {
      await this.model.space.save();
      this.model.space.layoutContainers.isDirty = false;
    }

    this.model.page.space = this.model.space;
    await this.model.page.save();
  }

  @action
  async cloneLayout() {
    const newPage = await this.model.page.clone();

    this.model.page.rollbackAttributes();

    this.router.transitionTo('manage.pages.layout', newPage.id);
  }

  @action
  onSelect(type, location, selection) {
    this.resetSelection();

    if (!selection) {
      return;
    }

    this.title = `${type} ${selection.name ?? location.join('.')}`;
    this.location = location;
    this.selection = selection;
  }

  @action
  changeColOptions(properties) {
    this.selectedCol.options = properties.options;
    this.selectedCol.componentCount = properties.componentCount;

    if (this.selectedCol.name != properties.name) {
      this.model.page.renameCol(this.selectedCol.name, properties.name);
    }

    const container = this.model.page.getContainerFor(this.selectedCol.name);

    if (container?.gid) {
      this.model.space.layoutContainers.setContainer(container.gid, container);
    }
  }

  @action
  changeRowOptions(options) {
    this.selectedRow.options = options;

    const container = this.model.page.getContainerFor(this.selectedRow.name);

    if (container?.gid) {
      this.model.space.layoutContainers.setContainer(container.gid, container);
    }
  }

  @action
  changeContainer(newValue) {
    this.selectedContainer.options = newValue.options;
    this.selectedContainer.visibility = newValue.visibility;
    this.selectedContainer.layers = newValue.layers;
    this.selectedContainer.name = newValue.name;
    this.selectedContainer.height = newValue.height;

    if (this.selectedContainer.gid != newValue.gid) {
      this.selectedContainer.gid = newValue.gid;
      this.selectedContainer.rows = newValue.rows;

      this.hasVisibleContainers = false;

      later(() => {
        this.hasVisibleContainers = true;
      });
    }

    if (this.selectedContainer.gid) {
      this.model.space.layoutContainers.setContainer(this.selectedContainer.gid, this.selectedContainer);
    }
  }

  @action
  changeGid(gid) {
    this.selectedContainer.gid = gid;
    this.model.space.layoutContainers.setContainer(gid, this.selectedContainer);
  }

  @action
  async revertPage(revisionId) {
    this.model.page = await this.model.page.loadRevision(revisionId);
  }
}
