/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageMapsEditRoute extends AuthenticatedRoute {
  @service('loading') loadingService;

  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return hash({
        map: this.store.findRecord('map', params.id),
      });
    }

    this.modelsToClear = ['map'];

    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    const map = await this.store.findRecord('map', params.id);
    await map.get('baseMaps')?.fetch();
    await map.get('iconSets')?.fetch();

    const defaultIconSets = this.store.query('icon-set', { default: true });
    const defautBaseMaps = this.store.query('base-map', { default: true });

    return hash({
      baseMaps: this.store.query('base-map', {
        usableByTeam: map.visibility.teamId,
      }),
      iconSets: this.store.query('icon-set', {
        usableByTeam: map.visibility.teamId,
      }),
      team: map.visibility.fetchTeam(),
      teams: this.store.query('team', teamsParams),
      indexedMaps: this.store.query('map', { isIndex: true }),
      map,
      defaultIconSets,
      defautBaseMaps,
    });
  }

  async afterModel(model) {
    await model.map?.visibility?.fetchTeam();
  }

  @action
  loading(transition) {
    return !transition.from || transition.from.name != transition.to.name;
  }
}
