/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, fillIn, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | new-record-form/space', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let server;
  let spaceData;

  hooks.beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');

    spaceData = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('for a normal user', function (hooks) {
    it('renders an alert when there is no team', async function () {
      await render(hbs`<NewRecordForm::Space />`);

      expect(this.element.querySelector('.alert-danger .btn-add-team')).to.exist;
      expect(this.element.querySelector('.row-team')).not.to.exist;
    });

    it('does not render an alert when there is a team and it is added by default', async function () {
      const teamData = server.testData.storage.addDefaultTeam();
      const team = await this.store.findRecord('team', teamData._id);

      this.set('teams', [team]);
      this.set('space', this.store.createRecord('space', {}));

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<NewRecordForm::Space @teams={{this.teams}} @space={{this.space}} @all={{false}} @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await fillIn('.input-name', 'new space name');
      await fillIn('.input-space-subdomain', 'test');

      await click('.btn-submit');

      expect(value.name).to.equal('new space name');
      expect(value.domain).to.equal('test.giscollective.com');
      expect(value.slug).to.equal('test');
      expect(value.visibility).to.deep.equal({
        team,
        isPublic: false,
        isDefault: false,
      });
    });

    it('renders the team section when there are 2 teams', async function () {
      const teamData1 = server.testData.storage.addDefaultTeam();
      const teamData2 = server.testData.storage.addDefaultTeam('2');
      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      this.set('teams', [team1, team2]);
      this.set('space', this.store.createRecord('space', {}));

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<NewRecordForm::Space @teams={{this.teams}} @space={{this.space}} @all={{false}} @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await fillIn('.input-name', 'space name');
      await fillIn('.input-space-subdomain', 'other');

      this.element.querySelector('.row-team select').value = '2';
      await triggerEvent('.row-team select', 'change');

      await click('.btn-submit');

      expect(value.name).to.equal('space name');
      expect(value.template).to.be.undefined;
      expect(value.domain).to.equal('other.giscollective.com');
      expect(value.visibility.team.id).to.equal('2');
    });

    it('can submit a space with a template without base models', async function () {
      const teamData1 = server.testData.storage.addDefaultTeam();
      const teamData2 = server.testData.storage.addDefaultTeam('2');
      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      this.set('teams', [team1, team2]);
      this.set('space', this.store.createRecord('space', {}));

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<NewRecordForm::Space @teams={{this.teams}} @space={{this.space}} @all={{false}} @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await fillIn('.input-name', 'space name');
      await fillIn('.input-space-subdomain', 'other');

      this.element.querySelector('.row-team select').value = '2';
      await triggerEvent('.row-team select', 'change');

      await click('.space-container .btn-select');

      await click('.btn-submit');

      expect(value.name).to.equal('space name');
      expect(value.template).to.equal('6227c131624b2cf1626dd029');
      expect(value.domain).to.equal('other.giscollective.com');
      expect(value.visibility.team.id).to.equal('2');
    });

    it('can submit a space with a template with base models', async function () {
      const teamData1 = server.testData.storage.addDefaultTeam();
      const teamData2 = server.testData.storage.addDefaultTeam('2');

      const map = server.testData.storage.addDefaultMap();
      const calendar = server.testData.storage.addDefaultCalendar();

      spaceData.defaultModels = {
        map: '1',
        calendar: '2',
      };

      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      this.set('teams', [team1, team2]);
      this.set('space', this.store.createRecord('space', {}));

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<NewRecordForm::Space @teams={{this.teams}} @space={{this.space}} @all={{false}} @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await fillIn('.input-name', 'space name');
      await fillIn('.input-space-subdomain', 'other');

      this.element.querySelector('.row-team select').value = '2';
      await triggerEvent('.row-team select', 'change');

      await click('.space-container .btn-select');

      const selects = [...this.element.querySelectorAll('.row-records select')];

      selects[0].value = map._id;
      selects[1].value = calendar._id;
      await triggerEvent(selects[0], 'change');
      await triggerEvent(selects[1], 'change');

      await click('.btn-submit');

      expect(value.name).to.equal('space name');
      expect(value.template).to.equal('6227c131624b2cf1626dd029');
      expect(value.domain).to.equal('other.giscollective.com');
      expect(value.visibility.team.id).to.equal('2');
      expect(value.defaultModels.toJSON()).to.deep.equal({
        map: '5ca89e37ef1f7e010007f54c',
        campaign: '',
        calendar: '5cc8dc1038e882010061221',
        newsletter: '',
      });
    });

    it('can select a team after the template was selected', async function () {
      const teamData1 = server.testData.storage.addDefaultTeam();
      const teamData2 = server.testData.storage.addDefaultTeam('2');

      server.testData.storage.addDefaultMap();
      server.testData.storage.addDefaultCalendar();

      spaceData.defaultModels = {
        map: '1',
        calendar: '2',
      };

      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      this.set('teams', [team1, team2]);
      this.set('space', this.store.createRecord('space', {}));

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<NewRecordForm::Space @teams={{this.teams}} @space={{this.space}} @all={{false}} @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await click('.space-container .btn-select');

      server.history = [];

      this.element.querySelector('.row-team select').value = '2';
      await triggerEvent('.row-team select', 'change');

      expect(server.history).to.contain('GET /mock-server/maps?team=2');
      expect(server.history).to.contain('GET /mock-server/calendars?team=2');
    });
  });

  describe('for admin user', function (hooks) {
    hooks.beforeEach(function () {
      let service = this.owner.lookup('service:user');
      service.userData = { isAdmin: true };
    });

    it('renders the btn-show-all-teams', async function () {
      const teamData1 = server.testData.storage.addDefaultTeam();
      const teamData2 = server.testData.storage.addDefaultTeam('2');
      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      this.set('teams', [team1, team2]);
      this.set('space', this.store.createRecord('space', {}));

      await render(
        hbs`<NewRecordForm::Space @teams={{this.teams}} @space={{this.space}} @all={{false}} @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      expect(this.element.querySelector('.btn-show-all-teams')).to.exist;
    });
  });
});
