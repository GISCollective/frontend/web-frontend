/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';

export default class InputListComponent extends Component {
  @tracked value;

  @action
  setup() {
    this.value = A(this.args.value);
  }

  @action
  remove(index) {
    this.value.splice(index, 1);

    this.args.onChange(this.value);
  }

  @action
  async add() {
    this.value.push(await this.args.factoryValue?.());

    this.args.onChange?.(this.value);
  }

  @action
  change(index, value) {
    this.value.splice(index, 1, value);

    this.args.onChange?.(this.value);
  }
}
