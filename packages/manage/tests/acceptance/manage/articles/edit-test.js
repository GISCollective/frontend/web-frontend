/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, waitUntil, click, fillIn, waitFor, triggerEvent } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import { selectSearch } from 'ember-power-select/test-support';
import { EmbeddedMapEditor } from 'manage/lib/embedded-map-editor';

describe('Acceptance | manage/articles/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let article;
  let receivedPicture;
  let defaultPicture;
  let editorJsService;
  let team;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    team = server.testData.storage.addDefaultTeam();
    article = server.testData.storage.addDefaultArticle();
    let picture = server.testData.create.picture();

    article.categories = ['Blog'];
    article.pictures = [picture._id];

    server.testData.storage.addArticle(article);
    server.testData.storage.addDefaultPicture();

    defaultPicture = server.testData.create.picture('1');
    defaultPicture._id = '1';

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/pictures/:id`, (request) => {
      const picture = JSON.parse(request.requestBody);
      picture._id = request.params.id;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture,
        }),
      ];
    });

    server.get(`/mock-server/articles/_/categories`, () => {
      return [200, { 'Content-Type': 'application/json' }, '{ "categories": ["category 1", "category 2"] }'];
    });

    editorJsService = this.owner.lookup('service:editorJs');
  });

  it('redirects to the login page when the user is not authenticated', async function () {
    invalidateSession();
    await visit(`/manage/articles/${article._id}`);
    expect(currentURL()).to.equal(`/login?redirect=%2Fmanage%2Farticles%2F${article._id}`);
  });

  describe('when the user is authenticated', function (hooks) {
    let receivedArticle;

    hooks.beforeEach(async function () {
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultTeam();
      authenticateSession();

      receivedArticle = null;
      server.put(`/mock-server/articles/${article._id}`, (request) => {
        receivedArticle = JSON.parse(request.requestBody);
        receivedArticle.article._id = article._id;

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedArticle)];
      });
    });

    describe('for a regular article', function (hooks) {
      hooks.beforeEach(async function () {
        await visit(`/manage/articles/${article._id}`);
      });

      it('shows the breadcrumbs', async function () {
        expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Articles', 'title', 'Edit']);
        expect(PageElements.breadcrumbLinks()).to.deep.equal([
          '/manage/dashboards',
          '/manage/dashboards/5ca78e2160780601008f69e6',
          '/manage/dashboards/articles/5ca78e2160780601008f69e6',
        ]);
      });

      it('can visit /manage/articles/:id', async function () {
        expect(this.element.querySelector('.article-viewer h1').textContent.trim()).to.equal('title');
        expect(this.element.querySelector('.article-viewer p').textContent.trim()).to.equal('some content');
      });

      it('can update the title and a paragraph', async function () {
        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
          timeout: 4000,
        });

        await click('.ce-header');
        await fillIn('.ce-header', ' --- new title');

        await click('.ce-paragraph');
        await fillIn('.ce-paragraph', ' --- new paragraph');

        await click('.ce-header');

        await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });

        await click('.btn-submit');
        await waitUntil(() => receivedArticle, { timeout: 10000 });

        expect(receivedArticle.article.content.blocks).to.deep.equal([
          { type: 'header', data: { text: ' --- new title', level: 1 } },
          { type: 'paragraph', data: { text: ' --- new paragraph' } },
        ]);
      });

      it('can add a picture in the article', async function (a) {
        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
          timeout: 4000,
        });

        await PageElements.waitEditorJs(this.element);

        await click('.ce-paragraph');
        await click('.ce-toolbar__plus');
        await click(".ce-popover-item[data-item-name='image']");

        await waitUntil(() => document.querySelector("body > [type='file']"));

        const blob = server.testData.create.pngBlob();
        await triggerEvent(document.querySelector("body > [type='file']"), 'change', { files: [blob] });

        await waitUntil(() => receivedPicture);

        a.deepEqual(receivedPicture.picture.meta, {
          renderMode: '',
          attributions: '',
          link: {
            model: 'Article',
            modelId: '5ca78e2160780601008f69e6',
          },
          data: {},
          disableOptimization: false,
        });
      });

      it('can add a video in the article', async function (a) {
        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
          timeout: 4000,
        });

        await PageElements.waitEditorJs(this.element);

        await click('.ce-paragraph');
        await click('.ce-toolbar__plus');

        expect(this.element.querySelector(".ce-popover-item[data-item-name='embedded-video']")).to.exist;
      });

      it('can update the categories', async function () {
        await click('.row-categories .btn-edit');
        await click('.row-categories .btn-add');

        await selectSearch('.row-categories .text-value-1', 'new name');

        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.categories).to.deep.equal(['Blog', 'new name']);
      });

      it('can update the cover', async function () {
        await click('.row-cover .btn-edit');

        const blob = server.testData.create.pngBlob();

        await triggerEvent(".row-cover input[type='file']", 'change', {
          files: [blob],
        });

        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.cover).to.deep.equal('1');
        expect(receivedPicture.picture.meta).to.deep.equal({
          renderMode: '',
          attributions: '',
          link: { model: 'Article', id: '5ca78e2160780601008f69e6' },
          data: {},
          disableOptimization: false,
        });
      });

      it('can add a picture to the gallery', async function () {
        const blob = server.testData.create.pngBlob();

        await click('.row-gallery .btn-edit');

        await triggerEvent(".row-gallery input[type='file']", 'change', {
          files: [blob],
        });

        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.pictures).to.deep.equal(['5cc8dc1038e882010061545a', '1']);
        expect(receivedPicture.picture.meta).to.deep.equal({
          renderMode: '',
          attributions: '',
          link: { model: 'Article', modelId: '5ca78e2160780601008f69e6' },
          data: {},
          disableOptimization: false,
        });
      });

      it('can update the slug', async function () {
        await click('.row-slug .btn-edit');
        await fillIn('.row-slug input', 'new-name');
        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.slug).to.equal('new-name');
      });

      it('can update the order', async function () {
        await click('.row-order .btn-edit');
        await fillIn('.row-order input', '3');
        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.order).to.equal(3);
      });

      it('resets the value when the cancel button is pressed', async function () {
        await click('.row-article .btn-edit');

        await wait(200);

        await click('.ce-header');
        await fillIn('.ce-header', ' --- new title');

        await wait(200);

        await click('.ce-paragraph');
        await fillIn('.ce-paragraph', ' --- new paragraph');

        await wait(200);

        await click('.ce-header');

        await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });

        await click('.btn-cancel');

        const paragraphs = [...this.element.querySelectorAll('.article-viewer p')];

        expect(receivedArticle).to.equal(null);
        expect(this.element.querySelector('.article-viewer h1').textContent.trim()).to.equal('title');
        expect(paragraphs[paragraphs.length - 1].textContent.trim()).to.equal('some content');
      });

      it('should be able to unpublish the article', async function () {
        const openBtn = this.element.querySelector('.side-bar .btn-open');
        if (openBtn) {
          await click(openBtn);
        }

        await click(this.element.querySelector('.side-bar .container-group-is-public .btn-switch'));

        await waitUntil(() => receivedArticle != null, { timeout: 3000 });

        expect(receivedArticle.article.visibility.isPublic).to.equal(false);
      });

      it('does not show the isDefault section', async function () {
        expect(this.element.querySelector('.container-group-is-default')).not.to.exist;
      });

      it('can change the release date', async function () {
        await visit(`/manage/articles/${article._id}`);

        await click('.row-release-date .btn-edit');

        await fillIn('.datepicker-input', '2025-05-20');

        this.element.querySelector('.form-select.hour').value = '12';
        await triggerEvent('.form-select.hour', 'change');

        this.element.querySelector('.form-select.minute').value = '30';
        await triggerEvent('.form-select.minute', 'change');

        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.releaseDate).to.equal('2025-05-20T10:30:00.000Z');
      });

      it('does not render the release subject', async function () {
        expect(this.element.querySelector('.container-group-subject')).not.to.exist;
      });

      it('can add a map', async function () {
        editorJsService.tools['map/map-view'] = {
          class: EmbeddedMapEditor,
        };

        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-js.editor-is-ready')?.editorJs);

        const editorJs = this.element.querySelector('.editor-js.editor-is-ready').editorJs;
        editorJs.blocks.insert('map/map-view', { source: { id: 'old-map-id' }, height: '321' }, {}, 0, true);

        await waitFor('.map-editor-id');
        await waitFor('.map-editor-height');

        expect(this.element.querySelector('.map-editor-id').value).to.equal('old-map-id');
        expect(this.element.querySelector('.map-editor-height').value).to.equal('321');

        const e = new Event('change');

        this.element.querySelector('.map-editor-id').value = 'some-map-id';
        this.element.querySelector('.map-editor-id').dispatchEvent(e);

        this.element.querySelector('.map-editor-height').value = '123';
        this.element.querySelector('.map-editor-height').dispatchEvent(e);

        await waitUntil(() => !this.element.querySelector('.btn-submit  ').hasAttribute('disabled'), { timeout: 3000 });

        await click('.btn-submit');
        await waitUntil(() => receivedArticle, { timeout: 10000 });

        expect(receivedArticle.article.content.blocks).to.deep.equal([
          {
            type: 'map/map-view',
            data: {
              map: {
                mode: 'use a map',
                flyOverAnimation: false,
                featureHover: 'mouse pointer',
                featureSelection: 'open details in panel',
                enableMapInteraction: 'automatic',
                enablePinning: false,
                restrictView: true,
                showUserLocation: false,
                showZoomButtons: false,
                showBaseMapSelection: false,
              },
              source: { useSelectedModel: false, model: 'map', id: 'some-map-id' },
              sizing: 'fill',
              height: '123',
            },
          },
          { type: 'header', data: { text: 'title', level: 1 } },
          { type: 'paragraph', data: { text: 'some content' } },
        ]);
      });

      it('can delete a picture', async function () {
        await click('.container-group-photo-gallery .btn-edit');

        await click('.btn-delete');
        await click('.container-group-photo-gallery .btn-submit');

        expect(receivedArticle.article.pictures).to.deep.equal([]);
      });

      it('does not have the events section', async function (a) {
        team.allowEvents = true;
        server.testData.storage.addTeam(team);

        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-events')).not.to.exist;
      });
    });

    describe('for a notification article', function (hooks) {
      hooks.beforeEach(async function () {
        article.slug = 'notification-campaign-add-632dd512eadb0601002443b0';
      });

      describe('for a regular user', function (hooks) {
        hooks.beforeEach(async function () {
          await visit(`/manage/articles/${article._id}`);
        });

        it('does not show the slug field', async function () {
          expect(this.element.querySelector('.row-slug')).not.to.exist;
        });

        it('does not show the visibility section', async function () {
          expect(this.element.querySelector('.container-group-team')).not.to.exist;
        });

        it('can add a picture in the article', async function (a) {
          await click('.row-article .btn-edit');

          await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
            timeout: 4000,
          });

          await PageElements.waitEditorJs(this.element);

          await click('.ce-paragraph');
          await click('.ce-toolbar__plus');
          await click(".ce-popover-item[data-item-name='image']");

          await waitUntil(() => document.querySelector("body > [type='file']"));

          const blob = server.testData.create.pngBlob();
          await triggerEvent(document.querySelector("body > [type='file']"), 'change', { files: [blob] });

          await waitUntil(() => receivedPicture);

          a.deepEqual(receivedPicture.picture.meta, {
            renderMode: '',
            attributions: '',
            link: { model: 'Article', modelId: '5ca78e2160780601008f69e6' },
            data: {},
            disableOptimization: false,
          });
        });
      });

      it('shows the slug field to an admin user', async function () {
        server.testData.storage.addDefaultUser(true);
        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-slug')).to.exist;
      });

      it('does not have the events section', async function (a) {
        team.allowEvents = true;
        server.testData.storage.addTeam(team);

        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-events')).not.to.exist;
      });

      it('can not add a video in the article', async function (a) {
        await visit(`/manage/articles/${article._id}`);

        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
          timeout: 4000,
        });

        await PageElements.waitEditorJs(this.element);

        await click('.ce-paragraph');
        await click('.ce-toolbar__plus');

        expect(this.element.querySelector(".ce-popover-item[data-item-name='embeddedVideo']")).not.to.exist;
      });
    });

    describe('for a newsletter welcome message article', function (hooks) {
      hooks.beforeEach(async function () {
        let newsletter = server.testData.storage.addDefaultNewsletter();

        article.slug = '';
        article.title = 'newsletter welcome article';
        article.type = 'newsletter-welcome-message';
        article.relatedId = newsletter._id;
        article.releaseDate = '2022-02-03T02:33:22Z';
        article.status = 'pending';
        article.canEdit = true;
      });

      it('hides the slug field', async function () {
        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-slug')).not.to.exist;
      });

      it('hides the categories field', async function () {
        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-categories')).not.to.exist;
      });

      it('hides the release date field', async function () {
        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-release-date')).not.to.exist;
      });

      it('hides the cover field', async function () {
        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-cover')).not.to.exist;
      });

      it('hides the gallery field', async function () {
        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-gallery')).not.to.exist;
      });

      it('does not have the events section', async function (a) {
        team.allowEvents = true;
        server.testData.storage.addTeam(team);

        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-events')).not.to.exist;
      });

      it('can not add a video in the article', async function (a) {
        await visit(`/manage/articles/${article._id}`);

        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
          timeout: 4000,
        });

        await PageElements.waitEditorJs(this.element);

        await click('.ce-paragraph');
        await click('.ce-toolbar__plus');

        expect(this.element.querySelector(".ce-popover-item[data-item-name='embeddedVideo']")).not.to.exist;
      });
    });

    describe('for a newsletter article', function (hooks) {
      let event;

      hooks.beforeEach(async function () {
        event = server.testData.storage.addDefaultEvent();
        let newsletter = server.testData.storage.addDefaultNewsletter();

        article.slug = '';
        article.title = 'newsletter article';
        article.type = 'newsletter-article';
        article.relatedId = newsletter._id;
        article.releaseDate = '2022-02-03T02:33:22Z';
        article.status = 'pending';
        article.canEdit = true;
      });

      it('shows the slug field', async function () {
        await visit(`/manage/articles/${article._id}`);

        expect(this.element.querySelector('.row-slug')).to.exist;
      });

      it('can change the subject', async function () {
        await visit(`/manage/articles/${article._id}`);

        await click('.row-subject .btn-edit');
        await PageElements.waitEditorJs(this.element);
        await fillIn('.row-subject .form-control', 'message subject');
        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(this.element.querySelector('.row-subject p').textContent.trim()).to.equal('message subject');

        expect(receivedArticle).to.deep.equal({
          article: {
            _id: '5ca78e2160780601008f69e6',
            title: 'newsletter article',
            content: '# title\nsome content',
            slug: '',
            subject: 'message subject',
            type: 'newsletter-article',
            relatedId: '5ca78e2160780601008f69ff',
            status: 'pending',
            categories: ['Blog'],
            cover: null,
            order: 999,
            pictures: ['5cc8dc1038e882010061545a'],
            releaseDate: '2022-02-03T02:33:22.000Z',
            info: {
              createdOn: '0001-01-01T00:00:00.000Z',
              lastChangeOn: '2020-02-02T19:31:07.000Z',
              author: '5b8a59caef739394031a3f67',
              changeIndex: 2,
            },
            visibility: {
              isPublic: true,
              isDefault: false,
              team: '5ca78e2160780601008f69e6',
            },
          },
        });
      });

      it('can change the release date', async function () {
        await visit(`/manage/articles/${article._id}`);

        await click('.row-release-date .btn-edit');

        await fillIn('.datepicker-input', '2025-04-20');

        this.element.querySelector('.form-select.hour').value = '12';
        await triggerEvent('.form-select.hour', 'change');

        this.element.querySelector('.form-select.minute').value = '30';
        await triggerEvent('.form-select.minute', 'change');

        await click('.btn-submit');

        await waitUntil(() => receivedArticle);

        expect(receivedArticle.article.releaseDate).to.deep.equal('2025-04-20T10:30:00.000Z');
      });

      it('shows the breadcrumbs', async function () {
        await visit(`/manage/articles/${article._id}`);

        expect(PageElements.breadcrumbs()).to.deep.equal([
          'Dashboard',
          'Open Green Map',
          'my first newsletter',
          'newsletter article',
          'Edit',
        ]);
        expect(PageElements.breadcrumbLinks()).to.deep.equal([
          '/manage/dashboards',
          '/manage/dashboards/5ca78e2160780601008f69e6',
          '/manage/dashboards/newsletter/5ca78e2160780601008f69ff',
        ]);
      });

      it('can add a picture in the article', async function (a) {
        await visit(`/manage/articles/${article._id}`);

        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
          timeout: 4000,
        });

        await PageElements.waitEditorJs(this.element);

        await click('.ce-paragraph');
        await click('.ce-toolbar__plus');
        await click(".ce-popover-item[data-item-name='image']");

        await waitUntil(() => document.querySelector("body > [type='file']"));

        const blob = server.testData.create.pngBlob();
        await triggerEvent(document.querySelector("body > [type='file']"), 'change', { files: [blob] });

        await waitUntil(() => receivedPicture);

        a.deepEqual(receivedPicture.picture.meta, {
          renderMode: '',
          attributions: '',
          link: {
            model: 'Article',
            modelId: '5ca78e2160780601008f69e6',
          },
          data: {},
          disableOptimization: true,
        });
      });

      it('can not add a video in the article', async function (a) {
        await visit(`/manage/articles/${article._id}`);

        await click('.row-article .btn-edit');

        await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
          timeout: 4000,
        });

        await PageElements.waitEditorJs(this.element);

        await click('.ce-paragraph');
        await click('.ce-toolbar__plus');

        expect(this.element.querySelector(".ce-popover-item[data-item-name='embeddedVideo']")).not.to.exist;
      });

      describe('the events section', function () {
        it('is not visible when allowEvents is false', async function (a) {
          team.allowEvents = false;
          server.testData.storage.addTeam(team);

          await visit(`/manage/articles/${article._id}`);

          expect(this.element.querySelector('.row-events')).not.to.exist;
        });

        it('is visible', async function (a) {
          team.allowEvents = true;
          article.events = [event._id];
          server.testData.storage.addTeam(team);

          await visit(`/manage/articles/${article._id}`);

          expect(this.element.querySelector('.row-events')).to.exist;
          let items = [...this.element.querySelectorAll('.row-events li')].map((a) => a.textContent.trim());

          expect(items).to.deep.equal(items);
        });

        it('can be updated', async function (a) {
          team.allowEvents = true;
          article.events = [event._id];
          server.testData.storage.addTeam(team);

          await visit(`/manage/articles/${article._id}`);

          await click('.row-events .btn-edit');
          await click('.row-events .btn-delete');
          await click('.row-events .btn-submit');

          await waitUntil(() => receivedArticle);
          expect(receivedArticle.article.events).to.deep.equal([]);
        });
      });
    });
  });
});
