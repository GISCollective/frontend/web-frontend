import setupDeprecationWorkflow from 'ember-cli-deprecation-workflow';

setupDeprecationWorkflow({
  workflow: [
    { handler: 'throw', matchId: 'ember-data:deprecate-promise-proxies' },
    { handler: 'throw', matchId: 'ember-data:deprecate-array-like' },
    {
      handler: 'throw',
      matchId: 'ember-data:deprecate-promise-many-array-behaviors',
    },
  ],
});
