/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class InputSizePxComponent extends Component {
  elementId = `input-size-unit-${guidFor(this)}`;

  @tracked _value;

  get strUnit() {
    if (this.args.unit == "%") {
      return "percentage";
    }

    return this.args.unit;
  }

  get value() {
    if (this._value !== undefined) {
      return this._value;
    }

    if (this.args.value) {
      return parseFloat(this.args.value);
    }

    return "";
  }

  triggerChange(value) {
    this.args.onChange?.(`${value}${this.args.unit}`);
  }

  @action
  change(newValue) {
    this._value = parseFloat(newValue);

    this.triggerChange(this._value);
  }

  @action
  setup() {
    if (this.args.value && this.args.value.indexOf(this.args.unit) == -1) {
      this.triggerChange(this.value);
    }
  }
}
