/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { htmlSafe } from "@ember/template";
import { later, cancel } from "@ember/runloop";
import { dasherize } from "@ember/string";

export class HighlightedElement {
  @tracked _element;
  @tracked rootElement;
  @tracked factor;
  @tracked selectionType;

  @tracked parent;
  @tracked child;

  @tracked titleWidth = 0;
  @tracked xTitle = 0;
  @tracked frame;

  resizeObserver = null;

  constructor(rootElement, element, factor = 1, selectionType = "") {
    this.rootElement = rootElement;
    this.element = element;
    this.factor = factor;
    this.selectionType = selectionType;

    this.frame = element.getBoundingClientRect();

    this.resizeObserver = new ResizeObserver(() => {
      this.frame = element.getBoundingClientRect();
    });

    this.resizeObserver.observe(element);
  }

  willDestroy() {
    return this.resizeObserver.disconnect();
  }

  get element() {
    const originalElementFound = this.rootElement.contains(this._element);

    if (!originalElementFound) {
      const path = this._element?.attributes?.getNamedItem?.("data-path")?.value;
      return this.rootElement.querySelector(`[data-path='${path}']`);
    }

    return this._element;
  }

  set element(value) {
    this._element = value;
  }

  get isHighlight() {
    return true;
  }

  get isPrimary() {
    return this.selectionType == "primary";
  }

  set isPrimary(value) {
    if (this.selectionType === "selection") {
      return;
    }

    this.selectionType = value ? "primary" : "";

    if (value) {
      for (const item of [...this.allParents, ...this.allChildren]) {
        item.selectionType = "";
      }
    }
  }

  get allParents() {
    const parents = [];
    let tmp = this.parent;

    while (tmp) {
      parents.push(tmp);
      tmp = tmp.parent;
    }

    return parents;
  }

  get allChildren() {
    const children = [];
    let tmp = this.child;

    while (tmp) {
      children.push(tmp);
      tmp = tmp.child;
    }

    return children;
  }

  get showName() {
    if (!this.child) {
      return true;
    }

    return this.child.name != this.name;
  }

  get xTitleEnd() {
    const distance = this.parent?.showName ? 3 : 1;
    return this.xTitle + this.titleWidth + distance;
  }

  get normalizedName() {
    return `${dasherize(this.name || "unknown").replace(/[\W_]+/g, "-")}-${dasherize(this.type || "unknown")}`;
  }

  hasTitleOnTheSameLine(other) {
    return Math.abs(this.top - other.top) < 9;
  }

  overlapsTitle(other) {
    if (!this.hasTitleOnTheSameLine(other)) {
      return false;
    }

    const endsBeforeOther = this.xTitleEnd <= other.xTitle;
    const startsAfterOther = this.xTitle >= other.xTitleEnd;

    if (endsBeforeOther && !startsAfterOther) {
      return false;
    }

    if (startsAfterOther && !endsBeforeOther) {
      return false;
    }

    return true;
  }

  updateXTitle() {
    this.xTitle = this.left;
    if (this.isPrimary) {
      return this.parent?.updateXTitle();
    }

    const allChildren = this.allChildren;
    const overlaps = allChildren.find((a) => this.overlapsTitle(a));

    if (!overlaps || !this.allChildren.length) {
      this.xTitle = this.left;
      return this.parent?.updateXTitle();
    }

    const endX = allChildren
      .filter((a) => this.hasTitleOnTheSameLine(a))
      .map((a) => a.xTitleEnd)
      .sort()
      .reverse();

    for (const value of endX) {
      this.xTitle = value;

      const overlaps = allChildren.find((a) => this.overlapsTitle(a));

      if (!overlaps) {
        break;
      }
    }

    this.parent?.updateXTitle();
  }

  get titleStyle() {
    return htmlSafe(`left: ${this.xTitle * this.factor}px; top: ${(this.top - 18) * this.factor}px`);
  }

  get isHidden() {
    return this.frame.width == 0 || this.frame.height == 0;
  }

  get top() {
    if (this.isHidden) {
      return this.parent?.top;
    }

    const rootFrame = this.rootElement.getBoundingClientRect();

    return this.frame.top - rootFrame.top;
  }

  get left() {
    if (this.isHidden) {
      return this.parent?.left;
    }

    const rootFrame = this.rootElement.getBoundingClientRect();

    return this.frame.left - rootFrame.left;
  }

  get style() {
    return htmlSafe(
      `width: ${this.frame.width * this.factor}px; height: ${this.frame.height * this.factor}px; left: ${
        this.left * this.factor
      }px; top: ${this.top * this.factor}px;`,
    );
  }

  get name() {
    return this.element.attributes.getNamedItem("data-name").value;
  }

  get path() {
    return this._element.attributes.getNamedItem("data-path")?.value;
  }

  get type() {
    if (!this.element.hasAttribute("data-type")) {
      return "";
    }

    return this.element.attributes.getNamedItem("data-type").value;
  }

  get gid() {
    if (!this._element.hasAttribute("data-gid")) {
      return "";
    }

    return this._element.attributes.getNamedItem("data-gid").value;
  }

  get editor() {
    if (!this.element.hasAttribute("data-editor")) {
      return "";
    }

    return this.element.attributes.getNamedItem("data-editor").value;
  }

  get title() {
    return `${this.name} (${this.type})`;
  }

  toJSON() {
    const result = {};

    const keys = ["name", "type", "gid", "path", "editor"];

    for (const key of keys) {
      if (this[key]) {
        result[key] = this[key];
      }
    }

    return result;
  }
}

export default class HighlightComponent extends Component {
  @tracked rootElement;
  @tracked list = [];

  @action
  setup(element) {
    this.rootElement = element;

    this.setupSelection();
  }

  @action
  setupSelection() {
    if (!this.rootElement) {
      return true;
    }

    if (!this.args.selection) {
      this.list = [];
      return;
    }

    const attributes = {};
    const fields = ["name", "type", "path", "gid"];

    for (const field of fields) {
      if (this.args.selection[field]) {
        attributes[field] = this.args.selection[field];
      }
    }

    const selector = Object.keys(attributes)
      .map((a) => `[data-${a}="${attributes[a]}"]`)
      .join("");
    const element = this.rootElement.querySelector(selector);

    this.list = [new HighlightedElement(this.rootElement, element, this.args.factor, "selection")];
    this.list[0].updateXTitle();
  }

  hiddenChildren(element) {
    let result = [];

    for (const child of element.children) {
      const frame = child.getBoundingClientRect();
      const isHidden = frame.width == 0 || frame.height == 0;

      if (child.hasAttribute("data-name") && isHidden) {
        result.push(child);
      }
    }

    return result;
  }

  @action
  updateFrames(ev) {
    if (this.args.selection) {
      return;
    }

    let currentNode = ev.target;
    cancel(this.removeTimer);

    if (
      currentNode.parentElement?.classList.contains("highlight-title") ||
      currentNode.classList.contains("highlight-title")
    ) {
      return;
    }

    if (ev.altKey) {
      return;
    }

    let nodes = [];

    while (currentNode != this.rootElement) {
      nodes.push(...this.hiddenChildren(currentNode));
      nodes.push(currentNode);
      currentNode = currentNode.parentElement;
    }

    nodes = nodes.filter((a) => a.hasAttribute("data-name")).reverse();
    this.list = nodes.map((a) => new HighlightedElement(this.rootElement, a, this.args.factor));

    if (!this.list.length) {
      return;
    }

    const lastElement = this.list[this.list.length - 1];

    if (!lastElement) {
      return;
    }

    lastElement.isPrimary = true;

    for (let i = 1; i < this.list.length; i++) {
      this.list[i].parent = this.list[i - 1];
    }

    for (let i = 0; i < this.list.length - 1; i++) {
      this.list[i].child = this.list[i + 1];
    }

    later(() => {
      lastElement.updateXTitle();
    }, 10);
  }

  @action
  removeFrames(ev) {
    if (this.args.selection) {
      return;
    }

    if (ev.altKey) {
      return;
    }

    this.removeTimer = later(() => {
      this.list = [];
      this.removeTimer = null;
    }, 10);
  }
}
