/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action, get } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ViewBaseSourceFieldComponent extends Component {
  @tracked _value;
  @tracked _fieldName;
  @tracked fieldValue;

  defaultFor(field) {
    if (field == 'date') {
      return 'info.createdOn';
    }

    return field;
  }

  get value() {
    return this._value ?? this.args.value ?? {};
  }

  get fieldName() {
    if (this._fieldName) {
      return this._fieldName;
    }

    let result = this.value.data?.field?.name;

    if (!result) {
      result = this.defaultFor(this.args?.for);
    }

    return result;
  }

  get isEmpty() {
    if (!this.args.isEditor) {
      return false;
    }

    const fieldValue = this.fieldValue;

    if (Array.isArray(fieldValue)) {
      return fieldValue.length == 0;
    }

    return fieldValue === null || fieldValue === undefined;
  }

  @action
  async loaded() {
    const fieldName = this.fieldName;
    const fieldValue = this.args.record?.[this.fieldName];

    if (typeof fieldValue == 'function') {
      this.fieldValue = await fieldValue.call(this.args.record);
    } else {
      this.fieldValue = await fieldValue;
    }

    return this.args.onLoad?.(fieldName, this.fieldValue);
  }

  @action
  updateProps(event) {
    this._fieldName = event.detail.value.name;

    return this.loaded();
  }
}
