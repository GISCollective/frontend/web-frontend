/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, fillIn, triggerEvent, waitUntil, typeIn } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';
import Modal from 'core/test-support/modal';

describe('Acceptance | manage/calendars/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedCalendar;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();

    receivedCalendar = null;

    server.post('/mock-server/calendars', (request) => {
      receivedCalendar = JSON.parse(request.requestBody);
      const item = server.testData.storage.addDefaultCalendar();
      receivedCalendar.calendar._id = item._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedCalendar)];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('redirects to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/calendars/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fcalendars%2Fadd');
  });

  describe('when an user is authenticated', function (hooks) {
    hooks.beforeEach(function () {
      let team = server.testData.storage.addDefaultTeam();
      team.allowEvents = true;

      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();
      await visit('/manage/calendars/add');
      expect(currentURL()).to.equal('/manage/calendars/add');
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit(`/manage/calendars/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'New calendar']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
      ]);
    });

    it('redirects to the edit page after the request response is received', async function () {
      authenticateSession();

      await visit(`/manage/calendars/add`);

      await PageElements.waitEditorJs(this.element);

      await fillIn('.form-control-name', 'new calendar');

      await fillIn('.ce-paragraph', '');
      await typeIn('.ce-paragraph', 'test message');
      await PageElements.wait(510);

      this.element.querySelector('.row-type select').value = 'schedules';
      await triggerEvent('.row-type select', 'change');

      await click('.btn-submit');

      await waitUntil(() => receivedCalendar);
      await waitUntil(() => currentURL() != '/manage/calendars/add');

      expect(currentURL()).to.equal(`/manage/calendars/edit/${receivedCalendar.calendar._id}`);
    });

    it('can submit the form without changing the article', async function (a) {
      authenticateSession();

      await visit(`/manage/calendars/add`);

      await PageElements.waitEditorJs(this.element);

      await fillIn('.form-control-name', 'new calendar');

      this.element.querySelector('.row-type select').value = 'schedules';
      await triggerEvent('.row-type select', 'change');

      await click('.btn-submit');

      await waitUntil(() => receivedCalendar);
      await waitUntil(() => currentURL() != '/manage/calendars/add');

      a.deepEqual(receivedCalendar.calendar, {
        name: 'new calendar',
        article: { blocks: [{ type: 'paragraph', data: { text: 'This is the calendar related to our project.' } }] },
        iconSets: { useCustomList: false, list: [] },
        map: { isEnabled: false, map: '' },
        type: 'schedules',
        attributes: [],
        visibility: { isPublic: false, isDefault: false, team: '5ca78e2160780601008f69e6' },
        info: {},
        cover: null,
        recordName: null,
        _id: '5cc8dc1038e882010061221',
      });
    });
  });
});
