/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import DoubleClickZoom from 'ol/interaction/DoubleClickZoom';
import { action } from '@ember/object';

export default class MapInteractionDoubleClickZoomComponent extends Component {
  @action
  setupOl() {
    if (!this.args.map) {
      return;
    }

    this.interaction = new DoubleClickZoom({
      duration: this.args.duration,
      delta: this.args.delta,
    });

    this.args.map.addInteraction(this.interaction);
    this.currentInteractions = [this.interaction];
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (this.currentInteractions && this.args.map) {
      this.currentInteractions.forEach((item) => {
        this.args.map.removeInteraction(item);
      });
    }
  }
}
