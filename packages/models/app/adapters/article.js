/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class ArticleAdapter extends Adapter {
  get modelName() {
    return 'article';
  }

  sendTestEmail(articleId) {
    const url = this.buildURL('article', articleId) + '/test';
    return this.ajax(url, 'POST');
  }

  categories(teamId, relatedCategory) {
    let url = this.buildURL('article', '_') + '/categories';
    const query = [];

    if (teamId) {
      query.push(`team=${encodeURIComponent(teamId)}`);
    }

    if (relatedCategory) {
      query.push(`category=${encodeURIComponent(relatedCategory)}`);
    }

    if (query.length) {
      url = `${url}?${query.join('&')}`;
    }

    return this.ajax(url, 'GET');
  }

  deleteMany(query) {
    const url = [this.urlForQuery(query, 'article'), this.getParams(query)].filter((a) => a).join('?');
    return this.ajax(url, 'DELETE');
  }
}
