/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, click, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/site-attributes', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Input::SiteAttributes />`);
    expect(this.element.textContent.trim()).to.contain('attributes');
  });

  describe('when there are no attributes', function (hooks) {
    let value;

    hooks.beforeEach(function () {
      this.set('attributes', {});

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to add a new group', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-new-anonymous-group');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        'New Group 1': {},
      });
    });

    it('allows to add a new group with an attribute', async function () {
      await render(hbs`<Input::SiteAttributes
      @editablePanel="attributes"
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      await click('.btn-new-anonymous-group');
      await click('.btn-add-new-attribute');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        'New Group 1': { 'new attribute': '' },
      });
    });

    it('allows to add a new group with an attribute and value', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-new-anonymous-group');
      await click('.btn-add-new-attribute');

      await fillIn('.input-attribute-name', 'b');
      await fillIn('.input-attribute-value', '3');
      await fillIn('.input-group-name', 'group');

      await click('.btn-submit');

      expect(value).to.deep.equal({
        group: { b: '3' },
      });
    });
  });

  describe('when there is an object attribute', function (hooks) {
    let value;

    hooks.beforeEach(function () {
      this.set('attributes', {
        key: { a: 1 },
      });

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to remove an object attribute', async function () {
      await render(hbs`<Input::SiteAttributes
      @editablePanel="attributes"
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.group-attribute .btn-delete');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: {},
      });
    });

    it('allows to rename an attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-name', 'b');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: { b: 1 },
      });
    });

    it('allows to update a value', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-value', 2);
      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: { a: '2' },
      });
    });

    it('allows to update the key', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await fillIn('.input-group-name', 'newKey');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        newKey: { a: 1 },
      });
    });

    it('allows to add a new attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-add-new-attribute');

      const names = this.element.querySelectorAll('.input-attribute-name');
      const values = this.element.querySelectorAll('.input-attribute-value');

      await fillIn(names[1], 'b');
      await fillIn(values[1], '3');

      await click('.btn-submit');

      expect(value).to.deep.equal({ key: { a: 1, b: '3' } });
    });

    it('allows to delete the group', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-delete-group');
      await click('.btn-submit');

      expect(value).to.deep.equal({});
    });
  });

  describe('when there is an object with dots in attributes keys', function (hooks) {
    let value;

    hooks.beforeEach(function () {
      this.set('attributes', {
        'key nr.': { 'a.b': 1 },
      });

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to rename an attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-name', 'b');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        'key nr.': { b: 1 },
      });
    });

    it('allows to update a value', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-value', 2);
      await click('.btn-submit');

      expect(value).to.deep.equal({
        'key nr.': { 'a.b': '2' },
      });
    });

    it('allows to update the key', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-group-name', 'newKey');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        newKey: { 'a.b': 1 },
      });
    });
  });

  describe('when there is an item list', function (hooks) {
    let value;

    hooks.beforeEach(function () {
      value = null;
      this.set('attributes', {
        key: [{ a: 1 }, { a: 2 }],
      });

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to remove an item in list', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.group-attribute .btn-delete');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: [{}, { a: 2 }],
      });
    });

    it('allows to rename an item in list', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-name', 'b');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: [{ b: 1 }, { a: 2 }],
      });
    });

    it('allows to update a value', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-value', 2);
      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: [{ a: '2' }, { a: 2 }],
      });
    });

    it('allows to update the key', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await fillIn('.input-group-name', 'newKey');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        newKey: [{ a: 1 }, { a: 2 }],
      });
    });

    it('allows to add a new attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-add-new-attribute');

      const names = this.element.querySelectorAll('.input-attribute-name');
      const values = this.element.querySelectorAll('.input-attribute-value');

      await fillIn(names[1], 'b');
      await fillIn(values[1], '3');

      await click('.btn-submit');

      expect(value).to.deep.equal({ key: [{ a: 1, b: '3' }, { a: 2 }] });
    });

    it('allows to delete a group', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-delete-group');
      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: [{ a: 2 }],
      });
    });

    it('allows to add a new item', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-add-list-group-item');
      const buttons = this.element.querySelectorAll('.btn-add-new-attribute');
      await click(buttons[2]);

      const names = this.element.querySelectorAll('.input-attribute-name');
      const values = this.element.querySelectorAll('.input-attribute-value');

      await fillIn(names[2], 'b');
      await fillIn(values[2], '3');

      await click('.btn-submit');

      expect(value).to.deep.equal({
        key: [{ a: 1 }, { a: 2 }, { b: '3' }],
      });
    });
  });

  describe('when there is a group matching an icon', function (hooks) {
    hooks.beforeEach(function () {
      let icon = {};
      icon.name = 'icon3';
      icon.localName = 'local icon 3';
      icon.attributes = [
        {
          name: 'attribute3',
          help: 'some help message',
          displayName: 'local attribute3',
          type: 'short text',
        },
      ];

      this.set('icons', [icon]);

      this.set('attributes', {
        icon3: { attribute3: 'value' },
      });

      this.set('save', () => {});
    });

    it('shows the icon name instead of the group input', async function () {
      await render(hbs`<Input::SiteAttributes
      @editablePanel="attributes"
      @icons={{this.icons}}
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      expect(this.element.querySelector('.input-group-name')).not.to.exist;
      expect(this.element.querySelector('.icon-group-name').textContent.trim()).to.equal('icon3');
    });

    it('shows the attribute name instead of the attribute name input', async function () {
      await render(hbs`<Input::SiteAttributes
      @editablePanel="attributes"
      @icons={{this.icons}}
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      expect(this.element.querySelector('.input-attribute-name')).not.to.exist;
      expect(this.element.querySelector('.attribute-display-name').textContent.trim()).to.equal('local attribute3');
    });

    it('can add a group for the icon', async function () {
      this.set('attributes', {});

      await render(hbs`<Input::SiteAttributes
      @editablePanel="attributes"
      @icons={{this.icons}}
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      await click('.btn-add-icon-group');

      expect(this.element.querySelector('.attribute-display-name').textContent.trim()).to.equal('local attribute3');
    });
  });

  describe('when there is a required attribute', function (hooks) {
    hooks.beforeEach(function () {
      let icon = {};
      icon.name = 'icon3';
      icon.localName = 'local icon 3';
      icon.attributes = [
        {
          name: 'attribute3',
          help: 'some help message',
          displayName: 'local attribute3',
          type: 'short text',
          isRequired: true,
        },
      ];

      this.set('icons', [icon]);

      this.set('attributes', {
        icon3: { attribute3: 'value' },
      });

      this.set('save', () => {});
    });

    it('displays an asterisk next to the attribute name', async function () {
      await render(hbs`<Input::SiteAttributes
      @editablePanel="attributes"
      @icons={{this.icons}}
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      expect(this.element.querySelector('.is-required-asterisk')).to.exist;

      await click('.btn-edit');
      expect(this.element.querySelector('.is-required-asterisk')).to.exist;
    });
  });

  describe('when there is an icon with attributes with display names', function (hooks) {
    hooks.beforeEach(function () {
      let icon = {};
      icon.name = 'icon3';
      icon.localName = 'local icon 3';
      icon.attributes = [
        {
          name: 'attribute1',
          help: 'some help message 1',
          displayName: 'local attribute1',
          type: 'short text',
        },
        {
          name: 'attribute2',
          help: 'some help message 2',
          displayName: 'local attribute2',
          type: 'short text',
        },
        {
          name: 'attribute3',
          help: 'some help message 3',
          displayName: 'local attribute3',
          type: 'short text',
        },
      ];

      this.set('icons', [icon]);

      this.set('attributes', {
        icon3: {
          attribute3: 'value3',
          attribute2: 'value2',
          attribute1: 'value1',
          attribute4: 'value4',
        },
      });

      this.set('save', () => {});
    });

    it('shows the attributes in the icon order', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @icons={{this.icons}}
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      const names = this.element.querySelectorAll('.site-attributes-property .attribute-display-name');

      expect(names).to.have.length(3);
      expect(names[0].textContent.trim()).to.equal('local attribute1');
      expect(names[1].textContent.trim()).to.equal('local attribute2');
      expect(names[2].textContent.trim()).to.equal('local attribute3');
      expect(this.element.querySelector('.input-attribute-name').value).to.equal('attribute4');
    });
  });

  describe('when there is an icon with attributes without display names', function (hooks) {
    hooks.beforeEach(function () {
      let icon = {};
      icon.name = 'icon3';
      icon.localName = 'local icon 3';
      icon.attributes = [
        {
          name: 'attribute1',
          help: 'some help message 1',
          displayName: '',
          type: 'short text',
        },
        {
          name: 'attribute2',
          help: 'some help message 2',
          displayName: '',
          type: 'short text',
        },
        {
          name: 'attribute3',
          help: 'some help message 3',
          displayName: '',
          type: 'short text',
        },
      ];

      this.set('icons', [icon]);

      this.set('attributes', {
        icon3: {
          attribute3: 'value3',
          attribute2: 'value2',
          attribute1: 'value1',
          attribute4: 'value4',
        },
      });

      this.set('save', () => {});
    });

    it('shows the display names and names in the icon order', async function () {
      await render(hbs`<Input::SiteAttributes
        @editablePanel="attributes"
        @icons={{this.icons}}
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      const displayNames = [...this.element.querySelectorAll('.site-attributes-property .attribute-display-name')].map(
        (a) => a.textContent.trim(),
      );

      expect(displayNames).to.deep.equal(['attribute1', 'attribute2', 'attribute3']);
    });
  });
});
