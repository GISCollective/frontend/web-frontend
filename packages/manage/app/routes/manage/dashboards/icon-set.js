/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { addIconGroupToStore } from '../../../lib/icons';

class IconSetModelData {
  @tracked iconSet;
  @tracked team;
  @tracked iconTree;

  constructor(params, store) {
    this.params = params;
    this.store = store;
  }

  @action
  async reload() {
    this.iconSet = await this.store.findRecord('icon-set', this.params.id);
    this.team = await this.iconSet.visibility.fetchTeam();

    this.iconTree = await this.store.adapterFor('icon').group({ iconSet: this.params.id });

    addIconGroupToStore(this.iconTree, this.store);

    return this;
  }
}

export default class ManageDashboardsIconSetRoute extends AuthenticatedRoute {
  async model(params) {
    const iconSetModel = new IconSetModelData(params, this.store);

    return iconSetModel.reload();
  }
}
