/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from "qunit";
import { setupRenderingTest, it } from "dummy/tests/helpers";
import { render, triggerEvent, waitFor, waitUntil } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectChoose, selectSearch } from "ember-power-select/test-support";

module("Integration | Component | editor/source-record", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let changedValue;

  let receivedPicture;
  let picture1;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle("1");
    server.testData.storage.addDefaultArticle("2");
    picture1 = server.testData.storage.addDefaultPicture("1");

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { "Content-Type": "application/json" },
        JSON.stringify({
          picture: picture1,
        }),
      ];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders the source record fields", async function (a) {
    this.set("value", { model: "article", id: "1" });

    await render(hbs`<Editor::SourceRecord @value={{this.value}} />`);

    await waitFor(".model-name");

    const selector = this.element.querySelector(".model-name");
    expect(selector.value).to.equal("article");

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("title");
  });

  it("can select a custom model and article", async function (a) {
    this.set("value", {});

    await render(hbs`<Editor::SourceRecord @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".model-name").value = "article";
    await triggerEvent(".model-name", "change");

    expect(changedValue).to.deep.equal({
      useSelectedModel: false,
      useDefaultModel: false,
      model: "article",
      id: null,
    });

    await selectSearch(".model-id", "test");
    await selectChoose(".model-id", "title");

    a.deepEqual(changedValue, {
      id: "1",
      model: "article",
      useDefaultModel: false,
      useSelectedModel: false,
    });
  });

  it("can select the default map", async function () {
    this.set("value", {});

    await render(hbs`<Editor::SourceRecord @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".model-name").value = "map";
    await triggerEvent(".model-name", "change");

    this.element.querySelector(".use-default-model").value = "yes";
    await triggerEvent(".use-default-model", "change");

    expect(changedValue).to.deep.equal({ useSelectedModel: false, useDefaultModel: true, model: "map" });
  });

  it("can select the default newsletter when there is a preferred model", async function (a) {
    this.set("value", {});

    await render(
      hbs`<Editor::SourceRecord @value={{this.value}} @onChange={{this.onChange}} @preferredModel="newsletter" />`,
    );

    this.element.querySelector(".use-default-model").value = "yes";
    await triggerEvent(".use-default-model", "change");

    expect(this.element.querySelector(".model-id")).not.to.exist;

    a.deepEqual(changedValue, { useSelectedModel: false, useDefaultModel: true, model: "newsletter" });
  });

  it("can select the picture model when it is data-with-picture is true", async function (a) {
    this.set("value", {});
    this.set("testElement", "");

    await render(
      hbs`<div class="test" data-with-picture="true"></div><Editor::SourceRecord @element={{this.testElement}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.set("testElement", this.element.querySelector(".test"));

    const options = [...this.element.querySelectorAll(".model-name option")].map((a) => a.textContent.trim());

    a.deepEqual(options, [
      "",
      "article",
      "map",
      "survey",
      "icon",
      "icon set",
      "feature",
      "team",
      "event",
      "calendar",
      "newsletter",
      "picture",
    ]);
  });

  it("can upload a picture when data-with-picture is true", async function () {
    this.set("value", {});
    this.set("testElement", "");

    await render(
      hbs`<div class="test" data-with-picture="true"></div><Editor::SourceRecord @element={{this.testElement}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.set("testElement", this.element.querySelector(".test"));

    this.set("model", {
      page: {
        id: "page-id",
      },
    });

    await render(
      hbs`<div class="test" data-with-picture="true"></div><Editor::SourceRecord @model={{this.model}} @element={{this.testElement}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.element.querySelector(".model-name").value = "picture";
    await triggerEvent(".model-name", "change");

    const blob = server.testData.create.pngBlob();
    await triggerEvent(".input-picture [type='file']", "change", {
      files: [blob],
    });

    await waitUntil(() => receivedPicture);

    expect(receivedPicture.picture.meta.link).to.deep.equal({ model: "Page", id: "page-id" });
  });
});
