/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class InputManageDataBindingDestinationComponent extends Component {
  @service store;
  @tracked currentMap;
  @tracked _value = null;

  get value() {
    if (this._value !== null) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  change(value) {
    this._value = value;
  }

  @action
  save() {
    return this.args.onSave(this.value);
  }

  @action
  async setupCurrentValue() {
    this.currentMap = null;

    if (this.args.value?.type == 'Map') {
      try {
        this.currentMap = await this.store.findRecord('map', this.args.value?.modelId);
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }
  }
}
