/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, fillIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/text', function (hooks) {
  setupRenderingTest(hooks);

  describe('in the view mode', function (hooks) {
    it('renders the title', async function () {
      await render(hbs`<Manage::Text @title="test" @isMainValue={{true}}/>`);

      expect(this.element.querySelector('.panel-title').textContent.trim()).to.equal('test');
    });

    it('renders the value', async function () {
      await render(hbs`<Manage::Text @title="test" @isMainValue={{true}} @value="the test value"/>`);
      expect(this.element.querySelector('p').textContent.trim()).to.equal('the test value');
    });

    it('renders an error message when the value is not set and the emptyError exists', async function () {
      await render(hbs`<Manage::Text @title="test" @isMainValue={{true}} @emptyError="the error message"/>`);
      expect(this.element.querySelector('.alert').textContent.trim()).to.equal('the error message');
    });
  });

  describe('in edit mode', function (hooks) {
    it('should render an input text with the value', async function () {
      await render(
        hbs`<Manage::Text @title="test" @editablePanel="test" @isMainValue={{true}} @value="the test value"/>`,
      );
      expect(this.element.querySelector('input').value).to.equal('the test value');
    });

    it('should save a new value', async function () {
      let value;

      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::Text @title="test" @editablePanel="test" @isMainValue={{true}} @value="the test value" @onSave={{this.save}}/>`,
      );
      await fillIn('input', 'hello world');
      expect(this.element.querySelector(".invalid-feedback")).not.to.exist;

      await click('.btn-submit');

      expect(value).to.equal('hello world');
    });

    it('shows a validation message when an email field is invalid', async function () {
      let value;

      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::Text @type="email" @title="test" @editablePanel="test" @isMainValue={{true}} @value="the test value" @onSave={{this.save}}/>`,
      );
      await fillIn('input', 'hello world');

      expect(this.element.querySelector(".invalid-feedback")).to.have.textContent('This does not look like an email address.');
    });

    it('can set an email', async function () {
      let value;

      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::Text @type="email" @title="test" @editablePanel="test" @isMainValue={{true}} @value="the test value" @onSave={{this.save}}/>`,
      );
      await fillIn('input', 'a@b.cde');

      expect(this.element.querySelector(".invalid-feedback")).not.to.exist;

      await click('.btn-submit');

      expect(value).to.equal('a@b.cde');
    });
  });
});
