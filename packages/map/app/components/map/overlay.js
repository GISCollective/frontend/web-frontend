/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import Overlay from 'ol/Overlay';
import { action } from '@ember/object';

export default class MapOverlayComponent extends Component {
  get lon() {
    return this.args.position?.[0];
  }

  get lat() {
    return this.args.position?.[1];
  }

  @action
  setup(element) {
    if (!this.args.map) {
      return;
    }

    if (!this.overlay) {
      this.overlay = new Overlay({
        element,
        stopEvent: true,
        insertFirst: true,
        positioning: 'bottom-center',
        autoPan: false,
      });
    }

    try {
      this.args.map.addOverlay(this.overlay);
      this.overlay.setPosition(this.args.position);
      // eslint-disable-next-line no-empty
    } catch (err) {}
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.args.map?.removeOverlay(this.overlay);
  }
}
