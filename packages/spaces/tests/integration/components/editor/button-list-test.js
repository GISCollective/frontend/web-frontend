/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, click, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/button-list", function (hooks) {
  setupRenderingTest(hooks);
  let changedValue;

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("change", (v) => {
      changedValue = v;
    });

    this.set("value", [
      {
        name: "first button",
        classes: ["btn-primary", "size-btn-lg"],
        type: "app-store",
        storeType: "apple",
        link: "https://giscollective.com/page1",
      },
    ]);
  });

  it("renders the values", async function (a) {
    await render(hbs`<Editor::ButtonList @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelectorAll(".button-section")).to.have.length(1);

    expect(this.element.querySelector(".input-button-type").value).to.equal("app-store");
    expect(this.element.querySelector(".input-store-type").value).to.equal("apple");
  });

  it("can change the button type", async function () {
    await render(hbs`<Editor::ButtonList @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".input-button-type").value = "default";
    await triggerEvent(".input-button-type", "change");

    expect(changedValue).to.deep.equal([
      {
        name: "first button",
        classes: ["btn-primary", "size-btn-lg"],
        type: "default",
        storeType: "apple",
        link: "https://giscollective.com/page1",
        newTab: false,
      },
    ]);
  });

  it("can change the store type", async function () {
    await render(hbs`<Editor::ButtonList @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".input-store-type").value = "android";
    await triggerEvent(".input-store-type", "change");

    expect(changedValue).to.deep.equal([
      {
        name: "first button",
        classes: ["btn-primary", "size-btn-lg"],
        type: "app-store",
        storeType: "android",
        link: "https://giscollective.com/page1",
        newTab: false,
      },
    ]);
  });
});
