/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, waitUntil, fillIn, triggerEvent } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Acceptance | manage/databindings/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let dataBinding;
  let receivedDataBinding;


  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    dataBinding = server.testData.create.dataBinding();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultMap('1');
    server.testData.storage.addDefaultMap('2');
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();

    server.testData.storage.addDefaultUser();

    server.testData.storage.addDataBinding(dataBinding);

    server.put(`/mock-server/databindings/${dataBinding._id}`, (request) => {
      receivedDataBinding = JSON.parse(request.requestBody);
      receivedDataBinding.dataBinding._id = dataBinding._id;

      server.testData.storage.addDataBinding(receivedDataBinding.dataBinding);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedDataBinding)];
    });

    receivedDataBinding = null;
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/databindings/edit/${dataBinding._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdatabindings%2Fedit%2F5ca7bfc0ecd8490100cab980');
  });

  it('can visit /manage/databindings/edit', async function () {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);
    expect(currentURL()).to.equal('/manage/databindings/edit/5ca7bfc0ecd8490100cab980');
  });

  it('can change the name', async function () {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);

    await click('.row-name .btn-edit');
    await fillIn('.row-name input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedDataBinding);

    expect(receivedDataBinding).to.deep.equal({
      dataBinding: {
        name: 'new name',
        destination: {
          modelId: '5ca89e37ef1f7e010007f54c',
          type: 'Map',
          deleteNonSyncedRecords: false,
        },
        descriptionTemplate: '',
        fields: [
          { key: 'isPublished', destination: '', preview: [] },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '', preview: [] },
          { key: 'maps ', destination: 'attributes', preview: [] },
          { key: 'position.lon', destination: 'position.lon', preview: [] },
          { key: 'description', destination: 'description', preview: [] },
          { key: 'pictures', destination: 'pictures', preview: [] },
          { key: 'icons ', destination: 'icons', preview: [] },
          { key: 'attributes', destination: '', preview: [] },
          { key: 'contributors ', destination: 'contributors', preview: [] },
          { key: 'position.lat', destination: 'position.lat', preview: [] },
        ],
        updateBy: '',
        crs: '',
        connection: { type: 'file', config: {} },
        visibility: {
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        extraIcons: [],
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('can change the destination', async function () {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);

    await click('.container-group-destination .btn-edit');

    this.element.querySelector('.select-map').value = '2';
    await triggerEvent('.select-map', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedDataBinding);

    expect(receivedDataBinding).to.deep.equal({
      dataBinding: {
        name: 'test',
        destination: {
          modelId: '2',
          type: 'Map',
          deleteNonSyncedRecords: false,
        },
        descriptionTemplate: '',
        fields: [
          { key: 'isPublished', destination: '', preview: [] },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '', preview: [] },
          { key: 'maps ', destination: 'attributes', preview: [] },
          { key: 'position.lon', destination: 'position.lon', preview: [] },
          { key: 'description', destination: 'description', preview: [] },
          { key: 'pictures', destination: 'pictures', preview: [] },
          { key: 'icons ', destination: 'icons', preview: [] },
          { key: 'attributes', destination: '', preview: [] },
          { key: 'contributors ', destination: 'contributors', preview: [] },
          { key: 'position.lat', destination: 'position.lat', preview: [] },
        ],
        updateBy: '',
        crs: '',
        connection: {
          type: 'file',
          config: {},
        },
        visibility: {
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        extraIcons: [],
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('can change the connection info', async function () {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);

    await click('.container-group-connection-info .btn-edit');

    this.element.querySelector('.select-type').value = 'airtable';
    await triggerEvent('.select-type', 'change');

    await fillIn('.connection-info-_apiKey', 'a');
    await fillIn('.connection-info-baseId', 'b');
    await fillIn('.connection-info-tableName', 'c');
    await fillIn('.connection-info-viewName', 'd');

    await click('.btn-submit');
    await waitUntil(() => receivedDataBinding);

    expect(receivedDataBinding).to.deep.equal({
      dataBinding: {
        name: 'test',
        destination: {
          modelId: '5ca89e37ef1f7e010007f54c',
          type: 'Map',
          deleteNonSyncedRecords: false,
        },
        descriptionTemplate: '',
        fields: [
          { key: 'isPublished', destination: '', preview: [] },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '', preview: [] },
          { key: 'maps ', destination: 'attributes', preview: [] },
          { key: 'position.lon', destination: 'position.lon', preview: [] },
          { key: 'description', destination: 'description', preview: [] },
          { key: 'pictures', destination: 'pictures', preview: [] },
          { key: 'icons ', destination: 'icons', preview: [] },
          { key: 'attributes', destination: '', preview: [] },
          { key: 'contributors ', destination: 'contributors', preview: [] },
          { key: 'position.lat', destination: 'position.lat', preview: [] },
        ],
        updateBy: '',
        crs: '',
        connection: {
          type: 'airtable',
          config: { _apiKey: 'a', baseId: 'b', tableName: 'c', viewName: 'd' },
        },
        visibility: {
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        extraIcons: [],
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('can change the description template', async function () {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);

    await click('.row-description-template .btn-edit');
    await fillIn('.row-description-template textarea', 'new description');
    await click('.btn-submit');

    await waitUntil(() => receivedDataBinding);

    expect(receivedDataBinding).to.deep.equal({
      dataBinding: {
        name: 'test',
        destination: {
          modelId: '5ca89e37ef1f7e010007f54c',
          type: 'Map',
          deleteNonSyncedRecords: false,
        },
        descriptionTemplate: 'new description',
        fields: [
          { key: 'isPublished', destination: '', preview: [] },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '', preview: [] },
          { key: 'maps ', destination: 'attributes', preview: [] },
          { key: 'position.lon', destination: 'position.lon', preview: [] },
          { key: 'description', destination: 'description', preview: [] },
          { key: 'pictures', destination: 'pictures', preview: [] },
          { key: 'icons ', destination: 'icons', preview: [] },
          { key: 'attributes', destination: '', preview: [] },
          { key: 'contributors ', destination: 'contributors', preview: [] },
          { key: 'position.lat', destination: 'position.lat', preview: [] },
        ],
        updateBy: '',
        crs: '',
        connection: {
          type: 'file',
          config: {},
        },
        visibility: {
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        extraIcons: [],
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('can change the crs field', async function () {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);

    await click('.row-crs .btn-edit');
    await fillIn('.row-crs input', 'new crs');
    await click('.btn-submit');

    await waitUntil(() => receivedDataBinding);

    expect(receivedDataBinding).to.deep.equal({
      dataBinding: {
        name: 'test',
        destination: {
          modelId: '5ca89e37ef1f7e010007f54c',
          type: 'Map',
          deleteNonSyncedRecords: false,
        },
        descriptionTemplate: '',
        fields: [
          { key: 'isPublished', destination: '', preview: [] },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '', preview: [] },
          { key: 'maps ', destination: 'attributes', preview: [] },
          { key: 'position.lon', destination: 'position.lon', preview: [] },
          { key: 'description', destination: 'description', preview: [] },
          { key: 'pictures', destination: 'pictures', preview: [] },
          { key: 'icons ', destination: 'icons', preview: [] },
          { key: 'attributes', destination: '', preview: [] },
          { key: 'contributors ', destination: 'contributors', preview: [] },
          { key: 'position.lat', destination: 'position.lat', preview: [] },
        ],
        updateBy: '',
        crs: 'new crs',
        connection: {
          type: 'file',
          config: {},
        },
        visibility: {
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        extraIcons: [],
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('can change the updateBy field', async function (a) {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);

    await click('.row-update-by .btn-edit');

    this.element.querySelector('.row-update-by select').value = '_id';
    await triggerEvent('.row-update-by select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedDataBinding);

    a.deepEqual(receivedDataBinding, {
      dataBinding: {
        name: 'test',
        destination: {
          modelId: '5ca89e37ef1f7e010007f54c',
          type: 'Map',
          deleteNonSyncedRecords: false,
        },
        descriptionTemplate: '',
        fields: [
          { key: 'isPublished', destination: '', preview: [] },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '', preview: [] },
          { key: 'maps ', destination: 'attributes', preview: [] },
          { key: 'position.lon', destination: 'position.lon', preview: [] },
          { key: 'description', destination: 'description', preview: [] },
          { key: 'pictures', destination: 'pictures', preview: [] },
          { key: 'icons ', destination: 'icons', preview: [] },
          { key: 'attributes', destination: '', preview: [] },
          { key: 'contributors ', destination: 'contributors', preview: [] },
          { key: 'position.lat', destination: 'position.lat', preview: [] },
        ],
        updateBy: '_id',
        crs: '',
        connection: {
          type: 'file',
          config: {},
        },
        visibility: {
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        extraIcons: [],
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('can change the extra icons field', async function () {
    authenticateSession();
    await visit(`/manage/databindings/edit/${dataBinding._id}`);

    await click('.icon-select');
    await click('.btn-icon');
    await click('.modal-footer .btn-success');

    await waitUntil(() => receivedDataBinding);

    expect(receivedDataBinding).to.deep.equal({
      dataBinding: {
        name: 'test',
        destination: {
          modelId: '5ca89e37ef1f7e010007f54c',
          type: 'Map',
          deleteNonSyncedRecords: false,
        },
        descriptionTemplate: '',
        fields: [
          { key: 'isPublished', destination: '', preview: [] },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '', preview: [] },
          { key: 'maps ', destination: 'attributes', preview: [] },
          { key: 'position.lon', destination: 'position.lon', preview: [] },
          { key: 'description', destination: 'description', preview: [] },
          { key: 'pictures', destination: 'pictures', preview: [] },
          { key: 'icons ', destination: 'icons', preview: [] },
          { key: 'attributes', destination: '', preview: [] },
          { key: 'contributors ', destination: 'contributors', preview: [] },
          { key: 'position.lat', destination: 'position.lat', preview: [] },
        ],
        updateBy: '',
        crs: '',
        connection: { type: 'file', config: {} },
        visibility: {
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        extraIcons: ['5ca7bfc0ecd8490100cab980'],
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });
});
