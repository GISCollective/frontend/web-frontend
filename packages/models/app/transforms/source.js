/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class SourceTransform extends Transform {
  deserialize(serialized) {
    return new Source(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON ? deserialized?.toJSON() : deserialized;
  }
}

export class Source {
  @tracked type = '';
  @tracked modelId = '';
  @tracked remoteId = '';
  @tracked syncAt = null;

  constructor(serialized) {
    if (serialized?.type) {
      this.type = `${serialized?.type}`;
    }

    if (serialized?.modelId) {
      this.modelId = `${serialized?.modelId}`;
    }

    if (serialized?.remoteId) {
      this.remoteId = `${serialized?.remoteId}`;
    }

    if (serialized?.syncAt) {
      this.syncAt = `${serialized?.syncAt}`;
    }
  }

  toJSON() {
    return {
      type: this.type,
      modelId: this.modelId,
      remoteId: this.remoteId,
      syncAt: new Date(this.syncAt).toISOString(),
    };
  }
}
