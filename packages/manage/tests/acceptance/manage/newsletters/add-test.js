/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, fillIn, waitUntil } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/newsletters/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedNewsletter;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultPreferences();

    receivedNewsletter = null;

    const item = server.testData.storage.addDefaultNewsletter();

    server.testData.storage.addStat(`newsletter-subscribers-newsletter-${item._id}`, 20);
    server.testData.storage.addStat(`newsletter-emails-newsletter-${item._id}`, 30);
    server.testData.storage.addStat(`newsletter-open-newsletter-${item._id}`, 40);
    server.testData.storage.addStat(`newsletter-interactions-newsletter-${item._id}`, 50);

    server.post('/mock-server/newsletters', (request) => {
      receivedNewsletter = JSON.parse(request.requestBody);
      receivedNewsletter.newsletter._id = item._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedNewsletter)];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('redirects to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/newsletters/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fnewsletters%2Fadd');
  });

  describe('when an user is authenticated', function (hooks) {
    hooks.beforeEach(function () {
      let team = server.testData.storage.addDefaultTeam();
      team.allowNewsletters = true;

      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();
      await visit('/manage/newsletters/add');

      expect(currentURL()).to.equal('/manage/newsletters/add');
    });

    it('redirects to the edit page after the request response is received', async function () {
      authenticateSession();

      await visit(`/manage/newsletters/add`);

      await fillIn('.row-name input.form-control', 'new name');

      await fillIn('.row-description textarea', 'new description');

      await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

      await waitUntil(() => receivedNewsletter);

      expect(receivedNewsletter.newsletter.name).to.equal('new name');
      expect(receivedNewsletter.newsletter.description).to.equal('new description');
      expect(receivedNewsletter.newsletter.visibility).to.deep.equal({
        isPublic: false,
        isDefault: false,
        team: '5ca78e2160780601008f69e6',
      });

      expect(currentURL()).to.equal(`/manage/dashboards/newsletter/${receivedNewsletter.newsletter._id}`);
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit(`/manage/newsletters/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'New newsletter']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);
    });
  });
});
