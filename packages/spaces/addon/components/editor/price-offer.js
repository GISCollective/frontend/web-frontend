/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsPriceOfferComponent extends Component {
  @tracked _title;
  @tracked _subtitle;
  @tracked _priceList;
  @tracked _price;
  @tracked _priceDetails;
  @tracked _button;
  @tracked _buttonClasses;
  @tracked _container;

  get title() {
    return this.getProp("title");
  }

  get subtitle() {
    return this.getProp("subtitle");
  }

  get priceList() {
    return this.getProp("priceList", []);
  }

  get price() {
    return this.getProp("price");
  }

  get priceDetails() {
    return this.getProp("priceDetails");
  }

  get button() {
    return this.getProp("button");
  }

  get buttonClasses() {
    if (this._buttonClasses) {
      return this._buttonClasses;
    }

    return this.args.value?.data?.button?.classes ?? [];
  }

  get container() {
    return this.getProp("container");
  }

  getProp(name, _default = {}) {
    if (this[`_${name}`] !== undefined) {
      return this[`_${name}`];
    }

    return this.args.value?.data?.[name] ?? _default;
  }

  @action
  change(name, value) {
    this[name] = value;

    return this.triggerChange();
  }

  @action
  containerClassesChanged(newValue) {
    this._container = {
      ...this.container,
      classes: newValue,
    };

    return this.triggerChange();
  }

  triggerChange() {
    const newValue = {
      title: this.title,
      subtitle: this.subtitle,
      price: this.price,
      priceList: this.priceList,
      priceDetails: this.priceDetails,
      button: this.button,
      container: this.container,
    };

    newValue.button.classes = this.buttonClasses;

    return this.args.onChange(newValue);
  }
}
