/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/card', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a private card with no options', async function () {
    this.set('record', {
      name: 'title',
    });

    await render(hbs`<Manage::Card @record={{this.record}} />`);

    expect(this.element.querySelector('.title').textContent.trim()).to.equal('title');
    expect(this.element.querySelector('.badge')).not.to.exist;
    expect(this.element.querySelector('.btn-extend')).not.to.exist;
  });

  it('renders a public card with no options', async function () {
    this.set('record', {
      visibility: {
        isPublic: true,
      },
      name: 'title',
    });

    await render(hbs`<Manage::Card @record={{this.record}} />`);

    expect(this.element.querySelector('.title').textContent.trim()).to.equal('title');
    expect(this.element.querySelector('.badge').textContent.trim()).to.equal('public');
    expect(this.element.querySelector('.btn-extend')).not.to.exist;
  });

  it('renders the options when the record can be edited', async function () {
    this.set('record', {
      visibility: {
        isPublic: true,
      },
      canEdit: true,
      name: 'title',
    });

    await render(hbs`<Manage::Card @record={{this.record}} @hasOptions={{true}} />`);

    expect(this.element.querySelector('.options .dropdown')).to.exist;
  });

  it('renders the label when is set', async function () {
    await render(hbs`<Manage::Card @title="title" @label="label" as | type |>
</Manage::Card>`);

    expect(this.element.querySelector('.label').textContent.trim()).to.equal('label');
  });

  it('triggers the on click event when the title is pressed', async function () {
    let pressed;
    this.set('onClick', () => {
      pressed = true;
    });

    await render(hbs`<Manage::Card @title="title" @onClick={{this.onClick}} />`);

    await click('.title .btn');
    expect(pressed).to.be.true;
  });
});
