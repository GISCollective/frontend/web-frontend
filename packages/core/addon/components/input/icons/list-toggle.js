/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class InputIconsListToggleComponent extends Component {
  @tracked list = [];

  get value() {
    return this.args.value || [];
  }

  @action
  setup() {
    later(() => {
      this.updateList();
    });
  }

  @action
  updateList() {
    const selectedIds = this.args.value?.map((a) => a.id) ?? [];

    if (this.list.length == 0) {
      this.list = this.args.list?.map((icon) => new RenderedItem(icon)) ?? [];
    }

    this.list.forEach((item) => {
      item.isSelected = selectedIds.indexOf(item.icon.id) >= 0;
    });
  }

  @action
  toggle(icon) {
    let value = this.value.slice();
    const exists = value.find((a) => a.id == icon.id);

    if (exists) {
      value = value.filter((a) => a.id != icon.id);
    } else {
      value.push(icon);
    }

    this.args.onChange?.(value);
  }
}

class RenderedItem {
  @tracked icon;
  @tracked isSelected = false;

  constructor(icon) {
    this.icon = icon;
  }
}
