/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class NewsletterMessageListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map((a) => new NewsletterMessage(a));
  }

  serialize(deserialized) {
    return deserialized?.map((a) => a.toJSON()) ?? [];
  }
}

export class NewsletterMessage {
  @tracked articleId;
  @tracked status;
  @tracked deliveryDate;

  constructor(data) {
    if (typeof data.article == 'string') {
      this.articleId = data.article;
    }

    if (typeof data.status == 'string') {
      this.status = data.status;
    }

    if (data.deliveryDate) {
      this.deliveryDate = new Date(data.deliveryDate);
    }
  }

  toJSON() {
    return {
      article: this.articleId,
      status: this.status,
      deliveryDate: this.deliveryDate?.toISOString(),
    };
  }
}
