export interface StoreType {
  query<T>(model: string, query: Record<string, string | number | boolean>): Promise<T[]>;
  peekRecord<T>(model: string, id: string): T | null;
  findRecord<T>(model: string, id: string): Promise<T>;
  queryRecord<T>(model: string, query: Record<string, any>): Promise<T>;
}
