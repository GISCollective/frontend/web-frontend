/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../../../base/app-route';
import { hash } from 'rsvp';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class BrowseMapsMapViewIconSetRoute extends Route {
  @service fullscreen;
  @service icons;

  model(params) {
    const iconsParams = {
      iconSet: params.set_id,
    };

    this.loadingIconSet = this.store.peekRecord('icon-set', iconsParams.iconSet) || this.store.findRecord('icon-set', iconsParams.iconSet);

    const parentModel = this.modelFor('browse.maps.map-view');

    if (parentModel.map && parentModel.map.id) {
      iconsParams.map = parentModel.map.id;
    }

    const icons = this.icons.group(iconsParams);

    return hash({
      icons,
      iconSet: this.loadingIconSet,
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    // eslint-disable-next-line ember/no-controller-access-in-routes
    controller.parent = this.controllerFor('browse.maps.map-view');
  }

  @action
  loading() {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    let controller = this.controllerFor('browse.maps.map-view.icon-set-loading');
    controller.loadingIconSet = this.loadingIconSet;

    return true;
  }

  activate() {
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    const parentController = this.controllerFor('browse.maps.map-view');
    parentController.search = '';
    this.iconsCache = null;
  }
}
