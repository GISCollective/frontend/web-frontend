/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { Datepicker } from 'vanillajs-datepicker';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageEditorsDateComponent extends Component {
  @tracked _value = null;

  get value() {
    if (this._value !== null) {
      return this._value;
    }

    return this.args.value;
  }

  set value(newValue) {
    this._value = newValue;
    this.args.onChange?.(newValue);
  }

  @action
  change(event) {
    const localAsGMT = new Date(event.detail.date.getTime() - event.detail.date.getTimezoneOffset() * 60 * 1000);

    this.value = localAsGMT.toISOString().split('T')[0];
  }

  @action
  setup(element) {
    this.datepicker = new Datepicker(element, {
      buttonClass: 'btn',
      format: 'yyyy-mm-dd',
    });
  }
}
