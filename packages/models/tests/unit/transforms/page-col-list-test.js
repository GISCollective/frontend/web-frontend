/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { PageCol } from 'models/transforms/page-col-list';
import { fetchSourceList } from 'models/lib/fetch-source';

describe('Unit | Transform | page col list', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:page-col-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize([
      {
        options: ['option 1', 'option 2'],
        cols: [
          {
            type: 'type',
            data: {},
            options: ['option 1', 'option 2'],
          },
        ],
      },
    ]);

    expect(deserialized.length).to.deep.equal(1);
  });

  it('can serialize an object without gid', function () {
    let deserialized = transform.deserialize([{ col: '1', row: '1', type: 'article', name: 'test', data: {} }]);

    expect(transform.serialize(deserialized)).to.deep.equal([
      { container: 0, col: 1, row: 1, type: 'article', name: 'test', data: {} },
    ]);
  });

  it('can serialize an object with an empty gid', function () {
    let deserialized = transform.deserialize([
      {
        col: '1',
        row: '1',
        type: 'article',
        gid: '',
        name: 'test',
        data: { a: 1 },
      },
    ]);

    expect(transform.serialize(deserialized)).to.deep.equal([
      {
        container: 0,
        col: 1,
        row: 1,
        type: 'article',
        name: 'test',
        data: { a: 1 },
      },
    ]);
  });

  describe('incrementColNameAfter', function () {
    it('increments a default name when it matches the arguments', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementColNameAfter(0, 0, 0);

      expect(pageCol.name).to.equal('0.0.1');
      expect(pageCol.container).to.equal(0);
      expect(pageCol.row).to.equal(0);
      expect(pageCol.col).to.equal(1);
    });

    it('keeps the name when the container does not match', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementColNameAfter(1, 0, 0);

      expect(pageCol.name).to.equal('0.0.0');
    });

    it('keeps the name when the row does not match', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementColNameAfter(0, 1, 0);

      expect(pageCol.name).to.equal('0.0.0');
    });

    it('keeps the name when the col is greater than the name', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementColNameAfter(0, 0, 1);

      expect(pageCol.name).to.equal('0.0.0');
    });

    it('keeps the name when it is not a default one', function () {
      const pageCol = new PageCol({ name: 'some name' });

      pageCol.incrementColNameAfter(0, 0, 1);

      expect(pageCol.name).to.equal('some name');
    });

    it('keeps the name when it is an invalid default name', function () {
      const pageCol = new PageCol({ name: '...' });

      pageCol.incrementColNameAfter(0, 0, 1);

      expect(pageCol.name).to.equal('...');
    });

    it('adds -1 for the missing name parts', function () {
      const pageCol = new PageCol({ name: '2.0.' });
      pageCol.container = 2;

      pageCol.incrementColNameAfter(2, 0, 0);

      expect(pageCol.name).to.equal('2.0.-1');
      expect(pageCol.container).to.equal(2);
      expect(pageCol.row).to.equal(0);
      expect(pageCol.col).to.equal(-1);
    });
  });

  describe('incrementRowNameAfter', function () {
    it('increments a default col name when it matches the arguments', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementRowNameAfter(0, 0);

      expect(pageCol.name).to.equal('0.1.0');
      expect(pageCol.container).to.equal(0);
      expect(pageCol.row).to.equal(1);
      expect(pageCol.col).to.equal(0);
    });

    it('increments a default row name when it matches the arguments', function () {
      const pageCol = new PageCol({ name: '0.0.x' });

      pageCol.incrementRowNameAfter(0, 0);

      expect(pageCol.name).to.equal('0.1.-1');
      expect(pageCol.container).to.equal(0);
      expect(pageCol.row).to.equal(1);
      expect(pageCol.col).to.equal(-1);
    });

    it('keeps the name when the container does not match', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementRowNameAfter(1, 0);

      expect(pageCol.name).to.equal('0.0.0');
    });

    it('keeps the name when the row is greater than the name', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementRowNameAfter(0, 1);

      expect(pageCol.name).to.equal('0.0.0');
    });

    it('keeps the name when it is an invalid default name', function () {
      const pageCol = new PageCol({ name: '...' });

      pageCol.incrementRowNameAfter(0, 0);

      expect(pageCol.name).to.equal('...');
    });

    it('adds -1 for the missing name parts', function () {
      const pageCol = new PageCol({ name: '2..' });

      pageCol.incrementRowNameAfter(2, 0);

      expect(pageCol.name).to.equal('2.-1.-1');
      expect(pageCol.container).to.equal(2);
      expect(pageCol.row).to.equal(-1);
      expect(pageCol.col).to.equal(-1);
    });
  });

  describe('incrementContainerNameAfter', function () {
    it('increments a default name when it matches the arguments', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementContainerNameAfter(0);

      expect(pageCol.name).to.equal('1.0.0');

      expect(pageCol.container).to.equal(1);
      expect(pageCol.row).to.equal(0);
      expect(pageCol.col).to.equal(0);
    });

    it('increments a default name of a container component', function () {
      const pageCol = new PageCol({ name: '0.x.x' });

      pageCol.incrementContainerNameAfter(0);

      expect(pageCol.name).to.equal('1.-1.-1');

      expect(pageCol.container).to.equal(1);
      expect(pageCol.row).to.equal(-1);
      expect(pageCol.col).to.equal(-1);
    });

    it('keeps the name when the row is greater than the name', function () {
      const pageCol = new PageCol({ name: '0.0.0' });

      pageCol.incrementContainerNameAfter(1);

      expect(pageCol.name).to.equal('0.0.0');
    });

    it('keeps the name when it is an invalid default name', function () {
      const pageCol = new PageCol({ name: '...' });

      pageCol.incrementContainerNameAfter(0);

      expect(pageCol.name).to.equal('...');
    });

    it('adds -1 for the missing components', function () {
      const pageCol = new PageCol({ name: '2..' });

      pageCol.incrementContainerNameAfter(2);

      expect(pageCol.name).to.equal('3.-1.-1');
      expect(pageCol.container).to.equal(3);
      expect(pageCol.row).to.equal(-1);
      expect(pageCol.col).to.equal(-1);
    });
  });
});
