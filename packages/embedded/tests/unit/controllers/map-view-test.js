import { module, test } from 'qunit';
import { setupTest } from 'embedded/tests/helpers';

module('Unit | Controller | map-view', function (hooks) {
  setupTest(hooks);

  // TODO: Replace this with your real tests.
  test('it exists', function (assert) {
    let controller = this.owner.lookup('controller:map-view');
    assert.ok(controller);
  });
});
