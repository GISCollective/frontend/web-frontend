/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageArticleComponent extends Component {
  @tracked _value;
  @tracked _title;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  get isDisabled() {
    return this._value ? false : true;
  }

  @action
  change(title, article) {
    this._title = title;
    this._value = article;
  }

  @action
  cancel() {
    this._value = null;

    return this.args.onCancel?.();
  }

  @action
  save() {
    return this.args.onSave?.(this._title, this._value);
  }
}
