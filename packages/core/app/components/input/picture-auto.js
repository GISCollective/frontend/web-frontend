/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import PictureMeta from 'models/lib/picture-meta';

export default class InputPictureAutoComponent extends Component {
  @tracked isError = false;
  @tracked progress = 100;
  @tracked _value;

  @service toaster;
  @service store;

  updateProgress(value) {
    this.progress = value;

    if (!this.value) {
      return;
    }

    this.value.progress = value;
  }

  get value() {
    if (!this._value) {
      return this.args.value;
    }

    return this._value;
  }

  get createImageFn() {
    if (this.args.createImage) {
      return this.args.createImage();
    }

    return this.store.createRecord('picture', {
      name: 'logo',
      picture: '',
      meta: new PictureMeta(),
    });
  }

  @action
  createImage() {
    return this.createImageFn;
  }

  @action
  isCreated() {
    if (!this.args.value) {
      return;
    }

    if (this.args.value.get('isNew')) {
      this.save(this.args.index, this.args.value);
    }
  }

  @action
  async save(index, value) {
    if (!value.save) {
      return;
    }

    this.isError = false;

    if (value.get('hasDirtyAttributes') || value.get('isNew')) {
      try {
        await value.save({
          adapterOptions: {
            progress: (value) => {
              this.updateProgress(value);
            },
          },
        });
        this.args.save?.(index, value);
        this.args.onChange?.(value);
      } catch (err) {
        this.isError = true;
        this.toaster.handleError(err);
      }
    }
  }
}
