/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from './base/app-route';
import { service } from '@ember/service';

export default class AdminRoute extends Route {
  @service user;

  async beforeModel() {
    if (!this.user.isAdmin) {
      return this.router.transitionTo('index');
    }
  }
}
