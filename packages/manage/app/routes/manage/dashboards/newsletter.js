/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageDashboardsNewsletterRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.isFastBoot) {
      return null;
    }

    const newsletter = await this.store.findRecord('newsletter', params.id);
    const team = newsletter.visibility.fetchTeam();

    const articles = this.store.query('article', {
      relatedId: newsletter.id,
      type: 'newsletter-article',
    });

    return hash({
      newsletter,
      team,
      welcomeMessage: newsletter.welcomeMessage,
      articles,
    });
  }
}
