/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ActivateController extends Controller {
  @service notifications;
  @service fastboot;
  @service router;
  @service store;

  queryParams = ['email', 'token'];
  @tracked email = null;
  @tracked token = null;
  @tracked userEmail = '';

  get isActivationResponse() {
    return !this.model.error && this.email && this.token;
  }

  get errorCode() {
    return this.model.error ? 'activation-expire-error' : '';
  }

  @action
  async activate() {
    await this.store.adapterFor('user').activate(this.userEmail);

    this.notifications.showMessage('Activate account', 'If your email is in our database, a new link was sent.');
    this.router.transitionTo('login.index');
  }
}
