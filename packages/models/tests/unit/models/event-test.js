/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Unit | Model | event', function (hooks) {
  setupTest(hooks, {}, this);

  let calendar;
  let event;
  let server;
  let store;

  hooks.before(function () {
    server = new TestServer();

    calendar = server.testData.storage.addDefaultCalendar();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    store = this.owner.lookup('service:store');
    server.history = [];
  });

  it('exists', function () {
    let model = store.createRecord('event', {});
    expect(model).to.be.ok;
  });

  it('returns the calendar id and name as categories', async function () {
    let calendar = store.createRecord('calendar', { id: 'calendar-id', name: 'calendar name' });
    let event = store.createRecord('event', { id: 'some-id', calendar });

    expect(event.categories).to.deep.equal(['calendar-id', 'calendar name']);
  });

  describe('relatedIdsMap', function () {
    it('returns nothing when the event has no calendar', async function () {
      let event = store.createRecord('event', { id: 'some-id' });

      expect(event.relatedIdsMap).to.deep.equal({});
    });

    it('returns nothing when it has a calendar and not a team', async function () {
      let calendar = store.createRecord('calendar', { id: 'calendar-id', name: 'calendar name' });
      let event = store.createRecord('event', { id: 'some-id', calendar });

      expect(event.relatedIdsMap).to.deep.equal({ calendar: 'calendar-id', team: undefined });
    });

    it('returns an object when it has a calendar and a team', async function () {
      let calendar = store.createRecord('calendar', { id: 'calendar-id', name: 'calendar name' });
      let event = store.createRecord('event', { id: 'some-id', calendar, visibility: { teamId: '1' } });

      expect(event.relatedIdsMap).to.deep.equal({ calendar: 'calendar-id', team: '1' });
    });
  });
});
