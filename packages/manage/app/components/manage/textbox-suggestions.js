/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageTextboxSuggestionsComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  onChange(value) {
    this._value = value;
    return this.save();
  }

  @action
  save() {
    if (this.args.onSave) {
      this.args.onSave(this.value);
    }

    this._value = null;
  }
}
