/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | view/list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a list with all options', async function () {
    this.set('value', {
      data: {
        textList: ['category 1', 'category 2'],
        textStyle: {
          classes: ['text-center'],
          color: 'green',
        },
        iconStyle: {
          color: 'red',
          name: 'check',
          size: '2',
        },
      },
    });

    await render(hbs`<View::List @value={{this.value}} />`);

    expect(this.element.querySelector('.list-item')).to.have.classes(
      'text-with-options text-center text-green list-item',
    );
    expect(this.element.querySelector('.fa-check')).to.have.classes(
      'fa-2x me-2 text-red',
    );
    expect(
      [...this.element.querySelectorAll('.list-item')].map((a) =>
        a.textContent.trim(),
      ),
    ).to.deep.equal(['category 1', 'category 2']);
  });
});
