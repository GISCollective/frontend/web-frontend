/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { TrackedMap } from 'tracked-maps-and-sets';
import { service } from '@ember/service';
import { debounce } from '@ember/runloop';
import { htmlSafe } from '@ember/template';

export default class PageColGalleryComponent extends Component {
  @service fullscreen;

  @tracked pictures;
  @tracked pictureListModel;
  @tracked selectedId;
  @tracked _gallery;

  @tracked isGalleryVisible = false;

  @tracked pictureList;

  @tracked normalizedWidths = [];

  sizeUpdates = 0;

  get gallery() {
    if (this._gallery) {
      return this._gallery;
    }

    return this.args.value?.data?.gallery ?? {};
  }

  get primaryPhoto() {
    if (!this.pictureList?.length) {
      return null;
    }

    if (this.mode != 'unique') {
      return null;
    }

    return this.pictureList[0];
  }

  get parentRecord() {
    if (this.args.value?.useSelectedModel) {
      return this.args.model?.selectedModel;
    }

    return null;
  }

  get record() {
    const key = this.args.value?.modelKey;
    return this.args.model?.[key];
  }

  get mode() {
    return this.gallery.primaryPhoto ?? '';
  }

  get galleryPictures() {
    if (!this.pictures?.length) {
      return [];
    }

    const includeCover = this.mode == 'same as the list';

    if (includeCover) {
      return this.pictures ?? [];
    }

    return this.pictures.slice(1);
  }

  get primaryPhotoValue() {
    const classes = this.args.value?.data?.primaryPhoto?.classes ?? [];
    const attributions = this.gallery.showPrimaryPhotoAttributions
      ? 'below'
      : '';

    return {
      sizing: 'auto',
      classes,
      attributions,
    };
  }

  get pictureOptions() {
    return {
      sizing: 'auto',
      classes: this.args.value?.data?.photoList?.classes ?? [],
    };
  }

  get pictureStyles() {
    return (
      this.normalizedWidths?.map?.((a) => htmlSafe(`width: ${a ?? 0}%`)) ?? []
    );
  }

  get pictureValues() {
    const list =
      this.galleryPictures.map?.((picture, i) => ({
        options: {
          sizing: 'auto',
          classes: this.args.value?.data?.photoList?.classes ?? [],
        },
        model: picture,
        style: htmlSafe(`width: ${this.normalizedWidths?.[i] ?? 0}%`),
      })) ?? [];

    return list;
  }

  get firstPicture() {
    return this.largePictureValues?.[0];
  }

  get photoList() {
    let list = this.pictureList ?? [];

    if (this.gallery.showPhotoList === false) {
      list = list.slice(2);
    }

    if (this.mode == 'unique' || this.mode == 'hidden') {
      return list.slice(1);
    }

    return list;
  }

  willDestroy() {
    super.willDestroy();

    this.hideGallery();
    this.isGalleryVisible = false;
  }

  @action
  updateGalleryProps(event) {
    this._gallery = event.detail.value?.blocks;
  }

  @action
  async setupPictureList(record, fieldName, value) {
    this.pictureList = value;
  }

  @action
  updateWidths() {
    if (this.sizeUpdates > 50) {
      return;
    }

    debounce(
      this,
      () => {
        this.sizeUpdates++;
        const images = [
          ...this.picturesContainerElement.querySelectorAll(
            '.picture-list-item img',
          ),
        ].map((a) => ({ width: a.naturalWidth, height: a.naturalHeight }));

        if (images.length == this.normalizedWidths?.filter((a) => a)?.length) {
          return;
        }

        const normalizedWidths = toPercentageWidth(
          images.map((a) => normalizedWidth(a.width, a.height)),
        );

        if (normalizedWidths.filter((a) => isNaN(a)).length > 0) {
          return;
        }

        this.normalizedWidths = normalizedWidths;
      },
      100,
    );
  }

  @action
  hideGallery() {
    this.isGalleryVisible = false;
  }

  @action
  async setup(element) {
    this.picturesContainerElement = element;

    let pictures = await this.record?.pictures;

    if (!this.pictureListModel) {
      this.pictureListModel = new TrackedMap();
    }

    if (pictures?.toArray) {
      pictures = pictures.slice();
    }

    const cover = await this.record?.cover;

    if (cover && !pictures.find((a) => a.id == cover.id)) {
      pictures = [cover, ...pictures];
    }

    this.pictures = pictures;

    if (!pictures?.length) {
      return;
    }

    for (const picture of this.pictures) {
      this.pictureListModel.set(`picture_${picture.id}`, picture);
    }
  }

  @action
  async showGallery() {
    if (this.args.isEditor) {
      return;
    }

    this.isGalleryVisible = true;
  }

  @action
  async showSlideshow(picture) {
    this.selectedId = picture.id;
  }

  @action
  async hideSlideshow() {
    this.selectedId = null;
  }
}

///

export function normalizedProportion(width, height) {
  const result = width / height;

  if (isNaN(result) || !isFinite(result)) {
    return 0;
  }

  return result;
}

export function normalizedWidth(width, height) {
  return normalizedProportion(width, height) * 100;
}

export function toPercentageWidth(widths) {
  if (widths.length == 0) {
    return [];
  }

  if (widths.length == 1) {
    return [100];
  }

  const total =
    widths.reduce((a, b) => a + b, 0) + widths.filter((a) => a).length;
  const proportion = 100 / total;

  return widths
    .map((a) => proportion * a)
    .map((a) => Math.floor(a * 1000) / 1000);
}
