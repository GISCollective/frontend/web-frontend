/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ManageSiteComponent extends Component {
  get parentMap() {
    if (!this.map || !this.map.id) {
      return null;
    }

    return this.map.id;
  }

  @action
  delete(item) {
    return this.onDelete(item);
  }
}
