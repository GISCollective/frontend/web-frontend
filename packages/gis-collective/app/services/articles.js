/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service, { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ArticlesService extends Service {
  @service store;
  @service fastboot;
  @service user;
  @service space;

  @tracked resolved = false;
  @tracked about;

  get aboutVisible() {
    return this.about?.visibility?.isPublic ?? false;
  }

  async checkAbout() {
    this.resolved = true;

    try {
      this.about = await this.store.queryRecord('article', { id: 'about', team: this.space.currentSpace.visibility.teamId });
    } catch (err) {
      this.about = null;
    }

    return this.hasAbout;
  }

  fillMetaInfo() {
    return this.articles?.fillMetaInfo(...arguments);
  }

  get hasAbout() {
    if(this.aboutVisible) {
      return true;
    }

    return this.user.isAdmin && this.about ? true : false;
  }
}
