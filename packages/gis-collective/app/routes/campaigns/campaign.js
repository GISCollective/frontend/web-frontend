/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/app-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class CampaignsCampaignRoute extends Route {
  @service fastboot;
  @service position;
  @service session;
  @service headData;
  @service store;

  async model(params) {
    const campaign = await this.store.findRecord('campaign', params.id);
    const cover = campaign.get("cover");

    return hash({
      campaign,
      cover
    });
  }
}
