/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from 'manage/routes/base/authenticated-route';
import { hash } from 'rsvp';
import LazyList from 'models/lib/lazy-list';

export default class ManageCampaignAnswersRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const query = { author: this.user.id };

    const answers = new LazyList('campaign-answer', query, this.store);
    await answers.loadNext();

    return hash({
      answers,
    });
  }
}
