import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ManageViewModeComponent extends Component {
  @action
  change(newValue) {
    this.args.value.viewMode = newValue;

    return this.args.onChange?.(this.args.value);
  }
}
