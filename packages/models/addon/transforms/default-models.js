import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class DefaultModelsTransform extends Transform {
  deserialize(serialized) {
    return new DefaultModels(serialized);
  }

  serialize(deserialized) {
    return new DefaultModels(deserialized).toJSON();
  }
}

export const defaultModelNames = ['map', 'campaign', 'calendar', 'newsletter'];

export class DefaultModels {
  @tracked map = '';
  @tracked campaign = '';
  @tracked calendar = '';
  @tracked newsletter = '';

  constructor(value) {
    if (typeof value != 'object') {
      return;
    }

    for (const key of defaultModelNames) {
      this[key] = value[key];
    }
  }

  toJSON() {
    const result = {};

    for (const key of defaultModelNames) {
      result[key] = this[key] || '';
    }

    return result;
  }
}
