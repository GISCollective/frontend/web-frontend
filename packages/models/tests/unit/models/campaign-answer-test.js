/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Model | campaign answer', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('campaign-answer', {});
    expect(model).to.be.ok;
  });

  describe('name', function () {
    it('returns the id when the about attributes are not set', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('campaign-answer', { id: 'some-id' });

      expect(model.name).to.equal('answer some-id');
    });

    it('returns the name when is set in the attributes', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('campaign-answer', {
        id: 'some-id',
        attributes: {
          about: {
            name: 'some test',
          },
        },
      });

      expect(model.name).to.equal('some test');
    });
  });

  describe('description', function () {
    it('returns an empty strung when the about attributes are not set', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('campaign-answer', { id: 'some-id' });

      expect(model.description).to.equal('');
    });

    it('returns the description when is set in the attributes', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('campaign-answer', {
        id: 'some-id',
        attributes: {
          about: {
            description: 'some test',
          },
        },
      });

      expect(model.description).to.equal('some test');
    });
  });
});
