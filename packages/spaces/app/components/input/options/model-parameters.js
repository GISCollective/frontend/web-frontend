/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { knownQueryParameters } from "../../../lib/known-query-parameters";

export default class InputOptionsModelParametersComponent extends Component {
  @tracked _value;
  @tracked parameters;

  elementId = `model-parameters-${guidFor(this)}`;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  get modelName() {
    return this.args.value?.model ?? "";
  }

  @action
  setupParameters() {
    if (!knownQueryParameters[this.modelName]) {
      return [];
    }

    this.parameters = [];

    for (const name of Object.keys(knownQueryParameters[this.modelName])) {
      this.parameters.push({
        name,
        value: this.args.value?.parameters?.[name],
      });
    }
  }

  triggerChange() {
    return this.args.onChange?.(this.value);
  }

  @action
  change(name, value) {
    if (!this._value) {
      this._value = this.value;
    }

    if (!this._value.parameters) {
      this._value.parameters = {};
    }

    this._value.parameters[name] = value;

    return this.triggerChange();
  }
}
