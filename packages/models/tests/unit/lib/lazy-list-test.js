/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { expect } from 'chai';
import { setupTest, it, describe } from 'dummy/tests/helpers';
import LazyList from 'models/lib/lazy-list';
import { Factories } from 'queries';
import {
  TrackedArray,
} from 'tracked-built-ins';

module('Unit | Adapter | lazy-list', function (hooks) {
  setupTest(hooks);

  hooks.before(function() {
    Factories.instance = {
      A: (a) => TrackedArray.from(a ?? [])
    };
  });

  it('initializes the object with an empty bucket list', function () {
    let list = new LazyList();

    expect(list.buckets.length).to.equal(0);
  });

  it('initializes the object with loadedAny = false', function () {
    let list = new LazyList();

    expect(list.loadedAny).to.equal(false);
    expect(list.isEmpty).to.equal(true);
  });

  it('initializes the object with canLoadMore = true', function () {
    let list = new LazyList(null, {}, {});

    expect(list.canLoadMore).to.equal(true);
  });

  it('initializes the object with isLoading = false', function () {
    let list = new LazyList();

    expect(list.isLoading).to.equal(false);
  });

  it('initializes the object with lastError = null', function () {
    let list = new LazyList();

    expect(list.lastError).to.equal(null);
  });

  it('sets is loading to true when the first page is requested', async function () {
    let done;
    let mockStore = {
      query: () =>
        new Promise((d) => {
          done = d;
        }),
    };

    let list = new LazyList('a', {}, mockStore);

    list.loadNext();

    expect(list.isLoading).to.equal(true);

    done();
    await list;
  });

  it("sets the error when the data can't be loaded", async function () {
    let mockStore = {
      query: () => new Promise((done, fail) => fail('some message')),
    };

    let list = new LazyList('a', {}, mockStore);

    await list.loadNext();

    expect(list.isLoading).to.equal(false);
    expect(list.lastError).to.equal('some message');
    expect(list.canLoadMore).to.equal(false);
  });

  it('requests the first page from the store with the right params', async function (a) {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);

    await list.loadNext();

    a.deepEqual(params, [
      'campaign',
      {
        limit: 24,
        skip: 0,
        team: 1,
      },
    ]);
  });

  it('does not request the records when there is an error', async function (a) {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);
    list.lastError = 'error';
    await list.loadNext();

    expect(params).not.to.exist;
  });

  it('adds the first page result to the bucket list', async function () {
    let mockStore = {
      query: function () {
        return [{ id: 1 }, { id: 2 }];
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);

    await list.loadNext();

    expect(list.buckets).to.have.length(1);
    expect(list.buckets[0]).to.deep.equal([{ id: 1 }, { id: 2 }]);
    expect(list.items).to.deep.equal([{ id: 1 }, { id: 2 }]);
    expect(list.loadedAny).to.equal(true);
    expect(list.isEmpty).to.equal(false);
  });

  it('adds a native array to the buckets list when the result is a tracked array', async function () {
    let mockStore = {
      query: function () {
        return Factories.instance.A([{ id: 1 }, { id: 2 }]);
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);

    await list.loadNext();

    expect(list.buckets[0].constructor === Array).to.equal(true);
  });

  it('adds the first two pages results to the bucket list', async function () {
    let mockStore = {
      query: function () {
        return [{ id: 1 }, { id: 2 }];
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);
    list.itemsPerPage = 2;

    await list.loadNext();
    await list.loadNext();

    expect(list.buckets).to.have.length(2);
    expect(list.buckets[0]).to.deep.equal([{ id: 1 }, { id: 2 }]);
    expect(list.buckets[1]).to.deep.equal([{ id: 1 }, { id: 2 }]);
    expect(list.items).to.deep.equal([{ id: 1 }, { id: 2 }, { id: 1 }, { id: 2 }]);
  });

  it('counts the loaded records and use the value as skip param', async function (a) {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);
    let objects1 = [];
    let objects2 = [];

    for (let i = 0; i < 24; i++) {
      objects1.push({ id: i });
      objects2.push({ id: i });
    }

    list.buckets.push(objects1);
    list.buckets.push(objects2);

    await list.loadNext();

    a.deepEqual(params, [
      'campaign',
      {
        limit: 24,
        skip: 48,
        team: 1,
      },
    ]);
  });

  it('counts the undeleted records and use the value as skip param', async function (a) {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);
    let objects = [];

    for (let i = 0; i < 24; i++) {
      objects.push({ id: i, isDeleted: i % 2 });
    }

    list.buckets.push(objects);

    await list.loadNext();

    a.deepEqual(params, [
      'campaign',
      {
        limit: 24,
        skip: 12,
        team: 1,
      },
    ]);
  });

  it('can not load more data when the last bucket has less items than the limit', async function (a) {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList('campaign', { team: 1 }, mockStore);
    let objects1 = [];

    for (let i = 0; i < 24; i++) {
      objects1.push({ id: i });
    }

    list.buckets.push(objects1);
    list.buckets.push([{ id: 'last' }]);

    await list.loadNext();

    expect(list.canLoadMore).to.equal(false);
    expect(params).not.to.exist;
  });

  describe('limitTo', function () {
    it('returns a new LazyList with 4 items when there are 24 items', async function (a) {
      let list = new LazyList('campaign', { team: 1 });
      let objects1 = [];

      for (let i = 0; i < 24; i++) {
        objects1.push({ id: i });
      }

      list.buckets.push(objects1);
      list.buckets.push([{ id: 'last' }]);

      const newList = list.limitTo(4);

      expect(newList.canLoadMore).to.equal(false);
      a.deepEqual(newList.items, [{ id: 0 }, { id: 1 }, { id: 2 }, { id: 3 }]);
    });
  });

  describe('when the query has a limit', async function () {
    it('initializes the object with canLoadMore = true', function () {
      let list = new LazyList('a', { limit: 10 }, {});

      expect(list.canLoadMore).to.equal(true);
    });

    it('requests the first page from the store with the right params', async function (a) {
      let params;
      let mockStore = {
        query: function () {
          params = [...arguments];

          return new Promise((done, fail) => done([]));
        },
      };

      let list = new LazyList('campaign', { limit: 10, team: 1 }, mockStore);

      await list.loadNext();

      a.deepEqual(params, [
        'campaign',
        {
          limit: 10,
          skip: 0,
          team: 1,
        },
      ]);

      expect(list.canLoadMore).to.equal(false);
    });
  });
});
