/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManagePhotoGalleryComponent extends Component {
  @tracked _value;

  get value() {
    if (this.args.editablePanel == this.args.title) {
      return this._value ?? this.args.value;
    }

    return this.args.value;
  }

  @action
  save() {
    return this.args.onSave(this.value);
  }

  @action
  cancel() {
    this._value = null;
    return this.args.onCancel();
  }

  @action
  change(newValue) {
    this._value = newValue;
  }
}
