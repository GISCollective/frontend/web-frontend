/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { hasMany, belongsTo, attr } from '@ember-data/model';
import { toContentBlocks } from 'models/lib/content-blocks';

export default class CampaignAnswerModel extends Model {
  @belongsTo('campaign', { async: true, inverse: null })
  campaign;

  @hasMany('picture', { async: true, inverse: null })
  pictures;

  @hasMany('sound', { async: true, inverse: null })
  sounds;

  @attr('geo') position;
  @attr('model-info') info;
  @hasMany('icon', { async: true, inverse: null })
  icons;

  @attr attributes;
  @attr contributor;

  @attr('number') status;

  @attr('review') review;

  @belongsTo('feature', { async: true, inverse: null })
  featureId;

  @attr('boolean') canEdit;
  @attr('boolean') canReview;

  get strStatus() {
    switch (this.status) {
      case 0:
        return 'processing';

      case 1:
        return 'pending';

      case 2:
        return 'published';

      case 3:
        return 'unpublished';

      case 4:
        return 'rejected';

      default:
        return 'unknown';
    }
  }

  get statusColor() {
    switch (this.status) {
      case 0:
        return 'secondary';

      case 1:
        return 'info';

      case 2:
        return 'success';

      case 3:
        return 'secondary';

      case 4:
        return 'danger';

      default:
        return 'secondary';
    }
  }

  get name() {
    if (!this.attributes?.about?.name) {
      return `answer ${this.id}`;
    }

    return this.attributes.about.name;
  }

  set title(value) {
    if (!this.attributes.about) {
      this.attributes.about = {};
    }

    this.attributes.about.name = value;
  }

  get firstIcon() {
    if (this.icons?.isDestroying || this.icons?.isDestroyed) {
      return null;
    }

    return this.resolvedIcons?.find((a) => typeof a.get('image') != 'undefined');
  }

  get description() {
    if (!this.attributes?.about?.description) {
      return ``;
    }

    return this.attributes.about.description;
  }

  set content(value) {
    if (!this.attributes.about) {
      this.attributes.about = {};
    }

    this.attributes.about.description = value;
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get resolvedIcons() {
    return this.hasMany('icons').value();
  }

  async approve() {
    await this.store.adapterFor(this.constructor.modelName).approve(this.id);
    await this.reload();
  }

  async reject() {
    await this.store.adapterFor(this.constructor.modelName).reject(this.id);
    await this.reload();
  }

  async revert() {
    await this.store.adapterFor(this.constructor.modelName).revert(this.id);
    await this.reload();
  }

  deleteMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.deleteMany(...arguments);
  }
}
