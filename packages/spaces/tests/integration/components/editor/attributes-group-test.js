import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { click, fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

describe("Integration | Component | editor/attributes-group", function (hooks) {
  setupRenderingTest(hooks);

  it("renders an empty string list by default", async function (assert) {
    await render(hbs`<Editor::AttributesGroup />`);

    expect(this.element.querySelector(".input-group")).not.to.exist;
  });

  it("can add a group", async function () {
    let value;

    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::AttributesGroup @onChange={{this.onChange}} />`);

    await click(".btn-add");

    await fillIn(".text-value-0", "other");

    expect(value).to.deep.equal({
      groups: ["other"],
    });
  });

  it("can delete a group", async function () {
    let value;

    this.set("onChange", (v) => {
      value = v;
    });

    this.set("value", {
      groups: ["other"],
    });

    await render(hbs`<Editor::AttributesGroup @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".btn-danger");

    expect(value).to.deep.equal({
      groups: [],
    });
  });
});
