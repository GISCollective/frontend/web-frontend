/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/feature/issue-button', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let feature;
  let store;

  hooks.beforeEach(async function () {
    server = new TestServer();
    server.testData.storage.addDefaultFeature('1');

    store = this.owner.lookup('service:store');
    feature = await store.findRecord('feature', '1');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Feature::IssueButton />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the button label and classes', async function () {
    this.set('value', {
      data: {
        source: { model: 'feature', id: '1' },
        button: {
          issueType: 'photo',
        },
        link: {
          classes: ['btn-primary', 'size-btn-lg'],
        },
      },
      modelKey: 'feature_1',
    });

    this.set('model', {
      feature_1: feature,
    });

    await render(
      hbs`<View::Feature::IssueButton @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a').textContent.trim()).to.equal(
      'suggest a photo',
    );
    expect(this.element.querySelector('a')).to.have.classes(
      'btn-primary size-btn-lg',
    );
  });

  it('renders the link when the issue type is generic', async function () {
    this.set('value', {
      data: {
        source: { model: 'feature', id: '1' },
        button: {
          issueType: 'generic',
        },
        style: {
          classes: ['btn-primary', 'size-btn-lg'],
        },
      },
      modelKey: 'feature_1',
    });

    this.set('model', {
      feature_1: feature,
    });

    await render(
      hbs`<View::Feature::IssueButton @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a').textContent.trim()).to.equal(
      'report a problem',
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/add/issue?feature=1&next=null&type=generic',
    );
  });
});
