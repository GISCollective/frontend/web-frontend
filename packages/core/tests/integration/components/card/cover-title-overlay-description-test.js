/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | card/cover-title-overlay-description', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let store;
  let mapData;
  let picture;

  hooks.before(function () {
    server = new TestServer();
    mapData = server.testData.storage.addDefaultMap();
    picture = server.testData.storage.addDefaultPicture();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };

    store = this.owner.lookup('service:store');
  });

  it('renders', async function () {
    await render(hbs`<Card::CoverTitleOverlayDescription />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the map title', async function (a) {
    const map = await store.findRecord('map', mapData._id);

    this.set('map', map);

    await render(hbs`<Card::CoverTitleOverlayDescription @record={{this.map}} />`);

    expect(this.element.querySelector('.name').textContent.trim()).to.equal('Harta Verde București');
  });

  it('does not render a link when the value is not set', async function () {
    const map = await store.findRecord('map', mapData._id);

    this.set('map', map);

    await render(hbs`<Card::CoverTitleOverlayDescription @record={{this.map}} />`);

    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('does not render a link when the value has a destination to a different model type', async function () {
    const map = await store.findRecord('map', mapData._id);

    this.set('campaign', map);

    this.set('value', {
      data: {
        destination: { slug: '/campaigns/:campaign-id' },
      },
    });

    await render(hbs`<Card::CoverTitleOverlayDescription @record={{this.campaign}} @value={{this.value}} />`);

    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders a link when the value has a map destination', async function () {
    const map = await store.findRecord('map', mapData._id);

    this.set('campaign', map);

    this.set('value', {
      data: {
        destination: { slug: '/maps/:map-id' },
      },
    });

    await render(hbs`<Card::CoverTitleOverlayDescription @record={{this.campaign}} @value={{this.value}} />`);

    expect(this.element.querySelector('a')).to.have.attribute('href', `/maps/${mapData._id}`);
  });
});
