/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { set, get } from '@ember/object';

export async function imageListUpload(record, pictures, field, retries = 3) {
  const items = pictures.map((picture) => ({ picture, success: false }));

  for (const item of items) {
    let attempts = 0;

    while (!item.success && attempts < retries) {
      attempts++;
      try {
        item.picture = await item.picture.save();
        item.success = true;
      } catch (err) {
        console.error(err);
      }
    }

    if (!item.success && !item.picture.isNew) {
      item.success = true;
    }
  }

  set(
    record,
    field,
    items.filter((a) => a.success).map((a) => a.picture),
  );

  await record.save();

  return record;
}
