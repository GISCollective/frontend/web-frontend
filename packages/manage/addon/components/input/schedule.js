/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { DateTime } from 'luxon';
import { CalendarEntry, scheduleGroup, beginSchedule, endSchedule } from 'models/transforms/calendar-entry-list';
import { action } from '@ember/object';

export default class InputScheduleComponent extends Component {
  @tracked _value;

  yesNoOptions = [
    { id: false, name: 'no' },
    { id: true, name: 'yes' },
  ];

  get value() {
    if (Array.isArray(this._value)) {
      return this._value;
    }

    return this.args.value;
  }

  get begin() {
    return beginSchedule(this.value);
  }

  get end() {
    return endSchedule(this.value);
  }

  get hasDateInterval() {
    return this.end.year <= 2050;
  }

  get days() {
    return scheduleGroup(this.value);
  }

  get timezone() {
    return this.begin.zoneName;
  }

  @action
  async changeBegin(newValue) {
    this.changeDate('begin', newValue, true);
    this.changeDate('end', newValue);
  }

  @action
  async changeDate(prop, newValue, ignoreChange) {
    let value = this.days;

    for (const dayIndex in value) {
      for (const intervalIndex in value[dayIndex].list) {
        if (isNaN(intervalIndex)) {
          continue;
        }

        value[dayIndex].list[intervalIndex][prop] = value[dayIndex].list[intervalIndex][prop]
          .set({
            year: newValue.year,
            month: newValue.month,
            day: newValue.day,
          })
          .plus({ day: dayIndex });
      }
    }

    this._value = value.flatMap((a) => a.list);

    if (!ignoreChange) {
      return this.args.onChange?.(this.value);
    }
  }

  @action
  changeDateInterval(value) {
    const newEndDate = value?.id ? this.begin.plus({ years: 1 }) : this.begin.set({ year: 2100 });

    return this.changeDate('intervalEnd', newEndDate);
  }

  @action
  async changeTime(dayIndex, intervalIndex, prop, newValue) {
    let value = this.days;

    value[dayIndex].list[intervalIndex][prop] = newValue;

    this._value = value.flatMap((a) => a.list);

    return this.args.onChange?.(this.value);
  }

  @action
  deleteInterval(dayIndex, intervalIndex) {
    let value = this.days;

    value[dayIndex].list[intervalIndex] = null;

    this._value = value.flatMap((a) => a.list).filter((a) => a);

    return this.args.onChange?.(this.value);
  }

  @action
  changeTimezone(timezone) {
    for (const entry of this.value) {
      entry.begin = entry.begin.setZone(timezone, { keepLocalTime: true });
      entry.end = entry.end.setZone(timezone, { keepLocalTime: true });
      entry.intervalEnd = entry.intervalEnd.setZone(timezone, {
        keepLocalTime: true,
      });
      entry.timezone = timezone;
    }

    return this.args.onChange?.(this.value);
  }

  @action
  addInterval(dayIndex) {
    const begin = this.begin.startOf('day').set({ hour: 9 }).plus({ day: dayIndex });
    const end = begin.set({ hour: 18 });
    const intervalEnd = end.plus({ years: 1 });
    const timezone = begin.zoneName;

    this._value = [
      ...this.value,
      new CalendarEntry({
        begin,
        end,
        intervalEnd,
        repetition: 'weekly',
        timezone,
      }),
    ];

    return this.args.onChange?.(this.value);
  }

  @action
  createSchedule() {
    const begin = DateTime.now().startOf('day').set({ hour: 9 });
    const end = begin.set({ hour: 18 });
    const intervalEnd = end.set({ year: 2100 });
    const timezone = begin.zoneName;

    this._value = [
      new CalendarEntry({
        begin,
        end,
        intervalEnd,
        repetition: 'weekly',
        timezone,
      }),
    ];

    return this.args.onChange?.(this.value);
  }
}
