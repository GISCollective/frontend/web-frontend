/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { fillIn, render, triggerEvent, waitUntil } from '@ember/test-helpers';
import { expect } from 'chai';
import { hbs } from 'ember-cli-htmlbars';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';
import { DateTime } from 'luxon';

module('Integration | Component | input/calendar-entry', function (hooks) {
  setupRenderingTest(hooks);
  let value;
  let begin;
  let end;
  let timezone;
  let now;

  hooks.beforeEach(function () {
    value = undefined;
    this.set('change', function (v) {
      value = v;
    });

    now = new Date();

    begin = new Date(
      Date.UTC(
        now.getUTCFullYear(),
        now.getUTCMonth(),
        now.getUTCDate() + 1,
        now.getUTCHours(),
        now.getUTCMinutes(),
        0,
      ),
    );

    end = new Date(
      Date.UTC(
        now.getUTCFullYear(),
        now.getUTCMonth(),
        now.getUTCDate() + 1,
        now.getUTCHours() + 1,
        now.getUTCMinutes(),
        0,
      ),
    );
    timezone = DateTime.local().zoneName;
  });

  it('renders a simple interval when no value is set', async function (a) {
    await render(hbs`<Input::CalendarEntry @onChange={{this.change}} />`);

    await waitUntil(() => value);

    const now = DateTime.now().set({ seconds: 0, milliseconds: 0 });
    const tomorrow = now.plus({ day: 1 });
    const then = DateTime.now().plus({ hour: 1 }).set({ seconds: 0, milliseconds: 0 });
    const thenTomorrow = then.plus({ day: 1 });

    expect(this.element.querySelector('.value-begin .datepicker-input').value).to.equal(
      `${tomorrow.year}-${tomorrow.month}-${tomorrow.day}`,
    );
    expect(this.element.querySelector('.value-end .datepicker-input').value).to.equal(
      `${thenTomorrow.year}-${thenTomorrow.month}-${thenTomorrow.day}`,
    );
    expect(this.element.querySelector('.value-repeats').value).to.equal(`norepeat`);
    expect(this.element.querySelector('.value-interval-end .datepicker-input')).not.to.exist;

    expect(value.toJSON()).to.deep.equal({
      begin: begin.toISOString(),
      end: end.toISOString(),
      intervalEnd: '0001-01-01T00:00:00.000Z',
      repetition: 'norepeat',
      timezone,
    });
  });

  it('renders a UTC date', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'UTC',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);
    await waitUntil(() => this.element.querySelector('.value-interval-end .datepicker-input').value);

    expect(this.element.querySelector('.value-begin .datepicker-input').value).to.equal(`2023-1-1`);
    expect(this.element.querySelector('.value-end .datepicker-input').value).to.equal(`2023-1-1`);
    expect(this.element.querySelector('.value-repeats').value).to.equal(`monthly`);
    expect(this.element.querySelector('.value-interval-end .datepicker-input').value).to.equal(`2024-05-01`);

    expect(value).not.to.exist;
  });

  it('can change the begin date', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('.value-begin select.hour').value = '15';
    await triggerEvent('.value-begin select.hour', 'change');

    a.deepEqual(value.toJSON(), {
      begin: '2023-01-01T14:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });
  });

  it('can change the end date', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('.value-end select.hour').value = '15';
    await triggerEvent('.value-end select.hour', 'change');

    a.deepEqual(value.toJSON(), {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T14:00:00.000Z',
      intervalEnd: '2024-05-01T13:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });
  });

  it('shows an error message when the end date is before the start date', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector('.alert-end-date-before-start')).not.to.exist;

    await fillIn('.value-end .datepicker-input', '2022-01-01');

    expect(this.element.querySelector('.alert-end-date-before-start').textContent.trim()).to.equal(
      'The event ends before it starts.',
    );
  });

  it('shows an error message when the repetition end date is before the end date', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector('.alert-interval-end-date-before-end')).not.to.exist;

    await fillIn('.value-interval-end .datepicker-input', '2022-01-01');

    expect(this.element.querySelector('.alert-interval-end-date-before-end').textContent.trim()).to.equal(
      'The repetition end date is before the end of the event.',
    );
  });

  it('can change the intervalEnd date', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('.value-interval-end .datepicker-input', '2024-05-04');

    a.deepEqual(value.toJSON(), {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-04T10:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });
  });

  it('can change the event to a repeatable date', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      repetition: 'norepeat',
      timezone: 'CET',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('.value-repeats').value = 'monthly';
    await triggerEvent('.value-repeats', 'change');

    expect(this.element.querySelector('.value-interval-end .datepicker-input').value).to.equal('2023-01-01');

    await fillIn('.value-interval-end .datepicker-input', '2024-05-04');

    a.deepEqual(value.toJSON(), {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-04T10:00:00.000Z',
      repetition: 'monthly',
      timezone: 'CET',
    });
  });

  it('can change the timezone', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'Europe/Berlin',
    });

    await render(hbs`<Input::CalendarEntry @value={{this.value}} @onChange={{this.change}} />`);

    await selectSearch('.input-timezone', 'europe');
    await selectChoose('.input-timezone', 'Europe/Bucharest');

    a.deepEqual(value.toJSON(), {
      begin: '2022-12-31T23:00:00.000Z',
      end: '2023-01-01T00:00:00.000Z',
      intervalEnd: '2024-05-01T00:00:00.000Z',
      repetition: 'monthly',
      timezone: 'Europe/Bucharest',
    });
  });
});
