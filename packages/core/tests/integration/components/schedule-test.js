/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { CalendarEntry } from 'models/transforms/calendar-entry-list';
import { expect } from 'chai';

module('Integration | Component | schedule', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<Schedule />`);

    assert.dom(this.element).hasText('');
  });

  it('renders two intervals', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'CET',
      }),
      new CalendarEntry({
        begin: '2023-01-03T13:00:00Z',
        end: '2023-01-03T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'CET',
      }),
    ]);

    await render(hbs`<Schedule @value={{this.value}} />`);

    expect(
      this.element
        .querySelector('.date-interval')
        .textContent.split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('02/01/2023 - 02/01/2024');
    const items = this.element.querySelectorAll('.time-table-day');

    expect(
      items[0].textContent
        .split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('Mon: 11:00 - 13:00');
    expect(
      items[1].textContent
        .split('\n')
        .map((a) => a.trim())
        .filter((a) => a)
        .join(' '),
    ).to.equal('Tue: 14:00 - 19:00');
  });

  it('hides the date interval when the intervalEnd is after 2050', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2050-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'CET',
      }),
      new CalendarEntry({
        begin: '2023-01-03T13:00:00Z',
        end: '2023-01-03T18:00:00Z',
        intervalEnd: '2050-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'CET',
      }),
    ]);

    await render(hbs`<Schedule @value={{this.value}} />`);

    expect(this.element.querySelector('.date-interval')).not.to.exist;
  });
});
