import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class InputLanguage extends Component {
  @service user;

  @action
  change(v) {
    return this.args.onChange?.(v.id);
  }

  get languages() {
    return (
      this.user?.languages?.map?.((a) => ({
        name: a.name,
        id: a.locale,
      })) ?? []
    );
  }
}
