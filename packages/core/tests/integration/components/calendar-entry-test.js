/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | calendar-entry', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<CalendarEntry />`);

    assert.dom(this.element).hasText('');
  });

  it('renders a UTC date', async function (a) {
    const year = new Date().getFullYear();

    this.set('value', {
      begin: `${year}-01-01T00:00:00.000Z`,
      end: `${year}-01-01T01:00:00.000Z`,
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'norepeat',
      timezone: 'UTC',
    });

    await render(hbs`<CalendarEntry @value={{this.value}} />`);

    a.dom(this.element).hasText(`on 01/01/${year} 00:00 - 01:00 UTC`);
  });

  it('renders a UTC date that repeats daily', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'daily',
      timezone: 'UTC',
    });

    await render(hbs`<CalendarEntry @value={{this.value}} />`);

    a.dom(this.element).hasText('daily between 01/01/2023 and 01/05/2024 00:00 - 01:00 UTC');
  });

  it('renders a UTC date that repeats weekly', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'weekly',
      timezone: 'UTC',
    });

    await render(hbs`<CalendarEntry @value={{this.value}} />`);

    a.dom(this.element).hasText('weekly between 01/01/2023 and 01/05/2024 Sunday 00:00 - 01:00 UTC');
  });

  it('renders a UTC date that repeats monthly', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'monthly',
      timezone: 'UTC',
    });

    await render(hbs`<CalendarEntry @value={{this.value}} />`);

    a.dom(this.element).hasText('monthly between 01/01/2023 and 01/05/2024 on 01 00:00 - 01:00 UTC');
  });

  it('renders a UTC date that repeats yearly', async function (a) {
    this.set('value', {
      begin: '2023-01-01T00:00:00.000Z',
      end: '2023-01-01T01:00:00.000Z',
      intervalEnd: '2024-05-01T01:00:00.000Z',
      repetition: 'yearly',
      timezone: 'UTC',
    });

    await render(hbs`<CalendarEntry @value={{this.value}} />`);

    a.dom(this.element).hasText('yearly between 01/01/2023 and 01/05/2024 on Jan 01 00:00 - 01:00 UTC');
  });
});
