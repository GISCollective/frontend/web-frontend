/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageSurveyNotificationsComponent extends Component {
  @tracked _checked = null;

  get checked() {
    if (this._checked !== null) {
      return this._checked;
    }

    return typeof this.args.value?.add == 'string';
  }

  set checked(value) {
    this._checked = value;
  }

  get notChanged() {
    const hasNotification = typeof this.args.value?.add == 'string';

    return this.checked == hasNotification;
  }

  @action
  save() {
    if (this.notChanged) {
      return this.cancel();
    }

    this.args.onSave?.(this.checked);
  }

  @action
  cancel() {
    this._checked = null;
    this.args.onCancel?.();
  }
}
