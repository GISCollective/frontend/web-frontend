/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class MapAdapter extends Adapter {
  getMeta(mapId) {
    const url = this.buildURL('map', mapId) + '/meta';
    return this.ajax(url, 'GET');
  }

  getAttributeValues(mapId, attribute, term = '', limit = 5) {
    const url = this.buildURL('map', mapId) + `/attributes?attribute=${attribute}&term=${term}&limit=${limit}`;
    return this.ajax(url, 'GET');
  }
}
