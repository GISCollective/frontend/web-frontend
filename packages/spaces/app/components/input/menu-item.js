/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class InputMenuItemComponent extends Component {
  @tracked _name = null;
  @tracked _link = null;

  elementId = "input-menu-item-" + guidFor(this);

  get name() {
    if (this._name !== null) {
      return this._name;
    }

    return this.args.value?.name;
  }

  set name(value) {
    this._name = value;

    this.triggerChange();
  }

  get link() {
    if (this._link !== null) {
      return this._link;
    }

    return this.args.value?.link?.url || this.args.value?.link?.pageId;
  }

  set link(value) {
    this._link = value;

    this.triggerChange();
  }

  @action
  changeLink(value) {
    this.link = value;
  }

  triggerChange() {
    const link = {};

    if (this.link?.indexOf("http") == 0) {
      link.url = this.link;
    } else {
      link.pageId = this.link;
    }

    return this.args.onChange?.({
      name: this.name,
      link,
    });
  }
}
