/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
const EmberAddon = require("ember-cli/lib/broccoli/ember-addon");

module.exports = function (defaults) {
  const app = new EmberAddon(defaults, {
    sassOptions: {
      includePaths: [
        "../../node_modules/bootstrap/scss",
        "../../node_modules/@fontsource",
        "../../node_modules/@fontsource-variable",
        "../map/app",
        "../core/app",
        "../manage/app",
        "../spaces-view/app",
        "./app",
      ],
    },
    "ember-power-select": {
      theme: "bootstrap",
    },
    "@embroider/macros": {
      setConfig: {
        "@ember-data/store": {
          polyfillUUID: true,
        },
      },
    },
  });

  const { maybeEmbroider } = require("@embroider/test-setup");
  return maybeEmbroider(app, {
    skipBabel: [
      {
        package: "qunit",
      },
    ],
  });
};
