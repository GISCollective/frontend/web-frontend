/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class EditorIconContainerComponent extends Component {
  config = [
    {
      type: "px",
      name: "height",
    },
    {
      type: "margin",
    },
    {
      type: "link",
      name: "destination",
    },
  ];
}
