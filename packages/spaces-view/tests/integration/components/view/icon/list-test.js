/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/icon/list', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let feature;
  let store;
  let storedFeature;

  hooks.beforeEach(async function () {
    server = new TestServer();
    storedFeature = server.testData.create.feature('1');
    storedFeature.icons = ['1', '2'];

    server.testData.storage.addDefaultIcon('1');
    server.testData.storage.addDefaultIcon('2');
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addFeature(storedFeature);

    store = this.owner.lookup('service:store');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Icon::List />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('img')).not.to.exist;
  });

  it('renders the feature icons when a feature is set', async function () {
    feature = await store.findRecord('feature', '1');
    await feature.icons;

    this.set('value', {
      data: {},
      modelKey: 'feature_1',
    });

    this.set('model', {
      feature_1: feature,
    });

    await render(
      hbs`<View::Icon::List @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelectorAll('img')).to.have.length(2);
  });
});
