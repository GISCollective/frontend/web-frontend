/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageGroupButtonsComponent extends Component {
  @service fastboot;

  @action
  cancel() {
    this.args.onCancel?.(this.args.value);
  }

  @action
  save() {
    this.args.onSave?.(this.args.value);
  }

  @action
  edit() {
    this.args.onEdit?.(this.args.value);
  }
}
