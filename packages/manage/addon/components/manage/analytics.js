/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageAnalyticsComponent extends Component {
  elementId = `input-manage-analytics-${guidFor(this)}`;
  @tracked _matomoUrl = null;
  @tracked _siteId = null;

  get siteId() {
    if (this._siteId != null) {
      return this._siteId;
    }

    return this.args.value?.matomoSiteId;
  }

  set siteId(value) {
    this._siteId = value;
  }

  get matomoUrl() {
    if (this._matomoUrl != null) {
      return this._matomoUrl;
    }

    return this.args.value?.matomoUrl;
  }

  set matomoUrl(value) {
    this._matomoUrl = value;
  }

  @action
  save() {
    return this.args.onSave?.(this.matomoUrl, this.siteId);
  }

  @action
  cancel() {
    this.matomoUrl = null;
    this.siteId = null;
    return this.args.onCancel?.();
  }
}
