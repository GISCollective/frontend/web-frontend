/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageCalendarsEditRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const teamsParams = {};

    teamsParams.all = !!params.allTeams;
    teamsParams.edit = !params.allTeams;

    const calendar = await this.store.findRecord('calendar', params.id);

    const editCalendarModel = { calendar };

    if (calendar.map.isEnabled) {
      try {
        await this.store.findRecord('map', calendar.map.mapId);
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
      }
    }

    const teamId = calendar.get('visibility').teamId;

    editCalendarModel.maps = await this.store.query('map', {
      edit: true,
      team: teamId,
    });
    editCalendarModel.teams = await this.store.query('team', teamsParams);
    editCalendarModel.team = await calendar.get('visibility').fetchTeam();
    editCalendarModel.iconSets = this.store.query('icon-set', {
      usableByTeam: teamId,
    });

    return hash(editCalendarModel);
  }
}
