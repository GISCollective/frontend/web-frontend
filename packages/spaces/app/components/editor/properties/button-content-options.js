/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsPropertiesBsOptionsComponent extends Component {
  elementId = `editor-property-${guidFor(this)}`;

  @tracked _type;
  @tracked _link;
  @tracked _name;
  @tracked _storeType;
  @tracked _newTab;
  @tracked _enablePicture;

  triggerChange() {
    this.args.onChange?.({
      type: this.type,
      link: this.link,
      name: this.name,
      storeType: this.storeType,
      newTab: this.newTab,
      enablePicture: this.enablePicture,
    });
  }

  get link() {
    return this.getProp("link");
  }

  get type() {
    return this.getProp("type");
  }

  get name() {
    return this.getProp("name");
  }

  get storeType() {
    return this.getProp("storeType");
  }

  get newTab() {
    return this.getProp("newTab");
  }

  get enablePicture() {
    return this.getProp("enablePicture");
  }

  get isDefault() {
    return !this.isAppStore || this.args.isAction;
  }

  get isAppStore() {
    return this.type == "app-store";
  }

  getProp(name) {
    const value = this[`_${name}`];

    if (value || typeof value == "boolean") {
      return value;
    }

    return this.args.value?.[name];
  }

  @action
  change(name, value) {
    this[name] = value;
    return this.triggerChange();
  }
}
