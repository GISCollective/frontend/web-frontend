/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/options/row-options',  function (hooks) {
  setupRenderingTest(hooks);

  describe ('sm',  function (hooks) {
    describe ('when there are no options',  function (hooks) {
      let value;

      hooks.beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);
      });

      it('can set the height', async function () {
        expect(this.element.querySelector('.input-sm-min-vh').value).to.equal('default');

        this.element.querySelector('.input-sm-min-vh').value = '30';
        await triggerEvent('.input-sm-min-vh', 'change');

        expect(value).to.deep.equal(['min-vh-30']);
      });

      it('can set a vertical gutter', async function () {
        expect(this.element.querySelector('.input-sm-gy').value).to.equal('default');

        this.element.querySelector('.input-sm-gy').value = '3';
        await triggerEvent('.input-sm-gy', 'change');

        expect(value).to.deep.equal(['gy-3']);
      });

      it('can set a horizontal gutter', async function () {
        expect(this.element.querySelector('.input-sm-gx').value).to.equal('default');

        this.element.querySelector('.input-sm-gx').value = '3';
        await triggerEvent('.input-sm-gx', 'change');

        expect(value).to.deep.equal(['gx-3']);
      });

      it('can set the justify content', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-sm-justify-content').value).to.equal('default');

        this.element.querySelector('.input-sm-justify-content').value = 'center';
        await triggerEvent('.input-sm-justify-content', 'change');

        expect(value).to.deep.equal(['justify-content-center']);
      });

      it('can set the item alignment', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-sm-align-items').value).to.equal('default');

        this.element.querySelector('.input-sm-align-items').value = 'center';
        await triggerEvent('.input-sm-align-items', 'change');

        expect(value).to.deep.equal(['align-items-center']);
      });

      it('can set the row visibility', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-sm-row-visibility').value).to.equal('default');

        this.element.querySelector('.input-sm-row-visibility').value = 'no';
        await triggerEvent('.input-sm-row-visibility', 'change');

        expect(value).to.deep.equal(['row-visibility-no']);
      });

      it('can set all options', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-sm-align-items').value).to.equal('default');

        this.element.querySelector('.input-sm-gy').value = '3';
        await triggerEvent('.input-sm-gy', 'change');
        this.element.querySelector('.input-sm-gx').value = '2';
        await triggerEvent('.input-sm-gx', 'change');
        this.element.querySelector('.input-sm-justify-content').value = 'end';
        await triggerEvent('.input-sm-justify-content', 'change');
        this.element.querySelector('.input-sm-align-items').value = 'center';
        await triggerEvent('.input-sm-align-items', 'change');

        expect(value).to.deep.equal(['gy-3', 'gx-2', 'justify-content-end', 'align-items-center']);
      });
    });

    describe ('when there are options',  function (hooks) {
      let value;

      hooks.beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        this.set('row', {
          options: ['gy-3', 'gx-3', 'justify-content-center', 'align-items-center', 'flex-grow-1'],
        });
        await render(hbs`<Input::Options::RowOptions @row={{this.row}} @onChangeOptions={{this.change}}/>`);
      });

      it('can reset the vertical gutter', async function () {
        expect(this.element.querySelector('.input-sm-gy').value).to.equal('3');

        this.element.querySelector('.input-sm-gy').value = 'default';
        await triggerEvent('.input-sm-gy', 'change');

        expect(value).to.deep.equal(['gx-3', 'justify-content-center', 'align-items-center', 'flex-grow-1']);
      });

      it('can reset the horizontal gutter', async function () {
        expect(this.element.querySelector('.input-sm-gx').value).to.equal('3');

        this.element.querySelector('.input-sm-gx').value = 'default';
        await triggerEvent('.input-sm-gx', 'change');

        expect(value).to.deep.equal(['gy-3', 'justify-content-center', 'align-items-center', 'flex-grow-1']);
      });

      it('can reset the justify content', async function () {
        expect(this.element.querySelector('.input-sm-justify-content').value).to.equal('center');

        this.element.querySelector('.input-sm-justify-content').value = 'default';
        await triggerEvent('.input-sm-justify-content', 'change');

        expect(value).to.deep.equal(['gy-3', 'gx-3', 'align-items-center', 'flex-grow-1']);
      });

      it('can reset the align items', async function () {
        expect(this.element.querySelector('.input-sm-align-items').value).to.equal('center');

        this.element.querySelector('.input-sm-align-items').value = 'default';
        await triggerEvent('.input-sm-align-items', 'change');

        expect(value).to.deep.equal(['gy-3', 'gx-3', 'justify-content-center',
          'flex-grow-1']);
      });

      it('can reset the grow props', async function () {
        expect(this.element.querySelector('.input-sm-flex-grow').value).to.equal('1');

        this.element.querySelector('.input-sm-flex-grow').value = 'default';
        await triggerEvent('.input-sm-flex-grow', 'change');

        expect(value).to.deep.equal(['gy-3', 'gx-3', 'justify-content-center', 'align-items-center']);
      });
    });
  });

  describe ('md',  function (hooks) {
    describe ('when there are no options',  function (hooks) {
      let value;

      hooks.beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);
      });

      it('can set a vertical gutter', async function () {
        expect(this.element.querySelector('.input-md-gy').value).to.equal('default');

        this.element.querySelector('.input-md-gy').value = '3';
        await triggerEvent('.input-md-gy', 'change');

        expect(value).to.deep.equal(['gy-md-3']);
      });

      it('can set a horizontal gutter', async function () {
        expect(this.element.querySelector('.input-md-gx').value).to.equal('default');

        this.element.querySelector('.input-md-gx').value = '3';
        await triggerEvent('.input-md-gx', 'change');

        expect(value).to.deep.equal(['gx-md-3']);
      });

      it('can set the justify content', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-md-justify-content').value).to.equal('default');

        this.element.querySelector('.input-md-justify-content').value = 'center';
        await triggerEvent('.input-md-justify-content', 'change');

        expect(value).to.deep.equal(['justify-content-md-center']);
      });

      it('can set the item alignment', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-md-align-items').value).to.equal('default');

        this.element.querySelector('.input-md-align-items').value = 'center';
        await triggerEvent('.input-md-align-items', 'change');

        expect(value).to.deep.equal(['align-items-md-center']);
      });

      it('can set the row visibility', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-md-row-visibility').value).to.equal('default');

        this.element.querySelector('.input-md-row-visibility').value = 'no';
        await triggerEvent('.input-md-row-visibility', 'change');

        expect(value).to.deep.equal(['row-visibility-md-no']);
      });

      it('can set all options', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        this.element.querySelector('.input-md-gy').value = '3';
        await triggerEvent('.input-md-gy', 'change');
        this.element.querySelector('.input-md-gx').value = '2';
        await triggerEvent('.input-md-gx', 'change');
        this.element.querySelector('.input-md-justify-content').value = 'end';
        await triggerEvent('.input-md-justify-content', 'change');
        this.element.querySelector('.input-md-align-items').value = 'center';
        await triggerEvent('.input-md-align-items', 'change');

        expect(value).to.deep.equal(['gy-md-3', 'gx-md-2', 'justify-content-md-end', 'align-items-md-center']);
      });
    });

    describe ('when there are options',  function (hooks) {
      let value;

      hooks.beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        this.set('row', {
          options: ['gy-md-3', 'gx-md-3', 'justify-content-md-center', 'align-items-md-center'],
        });
        await render(hbs`<Input::Options::RowOptions @row={{this.row}} @onChangeOptions={{this.change}}/>`);
      });

      it('can reset the vertical gutter', async function () {
        expect(this.element.querySelector('.input-md-gy').value).to.equal('3');

        this.element.querySelector('.input-md-gy').value = 'default';
        await triggerEvent('.input-md-gy', 'change');

        expect(value).to.deep.equal(['gx-md-3', 'justify-content-md-center', 'align-items-md-center']);
      });

      it('can reset the horizontal gutter', async function () {
        expect(this.element.querySelector('.input-md-gx').value).to.equal('3');

        this.element.querySelector('.input-md-gx').value = 'default';
        await triggerEvent('.input-md-gx', 'change');

        expect(value).to.deep.equal(['gy-md-3', 'justify-content-md-center', 'align-items-md-center']);
      });

      it('can reset the justify content', async function () {
        expect(this.element.querySelector('.input-md-justify-content').value).to.equal('center');

        this.element.querySelector('.input-md-justify-content').value = 'default';
        await triggerEvent('.input-md-justify-content', 'change');

        expect(value).to.deep.equal(['gy-md-3', 'gx-md-3', 'align-items-md-center']);
      });

      it('can reset the align items', async function () {
        expect(this.element.querySelector('.input-md-align-items').value).to.equal('center');

        this.element.querySelector('.input-md-align-items').value = 'default';
        await triggerEvent('.input-md-align-items', 'change');

        expect(value).to.deep.equal(['gy-md-3', 'gx-md-3', 'justify-content-md-center']);
      });
    });
  });

  describe ('lg',  function (hooks) {
    describe ('when there are no options',  function (hooks) {
      let value;

      hooks.beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);
      });

      it('can set a vertical gutter', async function () {
        expect(this.element.querySelector('.input-lg-gy').value).to.equal('default');

        this.element.querySelector('.input-lg-gy').value = '3';
        await triggerEvent('.input-lg-gy', 'change');

        expect(value).to.deep.equal(['gy-lg-3']);
      });

      it('can set a horizontal gutter', async function () {
        expect(this.element.querySelector('.input-lg-gx').value).to.equal('default');

        this.element.querySelector('.input-lg-gx').value = '3';
        await triggerEvent('.input-lg-gx', 'change');

        expect(value).to.deep.equal(['gx-lg-3']);
      });

      it('can set the justify content', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-lg-justify-content').value).to.equal('default');

        this.element.querySelector('.input-lg-justify-content').value = 'center';
        await triggerEvent('.input-lg-justify-content', 'change');

        expect(value).to.deep.equal(['justify-content-lg-center']);
      });

      it('can set the item alignment', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-lg-align-items').value).to.equal('default');

        this.element.querySelector('.input-lg-align-items').value = 'center';
        await triggerEvent('.input-lg-align-items', 'change');

        expect(value).to.deep.equal(['align-items-lg-center']);
      });

      it('can set the row visibility', async function () {
        await render(hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector('.input-lg-row-visibility').value).to.equal('default');

        this.element.querySelector('.input-lg-row-visibility').value = 'no';
        await triggerEvent('.input-lg-row-visibility', 'change');

        expect(value).to.deep.equal(['row-visibility-lg-no']);
      });
    });

    describe ('when there are options',  function (hooks) {
      let value;

      hooks.beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        this.set('row', {
          options: ['gy-lg-3', 'gx-lg-3', 'justify-content-lg-center', 'align-items-lg-center'],
        });
        await render(hbs`<Input::Options::RowOptions @row={{this.row}} @onChangeOptions={{this.change}}/>`);
      });

      it('can reset the vertical gutter', async function () {
        expect(this.element.querySelector('.input-lg-gy').value).to.equal('3');

        this.element.querySelector('.input-lg-gy').value = 'default';
        await triggerEvent('.input-lg-gy', 'change');

        expect(value).to.deep.equal(['gx-lg-3', 'justify-content-lg-center', 'align-items-lg-center']);
      });

      it('can reset the horizontal gutter', async function () {
        expect(this.element.querySelector('.input-lg-gx').value).to.equal('3');

        this.element.querySelector('.input-lg-gx').value = 'default';
        await triggerEvent('.input-lg-gx', 'change');

        expect(value).to.deep.equal(['gy-lg-3', 'justify-content-lg-center', 'align-items-lg-center']);
      });

      it('can reset the justify content', async function () {
        expect(this.element.querySelector('.input-lg-justify-content').value).to.equal('center');

        this.element.querySelector('.input-lg-justify-content').value = 'default';
        await triggerEvent('.input-lg-justify-content', 'change');

        expect(value).to.deep.equal(['gy-lg-3', 'gx-lg-3', 'align-items-lg-center']);
      });

      it('can reset the align items', async function () {
        expect(this.element.querySelector('.input-lg-align-items').value).to.equal('center');

        this.element.querySelector('.input-lg-align-items').value = 'default';
        await triggerEvent('.input-lg-align-items', 'change');

        expect(value).to.deep.equal(['gy-lg-3', 'gx-lg-3', 'justify-content-lg-center']);
      });
    });
  });
});
