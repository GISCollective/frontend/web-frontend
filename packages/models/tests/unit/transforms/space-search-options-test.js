/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';

import { expect } from 'chai';
import { setupTest, it } from 'dummy/tests/helpers';

module('Unit | Transform | space search options', function (hooks) {
  setupTest(hooks);
  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:space-search-options');
  });

  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('converts an undefined to an object with all options', function (a) {
    let deserialized = transform.deserialize(undefined);

    expect(deserialized.toJSON()).to.deep.equal({
      features: false,
      maps: false,
      campaigns: false,
      iconSets: false,
      teams: false,
      icons: false,
      geocodings: false,
      events: false,
      articles: false,
    });
  });

  it('converts an object with all fields to an object with all options', function (a) {
    let deserialized = transform.deserialize({
      features: true,
      maps: true,
      campaigns: true,
      iconSets: true,
      teams: true,
      icons: true,
      geocodings: true,
      events: true,
      articles: true,
    });

    expect(deserialized.toJSON()).to.deep.equal({
      features: true,
      maps: true,
      campaigns: true,
      iconSets: true,
      teams: true,
      icons: true,
      geocodings: true,
      events: true,
      articles: true,
    });
  });

  it('serializes an object with all properties', function (a) {
    let deserialized = transform.deserialize({
      features: true,
      maps: true,
      campaigns: true,
      iconSets: true,
      teams: true,
      icons: true,
      geocodings: true,
      events: true,
      articles: true,
    });

    expect(transform.serialize(deserialized)).to.deep.equal({
      features: true,
      maps: true,
      campaigns: true,
      iconSets: true,
      teams: true,
      icons: true,
      geocodings: true,
      events: true,
      articles: true,
    });
  });
});
