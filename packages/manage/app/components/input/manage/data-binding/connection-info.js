/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { Connection } from '../../../../transforms/connection';

export default class InputManageDataBindingConnectionInfoComponent extends Component {
  @tracked _config = null;
  @tracked _type = null;

  get type() {
    if (this._type !== null) {
      return this._type;
    }

    return this.args.value?.type;
  }

  get config() {
    if (this._config !== null) {
      return this._config;
    }

    return this.args.value?.config;
  }

  get isInvalid() {
    return !this.args.value?.type;
  }

  get fields() {
    const config = this.args.value?.config ?? {};

    return Object.keys(config).map((a) => ({
      name: a.indexOf('_') == 0 ? a.substring(1) : a,
      value: config[a],
      isHidden: a.indexOf('_') == 0,
    }));
  }

  @action
  changeType(value) {
    this._type = value;
  }

  @action
  changeConfig(value) {
    this._config = value;
  }

  @action
  save() {
    return this.args.onSave?.(new Connection({ type: this.type, config: this.config }));
  }
}
