/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupTest, it } from 'dummy/tests/helpers';
import { expect } from 'chai';

module('Unit | Transform | link map', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:link-map');
  });

  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('deserializes null to an empty object', function () {
    const result = transform.deserialize(null);

    expect(result.toJSON()).to.deep.equal({});
  });

  it('deserializes a map with one object', function () {
    const result = transform.deserialize({
      a: {
        route: 'b',
      },
    });

    expect(result.toJSON()).to.deep.equal({
      a: {
        route: 'b',
      },
    });
  });

  it('deserializes a map with two objects', function () {
    const result = transform.deserialize({
      a: {
        route: 'b',
      },
      c: {
        path: 'd',
      },
    });

    expect(result.toJSON()).to.deep.equal({
      a: {
        route: 'b',
      },
      c: {
        path: 'd',
      },
    });
  });

  it('serializes a map with two objects', function () {
    const value = transform.deserialize({
      a: {
        route: 'b',
      },
      c: {
        path: 'd',
      },
    });

    expect(transform.serialize(value)).to.deep.equal({
      a: {
        route: 'b',
      },
      c: {
        path: 'd',
      },
    });
  });
});
