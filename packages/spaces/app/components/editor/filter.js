/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsFiltersComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? {};
  }

  @action
  change(key, value) {
    if (!this._value) {
      this._value = this.value;
    }

    this._value[key] = value;

    return this.args.onChange(this._value);
  }
}
