/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { later, cancel } from '@ember/runloop';
import { action } from '@ember/object';

export default class InputManageAttributionsComponent extends Component {
  @action
  focusIn() {
    cancel(this.outTimer);
  }

  @action
  focusOut() {
    this.outTimer = later(() => {
      this.args.focusOut?.();
    }, 5);
  }
}
