/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { GeoJson } from 'core/lib/geoJson';
import PositionDetails from 'core/lib/positionDetails';
import { later } from '@ember/runloop';

export default class AttributesGroupMapBaseComponent extends Component {
  @service intl;
  @service fullscreen;
  @service mapStyles;
  @service session;

  @tracked editMode = false;
  @tracked _selectedBaseMap;
  @tracked ignoreWebGl;
  @tracked map;
  @tracked baseMaps;
  dismissedAt = 0;

  get markerMessage() {
    if (this.details.type != 'manual') {
      return null;
    }

    return 'message-map-marker-move';
  }

  get extentLabelPosition() {
    const extent = this.args.extent.toExtent();
    const center = this.args.extent.center.coordinates;

    center[1] = extent[3];
    return center;
  }

  get extentStyle() {
    return this.mapStyles.extent(this.mapStyleElement);
  }

  @action
  layerSetupError(layer) {
    if (layer.type == 'MapBox') {
      this.ignoreWebGl = true;
    }
  }

  get baseMaps() {
    if (this.ignoreWebGl) {
      return this.args.map?.baseMaps?.filter((a) => !a.needsWebGl) ?? [];
    }

    return this.args.map?.baseMaps ?? [];
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    return this.baseMaps?.[0] ?? null;
  }

  get mapExtent() {
    if (this.hasPositionInExtent || this.hasUpdatedPos) {
      const coordinates = this.args.position?.coordinates ?? [0, 0];

      const radius = 0.005;

      const topRight = [coordinates[0] - radius, coordinates[1] - radius];
      const bottomLeft = [coordinates[0] + radius, coordinates[1] + radius];

      return topRight.concat(bottomLeft);
    }

    if (this.args.extent) {
      const geoJson = new GeoJson(this.args.extent);
      const extent = geoJson.toOlFeature().getGeometry().getExtent();

      return extent;
    }

    return [-114, -41, 42, 72];
  }

  get details() {
    return this.args.details ?? {};
  }

  get position() {
    return new GeoJson(this.args.position ?? {});
  }

  get bearer() {
    let { access_token } = this.session?.get?.('data.authenticated') ?? null;

    if (!access_token) {
      return null;
    }

    return `Bearer ${access_token}`;
  }

  @action
  mapSetup(map) {
    this._map = map;
    this._mapView = map.getView();
  }

  @action
  edit() {
    this.editMode = true;
    this.fullscreen.freezeBody();

    this.prevPosition = new GeoJson(this.position);
    this.prevDetails = new PositionDetails(this.details);

    this.selectedPosition = null;
    this.selectedDetails = null;
  }

  @action
  selectBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  confirm() {
    this.editMode = false;
    this.fullscreen.unfreezeBody();
    this.hasUpdatedPos = true;

    this.updateMapSize();
  }

  @action
  dismiss() {
    this.dismissedAt = new Date().getTime();
    this.editMode = false;
    this.fullscreen.unfreezeBody();

    this.args.onChange?.(this.prevPosition, this.prevDetails);

    later(() => {
      this.updateMapSize();
    }, 100);
  }

  @action
  updateMapSize() {
    this._map?.updateSize?.();
  }

  @action
  updatePosition(map, view) {
    this._map = map;
    this._mapView = view;
  }

  @action
  async setup() {
    this.oldType = this.details.type;

    if (this.args.campaign) {
      this.map = await this.args.campaign.map.fetch();
      this.baseMaps = await this.args.campaign.baseMaps.fetch();
    }
  }
}
