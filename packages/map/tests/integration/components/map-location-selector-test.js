import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { transform } from 'ol/proj';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import Service from '@ember/service';

class MockPosition extends Service {
  hasPosition = true;
  center = [0, 0];

  setPosition(value) {
    this.hasPosition = true;
    this.center = value;
  }
}

describe('Integration | Component | map-location-selector', function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultBaseMap('1');
    this.owner.register('service:position', MockPosition);
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('when the location is set', function (hooks) {
    hooks.beforeEach(function () {
      const position = this.owner.lookup('service:position');
      position.setPosition([22.369218111038208, 44.43939480420836]);
    });

    it('renders the map centered to the user when the user is in the map extent', async function () {
      this.set('extent', {
        type: 'Polygon',
        coordinates: [
          [
            [-10.0, -10.0],
            [-10.0, 50.0],
            [50.0, 50.0],
            [50.0, -10.0],
            [-10.0, -10.0],
          ],
        ],
      });
      await render(hbs`<MapLocationSelector @extent={{this.extent}}/>`);

      const center = transform(
        [22.369218111038208, 44.43939480420836],
        'EPSG:4326',
        'EPSG:3857',
      );

      const map = this.element.querySelector('.map').olMap;

      expect(map.getView().getCenter()).to.deep.equal(center);
      expect(map.getView().getZoom()).to.equal(18);
      expect(map.getAllLayers()).to.have.length(1);
    });

    it('renders the map focused on the extent when the user in not in the extent', async function () {
      this.set('extent', {
        type: 'Polygon',
        coordinates: [
          [
            [-12.0, -10.0],
            [-12.0, 10.0],
            [10.5, 10.0],
            [10.5, -10.0],
            [-12.0, -10.0],
          ],
        ],
      });
      await render(hbs`<MapLocationSelector @extent={{this.extent}}/>`);

      const map = this.element.querySelector('.map').olMap;

      const center = transform(
        map.getView().getCenter(),
        'EPSG:3857',
        'EPSG:4326',
      ).map(a => Math.round(a * 100) / 100);

      expect(center).to.deep.equal([ -0.75, 0 ]);
      expect(map.getView().getZoom()).to.be.closeTo(5, 1);
      expect(map.getAllLayers()).to.have.length(1);
    });

    it('renders the map focused on the extent when the position is not set', async function () {
      this.set('extent', {
        type: 'Polygon',
        coordinates: [
          [
            [-12.0, -10.0],
            [-12.0, 10.0],
            [10.5, 10.0],
            [10.5, -10.0],
            [-12.0, -10.0],
          ],
        ],
      });
      this.owner.lookup('service:position').center = [0, 0];
      this.owner.lookup('service:position').hasPosition = false;

      await render(hbs`<MapLocationSelector @extent={{this.extent}}/>`);

      const map = this.element.querySelector('.map').olMap;

      const center = transform(
        map.getView().getCenter(),
        'EPSG:3857',
        'EPSG:4326',
      ).map(a => Math.round(a * 100) / 100);

      expect(center).to.deep.equal([ -0.75, 0 ]);
      expect(map.getView().getZoom()).to.be.closeTo(5, 1);
      expect(map.getAllLayers()).to.have.length(1);
    });

    it('triggers the on change event when the save button is pressed', async function () {
      let value;
      this.set('change', (v) => {
        value = v;
      });
      this.set('extent', {
        type: 'Polygon',
        coordinates: [
          [
            [-10.0, -10.0],
            [-10.0, 50.0],
            [50.0, 50.0],
            [50.0, -10.0],
            [-10.0, -10.0],
          ],
        ],
      });

      await render(hbs`<MapLocationSelector @extent={{this.extent}} @onChange={{this.change}} />`);

      await click('.btn-save');

      expect(value[0]).to.be.closeTo(22.369218111038204, 0.00000001);
      expect(value[1]).to.be.closeTo(44.43939480420835, 0.00000001);
    });
  });
});
