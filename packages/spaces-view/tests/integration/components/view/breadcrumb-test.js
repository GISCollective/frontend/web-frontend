/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/breadcrumb', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let spaceData;
  let store;
  let page1;
  let page2;
  let page3;

  hooks.before(function () {
    server = new TestServer();

    page1 = server.testData.storage.addDefaultPage('000000000000000000000001');
    page2 = server.testData.storage.addDefaultPage('000000000000000000000002');
    page3 = server.testData.storage.addDefaultPage('000000000000000000000003');

    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultTeam();

    page1.name = 'page 1';
    page1.slug = 'test--page1';

    page2.name = 'page 2';
    page2.slug = 'test--page2';

    page3.name = 'page 3';
    page3.slug = 'test--page3';

    spaceData = server.testData.storage.addDefaultSpace();
    spaceData.getPagesMap = () => {
      return {
        page1: '000000000000000000000001',
        page2: '000000000000000000000002',
        page3: '000000000000000000000003',
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    store = this.owner.lookup('service:store');
    const space = store.findRecord('space', spaceData._id);

    this.set('space', space);
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Breadcrumb />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the breadcrumb items when they are set', async function () {
    const page = await store.findRecord('page', '000000000000000000000001');

    this.set('model', {
      page,
    });
    this.set('value', {
      data: {
        items: ['000000000000000000000002', '000000000000000000000003'],
      },
    });

    await render(
      hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} />`,
    );

    const links = [...this.element.querySelectorAll('a')];

    expect(links[0].textContent.trim()).to.equal('page 2');
    expect(links[0]).to.have.attribute('href', '/test/page2');

    expect(links[1].textContent.trim()).to.equal('page 3');
    expect(links[1]).to.have.attribute('href', '/test/page3');

    expect(
      this.element.querySelector('.breadcrumb-item.active').textContent.trim(),
    ).to.equal('page 1');
  });

  it('renders the link style editor data attributes', async function (a) {
    const page = await store.findRecord('page', '000000000000000000000001');

    this.set('model', {
      page,
    });
    this.set('value', {
      data: {
        items: ['000000000000000000000002', '000000000000000000000003'],
      },
      name: 'some name',
      gid: 'some gid',
    });

    await render(
      hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} @path="some.path"/>`,
    );

    await waitFor('.breadcrumb-item-link a');

    const link = this.element.querySelector('.breadcrumb-item-link a');

    expect(link).to.have.attribute('data-name', 'some name');
    expect(link).to.have.attribute('data-gid', 'some gid');
    expect(link).to.have.attribute('data-type', 'text');
    expect(link).to.have.attribute('data-path', 'some.path.data.links');
    expect(link).to.have.attribute('data-editor', 'text-with-options');
  });

  it('renders the link destination editor data attributes', async function (a) {
    const page = await store.findRecord('page', '000000000000000000000001');

    this.set('model', {
      page,
    });
    this.set('value', {
      data: {
        items: ['000000000000000000000002', '000000000000000000000003'],
      },
      name: 'some name',
      gid: 'some gid',
    });

    await render(
      hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} @path="some.path" @isEditor={{true}}/>`,
    );

    await waitFor('.breadcrumb-item-link a.d-none');

    const link = this.element.querySelector('.breadcrumb-item-link a.d-none');

    expect(link).to.have.attribute('data-name', 'some name');
    expect(link).to.have.attribute('data-gid', 'some gid');
    expect(link).to.have.attribute('data-type', 'link');
    expect(link).to.have.attribute('data-path', 'some.path.data.items.0');
    expect(link).to.have.attribute('data-editor', 'link');
  });

  it('renders the current page editor data attributes', async function (a) {
    const page = await store.findRecord('page', '000000000000000000000001');

    this.set('model', {
      page,
    });
    this.set('value', {
      data: {
        items: ['000000000000000000000002', '000000000000000000000003'],
      },
      name: 'some name',
      gid: 'some gid',
    });

    await render(
      hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} @path="some.path"/>`,
    );

    await waitFor('.breadcrumb-item.active .text-with-options');

    const link = this.element.querySelector(
      '.breadcrumb-item.active .text-with-options',
    );

    expect(link).to.have.attribute('data-name', 'some name');
    expect(link).to.have.attribute('data-gid', 'some gid');
    expect(link).to.have.attribute('data-type', 'text');
    expect(link).to.have.attribute('data-path', 'some.path.data.currentPage');
    expect(link).to.have.attribute('data-editor', 'text-with-options');
  });

  it('renders the list editor data attributes', async function (a) {
    const page = await store.findRecord('page', '000000000000000000000001');

    this.set('model', {
      page,
    });
    this.set('value', {
      data: {
        items: ['000000000000000000000002', '000000000000000000000003'],
      },
      name: 'some name',
      gid: 'some gid',
    });

    await render(
      hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} @path="some.path"/>`,
    );

    await waitFor('li + .d-none');

    const link = this.element.querySelector('li + .d-none');

    expect(link).to.have.attribute('data-name', 'some name');
    expect(link).to.have.attribute('data-gid', 'some gid');
    expect(link).to.have.attribute('data-type', 'list');
    expect(link).to.have.attribute('data-path', 'some.path.data.items');
    expect(link).to.have.attribute('data-editor', 'generic-list');
  });

  describe('when the current record is a feature', async function (hooks) {
    let feature;
    let map;

    hooks.beforeEach(async function () {
      const featureData = server.testData.storage.addDefaultFeature();
      const mapData = server.testData.storage.addDefaultMap();

      feature = await store.findRecord('feature', featureData._id);
      map = await store.findRecord('map', mapData._id);
    });

    it('renders the current name as the inactive breadcrumb (legacy)', async function () {
      const page = await store.findRecord('page', '000000000000000000000001');

      this.set('model', {
        page,
        selectedModel: feature,
      });
      this.set('value', {
        data: {
          items: ['000000000000000000000002', '000000000000000000000003'],
        },
      });

      await render(
        hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} />`,
      );

      expect(
        this.element
          .querySelector('.breadcrumb-item.active')
          .textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
    });

    it('renders the current name as the inactive breadcrumb', async function () {
      const page = await store.findRecord('page', '000000000000000000000001');

      this.set('model', {
        page,
        selectedModel: feature,
      });

      this.set('value', {
        data: {
          items: [
            { link: { pageId: '000000000000000000000002' } },
            { link: { pageId: '000000000000000000000003' } },
          ],
        },
      });

      await render(
        hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} />`,
      );

      expect(
        this.element
          .querySelector('.breadcrumb-item.active')
          .textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
    });

    it('renders the team and map names when they are part of the slug (legacy)', async function () {
      page2.slug = 'test--:team_id';
      page3.slug = 'test--:map_id';

      const page = await store.findRecord('page', '000000000000000000000001');
      await feature.get('maps');

      await map.get('visibility').fetchTeam();

      this.set('model', {
        page,
        selectedModel: feature,
      });
      this.set('value', {
        data: {
          items: ['000000000000000000000002', '000000000000000000000003'],
        },
      });

      await render(
        hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} />`,
      );

      const links = [...this.element.querySelectorAll('a')];

      expect(links[0].textContent.trim()).to.equal(
        map.get('visibility').team.name,
      );
      expect(links[0]).to.have.attribute(
        'href',
        `/test/${map.get('visibility').team.id}`,
      );

      expect(links[1].textContent.trim()).to.equal(map.name);
      expect(links[1]).to.have.attribute('href', `/test/${map.id}`);
    });

    it('renders the team and map names when they are part of the slug (legacy)', async function () {
      page2.slug = 'test--:team-id';
      page3.slug = 'test--:map-id';

      const page = await store.findRecord('page', '000000000000000000000001');
      const maps = await feature.get('maps');
      const map = maps[0];
      await map.get('visibility').fetchTeam();

      this.set('model', {
        page,
        selectedModel: feature,
      });
      this.set('value', {
        data: {
          items: ['000000000000000000000002', '000000000000000000000003'],
        },
      });

      await render(
        hbs`<View::Breadcrumb @value={{this.value}} @model={{this.model}} />`,
      );

      const links = [...this.element.querySelectorAll('a')];

      expect(links[0].textContent.trim()).to.equal(
        map.get('visibility').team.name,
      );
      expect(links[0]).to.have.attribute(
        'href',
        `/test/${map.get('visibility').team.id}`,
      );

      expect(links[1].textContent.trim()).to.equal(map.name);
      expect(links[1]).to.have.attribute('href', `/test/${map.id}`);
    });
  });
});
