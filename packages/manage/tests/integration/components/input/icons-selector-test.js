/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, triggerEvent, waitFor, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | input/icons-selector', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let iconSet1Data;
  let iconSet2Data;


  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    iconSet1Data = server.testData.storage.addDefaultIconSet();
    iconSet2Data = server.testData.storage.addDefaultIconSet('2');
  });

  hooks.afterEach(function() {
    return server.waitAllRequests();
  });

  it('renders nothing when there is no icon set', async function (assert) {
    await render(hbs`<Input::IconsSelector />`);

    assert.dom(this.element).hasText('');
  });

  describe('when there are 2 icon sets', function (hooks) {
    let icon1;
    let icon2;
    let icon3;
    let iconSet1;
    let iconSet2;

    hooks.beforeEach(async function () {
      icon1 = server.testData.storage.addDefaultIcon('1');
      icon1.category = '';
      icon1.subcategory = '';

      icon2 = server.testData.storage.addDefaultIcon('2');
      icon2.category = '';
      icon2.subcategory = '';

      icon3 = server.testData.storage.addDefaultIcon('3');
      icon3.category = '';
      icon3.subcategory = '';

      iconSet1 = await this.store.findRecord('icon-set', iconSet1Data._id);
      iconSet2 = await this.store.findRecord('icon-set', iconSet2Data._id);
      iconSet2.name = 'set 2';

      this.set('iconSets', [iconSet1, iconSet2]);
    });

    it('does not render the category or subcategory selector', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      expect(this.element.querySelector('.select-category')).not.to.exist;
      expect(this.element.querySelector('.select-subcategory')).not.to.exist;
    });

    it('renders an icon set selector', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      expect(this.element.querySelector('.select-icon-set')).to.exist;
      const categories = [...this.element.querySelectorAll('.select-icon-set option')];

      expect(categories.length).to.equal(2);
      expect(categories[0].textContent.trim()).to.equal('Green Map® Icons Version 3');
      expect(categories[1].textContent.trim()).to.equal('set 2');
    });

    it('renders the icons', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      await waitFor('.icon-container');

      const icons = [...this.element.querySelectorAll('.icon-container')];

      expect(icons).to.have.length(3);
      expect(icons[0]).to.have.attribute('data-icon-id', '1');
      expect(icons[1]).to.have.attribute('data-icon-id', '2');
      expect(icons[2]).to.have.attribute('data-icon-id', '3');
    });
  });

  describe('when there are 3 icons with no category or subcategory', function (hooks) {
    let icon1;
    let icon2;
    let icon3;
    let iconSet;

    hooks.beforeEach(async function () {
      icon1 = server.testData.storage.addDefaultIcon('1');
      icon1.category = '';
      icon1.subcategory = '';

      icon2 = server.testData.storage.addDefaultIcon('2');
      icon2.category = '';
      icon2.subcategory = '';

      icon3 = server.testData.storage.addDefaultIcon('3');
      icon3.category = '';
      icon3.subcategory = '';

      iconSet = await this.store.findRecord('icon-set', iconSet1Data._id);

      this.set('iconSets', [iconSet]);
    });

    it('does not render the category or subcategory selector', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      expect(this.element.querySelector('.select-category')).not.to.exist;
      expect(this.element.querySelector('.select-subcategory')).not.to.exist;
    });

    it('renders the icons', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      await waitFor('.icon-container');

      const icons = [...this.element.querySelectorAll('.icon-container')];

      expect(icons).to.have.length(3);
      expect(icons[0]).to.have.attribute('data-icon-id', '1');
      expect(icons[1]).to.have.attribute('data-icon-id', '2');
      expect(icons[2]).to.have.attribute('data-icon-id', '3');
    });
  });

  describe('when there are 2 icons with 2 different categories and no subcategories', function (hooks) {
    let icon1;
    let icon2;
    let icon3;
    let iconSet;

    hooks.beforeEach(async function () {
      icon1 = server.testData.storage.addDefaultIcon('1');
      icon1.category = 'category1';
      icon1.subcategory = '';

      icon2 = server.testData.storage.addDefaultIcon('2');
      icon2.category = 'category2';
      icon2.subcategory = '';

      icon3 = server.testData.storage.addDefaultIcon('3');
      icon3.category = 'category1';
      icon3.subcategory = '';

      iconSet = await this.store.findRecord('icon-set', iconSet1Data._id);

      this.set('iconSets', [iconSet]);
    });

    it('only renders the two categories', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      await waitUntil(() => this.element.querySelectorAll('.select-category option').length);

      const categories = [...this.element.querySelectorAll('.select-category option')];

      expect(categories.length).to.equal(2);
      expect(categories[0].textContent.trim()).to.equal('category1');
      expect(categories[1].textContent.trim()).to.equal('category2');

      expect(this.element.querySelector('.select-subcategory')).not.to.exist;
    });

    it('renders the icons', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      const icons = [...this.element.querySelectorAll('.icon-container')];

      expect(icons).to.have.length(2);
      expect(icons[0]).to.have.attribute('data-icon-id', '1');
      expect(icons[1]).to.have.attribute('data-icon-id', '3');
    });

    it('renders the icons on category change', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      this.element.querySelector('.select-category').value = 'category2';
      await triggerEvent('.select-category', 'change');

      const icons = [...this.element.querySelectorAll('.icon-container')];

      expect(icons).to.have.length(1);
      expect(icons[0]).to.have.attribute('data-icon-id', '2');
    });
  });

  describe('when there are 2 icons with 2 different categories and subcategories', function (hooks) {
    let icon1;
    let icon2;
    let icon3;
    let iconSet;

    hooks.beforeEach(async function () {
      icon1 = server.testData.storage.addDefaultIcon('1');
      icon1.category = 'category1';
      icon1.subcategory = 'subcategory1';

      icon2 = server.testData.storage.addDefaultIcon('2');
      icon2.category = 'category2';
      icon2.subcategory = 'subcategory2';

      icon3 = server.testData.storage.addDefaultIcon('3');
      icon3.category = 'category1';
      icon3.subcategory = 'subcategory3';

      iconSet = await this.store.findRecord('icon-set', iconSet1Data._id);

      this.set('iconSets', [iconSet]);
    });

    it('renders the two categories', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      await waitUntil(() => this.element.querySelectorAll('.select-category option').length);

      const categories = [...this.element.querySelectorAll('.select-category option')];

      expect(categories.length).to.equal(2);
      expect(categories[0].textContent.trim()).to.equal('category1');
      expect(categories[1].textContent.trim()).to.equal('category2');
    });

    it('renders the icons', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      expect(this.element.querySelectorAll('.icon-container')).to.have.length(1);
      expect(this.element.querySelector('.icon-container')).to.have.attribute('data-icon-id', '1');

      this.element.querySelector('.select-category').value = 'category2';
      await triggerEvent('.select-category', 'change');

      expect(this.element.querySelectorAll('.icon-container')).to.have.length(1);
      expect(this.element.querySelector('.icon-container')).to.have.attribute('data-icon-id', '2');
    });

    it('renders the subcategories of the first category', async function () {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      await waitUntil(() => this.element.querySelectorAll('.select-subcategory option').length);

      const subcategories = [...this.element.querySelectorAll('.select-subcategory option')];

      expect(subcategories.length).to.equal(2);
      expect(subcategories[0].textContent.trim()).to.equal('subcategory1');
      expect(subcategories[1].textContent.trim()).to.equal('subcategory3');
    });

    it('renders the first category by default', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      await waitUntil(() => this.element.querySelectorAll('.select-subcategory option').length);

      expect(this.element.querySelector('.select-category').value).to.equal('category1');
    });

    it('selects the subcategory when the category is updated', async function (a) {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);

      await waitUntil(() => this.element.querySelectorAll('.select-subcategory option').length);

      this.element.querySelector('.select-category').value = 'category2';
      await triggerEvent('.select-category', 'change');

      expect(this.element.querySelector('.select-subcategory').value).to.equal('subcategory2');
    });

    it('renders the first subcategory by default', async function () {
      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} />`);
      await waitUntil(() => this.element.querySelectorAll('.select-subcategory option').length);

      expect(this.element.querySelector('.select-subcategory').value).to.equal('subcategory1');
    });

    it('adds the selected icons', async function () {
      let value;

      this.set('change', (v) => {
        value = v;
      });

      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} @onChange={{this.change}} />`);
      await waitUntil(() => this.element.querySelectorAll('.select-subcategory option').length);

      await click("[data-icon-id='1']");

      expect(value).to.have.length(1);
      expect(value[0].id).to.equal('1');
    });

    it('hides the selected icons', async function () {
      const icon = await this.store.findRecord('icon', icon1._id);

      this.set('value', [icon]);

      await render(hbs`<Input::IconsSelector @iconSets={{this.iconSets}} @value={{this.value}} />`);
      await waitUntil(() => this.element.querySelectorAll('.select-subcategory option').length);

      expect(this.element.querySelectorAll('.icon-container')).to.have.length(0);
    });
  });
});
