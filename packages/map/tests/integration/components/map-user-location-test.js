import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { transform } from 'ol/proj';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import Service from '@ember/service';

class MockPosition extends Service {
  hasPosition = true;
  center = [0, 0];

  setPosition(value) {
    this.center = value;
  }
}

describe('Integration | Component | user-location', function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultBaseMap('1');
    this.owner.register('service:position', MockPosition);
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders a map with the user location when exists', async function () {
    const position = this.owner.lookup('service:position');
    position.setPosition([22.369218111038208, 44.43939480420836]);
    const center = transform(
      [22.369218111038208, 44.43939480420836],
      'EPSG:4326',
      'EPSG:3857',
    );

    await render(hbs`<MapUserLocation />`);
    const map = this.element.querySelector('.map').olMap;

    expect(map.getView().getCenter()).to.deep.equal(center);
    expect(map.getView().getZoom()).to.equal(18);
  });

  it('renders a placeholder when the user location is not available', async function () {
    const position = this.owner.lookup('service:position');
    position.setPosition([0, 0]);

    await render(hbs`<MapUserLocation />`);

    expect(this.element.querySelector('.map-plain-container')).not.to.exist;
    expect(this.element.querySelector('.message')).to.have.textContent(
      "Can't find your location. Enable your location services and try again.",
    );
  });
});
