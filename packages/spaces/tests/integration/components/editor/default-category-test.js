/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/default-category", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the value", async function (assert) {
    this.set("value", {
      value: "some value",
    });

    await render(hbs`<Editor::DefaultCategory @value={{this.value}} />`);

    expect(this.element.querySelector(".input-text-value").value).to.equal("some value");
  });

  it("can update the value", async function () {
    this.set("value", "some text");

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::DefaultCategory @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".input-text-value", "new text");

    expect(value).to.deep.equal({ value: "new text" });
  });
});
