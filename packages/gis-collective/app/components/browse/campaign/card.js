/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class BrowseCampaignCardComponent extends Component {
  @service clickOutside;
  @tracked isAdminView;

  get picture() {
    if (this.args.campaign?.get) {
      return this.args.campaign?.get?.('cover');
    }

    return this.args.campaign?.cover;
  }

  get destination() {
    let destination = `/campaigns/:campaign_id`;

    if (this.args.value?.data?.destination && this.args.value.data.destination != '') {
      destination = this.args.value.data.destination;
    }

    return destination.replace(':campaign_id', this.args.campaign?.id);
  }

  get pictureOptions() {
    return {
      ...this.args.value?.data?.picture,
      sizing: 'proportional',
    };
  }

  get showDescription() {
    return this.args.value?.data?.components?.description ?? false;
  }

  get showName() {
    return this.args.value?.data?.components?.name ?? true;
  }

  get showCover() {
    return this.args.value?.data?.components?.cover ?? true;
  }

  @action
  linkSetup(element) {
    this.link = element;
  }

  @action
  transitionToCampaign() {
    return this.link.click();
  }

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showAdmin() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.hideAdmin();
    });

    this.isAdminView = true;
  }

  @action
  hideAdmin() {
    this.isAdminView = false;
  }
}
