/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class EventLocationTransform extends Transform {
  deserialize(serialized) {
    return new EventLocation(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() ?? deserialized;
  }
}

export class EventLocation {
  @tracked type;
  @tracked value;

  constructor(obj) {
    this.type = obj?.type ?? 'text';
    this.value = obj?.value ?? '';
  }

  toJSON() {
    return {
      type: this.type,
      value: this.value,
    };
  }
}
