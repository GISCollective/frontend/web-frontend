/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { LayoutCol } from 'models/transforms/layout-row-list';

describe ('Integration | Component | layout-col-content',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no column', async function () {
    await render(
      hbs`<LayoutColContent as |col|>{{col.name}}</LayoutColContent>`
    );

    expect(this.element.querySelector('.col')).to.exist;
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a column when it has a col', async function () {
    const col = new LayoutCol({
      name: 'test 1',
    });

    this.set('col', col);

    await render(
      hbs`<LayoutColContent @col={{this.col}} as |col|>{{col.name}}</LayoutColContent>`
    );

    expect(this.element.querySelector('.col')).to.exist;
    expect(this.element.textContent.trim()).to.equal('test 1');
  });

  it('renders the all classes when there is no size', async function () {
    const col = new LayoutCol({
      name: 'test 1',
      options: ['col-6', 'col-md-4', 'col-lg-2'],
    });

    this.set('col', col);

    await render(
      hbs`<LayoutColContent @col={{this.col}} as |col|>{{col.name}}</LayoutColContent>`
    );

    expect(this.element.querySelector('.col')).to.exist;
    expect(this.element.querySelector('.col')).to.have.classes(
      'col col-6 col-md-4 col-lg-2'
    );
  });

  it('renders the classes without size when there is the sm size', async function () {
    const col = new LayoutCol({
      name: 'test 1',
      options: ['col-6', 'col-md-4', 'col-lg-2'],
    });

    this.set('col', col);

    await render(
      hbs`<LayoutColContent @size="sm" @col={{this.col}} as |col|>{{col.name}}</LayoutColContent>`
    );

    expect(this.element.querySelector('.col')).to.exist;
    expect(this.element.querySelector('.col')).to.have.classes(
      'col col-6'
    );
  });

  it('renders the md classes when there is the md size', async function () {
    const col = new LayoutCol({
      name: 'test 1',
      options: ['col-6', 'col-md-4', 'col-lg-2'],
    });

    this.set('col', col);

    await render(
      hbs`<LayoutColContent @size="md" @col={{this.col}} as |col|>{{col.name}}</LayoutColContent>`
    );

    expect(this.element.querySelector('.col')).to.exist;
    expect(this.element.querySelector('.col')).to.have.classes(
      'col col-4'
    );
  });

  it('renders the lg classes when there is the lg size', async function () {
    const col = new LayoutCol({
      name: 'test 1',
      options: ['col-6', 'col-md-4', 'col-lg-2'],
    });

    this.set('col', col);

    await render(
      hbs`<LayoutColContent @size="lg" @col={{this.col}} as |col|>{{col.name}}</LayoutColContent>`
    );

    expect(this.element.querySelector('.col')).to.exist;
    expect(this.element.querySelector('.col')).to.have.classes(
      'col col-2'
    );
  });

  it('renders the container options', async function () {
    const col = new LayoutCol({
      name: 'test 1',
      options: ['col-6', 'col-md-4', 'col-lg-2'],
      data: {
        container: {
          color: 'green-100'
        }
      }
    });

    this.set('col', col);

    await render(
      hbs`<LayoutColContent @size="lg" @col={{this.col}} as |col|>{{col.name}}</LayoutColContent>`
    );

    expect(this.element.querySelector(".page-col-base-container")).to.have.class("bg-green-100");
  });

  it('yields twice when componentCount is 2', async function () {
    const col = new LayoutCol({
      name: 'test 1',
      componentCount: 2,
    });

    this.set('col', col);

    await render(
      hbs`<LayoutColContent @col={{this.col}} as |col|>{{col.name}}</LayoutColContent>`
    );

    const components = [...this.element.querySelectorAll('.page-col-content')];

    expect(components.length).to.equal(2);
  });
});
