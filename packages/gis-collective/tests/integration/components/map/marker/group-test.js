/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | map/marker/group',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Map::Marker::Group />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the number of pending feature count', async function () {
    this.set('feature', {
      getProperties() {
        return {
          pendingCount: 1,
        };
      },
    });

    await render(hbs`<Map::Marker::Group @feature={{this.feature}} />`);
    expect(this.element.textContent.trim()).to.equal(
      'There is a pending feature'
    );
  });
});
