/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe('Integration | Component | input/link component', function (hooks) {
  setupRenderingTest(hooks);
  let changedValue;
  let server;
  let space;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage('000000000000000000000002');

    space = server.testData.storage.addDefaultSpace();
    space.getPagesMap = () => {
      return {
        page1: '000000000000000000000001',
        page2: '000000000000000000000002',
        page3: '000000000000000000000003',
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set('space', space);
    this.set('onChange', (v) => {
      changedValue = v;
    });
  });

  it('renders a pageId when set', async function () {
    this.set('value', {
      pageId: '000000000000000000000001',
    });

    await render(hbs`<Input::Link @value={{this.value}} @space={{this.space}} />`);

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal('page1');
  });

  it('renders an url when set', async function () {
    this.set('value', {
      url: 'https://giscollective.com',
    });

    await render(hbs`<Input::Link @value={{this.value}} @space={{this.space}} />`);

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal(
      'https://giscollective.com',
    );
  });

  it('can change to a page id', async function () {
    this.set('value', {
      url: 'https://giscollective.com',
    });

    await render(hbs`<Input::Link @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`);

    await selectSearch('.ember-power-select-trigger', 'page2');
    await selectChoose('.ember-power-select-trigger', 'page2');

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal('page2');

    expect(changedValue).to.deep.equal({ pageId: '000000000000000000000002' });
  });

  it('can change to an url when the typed value is selected', async function () {
    this.set('value', {
      pageId: '000000000000000000000002',
    });

    await render(hbs`<Input::Link @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`);

    await selectSearch('.ember-power-select-trigger', 'https://giscollective.com');

    await selectChoose('.ember-power-select-trigger', 'https://giscollective.com');

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal(
      'https://giscollective.com',
    );
    expect(changedValue).to.deep.equal({
      url: 'https://giscollective.com',
    });
  });

  it('can change to an url without choosing the option', async function () {
    this.set('value', {
      pageId: '000000000000000000000002',
    });

    await render(hbs`<Input::Link @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`);

    await selectSearch('.ember-power-select-trigger', 'https://giscollective.com');

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal(
      'https://giscollective.com',
    );
    expect(changedValue).to.deep.equal({
      url: 'https://giscollective.com',
    });
  });

  it('can set an anchor', async function () {
    this.set('value', {
      pageId: '000000000000000000000002',
    });

    await render(hbs`<Input::Link @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`);

    await selectSearch('.ember-power-select-trigger', '#container-1');
    await selectChoose('.ember-power-select-trigger', '#container-1');

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal('#container-1');
    expect(changedValue).to.deep.equal({
      url: '#container-1',
    });
  });

  it('can set a path', async function () {
    this.set('value', {});

    await render(hbs`<Input::Link @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`);

    await selectSearch('.ember-power-select-trigger', '/login');
    await selectChoose('.ember-power-select-trigger', '/login');

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal('/login');
    expect(changedValue).to.deep.equal({
      path: '/login',
    });
  });
});
