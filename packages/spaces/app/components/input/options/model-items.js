/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { guidFor } from "@ember/object/internals";
import { A } from "models/lib/array";

export default class InputOptionsModelItemsComponent extends Component {
  elementId = `model-items-${guidFor(this)}`;

  @service store;
  @service intl;
  @tracked _allTeams = [];
  @tracked _value;
  @tracked isLoading = true;

  get modelList() {
    const list = ["article", "map", "campaign", "icon", "icon-set", "feature", "team", "event", "calendar"];

    if (this.args?.model?.selectedModel) {
      list.push("selected-model");
    }

    return list.map((id) => ({
      id,
      name: this.intl.t(`option-panel.model-name-list.${id}`),
    }));
  }

  get modelPropertyList() {
    const list = [];

    this.args.model?.selectedModel?.eachRelationship((name, relationship) => {
      if (relationship.meta.kind == "hasMany") {
        list.push(name);
      }
    });

    if (this.args.preferredModel) {
      const extraFields = this.args?.model?.selectedModel?.extraFieldsFor?.(this.args?.preferredModel) ?? [];

      for (const field of extraFields) {
        list.push(field);
      }
    }

    return list;
  }

  get modelProperty() {
    return this.args?.value?.property;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  get isAll() {
    return this.queryType === "all";
  }

  set isAll(value) {
    if (value) {
      this.changed({ model: this.model });
    }
  }

  get isIds() {
    return this.queryType === "ids";
  }

  set isIds(value) {
    if (value) {
      this.changed({ ids: [], model: this.model });
    }
  }

  get isGroups() {
    return this.queryType === "groups";
  }

  set isGroups(value) {
    if (value) {
      this.changed({ groups: [], model: this.model });
    }
  }

  get isParameters() {
    return this.queryType === "parameters";
  }

  set isParameters(value) {
    if (value) {
      this.changed({ parameters: {}, model: this.model });
    }
  }

  get ids() {
    if (Array.isArray(this.value?.ids)) {
      return this.value.ids;
    }

    return undefined;
  }

  get groups() {
    if (Array.isArray(this.value?.groups)) {
      return this.value.groups.map((a) => {
        if (a.toJSON) {
          return a;
        }

        return new RecordGroup(a.name, a.ids, () => {
          this.changeGroups();
        });
      });
    }

    return undefined;
  }

  get model() {
    if (this.value?.useSelectedModel) {
      return "selected-model";
    }

    if (typeof this.value?.model == "string") {
      return this.value.model;
    }

    return ``;
  }

  get queryType() {
    if (typeof this.teamId == "string") {
      return "team";
    }

    if (Array.isArray(this.ids)) {
      return "ids";
    }

    if (Array.isArray(this.groups)) {
      return "groups";
    }

    if (this.value?.parameters) {
      return "parameters";
    }

    return "all";
  }

  getValue(id) {
    return this.items?.find((a) => a.id == id);
  }

  get team() {
    const visibility = this.args.space?.visibility ?? {};

    if (visibility.isDefault) {
      return null;
    }

    return visibility.teamId;
  }

  @action
  modelPropertyChanged(property) {
    this.changed({
      useSelectedModel: true,
      property,
    });
  }

  @action
  modelChanged(value) {
    if (value.id == "selected-model") {
      return this.changed({ useSelectedModel: true });
    }

    this.changed({ model: value.id });
  }

  @action
  changeValues(values) {
    const ids = values.filter((a) => a);

    this.changed({ ids, model: this.model });
  }

  @action
  changed(source) {
    if (this.isGroups) {
      this.args.onChange?.({
        ...source,
        groups: source.groups?.map((a) => a.toJSON()) ?? [],
      });
    } else {
      this.args.onChange?.(source);
    }

    this._value = source;
  }

  @action
  changeParameters(newValue) {
    this.changed({ parameters: newValue?.parameters, model: this.model });
  }

  @action
  changeIds(group, ids) {
    group.ids = ids;
    this.changeGroups();
  }

  @action
  addGroup() {
    const groups = this.groups ?? [];

    groups.push(
      new RecordGroup("new group", [], () => {
        this.changeGroups();
      }),
    );

    this.changed({
      model: this.model,
      groups,
    });
  }

  @action
  deleteGroup(index) {
    const groups = A(this.groups ?? []);

    groups.splice(index, 1);

    this.changed({
      model: this.model,
      groups,
    });
  }

  changeGroups() {
    const value = {
      model: this.model,
      groups: this.groups,
    };

    this.changed(value);
  }

  @action
  async setup() {
    this._allTeams = await this.store.findAll("team");

    this.isLoading = false;
  }
}

class RecordGroup {
  @tracked _name;
  @tracked _ids;

  constructor(name, ids, onChange) {
    this._name = name;
    this._ids = ids;
    this.onChange = onChange;
  }

  get ids() {
    return this._ids;
  }

  set ids(value) {
    this._ids = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
    this.onChange?.();
  }

  toJSON() {
    return {
      name: this._name,
      ids: this._ids ?? [],
    };
  }
}
