/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class PropertyListComponent extends Component {
  get list() {
    if (Array.isArray(this.args.group?.list)) {
      return this.args.group.list;
    }

    if (Array.isArray(this.args.group)) {
      return this.args.group;
    }

    return [];
  }
}
