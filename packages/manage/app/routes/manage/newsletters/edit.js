/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageNewslettersEditRoute extends AuthenticatedRoute {
  @service('loading') loadingService;

  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.modelsToClear = ['newsletter'];

    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    const newsletter = await this.store.findRecord('newsletter', params.id);

    return hash({
      teams: this.store.query('team', teamsParams),
      newsletter,
      team: newsletter.visibility.fetchTeam(),
      welcomeMessage: newsletter.welcomeMessage,
    });
  }

  async afterModel(model) {
    await model.newsletter?.visibility?.fetchTeam();
  }

  @action
  loading(transition) {
    return !transition.from || transition.from.name != transition.to.name;
  }
}
