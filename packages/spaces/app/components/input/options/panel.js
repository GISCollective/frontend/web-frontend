/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { dasherize } from "@ember/string";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { later } from "@ember/runloop";

export default class InputOptionsPanelComponent extends Component {
  elementId = `presentation-options-${guidFor(this)}`;

  @tracked _config;
  @tracked value = {};
  @tracked state;

  get classes() {
    return this.state?.classes ?? this.value.classes ?? [];
  }

  get config() {
    return this._config?.map((field) => ({
      ...field,
      class: dasherize(field.label || field.name || field.type),
      value: this.value[field.name],
    }));
  }

  createValue() {
    let result = JSON.parse(JSON.stringify(this.args.value ?? {}));

    if (typeof result != "object") {
      result = {};
    }

    for (const field of this._config ?? []) {
      const key = field.name;

      if (typeof result[key] == "undefined" && field.options?.[0]) {
        result[key] = field.options[0];
      }

      if (typeof result[key] == "undefined" && field.type == "bool") {
        result[key] = false;
      }
    }

    if (Array.isArray(this.args.value?.classes)) {
      result.classes = this.args.value.classes;
    }

    this.value = result;
    this.state = result;
  }

  @action
  changeCss(newStyle) {
    this.state = {
      ...this.state,
      style: newStyle,
    };

    return this.args.onChange?.(this.state);
  }

  @action
  change(name, type, value) {
    let newValue = value;

    if (typeof value == "object" && typeof value.id != "undefined") {
      newValue = value.id;
    }

    if (type == "number") {
      newValue = parseFloat(value);
    }

    return new Promise((res) =>
      later(() => {
        this.state = {
          ...this.state,
          [name]: newValue,
        };

        res(this.args.onChange?.(this.state));
      }),
    );
  }

  @action
  changeWithMerge(newValues) {
    this.state = {
      ...this.state,
      ...newValues,
    };

    return this.args.onChange?.(this.state);
  }

  @action
  changeClasses(newClasses) {
    this.state = {
      ...this.state,
      classes: newClasses,
    };

    return this.args.onChange?.(this.state);
  }

  @action
  updateValue() {
    if (this.stringify(this.state) == this.stringify(this.args.value)) {
      return;
    }

    this.createValue();
  }

  stringify(obj) {
    return JSON.stringify(obj, function (key, val) {
      if (val != null && typeof val == "object") {
        if (typeof val.id == "string") {
          return val.id;
        }
      }
      return val;
    });
  }

  @action
  updateConfig() {
    let a = this.stringify(this._config);
    let b = this.stringify(this.args.config);

    if (a != b) {
      this._config = this.args.config;
      this.createValue();
    }
  }

  @action
  checkDefaultValues() {
    later(() => {
      this.updateConfig();
      this.createValue();

      if (this.stringify(this.state) == this.stringify(this.args.value)) {
        return;
      }

      this.args.onChange?.(this.state);
    });
  }
}
