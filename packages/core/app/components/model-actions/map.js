/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class ModelActionsMapComponent extends Component {
  @service embed;
  @service space;

  get linkTarget() {
    return this.embed.isEnabled ? '_blank' : '_self';
  }

  get mapLink() {
    return this.space.currentSpace.getLinkFor('Map', this.args.record.id);
  }
}
