/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/text-simple', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::TextSimple />`);

    assert.dom(this.element).hasText('');
  });

  it('renders the value when set', async function (assert) {
    await render(hbs`<View::TextSimple @value="value" />`);

    assert.dom(this.element).hasText('value');
  });
});
