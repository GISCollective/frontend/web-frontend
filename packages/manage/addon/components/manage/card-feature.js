import Component from './card';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class ManageFeatureCard extends Component {
  @service intl;
  @tracked cover;

  get firstParagraph() {
    if (!this.args?.record?.firstParagraph) {
      return this.intl.t('message-no-feature-description');
    }

    return this.args?.record?.firstParagraph;
  }

  get visibilityClass() {
    if (this.args.record?.visibility == 1) {
      return 'success';
    }

    if (this.args.record?.visibility == -1) {
      return 'info';
    }

    return 'warning';
  }

  get hasCover() {
    return this.cover?.picture;
  }

  get bgCover() {
    if (!this.cover) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${this.cover.picture}/md')`);
  }

  get visibilityText() {
    if (this.args.record?.visibility == 1) {
      return 'visibility-public';
    }

    if (this.args.record?.visibility == -1) {
      return 'visibility-pending';
    }

    return 'visibility-private';
  }

  @action
  async setup() {
    this.cover = await this.args.record?.getCover?.();
  }
}
