/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';

export default class PresentationModel extends Model {
  @attr('model-info') info;
  @attr('string') name;
  @attr('string') slug;

  @attr('boolean') hasIntro;
  @attr('boolean') hasEnd;
  @attr('number') introTimeout;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr endSlideOptions;

  @attr('page-col-list') cols;
  @attr('visibility') visibility;
  @attr('boolean') canEdit;

  get title() {
    return this.name;
  }
}
