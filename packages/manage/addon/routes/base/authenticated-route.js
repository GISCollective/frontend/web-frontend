/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class AuthenticatedRoute extends Route {
  @service session;
  @service fastboot;
  @service store;
  @service router;
  @service user;

  modelsToClear = [];

  get isFastBoot() {
    return this.fastboot.isFastBoot;
  }

  beforeModel(transition) {
    if (this.session.isAuthenticated) {
      return;
    }

    return this.router.transitionTo('login.index', {
      queryParams: {
        redirect: transition?.intent?.url,
      },
    });
  }

  @action
  willTransition() {
    if (this.store.isDestroyed || this.store.isDestroying) {
      return;
    }

    this.modelsToClear?.forEach((modelName) => {
      this.store.peekAll(modelName).forEach((a) => {
        a.rollbackAttributes();
      });
    });
  }
}
