import { module } from 'qunit';
import { visit, currentURL, click, waitUntil, fillIn } from '@ember/test-helpers';
import { setupApplicationTest, it, describe } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import { expect } from 'chai';

module('Acceptance | manage/teams/subscription', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let team;
  let receivedTeam;


  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultSpace();
    team = server.testData.storage.addDefaultTeam();

    team.subscription = {
      name: 'test',
      expire: '2021-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      details: 'details',
      monthlySupportHours: 1,
      support: [
        {
          hours: 1,
          date: '2022-03-22T00:22:11Z',
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 2,
          date: '2022-03-23T00:22:11Z',
          details: 'details 2',
        },
      ],
    };

    server.put(`/mock-server/teams/${team._id}`, (request) => {
      receivedTeam = JSON.parse(request.requestBody);
      receivedTeam.team['_id'] = team._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedTeam)];
    });

    receivedTeam = null;
  });

  it('redirects to login when the user is not authenticated', async function (assert) {
    invalidateSession();

    await visit(`/manage/teams/subscription/${team._id}`);

    assert.strictEqual(currentURL(), `/login?redirect=%2Fmanage%2Fteams%2Fsubscription%2F5ca78e2160780601008f69e6`);
  });

  it('shows the page to an authenticated user', async function (assert) {
    authenticateSession();

    await visit(`/manage/teams/subscription/${team._id}`);
    expect(this.element.querySelector('.btn-edit')).not.to.exist;

    assert.strictEqual(currentURL(), `/manage/teams/subscription/${team._id}`);
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/teams/subscription/${team._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Subscription']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    ]);
  });

  describe('when the user is not an admin', function () {
    it('renders the support table', async function () {
      authenticateSession();

      await visit(`/manage/teams/subscription/${team._id}`);

      const cells = this.element.querySelectorAll('.manage-field-container-support-list .row-record td');
      expect(cells).to.have.length(3);

      expect(cells[0]).to.have.textContent('1');
      expect(cells[1]).to.have.textContent('Tue Mar 22 2022');
      expect(cells[2]).to.have.textContent('details 1');
    });

    it('renders the invoice table', async function () {
      authenticateSession();

      await visit(`/manage/teams/subscription/${team._id}`);

      const cells = this.element.querySelectorAll('.manage-field-container-invoices-list .row-record td');
      expect(cells).to.have.length(3);

      expect(cells[0]).to.have.textContent('2');
      expect(cells[1]).to.have.textContent('Wed Mar 23 2022');
      expect(cells[2]).to.have.textContent('details 2');
    });
  });

  describe('when the user is an admin', function (hooks) {
    hooks.beforeEach(async function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('can change the support list', async function () {
      authenticateSession();

      await visit(`/manage/teams/subscription/${team._id}`);

      await click('.manage-field-container-support-list .btn-edit');

      await fillIn('.value-hours', '10');

      await click('.btn-submit');

      await waitUntil(() => receivedTeam);

      expect(receivedTeam.team.subscription).to.deep.equal({
        name: 'test',
        domains: ['a.b.c'],
        comment: 'other',
        expire: '2021-01-01T00:00:00.000Z',
        details: 'details',
        monthlySupportHours: 1,
        invoices: [{ hours: 2, date: '2022-03-23T00:22:11.000Z', details: 'details 2' }],
        support: [{ hours: 10, date: '2022-03-22T00:22:11.000Z', details: 'details 1' }],
      });
    });
  });
});
