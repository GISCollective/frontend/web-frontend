/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | dropdown-theme', function (hooks) {
  setupRenderingTest(hooks);

  it('renders auto by default', async function (a) {
    localStorage.setItem('theme', '');
    await render(hbs`<DropdownTheme />`);

    expect(this.element.querySelector('.dropdown-toggle .fa-circle-half-stroke')).to.exist;
  });

  it('renders the sun icon when the preferred is light', async function (a) {
    localStorage.setItem('theme', 'light');
    await render(hbs`<DropdownTheme />`);

    expect(this.element.querySelector('.dropdown-toggle .fa-sun')).to.exist;
  });

  it('renders the moon icon when the preferred is dark', async function (a) {
    localStorage.setItem('theme', 'dark');
    await render(hbs`<DropdownTheme />`);

    expect(this.element.querySelector('.dropdown-toggle .fa-moon')).to.exist;
  });

  it('sets the light mode when the option is selected', async function (a) {
    localStorage.setItem('theme', 'dark');
    await render(hbs`<DropdownTheme />`);

    await click('.dropdown-item-light');

    expect(localStorage.getItem('theme')).to.equal('light');
    expect(this.element.querySelector('.dropdown-toggle .fa-sun')).to.exist;
  });
});
