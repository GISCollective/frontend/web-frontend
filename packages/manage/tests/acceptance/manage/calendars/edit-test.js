/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest, changeSelect } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitUntil, triggerEvent, fillIn } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/calendars/edit', function (hooks) {
  setupApplicationTest(hooks);

  let calendar;
  let server;
  let defaultPicture;
  let receivedCalendar;
  let receivedPicture;

  hooks.beforeEach(function () {
    server = new TestServer();
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    const user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIconSet("2");
    server.testData.storage.addDefaultMap();
    calendar = server.testData.storage.addDefaultCalendar();

    receivedCalendar = null;

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/calendars/${calendar._id}`, (request) => {
      receivedCalendar = JSON.parse(request.requestBody);
      receivedCalendar.calendar._id = calendar._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({ calendar })];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/calendars/edit/${calendar._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fcalendars%2Fedit%2F5cc8dc1038e882010061221');
  });

  it('should contain all the breadcrumbs links', async function () {
    authenticateSession();
    await visit(`/manage/calendars/edit/${calendar._id}`);
    await PageElements.waitEditorJs(this.element);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'test calendar', 'Edit']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
      '/manage/dashboards/calendar/5cc8dc1038e882010061221',
    ]);
  });

  it('can change the article', async function () {
    authenticateSession();
    await visit(`/manage/calendars/edit/${calendar._id}`);

    await click('.row-article .btn-edit');

    await PageElements.waitEditorJs(this.element);

    await fillIn('.ce-paragraph', 'new description');
    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
    await click('.btn-submit');

    await waitUntil(() => receivedCalendar, { timeout: 3000 });

    expect(receivedCalendar.calendar.article.blocks).to.deep.equal([
      { type: 'header', data: { text: calendar.name, level: 1 } },
      { type: 'paragraph', data: { text: 'new description' } },
    ]);
  });

  it('can change the cover', async function () {
    authenticateSession();

    await visit(`/manage/calendars/edit/${calendar._id}`);

    await click('.row-cover .btn-edit');

    const blob = server.testData.create.pngBlob();
    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    await waitUntil(() => receivedCalendar);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'calendar', id: '5cc8dc1038e882010061221' },
      data: {},
      disableOptimization: false,
    });

    expect(receivedCalendar.calendar.cover).to.equal('1');

    await server.waitAllRequests();
    await PageElements.wait(1000);
  });

  it('can change the map', async function () {
    let map = server.testData.create.map('calendar-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/calendars/edit/${calendar._id}`);
    await PageElements.waitEditorJs(this.element);

    await click('.row-map .btn-edit');
    this.element.querySelector('.row-map input.enable-map').checked = true;
    await triggerEvent('.row-map input.enable-map', 'change');

    this.element.querySelector('.row-map select').value = 'calendar-map';
    await triggerEvent('select', 'change');
    await click('.row-map .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedCalendar.calendar.map).to.deep.equal({
      isEnabled: true,
      map: 'calendar-map',
    });
  });

  it('can change the record name', async function () {
    authenticateSession();
    await visit(`/manage/calendars/edit/${calendar._id}`);

    expect(this.element.querySelector('.row-record-name .view-container').textContent.trim()).to.equal(
      'this calendar does not have a custom record name',
    );

    await click('.row-record-name .btn-edit');
    await fillIn('.row-record-name input', 'volunteering opportunity');
    await click('.btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedCalendar.calendar.recordName).to.equal('volunteering opportunity');
  });

  it('can change the icon sets', async function () {
    let map = server.testData.create.map('calendar-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/calendars/edit/${calendar._id}`);

    await click('.row-icon-sets .btn-edit');

    await changeSelect('.select-use-default-values', '1');

    await click('.row-icon-sets .btn-add-item');
    await click('.row-icon-sets .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedCalendar.calendar.iconSets).to.deep.equal({
      useCustomList: true,
      list: ['5ca7b702ecd8490100cab96f', '2'],
    });
  });

  it('can add a new attribute', async function () {
    let map = server.testData.create.map('calendar-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/calendars/edit/${calendar._id}`);

    await click('.row-attributes .btn-edit');

    expect(this.element.querySelectorAll('.input-icon-attribute')).to.have.length(1);

    await click('.btn-add-attribute');

    const attributes = this.element.querySelectorAll('.input-icon-attribute');

    await fillIn(attributes[1].querySelector('.input-attribute-name'), 'test-attribute');

    await click('.row-attributes .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedCalendar.calendar.attributes).to.deep.equal([
      {
        name: 'key1',
        displayName: '',
        help: '',
        type: 'long text',
        options: '',
        isPrivate: false,
        from: {},
        isInherited: false,
        isRequired: false,
      },
      {
        name: 'test-attribute',
        displayName: '',
        help: '',
        type: 'short text',
        options: '',
        isPrivate: false,
        from: {},
        isInherited: false,
        isRequired: false,
      },
    ]);
  });

  it('can not change the team', async function () {
    let map = server.testData.create.map('calendar-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/calendars/edit/${calendar._id}`);
    await PageElements.waitEditorJs(this.element);

    expect(this.element.querySelector('.container-group-team .btn-edit')).not.to.exist;
    expect(this.element.querySelector('.container-group-team .value').textContent.trim()).to.equal('Open Green Map');
  });
});
