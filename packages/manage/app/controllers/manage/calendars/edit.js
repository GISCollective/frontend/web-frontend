/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action, set } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageCalendarsEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service store;
  @service router;

  @tracked editableField;
  @tracked savingField;
  @tracked allTeams = false;
  @tracked _icons;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.calendar',
        model: this.model.calendar.get('id'),
        text: this.model.calendar.name,
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  @action
  async saveField(field, value) {
    set(this.model.calendar, field, value);

    return this.save();
  }

  @action
  saveIconSets(title, value, useCustomList) {
    this.editableField = title;
    this.model.calendar.iconSets.list = value;
    this.model.calendar.iconSets.useCustomList = useCustomList;

    return this.save();
  }

  @action
  saveArticle(name, article) {
    this.model.calendar.name = name;
    this.model.calendar.article = article;

    return this.save();
  }

  @action
  async save() {
    this.savingField = this.editableField;

    let result;

    try {
      result = await this.model.calendar.save();
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.editableField = '';
    this.savingField = '';

    return result;
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.calendar.rollbackAttributes();
  }
}
