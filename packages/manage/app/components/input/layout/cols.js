/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputLayoutColsComponent extends Component {
  @tracked _cols;

  get cols() {
    if (this._cols) {
      return this._cols;
    }

    return this.args.page?.layoutContainers?.[this.args.containerIndex]?.rows?.[this.args.rowIndex]?.cols ?? [];
  }

  get lastIndex() {
    if (!this.cols) {
      return 0;
    }

    return this.cols.length - 1;
  }

  @action
  select(index, event) {
    const col = this.cols[index];

    this.args.onSelect?.('col', [this.args.containerIndex, this.args.rowIndex, index], col);
    event.preventDefault();
  }

  @action
  addCol(event) {
    this.args.page?.addLayout(this.args.containerIndex, this.args.rowIndex, this.cols.length);
    this._cols = this.args.page?.layoutContainers?.[this.args.containerIndex].rows[this.args.rowIndex].cols;

    event.preventDefault();
  }

  @action
  deleteCol(index, event) {
    this.args.page?.deleteCol(this.args.containerIndex, this.args.rowIndex, index);
    this._cols = this.args.page?.layoutContainers?.[this.args.containerIndex].rows[this.args.rowIndex].cols;

    if (this.cols.length == 0) {
      this.args.page?.deleteRow(this.args.containerIndex, this.args.rowIndex);
      this.args.onRowRemove?.();
    }

    event.preventDefault();
  }
}
