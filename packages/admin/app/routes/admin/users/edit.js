/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedAdminRoute from '../../base/authenticated-admin-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class AdminUsersEditRoute extends AuthenticatedAdminRoute {
  @service preferences;

  model(params) {
    return hash({
      profile: this.store.findRecord('user-profile', params.id),
      user: this.store.findRecord('user', params.id),
      detailedLocation: this.preferences.requestPreference(`profile.detailedLocation`),
    });
  }
}
