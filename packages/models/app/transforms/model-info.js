/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { getOwner } from '@ember/application';
import { tracked } from '@glimmer/tracking';

export default class ModelInfoTransform extends Transform {
  deserialize(serialized) {
    let applicationInstance = getOwner(this);
    const store = applicationInstance.lookup('service:store');

    return new ModelInfo(serialized, store);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class ModelInfo {
  @tracked createdOn = null;
  @tracked author = null;
  @tracked lastChangeOn = null;
  @tracked originalAuthor = null;
  @tracked changeIndex = null;

  @tracked originalAuthorUser;
  @tracked authorUser;
  @tracked originalAuthorProfile;
  @tracked authorProfile;

  constructor(value, store) {
    this.store = store;

    if (!value || typeof value != 'object') {
      value = {};
    }

    if (value.author) {
      this.author = value.author;
    }

    if (value.changeIndex) {
      this.changeIndex = value.changeIndex;
    }

    if (value.createdOn) {
      this.createdOn = new Date(Date.parse(value.createdOn));
    }

    if (value.lastChangeOn) {
      this.lastChangeOn = new Date(value.lastChangeOn);
    }

    if (value.originalAuthor) {
      this.originalAuthor = value.originalAuthor;
    }
  }

  get isAnonymous() {
    return this.originalAuthor == '@anonymous';
  }

  async fetchUser(key) {
    if (!this[key]) {
      return;
    }

    this[`${key}User`] = this.store.peekRecord('user', this[key]);

    if (!this[`${key}User`]) {
      try {
        this[`${key}User`] = await this.store.findRecord('user', this[key]);
      } catch (err) {
        this[`${key}User`] = null;
      }
    }
  }

  async fetchProfile(key) {
    if (!this[key]) {
      return;
    }

    this[`${key}Profile`] = this.store.peekRecord('user-profile', this[key]);

    if (!this[`${key}Profile`]) {
      try {
        this[`${key}Profile`] = await this.store.findRecord('user-profile', this[key]);
      } catch (err) {
        this[`${key}Profile`] = null;
      }
    }
  }

  async fetch() {
    const promises = [];

    promises.push(this.fetchUser('originalAuthor'));
    promises.push(this.fetchUser('author'));

    promises.push(this.fetchProfile('originalAuthor'));
    promises.push(this.fetchProfile('author'));

    await Promise.all(promises);
  }

  toJSON() {
    const result = {};

    if (this.createdOn) {
      try {
        result.createdOn = this.createdOn.toISOString();
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }

    if (this.lastChangeOn) {
      try {
        result.lastChangeOn = this.lastChangeOn.toISOString();
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }

    if (this.author && (typeof this.author === 'string' || this.author instanceof String)) {
      result.author = this.author;
    }

    if (this.originalAuthor && (typeof this.originalAuthor === 'string' || this.originalAuthor instanceof String)) {
      result.originalAuthor = this.originalAuthor;
    }

    if (this.changeIndex && !isNaN(parseInt(this.changeIndex))) {
      result.changeIndex = parseInt(this.changeIndex);
    }

    return result;
  }
}
