/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { helper } from '@ember/component/helper';

export function sum(params) {
  return params.reduce((a, b) => {
    return parseFloat(a) + parseFloat(b);
  }, 0);
}

export default helper(sum);
