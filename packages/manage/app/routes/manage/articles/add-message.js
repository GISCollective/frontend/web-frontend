/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class AddArticleMessageRoute extends AuthenticatedRoute {
  modelsToClear = ['article'];

  queryParams = {
    newsletter: { refreshModel: true },
  };

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const newsletter = await this.store.findRecord('newsletter', params.newsletter);

    return hash({
      team: newsletter.visibility.fetchTeam(),
      newsletter,
    });
  }
}
