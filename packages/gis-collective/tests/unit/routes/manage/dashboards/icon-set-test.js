/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module, test } from 'qunit';
import { setupTest } from 'ogm/tests/helpers';

module('Unit | Route | manage/dashboards/icon-set', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:manage/dashboards/icon-set');
    assert.ok(route);
  });
});
