/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | container', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the sm size by default', async function (a) {
    this.set('value', {
      style: {
        md: {
          height: '233px',
          width: '122px',
        },
        lg: {
          height: '344px',
          width: '23%',
        },
        sm: {
          height: '122px',
        },
      },
      classes: ['col-8'],
    });

    await render(hbs`<Container @value={{this.value}}>
      test
    </Container>`);

    const id = this.element.querySelector('.page-col-base-container').attributes.getNamedItem('id').value;

    expect(this.element.querySelector('style').textContent).to.contain(`#${id}`);
    expect(this.element.querySelector('style').textContent).to.contain(
      `#${id} {height: 233px;width: 122px;flex: 0 0 auto}`,
    );
    expect(this.element.querySelector('style').textContent).to.contain(
      `#${id} {height: 344px;width: 23%;flex: 0 0 auto}`,
    );
    expect(this.element.querySelector('style').textContent).to.contain(`#${id} {height: 122px}`);
  });
});
