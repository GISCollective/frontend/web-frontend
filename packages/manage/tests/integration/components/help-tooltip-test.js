/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | help-tooltip', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<HelpTooltip>message</HelpTooltip>`);
    expect(this.element.querySelector('.fa-circle-question')).to.exist;
    expect(this.element.querySelector('.content').textContent.trim()).to.equal('message');
  });
});
