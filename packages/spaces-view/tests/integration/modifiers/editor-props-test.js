/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module, test } from 'qunit';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Modifier | editor-props', function (hooks) {
  setupRenderingTest(hooks);

  // TODO: Replace this with your real tests.
  // test('it renders', async function (assert) {
  //   this.set('value', {});

  //   await render(hbs`<div {{editor-props this.value}} />`);

  //   assert.dom(this.element).hasText('1234');
  // });
});
