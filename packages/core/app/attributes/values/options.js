/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import TextValue from './text';

export default class OptionsText extends TextValue {
  constructor(name, key, options, value) {
    super(name, key, 'options', value);
    this.options = options;
  }
}
