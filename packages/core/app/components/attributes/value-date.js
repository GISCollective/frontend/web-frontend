/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { Datepicker } from 'vanillajs-datepicker';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class AttributesValueDateComponent extends Component {
  @service clickOutside;
  @tracked _value = null;
  @tracked showCalendar = false;

  get value() {
    if (this._value !== null) {
      return this._value;
    }

    return this.args.value;
  }

  set value(newValue) {
    this._value = newValue;

    this.args.onChange?.(this.value);
  }

  @action
  toggleCalendar() {
    this.showCalendar = !this.showCalendar;

    if (this.showCalendar) {
      later(() => {
        this.clickOutside.subscribe(this.element, () => {
          this.showCalendar = false;
        });
      });
    }
  }

  @action
  setupElement(element) {
    this.element = element;
  }

  @action
  change(event) {
    const localAsGMT = new Date(event.detail.date.getTime() - event.detail.date.getTimezoneOffset() * 60 * 1000);

    this.value = localAsGMT.toISOString().split('T')[0];
  }

  @action
  setupCalendar(element) {
    this.datepicker = new Datepicker(element, {
      buttonClass: 'btn',
      format: 'yyyy-mm-dd',
    });
  }
}
