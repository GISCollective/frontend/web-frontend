/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/team', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let space;
  let team;

  hooks.beforeEach(function () {
    const service = this.owner.lookup('service:notifications');
    service.ask = () => 'yes';

    server = new TestServer();
    team = server.testData.create.team();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultIcon();

    space = server.testData.storage.addDefaultSpace();

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.get(`/mock-server/layouts/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('redirects to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/${team._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2F5ca78e2160780601008f69e6');
  });

  it('shows the dashboard page when the user is authenticated', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/${team._id}`);

    expect(currentURL()).to.equal(`/manage/dashboards/${team._id}`);
    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('header').textContent.trim()).to.contain(team.name);
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/${team._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);
  });

  it('does not display the subscription details', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/${team._id}`);

    expect(this.element.querySelector('.manage-team-subscription')).not.to.exist;
  });

  describe('when the team has a subscription', function (hooks) {
    hooks.beforeEach(function () {
      team.subscription = {
        name: 'test',
        expire: '2021-01-01T00:00:00.000Z',
        domains: ['a.b.c'],
        comment: 'other',
        monthlySupportHours: 1,
        details: 'Details',
        support: [
          {
            hours: 1,
            date: '2022-03-22T00:22:11.000Z',
            details: 'details 1',
          },
        ],
        invoices: [
          {
            hours: 2,
            date: '2022-03-23T00:22:11.000Z',
            details: 'details 2',
          },
        ],
      };
    });

    it('display the subscription details', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelector('.manage-team-subscription h4')).to.have.textContent('Subscription details');
      expect(this.element.querySelector('.manage-team-subscription h6')).to.have.textContent('Name');
      expect(this.element.querySelector('.manage-team-subscription .stat-value')).to.have.textContent('test');
    });
  });

  describe('when the team has no maps', function (hooks) {
    it('should display the option to add a map', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelector('.alert-info')).to.exist;
      expect(this.element.querySelector('.btn-add-map')).to.exist;
      expect(this.element.querySelector('.alert-info a')).to.have.attribute('href', '/manage/maps/add');
    });

    it('should not display the option to add a map when the team is a publisher', async function () {
      team.isPublisher = true;
      server.testData.storage.addTeam(team);

      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelector('.alert-info')).not.to.exist;
      expect(this.element.querySelector('.btn-add-map')).not.to.exist;
    });

    it('does not display the stats', async function () {
      team.isPublisher = true;
      server.testData.storage.addTeam(team);

      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelector('.manage-team-stats')).not.to.exist;
    });
  });

  describe('when the team has a map', function (hooks) {
    let map;
    let picture;

    hooks.beforeEach(function () {
      map = server.testData.create.map('5ca89e27ef1f7e010007f4af');
      map.canEdit = true;
      map.visibility.team = team._id;

      picture = server.testData.create.picture();

      server.testData.storage.addMap(map);
      server.testData.storage.addPicture(picture);

      server.testData.storage.addStat(`map-views-team-${team._id}`, 20);
      server.testData.storage.addStat(`campaign-contributions-team-${team._id}`, 30);
      server.testData.storage.addStat(`campaign-answer-contributors-team-${team._id}`, 40);
    });

    it('should display the map as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-map').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-map .title').textContent.trim()).to.equal(map.name);
    });

    it('should be able to delete the map', async function () {
      authenticateSession();
      let deletedSite = false;
      server.delete(`/mock-server/maps/${map._id}`, () => {
        deletedSite = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-map .dropdown-toggle');
      await click('.card-dashboard-map .btn-delete');

      expect(deletedSite).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-map')).not.to.exist;
    });

    it('displays the stats', async function () {
      team.isPublisher = true;
      server.testData.storage.addTeam(team);

      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelector('.manage-team-stats')).to.exist;
    });
  });

  describe('when the team has a space', function (hooks) {
    let picture;

    hooks.beforeEach(function () {
      picture = server.testData.create.picture();

      server.testData.storage.addPicture(picture);
    });

    it('should display the space as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-space').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-space .title').textContent.trim()).to.equal('GISCollective');
    });

    it('should be able to delete the space', async function () {
      authenticateSession();
      let deleted = false;

      server.delete(`/mock-server/spaces/${space._id}`, () => {
        deleted = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-space .dropdown-toggle');
      await click('.card-dashboard-space .btn-delete');

      expect(deleted).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-space')).not.to.exist;
    });
  });

  describe('when the team has a campaign', function (hooks) {
    let campaign;

    hooks.beforeEach(function () {
      campaign = server.testData.create.campaign();
      campaign.canEdit = true;
      campaign.visibility.isPublic = false;
      campaign.visibility.team = team._id;

      server.testData.storage.addCampaign(campaign);
    });

    it('should display the campaign as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-campaign').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-campaign .title').textContent.trim()).to.equal(campaign.name);
    });
  });

  describe('when the team has a newsletter', function (hooks) {
    let newsletter;

    hooks.beforeEach(function () {
      newsletter = server.testData.create.newsletter();
      newsletter.canEdit = true;
      newsletter.visibility.isPublic = false;
      newsletter.visibility.team = team._id;

      server.testData.storage.addNewsletter(newsletter);
    });

    it('should display the newsletter as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-newsletter').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-newsletter .title').textContent.trim()).to.equal(
        newsletter.name,
      );
    });
  });

  describe('when the team has an iconset', function (hooks) {
    let iconSet;

    hooks.beforeEach(function () {
      iconSet = server.testData.create.iconSet('5ca89e27ef1f7e010007f4af');
      iconSet.canEdit = true;
      iconSet.visibility.team = team._id;

      server.testData.storage.addIconSet(iconSet);
    });

    it('should display the iconSet as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-icon-set').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-icon-set .title').textContent.trim()).to.equal(iconSet.name);
    });

    it('should be able to delete the iconset', async function () {
      authenticateSession();
      let deletedSite = false;
      server.delete(`/mock-server/iconsets/${iconSet._id}`, () => {
        deletedSite = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-icon-set .dropdown-toggle');
      await click('.card-dashboard-icon-set .btn-delete');

      expect(deletedSite).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-icon-set')).not.to.exist;
    });
  });

  describe('when the team has a basemap', function (hooks) {
    let basemap;

    hooks.beforeEach(function () {
      basemap = server.testData.create.baseMap('5ca89e27ef1f7e010007f4af');
      basemap.canEdit = true;
      basemap.visibility.team = team._id;

      server.testData.storage.addBaseMap(basemap);
    });

    it('should display the basemap as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-basemap').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-basemap .title').textContent.trim()).to.equal(basemap.name);
    });

    it('should be able to delete the basemap', async function () {
      authenticateSession();
      let deletedSite = false;
      server.delete(`/mock-server/basemaps/${basemap._id}`, () => {
        deletedSite = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-basemap .dropdown-toggle');
      await click('.card-dashboard-basemap .btn-delete');

      expect(deletedSite).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-basemap')).not.to.exist;
    });
  });

  describe('when the team has a calendar', function (hooks) {
    let calendar;

    hooks.beforeEach(function () {
      calendar = server.testData.create.calendar('5ca89e27ef1f7e010007f4af');
      calendar.canEdit = true;
      calendar.visibility.team = team._id;

      server.testData.storage.addCalendar(calendar);
    });

    it('should display the calendar as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-calendar').length).to.equal(1);

      expect(this.element.querySelector('.card-dashboard-calendar .title').textContent.trim()).to.equal(calendar.name);
    });

    it('should be able to delete the calendar', async function () {
      authenticateSession();
      let deletedSite = false;
      server.delete(`/mock-server/calendars/${calendar._id}`, () => {
        deletedSite = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-calendar .dropdown-toggle');
      await click('.card-dashboard-calendar .btn-delete');

      expect(deletedSite).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-calendar')).not.to.exist;
    });
  });

  describe('when the team has a data binding', function (hooks) {
    let dataBinding;

    hooks.beforeEach(function () {
      dataBinding = server.testData.create.dataBinding('5ca89e27ef1f7e010007f4af');
      dataBinding.canEdit = true;
      dataBinding.visibility.team = team._id;

      server.testData.storage.addDataBinding(dataBinding);
    });

    it('should display the record as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-data-binding').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-data-binding .title').textContent.trim()).to.equal(
        dataBinding.name,
      );
    });

    it('should be able to delete the record', async function () {
      authenticateSession();
      let deletedDataBindings = false;
      server.delete(`/mock-server/databindings/${dataBinding._id}`, () => {
        deletedDataBindings = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-data-binding .dropdown-toggle');
      await click('.card-dashboard-data-binding .btn-delete');

      expect(deletedDataBindings).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-data-binding')).not.to.exist;
    });

    it('navigates to the page content edit when the edit button is pressed', async function () {
      server.testData.storage.addDefaultSpace();
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-data-binding .dropdown-toggle');
      await click('.card-dashboard-data-binding .dropdown-item-edit');

      expect(currentURL()).to.equal('/manage/databindings/edit/5ca89e27ef1f7e010007f4af');
    });
  });

  describe('when the team has a presentation', function (hooks) {
    let presentation;

    hooks.beforeEach(function () {
      presentation = server.testData.create.presentation('5ca89e27ef1f7e010007f4af');
      presentation.canEdit = true;
      presentation.visibility.team = team._id;

      server.testData.storage.addPresentation(presentation);
    });

    it('should display the presentation as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-presentation').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-presentation .title').textContent.trim()).to.equal(
        presentation.name,
      );
    });

    it('should be able to delete the presentation', async function () {
      authenticateSession();
      let deletedPresentation = false;
      server.delete(`/mock-server/presentations/${presentation._id}`, () => {
        deletedPresentation = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-presentation .dropdown-toggle');
      await click('.card-dashboard-presentation .btn-delete');

      expect(deletedPresentation).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-presentation')).not.to.exist;
    });

    it('should be able to edit a presentation', async function () {
      authenticateSession();
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-presentation .dropdown-toggle');
      await click('.card-dashboard-presentation .dropdown-item-edit');
      expect(currentURL()).to.equal('/manage/presentations/5ca89e27ef1f7e010007f4af/content');
    });
  });

  describe('when the team has an article', function (hooks) {
    let article;

    hooks.beforeEach(function () {
      article = server.testData.create.article('5ca89e27ef1f7e010007f4af');
      article.canEdit = true;
      article.visibility.team = team._id;

      server.testData.storage.addArticle(article);

      server.get(`/mock-server/articles/_/categories`, () => {
        return [200, { 'Content-Type': 'application/json' }, '{ "categories": ["category 1", "category 2"] }'];
      });
    });

    it('should display a link to the articles list', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      await click('.btn-show-articles');

      expect(currentURL()).to.equal(`/manage/dashboards/articles/${team._id}`);
    });
  });

  describe('when there is a map and survey', function(hooks) {
    let map;
    let campaign;
    let picture;

    hooks.beforeEach(function () {
      campaign = server.testData.storage.addDefaultCampaign();
      map = server.testData.storage.addDefaultMap();
      picture = server.testData.storage.addDefaultPicture();

      server.testData.storage.addStat(`map-views-team-${team._id}`, 20);
      server.testData.storage.addStat(`campaign-contributions-team-${team._id}`, 30);
      server.testData.storage.addStat(`campaign-answer-contributors-team-${team._id}`, 40);
    });

    it("renders a toolbar", async function(hooks) {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      const btns = [ ...this.element.querySelectorAll(".btn-group-name")].map(a => a.textContent.trim())

      expect(btns).to.deep.equal([ 'all', 'maps', 'surveys', 'spaces' ]);
    });

    it("only shows the surveys when the survey button is selected", async function(hooks) {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      const btns = [ ...this.element.querySelectorAll(".btn-group-name")];
      await click(btns[2]);

      expect(this.element.querySelector(".card-dashboard-map")).not.to.exist;
      expect(this.element.querySelector(".card-dashboard-campaigns")).to.exist;
    });
  });
});
