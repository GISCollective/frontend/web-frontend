/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, waitFor, fillIn, waitUntil, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import PageElements from "core/test-support/page-elements";

describe("Integration | Component | editor/blocks", function (hooks) {
  setupRenderingTest(hooks);

  it("shows the blocks without other options", async function () {
    this.set("value", [
      {
        type: "header",
        data: {
          text: "title1",
          level: 1,
        },
      },
    ]);

    await render(hbs`<Editor::Blocks @value={{this.value}} />`);
    await PageElements.waitEditorJs(this.element);

    expect(this.element.querySelector(".ce-header").innerHTML.trim()).to.equal("title1");
  });

  it("can change the value", async function () {
    this.set("value", [
      {
        type: "header",
        data: {
          text: "title1",
          level: 1,
        },
      },
    ]);

    let value;
    this.set("change", (newValue) => {
      value = newValue;

      value.time = 0;
      value.version = 0;
    });

    await render(hbs`<Editor::Blocks @value={{this.value}} @onChange={{this.change}} />`);
    await waitFor(".editor-is-ready", { timeout: 3000 });

    await fillIn("h1", "newValue");

    await waitUntil(() => value);

    expect(value).to.deep.equal([{ type: "header", data: { level: 1, text: "newValue" } }]);
  });
});
