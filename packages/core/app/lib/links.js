/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { toPagePath } from './slug';

export function resolveLinkObject(value, currentSpace, defaultSpace, query, variables) {
  if (!value) {
    return;
  }

  let to = '';

  if (typeof value == 'string') {
    to = value;
  }

  if (value?.route) {
    return value;
  }

  if (value?.url) {
    return value;
  }

  if (typeof value == 'object') {
    to = value.to || value.pageId || value.url || value.path;
  }

  if (!to) {
    return {};
  }

  if (to.indexOf('#') === 0) {
    return {
      anchor: to,
    };
  }

  if (to.indexOf('http:') === 0 || to.indexOf('https:') === 0) {
    return {
      url: to,
    };
  }

  if (to.indexOf('/') === 0) {
    return {
      path: replaceVars(to, query, variables),
    };
  }

  const slug = resolveDestination(to, currentSpace, defaultSpace);
  to = toPagePath(slug);

  return {
    path: replaceVars(to, query, variables),
  };
}

function replaceVars(to, query, variables) {
  if (query) {
    to += `?${query}`;
  }

  if (variables) {
    for (const key in variables) {
      to = to.replace(`:${key}-id`, variables[key]);
    }
  }

  return to;
}

export function resolveDestination(value, currentSpace, defaultSpace) {
  const space = currentSpace ?? defaultSpace;

  if (!space) {
    return value;
  }

  const pagesMap = space.getCachedPagesMap();

  const match = Object.entries(pagesMap).find((a) => a[0] == value || a[1] == value);

  return match?.[0];
}
