/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class InputIdListComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? [];
  }

  triggerChange() {
    return this.args.onChange?.([...this._value]);
  }

  @action
  addItem() {
    this._value = [...this.value, ""];
    this.triggerChange();
  }

  @action
  changeItem(index, id) {
    if (!this._value) {
      this._value = this.value;
    }

    this._value.splice(index, 1, id);

    this.triggerChange();
  }

  @action
  removeItem(index) {
    const value = [...this.value];

    value.splice(index, 1);

    this._value = value;

    this.triggerChange();
  }
}
