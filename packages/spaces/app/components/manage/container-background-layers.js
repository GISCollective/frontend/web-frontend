/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { later } from "@ember/runloop";

export default class ManageContainerBackgroundLayersComponent extends Component {
  @tracked containers;
  @tracked layout;

  get showLayerTitles() {
    return this.layout.layers.count > 1;
  }

  @action
  changeContainer(index, newData, newType) {
    const col = this.containers[index];

    col.type = newType;
    col.data = newData;

    this.args.onChange(col.name, newData, newType);
  }

  @action
  setup() {
    if (!this.args.selectedContainer) {
      return;
    }

    later(() => {
      this.containers = [this.args.value.upsertPageColByName(this.args.selectedContainer.name)];

      this.layout = this.args.value.layoutContainers.find((a) => a.name == this.args.selectedContainer.name);

      for (let i = 1; i < this.layout.layers.count; i++) {
        this.containers.push(this.args.value.upsertPageColByName(`${this.args.selectedContainer.name}.layer-${i}`));
      }
    });
  }
}
