/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { find, click, render, settled, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/button',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders async success', async function () {
    let deferred = { resolve: undefined };
    this.action = () => new Promise((resolve) => (deferred.resolve = resolve));

    await render(hbs`
      <Input::Button @action={{this.action}} data-test-foo>
      some text
      </Input::Button>
      `);
    click('[data-test-foo]');

    await waitFor(".fa-spin");

    deferred.resolve();
    await settled();


    expect(find('[data-test-foo]').innerText.trim()).to.equal('some text');
  });

  it('renders async failure', async function () {
    let deferred = { reject: undefined };
    this.action = () => new Promise((_, reject) => (deferred.reject = reject));

    await render(hbs`
      <Input::Button @action={{this.action}} data-test-foo>
      some text
      </Input::Button>
      `);
    click('[data-test-foo]');

    await waitFor(".fa-spin");

    deferred.reject();
    await settled();


    expect(this.element.querySelector('.svg-inline--fa')).to.have.class(
      'fa-triangle-exclamation'
    );
    await settled();
  });
});
