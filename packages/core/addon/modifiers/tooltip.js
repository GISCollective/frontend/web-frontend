import { buildWaiter } from '@ember/test-waiters';
import { modifier } from 'ember-modifier';
import { Tooltip } from 'bootstrap';

export default modifier(function domChanged(element, [title, ...args]) {
  const tooltip = new Tooltip(element, {
    animation: true,
    html: true,
    title,
    delay: { show: 200, hide: 0 },
  });

  return () => {
    tooltip.dispose();
  };
});
