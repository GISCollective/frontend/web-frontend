/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | view/pictures', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let pictureData;

  hooks.before(function () {
    server = new TestServer();

    pictureData = server.testData.storage.addDefaultPicture();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders a picture with links when the destination is set', async function (a) {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      pictures: [picture],
    });

    this.set('value', {
      modelKey: 'pictures',
      data: {
        ids: [picture.id],
        model: 'picture',
        options: [],
        container: {},
        picture: {},
        links: [
          { link: { url: 'https://giscollective.com' }, newTab: true },
          {},
        ],
      },
    });

    await render(
      hbs`<View::Pictures @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a')).to.exist;
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'https://giscollective.com',
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'target',
      '_blank',
    );
  });

  it('does not render links with a pictures when the destination is not set', async function (a) {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      pictures: [picture],
    });

    this.set('value', {
      modelKey: 'pictures',
      data: {
        ids: [picture.id],
        model: 'picture',
        options: [],
        container: {},
        picture: {},
        links: [],
      },
    });

    await render(
      hbs`<View::Pictures @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders the container style when it is set', async function (a) {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      pictures: [picture],
    });

    this.set('value', {
      modelKey: 'pictures',
      data: {
        ids: [picture.id, picture.id],
        model: 'picture',
        options: [],
        container: {
          classes: [
            'align-items-start',
            'align-items-md-end',
            'align-items-lg-center',
          ],
        },
        picture: {},
        links: [{ url: 'https://giscollective.com' }, {}],
      },
    });

    await render(
      hbs`<View::Pictures @value={{this.value}} @model={{this.model}} />`,
    );

    expect(
      this.element.querySelector('.page-col-base-container'),
    ).to.have.classes(
      'bg-undefined align-items-start align-items-md-end align-items-lg-center container-align-undefined page-col-picture-list',
    );
  });

  it('renders the image classes and styles', async function (a) {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      pictures: [picture],
    });

    this.set('value', {
      modelKey: 'pictures',
      data: {
        source: {
          ids: [picture.id, picture.id],
          model: 'picture',
        },
        options: [],
        container: {},
        picture: {
          sizing: 'custom height',
          customStyle: {
            md: {
              height: '235px',
            },
            lg: {
              height: '236px',
            },
            sm: {
              height: '234px',
            },
          },
          classes: ['mb-5', 'mb-md-5'],
        },
        links: [{ url: 'https://giscollective.com' }, {}],
      },
    });

    await render(
      hbs`<View::Pictures @value={{this.value}} @model={{this.model}} />`,
    );

    expect(
      this.element.querySelector('.picture-with-options img'),
    ).to.have.classes('mb-5', 'mb-md-5');

    const combinedStyles = [...this.element.querySelectorAll('style')]
      .map((a) => a.textContent)
      .join(' ');

    expect(combinedStyles).to.contain('{height: 235px}');
  });
});
