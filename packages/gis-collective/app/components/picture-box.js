/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class PictureBoxComponent extends Component {
  image() {
    if (!this.args.src) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${this.src}')`);
  }
}
