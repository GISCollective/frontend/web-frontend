/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class AdminAccessController extends Controller {
  @service fastboot;

  get isMultiProjectMode() {
    return this.model.isMultiProjectMode.value == 'true';
  }

  set isMultiProjectMode(value) {
    this.model.isMultiProjectMode.value = `${value}`;
  }

  get allowManageWithoutTeams() {
    return this.model.allowManageWithoutTeams.value == 'true';
  }

  set allowManageWithoutTeams(value) {
    this.model.allowManageWithoutTeams.value = `${value}`;
  }

  get isSaving() {
    return (
      this.fastboot.isFastBoot ||
      Object.keys(this.model)
        .map((a) => this.model[a].isSaving)
        .find((a) => a)
    );
  }

  get hasUpdate() {
    return !this.fastboot.isFastBoot && this.dirtyValues.length > 0;
  }

  get dirtyValues() {
    return Object.keys(this.model)
      .map((a) => this.model[a])
      .filter((a) => a.hasDirtyAttributes);
  }

  @action
  save() {
    return Promise.all(this.dirtyValues.map((a) => a.save()));
  }
}
