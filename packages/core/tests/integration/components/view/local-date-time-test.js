/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/local-date-time', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::LocalDateTime />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a date object value as a local date', async function () {
    this.set('value', Date.parse('2019-01-01T00:00:00.000Z'));

    await render(hbs`<View::LocalDateTime @value={{this.value}} />`);

    expect(this.element.querySelector('.date').textContent.trim()).to.equal('1/1/2019');
    expect(this.element.querySelector('.time').textContent.trim()).to.contain(`00:00 AM`);
  });

  it('renders a date string value as a local date', async function () {
    this.set('value', '2019-01-01T00:00:00.000Z');

    await render(hbs`<View::LocalDateTime @value={{this.value}} />`);

    expect(this.element.querySelector('.date').textContent.trim()).to.equal('1/1/2019');
    expect(this.element.querySelector('.time').textContent.trim()).to.contain(`00:00 AM`);
  });
});
