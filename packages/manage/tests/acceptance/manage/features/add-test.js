/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, fillIn, triggerEvent, waitFor } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/features/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedFeature;
  let map;
  let feature;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();

    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultTeam();

    server.testData.storage.addDefaultBaseMap('000000111111222222333333');
    server.testData.storage.addDefaultBaseMap('000000111111222222444444');

    map = server.testData.storage.addDefaultMap();
    feature = server.testData.storage.addDefaultFeature();

    server.testData.storage.addDefaultMap('000000000000000000000002');
    server.testData.storage.addDefaultUser(false, '5b8a59caef739394031a3f67');

    receivedFeature = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/features/add`);
    await PageElements.waitEditorJs(this.element);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Ffeatures%2Fadd');
  });

  describe('when an user is authenticated', function (hooks) {
    hooks.beforeEach(() => {
      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/features/add`);
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal('/manage/features/add');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal('New feature');

      expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to.exist;

      expect(this.element.querySelector('.row-map label').textContent.trim()).to.contain('Map');
      expect(this.element.querySelector('.row-map select')).to.exist;
      expect(this.element.querySelectorAll('.row-map select option').length).to.equal(3);

      expect(this.element.querySelector('.row-geometry label').textContent.trim()).to.contain('Geometry');
      expect(this.element.querySelector('.row-geometry .map-container')).to.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.have.attribute('disabled', '');
    });

    describe('when the server request is successfully', function (hooks) {
      hooks.beforeEach(function () {
        server.post('/mock-server/features', (request) => {
          receivedFeature = JSON.parse(request.requestBody);
          receivedFeature.feature._id = feature._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedFeature)];
        });

        server.put('/mock-server/features/:id', (request) => {
          receivedFeature = JSON.parse(request.requestBody);
          receivedFeature.feature._id = feature._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedFeature)];
        });
      });

      it('redirects to the edit page after the request response is received', async function () {
        authenticateSession();

        await visit(`/manage/features/add`);
        await PageElements.waitEditorJs(this.element);

        await fillIn('.row-name input.form-control', 'new feature name');

        this.element.querySelector('.row-map select').value = map._id;
        await triggerEvent('.row-map select', 'change');

        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json');

        editor.value = '';
        await fillIn(editor, `{ "type": "Point", "coordinates": [1, 2] }`);

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));
        await PageElements.waitEditorJs(this.element);

        expect(receivedFeature).to.deep.equal({
          feature: {
            _id: '5ca78e2160780601008f69e6',
            name: 'new feature name',
            description: '',
            position: {
              type: 'Point',
              coordinates: [1, 2],
            },
            visibility: 0,
            info: {},
            maps: ['5ca89e37ef1f7e010007f54c'],
          },
        });

        expect(currentURL()).to.equal(`/manage/features/edit/${feature._id}`);
      });

      it('can edit the icon attributes after submit', async function () {
        authenticateSession();

        await visit(`/manage/features/add`);


        await fillIn('.row-name input.form-control', 'new feature name');

        this.element.querySelector('.row-map select').value = map._id;
        await triggerEvent('.row-map select', 'change');

        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json');
        editor.value = '';

        await fillIn(editor, `{ "type": "Point", "coordinates": [1, 2] }`);

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        await click('.container-group-attributes .btn-edit');
        await click('.btn-add-icon-group');
        await click('.btn-add-new-attribute');

        await fillIn('.input-attribute-value', 'some value');

        await click('.btn-submit');

        expect(receivedFeature.feature.attributes).to.deep.equal({
          'Healthy Dining': { 'new attribute': 'some value' },
        });
      });

      it('can change the map', async function () {
        authenticateSession();

        await visit(`/manage/features/add`);
        await PageElements.waitEditorJs(this.element);

        this.element.querySelector('.row-map select').value = '000000000000000000000002';
        await triggerEvent('.row-map select', 'change');

        expect(this.element.querySelector('.row-map select').value).to.equal('000000000000000000000002');
      });
    });

    describe('when the server request fails', function (hooks) {
      hooks.beforeEach(function () {
        server.post('/mock-server/features', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description:
                    'The `Feature.description` field is required: {"info":{"createdOn":"2020-06-08T10:46:16Z","changeIndex":0,"lastChangeOn":"2020-06-08T10:46:16Z","originalAuthor":"5b870669796da25424540deb","author":"5b870669796da25424540deb"},"maps":["5ca89e2fef1f7e010007f50a"],"contributors":["5b870669796da25424540deb"],"name":"dsgf","visibility":0,"position":{"type":"Point","coordinates":[5.189189189189191,29.98833891386687]}}',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function (a) {
        authenticateSession();

        let error;

        const notifications = this.owner.lookup('service:notifications');
        notifications.handleError = (e) => {
          error = e['errors'][0];
        };

        await visit(`/manage/features/add`);
        await PageElements.waitEditorJs(this.element);

        await fillIn('.row-name input.form-control', 'new feature name');

        this.element.querySelector('.row-map select').value = map._id;
        await triggerEvent('.row-map select', 'change');

        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json');
        editor.value = '';
        await fillIn(editor, `{ "type": "Point", "coordinates": [1, 2] }`);

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));
        await PageElements.waitEditorJs(this.element);

        expect(currentURL()).to.equal(`/manage/features/add`);
        a.deepEqual(error, {
          description:
            'The `Feature.description` field is required: {"info":{"createdOn":"2020-06-08T10:46:16Z","changeIndex":0,"lastChangeOn":"2020-06-08T10:46:16Z","originalAuthor":"5b870669796da25424540deb","author":"5b870669796da25424540deb"},"maps":["5ca89e2fef1f7e010007f50a"],"contributors":["5b870669796da25424540deb"],"name":"dsgf","visibility":0,"position":{"type":"Point","coordinates":[5.189189189189191,29.98833891386687]}}',
          status: 400,
          title: 'Validation error',
        });
      });
    });
  });

  describe('when an admin user is authenticated', function (hooks) {
    hooks.beforeEach(() => {
      server.testData.storage.addDefaultUser(true);
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/features/add`);
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal('/manage/features/add');
    });

    describe('when the server request is successfully', function (hooks) {
      hooks.beforeEach(function () {
        server.post('/mock-server/features', (request) => {
          receivedFeature = JSON.parse(request.requestBody);
          receivedFeature.feature._id = feature._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedFeature)];
        });
      });

      it('queries the maps without the all query param', async function () {
        authenticateSession();

        await visit(`/manage/features/add`);
        await PageElements.waitEditorJs(this.element);

        await fillIn('.row-name input.form-control', 'new feature name');

        this.element.querySelector('.row-map select').value = map._id;
        await triggerEvent('.row-map select', 'change');

        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json');
        editor.value = '';
        await fillIn(editor, `{ "type": "Point", "coordinates": [1, 2] }`);

        expect(server.history).to.contain('GET /mock-server/maps?canAdd=true');

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));
        await PageElements.waitEditorJs(this.element);

        expect(receivedFeature).to.deep.equal({
          feature: {
            _id: '5ca78e2160780601008f69e6',
            name: 'new feature name',
            description: '',
            position: { type: 'Point', coordinates: [1, 2] },
            visibility: 0,
            info: {},
            maps: ['5ca89e37ef1f7e010007f54c'],
          },
        });

        expect(currentURL()).to.equal(`/manage/features/edit/${feature._id}`);
      });

      it('queries the maps with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/manage/features/add`);
        await PageElements.waitEditorJs(this.element);

        expect(server.history).to.contain('GET /mock-server/maps?canAdd=true');

        await click('.btn-show-all-teams');
        await PageElements.waitEditorJs(this.element);
        expect(server.history).to.contain('GET /mock-server/maps?all=true');
      });
    });
  });
});
