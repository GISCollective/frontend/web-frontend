import EmberRouter from '@ember/routing/router';
import config from 'embedded/config/environment';

if (config.rootURL == '/./') {
  config.rootURL = './';
}

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('map-view');
  this.route('map-selectable');

  this.route('user-location');
  this.route('location-selector');
  this.route('geo-json');
});
