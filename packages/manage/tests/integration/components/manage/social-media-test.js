import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import { SocialMediaLinks } from 'models/transforms/social-media-links';

describe('Integration | Component | manage/social-media', function (hooks) {
  setupRenderingTest(hooks);

  describe("in view mode", function () {
    it('renders an alert by default', async function (assert) {
      await render(hbs`<Manage::SocialMedia @title="social media"/>`);

      expect(this.element.querySelector(".alert")).to.have.textContent("Your team does not have any social media links.");
    });

    it('renders all links in view mode when set', async function (assert) {
      this.set("value", new SocialMediaLinks({
        "facebook": "https://example1.com",
        "youtube": "https://example2.com",
        "xTwitter": "https://example3.com",
        "instagram": "https://example4.com",
        "whatsApp": "https://example5.com",
        "tikTok": "https://example6.com",
        "linkedin": "https://example7.com",
      }));

      await render(hbs`<Manage::SocialMedia @title="social media" @value={{this.value}}/>`);

      const names = [...this.element.querySelectorAll(".name")].map(a => a.textContent.trim());
      const values = [...this.element.querySelectorAll(".value")].map(a => a.textContent.trim());

      expect(names).to.deep.equal(['Facebook', 'YouTube', 'X', 'Instagram', 'WhatsApp', 'TikTok', 'Linkedin']);
      expect(values).to.deep.equal(['https://example1.com', 'https://example2.com', 'https://example3.com', 'https://example4.com', 'https://example5.com', 'https://example6.com', 'https://example7.com']);

      expect(this.element.querySelector(".alert")).not.to.exist;
    });

    it('renders one link in view mode when set', async function (assert) {
      this.set("value", new SocialMediaLinks({
        "youtube": "https://example2.com",
      }));

      await render(hbs`<Manage::SocialMedia @title="social media" @value={{this.value}}/>`);

      const names = [...this.element.querySelectorAll(".name")].map(a => a.textContent.trim());
      const values = [...this.element.querySelectorAll(".value")].map(a => a.textContent.trim());

      expect(names).to.deep.equal(['YouTube']);
      expect(values).to.deep.equal(['https://example2.com']);

      expect(this.element.querySelector(".alert")).not.to.exist;
    });
  });

  describe("in edit mode", function () {
    it('can change an empty value', async function (a) {

      let value;
      this.set("save", (v) => {
        value = v;
      });

      await render(hbs`<Manage::SocialMedia @title="social media" @editablePanel="social media" @onSave={{this.save}} />`);

      const inputs = [...this.element.querySelectorAll("input")];

      let i = 0;

      for (const input of inputs) {
        i++;
        await fillIn(input, `https://example${i}.com`);
      }

      await click(".btn-submit");

      a.deepEqual(value.toJSON(), {
        "facebook": "https://example1.com",
        "instagram": "https://example4.com",
        "linkedin": "https://example7.com",
        "tikTok": "https://example6.com",
        "whatsApp": "https://example5.com",
        "xTwitter": "https://example3.com",
        "youtube": "https://example2.com"
      });
    });

    it('adds https when the url does not contain one', async function (a) {

      let value;
      this.set("save", (v) => {
        value = v;
      });

      await render(hbs`<Manage::SocialMedia @title="social media" @editablePanel="social media" @onSave={{this.save}} />`);

      const inputs = [...this.element.querySelectorAll("input")];

      let i = 0;

      for (const input of inputs) {
        i++;
        await fillIn(input, i);
      }

      await click(".btn-submit");

      a.deepEqual(value.toJSON(), {
        "facebook": "https://1",
        "instagram": "https://4",
        "linkedin": "https://7",
        "tikTok": "https://6",
        "whatsApp": "https://5",
        "xTwitter": "https://3",
        "youtube": "https://2"
      });
    });

    it('renders a value with all fields', async function (a) {
      let value;
      this.set("save", (v) => {
        value = v;
      });

      this.set("value", new SocialMediaLinks({
        "facebook": "https://example1.com",
        "youtube": "https://example2.com",
        "xTwitter": "https://example3.com",
        "instagram": "https://example4.com",
        "whatsApp": "https://example5.com",
        "tikTok": "https://example6.com",
        "linkedin": "https://example7.com",
      }));

      await render(hbs`<Manage::SocialMedia @title="social media" @editablePanel="social media" @onSave={{this.save}} @value={{this.value}} />`);

      const inputs = [...this.element.querySelectorAll("input")].map(a => a.value);

      a.deepEqual(inputs, [
        "https://example1.com",
        "https://example2.com",
        "https://example3.com",
        "https://example4.com",
        "https://example5.com",
        "https://example6.com",
        "https://example7.com"]);
    });
  });
});
