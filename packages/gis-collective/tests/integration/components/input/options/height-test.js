/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import typeIn from '@ember/test-helpers/dom/type-in';

describe ('Integration | Component | input/options/height',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the value', async function () {
    await render(hbs`<Input::Options::Height @size="sm" @value={{100}} />`);

    expect(this.element.querySelector('.sm-height').value).to.equal('100');
  });

  it('renders nothing when the value is not an int', async function () {
    await render(hbs`<Input::Options::Height @size="sm" @value={{"a"}} />`);

    expect(this.element.querySelector('.sm-height').value).to.equal('');
  });

  it('renders nothing when the value is a negative number', async function () {
    await render(hbs`<Input::Options::Height @size="sm" @value={{-1}} />`);

    expect(this.element.querySelector('.sm-height').value).to.equal('');
  });

  it('renders nothing when the value is 0', async function () {
    await render(hbs`<Input::Options::Height @size="sm" @value={{0}} />`);

    expect(this.element.querySelector('.sm-height').value).to.equal('');
  });

  it('triggers on change when the value is updated', async function () {
    let value;
    this.set('change', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::Options::Height @size="sm" @value={{0}} @onChange={{this.change}} />`
    );

    await typeIn('.sm-height', '100');
    expect(value).to.equal(100);
  });
});
