/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class ApplicationRoute extends Route {
  @service user;
  @service session;
  @service space;
  @service intl;

  queryParams = {
    // next: {}
  };

  async beforeModel(transition) {
    this.intl.setLocale('en-us');

    await this.space.setup();
    await this.session.setup();
    await this.user.ready();
  }
}
