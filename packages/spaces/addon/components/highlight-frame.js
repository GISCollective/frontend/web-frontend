/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class HighlightFrameComponent extends Component {
  icons = {
    slot: "plug",
    "data source": "database",
  };

  get showTextType() {
    if (!this.args.value.type) {
      return false;
    }

    if (this.icons[this.args.value.type]) {
      return false;
    }

    return this.args.value.name != this.args.value.type;
  }

  get iconType() {
    if (!this.args.value.type) {
      return false;
    }

    return this.icons[this.args.value.type];
  }

  @action
  select() {
    return this.args?.onSelect?.(this.args.value);
  }

  @action
  setupTitle(element) {
    const bbox = element.getBoundingClientRect();
    this.args.value.titleWidth = Math.ceil(bbox.width);
  }

  @action
  activate() {
    this.args.value.isPrimary = true;
  }
}
