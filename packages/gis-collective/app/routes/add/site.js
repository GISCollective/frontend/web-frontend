/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import IconGroupRoute from '../base/icon-group-route';
import EmberObject from '@ember/object';
import { hash } from 'rsvp';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

class AddSiteModel {
  @tracked feature;
  @tracked defaultIcons;
  @tracked defaultBaseMaps;

  constructor(params) {
    this.feature = params.feature;
    this.defaultIcons = params.defaultIcons;
    this.defaultBaseMaps = params.defaultBaseMaps;
  }
}

export default class AddSiteRoute extends IconGroupRoute {
  @service position;
  @service intl;

  queryParams = {
    lon: { refreshModel: false },
    lat: { refreshModel: false },
    map: { refreshModel: false },
    icons: { refreshModel: false },
    ignoreGps: { refreshModel: false },
  };

  async createFeature(params) {
    let feature = this.store.peekAll('feature').find((a) => a.get('id') === null);

    if (!feature || feature.position == null) {
      feature = this.controllerFor('add.site').createNewFeature(params);
    }

    return feature;
  }

  getDefaultIconSets() {
    if (this._defaultIconSets) {
      return new Promise((resolve) => {
        resolve(this._defaultIconSets);
      });
    }

    return this.store.query('icon-set', { default: true }).then((result) => {
      this._defaultIconSets = result;
      return result;
    });
  }

  getDefaultBaseMaps() {
    if (this._defaultBaseMaps) {
      return new Promise((resolve) => {
        resolve(this._defaultBaseMaps);
      });
    }

    return this.store.query('base-map', { default: true }).then((result) => {
      this._defaultBaseMaps = result;
      return result;
    });
  }

  getMaps(containing) {
    if (this._maps && this._maps.containing == containing) {
      return new Promise((resolve) => {
        resolve(this._maps.result);
      });
    }

    return this.store.query('map', { containing }).then((result) => {
      this._maps = { containing, result };
      return result;
    });
  }

  getIcons(iconSet) {
    if (this._icons && this._icons.iconSet == iconSet) {
      return new Promise((resolve) => {
        resolve(this._icons.result);
      });
    }

    let iconQuery = { iconSet, locale: this.intl.primaryLocale };

    return this.store
      .adapterFor('icon')
      .group(iconQuery)
      .then((icons) => {
        this.addIcons(icons);

        this._icons = { iconSet, result: icons };

        return icons;
      });
  }

  fillIcons(params, feature) {
    if (!params.icons) {
      return;
    }

    const icons = params.icons
      .split(',')
      .map((a) => this.store.peekRecord('icon', a))
      .filter((a) => a);

    feature.icons.setObjects(icons);
  }

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const feature = await this.createFeature(params);

    const initialQuery = {
      iconSets: this.getDefaultIconSets(),
      baseMaps: this.getDefaultBaseMaps(),
    };

    const result = await hash(initialQuery);

    const iconSetList = [];

    result.iconSets.forEach((item) => {
      iconSetList.push(item.id);
    });

    const defaultIcons = await this.getIcons(iconSetList.join());

    if (feature.icons.length == 0) {
      this.fillIcons(params, feature);
    }

    await feature.maps;

    return new AddSiteModel({
      feature,
      defaultIcons,
      defaultBaseMaps: result.baseMaps,
    });
  }

  async setupController(controller, model) {
    super.setupController(controller, model);

    await controller.updateMaps();
    await controller.updateIcons();
  }

  deactivate() {
    if (this.isFastBoot) {
      return;
    }

    const controller = this.controllerFor('add.site');

    if (controller.model.feature && !this.store.isDestroying) {
      this.store.unloadRecord(controller.model.feature);
    }
  }
}
