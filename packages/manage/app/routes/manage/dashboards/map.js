/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { action } from '@ember/object';
import LazyList from 'models/lib/lazy-list';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { DateTime } from 'luxon';

class MapModelData {
  @tracked map;
  @tracked team;
  @tracked features;
  @tracked mapMetrics;
  @tracked isAdmin;

  constructor(params, store) {
    this.params = params;
    this.store = store;
  }

  get start() {
    return this.end.minus({ months: 1 });
  }

  get end() {
    return DateTime.now().endOf('day');
  }

  @action
  async reload() {
    this.map = await this.store.findRecord('map', this.params.id);
    this.team = await this.map.visibility.fetchTeam();

    const query = { map: this.params.id };
    this.features = new LazyList('feature', query, this.store);
    this.features.itemsPerPage = 100;
    await this.features.loadNext();

    if (this.team.allowReports || this.isAdmin) {
      this.mapMetrics = await this.store.query('metric', {
        name: this.params.id,
        type: 'mapView',
        start: this.start.toISO(),
        end: this.end.toISO(),
      });
    }

    return this;
  }
}

export default class ManageDashboardsMapRoute extends AuthenticatedRoute {
  @service user;

  async model(params) {
    const mapModel = new MapModelData(params, this.store);
    mapModel.isAdmin = this.user.isAdmin;

    return mapModel.reload();
  }
}
