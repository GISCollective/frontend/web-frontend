/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import PictureMeta from 'models/lib/picture-meta';
import { service } from '@ember/service';
import { guidFor } from '@ember/object/internals';

export default class InputPhotoGalleryComponent extends Component {
  elementId = `input-photo-gallery-${guidFor(this)}`;

  @service store;
  @tracked isLoading;
  @tracked _value = null;
  @tracked showCanvas;
  @tracked selectedImage;

  get value() {
    return this._value ?? [];
  }

  get pictureName() {
    return this.selectedImage?.name ?? '';
  }

  set pictureName(value) {
    this.selectedImage.name = value;
    this.triggerOnChange();
  }

  get pictureAttributions() {
    return this.selectedImage?.meta?.attributions ?? '';
  }

  set pictureAttributions(value) {
    if (!this.selectedImage?.meta) {
      this.selectedImage.meta = {};
    }

    this.selectedImage.meta.attributions = value;
    this.triggerOnChange();
  }

  @action
  triggerOnChange() {
    return this.args.onChange?.(this.value);
  }

  @action
  async setup() {
    this._value = (await this.args.value) ?? [];
  }

  @action
  setupFileInput(element) {
    this.fileElement = element;
  }

  @action
  selectFile() {
    this.fileElement.click();
  }

  @action
  fileSelected(event) {
    for (const file of event.target.files) {
      const reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onload = (event) => {
        this.cameraSuccess(event.target.result);
      };
    }
  }

  cameraSuccess(imageURI) {
    const picture = this.store.createRecord('picture', {
      name: '',
      picture: imageURI,
      meta: new PictureMeta({
        link: {
          model: this.args.model,
          modelId: this.args.modelId,
        },
      }),
    });

    this._value.push(picture);
    this.triggerOnChange();
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    this._value = sourceList;

    this.triggerOnChange();
  }

  @action
  remove(index) {
    if(!this._value) {
      this._value = this.value;
    }

    this._value.splice(index, 1);

    this.triggerOnChange();
  }

  @action
  async rotate() {
    await this.selectedImage.rotate();

    return this.triggerOnChange();
  }

  get is360() {
    return this.selectedImage.is360;
  }

  set is360(value) {
    this.selectedImage.is360 = value;

    this.triggerOnChange();
  }

  @action
  async showDetails(index) {
    this.selectedImage = this.value[index];
    this.showCanvas = true;
  }
}
