/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageAttributionsListComponent extends Component {
  @tracked _list = null;

  get list() {
    if (this._list) {
      return this._list;
    }

    return this.args.list ?? [];
  }

  @action
  save() {
    this.args.onSave?.(this._list);
  }

  @action
  cancel() {
    this._list = null;
    this.args.onCancel();
  }

  @action
  changeList(value) {
    this._list = value;
  }
}
