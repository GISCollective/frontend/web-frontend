/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { without } from 'core/lib/array';

export default class ChkRecordSelectionComponent extends Component {
  get record() {
    return this.args.record ?? {};
  }

  get isSelected() {
    if (this.args.selection == 'all') {
      return true;
    }

    return this.args.selection?.includes(this.record.id);
  }

  set isSelected(value) {
    let newValue = Array.isArray(this.args.selection) ? this.args.selection : [];

    if (value) {
      newValue.push(this.record.id);
    }

    if (!value) {
      newValue = without(newValue, this.record.id);
    }

    return this.args.onSelect?.(newValue);
  }
}
