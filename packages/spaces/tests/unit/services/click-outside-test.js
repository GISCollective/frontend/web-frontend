/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupTest } from "../../helpers";

describe("Unit | Service | click-outside", function (hooks) {
  setupTest(hooks);
  let element;
  let element2;

  hooks.beforeEach(function () {
    element = document.createElement("div");
    element2 = document.createElement("div");
    document.body.appendChild(element);
    document.body.appendChild(element2);
  });

  hooks.afterEach(function () {
    if (document.body.contains(element)) {
      document.body.removeChild(element);
      document.body.removeChild(element2);
    }
  });

  it("should not have a subscriber on init", function () {
    let service = this.owner.lookup("service:click-outside");

    expect(service.hasSubscriber).to.equal(false);
  });

  it("can subscribe an element", function () {
    let service = this.owner.lookup("service:click-outside");

    service.subscribe(element, () => {});
    expect(service.hasSubscriber).to.equal(true);
  });

  it("does not unsubscribe when the same element is added", function () {
    let service = this.owner.lookup("service:click-outside");
    let val;

    this.set("func1", () => {
      val = 1;
    });

    this.set("func2", () => {
      val = 2;
    });

    service.subscribe(element, this.func1);
    service.subscribe(element, this.func2);
    expect(val).to.be.undefined;
  });

  it("does not hing when the same element is subscribed", function () {
    let service = this.owner.lookup("service:click-outside");
    let val;

    this.set("func1", () => {
      val = 1;
    });

    this.set("func2", () => {
      val = 2;
    });

    service.subscribe(element, this.func1);
    service.subscribe(element, this.func2);
    expect(val).to.be.undefined;
  });

  it("unsubscribes an element when unsubscribe is called", function () {
    let service = this.owner.lookup("service:click-outside");
    let val;

    this.set("func1", () => {
      val = 1;
    });

    service.subscribe(element, this.func1);
    service.unsubscribe();

    expect(val).to.equal(1);
  });

  it("does not unsubscribe an element if it is not in DOM", function () {
    let service = this.owner.lookup("service:click-outside");
    let val = 9;

    this.set("func1", () => {
      val = 1;
    });

    this.set("func2", () => {
      val = 2;
    });

    document.body.removeChild(element);
    service.subscribe(element, this.func1);
    service.subscribe(element, this.func2);

    expect(val).to.equal(9);
  });
});
