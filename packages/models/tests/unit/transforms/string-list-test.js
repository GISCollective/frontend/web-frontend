/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';

import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | string list', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:string-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize a list of strings', function () {
    let deserialized = transform.deserialize(['a', 'b', 'c']);

    expect(deserialized).to.deep.equal(['a', 'b', 'c']);
  });

  it('can serialize an array', function () {
    expect(transform.serialize(['a', 'b', 'c'])).to.deep.equal(['a', 'b', 'c']);
  });
});
