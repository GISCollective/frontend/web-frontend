/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";
import waitFor from '@ember/test-helpers/dom/wait-for';

describe ('Integration | Component | view/preview',  function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultArticle('2');
    server.testData.storage.addDefaultPicture('1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    const store = this.owner.lookup('service:store');

    const article_1 = await store.findRecord('article', '1');
    const article_2 = await store.findRecord('article', '2');
    const picture_1 = await store.findRecord('picture', '1');

    this.set('model', {
      article_1,
      article_2,
      picture_1,
    });
  });

  it('renders nothing when a col is not set', async function () {
    await render(hbs`<View::Preview />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders an article column', async function () {
    this.set('value', {
      type: 'article',
      data: {
        source: { id: '1', model: 'article' },
      },
      modelKey: 'article_1',
    });

    await render(
      hbs`<View::Preview @value={{this.value}} @model={{this.model}} />`
    );

    expect(
      this.element.querySelector('.article-preview').textContent
    ).to.contain('some content');
  });

  it('renders a picture column', async function () {
    this.set('value', {
      type: 'picture',
      record: {
        picture:
          'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
      },
      data: {
        sizing: 'auto',
      },
    });

    await render(hbs`<View::Preview @value={{this.value}}/>`);
    await waitFor('.picture-preview img');

    expect(
      this.element.querySelector('.picture-preview img')
    ).to.have.attribute(
      'src',
      'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm'
    );
  });
});
