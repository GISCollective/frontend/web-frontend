/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';
import { hash } from 'rsvp';
import { fillPageStateInModel } from 'core/lib/page-state';
import { toPageId, fillVariable, getVariablesFromPath } from 'core/lib/slug';
import { fetchSourceRecord } from 'models/lib/fetch-source';
import { fromPageState } from 'core/lib/page-state';

export class PageData {
  @tracked changeCount = 0;
  @tracked selectedModel;
  @tracked selectedType;
  @tracked selectedId;
}

export const validVariables = [
  'feature',
  'map',
  'campaign',
  'icon',
  'icon-set',
  'article',
];

export class PageGroup {
  @tracked path;
  @tracked page;
  @tracked store;
  @tracked data = new PageData();
  @tracked pageId;
  @tracked space;
  @tracked tracking;

  get slug() {
    return this.page?.slug ?? '';
  }

  get hasVariable() {
    return this.slug.includes(':');
  }

  get variableName() {
    return getVariablesFromPath(this.slug)[0] ?? '';
  }

  get variableValue() {
    let value;
    const pathPieces = this.path?.split?.('/').filter?.((a) => a.trim()) ?? [];
    const slugPieces = this.slug?.split?.('--') ?? [];

    slugPieces.forEach((item, index) => {
      if (!item.includes(':')) {
        return;
      }

      value = pathPieces[index];
    });

    return value ?? '';
  }

  get hasUnknownVariableValue() {
    return this.variableValue.startsWith(':');
  }

  fillState(state) {
    for (const col of this.page.cols) {
      col.fillState(state);
    }
  }

  async fetchSelectedModel() {
    if (this.store.isDestroyed) {
      return;
    }

    if (!this.hasVariable) {
      return;
    }

    const model = this.variableName.replace(':', '').replace('-id', '');
    const id = this.variableValue;

    if (!model) {
      return;
    }

    if (!this.variableValue) {
      return;
    }

    let record = await fetchSourceRecord(
      {
        model,
        id,
      },
      this.space,
      this.store,
    );

    this.data[model] = record;
    this.data.selectedModel = record;
    this.data.selectedType = model;
    this.data.selectedId = id;

    await this.tracking?.addLoad?.(model, id);
  }

  async ensurePage() {
    if (this.page) {
      return;
    }

    this.page = await this.store.findRecord('page', this.pageId);
  }

  async fetch() {
    if (!this.pageId) {
      return;
    }

    await this.ensurePage();

    await this.page.fetchColsWithoutParams(this.data);
    await this.fetchSelectedModel();
    await this.page.fetchColsWithParams(this.data);
    const loadedData = await hash(this.data);

    for (const key of Object.keys(loadedData)) {
      this.data[key] = loadedData[key];
    }

    this.data.space = this.space;
    this.data.page = this.page;
    this.data.changeCount++;
  }
}

export class PageModel {
  @tracked space;
  @tracked store;
  @tracked headData;

  @tracked main = new PageGroup();
  @tracked panel = null;

  constructor(store, space, path, tracking) {
    this.store = store;
    this.space = space;

    this.main.store = store;
    this.main.path = path;
    this.main.space = space;
    this.main.tracking = tracking;
    this.main.pageId = toPageId(path, this.space);
  }

  fillState(state) {
    fillPageStateInModel(state, this.main.data);

    this.main.fillState(state);
    if (!this.main.page.canShowFeaturePanel) {
      this.panel = null;
      return;
    }

    if (!this.main.data['state-feature-id']) {
      this.panel = null;
      return;
    }

    if (!this.panel) {
      this.panel = new PageGroup();
      this.panel.tracking = this.tracking;
    }

    this.panel.path = `/_panel/feature/${this.main.data['state-feature-id']}`;
    const pageId = toPageId(this.panel.path, this.space);

    if (this.panel?.pageId != pageId) {
      this.panel.page = null;
      this.panel.store = this.store;
      this.panel.space = this.space;
      this.panel.pageId = pageId;
    }
  }

  async fetch() {
    await this.main.fetch();

    await this.panel?.fetch();

    if (!this.space) {
      this.space = await this.main.page.space;
    }
  }

  static clearCache() {
    this.cachedModel = null;
  }

  static async getCachedModel(path, store, space, tracking) {
    if (
      this.cachedModel?.main?.path != path ||
      this.cachedModel?.space?.isDestroyed
    ) {
      this.cachedModel = new PageModel(store, space, path, tracking);
    }

    return this.cachedModel;
  }

  static async getPageModelFromPath(
    path,
    strState,
    store,
    space,
    router,
    tracking,
  ) {
    let pageModel = await PageModel.getCachedModel(
      path,
      store,
      space,
      tracking,
    );

    if (!pageModel?.main?.pageId) {
      return;
    }

    await pageModel?.main?.ensurePage();

    if (pageModel?.main?.hasUnknownVariableValue) {
      const newPath = await fillVariable(
        path,
        pageModel?.main?.variableName,
        store,
      );

      if (newPath) {
        router?.transitionTo?.(newPath);
        return {};
      }

      return;
    }

    const state = fromPageState(strState);
    pageModel.fillState(state);

    if (pageModel?.main?.pageId) {
      try {
        await pageModel.fetch();
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
        pageModel.main = null;
      }
    }

    return pageModel;
  }
}
