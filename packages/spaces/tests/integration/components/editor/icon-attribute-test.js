/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, fillIn, typeIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import click from "@ember/test-helpers/dom/click";
import PageElements from "core/test-support/page-elements";

describe("Integration | Component | editor/icon-attribute", function (hooks) {
  setupRenderingTest(hooks);

  let value;

  hooks.beforeEach(function () {
    value = null;
    this.set("change", (v) => {
      value = v;
      this.set("value", {
        "icon name": {
          key: v,
        },
      });
    });
  });

  describe("for single instance attributes", function (hooks) {
    hooks.beforeEach(function () {
      this.set("value", {
        "icon name": {
          key: "value",
        },
      });

      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
        },
      });
    });

    it("renders the attribute value as text when is an object without type", async function () {
      await render(hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} />`);

      expect(this.element.querySelectorAll(".input-text-field")).to.have.length(1);
      expect(this.element.querySelector(".input-text-field").value).to.equal("value");
    });

    it("allows changing the text value", async function () {
      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await fillIn(".input-text-field", "new value");

      expect(value).to.equal("new value");
    });

    it("allows changing the long text value", async function (a) {
      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "long text",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await PageElements.waitEditorJs(this.element);
      await click(".ce-paragraph");
      await fillIn(".ce-paragraph", "new value");

      await PageElements.wait(600);

      a.deepEqual(value, {
        blocks: [
          {
            data: {
              text: "new value",
            },
            type: "paragraph",
          },
        ],
      });
    });

    it("allows changing an int value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "integer",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await fillIn(".input-text-field", "22");

      expect(value).to.equal(22);
    });

    it("allows changing a decimal value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "decimal",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await fillIn(".input-text-field", "22.3");

      expect(value).to.equal(22.3);
    });

    it("allows changing a boolean value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "boolean",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      const select = this.element.querySelector("select");
      select.value = "false";
      await triggerEvent(select, "change");

      expect(value).to.equal(false);
    });

    it("allows changing a date value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "date",
            },
          ],
        },
      });

      this.set("value", {
        "icon name": {
          key: "2020-05-12",
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await click(".datepicker-input");
      await click(".datepicker-cell.day");

      expect(value).to.equal("2020-04-26");
    });

    it("allows changing options value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "options",
              options: "value1, value2, value3",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      const select = this.element.querySelector("select");
      select.value = "value2";
      await triggerEvent(select, "change");

      expect(value).to.equal("value2");
    });

    it("allows changing options with other value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "options with other",
              options: "value1, value2, value3",
            },
          ],
        },
      });

      await render(hbs`<Editor::IconAttribute @value="" @options={{this.options}} @onChange={{this.change}} />`);

      const select = this.element.querySelector("select");

      select.value = "Other";
      await triggerEvent(select, "change");
      await typeIn("input", "this is a custom option");

      expect(value).to.equal("this is a custom option");
    });
  });

  describe("for list instance attributes", function (hooks) {
    hooks.beforeEach(function () {
      this.set("value", {
        "icon name": [
          {
            key: "value",
          },
        ],
      });

      this.set("options", {
        key: "key",
        icon: {
          allowMany: true,
          name: "icon name",
        },
      });
    });

    it("renders the attribute value as text when is an object without type", async function () {
      await render(hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} />`);

      expect(this.element.querySelectorAll(".input-text-field")).to.have.length(1);
      expect(this.element.querySelector(".input-text-field").value).to.equal("value");
    });

    it("allows changing the text value", async function () {
      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await fillIn(".input-text-field", "new value");

      expect(value).to.deep.equal(["new value"]);
    });

    it("allows changing the long text value", async function (a) {
      this.set("options", {
        key: "key",
        icon: {
          allowMany: true,
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "long text",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await PageElements.waitEditorJs(this.element);
      await click(".ce-paragraph");
      await fillIn(".ce-paragraph", "new value");

      await PageElements.wait(600);

      a.deepEqual(value, [
        {
          blocks: [
            {
              data: {
                text: "new value",
              },
              type: "paragraph",
            },
          ],
        },
      ]);
    });

    it("allows adding a new value", async function () {
      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await click(".btn-add");

      const inputs = this.element.querySelectorAll(".input-text-field");

      await fillIn(inputs[1], "new value");

      expect(value).to.deep.equal(["value", "new value"]);
    });

    it("allows deleting a value", async function () {
      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await click(".btn-delete");

      expect(value).to.deep.equal([]);
    });

    it("allows changing an int value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          allowMany: true,
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "integer",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await fillIn(".input-text-field", "22");

      expect(value).to.deep.equal([22]);
    });

    it("allows changing a decimal value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          allowMany: true,
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "decimal",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      await fillIn(".input-text-field", "22.3");

      expect(value).to.deep.equal([22.3]);
    });

    it("allows changing a boolean value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          allowMany: true,
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "boolean",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      const select = this.element.querySelector("select");
      select.value = "false";
      await triggerEvent(select, "change");

      expect(value).to.deep.equal([false]);
    });

    it("allows changing options value", async function () {
      this.set("options", {
        key: "key",
        icon: {
          allowMany: true,
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "options",
              options: "value1, value2, value3",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      const select = this.element.querySelector("select");
      select.value = "value2";
      await triggerEvent(select, "change");

      expect(value).to.deep.equal(["value2"]);
    });

    it("allows changing options with other value", async function () {
      this.set("value", {
        "icon name": {
          key: "",
        },
      });

      this.set("options", {
        key: "key",
        icon: {
          allowMany: true,
          name: "icon name",
          attributes: [
            {
              name: "key",
              type: "options with other",
              options: "value1, value2, value3",
            },
          ],
        },
      });

      await render(
        hbs`<Editor::IconAttribute @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`,
      );

      const select = this.element.querySelector("select");

      select.value = "Other";
      await triggerEvent(select, "change");
      await typeIn("input", "this is a custom option");

      expect(value).to.deep.equal(["this is a custom option"]);
    });
  });
});
