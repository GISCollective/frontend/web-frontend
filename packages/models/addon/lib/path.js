/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { dasherize } from '@ember/string';

export function replaceVars(modelName, path, record) {
  if (!record) {
    return null;
  }

  const variables = record.relatedIdsMap ?? {};

  if (typeof record == 'object') {
    variables[dasherize(modelName)] = record.slug || record.id;
  } else {
    variables[dasherize(modelName)] = record;
  }

  for (const key in variables) {
    let variableName = `:${key}-id`;

    path = path.replace(variableName, variables[key]);
  }

  if (path.indexOf(':') > -1) {
    return null;
  }

  return path;
}
