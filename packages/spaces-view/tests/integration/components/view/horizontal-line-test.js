/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/horizontal-line', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<View::HorizontalLine />`);
    expect(this.element.textContent.trim()).to.equal('');
  });
});
