/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/button', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let spaceData;
  let space;

  hooks.before(async function () {
    server = new TestServer();
    server.testData.create.feature('1');

    server.testData.storage.addDefaultPage();
    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    const spaceService = this.owner.lookup('service:space');

    space = await store.findRecord('space', spaceData._id);
    space.getCachedPagesMap = () => {
      return {
        page1: '000000000000000000000001',
        page2: '000000000000000000000002',
        page3: '000000000000000000000003',
      };
    };

    spaceService.currentSpace = space;

    this.set('space', space);
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders a button inside a container when no value is set', async function () {
    this.set('value', {
      data: {},
    });

    await render(
      hbs`<View::Button @value={{this.value}} @isEditor={{true}} />`,
    );

    expect(
      this.element.querySelector('.page-col-base-container .btn.btn-empty'),
    ).to.exist;
  });

  describe('legacy', async function () {
    it('renders a link button', async function () {
      this.set('value', {
        data: {
          content: {
            name: 'first button',
            type: 'default',
            link: 'https://giscollective.com/page1',
          },
          style: { classes: ['btn-primary', 'size-btn-lg', 'w-50'] },
        },
      });

      await render(hbs`<View::Button @value={{this.value}} />`);

      expect(this.element.querySelector('.btn').textContent.trim()).to.equal(
        'first button',
      );

      expect(this.element.querySelector('.btn')).to.have.attribute(
        'class',
        'btn btn-primary size-btn-lg w-50',
      );
    });

    it('renders a link with a _blank target', async function () {
      this.set('value', {
        data: {
          content: {
            name: 'first button',
            type: 'default',
            link: 'https://giscollective.com/page1',
            newTab: true,
          },
          style: { classes: ['btn-primary', 'size-btn-lg', 'w-50'] },
        },
      });

      await render(hbs`<View::Button @value={{this.value}} />`);

      expect(this.element.querySelector('.btn')).to.have.attribute(
        'target',
        '_blank',
      );
    });

    it('renders a link with the selected item id when the variable matches', async function (a) {
      space.getCachedPagesMap = () => {
        return {
          'page1--:feature-id': '000000000000000000000001',
          page2: '000000000000000000000002',
          page3: '000000000000000000000003',
        };
      };

      this.set('value', {
        data: {
          content: {
            name: 'first button',
            type: 'default',
            link: { pageId: '000000000000000000000001' },
            newTab: true,
          },
          style: { classes: ['btn-primary', 'size-btn-lg', 'w-50'] },
        },
      });

      this.set('model', {
        selectedId: '6565bcb2a6f48a0d7ff0fafc',
        selectedType: 'feature',
      });

      await render(
        hbs`<View::Button @value={{this.value}} @space={{this.space}} @model={{this.model}} />`,
      );

      expect(this.element.querySelector('a')).to.have.attribute(
        'href',
        '/page1/6565bcb2a6f48a0d7ff0fafc',
      );
    });

    it('renders a link with a pageId using the space service when one is not set', async function () {
      this.set('value', {
        data: {
          content: {
            name: 'first button',
            type: 'default',
            link: {
              pageId: '000000000000000000000001',
            },
          },
        },
      });

      await render(
        hbs`<View::Button @value={{this.value}} @space={{this.space}} />`,
      );

      expect(this.element.querySelector('.btn')).to.have.attribute(
        'href',
        '/page1',
      );
    });

    it('applies the container styles', async function () {
      this.set('value', {
        data: {
          content: {
            name: 'first button',
            type: 'default',
            link: 'https://giscollective.com/page1',
          },
          style: { classes: ['btn-primary', 'size-btn-lg', 'w-50'] },
          containerStyle: { classes: ['text-center'] },
        },
      });

      await render(hbs`<View::Button @value={{this.value}} />`);

      expect(this.element.querySelector('.button-container')).to.have.class(
        'text-center',
      );
    });

    it('renders an app store button', async function () {
      this.set('value', {
        data: {
          content: {
            type: 'app-store',
            storeType: 'apple',
            link: 'https://giscollective.com/page1',
          },
          style: { classes: ['btn-primary', 'size-btn-lg', 'w-50'] },
        },
      });

      await render(hbs`<View::Button @value={{this.value}} />`);

      expect(this.element.querySelector('.btn').textContent.trim()).to.contain(
        'Download on the',
      );

      expect(this.element.querySelector('.btn')).to.have.attribute(
        'class',
        'btn btn-app-store justify-content-center d-inline-flex btn-primary size-btn-lg w-50',
      );
    });
  });
});
