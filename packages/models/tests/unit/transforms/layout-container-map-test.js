/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { LayoutContainerMap } from 'dummy/transforms/layout-container-map';
import { LayoutContainer } from 'dummy/transforms/layout-container-list';

describe('Unit | Transform | layout container map', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:layout-container-map');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal(new LayoutContainerMap({}));
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal(new LayoutContainerMap({}));
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize({
      'main-menu-container': {
        options: ['option1'],
        gid: 'main-menu-container',
        background: {},
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        rows: [
          {
            options: ['option 1', 'option 2'],
            cols: [
              {
                type: 'type',
                data: {},
                options: ['option 1', 'option 2'],
              },
            ],
          },
        ],
      },
    });

    expect(deserialized.idList.length).to.deep.equal(1);
    expect(deserialized.map['main-menu-container'].gid).to.equal('main-menu-container');
    expect(deserialized.map['main-menu-container'].rows.length).to.equal(1);
    expect(deserialized.map['main-menu-container'].options).to.deep.equal(['option1']);
    expect(deserialized.map['main-menu-container'].layers).to.deep.equal({
      count: 2,
      effect: 'some-effect',
      showContentAfter: 3,
    });
  });

  it('can serialize an object', function (a) {
    let deserialized = transform.deserialize({
      'main-menu-container': {
        options: ['option1'],
        gid: 'main-menu-container',
        background: {},
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        rows: [
          {
            options: ['option 1', 'option 2'],
            cols: [
              {
                type: 'type',
                data: {},
                options: ['option 1', 'option 2'],
              },
            ],
          },
        ],
      },
    });

    a.deepEqual(transform.serialize(deserialized), {
      'main-menu-container': {
        rows: [
          {
            name: '',
            data: {},
            cols: [
              {
                type: 'type',
                data: {},
                name: '',
                componentCount: 1,
                options: ['option 1', 'option 2'],
              },
            ],
            options: ['option 1', 'option 2'],
          },
        ],
        options: ['option1'],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        gid: 'main-menu-container',
      },
    });
  });

  describe('exists', function (hooks) {
    let map;

    hooks.beforeEach(function () {
      map = transform.deserialize({
        'main-menu-container': {
          options: ['option1'],
          gid: 'main-menu-container',
          background: {},
          layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
          rows: [
            {
              options: ['option 1', 'option 2'],
              cols: [
                {
                  type: 'type',
                  data: {},
                  options: ['option 1', 'option 2'],
                },
              ],
            },
          ],
        },
      });
    });

    it('returns true when the gid is defined', function () {
      expect(map.exists('main-menu-container')).to.equal(true);
    });

    it('returns false when the gid is not defined', function () {
      expect(map.exists('other')).to.equal(false);
    });
  });

  describe('setContainer', function (hooks) {
    let emptyMap;

    hooks.beforeEach(function () {
      emptyMap = transform.deserialize({});
    });

    it('can add a new container to an empty map', function () {
      const container = new LayoutContainer();
      emptyMap.setContainer('some container', container);

      expect(emptyMap.idList).to.deep.equal(['some container']);
      expect(emptyMap.map['some container']).to.equal(container);
      expect(emptyMap.map['some container'].gid).to.equal('some container');
    });

    it('can add a container to an empty map only  once', function () {
      const container = new LayoutContainer();
      emptyMap.setContainer('some container', container);
      emptyMap.setContainer('some container', container);

      expect(emptyMap.idList).to.deep.equal(['some container']);
      expect(emptyMap.map['some container']).to.equal(container);
      expect(emptyMap.map['some container'].gid).to.equal('some container');
    });
  });
});
