/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './base';
import Polygon from 'ol/geom/Polygon';

export default class MapFeaturesGeojsonPolygonComponent extends Component {
  getGeometry() {
    const coordinates = this.args.value.coordinates?.slice?.() ?? [];
    const geometry = new Polygon(coordinates);
    geometry.transform('EPSG:4326', 'EPSG:3857');

    return geometry;
  }
}
