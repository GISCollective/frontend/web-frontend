/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageDashboardsDashboardController extends Controller {
  @service router;
  @service intl;

  @action
  browseFeature(id) {
    this.router.transitionTo('manage.features.edit', id);
  }

  get query() {
    const result = {
      map: this.model.map.id,
    };

    return result;
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        text: this.model.map?.name,
      },
    ];
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }
}
