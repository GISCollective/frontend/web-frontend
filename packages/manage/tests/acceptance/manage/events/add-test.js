/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { visit, currentURL, fillIn, typeIn, triggerEvent, click, waitUntil } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/events/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedEvent;
  let calendar;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();
    calendar = server.testData.storage.addDefaultCalendar();
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultMap();

    receivedEvent = null;

    server.post('/mock-server/events', (request) => {
      receivedEvent = JSON.parse(request.requestBody);
      const item = server.testData.storage.addDefaultEvent();
      receivedEvent.event._id = item._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedEvent)];
    });
    server.post('/mock-server/pictures', (request) => {
      let receivedPicture = JSON.parse(request.requestBody);
      const item = server.testData.storage.addDefaultPicture();
      receivedPicture.picture._id = item._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPicture)];
    });
  });

  it('redirects to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/events/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fevents%2Fadd');
  });

  describe('when an user is authenticated', function (hooks) {
    hooks.beforeEach(function () {
      let team = server.testData.storage.addDefaultTeam();
      team.allowEvents = true;

      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();
      await visit('/manage/events/add');
      expect(currentURL()).to.equal('/manage/events/add');
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit(`/manage/events/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'test calendar', 'New event']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
        '/manage/dashboards/calendar/5cc8dc1038e882010061221',
      ]);
    });

    it('renders custom names for a calendar with a custom name', async function () {
      calendar.recordName = 'volunteering opportunity';

      authenticateSession();
      await visit(`/manage/events/add`);

      await wait(1000);

      expect(this.element.querySelector('header')).to.have.textContent('New volunteering opportunity');
      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Dashboard',
        'Open Green Map',
        'test calendar',
        'New volunteering opportunity',
      ]);
      expect(this.element.querySelector('.form-control-name')).to.have.attribute(
        'placeholder',
        'what is the volunteering opportunity name?',
      );
      expect(this.element.querySelector('.ce-paragraph')).to.have.textContent(
        'Presenting our exciting new volunteering opportunity, carefully curated to bring you a unique and memorable experience. Get ready to be part of something extraordinary!',
      );
    });

    it('redirects to the edit page after the request response is received', async function () {
      authenticateSession();

      await visit(`/manage/events/add`);

      await PageElements.waitEditorJs(this.element);

      await fillIn('.form-control-name', 'new event');

      await fillIn('.ce-paragraph', '');
      await typeIn('.ce-paragraph', 'test message');
      await PageElements.wait(510);

      /// cover
      const blob = server.testData.create.pngBlob();

      await triggerEvent(".row-cover input[type='file']", 'change', {
        files: [blob],
      });

      await click('.btn-submit');

      await waitUntil(() => receivedEvent);
      await waitUntil(() => currentURL() != '/manage/events/add');

      expect(currentURL()).to.equal(`/manage/events/edit/${receivedEvent.event._id}`);
      expect(receivedEvent.event).to.deep.equal({
        name: 'new event',
        article: { blocks: [{ type: 'paragraph', data: { text: 'test message' } }] },
        entries: [],
        allOccurrences: [],
        visibility: { isPublic: true, isDefault: false, team: '5ca78e2160780601008f69e6' },
        info: {},
        calendar: '5cc8dc1038e882010061221',
        cover: '5cc8dc1038e882010061545a',
        _id: '5cc8dc1038e882010061221',
      });
    });

    it('can submit the form without changing the article', async function () {
      authenticateSession();

      await visit(`/manage/events/add`);

      await PageElements.waitEditorJs(this.element);

      await fillIn('.form-control-name', 'new event');

      await click('.btn-submit');

      await waitUntil(() => receivedEvent);
      await waitUntil(() => currentURL() != '/manage/events/add');

      expect(receivedEvent).to.deep.equal({
        event: {
          name: 'new event',
          article: {
            blocks: [
              {
                type: 'paragraph',
                data: {
                  text: 'Presenting our exciting new event, carefully curated to bring you a unique and memorable experience. Get ready to be part of something extraordinary!',
                },
              },
            ],
          },
          entries: [],
          allOccurrences: [],
          visibility: { isPublic: true, isDefault: false, team: '5ca78e2160780601008f69e6' },
          info: {},
          calendar: '5cc8dc1038e882010061221',
          cover: null,
          _id: '5cc8dc1038e882010061221',
        },
      });
    });
  });
});
