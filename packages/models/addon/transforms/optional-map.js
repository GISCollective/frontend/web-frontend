/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { getOwner } from '@ember/application';
import { tracked } from '@glimmer/tracking';

export default class MaskTransform extends Transform {
  deserialize(serialized) {
    let applicationInstance = getOwner(this);
    const store = applicationInstance.lookup('service:store');

    return new OptionalMap(serialized, store);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class OptionalMap {
  @tracked isEnabled;
  @tracked mapId;

  constructor(value, store) {
    this.store = store;

    if (!value) {
      return;
    }

    this.isEnabled = value.isEnabled;

    if (value.map?.id) {
      this.mapId = value.map.id;
    } else if (value.map?._id) {
      this.mapId = value.map._id;
    } else if (value.map) {
      this.mapId = value.map;
    }
  }

  get map() {
    if (!this.store || !this.mapId || !this.isEnabled) {
      return null;
    }

    return this.store.peekRecord('map', this.mapId);
  }

  fetch() {
    if (!this.isEnabled) {
      return null;
    }

    if (this.map) {
      return this.map;
    }

    return this.store.findRecord('map', this.mapId);
  }

  set map(value) {
    if (!value || !value.id) {
      this.mapId = '';
    }

    this.mapId = value.id;
  }

  toJSON() {
    return {
      isEnabled: this.isEnabled ?? false,
      map: this.mapId ?? '',
    };
  }
}
