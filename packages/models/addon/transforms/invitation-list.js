import { tracked } from '@glimmer/tracking';

export default class InvitationListTransform {
  deserialize(serialized) {
    return serialized?.map?.((a) => new Invitation(a)) ?? [];
  }

  serialize(deserialized) {
    return deserialized?.map?.((a) => a?.toJSON() ?? a) ?? [];
  }

  static create() {
    return new this();
  }
}

export class Invitation {
  @tracked email = '';
  @tracked role = '';

  constructor(value) {
    if (value?.email) {
      this.email = value.email;
    }

    if (value?.role) {
      this.role = value.role;
    }
  }

  toJSON() {
    return {
      email: this.email ?? '',
      role: this.role ?? '',
    };
  }
}
