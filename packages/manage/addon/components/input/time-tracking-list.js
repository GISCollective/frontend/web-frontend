import Component from '@glimmer/component';
import { action } from '@ember/object';
import { TimeTracking } from 'models/transforms/subscription-details';

export default class InputTimeTrackingListComponent extends Component {
  @action
  factoryValue() {
    return new TimeTracking({
      hours: 1,
      date: new Date().toISOString(),
    });
  }
}
