/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | view/sound-list',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::SoundList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders two sounds', async function () {
    this.set('value', [
      {
        sound: 'sound1',
        name: 'name1',
      },
      {
        sound: 'sound2',
        name: 'name2',
      },
    ]);

    await render(hbs`<View::SoundList @value={{this.value}} />`);

    expect(
      this.element.querySelectorAll('.sound-container-minimal')
    ).to.have.length(2);
    expect(this.element.querySelectorAll('audio')).to.have.length(2);
    expect(this.element.querySelectorAll('source')[0]).to.have.attribute(
      'src',
      'sound1/weba-192'
    );
    expect(this.element.querySelectorAll('source')[1]).to.have.attribute(
      'src',
      'sound1/ogg-192'
    );
  });
});
