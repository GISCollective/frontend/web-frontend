/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';
import PageColMapTransform from '../transforms/page-col-map';
import { htmlSafe } from '@ember/template';
import { dasherize } from '@ember/string';
import { replaceVars } from 'models/lib/path';
import { DefaultModels } from 'models/transforms/default-models';

export default class Space extends Model {
  @attr('string') name;
  @attr('string') description;

  @belongsTo('picture', { async: true, inverse: null })
  logo;

  @belongsTo('picture', { async: true, inverse: null })
  logoSquare;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr('string') landingPageId;
  @attr('string') globalMapPageId;

  @attr('page-col-map') cols;
  @attr('layout-container-map') layoutContainers;
  @attr('color-palette') colorPalette;
  @attr('font-styles') fontStyles;

  @attr attributions;

  @attr('string') domain;
  @attr('string') domainValidationKey;
  @attr('string') template;
  @attr('string') locale;

  @attr('boolean') hasDomainValidated;
  @attr('string') domainStatus;
  @attr('boolean') isTemplate;

  @attr('boolean') allowDomainChange;

  @attr('string') matomoUrl;
  @attr('string') matomoSiteId;

  @attr('model-info') info;
  @attr('visibility') visibility;

  @attr('boolean') canEdit;
  @attr('string') style;

  @attr('space-search-options') searchOptions;

  @attr('link-map') redirects;

  @attr('default-models', {
    defaultValue() {
      return new DefaultModels();
    },
  })
  defaultModels;

  get teamId() {
    return this.visibility?.teamId;
  }

  get title() {
    return this.name;
  }

  get pagesMap() {
    return this._getPagesMap ?? {};
  }

  get categories() {
    return this._categories ?? {};
  }

  async getPagesMap() {
    if (!this._getPagesMap) {
      this._getPagesMap = await this.store.adapterFor(this.constructor.modelName).getPagesMap(this.id);
    }

    return this._getPagesMap;
  }

  async getCover() {
    return await this.cover;
  }

  async getCategories() {
    if (!this._categories) {
      this._categories = await this.store.adapterFor(this.constructor.modelName).categories(this.id);
    }

    return this._categories;
  }

  getCachedPagesMap() {
    return this._getPagesMap ?? {};
  }

  async getPagePath(slugOrId) {
    const pages = await this.getPagesMap();

    const match = Object.entries(pages).find((a) => a[1] == slugOrId || a[0] == slugOrId);

    if (!match) {
      return;
    }

    const slugPieces = ['', ...match[0].split('--')];

    return slugPieces.join('/');
  }

  verifyDomain() {
    return this.store.adapterFor(this.constructor.modelName).verifyDomain(this.id);
  }

  setPageCol(id, type, data) {
    if (!this.cols) {
      this.cols = new PageColMapTransform().deserialize({});
    }

    this.cols.setPageCol(id, type, data);
  }

  getPageCol(id) {
    if (!this.cols) {
      this.cols = new PageColMapTransform().deserialize({});
    }

    return this.cols.map[id];
  }

  hasPageCol(id) {
    if (typeof id != 'string') {
      return false;
    }

    return this.cols?.hasPageCol(id);
  }

  getLinkFor(modelName, record) {
    if (!modelName || !record) {
      return;
    }

    const recordVariable = `:${dasherize(modelName)}-id`;

    let match;

    if (Array.isArray(record.categories)) {
      const pageId = record.categories.map((category) => this._categories?.[category]).find((a) => a);

      match = Object.keys(this.pagesMap).find((a) => this._getPagesMap?.[a] == pageId);
    }

    if (!match) {
      match = Object.keys(this.pagesMap)
        .filter((a) => a.indexOf('_panel') != 0)
        .find((a) => a.includes(recordVariable));
    }

    const defaultPages = {
      Feature: `browse--sites--:feature-id`,
      Team: `browse--teams--:team-id`,
      Map: `browse--sites?map=:map-id`,
      MapView: `browse--maps--:map-view-id--map-view`,
    };

    if (!match) {
      match = defaultPages[modelName];
    }

    if (!match) {
      return;
    }

    let path = `--${match}`.split('--').join('/');

    return replaceVars(modelName, path, record);
  }

  get searchTeamId() {
    if (
      this.visibility.isDefault ||
      this.domain == 'platform.greenmap.org' ||
      this.domain == 'local-platform.greenmap.org'
    ) {
      return null;
    }

    return this.visibility.teamId;
  }

  get cssCover() {
    // eslint-disable-next-line ember/no-get, ember/classic-decorator-no-classic-methods
    if (!this.get('cover.picture')) {
      return null;
    }

    // eslint-disable-next-line ember/no-get, ember/classic-decorator-no-classic-methods
    return htmlSafe(`background-image: url('${this.get('cover.picture')}/md');`);
  }

  get isPublished() {
    return this.visibility.isPublic;
  }
}
