/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { PageCol } from 'models/transforms/page-col-list';
import { service } from '@ember/service';

export default class PageColMapTransform extends Transform {
  @service store;
  @service space;
  @service fastboot;

  deserialize(serialized) {
    return new PageColMap(serialized, this.store, this.space, this.fastboot);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() ?? {};
  }
}

export class PageColMap {
  @tracked idList;
  @tracked map = {};

  constructor(data, store, space, fastboot) {
    this.store = store;
    this.space = space;
    this.fastboot = fastboot;
    this.idList = Object.keys(data).sort();

    for (const id of this.idList) {
      this.map[id] = new PageCol({
        type: data[id].type,
        data: data[id].data,
      });
    }
  }

  hasPageCol(id) {
    return this.idList.includes(id) ? true : false;
  }

  setPageCol(id, type, data) {
    if (this.hasPageCol(id) && this.map[id].toJSON) {
      this.map[id].type = type;
      this.map[id].data = data;

      return;
    }

    if (this.hasPageCol(id) && !this.map[id].toJSON) {
      this.map[id] = new PageCol({
        type: type,
        data: data,
      });
    }

    this.idList.push(id);
    this.idList = this.idList.sort();

    this.map[id] = new PageCol({
      type: type,
      data: data,
    });
  }

  toJSON() {
    const result = {};

    for (const id of this.idList) {
      result[id] = this.map[id].toGlobalJSON?.() ?? {};
    }

    return result;
  }
}
