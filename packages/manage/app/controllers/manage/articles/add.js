/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { onPictureUpload } from '../../../lib/editor-js-picture';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddArticleController extends Controller {
  @service intl;
  @service store;
  @service notifications;
  @service router;
  @service user;

  destination = 'manage.articles.edit';

  @tracked all = false;
  @tracked hasTeam;
  @tracked team;
  @tracked existingCategories;
  @tracked primaryCategory;

  get canSubmit() {
    let team = this.editableModel.visibility?.team;
    const hasArticle = this.model.article.title && this.model.article.content?.blocks?.length;

    if (team && hasArticle) {
      return true;
    }

    return hasArticle && this.hasTeam;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.article.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new article'),
      capitalize: true,
    });

    return pieces;
  }

  @action
  async handlePrimaryChange(value) {
    this.primaryCategory = value;
    await this.updateCategories();
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  change(field, value) {
    this.editableModel.set(field, value);
    this.hasTeam = !!this.editableModel.visibility.team;

    if (field == 'visibility.team') {
      this.team = value;
    }
  }

  @action
  async updateCategories() {
    this.existingCategories = (
      await this.store.adapterFor('article').categories(this.editableModel?.visibility?.team?.id, this.primaryCategory)
    ).categories;
  }

  @action
  async changeTeam(value) {
    const result = this.change('visibility.team', value);

    if (this.model.article.visibility?.teamId != value.id) {
      await this.updateCategories();
    }

    return result;
  }

  @action
  changeCategories(categories) {
    this.model.article.categories = categories;
  }

  @action
  changeArticleValue(title, article) {
    this.model.article.title = title;
    this.model.article.content = article;
  }

  @action
  onPictureUpload(value, type) {
    return onPictureUpload(value, type, this.store, {}, false, (picture) => {
      this.model.pictures.push(picture);
    });
  }

  get editableModel() {
    return this.model.article;
  }

  @action
  async save() {
    let result;

    try {
      result = await this.editableModel.save();
    } catch (err) {
      return this.notifications.handleError(err);
    }

    for (let picture of this.model.pictures) {
      picture.meta.link.model = 'Article';
      picture.meta.link.modelId = result.id;

      await picture.save();
    }

    this.router.transitionTo(this.destination, result.id);
  }
}
