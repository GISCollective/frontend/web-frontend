/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class BaseListComponent extends Component {
  @service store;
  @tracked _records;

  get records() {
    if (this._records) {
      return this._records;
    }

    const key = this.args.value?.modelKey;

    if (key && this.args.model?.[key]?.loadedAny) {
      return this.args.model?.[key].items;
    }

    if (Array.isArray(this.args.value?.data?.records)) {
      return this.args.value.data.records;
    }

    if (Array.isArray(this.args.value)) {
      return this.args.value;
    }

    return [];
  }

  get size() {
    return 'lg';
  }

  async loadIds() {
    if (!this.ids) {
      return;
    }

    const records = await Promise.all(
      this.ids.map(
        (id) =>
          this.store.peekRecord(this.recordType, id) ||
          this.store.findRecord(this.recordType, id),
      ),
    );

    this._records = records.filter((a) => Boolean(a));
  }

  @action
  setup() {
    this.ids = this.args.value?.data?.source?.ids ?? this.args.value?.data?.ids;

    this.loadIds();
  }
}
