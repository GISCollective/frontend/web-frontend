/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import PageRoute from '../page';

export default class CampaignsIndexRoute extends PageRoute {
  templateName = 'page';

  model(params, transition) {
    return super.model({ wildcard: '/campaigns' }, transition);
  }
}
