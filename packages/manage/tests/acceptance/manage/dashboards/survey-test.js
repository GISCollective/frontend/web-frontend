/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/survey', function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let team;
  let calendar;
  let campaign;
  let answer;

  hooks.beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    team = server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultFeature();
    server.testData.storage.addDefaultIconSet();

    server.testData.storage.addDefaultCalendar();
    calendar = server.testData.storage.addDefaultCalendar('editable-calendar');
    calendar.name = 'Editable calendar';
    calendar.canEdit = true;

    campaign = server.testData.storage.addDefaultCampaign();
    campaign.canEdit = true;

    answer = server.testData.storage.addDefaultCampaignAnswer();
    answer.canEdit = true;
  });

  hooks.afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('redirects to the login page when the user is not authenticated', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/survey/${campaign._id}`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2Fsurvey%2F5ca78aa160780601008f6aaa');
  });

  it('displays an answer as a card', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/survey/${campaign._id}`);

    await click('.btn-grid');

    expect(this.element.querySelectorAll('.card-dashboard-answer').length).to.equal(1);
    expect(this.element.querySelector('.card-dashboard-answer .title .btn').textContent.trim()).to.equal('some test');
  });

  it('displays the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/survey/${campaign._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Campaign 1']);

    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/000000000000000000000001',
    ]);
  });

  it('can edit the survey', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/survey/${campaign._id}`);

    await click('.btn-campaign-edit');

    expect(currentURL()).to.equal('/manage/surveys/edit/5ca78aa160780601008f6aaa');
  });

  it('can access a survey answer', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/survey/${campaign._id}`);

    await click('.btn-grid');

    await click('.card-dashboard-answer .title .btn');

    expect(currentURL()).to.equal('/manage/survey-answers/5ca78aa160780601008f6bbb');

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Campaign 1', 'some test']);

    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/000000000000000000000001',
      '/manage/dashboards/survey/5ca78aa160780601008f6aaa',
    ]);
  });

  it('can delete an answer using the menu', async function () {
    const service = this.owner.lookup('service:notifications');
    service.ask = () => 'yes';

    authenticateSession();
    let deletedEvent = false;
    server.delete(`/mock-server/campaignanswers/:id`, () => {
      deletedEvent = true;

      return [204, {}, ''];
    });

    await visit(`/manage/dashboards/survey/${campaign._id}`);

    await click('.row-dashboard-answer .dropdown-toggle');
    await click('.dropdown-item-delete');

    expect(deletedEvent).to.eql(true);
    expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
  });

  it('can delete an answer using the selection', async function () {
    authenticateSession();
    let deletedEvent = false;
    server.delete(`/mock-server/campaignanswers/:id`, () => {
      deletedEvent = true;

      return [204, {}, ''];
    });

    const service = this.owner.lookup('service:modal');
    service.confirm = () => 'yes';

    await visit(`/manage/dashboards/survey/${campaign._id}`);

    await click('.row-dashboard-answer .chk-selected');

    await click('.navbar .btn-delete');

    expect(deletedEvent).to.eql(true);
    expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
  });

  it('can delete all answers', async function (a) {
    authenticateSession();

    const otherAnswer = server.testData.storage.addDefaultCampaignAnswer('2');
    otherAnswer.canEdit = true;

    let queryParams;

    let deletedEvent = false;
    server.delete(`/mock-server/campaignanswers`, (req) => {
      queryParams = req.queryParams;
      deletedEvent = true;

      return [204, {}, ''];
    });

    const service = this.owner.lookup('service:modal');
    service.confirm = () => 'yes';

    await visit(`/manage/dashboards/survey/${campaign._id}`);
    await click('.chk-select-all');

    await click('.navbar .btn-delete');

    expect(deletedEvent).to.eql(true);
    expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
    a.deepEqual(queryParams, {
      campaign: '5ca78aa160780601008f6aaa',
    });
  });
});
