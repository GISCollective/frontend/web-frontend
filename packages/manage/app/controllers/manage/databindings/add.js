/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class AddDataBindingController extends Controller {
  @service notifications;
  @service user;
  @service router;
  @service intl;

  @tracked canSubmit = false;
  @tracked all = false;
  @tracked team;

  queryParams = ['all'];

  get teams() {
    if (this.user.isAdmin) {
      return this.model.teams;
    }

    return this.model.teams.filter((a) => a.allowCustomDataBindings == true);
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.dataBinding.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new data binding'),
      capitalize: true,
    });

    return pieces;
  }

  @action
  checkCanSubmit() {
    if (!this.model.dataBinding.visibility.team) {
      this.canSubmit = false;
      return;
    }

    if (!this.model.dataBinding.name) {
      this.canSubmit = false;
      return;
    }

    if (!this.model.dataBinding.destination?.modelId) {
      this.canSubmit = false;
      return;
    }

    if (!this.model.dataBinding.connection.type) {
      this.canSubmit = false;
      return;
    }

    if (!this.model.dataBinding.connection.config) {
      this.canSubmit = false;
      return;
    }

    this.canSubmit = true;
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  changeTeam(value) {
    this.team = value;
    this.model.dataBinding.set('visibility.team', value);

    later(() => {
      this.checkCanSubmit();
    });
  }

  @action
  change(key, value) {
    later(() => {
      this.model.dataBinding.set(key, value);
      this.checkCanSubmit();
    });
  }

  @action
  async save() {
    const result = await this.model.dataBinding.save();

    later(() => {
      this.router.transitionTo('manage.databindings.edit', result.id);
    });
  }
}
