/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Application from 'dummy/app';
import config from 'dummy/config/environment';
import QUnit from 'qunit';
import Pretender from 'pretender';
import { marked } from 'marked';
import sanitizeHtml from 'sanitize-html';
import blurhash from 'blurhash';
import md5 from 'blueimp-md5';
import { setApplication } from '@ember/test-helpers';
import { setup } from 'qunit-dom';
import { start } from 'ember-qunit';
import chai from 'chai';
import chaiDateTime from 'chai-datetime';

import { Settings } from 'luxon';
Settings.defaultZone = 'EET';

chai.use(chaiDateTime);

setApplication(Application.create(config.APP));

setup(QUnit.assert);

QUnit.config.failOnZeroTests = false;

start({
  failOnZeroTests: false,
});
