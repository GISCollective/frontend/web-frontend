import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

describe("Integration | Component | editor/video-source", function (hooks) {
  setupRenderingTest(hooks);

  it("renders a value", async function (assert) {
    this.set("value", {
      url: "http://example.com",
    });

    await render(hbs`<Editor::VideoSource @value={{this.value}}/>`);
    expect(this.element.querySelector(".form-control.url").value).to.equal("http://example.com");
  });
});
