import Component from '@glimmer/component';

const defaultNames = ['position', 'name', 'description', 'icons', 'pictures', 'sounds'];

export default class ManageOtherAnswersComponent extends Component {
  get questions() {
    return this.args.questions?.filter((a) => !defaultNames.includes(a.name)) ?? [];
  }

  get isDisabled() {
    return this.args.isDisabled || this.questions.length == 0;
  }
}
