/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | layout container list', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:layout-container-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize([
      {
        options: ['option1'],
        gid: 'main-menu-container',
        anchor: 'some-anchor',
        background: {},
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        rows: [
          {
            options: ['option 1', 'option 2'],
            cols: [
              {
                type: 'type',
                data: {},
                options: ['option 1', 'option 2'],
              },
            ],
          },
        ],
      },
    ]);

    expect(deserialized.length).to.deep.equal(1);
    expect(deserialized[0].gid).to.equal('main-menu-container');
    expect(deserialized[0].anchor).to.equal('some-anchor');
    expect(deserialized[0].rows.length).to.equal(1);
    expect(deserialized[0].options).to.deep.equal(['option1']);
    expect(deserialized[0].layers).to.deep.equal({
      count: 2,
      effect: 'some-effect',
      showContentAfter: 3,
    });
  });

  it('can serialize an object', function (a) {
    let deserialized = transform.deserialize([
      {
        rows: [
          {
            cols: [
              {
                type: 'type',
                data: {},
                name: '',
                options: ['option 1', 'option 2'],
              },
            ],
            options: ['option 1', 'option 2'],
          },
        ],
        gid: 'some-gid',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        options: ['option1'],
      },
    ]);

    a.deepEqual(transform.serialize(deserialized), [
      {
        rows: [
          {
            name: '0.0',
            data: {},
            cols: [
              {
                type: 'type',
                data: {},
                name: '0.0.0',
                componentCount: 1,
                options: ['option 1', 'option 2'],
              },
            ],
            options: ['option 1', 'option 2'],
          },
        ],
        gid: 'some-gid',
        options: ['option1'],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
      },
    ]);
  });
});
