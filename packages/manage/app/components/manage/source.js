/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class ManageSourceComponent extends Component {
  @service store;

  @tracked campaign;
  @tracked dataBinding;
  @tracked baseMap;
  @tracked hasMissingModel = false;

  get isCampaign() {
    return this.args.value?.type == 'Campaign' && !this.hasMissingModel;
  }

  get isDataBinding() {
    return this.args.value?.type == 'DataBinding' && !this.hasMissingModel;
  }

  get isBaseMap() {
    return this.args.value?.type == 'BaseMap' && !this.hasMissingModel;
  }

  get syncAt() {
    return new Date(this.args.value?.syncAt);
  }

  get hasSyncAt() {
    return this.syncAt.getFullYear() > 1990;
  }

  @action
  async setup() {
    try {
      if (this.isCampaign) {
        this.campaign = await this.store.findRecord('campaign', this.args.value.modelId);
      }

      if (this.isDataBinding) {
        this.dataBinding = await this.store.findRecord('data-binding', this.args.value.modelId);
      }

      if (this.isBaseMap) {
        this.baseMap = await this.store.findRecord('base-map', this.args.value.modelId);
      }
    } catch (err) {
      this.hasMissingModel = true;
    }
  }
}
