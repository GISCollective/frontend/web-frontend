/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/value-date', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a message when the value is not set', async function () {
    this.set('value', '');

    await render(hbs`<Attributes::ValueDate @value={{this.value}} />`);

    expect(this.element.querySelector('.calendar-container')).to.have.class('d-none');
    expect(this.element.querySelector('.current-value')).not.to.exist;
    expect(this.element.querySelector('.btn-select-new-date')).to.exist;
    expect(this.element.querySelector('.btn-select-new-date').textContent.trim()).to.equal(
      'Select a date from the calendar',
    );
  });

  it('shows the calendar when the new date button is pressed', async function () {
    this.set('value', null);

    await render(hbs`<Attributes::ValueDate @value={{this.value}} />`);
    expect(this.element.querySelector('.btn-hide')).not.to.exist;
    expect(this.element.querySelector('.calendar-container')).to.have.class('d-none');

    await click('.btn-select-new-date');

    expect(this.element.querySelector('.btn-select-new-date')).not.to.exist;

    expect(this.element.querySelector('.select-new-date-message').textContent.trim()).to.equal(
      'Select a date from the calendar',
    );
    expect(this.element.querySelector('.btn-hide')).to.exist;
    expect(this.element.querySelector('.calendar-container')).not.to.have.class('d-none');
  });

  it('renders the selected value', async function () {
    this.set('value', '2021-09-22');

    await render(hbs`<Attributes::ValueDate @value={{this.value}} />`);

    expect(this.element.querySelector('.current-value').textContent.trim()).to.equal('2021-09-22');

    expect(this.element.querySelector('.datepicker-cell.day.selected').textContent.trim()).to.equal('22');
  });

  it('triggers a change event when a date is clicked', async function () {
    this.set('value', '2021-09-22');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Attributes::ValueDate @value={{this.value}} @onChange={{this.change}} />`);

    await click('.btn-change');

    await click('.datepicker-cell.day');

    expect(value).to.equal('2021-08-29');
  });

  it('hides the calendar on click outside', async function () {
    this.set('value', '2021-09-22');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Attributes::ValueDate @value={{this.value}} @onChange={{this.change}} />`);

    await click('.btn-change');

    await click(this.element.parentElement);

    expect(this.element.querySelector('.calendar-container')).to.have.class('d-none');

    expect(value).not.to.exist;
  });
});
