/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action, get } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class ArticleViewerComponent extends Component {
  @service router;
  @service space;
  @service fastboot;

  @tracked _blocks;

  getStyle(name) {
    return get(this.args.options ?? {}, `${name}Style`) || get(this.args.value ?? {}, `${name}Style`);
  }

  get paragraphStyle() {
    return this.getStyle('paragraph');
  }

  get h1Style() {
    return this.getStyle('h1');
  }

  get h2Style() {
    return this.getStyle('h2');
  }

  get h3Style() {
    return this.getStyle('h3');
  }

  get h4Style() {
    return this.getStyle('h4');
  }

  get h5Style() {
    return this.getStyle('h5');
  }

  get h6Style() {
    return this.getStyle('h6');
  }

  get rawValue() {
    if (this.args.value?.content) {
      return this.args.value?.content;
    }

    if (!this.args.value?.blocks) {
      return { blocks: [] };
    }

    return this.args.value;
  }

  get value() {
    if (this._blocks) {
      return { blocks: this._blocks };
    }

    return this.rawValue;
  }

  get isChanged() {
    if (this.value.blocks?.length != this._blocks?.length) {
      return true;
    }

    const newBlocks = this.toNiceBlocks(this.value.blocks);

    for (const i in newBlocks) {
      if (this._blocks[i].modelKey && !newBlocks[i].modelKey) {
        newBlocks[i].modelKey = this._blocks[i].modelKey;
      }

      if (JSON.stringify(newBlocks[i]) != JSON.stringify(this._blocks[i])) {
        return true;
      }
    }

    return false;
  }

  @action
  reset() {
    later(() => (this._blocks = this.toNiceBlocks(this.rawValue.blocks)));
    later(() => this.updateLinks());
  }

  @action
  setup(element) {
    this.containerElement = element;

    if (this._blocks) {
      return;
    }

    later(() => {
      this._blocks = this.toNiceBlocks(this.value.blocks);

      later(() => {
        this.updateLinks();
      });
    });
  }

  @action
  updateLinks() {
    const links = this.containerElement.querySelectorAll('a');

    for (const link of links) {
      let target = this.args.target;
      const href = link.getAttribute('href');

      if (this.space.isExternalUrl(href)) {
        target = '_blank';
      }

      if (!target) {
        continue;
      }

      link.setAttribute('target', target);
    }
  }

  @action
  updateProps(event) {
    let blocks = [];
    if (Array.isArray(event.detail.value)) {
      blocks = event.detail.value;
    }

    if (Array.isArray(event.detail.value?.blocks)) {
      blocks = event.detail.value.blocks;
    }

    later(() => {
      this._blocks = this.toNiceBlocks(blocks);

      later(() => {
        this.updateLinks();
      });
    });
  }

  addHashTagLinks(content) {
    if (typeof content != 'string') {
      return content;
    }

    const pieces = content?.split('#') ?? [];

    pieces.forEach((element, index) => {
      if (index == 0) {
        return;
      }

      if (element.length == 0) {
        pieces[index] = '#';
        return;
      }

      const prev = pieces[index - 1];

      if ((prev.endsWith('&') || !prev.endsWith(' ')) && prev.length > 0) {
        pieces[index - 1] = pieces[index - 1] + '#';
        return;
      }

      if (element.substring(0, 1) == ' ') {
        pieces[index] = '#' + element;
        return;
      }

      const sectionPieces = element.split(' ');
      let url;

      try {
        url = this.router.urlFor('browse.index') + '?tag=' + sectionPieces[0];
      } catch (err) {
        url = `/test/${sectionPieces[0]}`;
      }
      sectionPieces[0] = `<a href="${url}">#${sectionPieces[0]}</a>`;

      pieces[index] = sectionPieces.join(' ');
    });

    return pieces.join('');
  }

  getTextBlock(block) {
    return {
      type: block.type,
      data: {
        ...block.data,
        text: htmlSafe(this.addHashTagLinks(block.data.text)),
      },
    };
  }

  getList(block) {
    const updateContent = (a) => {
      if (typeof a == 'string') {
        return htmlSafe(this.addHashTagLinks(a));
      }

      if (a.content) {
        a.content = htmlSafe(this.addHashTagLinks(a.content));
      }

      return a;
    };

    const items = block.data.items.map(updateContent);

    return {
      type: block.type,
      data: {
        ...block.data,
        items,
      },
    };
  }

  getQuote(block) {
    return {
      type: block.type,
      data: {
        ...block.data,
        text: htmlSafe(block.data.text),
        caption: htmlSafe(block.data.caption),
      },
    };
  }

  getTable(block) {
    const header = block.data.content[0].map((a) => htmlSafe(this.addHashTagLinks(a)));
    const body = [];

    for (let i = 1; i < block.data.content.length; i++) {
      body.push(block.data.content[i].map((a) => htmlSafe(this.addHashTagLinks(a))));
    }

    return {
      type: block.type,
      data: {
        header,
        body,
      },
    };
  }

  get blocks() {
    if (this.fastboot.isFastBoot) {
      return this.toNiceBlocks(this.value.blocks);
    }

    return this._blocks;
  }

  get plainBlocks() {
    return this.value.blocks
      .map((block) => {
        if (block.type === 'header') {
          return block.data?.text ?? '';
        }

        if (block.type === 'paragraph') {
          return block.data?.text ?? '';
        }

        if (block.type === 'list') {
          return block.data.items.join(', ');
        }

        if (block.type === 'quote') {
          return `"${block.data?.text ?? ''}"`;
        }

        return `-- ${block.type} --`;
      })
      .join('\n\n');
  }

  toNiceBlocks(blocks) {
    return blocks.map((block) => {
      if (block.type === 'header') {
        return this.getTextBlock(block);
      }

      if (block.type === 'paragraph') {
        return this.getTextBlock(block);
      }

      if (block.type === 'list') {
        return this.getList(block);
      }

      if (block.type === 'quote') {
        return this.getQuote(block);
      }

      if (block.type === 'table') {
        return this.getTable(block);
      }

      return block;
    });
  }
}
