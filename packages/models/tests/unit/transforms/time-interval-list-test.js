/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { expect } from 'chai';

import { setupTest, it } from 'dummy/tests/helpers';

module('Unit | Transform | time interval list', function (hooks) {
  setupTest(hooks);
  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:time-interval-list');
  });

  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('converts an undefined to an empty list', function (a) {
    let deserialized = transform.deserialize(undefined);

    expect(deserialized).to.deep.equal([]);
  });

  it('converts a list of intervals', function (a) {
    let deserialized = transform.deserialize([
      {
        begin: '2022-01-01T01:00:00Z',
        end: '2022-01-03T01:00:00Z',
      },
    ]);

    a.deepEqual(
      deserialized.map((a) => a.toJSON()),
      [
        {
          begin: '2022-01-01T01:00:00.000Z',
          end: '2022-01-03T01:00:00.000Z',
        },
      ],
    );
  });

  it('serializes a list of objects', function (a) {
    let deserialized = transform.deserialize([
      {
        begin: '2022-01-01T01:00:00Z',
        end: '2022-01-03T01:00:00Z',
      },
    ]);

    a.deepEqual(transform.serialize(deserialized), [
      {
        begin: '2022-01-01T01:00:00.000Z',
        end: '2022-01-03T01:00:00.000Z',
      },
    ]);
  });
});
