import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class UserLocationComponent extends Component {
  @service position;

  get hasPosition() {
    return this.position?.center?.[0] && this.position?.center?.[1];
  }
}
