/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe('Unit | Service | modal', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  it('exists', function() {
    let service = this.owner.lookup('service:modal');
    expect(service).to.be.ok;
  });
});
