/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { DateTime } from 'luxon';
import { service } from '@ember/service';

export default class PageColEventDatesComponent extends Component {
  @service intl;
  @tracked record;

  addDateKeys(prefix, dest, date) {
    const localDate = date.setZone('default');

    dest[`${prefix}Time`] = localDate.toLocaleString(DateTime.TIME_SIMPLE);
    dest[`${prefix}Date`] = localDate.toLocaleString(DateTime.DATE_MED);
    dest[`${prefix}Y`] = localDate.toFormat('yyyy');
    dest[`${prefix}M`] = localDate.toFormat('MMMM');
    dest[`${prefix}D`] = localDate.toFormat('d');
    dest[`${prefix}DWeek`] = localDate.toFormat('EEEE');
  }

  toNiceString(entry) {
    let vars = {};

    this.addDateKeys('begin', vars, entry.begin);
    this.addDateKeys('end', vars, entry.end);

    if (entry.intervalEnd) {
      this.addDateKeys('intervalEnd', vars, entry.intervalEnd);
    }

    let intervalT =
      `${vars.beginD} ${vars.beginM}` == `${vars.endD} ${vars.endM}`
        ? `event_date.interval_same_day`
        : `event_date.interval_days_${entry.repetition}`;

    return (
      this.intl.t(`event_date.repeats_${entry.repetition}`, vars) +
      ', ' +
      this.intl.t(intervalT, vars)
    );
  }

  get niceEntries() {
    return this.record?.entries?.map((a) => this.toNiceString(a));
  }

  @action
  loaded(record) {
    this.record = record;
  }
}
