/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, fillIn, waitFor, triggerEvent, waitUntil } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/articles/add-message', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedArticle;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();

    receivedArticle = null;
  });

  it('redirects to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/articles/add-message?newsletter=123`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Farticles%2Fadd-message%3Fnewsletter%3D123');
  });

  describe('when an user is authenticated', function (hooks) {
    let newsletter;

    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultArticle('000000000000000000000001');
      server.testData.storage.addDefaultTeam();

      server.testData.storage.addStat(`newsletter-subscribers-newsletter-5ca78e2160780601008f69ff`, 20);
      server.testData.storage.addStat(`newsletter-emails-newsletter-5ca78e2160780601008f69ff`, 30);
      server.testData.storage.addStat(`newsletter-open-newsletter-5ca78e2160780601008f69ff`, 40);
      server.testData.storage.addStat(`newsletter-interactions-newsletter-5ca78e2160780601008f69ff`, 50);

      newsletter = server.testData.storage.addDefaultNewsletter();

      server.post('/mock-server/articles', (request) => {
        receivedArticle = JSON.parse(request.requestBody);
        receivedArticle.article._id = '1';

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedArticle)];
      });
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();

      await visit(`/manage/articles/add-message?newsletter=${newsletter._id}`);

      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Dashboard',
        'Open Green Map',
        'my first newsletter',
        'New message',
      ]);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
        '/manage/dashboards/newsletter/5ca78e2160780601008f69ff',
      ]);
    });

    it('navigates to the newsletter dashboard on success', async function () {
      authenticateSession();

      await visit(`/manage/articles/add-message?newsletter=${newsletter._id}`);
      expect(currentURL()).to.equal(`/manage/articles/add-message?newsletter=${newsletter._id}`);

      await waitFor('.editor-is-ready', { timeout: 3000 });

      await click('.add-title-link');

      await waitUntil(() => !this.element.querySelector('.alert-danger'));

      await click('.add-paragraph-link');

      await waitUntil(() => !this.element.querySelector('.add-paragraph-link'));

      await fillIn('.input-subject', 'new-subject');

      await fillIn('.ce-paragraph', 'test message');
      await fillIn('.ce-paragraph', 'test message');

      await fillIn('.datepicker-input', '2025-04-20');

      this.element.querySelector('.form-select.hour').value = '12';
      await triggerEvent('.form-select.hour', 'change');

      this.element.querySelector('.form-select.minute').value = '30';
      await triggerEvent('.form-select.minute', 'change');

      expect(this.element.querySelector('.btn-submit')).not.have.attribute('disabled');

      await click('.btn-submit');

      expect(currentURL()).to.equal('/manage/dashboards/newsletter/5ca78e2160780601008f69ff');
    });
  });
});
