/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | effect/scroll-parallax', function (hooks) {
  setupRenderingTest(hooks);

  it('sets the top positions when the effect is initialized', async function () {
    this.set('isReady', false);
    await render(hbs`
      <div class="container" style="height: 300px; overflow: auto;">
        {{#if this.isReady}}
          <Effect::ScrollParallax style="height: 400px">
            <div class="layer">a</div>
            <div class="layer">b</div>
            <div class="content">c</div>
            <div class="layer">d</div>
          </Effect::ScrollParallax>
        {{else}}
          <div style="height: 400px"></div>
        {{/if}}
      </div>
    `);

    this.element.querySelector('.container').scrollTop = 50;

    this.set('isReady', true);

    expect(this.element.querySelector('.content')).to.have.attribute(
      'style',
      'transform: translate3d(0px, 0px, 0px); opacity: 0.88;',
    );
  });
});
