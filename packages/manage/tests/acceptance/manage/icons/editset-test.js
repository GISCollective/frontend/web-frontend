/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest, wait } from 'dummy/tests/helpers';
import {
  visit,
  currentURL,
  waitUntil,
  click,
  triggerEvent,
  fillIn,
  blur,
  triggerKeyEvent,
  settled,
} from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/icons/editset', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedIconSet;
  let team;
  let iconSet;
  let isDeleted;
  let receivedPicture;

  hooks.beforeEach(function () {
    server = new TestServer();
    team = server.testData.create.team();
    iconSet = server.testData.create.iconSet();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultTeam('5ca78e2160780601008f69e6');
    server.testData.storage.addTeam(team);
    server.testData.storage.addIconSet(iconSet);

    const defaultPicture = server.testData.create.picture();

    isDeleted = false;
    server.delete(`/mock-server/iconsets/${iconSet._id}`, () => {
      isDeleted = true;

      return [204, {}, ''];
    });
    server.put(`/mock-server/iconsets/${iconSet._id}`, (request) => {
      server.testData.storage.addDefaultIconSet();

      receivedIconSet = JSON.parse(request.requestBody);
      receivedIconSet.iconSet._id = iconSet._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedIconSet)];
    });

    server.get(`/mock-server/iconsets/${iconSet._id}/categories`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        iconSetCategories: [{ category: 'category 1' }, { category: 'category 2' }],
      }),
    ]);

    server.get(`/mock-server/iconsets/${iconSet._id}/subcategories`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        iconSetSubcategories: [{ subcategory: 'subcategory 1' }, { subcategory: 'subcategory 2' }],
      }),
    ]);

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    receivedIconSet = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Ficons%2Fedit%2F5ca7b702ecd8490100cab96f');
  });

  it('can visit /manage/icons/editset', async function () {
    authenticateSession();
    await visit(`/manage/icons/edit/${iconSet._id}`);
    expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}`);

    expect(this.element.querySelector('.error')).not.to.exist;
    expect(PageElements.breadcrumbs()).to.deep.equal([
      'Dashboard',
      'Open Green Map',
      'Green Map® Icons Version 3',
      'Edit',
    ]);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal('Edit icon set');
    expect(PageElements.rowTitles()).to.deep.equal([
      'name and description',
      'cover photo',
      'styles',
      'Points',
      'Lines',
    ]);
  });

  it('can change the name', async function () {
    authenticateSession();
    await visit(`/manage/icons/edit/${iconSet._id}`);

    await wait(200);
    await click('.row-article .btn-edit');

    await wait(200);
    await click('.ce-header');
    await fillIn('.ce-header', 'new name');

    await wait(200);
    await triggerKeyEvent('.ce-header', 'keyup', 'Enter');

    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));
    await click('.btn-submit');

    await waitUntil(() => receivedIconSet);
    expect(receivedIconSet.iconSet.name).to.equal('new name');
  });

  it('can change the description', async function () {
    authenticateSession();
    await visit(`/manage/icons/edit/${iconSet._id}`);

    await wait(200);
    await click('.row-article .btn-edit');

    await click('.ce-paragraph');
    await wait(200);

    await fillIn('.ce-paragraph', 'new description');
    await wait(200);

    await click('.ce-header');

    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
    await click('.btn-submit');

    await waitUntil(() => receivedIconSet);
    expect(receivedIconSet.iconSet.description.blocks).to.deep.equal([
      {
        type: 'header',
        data: { text: 'Green Map® Icons Version 3', level: 1 },
      },
      { type: 'paragraph', data: { text: 'new description' } },
    ]);
  });

  it('can change the cover', async function () {
    authenticateSession();
    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);

    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-cover-photo input[type='file']", 'change', {
      files: [blob],
    });

    await waitUntil(() => receivedIconSet);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'IconSet', id: iconSet._id },
      data: {},
      disableOptimization: false,
    });

    expect(receivedIconSet.iconSet.cover).to.equal('5cc8dc1038e882010061545a');
    await PageElements.wait(100);
  });

  it('can change the styles', async function () {
    authenticateSession();
    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);

    await click('.container-group-styles .btn-edit');

    let colorInput = this.element.querySelector('.col-site .icon-style-background-color');
    colorInput.value = '#cc0011';
    await triggerEvent(colorInput, 'change');
    await blur(colorInput);

    colorInput = this.element.querySelector('.col-line .icon-style-background-color');
    colorInput.value = '#cc0022';
    await triggerEvent(colorInput, 'change');
    await blur(colorInput);

    colorInput = this.element.querySelector('.col-polygon .icon-style-background-color');
    colorInput.value = '#cc0033';
    await triggerEvent(colorInput, 'change');
    await blur(colorInput);

    await click('.container-group-styles .btn-submit');

    await waitUntil(() => receivedIconSet);

    expect(receivedIconSet.iconSet.styles.site).to.deep.equal({
      isVisible: true,
      shape: 'circle',
      borderColor: 'white',
      backgroundColor: '#cc0011',
      borderWidth: 1,
      size: 21,
    });

    expect(receivedIconSet.iconSet.styles.line).to.deep.equal({
      borderColor: 'blue',
      backgroundColor: '#cc0022',
      borderWidth: 1,
      lineDash: [],
    });

    expect(receivedIconSet.iconSet.styles.polygon).to.deep.equal({
      borderColor: 'blue',
      backgroundColor: '#cc0033',
      borderWidth: 1,
      lineDash: [],
      showAsLineAfterZoom: 99,
    });
  });

  it('should be able to change the team', async function () {
    authenticateSession();

    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);

    await PageElements.openSideBar(this.element);

    await click('.container-group-team .btn-edit');

    this.element.querySelector('.value.editing select').value = '000000000000000000000001';
    await triggerEvent('.value.editing select', 'change');

    await waitUntil(() => receivedIconSet != null, { timeout: 3000 });

    expect(receivedIconSet.iconSet.visibility.team).to.equal('000000000000000000000001');
  });

  it('should render the "is public" label', async function () {
    authenticateSession();

    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain('is public');
    expect(this.element.querySelector('.container-group-is-public label').textContent).to.contain('is public');
  });

  it('should be able to unsubmit the iconset', async function () {
    authenticateSession();

    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    await PageElements.openSideBar(this.element);

    await click(this.element.querySelector('.side-bar .container-group-is-public .btn-switch'));

    await waitUntil(() => receivedIconSet != null, { timeout: 3000 });

    expect(receivedIconSet.iconSet.visibility.isPublic).to.equal(false);
  });

  it('should render the "is default" label', async function () {
    authenticateSession();

    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain('is default');
    expect(this.element.querySelector('.container-group-is-default label').textContent).to.contain('is default');
  });

  it('should be able to set the iconset to not be default', async function () {
    authenticateSession();

    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-is-default .btn-switch'));

    await waitUntil(() => receivedIconSet != null, { timeout: 3000 });

    expect(receivedIconSet.iconSet.visibility.isDefault).to.equal(false);
  });

  it('should not show the show all teams button', async function () {
    authenticateSession();
    await visit(`/manage/icons/edit/${iconSet._id}`);
    await PageElements.waitEditorJs(this.element);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).not.to.exist;
  });

  describe('when the authenticated user is an administrator', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('should show all teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}`);
      await PageElements.waitEditorJs(this.element);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?all=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}?allTeams=true`);
      expect(server.history).to.contain(`GET /mock-server/teams?all=true`);

      expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).to.exist;
    });

    it('should show owned teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}?allTeams=true`);
      await PageElements.waitEditorJs(this.element);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?edit=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}`);
      expect(server.history).to.contain(`GET /mock-server/teams?edit=true`);

      expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).to.exist;
    });
  });
});
