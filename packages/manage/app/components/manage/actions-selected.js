/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageActionsSelectedComponent extends Component {
  @tracked isBusy;
  @tracked isDeleting;
  @tracked currentAction;

  @action
  async handleDelete() {
    this.isBusy = true;
    this.isDeleting = true;

    let result;

    this.args.selected == 'all'
      ? await this.args.onAction('deleteAll', 'all')
      : await this.args.onDelete(this.args.selected);

    this.clearSelection();

    this.isBusy = false;
    this.isDeleting = false;

    return result;
  }

  @action
  async handleAction(key) {
    this.isBusy = true;
    this.currentAction = key;

    let result;

    try {
      result = await this.args.onAction(key, this.args.selected);
    } catch (err) {
      console.error(err);
    }

    this.isBusy = false;
    this.currentAction = '';

    return result;
  }

  @action
  clearSelection() {
    this.args.onClearSelection?.();
  }
}
