import {
  TrackedArray,
} from 'tracked-built-ins';

export const A = (a) => TrackedArray.from(a ?? []);