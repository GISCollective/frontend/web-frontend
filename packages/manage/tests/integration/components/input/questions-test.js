/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | input/questions', function (hooks) {
  setupRenderingTest(hooks);

  let value;

  hooks.beforeEach(function () {
    this.set('change', (v) => {
      value = v;
    });
  });

  it('can add a new question', async function (assert) {
    await render(hbs`<Input::Questions @onChange={{this.change}} />`);

    expect(this.element.querySelectorAll('.input-question')).to.have.length(0);

    await click('.btn-add-question');

    expect(this.element.querySelectorAll('.input-question')).to.have.length(1);
  });

  it('can render a question', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        name: 'question',
        help: 'help message',
        type: 'short text',
        isRequired: true,
      },
    ]);

    await render(hbs`<Input::Questions @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector('.text-question').value).to.equal('some question');
    expect(this.element.querySelector('.text-help').value).to.equal('help message');
    expect(this.element.querySelector('.text-name').value).to.equal('question');
    expect(this.element.querySelector('.select-type').value).to.equal('short text');
    expect(this.element.querySelector('.chk-is-required').checked).to.equal(true);
    expect(this.element.querySelector('.btn-toggle-visibility')).not.to.exist;
  });

  it('can change a question', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        isRequired: true,
      },
    ]);

    await render(hbs`<Input::Questions @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('.text-question', 'new question');
    await fillIn('.text-help', 'new help message');
    await fillIn('.text-name', 'new field');

    expect(value).to.deep.equal([
      {
        question: 'new question',
        help: 'new help message',
        type: 'short text',
        name: 'new field',
        isRequired: true,
        options: undefined,
      },
    ]);
  });

  it('can delete a question', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        isRequired: true,
      },
    ]);

    await render(hbs`<Input::Questions @value={{this.value}} @onChange={{this.change}} />`);

    await click('.btn-delete');

    expect(this.element.querySelectorAll('.input-question')).to.have.length(0);
    expect(value).to.deep.equal([]);
  });

  it('can not delete a question when disableDelete is true', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        isRequired: true,
        disableDelete: true,
      },
    ]);

    await render(hbs`<Input::Questions @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector('.btn-delete')).not.to.exist;
  });

  it('shows a hide toggle button when hasVisibility is true', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        isRequired: true,
        hasVisibility: true,
        isVisible: true,
      },
    ]);

    await render(hbs`<Input::Questions @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector('.btn-toggle-visibility')).to.exist;
    expect(this.element.querySelector('.text-name')).not.to.exist;
    expect(this.element.querySelector('.select-type')).not.to.exist;
  });

  it('can toggle a question when hasVisibility is true', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        name: 'email',
        isRequired: true,
        hasVisibility: true,
        isVisible: true,
      },
    ]);

    await render(hbs`<Input::Questions @value={{this.value}} @onChange={{this.change}} />`);

    await click('.btn-toggle-visibility');
    expect(this.element.querySelector('.input-question')).not.to.exist;

    expect(value).to.deep.equal([
      {
        question: 'some question',
        help: 'help message',
        name: 'email',
        type: 'short text',
        isRequired: true,
        hasVisibility: true,
        isVisible: false,
      },
    ]);

    expect(this.element.querySelector('.alert.alert-info')).to.have.textContent(
      "The user won't be asked to enter an email.",
    );
  });

  it('shows an alert when there are two items with the same name', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        name: 'email',
      },
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        name: 'email',
      },
    ]);

    await render(hbs`<Input::Questions @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector('.alert.alert-danger')).to.have.textContent(
      'There is already a question with the same name.',
    );
  });
});
