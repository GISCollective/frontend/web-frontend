/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class EditorTextWithOptionsComponent extends Component {
  get isHeading() {
    if (this.args.element?.attributes?.getNamedItem?.("data-selectable-level")?.value.toLowerCase?.() == "false") {
      return false;
    }

    const tagName = this.args.element?.tagName?.toLowerCase?.();

    return ["h1", "h2", "h3", "h4", "h5", "h6"].includes(tagName);
  }

  get enableTextValue() {
    return this.args.element?.attributes?.getNamedItem?.("data-text-value")?.value.toLowerCase?.() == "true";
  }
}
