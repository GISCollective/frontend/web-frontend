/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../base/authenticated-route';
import { hash } from 'rsvp';
import { DataBindingDestination } from '../../transforms/data-binding-destination';
import { Connection } from '../../transforms/connection';

export default class AddDataBindingRoute extends AuthenticatedRoute {
  modelsToClear = ['data-binding'];

  queryParams = {
    all: { refreshModel: true },
  };

  async getTeams(all) {
    const params = {};

    if (all) {
      params.all = all;
    } else {
      params.edit = true;
      params.allowCustomDataBindings = true;
    }

    return await this.store.query('team', params);
  }

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const teamParams = { edit: true };

    if (params.all) {
      teamParams.all = params.all;
    }

    const teams = await this.getTeams(params.all);

    if (!this.user.isAdmin && teams.length == 0) {
      this.router.transitionTo('/');
      return;
    }

    return hash({
      dataBinding: this.store.createRecord('data-binding', {
        name: '',
        destination: new DataBindingDestination(),
        descriptionTemplate: '',
        fields: {},
        extraIcons: [],
        updateBy: '',
        crs: '',
        type: '',
        connection: new Connection({}),
        visibility: {
          isPublic: false,
          team: null,
        },
      }),
      teams,
    });
  }
}
