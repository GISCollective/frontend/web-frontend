/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from '../../base/app-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class BrowseTeamsTeam extends AppRoute {
  @service store;
  @service headData;

  async model(params) {
    const team = await this.store.findRecord('team', params.id);
    const campaigns = await this.store.query('campaign', { team: params.id });

    const users = [];

    const owners = await team.owners;
    for(let user of owners) {
      users.push(user);
    }

    const leaders = await team.leaders;
    for(let user of leaders) {
      users.push(user);
    }

    const _members = await team.members;
    for(let user of _members) {
      users.push(user);
    }

    const guests = await team.guests;
    for(let user of guests) {
      users.push(user);
    }

    const members = Promise.all(users.map((a) => this.store.findRecord('user-profile', a.id)));

    return hash({
      team,
      campaigns,
      members,
      maps: this.store.query('map', { team: params.id }),
      pictures: team.pictures,
      logo: team.logo,
    });
  }

  async afterModel(model) {
    await model.team.get('logo');
    await model.team.fillMetaInfo(this.headData);
  }
}
