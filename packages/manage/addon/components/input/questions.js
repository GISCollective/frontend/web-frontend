/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { A } from 'core/lib/array';
import { later } from '@ember/runloop';

export default class InputQuestionsComponent extends Component {
  @tracked value = A();

  @action
  setup() {
    later(() => {
      for(let item of this.args.value ?? []) {
        this.value.push(item);
      }
    });
  }

  @action
  addRecord() {
    this.value.push({
      isVisible: true,
    });
  }

  @action
  change(index, value) {
    return new Promise((success, reject) => {
      later(() => {
        try {
          this.value.splice(index, 1, value);

          return success(this.args.onChange(this.value));
        } catch (err) {
          reject(err);
        }
      });
    });
  }

  @action
  remove(index) {
    this.value.splice(index, 1);

    return this.args.onChange(this.value);
  }

  @action
  toggleVisibility(index) {
    const question = { ...this.value[index] };
    question.isVisible = !question.isVisible;

    this.value.splice(index, 1, question);

    return this.args.onChange(this.value);
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    return this.args.onChange(this.value);
  }
}
