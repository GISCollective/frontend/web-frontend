/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it } from "dummy/tests/helpers";
import { setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | input/options/container-options", function (hooks) {
  setupRenderingTest(hooks);

  it("can change the width", async function () {
    let value;

    this.set("change", (v) => {
      value = v.toJSON();
    });

    await render(hbs`<Input::Options::ContainerOptions @onChange={{this.change}} />`);

    expect(this.element.querySelector(".width-select").value).to.equal("default");

    this.element.querySelector(".width-select").value = "fluid";
    await triggerEvent(".width-select", "change");

    expect(value).to.deep.equal({
      rows: [],
      options: ["container-fluid"],
      height: "default",
      visibility: "always",
      layers: { count: 1, effect: "none", showContentAfter: 1 },
    });
  });

  it("can reset the width", async function () {
    let value;

    this.set("change", (v) => {
      value = v.toJSON();
    });

    this.set("container", { options: ["container-fluid"] });

    await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChange={{this.change}} />`);

    expect(this.element.querySelector(".width-select").value).to.equal("fluid");

    this.element.querySelector(".width-select").value = "default";
    await triggerEvent(".width-select", "change");

    expect(value).to.deep.equal({
      rows: [],
      options: [],
      visibility: "always",
      height: "default",
      layers: { count: 1, effect: "none", showContentAfter: 1 },
    });
  });

  it("can change the container height", async function () {
    let value;

    this.set("change", (v) => {
      value = v.toJSON();
    });

    this.set("container", { options: ["container-fluid", "min-vh-20"] });

    await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChange={{this.change}} />`);

    expect(this.element.querySelector(".input-sm-min-vh").value).to.equal("20");

    this.element.querySelector(".input-sm-min-vh").value = "30";
    await triggerEvent(".input-sm-min-vh", "change");

    expect(value).to.deep.equal({
      rows: [],
      options: ["container-fluid", "min-vh-30"],
      visibility: "always",
      height: "default",
      layers: { count: 1, effect: "none", showContentAfter: 1 },
    });
  });

  it("can change the container alignment", async function () {
    let value;

    this.set("change", (v) => {
      value = v.toJSON();
    });

    this.set("container", { options: ["justify-content-start"] });

    await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChange={{this.change}} />`);

    expect(this.element.querySelector(".input-sm-justify-content").value).to.deep.equal("start");

    this.element.querySelector(".input-sm-justify-content").value = "center";
    await triggerEvent(".input-sm-justify-content", "change");

    expect(value).to.deep.equal({
      rows: [],
      options: ["justify-content-center"],
      visibility: "always",
      height: "default",
      layers: { count: 1, effect: "none", showContentAfter: 1 },
    });
  });

  it("can change the visibility rule", async function () {
    let value;

    this.set("change", (v) => {
      value = v.toJSON();
    });

    this.set("container", { options: ["justify-content-start"] });

    await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChange={{this.change}} />`);

    expect(this.element.querySelector(".visibility-select").value).to.equal("always");
    this.element.querySelector(".visibility-select").value = "user:authenticated";
    await triggerEvent(".visibility-select", "change");

    expect(value).to.deep.equal({
      rows: [],
      options: ["justify-content-start"],
      height: "default",
      visibility: "user:authenticated",
      layers: { count: 1, effect: "none", showContentAfter: 1 },
    });
  });

  it("renders the visibility value", async function () {
    this.set("container", {
      options: ["justify-content-start"],
      visibility: "user:authenticated",
    });

    await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} />`);

    expect(this.element.querySelector(".visibility-select").value).to.equal("user:authenticated");
  });

  describe("the layers section", function (hooks) {
    it("can change the layers count", async function () {
      let value;

      this.set("change", (v) => {
        value = v.toJSON();
      });

      this.set("container", { options: ["justify-content-start"] });

      await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChange={{this.change}} />`);

      await fillIn(".form-control.count", 3);
      await fillIn(".show-content-after", 2);

      expect(value).to.deep.equal({
        height: "default",
        rows: [],
        options: ["justify-content-start"],
        visibility: "always",
        layers: { count: 3, effect: "none", showContentAfter: 2 },
      });
    });

    it("can not set a layer count < 1", async function () {
      let value;

      this.set("change", (v) => {
        value = v.toJSON();
      });

      this.set("container", { options: ["justify-content-start"] });

      await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChange={{this.change}} />`);

      await fillIn(".form-control.count", 0);
      await fillIn(".show-content-after", 2);

      expect(value).to.deep.equal({
        height: "default",
        rows: [],
        options: ["justify-content-start"],
        visibility: "always",
        layers: { count: 1, effect: "none", showContentAfter: 1 },
      });
    });

    it("renders the layer values", async function () {
      this.set("container", {
        layers: { count: 10, effect: "none", showContentAfter: 5 },
      });

      await render(hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChange={{this.change}} />`);

      expect(this.element.querySelector(".form-control.count").value).to.equal("10");
      expect(this.element.querySelector(".show-content-after").value).to.equal("5");
    });
  });
});
