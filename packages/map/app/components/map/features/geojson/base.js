/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import Feature from 'ol/Feature';

export default class MapFeaturesGeojsonPolygonComponent extends Component {
  @tracked source;
  @tracked feature;

  @action
  setup() {
    if (!this.args.source) {
      return;
    }

    this.feature = new Feature();
    this.source = this.args.source;

    this.source.addFeature(this.feature);

    if (this.args.value) {
      this.updateValue();
    }
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (!this.source) {
      return;
    }

    let exists;
    this.source.forEachFeature((item) => {
      exists = exists || item == this.feature;
    });

    if (exists) {
      this.source.removeFeature(this.feature);
    }
  }

  @action
  updateValue() {
    if (!this.source) {
      return this.setup();
    }

    const geometry = this.getGeometry();

    this.feature.setGeometry(geometry);
  }
}
