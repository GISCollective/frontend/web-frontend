/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import RESTAdapter from '@ember-data/adapter/rest';
import { service } from '@ember/service';
import { getOwner } from '@ember/application';

export default class ApplicationAdapter extends RESTAdapter {
  useFetch = true;
  @service session;
  @service fastboot;
  @service intl;

  getParams(query) {
    if (!query) {
      return '';
    }

    return Object.keys(query)
      .map((a) => `${a}=${encodeURIComponent(query[a])}`)
      .join('&');
  }

  get headers() {
    let headers = {};

    if (this.session.isAuthenticated) {
      headers['Authorization'] = `Bearer ${this.session.data.authenticated.access_token}`;
    }

    headers['Accept-Language'] = this.intl.primaryLocale;

    return headers;
  }

  get host() {
    const config = getOwner(this).resolveRegistration('config:environment');

    try {
      // eslint-disable-next-line no-undef
      if (this.fastboot.isFastBoot && apiHost) {
        // eslint-disable-next-line no-undef
        return apiHost;
      }
    } catch (err) {
      if (err.message != 'apiHost is not defined') {
        // eslint-disable-next-line no-console
        console.error(err.message);
      }
    }

    if (this.fastboot.isFastBoot && config.fastbootApiUrl) {
      return config.fastbootApiUrl;
    }

    return config.apiUrl;
  }

  urlForQueryRecord(options, modelName) {
    if (typeof options == 'string') {
      return this.buildURL(modelName, options);
    }

    return super.urlForQueryRecord(...arguments);
  }

  buildURL(modelName, id, snapshot, requestType, query) {
    let url = super.buildURL(modelName.replace('-', ''), id, snapshot, requestType, query).toLowerCase();

    if (snapshot?.adapterOptions?.locale) {
      url += `?locale=${snapshot.adapterOptions.locale}`;
    }

    if (typeof snapshot?.adapterOptions?.validation == 'boolean') {
      url += `?validation=${snapshot.adapterOptions.validation}`;
    }

    return url;
  }

  async queryRecord(store, schema, query, options) {
    if (typeof query == 'string') {
      query = { id: query };
    }

    const id = query.id;
    query.id = undefined;

    let url = this.buildURL(schema.modelName, id);

    const vars = Object.keys(query)
      .filter((a) => query[a])
      .map((a) => `${a}=${query[a]}`);

    if (vars.length > 0) {
      url = `${url}?${vars.join('&')}`;
    }

    return this.ajax(url, 'GET');
  }

  publish(recordId) {
    const url = this.buildURL(this.modelName, recordId) + '/publish';
    return this.ajax(url, 'POST');
  }

  unpublish(recordId) {
    const url = this.buildURL(this.modelName, recordId) + '/unpublish';
    return this.ajax(url, 'POST');
  }

  publishMany(query) {
    const url = [this.urlForQuery(query, this.modelName) + '/publish', this.getParams(query)]
      .filter((a) => a)
      .join('?');

    return this.ajax(url, 'POST');
  }

  unpublishMany(query) {
    const url = [this.urlForQuery(query, this.modelName) + '/unpublish', this.getParams(query)]
      .filter((a) => a)
      .join('?');
    return this.ajax(url, 'POST');
  }

  deleteMany(query) {
    const url = [this.urlForQuery(query, this.modelName), this.getParams(query)].filter((a) => a).join('?');
    return this.ajax(url, 'DELETE');
  }
}
