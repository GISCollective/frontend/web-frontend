/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';

export default class BlocksComponent extends Component {
  get blocks() {
    if (typeof this.args.value == 'string') {
      return toContentBlocks(this.args.value);
    }

    return this.args.value;
  }

  get value() {
    return firstParagraph(this.blocks ?? { blocks: [] });
  }
}
