/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "./page-col-editor";

export default class ManageContainerBackgroundOptionsComponent extends Component {
  get availableTypes() {
    return this.pageCols.backgroundComponents;
  }
}
