/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import sanitizeHtml from 'sanitize-html';

export default class PageColImageComponent extends Component {
  get data() {
    return this.args.value?.data ?? {};
  }

  get caption() {
    return sanitizeHtml(this.data.caption ?? '', {
      allowedTags: [],
      allowedAttributes: [],
    });
  }

  get src() {
    return this.data.file?.url;
  }
}
