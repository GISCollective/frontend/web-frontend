/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "./base-options";
import { guidFor } from "@ember/object/internals";

export default class LayoutOptionsComponent extends Component {
  elementId = `layout-options-${guidFor(this)}`;
}
