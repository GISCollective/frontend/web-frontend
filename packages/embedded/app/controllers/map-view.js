import Controller from '@ember/controller';
import { service } from '@ember/service';
import { transform } from 'ol/proj';
import { action } from '@ember/object';

export default class MapViewController extends Controller {
  @service mapStyles;
  @service embeddedClient;
  @service position;
  @service tiles;

  get center() {
    if (!this.embeddedClient.value?.center) {
      return null;
    }

    return transform(
      this.embeddedClient.value?.center,
      'EPSG:4326',
      'EPSG:3857',
    );
  }

  get tilesUrl() {
    let url = `${this.tiles.url}/{z}/{x}/{y}?format=vt`;

    if (this.model.map) {
      url = `${url}&map=${this.model.map.id}`;
    }

    if (this.icons && this.icons !== '') {
      url = `${url}&icons=${this.icons}&iop=and`;
    }

    return url;
  }

  @action
  selectFeature() {}
}
