/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | decorators', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:decorators');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized).to.deep.equal({
      showAsLineAfterZoom: 99,
      useDefault: true,
      changeIndex: 0,
      maxZoom: 20,
      minZoom: 0,
      showLineMarkers: false,
      keepWhenSmall: true,
      center: [],
    });
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({
      showAsLineAfterZoom: 99,
      useDefault: true,
      changeIndex: 0,
      maxZoom: 20,
      minZoom: 0,
      showLineMarkers: false,
      keepWhenSmall: true,
      center: [],
    });

    expect(deserialized).to.deep.equal({
      showAsLineAfterZoom: 99,
      useDefault: true,
      changeIndex: 0,
      maxZoom: 20,
      minZoom: 0,
      showLineMarkers: false,
      keepWhenSmall: true,
      center: [],
    });
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize({
      showAsLineAfterZoom: 1,
      useDefault: false,
      changeIndex: 2,
      maxZoom: 3,
      minZoom: 4,
      showLineMarkers: true,
      keepWhenSmall: false,
      center: [1, 2],
    });

    expect(deserialized.toJSON()).to.deep.equal({
      showAsLineAfterZoom: 1,
      useDefault: false,
      changeIndex: 2,
      maxZoom: 3,
      minZoom: 4,
      showLineMarkers: true,
      keepWhenSmall: false,
      center: [1, 2],
    });
  });

  it('can serialize an object', function () {
    const decorators = {
      showAsLineAfterZoom: 1,
      useDefault: false,
      changeIndex: 2,
      maxZoom: 3,
      minZoom: 4,
      showLineMarkers: true,
      keepWhenSmall: false,
      center: [1, 2],
    };

    let deserialized = transform.deserialize(decorators);

    expect(transform.serialize(deserialized)).to.deep.equal(decorators);
  });
});
