/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { click, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { Review } from 'dummy/transforms/review';

describe('Integration | Component | manage/campaign-answer/review', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let server;
  let answer;
  let user;
  let approved;
  let rejected;
  let restored;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function() {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    const profile = server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultUser(false, user._id);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultCampaign();

    let icon = server.testData.storage.addDefaultIcon();

    answer = server.testData.storage.addDefaultCampaignAnswer();
    answer.icons = [icon._id];
    answer.info.originalAuthor = profile._id;

    server.testData.storage.addDefaultUser(answer.info.originalAuthor);

    let answerModel = await this.store.findRecord('campaign-answer', answer._id);
    this.set('answer', answerModel);

    approved = false;
    server.post(`/mock-server/campaignanswers/${answer._id}/approve`, (request) => {
      approved = true;

      return [201, {}];
    });

    rejected = false;
    server.post(`/mock-server/campaignanswers/${answer._id}/reject`, (request) => {
      rejected = true;

      return [201, {}];
    });

    restored = false;
    server.post(`/mock-server/campaignanswers/${answer._id}/restore`, (request) => {
      restored = true;

      return [201, {}];
    });
  });

  it('renders a pending message for an answer without a feature', async function () {
    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.card-title').textContent.trim()).to.equal('Review');
    expect(this.element.querySelector('.card-text').textContent.trim()).to.equal(
      'This contribution needs a review from some one from the team.',
    );
  });

  it('renders a processing message', async function () {
    this.answer.status = 0;

    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.card-text').textContent.trim()).to.equal(
      'This contribution is being processed.',
    );
  });

  it('renders a published message', async function () {
    this.answer.status = 2;

    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.card-text').textContent.trim()).to.equal(
      'This contribution is approved and published.',
    );
  });

  it('renders a unpublished message', async function () {
    this.answer.status = 3;

    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.card-text').textContent.trim()).to.equal(
      'This contribution is approved and unpublished.',
    );
  });

  it('renders a rejected message', async function () {
    this.answer.status = 4;

    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.card-text').textContent.trim()).to.equal('This contribution is rejected.');
  });

  it('renders a contribution message when the status is unknown', async function () {
    this.answer.status = 5;

    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.card-text').textContent.trim()).to.equal(
      'This contribution can not be reviewed.',
    );
  });

  it('can approve an answer', async function () {
    this.answer.status = 1;
    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.btn-revert')).not.to.exist;

    await click('.btn-approve');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(approved).to.be.true;
  });

  it('can reject an answer', async function () {
    this.answer.status = 1;
    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.btn-revert')).not.to.exist;

    await click('.btn-reject');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(rejected).to.be.true;
  });

  it('can restore an approved and published feature', async function () {
    this.answer.status = 2;
    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.btn-reject')).not.to.exist;
    expect(this.element.querySelector('.btn-approve')).not.to.exist;

    await click('.btn-revert');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(restored).to.be.true;
  });

  it('can restore an approved and published feature', async function () {
    this.answer.status = 3;
    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.btn-reject')).not.to.exist;
    expect(this.element.querySelector('.btn-approve')).not.to.exist;

    await click('.btn-revert');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(restored).to.be.true;
  });

  it('can restore an rejected feature', async function () {
    this.answer.status = 4;
    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.btn-reject')).not.to.exist;
    expect(this.element.querySelector('.btn-approve')).not.to.exist;

    await click('.btn-revert');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(restored).to.be.true;
  });

  it('shows the review author and date', async function () {
    this.answer.status = 4;
    this.answer.review = new Review(
      {
        reviewedOn: '2022-03-22T13:33:22',
        user: user._id,
      },
      this.store,
    );

    await render(hbs`<Manage::CampaignAnswer::Review @value={{this.answer}} />`);

    expect(this.element.querySelector('.profile-name').textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('.review-date').textContent.trim()).to.equal('3/22/2022\n    1:33:22 PM');
  });
});
