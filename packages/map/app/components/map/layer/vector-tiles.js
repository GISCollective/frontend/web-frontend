/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { service } from '@ember/service';

import MVT from 'ol/format/MVT';
import VectorTile from 'ol/layer/VectorTile';
import VectorTileSource from 'ol/source/VectorTile';

import { action } from '@ember/object';
import FeatureSelect from './interactions/FeatureSelect';
import config from '../../../config/environment';

import Component from '@glimmer/component';
import { later, cancel } from '@ember/runloop';
import { buildWaiter } from '@ember/test-waiters';
import { getOwner } from '@ember/application';

let tileWaiter = buildWaiter('tile-load-waiter');

export default class VectorTilesComponent extends Component {
  @service layer;
  @service featureStyle;
  @service session;
  @service loading;
  @service store;

  debugTiles = config.debugTiles;
  iconCache = {};
  neededIcons = [];
  ignoredIcons = [];

  constructor() {
    super(...arguments);

    this.source = new VectorTileSource({
      format: new MVT(),
      tileLoadFunction: (tile, url) => {
        tile.setLoader((extent, resolution, projection) => {
          if(this.isDestroyed) {
            return;
          }

          const xhr = new XMLHttpRequest();
          xhr.open('GET', url);
          xhr.responseType = 'arraybuffer';

          if (this.args.bearer) {
            xhr.setRequestHeader('Authorization', this.args.bearer);
          }

          const owner = getOwner(this.loading);
          if(!owner.isDestroyed || !this.loading.isDestroyed) {
            this.loading.isLoading = true;
          }

          let token = tileWaiter.beginAsync();

          xhr.onload = () => {
            const format = tile.getFormat();

            tile.setFeatures(
              format.readFeatures(xhr.response, {
                extent: extent,
                featureProjection: projection,
              }),
            );

            if(!owner.isDestroyed || !this.loading.isDestroyed) {
              this.loading.isLoading = false;
            }
            tileWaiter.endAsync(token);
          };

          xhr.send();
        });
      },
    });
  }

  get oldUrl() {
    return this.olLayer?.getSource?.()?.getUrls?.()?.[0];
  }

  setupLayerObject() {
    if (this.olLayer) {
      return false;
    }

    this.olLayer = this.layer.getCached(
      this.args.cacheId,
      'vt',
      this.args.map,
      () =>
        new VectorTile({
          source: this.source,
          declutter: false,
          renderMode: 'vector',
          zIndex: 9999,
        }),
    );

    this.olLayer.setSource(this.source);

    this.olLayer.setStyle((feature, resolution) => {
      return this.args.style?.(
        feature,
        resolution,
        this.args.selectedId,
        this.loadIcon,
      );
    });

    this.featureSelect = new FeatureSelect({
      layers: [this.olLayer],
    });

    this.featureSelect.notify = (feature) => {
      this.args.onSelect?.(feature, this.olLayer);
    };

    this.pointerMove = (event) => {
      this.handlePointerMove(event);
    };

    return true;
  }

  async loadNeededIcons() {
    while (this.neededIcons.length) {
      const id = this.neededIcons.pop();

      const icon = this.store.peekRecord('icon', id);

      if (icon) {
        continue;
      }

      try {
        await this.store.findRecord('icon', id);
      } catch (err) {
        this.ignoredIcons.push(id);
        console.log(`Can't load the icon with id: ${id}.`);
      }
    }

    this.neededIcons = [];
    this.olLayer.changed?.();
  }

  @action
  async loadIcon(id) {
    if (!this.neededIcons.includes(id) && !this.ignoredIcons.includes(id)) {
      this.neededIcons.push(id);
    }

    if (this.neededIcons.length == 0) {
      return;
    }

    cancel(this.iconLoadTimer);
    this.iconLoadTimer = later(() => {
      return this.loadNeededIcons();
    }, 50);
  }

  @action
  setupElement(element) {
    element.selectFeature = (feature) => {
      this.args.onSelect?.(feature, this.olLayer);
    };

    element.hoverFeature = (feature) => {
      this.args.onHover?.(feature);
    };

    later(() => {
      this.update();
    });
  }

  @action
  update() {
    let isChanged = this.setupLayerObject();

    if (this.oldMap != this.args.map) {
      this.removeFromMap();
      this.oldMap = this.args.map;
      this.args.map.getLayers().push(this.olLayer);
      this.args.map.addInteraction(this.featureSelect);

      this.args.map.on('pointermove', this.pointerMove);
      isChanged = true;
    }

    if (this.oldUrl != this.args.url) {
      this.source.setUrl(this.args.url);

      isChanged = true;
    }

    if (this.oldSelectedId != this.args.selectedId) {
      this.oldSelectedId = this.args.selectedId;
      this.olLayer.changed();

      isChanged = true;
    }

    if (isChanged) {
      this.args.onLayerUpdate?.(this.olLayer);
    }
  }

  removeFromMap() {
    if (!this.oldMap) {
      return;
    }

    this.oldMap.removeInteraction(this.featureSelect);
    this.oldMap.un('pointermove', this.pointerMove);
    this.oldMap.removeLayer(this.olLayer);
  }

  handlePointerMove(evt) {
    if (
      this.handlingMouseMove ||
      evt.dragging ||
      !this.olLayer ||
      !this.args.map
    ) {
      return;
    }

    const pixel = this.args.map.getEventPixel(evt.originalEvent);

    this.handlingMouseMove = true;
    this.olLayer.getFeatures(pixel).then(
      (features) => {
        this.handlingMouseMove = false;

        const feature = features?.[0];

        if (this.feature == feature) {
          return;
        }

        this.feature = feature;
        this.args.onHover?.(feature);
      },
      () => {
        if (this.feature == null) {
          return;
        }

        this.feature = null;
        this.args.onHover?.(null);
        this.handlingMouseMove = false;
      },
    );
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.removeFromMap();
  }
}
