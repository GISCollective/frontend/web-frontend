/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { module } from "qunit";
import { setupRenderingTest, it } from "dummy/tests/helpers";
import { fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

module("Integration | Component | editor/source-attribute", function (hooks) {
  setupRenderingTest(hooks);

  let value;

  hooks.beforeEach(function () {
    value = null;

    this.set("onChange", (v) => {
      value = v;
    });
  });

  it("renders the values", async function (a) {
    this.set("value", {
      field: "icon.property",
    });

    await render(hbs`<Editor::SourceAttribute @value={{this.value}} />`);

    expect(this.element.querySelector(".input-attribute-field").value).to.equal("icon.property");
  });

  it("can change the value", async function (a) {
    this.set("value", {
      field: "icon.property",
    });

    await render(hbs`<Editor::SourceAttribute @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".input-attribute-field", "new value");
  });
});
