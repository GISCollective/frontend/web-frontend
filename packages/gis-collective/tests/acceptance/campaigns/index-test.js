/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click, waitFor } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import { authenticateSession } from 'ember-simple-auth/test-support';
import Modal from 'core/test-support/modal';
import waitUntil from '@ember/test-helpers/wait-until';
import { PageModel } from 'ogm/lib/page-model';

describe ('Acceptance | campaigns/index', function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    PageModel.clearCache();
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture();

    const page = server.testData.create.page();
    page._id = 'campaigns';
    page.cols = [
      {
        row: 0,
        col: 0,
        name: '0.0.0',
        type: 'article',
        data: {
          source: {
            id: 'campaigns',
            model: 'article',
          },
        },
      },
      {
        row: 1,
        col: 0,
        name: '0.1.0',
        type: 'card-list',
        data: {
          model: 'campaign',
          destination: { slug: '/campaigns/:campaign-id' },
          columns: {
            classes: ['col-12', 'col-sm-6', 'col-md-4', 'col-lg-3']
          }
        },
      },
    ];
    page.layoutContainers = [
      {
        rows: [
          {
            options: [],
            cols: [
              {
                name: '0.0.0',
                type: '',
                options: [],
              },
            ],
          },
          {
            options: [],
            cols: [
              {
                name: '0.1.0',
                type: '',
                options: [],
              },
            ],
          },
          {
            options: [],
            cols: [
              {
                name: '0.2.0',
                type: '',
                options: [],
              },
            ],
          },
        ],
        options: ['pt-5'],
      },
    ];

    server.testData.storage.addPage(page);
    server.testData.create.pagesMap['campaigns'] = 'campaigns';

    const article = server.testData.create.article();
    article._id = 'campaigns';
    article.content = '# Campaigns';
    article.title = 'Campaigns';
    server.testData.storage.addArticle(article);
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('can visit /campaigns', async function () {
    await visit('/campaigns');
    expect(this.element.querySelector('.error')).not.to.exist;
    expect(currentURL()).to.equal('/campaigns');
  });

  describe ('when there is a campaign available', function (hooks) {
    let campaign;

    hooks.beforeEach(function () {
      campaign = server.testData.storage.addDefaultCampaign();
      server.testData.storage.addDefaultPicture();
    });

    it('should display the campaign', async function () {
      await visit('/campaigns');

      expect(
        this.element.querySelector('.container h1').textContent.trim()
      ).to.equal('Campaigns');
      expect(this.element.querySelector('.card-cover-title-description')).to
        .exist;

      expect(this.element.querySelector('.card-title').textContent).to.contain(
        campaign.name
      );

      await waitUntil(
        () =>
          this.element
            .querySelector('.picture-bg')
            .attributes.getNamedItem('style')?.value != ''
      );

      const style = this.element
        .querySelector('.picture-bg')
        .attributes.getNamedItem('style')?.value;
      expect(style).to.contain(
        `background-image: url(/test/5d5aa72acac72c010043fb59.jpg.lg.jpg`
      );
      expect(style).to.contain(
        `);aspect-ratio: 1;`
      );
      expect(
        this.element.querySelector('.card-cover-title-description .btn-extend')
      ).not.to.exist;
    });
  });

  describe ('when there are two campaigns available', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultCampaign('5ca8baf2ef1f7e01000849ba');
      server.testData.storage.addDefaultCampaign('6ca8baf2ef1f7e01000849bz');

      server.testData.storage.addDefaultPicture();
    });

    it('should display both campaign links', async function () {
      await visit('/campaigns');

      expect(this.element.querySelector('.card-cover-title-description')).to
        .exist;

      let campaigns = this.element.querySelectorAll('.card-title a');

      await waitUntil(() => {
        campaigns = this.element.querySelectorAll('.card-title a');

        const href1 = campaigns[0]?.attributes.getNamedItem('href').value;
        const href2 = campaigns[1]?.attributes.getNamedItem('href').value;

        return (
          href1 &&
          href2 &&
          href1 != '/campaigns/undefined' &&
          href2 != '/campaigns/undefined'
        );
      });

      const href1 = campaigns[0].attributes.getNamedItem('href').value;
      const href2 = campaigns[1].attributes.getNamedItem('href').value;

      expect(href1).to.equal('/campaigns/5ca8baf2ef1f7e01000849ba');
      expect(href2).to.equal('/campaigns/6ca8baf2ef1f7e01000849bz');
    });
  });

  describe ('when the user is authenticated', function (hooks) {
    hooks.beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultUser(true, '5b8a59caef739394031a3f67');
    });

    describe ('when there is an editable campaign available', function (hooks) {
      let campaign;

      hooks.beforeEach(function () {
        campaign = server.testData.create.campaign();
        campaign.canEdit = true;
        server.testData.storage.addCampaign(campaign);

        server.testData.storage.addDefaultPicture();
        server.server.delete(`/mock-server/campaigns/${campaign._id}`, () => {
          return [
            204,
            { 'Content-Type': 'application/json' },
            JSON.stringify({}),
          ];
        });
      });

      it('should delete the campaign', async function () {
        await visit('/campaigns');

        await click('.card-cover-title-description .btn-extend');

        await waitFor('.card-cover-title-description .btn-delete');
        click('.card-cover-title-description .btn-delete');

        await Modal.waitToDisplay();
        await Modal.clickDangerButton();

        expect(
          server.history.filter((a) => a.indexOf('DELETE') != -1)[0]
        ).to.contain(`/mock-server/campaigns/${campaign._id}`);
      });
    });
  });
});
