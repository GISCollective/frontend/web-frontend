/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { resolveLinkObject } from '../../lib/links';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { later } from '@ember/runloop';

export default class ComponentsPageColRedirectComponent extends Component {
  @service headData;
  @service fastboot;
  @service space;

  doFastbootRedirect(value) {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    if (value.substr(0, 1) == '/') {
      value = `https://${this.space.currentDomain}${value}`;
    }

    this.fastboot.deferRendering(
      new Promise((done) => {
        setTimeout(() => {
          this.headData.redirect = value;
          done();
        });

        if (this.fastboot?.response?.headers) {
          this.fastboot.response.headers.set('location', value);
          this.fastboot.response.statusCode = 302;
        }
      }),
    );
  }

  @action
  navigate(element) {
    setTimeout(() => {
      if (this.isDestroyed) {
        return;
      }

      later(() => {
        element.click();
      }, 1000);
    }, 500);
  }

  get link() {
    return resolveLinkObject(this.args.value?.data?.redirect?.destination);
  }

  get niceLink() {
    const value = Object.values(this.link)[0];

    if (this.fastboot.isFastBoot && !this.args.isEditor && value) {
      this.doFastbootRedirect(value);
    }

    return value;
  }
}
