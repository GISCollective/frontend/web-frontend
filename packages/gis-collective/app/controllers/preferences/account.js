/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class PreferencesAccountController extends Controller {
  @service modal;
  @service session;
  @service notifications;
  @service router;
  @service intl;

  @action
  delete() {
    return this.modal.confirmWithPassword(this.intl.t('delete account', { size: 1 }), this.intl.t('Are you sure you want to delete this account?')).then((password) => {
      this.model
        .removeUser(password)
        .then(() => {
          this.notifications.showMessage(this.intl.t('Remove user'), this.intl.t('You have successfully removed your user.'));
          this.session.invalidate();
          this.router.transitionTo('/');
        })
        .catch((err) => {
          this.notifications.handleError(err);
        });
    });
  }

  @action
  changePassword() {
    return this.model
      .changePassword(...arguments)
      .then(() => {
        this.notifications.showMessage(this.intl.t('change password'), this.intl.t('You have successfully changed your password.'));
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
