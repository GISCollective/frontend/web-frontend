import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | manage/feature-card', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let featureData;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.after(async function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.testData.storage.addDefaultPicture();
    featureData = server.testData.storage.addDefaultFeature();
  });

  it('renders a public feature', async function (assert) {
    let feature = await this.store.findRecord('feature', featureData._id);
    feature.description =
      'El Hospital Naval Dr. Luis Díaz Soto fue planificado primero por presidente Dr. Ramón Grau San Martín en otro lugar de la Ciudad de la Habana, incluso';
    feature.canEdit = true;

    this.set('feature', feature);

    await render(hbs`<Manage::CardFeature @record={{this.feature}} />`);

    expect(this.element.querySelector('.name')).to.have.textContent('Nomadisch Grün - Local Urban Food');
    expect(this.element.querySelector('.description')).to.have.textContent(feature.firstParagraph);
    expect(this.element.querySelector('.btn-main-action')).to.have.textContent('edit');
    expect(this.element.querySelector('.visibility')).to.have.textContent('public');
    expect(this.element.querySelector('.btn-main-action')).to.have.attribute(
      'href',
      '/manage/features/edit/5ca78e2160780601008f69e6',
    );
    expect(this.element.querySelector('.cover')).to.have.attribute(
      'style',
      "background-image: url('/test/5d5aa72acac72c010043fb59.jpg/md')",
    );
  });

  it('renders a private feature', async function (assert) {
    let feature = await this.store.findRecord('feature', featureData._id);
    feature.visibility = 0;

    this.set('feature', feature);

    await render(hbs`<Manage::CardFeature @record={{this.feature}} />`);

    expect(this.element.querySelector('.visibility')).to.have.textContent('private');
  });

  it('renders a pending feature', async function (assert) {
    let feature = await this.store.findRecord('feature', featureData._id);
    feature.visibility = -1;

    this.set('feature', feature);

    await render(hbs`<Manage::CardFeature @record={{this.feature}} />`);

    expect(this.element.querySelector('.visibility')).to.have.textContent('pending');
  });
});
