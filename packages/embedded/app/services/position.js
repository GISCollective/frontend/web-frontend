import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class PositionService extends Service {
  @tracked center;

  constructor() {
    super(...arguments);

    window.setPosition = this.setPosition;
  }

  @action
  setPosition(newCenter) {
    this.center = newCenter;
  }
}
