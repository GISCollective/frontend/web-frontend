/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/string-list', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders the view mode with no values', async function () {
      await render(hbs`<Manage::StringList @title="some title"/>`);

      expect(this.element.querySelector('.panel-title').textContent.trim()).to.equal('some title');
      expect(this.element.querySelector('.alert-secondary').textContent.trim()).to.equal('There are no values');
    });

    it('renders the view mode with values', async function () {
      this.set('list', ['value1', 'value2']);

      await render(hbs`<Manage::StringList @list={{this.list}} @title="some title"/>`);

      const listItems = this.element.querySelectorAll('li');

      expect(listItems).to.have.length(2);
      expect(listItems[0].textContent.trim()).to.equal('value1');
      expect(listItems[1].textContent.trim()).to.equal('value2');
    });

    it('triggers the view button when the edit button is pressed', async function () {
      this.set('list', ['value1', 'value2']);

      let value;
      this.set('edit', (v) => {
        value = v;
      });

      await render(hbs`<Manage::StringList @onEdit={{this.edit}} @list={{this.list}} @title="some title"/>`);

      await click('.btn-edit');

      expect(value).to.equal('some title');
    });
  });

  describe('in edit mode', function (hooks) {
    it('can add a string to an empty list', async function () {
      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(hbs`<Manage::StringList @onSave={{this.save}} @editablePanel="some title" @title="some title"/>`);

      await click('.btn-add');
      await fillIn('.text-value-0', 'some value');
      await click('.btn-submit');

      expect(value).to.deep.equal(['some value']);
    });
  });
});
