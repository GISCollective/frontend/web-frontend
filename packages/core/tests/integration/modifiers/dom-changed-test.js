import { render, settled } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { expect } from 'chai';

describe('Integration | Modifier | dom-changed', function (hooks) {
  setupRenderingTest(hooks);

  it('does nothing when there is no function', async function (assert) {
    await render(hbs`<div {{dom-changed}}></div>`);

    assert.ok(true);
  });

  it('does nothing when there is a function and nothing changes', async function (assert) {
    let called;

    this.set('callback', () => {
      called = true;
    });

    await render(hbs`<div {{dom-changed this.callback}}></div>`);

    expect(called).not.to.exist;
  });

  it('triggers the callback when the layout changes', async function (assert) {
    let called;

    this.set('callback', () => {
      called = true;
    });
    this.set('show', false);

    await render(hbs`<div {{dom-changed this.callback}}>
      {{#if this.show}}
        hello
      {{/if}}
    </div>`);

    this.set('show', true);
    await settled();

    expect(called).to.equal(true);
  });
});
