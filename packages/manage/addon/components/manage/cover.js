/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageCoverComponent extends Component {
  @tracked value;

  get imageStyle() {
    if (!this.args.value) {
      return htmlSafe('');
    }

    const picture = this.args.value?.get('picture');

    let rnd = '';

    if (!picture.startsWith('data:')) {
      rnd = '/lg?rnd=' + Math.random();
    }

    if (!picture) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${picture}${rnd}')`);
  }

  get isDisabled() {
    return !this.value;
  }

  @action
  async save() {
    await this.value.save();

    return this.args.onSave?.(this.value);
  }

  @action
  async cancel() {
    this.value = null;

    return this.args.onCancel?.();
  }

  @action
  changeValue(_, value) {
    this.value = value;
  }
}
