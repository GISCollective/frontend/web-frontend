/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | space-style',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the content', async function () {
    await render(hbs`
      <SpaceStyle>
        template block text
      </SpaceStyle>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
  });

  it('adds the space style', async function () {
    this.set('space', { style: 'some-style' });
    await render(hbs`
      <SpaceStyle @space={{this.space}}>
        template block text
      </SpaceStyle>
    `);

    expect(this.element.querySelector('link')).to.have.attribute(
      'rel',
      'stylesheet'
    );
    expect(this.element.querySelector('link')).to.have.attribute(
      'href',
      'some-style'
    );
  });

  it('adds a data-bs-theme attribute', async function () {
    this.set('space', { style: 'some-style', id: 'space-id' });
    await render(hbs`
      <SpaceStyle @space={{this.space}}>
        template block text
      </SpaceStyle>
    `);

    expect(this.element.querySelector('.space-theme')).to.have.attribute(
      'data-bs-theme',
      'light-space-id'
    );
  });
});
