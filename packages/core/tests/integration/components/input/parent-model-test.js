/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { expect } from 'chai';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | input/parent-model', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let value;

  hooks.beforeEach(function () {
    server = new TestServer();
    value = undefined;
    this.set('change', (v) => {
      value = v;
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is not attribute', async function (assert) {
    await render(hbs`<Input::ParentModel />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('allows selecting a calendar', async function () {
    server.testData.storage.addDefaultCalendar('1');
    server.testData.storage.addDefaultCalendar('2');

    this.set('teams', ['1', '2']);

    await render(hbs`<Input::ParentModel @teams={{this.teams}} @onChange={{this.change}} @model="calendar"/>`);

    expect(server.history).to.deep.equal(['GET /mock-server/calendars?team=1%2C2']);
    expect(this.element.querySelector('.form-select').value).to.equal('1');
    expect(value.id).to.equal('1');

    this.element.querySelector('.form-select').value = '2';
    await triggerEvent('.form-select', 'change');

    expect(this.element.querySelector('.form-select').value).to.equal('2');
    expect(value.id).to.equal('2');
  });

  it('renders a label when there is just one calendar', async function () {
    server.testData.storage.addDefaultCalendar('1');

    this.set('teams', ['1', '2']);

    await render(hbs`<Input::ParentModel @teams={{this.teams}} @onChange={{this.change}} @model="calendar"/>`);

    expect(server.history).to.deep.equal(['GET /mock-server/calendars?team=1%2C2']);
    expect(this.element.querySelector('.form-select')).not.to.exist;
    expect(this.element.querySelector('.value').textContent.trim()).to.equal('test calendar');
  });

  it('allows selecting a map', async function () {
    server.testData.storage.addDefaultMap('1');
    server.testData.storage.addDefaultMap('2');

    this.set('teams', ['1', '2']);

    await render(hbs`<Input::ParentModel @teams={{this.teams}} @onChange={{this.change}} @model="map"/>`);

    expect(this.element.querySelector('.form-select').value).to.equal('1');
    expect(value.id).to.equal('1');

    this.element.querySelector('.form-select').value = '2';
    await triggerEvent('.form-select', 'change');

    expect(this.element.querySelector('.form-select').value).to.equal('2');
    expect(value.id).to.equal('2');
  });
});
