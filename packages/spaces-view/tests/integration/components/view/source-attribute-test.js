/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | view/source-attribute', function (hooks) {
  setupRenderingTest(hooks);

  let featureData;
  let server;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    featureData = server.testData.storage.addDefaultFeature();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::SourceAttribute>asd</View::SourceAttribute>`);
    assert.dom(this.element).hasText('');
  });

  it('renders the attribute when the record has the attribute', async function (a) {
    featureData.attributes = {
      icon: {
        property: 'value',
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {
        sourceAttribute: {
          field: 'icon.property',
        },
      },
    });

    this.set('record', feature);

    await render(
      hbs`<View::SourceAttribute @record={{this.record}} @value={{this.value}} >
        <:default as |value| >{{value}}</:default>
        <:blocks>blocks</:blocks>
      </View::SourceAttribute>`,
    );

    expect(this.element.textContent.trim()).to.equal('value');
  });

  it('renders "yes" when the attribute is "true"', async function (a) {
    featureData.attributes = {
      icon: {
        property: true,
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {
        sourceAttribute: {
          field: 'icon.property',
        },
      },
    });

    this.set('record', feature);

    await render(
      hbs`<View::SourceAttribute @record={{this.record}} @value={{this.value}} >
        <:default as |value| >{{value}}</:default>
        <:blocks>blocks</:blocks>
      </View::SourceAttribute>`,
    );

    expect(this.element.textContent.trim()).to.equal('yes');
  });

  it('renders the blocks when the attribute is a rich text', async function (a) {
    featureData.attributes = {
      icon: {
        property: {
          blocks: [{ type: 'paragraph', data: { text: 'description' } }],
        },
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {
        sourceAttribute: {
          field: 'icon.property',
        },
      },
    });

    this.set('record', feature);

    await render(
      hbs`<View::SourceAttribute @record={{this.record}} @value={{this.value}} >
        <:default>default</:default>
        <:blocks as |value|>
          <View::Blocks
            @value={{value}}
          />
        </:blocks>
      </View::SourceAttribute>`,
    );

    expect(this.element.textContent.trim()).to.equal('description');
  });

  it('renders a placeholder when the record has no attribute and it is an editor view', async function (a) {
    featureData.attributes = {
      icon: {
        property: {
          blocks: [{ type: 'paragraph', data: { text: 'description' } }],
        },
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {},
    });

    this.set('record', feature);

    await render(
      hbs`<View::SourceAttribute @isEditor={{true}} @record={{this.record}} @value={{this.value}} >
        <:default as |value| >{{value}}</:default>
        <:blocks as |value|>
          <View::Blocks
            @value={{value}}
          />
        </:blocks>
      </View::SourceAttribute>`,
    );

    expect(this.element).to.have.textContent('no value');
  });
});
