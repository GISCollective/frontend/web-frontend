/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, click } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Acceptance | manage/newsletter/unsubscribe',  function (hooks) {
  setupApplicationTest(hooks);

  let newsletter;
  let server;
  let receivedEmail;

  hooks.beforeEach(() => {
    server = new TestServer();
    newsletter = server.testData.storage.addDefaultNewsletter();

    server.post(
      '/mock-server/newsletters/5ca78e2160780601008f69ff/unsubscribe',
      (request) => {
        receivedEmail = JSON.parse(request.requestBody);
        return [201, {}, ''];
      }
    );
  });


  hooks.afterEach(() => {
    server.shutdown();
  });

  it('can visit /manage/newsletter/unsubscribe', async function () {
    await visit(`/manage/newsletter/${newsletter._id}/unsubscribe/email-id`);

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'my first newsletter'
    );

    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Would you like to unsubscribe from our newsletter?'
    );
    expect(this.element.querySelector('.newsletter-container .btn').textContent.trim()).to.equal(
      'Unsubscribe from this list'
    );
  });

  it('makes a request when the button is clicked', async function () {
    await visit(`/manage/newsletter/${newsletter._id}/unsubscribe/email-id`);

    await click('.newsletter-container .btn');

    expect(receivedEmail).to.deep.equal({ email: 'email-id' });
  });
});
