/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { PageCol } from 'models/transforms/page-col-list';
import { service } from '@ember/service';

export default class PageColEventLocationComponent extends Component {
  @service store;
  @service space;
  @service fastboot;
  @service pageCols;

  @tracked record;
  @tracked _card;
  @tracked _value;

  get textLocation() {
    if (this.record?.location?.type != 'Text') {
      return null;
    }

    return this.record?.location?.value;
  }

  get featureSource() {
    if (this.record?.location?.type != 'Feature') {
      return null;
    }

    return new PageCol({
      data: {
        source: { model: 'feature', id: this.record?.location?.value },
      },
    });
  }

  @action
  updateCardProps(event) {
    const value = this.args.value;
    value.data.card = event.detail.value;

    this._card = event.detail.value;
    this._value = value;
  }

  get card() {
    return this._card ?? this.args.value?.data?.card ?? this.args.value?.data;
  }

  get value() {
    return this._value ?? this.args.value;
  }

  @action
  loaded(record) {
    this.record = record;
  }
}
