/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';

export default class ManageIconsAddRoute extends AuthenticatedRoute {
  modelsToClear = ['icon'];

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    this.modelsToClear = ['icon'];

    let editableIconSets;
    let iconSet;
    let hasSelectedSet = false;

    if (params.set_id == '_') {
      editableIconSets = await this.store.query('iconSet', { edit: true });
      iconSet = editableIconSets[0];
    } else {
      iconSet = await this.store.findRecord('iconSet', params.set_id);
      hasSelectedSet = true;
    }

    const icons = await this.store.query('icon', { edit: true });

    return {
      hasSelectedSet,
      editableIconSets,
      iconSet,
      icons,
      icon: this.store.createRecord('icon', {
        name: '',
        category: params.c ?? '',
        subcategory: params.sc ?? '',
        iconSet,
        attributes: [],
        parent: '',
        image: {},
      }),
    };
  }
}
