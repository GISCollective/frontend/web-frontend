/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class MenuItemComponent extends Component {
  @tracked _style;

  get style() {
    return this._style ?? this.args.style ?? this.args.item ?? {};
  }

  get class() {
    const style = this.style;

    if (!this.args.item) {
      return '';
    }

    return Object.values(style.classes ?? []).join(' ');
  }

  get pictureValue() {
    if(!this.args.picture) {
      return null;
    }

    return {
      pictureOptions: this.args.picture
    }
  }

  @action
  updateProps(event) {
    this._style = event.detail.value;
  }
}
