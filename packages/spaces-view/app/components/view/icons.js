/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class PageColIconsComponent extends Component {
  @service store;
  @tracked _records;

  get rowClass() {
    if (!this.useFlex) {
      return '';
    }

    return `row g-${this.gutter} row-cols-${this.rowColSize}`;
  }

  get colClass() {
    if (!this.useFlex) {
      return `m-${this.gutter} d-inline-block`;
    }

    return 'col';
  }

  get gutter() {
    return this.args.value?.data?.gutter ?? '1';
  }

  get useFlex() {
    return this.args.value?.data?.flex != 'disabled';
  }

  get rowColSize() {
    return this.args.value?.data?.rowColSize ?? '0';
  }

  get size() {
    return this.args.value?.data?.viewMode == 'medium' ? 'lg' : 'sm';
  }

  get showLinks() {
    return this.args.value?.data?.links == 'enabled';
  }

  get showCards() {
    return this.args.value?.data?.viewMode == 'cards';
  }

  get records() {
    if (this._records) {
      return this._records;
    }

    if (Array.isArray(this.args.value?.data?.records)) {
      return this.args.value.data.records;
    }

    if (Array.isArray(this.args.value)) {
      return this.args.value;
    }

    return [];
  }

  @action
  async setup() {
    if (this.args.value?.data?.ids) {
      let records = this.args.value?.data?.ids
        .map(
          (id) =>
            this.store.peekRecord('icon', id) ||
            this.store.findRecord('icon', id),
        )
        .filter((a) => Boolean(a));

      this._records = await Promise.all(records);
    }
  }
}
