/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | input/options/model-field", function (hooks) {
  setupRenderingTest(hooks);

  it("renders nothing when there is no value", async function () {
    await render(hbs`<Input::Options::ModelField />`);
    expect(this.element.textContent.trim()).to.equal("");
  });

  it("renders the map description fields for a map model with for=description", async function () {
    this.set("col", {
      modelKey: "map",
    });
    this.set("model", {
      map: {
        constructor: {
          modelName: "map",
        },
      },
    });

    await render(hbs`<Input::Options::ModelField @col={{this.col}} @model={{this.model}} @for="description" />`);

    const options = [...this.element.querySelectorAll("option")].map((a) => a.textContent.trim());

    expect(options).to.deep.equal(["description", "tagLine"]);
  });

  it("renders the map description fields for a map model with for=description", async function (a) {
    this.set("col", {
      modelKey: "article",
    });
    this.set("model", {
      article: {
        constructor: {
          modelName: "article",
        },
      },
    });

    await render(hbs`<Input::Options::ModelField @col={{this.col}} @model={{this.model}} @for="date" />`);

    const options = [...this.element.querySelectorAll("option")].map((a) => a.textContent.trim());

    expect(options).to.deep.equal(["releaseDate", "info.lastChangeOn", "info.createdOn"]);
  });
});
