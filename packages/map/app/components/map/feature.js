/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class MapFeatureComponent extends Component {
  @service fullscreen;
  @service mapStyles;

  get type() {
    if (!this.args.value) {
      return 'Point';
    }

    return this.args.value.type;
  }

  get featureStyle() {
    return (feature) => {
      const type = feature.getGeometry().getType();
      const properties = {};

      if (typeof this.args.icon?.get == 'function') {
        properties['icon'] = this.args.icon.get('id');
      } else {
        properties['icon'] = this.args.icon?.id;
      }

      if (type == 'Polygon' || type == 'MultiPolygon') {
        properties.type = 'polygon';
      }

      if (type == 'Polygon') {
        properties.type = 'polygon';
      }

      if (type == 'MultiLineString') {
        properties.type = 'line';
      }

      if (type == 'LineString') {
        properties.type = 'line';
      }

      if (type == 'Point') {
        properties.type = 'point';
      }

      const res = this.mapStyles.fromProperties(properties);

      if (res.then) {
        return [];
      }

      return res;
    };
  }

  get isPoint() {
    return this.type == 'Point';
  }

  get isLine() {
    return this.type == 'LineString';
  }

  get isMultiLine() {
    return this.type == 'MultiLineString';
  }

  get isPolygon() {
    return this.type == 'Polygon';
  }

  get isMultiPolygon() {
    return this.type == 'MultiPolygon';
  }

  @action
  iconUpdate() {
    this.featureStyle.icon = this.args.icon;
  }
}
