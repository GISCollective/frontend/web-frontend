/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { later, cancel } from '@ember/runloop';
import { service } from '@ember/service';

export default class MenuItemListComponent extends Component {
  @service space;
  @tracked showIndex;

  isSelected(link) {
    if (!this.args.model?.page?.id) {
      return;
    }

    if (!link?.url && !link?.pageId) {
      return false;
    }

    if (link?.url && this.space.currentLocation?.endsWith(link.url)) {
      return true;
    }

    return link?.pageId === this.args.model?.page?.id;
  }

  get items() {
    const result = [];

    for (const topItem of this.args.value) {
      const name = topItem.name;
      const link = topItem.link;

      let isTopItemSelected = this.isSelected(topItem.link);

      let dropDown = [];
      for (const item of topItem.dropDown ?? []) {
        let isSelected = this.isSelected(item.link);
        isTopItemSelected = isTopItemSelected || isSelected;

        let classes = this.args.classes?.regular ?? {};

        if (isSelected && this.args.classes?.selected) {
          classes = this.args.classes.selected;
        }

        dropDown.push({
          ...item,
          classes,
          isSelected,
          computedClass: Object.values(classes).join(' '),
        });
      }

      const classes = isTopItemSelected
        ? this.args.classes?.selected
        : this.args.classes?.regular;

      result.push({
        ...topItem,
        name,
        link,
        dropDown,
        classes: classes ?? [],
      });
    }

    return result;
  }

  @action
  hideHoverDropdown() {
    if (this.args.ignoreHover) {
      return;
    }

    this.dropdownTimer = later(() => {
      this.showIndex = -1;
    }, 300);
  }

  @action
  showHoverDropdown(index) {
    if (this.args.ignoreHover) {
      return;
    }
    cancel(this.dropdownTimer);
    this.showIndex = index;
  }

  @action
  showClickDropdown(index) {
    if (!this.args.ignoreHover) {
      return;
    }

    if (this.showIndex === index) {
      this.showIndex = -1;
      return;
    }

    this.showIndex = index;
  }
}
