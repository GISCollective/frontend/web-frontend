/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | model-actions/icon',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the campaign is not editable', async function () {
    await render(hbs`<ModelActions::Icon />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the edit button when the record is editable', async function () {
    this.set('record', {
      id: 'id',
      canEdit: true,
    });

    await render(hbs`<ModelActions::Icon @record={{this.record}} />`);

    expect(this.element.textContent.trim()).to.equal('Edit');
  });
});
