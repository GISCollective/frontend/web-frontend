/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from '../../../base/app-route';
import { service } from '@ember/service';

export default class BrowseMapsMapViewIndexRoute extends AppRoute {
  @service fullscreen;
  @service embed;

  activate() {
    if (!this.embed.isEnabled) {
      this.fullscreen.isEnabled = false;
    }
  }
}
