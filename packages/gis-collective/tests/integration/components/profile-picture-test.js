/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | profile-picture',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<ProfilePicture />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('.is-square')).not.to.exist;
  });

  it('renders a square picture', async function () {
    await render(hbs`<ProfilePicture @isSquare={{true}}/>`);
    expect(this.element.querySelector('.is-square')).to.exist;
  });
});
