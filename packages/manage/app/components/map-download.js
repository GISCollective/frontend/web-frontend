/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { getOwner } from '@ember/application';
import { service } from '@ember/service';

export default class MapDownloadComponent extends Component {
  @service clickOutside;
  @service fastboot;
  @tracked isOpened;

  elementId = 'map-download-' + guidFor(this);

  get apiUrl() {
    const config = getOwner(this).resolveRegistration('config:environment');

    return config.apiUrl;
  }

  get isDisabled() {
    return this.fastboot.isFastBoot;
  }

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showOptions() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.isOpened = false;
    });

    this.isOpened = true;
  }
}
