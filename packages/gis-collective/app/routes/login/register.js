/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from '../base/app-route';
import { hash } from 'rsvp';

export default class RoutesLoginRoute extends AppRoute {
  beforeModel() {
    if (this.session.isAuthenticated) {
      return this.router.transitionTo('index');
    }

    return this.preferences.getPreference('register.enabled').then((registerEnabled) => {
      const value = registerEnabled.value;

      if (value != 'true' && value !== true) {
        this.router.transitionTo('index');
      }
    });
  }

  async model() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    const captchaEnabled = await this.preferences.getPreference('captcha.enabled');
    const registerUrl = await this.preferences.getPreference('register.url');

    return hash({
      captchaEnabled: captchaEnabled.value,
      registerUrl: registerUrl.value,
      challenge: ['reCAPTCHA', 'mtCAPTCHA'].indexOf(captchaEnabled.value) != -1 ? this.store.adapterFor('user').registerChallenge() : null,
    });
  }

  afterModel(model) {
    if(!model?.registerUrl) {
      return;
    }

    if(this.space.windowDomain == "localhost") {
      return;
    }

    model.shouldRedirect = model.registerUrl?.indexOf(`https://${this.space.windowDomain}`) != 0;
  }
}
