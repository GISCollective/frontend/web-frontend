/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';
import { generateBlur, idToHash } from '../lib/blur';

export default class IconSetModel extends Model {
  @attr('string') name;
  @attr description;
  @attr('boolean') canEdit;
  @attr('icon-style') styles;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr('visibility') visibility;
  @attr('icon-sprite') sprites;

  async getCover() {
    return await this.cover;
  }

  get hashBg() {
    return generateBlur(idToHash(this.id), 4, 4);
  }

  get title() {
    return this.name;
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  get hasCover() {
    if (!this.cover) {
      return false;
    }

    return this.cover.get('name') != 'default';
  }

  categories() {
    return this.store.adapterFor(this.constructor.modelName).categories(this, ...arguments);
  }

  subcategories() {
    return this.store.adapterFor(this.constructor.modelName).subcategories(this, ...arguments);
  }
}
