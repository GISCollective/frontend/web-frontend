/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';

export default class NewsletterModel extends Model {
  @attr('string') name;
  @attr('string') description;

  @attr('model-info') info;
  @attr('visibility') visibility;

  @belongsTo('article', { async: true, inverse: null })
  welcomeMessage;

  @attr('boolean') canEdit;

  async subscribe(email) {
    return this.store.adapterFor(this.constructor.modelName).subscribe(this.id, email);
  }

  async unsubscribe(email) {
    return this.store.adapterFor(this.constructor.modelName).unsubscribe(this.id, email);
  }

  importEmails(value, replace) {
    return this.store.adapterFor(this.constructor.modelName).importEmails(this.id, value, replace);
  }

  downloadEmails() {
    return this.store.adapterFor(this.constructor.modelName).downloadEmails(this.id);
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  set descriptionContentBlocks(content) {
    this.description = toContentBlocks(content, this.name);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  get title() {
    return this.name;
  }
}
