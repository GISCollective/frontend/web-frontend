/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/group', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Attributes::Group />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('does not render the validation message when it is not present', async function () {
    this.set('group', {});

    await render(hbs`<Attributes::Group @group={{this.group}}/>`);

    expect(this.element.querySelector('.icon-validation-error')).not.to.exist;
  });

  it('renders the validation message when it is present', async function () {
    this.set('group', {
      validationMessage: 'there is an error',
    });

    await render(hbs`<Attributes::Group @group={{this.group}}/>`);

    expect(this.element.querySelector('.icon-validation-error')).to.exist;
  });
});
