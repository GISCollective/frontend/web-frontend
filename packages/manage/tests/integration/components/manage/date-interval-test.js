/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/date-interval', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders a warning when the time interval is not set', async function () {
      await render(hbs`<Manage::DateInterval @title="title" />`);
      expect(this.element.querySelector('.alert-warning').textContent.trim()).to.equal(
        'There is no date interval set.',
      );
    });

    it('renders a warning when the time interval is not set when the dates are way in the past', async function () {
      this.set('value', {
        startDate: new Date(Date.parse('1980-01-01T00:00:00')),
        endDate: new Date(Date.parse('1982-01-01T00:00:00')),
      });

      await render(hbs`<Manage::DateInterval @title="title" @value={{this.value}} />`);

      expect(this.element.querySelector('.alert-warning').textContent.trim()).to.equal(
        'There is no date interval set.',
      );
    });

    it('renders the dates when they are set', async function () {
      this.set('value', {
        startDate: new Date(Date.parse('2019-01-01T00:00:00')),
        endDate: new Date(Date.parse('2022-01-01T00:00:00')),
      });

      await render(hbs`<Manage::DateInterval @title="title" @value={{this.value}} />`);

      expect(this.element.querySelector('.alert-warning')).not.to.exist;
      expect(this.element.querySelector('.view-container').textContent.trim()).to.equal(
        'Between 1/1/2019 and 1/1/2022.',
      );
    });

    it('renders only the start date when end date is not set', async function () {
      this.set('value', {
        startDate: new Date(Date.parse('2019-01-01T00:00:00')),
      });

      await render(hbs`<Manage::DateInterval @title="title" @value={{this.value}} />`);

      expect(this.element.querySelector('.alert-warning')).not.to.exist;

      expect(this.element.querySelector('.view-container').textContent.trim()).to.equal('Starting 1/1/2019.');
    });

    it('renders only the end date when start date is not set', async function () {
      this.set('value', {
        endDate: new Date(Date.parse('2019-01-01T00:00:00')),
      });

      await render(hbs`<Manage::DateInterval @title="title" @value={{this.value}} />`);

      expect(this.element.querySelector('.alert-warning')).not.to.exist;

      expect(this.element.querySelector('.view-container').textContent.trim()).to.equal('Until 1/1/2019.');
    });
  });

  describe('in edit mode', function (hooks) {
    it('can disable the start value', async function () {
      this.set('value', { startDate: new Date(Date.parse('2019-01-01T00:00:00')) });

      let value;
      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::DateInterval @allowUnset={{true}} @title="title" @isMainValue={{true}} @onSave={{this.save}}
  @value={{this.value}} @editablePanel="title" />`,
      );
      await click('.col-start-date input[type=checkbox]');
      await click('.btn-submit');

      expect(value?.getFullYear()).to.equal(0);
    });

    it('can disable the end value', async function () {
      this.set('value', { endDate: new Date(Date.parse('2019-01-01T00:00:00')) });

      let value;
      this.set('save', (_, a) => {
        value = a;
      });

      await render(
        hbs`<Manage::DateInterval @allowUnset={{true}} @title="title" @isMainValue={{true}} @onSave={{this.save}}
  @value={{this.value}} @editablePanel="title" />`,
      );

      await click('.col-end-date input[type=checkbox]');
      await click('.btn-submit');

      expect(value?.getFullYear()).to.equal(0);
    });

    it('changes the start year', async function () {
      this.set('value', { startDate: new Date(Date.parse('2019-01-01T00:00:00')) });

      let value;
      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::DateInterval @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}}
  @editablePanel="title" />`,
      );

      await fillIn('.datepicker-input', '2012-02-01');

      await click('.btn-submit');

      expect(value.getFullYear()).to.equal(2012);
    });

    it('resets the value after cancel is pressed', async function () {
      this.set('value', { startDate: new Date(Date.parse('2019-01-01T00:00:00')) });

      await render(
        hbs`<Manage::DateInterval @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}}
  @editablePanel="title" />`,
      );

      await fillIn('.datepicker-input', '2012-02-01');

      await click('.btn-cancel');
      expect(this.element.querySelector('.datepicker-input').value).to.equal('2012-02-01');
    });
  });
});
