/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { A } from "models/lib/array";

export default class BrowseTableComponent extends Component {
  get header() {
    if (!this.args.value || !Array.isArray(this.args.value)) {
      return [];
    }

    const result = [];

    this.args.value
      .filter((a) => a && typeof a === 'object')
      .forEach((row) => {
        const items = Object.keys(row).filter((a) => result.indexOf(a) == -1);

        for(let item of items) {
          result.push(item);
        }
      });

    return A(result);
  }

  get rows() {
    const header = this.header;

    return this.args.value
      .filter((a) => a && typeof a === 'object')
      .map((value, index) => {
        const values = header.map((key) => (value[key] === undefined ? '-' : value[key]));

        return {
          index: index + 1,
          values,
        };
      });
  }
}
