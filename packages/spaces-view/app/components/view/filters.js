/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class PageColFiltersComponent extends Component {
  get containerValue() {
    return this.args.value?.data?.style || this.args.value?.data?.container;
  }
}
