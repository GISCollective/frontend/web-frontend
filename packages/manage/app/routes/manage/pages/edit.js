/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { PageColMap } from 'models/transforms/page-col-map';
import { LayoutContainerMap } from 'models/transforms/layout-container-map';
import { service } from '@ember/service';
import { fetch } from 'models/lib/fetch-source';

export default class ManagePagesEditRoute extends AuthenticatedRoute {
  @service store;
  @service fastboot;
  @service space;

  queryParams = {
    selectedId: {
      refreshModel: true,
    },
    allTeams: {
      refreshModel: true,
    },
  };

  async fillModel(slug, model, params, teamId) {
    const slugPieces = slug.split('--');

    let variable;

    slugPieces
      .filter((a) => a.includes(':'))
      .forEach((item) => {
        variable = item.replace(':', '').replace('-id', '');
      });

    if (!variable) {
      return;
    }

    let record;

    if (params['selectedId']) {
      try {
        const query = { id: params.selectedId };
        if (variable == 'article') {
          query.team = teamId;
        }

        record = await this.store.queryRecord(variable, query);
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
      }
    }

    if (!record) {
      try {
        const recordList = await this.store.query(variable, {
          limit: 1,
          team: teamId,
        });
        record = recordList[0];
      } catch (err) {
        console.error(err);
      }
    }

    model[variable] = record;
    model.selectedModel = record;
    model.selectedType = variable;
    model.selectedId = record?.id;
  }

  async teamsModel(params, queries) {
    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    return hash({
      ...queries,
      teams: this.store.query('team', teamsParams),
    });
  }

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return false;
    }

    let page = await this.store.findRecord('page', params.id, { reload: true });
    let space = await page.space;
    await space.getPagesMap();

    const modelData = {};

    if (!space.cols) {
      space.cols = new PageColMap({});
    }
    if (!space.layoutContainers) {
      space.layoutContainers = new LayoutContainerMap({});
    }

    if (page.slug.includes(':')) {
      await this.fillModel(page.slug, modelData, params, space.visibility.teamId);
    }

    try {
      for (const col of page.cols) {
        const promise = await fetch(modelData, col, space, this.store);
        const key = col.modelKey;

        if (promise && key) {
          modelData[key] = promise;
          col.record = promise;
        }
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      page = null;
    }

    const team = await page.visibility.fetchTeam();

    const data = await this.teamsModel(params, {
      ...modelData,
      page,
      space,
      team,
      teamSpaces: this.store.query('space', { team: team?.id }),
    });

    const resolvedData = await hash(data);

    return new PagesRoute(resolvedData);
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    if (model) {
      controller.selectedId = model.selectedId;
      model.onSelectId = (v) => (controller.selectedId = v);
      controller.reset?.();
    }
  }
}

class PagesRoute {
  @tracked page;
  @tracked space;
  @tracked selectedModel;

  get selectedId() {
    return this._selectedId;
  }

  set selectedId(value) {
    this._selectedId = value;
    this.onSelectId?.(value);
  }

  constructor(data) {
    this.page = data.page;
    this.space = data.space;
    this.layoutRevisions = data.layoutRevisions;
    this.selectedModel = data.selectedModel;

    Object.keys(data).forEach((k) => {
      if (!this[k]) {
        this[k] = data[k];
      }
    });
  }
}
