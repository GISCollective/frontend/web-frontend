/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { fetchSourceRecord } from 'models/lib/fetch-source';
import { fetch } from 'models/lib/fetch-source';

export default class PageColBaseSourceRecordComponent extends Component {
  @tracked _record;
  @tracked renders = 0;
  @service store;

  get record() {
    if (this._record) {
      return this._record;
    }

    return this.model[this.modelKey];
  }

  get model() {
    return this.args.model ?? {};
  }

  get modelName() {
    return this.args.value?.data?.source?.model ?? '';
  }

  get modelKey() {
    return this.args.value?.modelKey ?? '';
  }

  @action
  async updateProps(event) {
    this.args.value.data = {
      ...this.args.value.data,
      source: event.detail.value,
    };

    return this.setup();
  }

  @action
  async setup() {
    if (
      this.prevModelKey == this.modelKey &&
      this.modelKey != 'selectedModel'
    ) {
      return;
    }

    if (!this.modelKey && this.args.value?.record) {
      this._record = await this.args.value.record;
      this.args.onLoad?.(this.record);
      this.renders++;

      return;
    }

    if (this.args.value && !this.modelKey && !this.args.value?.record) {
      this._record = await fetchSourceRecord(
        this.args.value?.data?.source,
        this.args.model?.space,
        this.store,
      );

      this.args.value.record = this._record;
      this.args.onLoad?.(this.record);
      this.renders++;

      return;
    }

    this.prevModelKey = this.modelKey;

    let promise;

    if (this.model.get) {
      promise = this.model.get(this.modelKey);
    }

    if (!promise) {
      promise = this.model[this.modelKey];
    }

    if (!promise) {
      promise = fetch(
        this.model,
        this.args.value,
        this.model.space,
        this.store,
      );
    }

    const record = await promise;
    this.model[this.modelKey] = record;

    if (record?.id == this._record?.id) {
      return;
    }
    this.renders++;

    await this.args.onLoad?.(record);

    this._record = record;
  }
}
