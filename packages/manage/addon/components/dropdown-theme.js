/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class DropdownThemeComponent extends Component {
  @service theme;

  icons = {
    auto: 'circle-half-stroke',
    light: 'sun',
    dark: 'moon',
  };

  get icon() {
    return this.icons[this.theme.preferred];
  }

  @action
  setTheme(name) {
    this.theme.preferred = name;
  }
}
