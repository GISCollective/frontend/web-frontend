/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../../../base/app-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';
import LazyList from 'models/lib/lazy-list';

export default class BrowseMapsMapViewSearchRoute extends Route {
  @service fullscreen;
  @service fastboot;

  queryParams = {
    allFeatures: {
      refreshModel: true,
    },
  };

  async model(params) {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    let controller = this.controllerFor('browse.maps.map-view');
    controller.term = params.term;

    const parentModel = this.modelFor('browse.maps.map-view');

    if (parentModel.map) {
      params.map = parentModel.map.id;
    }

    const lon = controller.lon;
    const lat = controller.lat;

    if (lon && lat) {
      params.lon = lon;
      params.lat = lat;
    }

    params.per_page = 12;

    const features = new LazyList('feature', params, this.store);
    await features.loadNext();

    let maps;
    let places;

    if (!parentModel.map && !params.allFeatures && params.term.length > 3) {
      maps = this.store.query('map', { ...params, limit: 15 });
      places = await this.store.query('geocoding', { query: params.term });
    }

    return hash({
      mapViewController: controller,
      mapId: params.map,
      term: params.term,
      features,
      maps,
      places,
    });
  }

  async afterModel(model) {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    const features = await model.features?.buckets?.[0];
    const maps = await model.mapBuckets?.columns?.[0];

    const items = [];

    features?.forEach((feature) => {
      feature.icons.forEach((icon) => {
        items.push(
          this.store.findRecord('icon', icon.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });

      feature.pictures.forEach((picture) => {
        items.push(
          this.store.findRecord('picture', picture.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });
    });

    maps?.forEach((map) => {
      items.push(map.get('cover'));
    });

    return Promise.all(items);
  }

  activate() {
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = true;
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = false;
    controller.allFeatures = null;
  }
}
