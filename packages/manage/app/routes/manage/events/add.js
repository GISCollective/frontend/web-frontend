/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';
import { ModelInfo } from '../../../transforms/model-info';

export default class ManageEventsAddRoute extends AuthenticatedRoute {
  @service user;
  modelsToClear = ['event', 'picture'];

  queryParams = {
    calendar: { refreshModel: true },
  };

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const newData = {
      name: '',
      info: new ModelInfo(),
      visibility: {
        isPublic: false,
        team: null,
      },
    };

    let hideCalendar = false;

    if (params.calendar) {
      newData.calendar = await this.store.findRecord('calendar', params.calendar);
      newData.visibility = newData.calendar.visibility;
      hideCalendar = true;
    }

    return hash({
      hideCalendar,
      teams: this.user.teams,
      event: this.store.createRecord('event', newData),
    });
  }
}
