/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, waitFor } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import click from "@ember/test-helpers/dom/click";

describe("Integration | Component | editor/icons", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let icon1;
  let icon2;

  hooks.before(function () {
    server = new TestServer();

    icon1 = server.testData.create.icon("1");
    icon2 = server.testData.create.icon("2");

    server.testData.storage.addIcon(icon1);
    server.testData.storage.addIcon(icon2);

    server.post("/mock-server/icons", (request) => {
      const iconRequest = JSON.parse(request.requestBody);
      iconRequest.icon["_id"] = "5cc8dc1038e882010061545a";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(iconRequest)];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  describe("when the value is a list of icons", function () {
    it("renders a list of icons", async function () {
      this.set("value", [icon1, icon2]);
      await render(hbs`<Editor::Icons @value={{this.value}} />`);

      expect(this.element.querySelectorAll(".icon-container")).to.have.length(2);
      expect(this.element.querySelector(".icon-container img")).to.have.attribute(
        "src",
        "/test/5ca7bfbfecd8490100cab97d.jpg",
      );
    });

    it("can remove an icon", async function () {
      this.set("value", [icon1, icon2]);
      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(hbs`<Editor::Icons @value={{this.value}} @onChange={{this.change}} />`);

      await click(".btn-remove-icon");
      expect(value).to.have.length(1);
    });

    it("can add an icon", async function () {
      this.set("value", []);
      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(hbs`<Editor::Icons @value={{this.value}} @onChange={{this.change}} />`);

      await waitFor(".btn-select-icons");
      await click(".btn-select-icons");
      await waitFor(".btn-icon");
      await click(".btn-icon");

      expect(value).to.have.length(1);
    });

    it("does not show the display settings", async function () {
      await render(hbs`<Editor::Icons @value={{this.value}} />`);

      expect(this.element.querySelector(".manage-editors-icons-display")).not.to.exist;
    });
  });

  describe("when the value is a list of wrapped records", function () {
    it("renders a list of icons", async function () {
      this.set("value", { data: { ids: [icon1._id, icon2._id] } });

      await render(hbs`<Editor::Icons @value={{this.value}} />`);

      await waitFor(".icon-container");
      expect(this.element.querySelectorAll(".icon-container")).to.have.length(2);
      expect(this.element.querySelector(".icon-container img")).to.have.attribute(
        "src",
        "/test/5ca7bfbfecd8490100cab97d.jpg",
      );
    });

    it("can remove an icon", async function () {
      this.set("value", { data: { ids: [icon1._id, icon2._id] } });
      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(hbs`<Editor::Icons @value={{this.value}} @onChange={{this.change}} />`);

      await waitFor(".btn-remove-icon");
      await click(".btn-remove-icon");
      expect(value).to.deep.equal({
        ids: ["2"],
        model: "icon",
        viewMode: "small",
        links: "disabled",
        gutter: "default",
        rowColSize: "auto",
      });
    });

    it("can add an icon", async function () {
      this.set("value", { data: {} });
      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(hbs`<Editor::Icons @value={{this.value}} @onChange={{this.change}} />`);

      await waitFor(".btn-select-icons");
      await click(".btn-select-icons");
      await click(".btn-icon");

      expect(value).to.deep.equal({
        ids: ["1"],
        model: "icon",
        viewMode: "small",
        links: "disabled",
        gutter: "default",
        rowColSize: "auto",
      });
    });

    it("can change the display settings", async function () {
      this.set("value", {
        data: {
          ids: [icon1._id, icon2._id],
          viewMode: "medium",
          links: "enabled",
        },
      });
      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(hbs`<Editor::Icons @value={{this.value}} @onChange={{this.change}} />`);

      expect(this.element.querySelector(".icons-view-mode").value).to.equal("medium");
      expect(this.element.querySelector(".icons-links").value).to.equal("enabled");

      this.element.querySelector(".icons-view-mode").value = "small";
      await triggerEvent(".icons-view-mode", "change");

      this.element.querySelector(".icons-page-col-size").value = "2";
      await triggerEvent(".icons-page-col-size", "change");

      this.element.querySelector(".icons-gutter").value = "3";
      await triggerEvent(".icons-gutter", "change");

      this.element.querySelector(".icons-links").value = "disabled";
      await triggerEvent(".icons-links", "change");

      expect(value).to.deep.equal({
        ids: ["1", "2"],
        model: "icon",
        viewMode: "small",
        links: "disabled",
        gutter: "3",
        rowColSize: "2",
      });
    });
  });

  describe("when the value is a list of wrapped ids", function () {
    it("renders a list of icons", async function () {
      this.set("value", { data: { records: [icon1, icon2] } });

      await render(hbs`<Editor::Icons @value={{this.value}} />`);

      expect(this.element.querySelectorAll(".icon-container")).to.have.length(2);
      expect(this.element.querySelector(".icon-container img")).to.have.attribute(
        "src",
        "/test/5ca7bfbfecd8490100cab97d.jpg",
      );
    });

    it("can remove an icon", async function () {
      this.set("value", { data: { records: [icon1, icon2] } });
      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(hbs`<Editor::Icons @value={{this.value}} @onChange={{this.change}} />`);

      await click(".btn-remove-icon");
      expect(value).to.deep.equal({
        ids: ["2"],
        model: "icon",
        viewMode: "small",
        links: "disabled",
        gutter: "default",
        rowColSize: "auto",
      });
    });

    it("can add an icon", async function () {
      this.set("value", { data: {} });
      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(hbs`<Editor::Icons @value={{this.value}} @onChange={{this.change}} />`);

      await waitFor(".btn-select-icons");
      await click(".btn-select-icons");
      await click(".btn-icon");

      expect(value).to.deep.equal({
        ids: ["1"],
        model: "icon",
        viewMode: "small",
        links: "disabled",
        gutter: "default",
        rowColSize: "auto",
      });
    });
  });
});
