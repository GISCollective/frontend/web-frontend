/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "../base/modelItem";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { toPagePath } from "core/lib/slug";
import { guidFor } from "@ember/object/internals";

export default class ManageEditorsFeatureCampaignContributionComponent extends Component {
  elementId = guidFor(this);

  @tracked _styleClasses;
  @tracked _button;
  @tracked pagesMap;
  @tracked buttonConfig;

  get buttonOptions() {
    if (this._styleClasses) {
      return this._styleClasses;
    }

    return this.args?.value?.data?.style?.classes;
  }

  get button() {
    if (this._button) {
      return this._button;
    }

    return this.args?.value?.data?.button ?? {};
  }

  triggerChange() {
    return this.args.onChange({
      source: this.source,
      button: this.button,
      style: {
        classes: this.buttonOptions,
      },
    });
  }

  get pageList() {
    if (!this.pagesMap) {
      return [];
    }

    const result = [];

    Object.keys(this.pagesMap)
      .filter((a) => a.includes(`:campaign_id`) || a.includes(`:campaign-id`))
      .map(toPagePath)
      .forEach((a) => {
        result.push(a);
      });

    return result;
  }

  @action
  async setup() {
    this.pagesMap = await this.args?.space?.getPagesMap();

    this.buttonConfig = [
      {
        name: "label",
        type: "text",
      },
      {
        name: "destination",
        options: this.pageList,
      },
    ];
  }

  @action
  changeSource(id) {
    this._source = {
      model: "campaign",
      id,
    };

    return this.triggerChange();
  }

  @action
  changeButtonIssueType(value) {
    this._buttonIssueType = value;

    return this.triggerChange();
  }

  @action
  changeButtonOptions(value) {
    this._styleClasses = value;

    return this.triggerChange();
  }

  @action
  changeButton(value) {
    this._button = value;
    this.triggerChange();
  }
}
