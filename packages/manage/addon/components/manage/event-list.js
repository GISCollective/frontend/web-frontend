import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageEventList extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  change(newValue) {
    this._value = newValue;
  }

  @action
  save() {
    return this.args.onSave?.(this.value);
  }

  @action
  cancel() {
    return this.args.onCancel?.();
  }
}
