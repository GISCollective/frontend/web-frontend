/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { addIconGroupToStore } from 'models/lib/icons';
import { tracked } from '@glimmer/tracking';

export default class InputIconsSelectorComponent extends Component {
  @service store;
  @tracked icons;
  @tracked isLoading;
  @tracked category;
  @tracked subcategory;
  @tracked _iconSet;
  @tracked value;

  get iconSet() {
    if (this._iconSet) {
      return this._iconSet;
    }

    if (!this.args.iconSets?.length) {
      return null;
    }

    return this.args.iconSets[0];
  }

  @action
  changeIconSet(iconSet) {
    this._iconSet = iconSet;

    return this.setup();
  }

  get selectedIcons() {
    if (!this.icons) {
      return;
    }

    const value = this.value?.map?.((a) => a.id) ?? [];
    let group = this.icons;

    if (this.category) {
      group = group.categories[this.category];
    }

    if (this.subcategory) {
      group = group.categories[this.subcategory];
    }

    return group?.icons?.filter((icon) => !value.includes(icon.id)) ?? [];
  }

  get subcategories() {
    return Object.keys(this.icons?.categories[this.category]?.categories ?? {}) ?? [];
  }

  get categories() {
    return Object.keys(this.icons?.categories ?? {}) ?? [];
  }

  updateSubcategory() {
    if (this.subcategories.length > 0) {
      this.subcategory = this.subcategories[0];
    } else {
      this.subcategory = null;
    }
  }

  @action
  addIcon(icon) {
    this.value = [...this.value, icon];

    return this.args.onChange(this.value);
  }

  @action
  changeCategory(value) {
    this.category = value;

    this.updateSubcategory();
  }

  @action
  changeSubcategory(value) {
    this.subcategory = value;
  }

  @action
  async setup() {
    if (!this.iconSet) {
      return;
    }

    this.value = await this.args.value ?? [];

    this.isLoading = true;
    this.icons = await this.store.adapterFor('icon').group({ iconSet: this.iconSet.id });
    addIconGroupToStore(this.icons, this.store);

    if (this.categories.length > 0) {
      this.category = this.categories[0];
    } else {
      this.category = null;
    }

    this.updateSubcategory();

    this.isLoading = false;
  }

  @action
  onChangeCategoryState(state) {
    const newState = fromPageState(state);
    const subcategoryList = Object.keys(this.icons?.categories?.[newState.ic]?.categories ?? {});

    newState.isc = subcategoryList[0];

    return this.args.onChangeState(toPageState(newState));
  }
}
