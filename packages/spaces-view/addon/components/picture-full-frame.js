import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { service } from '@ember/service';

export default class PictureFulllframeComponent extends Component {
  @service fullscreen;

  @tracked pictureTarget;
  @tracked isPicturePresentable = false;
  @tracked isPicturePresenting = false;

  get largePictureValues() {
    return {
      sizing: 'auto',
      classes: [],
      attributions: 'below',
    };
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.fullscreen.unfreezeBody();
  }

  @action
  setup() {
    this.pictureTarget = document.querySelector('#picture-target');

    later(() => {
      this.fullscreen.freezeBody();
      this.isPicturePresentable = true;
    }, 1);
  }

  @action
  hidePicture() {
    this.isPictureVisible = false;
    this.isPicturePresentable = false;
    this.isPicturePresenting = false;

    this.args.onHide?.();
  }

  @action
  checkHidePicture() {
    if (!this.isPicturePresentable) {
      return this.hidePicture();
    }

    this.isPicturePresenting = true;
  }

  @action
  prepareHidePicture() {
    this.isPicturePresentable = false;

    later(() => {
      const modal = this.pictureTarget.querySelector(
        '.page-col-gallery-picture',
      );

      if (!modal) {
        return;
      }

      const style = getComputedStyle?.(modal);

      if (style.opacity == 0) {
        this.checkHidePicture();
      }
    }, 10);
  }
}
