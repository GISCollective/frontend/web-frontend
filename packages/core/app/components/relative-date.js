/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class RelativeDateComponent extends Component {
  get lastChangeOnSeconds() {
    if (!this.args.date) {
      return false;
    }

    if (!this.args.date?.getFullYear?.()) {
      return false;
    }

    if (this.args.date.getFullYear() < 2000) {
      return false;
    }

    const diff = new Date() - this.args.date;

    return parseInt(diff / 1000);
  }

  get lastChangeOn() {
    if (isNaN(this.lastChangeOnSeconds)) {
      return '';
    }

    if (Math.abs(this.lastChangeOnSeconds) < 60) {
      return parseInt(this.lastChangeOnSeconds) * -1;
    }

    if (Math.abs(this.lastChangeOnSeconds) < 3600) {
      return parseInt(this.lastChangeOnSeconds / 60) * -1;
    }

    if (Math.abs(this.lastChangeOnSeconds) < 3600 * 24) {
      return parseInt(this.lastChangeOnSeconds / 3600) * -1;
    }

    if (Math.abs(this.lastChangeOnSeconds) < 3600 * 24 * 365) {
      return parseInt(this.lastChangeOnSeconds / (3600 * 24)) * -1;
    }

    return parseInt(this.lastChangeOnSeconds / (3600 * 24 * 365)) * -1;
  }

  get lastChangeOnUnit() {
    if (isNaN(this.lastChangeOnSeconds)) {
      return '';
    }

    if (Math.abs(this.lastChangeOnSeconds) < 60) {
      return 'second';
    }

    if (Math.abs(this.lastChangeOnSeconds) < 3600) {
      return 'minute';
    }

    if (Math.abs(this.lastChangeOnSeconds) < 3600 * 24) {
      return 'hour';
    }

    if (Math.abs(this.lastChangeOnSeconds) < 3600 * 24 * 365) {
      return 'day';
    }

    return 'year';
  }
}
