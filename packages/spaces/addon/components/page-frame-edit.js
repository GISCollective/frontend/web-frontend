import Component from "@glimmer/component";
import { action } from "@ember/object";
import { later } from "@ember/runloop";
import { tracked } from "@glimmer/tracking";

export default class PageFrameEdit extends Component {
  get highlightSelection() {
    if (!this.args.selection?.isHighlight) {
      return null;
    }

    return this.args.selection;
  }

  @action
  ensureColExists(name) {
    later(() => {
      this.args.model.page?.upsertPageColByName(name);
    });
  }
}
