import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | input-event-list', function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.beforeEach(function() {
    server.testData.storage.addDefaultEvent('1');
    server.testData.storage.addDefaultEvent('2');
  });

  hooks.after(function() {
    server.shutdown();
  });

  it('renders nothing by default', async function (assert) {
    await render(hbs`<Input::EventList />`);

    expect(this.element.querySelector('li')).not.to.exist;
    expect(this.element.querySelector('select')).not.to.exist;
    expect(this.element.querySelector('.btn-add')).not.to.exist;
  });

  it('can add one event', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('events', await this.store.findAll('event'));

    await render(hbs`<Input::EventList @list={{this.events}} @onChange={{this.change}} />`);

    await click('.btn-add');

    this.element.querySelector('select').value = '1';
    await triggerEvent('select', 'change');

    expect(value.map((a) => a.id)).to.deep.equal(['1']);
  });
});
