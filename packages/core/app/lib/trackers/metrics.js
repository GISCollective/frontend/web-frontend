/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { camelize } from '@ember/string';

export class MetricsTracker {
  constructor(store, preferences, fastboot) {
    this.preferences = preferences;
    this.fastboot = fastboot;
    this.store = store;
  }

  addPage() {}

  addView(type, id, labels = {}) {
    if (this.preferences.referer) {
      labels['referer'] = this.preferences.referer;
    }

    return this.store
      .createRecord('metric', {
        name: id,
        type: camelize(`${type}View`),
        reporter: this.preferences.uid,
        value: 1,
        time: new Date(),
        labels,
      })
      .save();
  }

  addLoad(type, id, labels = {}) {
    if (this.preferences.referer) {
      labels['referer'] = this.preferences.referer;
    }

    return this.store
      .createRecord('metric', {
        name: id,
        type: camelize(`${type}Load`),
        reporter: this.preferences.uid,
        value: 1,
        time: new Date(),
        labels,
      })
      .save();
  }

  addMapView(path, mapId) {
    let labels = {};

    if (this.preferences.referer) {
      labels['referer'] = this.preferences.referer;
    }

    return this.store
      .createRecord('metric', {
        name: mapId,
        type: 'mapView',
        reporter: this.preferences.uid,
        value: 1,
        time: new Date(),
        labels,
      })
      .save();
  }
}
