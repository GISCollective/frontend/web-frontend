/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ManageDashboardsDashboardController extends Controller {
  @service router;
  @service intl;

  queryParams = ['category', 'teamCategory'];

  @tracked category = '';
  @tracked teamCategory = '';

  @action
  browseArticle(id) {
    this.router.transitionTo('manage.articles.edit', id);
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }

  @action
  selectCategory(name) {
    this.category = name;
  }

  @action
  selectTeamCategory(name) {
    this.category = '';
    this.teamCategory = name;
  }

  get addCategory() {
    const list = [];

    if (this.teamCategory) {
      list.push(this.teamCategory);
    }

    if (this.category) {
      list.push(this.category);
    }

    return list.join(',');
  }

  get query() {
    const result = {
      team: this.model.team.id,
      type: 'any',
    };

    const categoryQuery = [];

    if (this.category) {
      categoryQuery.push(this.category);
    }

    if (this.teamCategory) {
      categoryQuery.push(this.teamCategory);
    }

    if (categoryQuery.length) {
      result.category = categoryQuery.join(',');
    }

    return result;
  }

  get categoryState() {
    return `cn@${this.category}`;
  }

  get hasCategories() {
    if (!this.teamCategory && this.model.team?.categories?.length) {
      return false;
    }

    return this.model.categoryList.length > 0;
  }

  get categoriesValue() {
    let categoryList = this.model.categoryList;

    categoryList = [this.intl.t('show all'), ...categoryList];

    return {
      data: {
        categoryList,
        categoryStyle: {
          color: 'gray-900',
          hoverColor: 'primary',
          activeColor: 'blue',
          classes: ['fw-bold'],
        },
      },
    };
  }

  get teamCategoriesValue() {
    if (!this.model.team?.categories) {
      return null;
    }

    let categories = this.model.team?.categories;

    const categoryList = [this.intl.t('show all'), ...categories];

    return {
      data: {
        categoryList,
        categoryStyle: {
          color: 'gray-900',
          hoverColor: 'primary',
          activeColor: 'blue',
          classes: ['fw-bold'],
        },
      },
    };
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        text: this.intl.t('articles'),
        capitalize: true,
      },
    ];
  }
}
