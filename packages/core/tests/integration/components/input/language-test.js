import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | input/language', function (hooks) {
  setupRenderingTest(hooks);

  it('renders an empty select when there is no language available', async function (assert) {
    await render(hbs`<Input::Language />`);

    expect(this.element.querySelectorAll('option').length).to.equal(1);
  });

  it('renders a selected language when some are available', async function (assert) {
    const service = this.owner.lookup('service:user');
    service.languages = [
      { name: 'English', locale: 'en-us' },
      { name: 'Romana', locale: 'ro-ro' },
      { name: 'French', locale: 'fr-fr' },
    ];

    await render(hbs`<Input::Language @value="fr-fr" />`);

    expect(this.element.querySelectorAll('option').length).to.equal(3);
    expect(this.element.querySelector('select').value).to.equal('fr-fr');
  });

  it('raises the on change event when the value is changed', async function () {
    const service = this.owner.lookup('service:user');
    service.languages = [
      { name: 'English', locale: 'en-us' },
      { name: 'Romana', locale: 'ro-ro' },
      { name: 'French', locale: 'fr-fr' },
    ];

    let value;
    this.set('changed', (v) => {
      value = v;
    });

    await render(hbs`<Input::Language @value="fr-fr" @onChange={{this.changed}} />`);

    this.element.querySelector('select').value = 'en-us';
    await triggerEvent('select', 'change');

    expect(value).to.equal('en-us');
  });
});
