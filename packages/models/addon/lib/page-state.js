/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export const stateKeyToModel = {
  if: ['state-icons', 'state-icon'],
  l: ['state-location'],
  fid: ['state-feature-id'],
  cn: ['state-category'],
  cu: ['state-currency'],
  at: ['state-attributes'],
  da: ['state-date'],
  er: ['state-repetition'],
  ffid: ['state-feature'],
  dow: ['state-day-of-week'],
};

export const modelToStateKey = {};

for (const key in stateKeyToModel) {
  for (const modelKey of stateKeyToModel[key]) {
    modelToStateKey[modelKey] = key;
    modelToStateKey[modelKey.replace('state-', '')] = key;
  }
}

export function fromPageState(strState) {
  const state = {};

  const pieces = strState?.split('@') ?? [];

  if (pieces.length <= 1) {
    return state;
  }

  for (let i = 0; i < pieces.length; i += 2) {
    state[pieces[i]] = pieces[i + 1];
  }

  return state;
}

export function toPageState(state) {
  const pieces = [];

  for (const key of Object.keys(state ?? {})) {
    if (state[key] !== undefined) {
      pieces.push(key);
      pieces.push(state[key]);
    }
  }

  return pieces.join('@');
}

export function fillPageStateInModel(state, model) {
  for (const value of Object.values(stateKeyToModel).flatMap((a) => a)) {
    model[value] = undefined;
  }

  for (const key of Object.keys(state)) {
    const value = state[key];

    for (const dest of stateKeyToModel?.[key] ?? []) {
      model[dest] = value;
    }
  }
}
