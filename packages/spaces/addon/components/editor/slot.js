/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";
import { service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { PageCol } from "models/transforms/page-col-list";
import { later } from "@ember/runloop";

export default class EditorSlotComponent extends Component {
  @service pageCols;
  @service store;
  @service fastboot;

  @tracked _value;
  @tracked showChangeGlobalType;

  @tracked isReady = true;
  prevPageCol = "";

  elementId = `page-col-editor-${guidFor(this)}`;

  get gid() {
    return this.value.gid;
  }

  get name() {
    return this.value.name;
  }

  get value() {
    if (this._value !== undefined) {
      return this._value;
    }

    return this.args.value;
  }

  get availableTypes() {
    return this.pageCols.availableTypes;
  }

  get type() {
    return this.value.type;
  }

  get hasUpdate() {
    return typeof this.newValue != "undefined" || this.newType || this.newType === null;
  }

  get space() {
    return this.args.model?.space;
  }

  get selectedComponent() {
    if (this.value.gid) {
      return this.value.gid;
    }

    return this.type;
  }

  @action
  changeName(newName) {
    if (this.value.name == newName) {
      return;
    }

    try {
      this.args.model.page.renameCol(this.value.name, newName);
    } catch (err) {
      console.log(err);
      return;
    }

    const result = new PageCol(this.value);
    result.name = newName;
    this._value = result;

    return this.args.onChange?.(result);
  }

  change(gid) {
    const result = new PageCol(this.value);
    result.gid = gid;
    this._value = result;

    return this.args.onChange?.(result);
  }

  @action
  async changeGid(id) {
    await this.change(id);

    this.space.cols?.setPageCol?.(id, this.value.type, this.value.data);
  }

  @action
  removeGid() {
    return this.change();
  }

  @action
  async typeChanged(value) {
    let result;

    if (!value) {
      result = new PageCol(this.value);

      result.data = {};
      result.type = "";
      result.gid = "";

      this._value = result;
      return this.args.onChange?.(result);
    }

    if (this.space?.hasPageCol(value)) {
      const col = this.space?.getPageCol(value);
      result = new PageCol(col);
      result.gid = value;
      result.name = this.value.name;
      result.col = this.value.col;
      result.row = this.value.row;
      result.container = this.value.container;
    } else {
      result = new PageCol(this.value);
      result.type = value;
      result.gid = null;
    }

    this._value = result;
    return this.args.onChange?.(result);
  }

  @action
  async dataChanged(value) {
    this.newValue = value;
    return this.change();
  }

  @action
  changeGlobalType(newType) {
    this.showChangeGlobalType = false;

    const result = new PageCol(this.value);
    result.type = newType;

    this._value = result;

    this.space.cols?.setPageCol?.(this.value.gid, this.value.type, this.value.data);
    return this.args.onChange?.(result);
  }

  @action
  toggleChangeGlobalType() {
    this.showChangeGlobalType = !this.showChangeGlobalType;
  }

  @action
  ensureCol() {
    if (this.value) {
      return;
    }

    let name = this.args.element?.attributes?.getNamedItem?.("data-col-name")?.value ?? "";

    let result = new PageCol({
      name,
    });

    later(() => {
      this._value = result;
    });
  }
}
