/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';

export default class AddSpaceRoute extends AuthenticatedRoute {
  modelsToClear = ['space'];

  queryParams = {
    all: { refreshModel: true },
    next: { refreshModel: false },
  };

  model(params) {
    if (this.isFastBoot) {
      return;
    }

    const teamParams = {};

    if (params.all) {
      teamParams.all = params.all;
    } else {
      teamParams.edit = true;
    }

    return hash({
      space: this.store.createRecord('space', {
        name: '',
        description: '',
        info: new ModelInfo(),
        visibility: {
          isPublic: false,
          team: null,
        },
      }),
      teams: this.store.query('team', teamParams),
    });
  }
}
