/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class UserDropDownComponent extends Component {
  @service session;
  @service user;
  @service fastboot;

  @action
  handleSignOut() {
    return this.args.onSignOut?.();
  }
}
