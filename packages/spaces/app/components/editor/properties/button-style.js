/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { dasherize } from "@ember/string";

export default class ManageEditorsPropertiesButtonStyleComponent extends Component {
  get allOptions() {
    const options = [
      "primary",
      "secondary",
      "success",
      "danger",
      "warning",
      "info",
      "light",
      "dark"]

    const colors = this.args.space?.colorPalette?.customColors?.map(a => a.name.toLowerCase().replace(/[^0-9a-z_-\s]/gi, '').replace(/\s{2,}/g, ' ')) ?? [];

    for(let color of colors) {
      options.push(dasherize(color));
    }

    return [...options, ...options.map(a => `outline-${a}`), "link", "color"];
  }
}
