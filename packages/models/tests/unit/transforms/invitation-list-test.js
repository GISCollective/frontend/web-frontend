import { module } from 'qunit';

import { setupTest, it } from 'dummy/tests/helpers';
import { expect } from 'chai';

module('Unit | Transform | invitation list', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:invitation-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize a list of strings', function () {
    let deserialized = transform.deserialize([
      { email: 'test', role: 'guests' },
      { email: 'Test2', role: 'members' },
    ]);

    expect(deserialized).to.deep.equal([
      { email: 'test', role: 'guests' },
      { email: 'Test2', role: 'members' },
    ]);
  });

  it('can serialize an array', function () {
    const list = transform.deserialize([
      { email: 'test', role: 'guests' },
      { email: 'Test2', role: 'members' },
    ]);

    expect(transform.serialize(list)).to.deep.equal([
      { email: 'test', role: 'guests' },
      { email: 'Test2', role: 'members' },
    ]);
  });
});
