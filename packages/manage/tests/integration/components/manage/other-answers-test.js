import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/other-answers', function (hooks) {
  setupRenderingTest(hooks);

  describe('the view mode', function () {
    it('renders an alert when other questions are not defined', async function (assert) {
      await render(hbs`<Manage::OtherAnswers @title="title" />`);

      expect(this.element.querySelector('.alert.alert-info')).to.have.textContent(
        'The survey does not include any additional general questions.',
      );
    });

    it('renders the answer of a generic question when set', async function (assert) {
      this.set('questions', [
        {
          question: 'extra?',
          type: 'short text',
          name: 'extra',
          isRequired: true,
          isVisible: true,
        },
        {
          question: 'name?',
          type: 'short text',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ]);

      this.set('value', {
        extra: 'value',
      });

      await render(hbs`<Manage::OtherAnswers @title="title" @value={{this.value}} @questions={{this.questions}} />`);

      expect(this.element.querySelectorAll('.question')).to.have.length(1);
      expect(this.element.querySelector('.question')).to.have.textContent('extra?');

      expect(this.element.querySelectorAll('.value')).to.have.length(1);
      expect(this.element.querySelector('.value')).to.have.textContent('value');
    });
  });

  describe('the edit mode', function () {
    it('renders the answer of a generic question when set', async function (assert) {
      this.set('questions', [
        {
          question: 'extra?',
          type: 'short text',
          name: 'extra',
          isRequired: true,
          isVisible: true,
        },
        {
          question: 'name?',
          type: 'short text',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ]);

      this.set('value', {
        extra: 'value',
      });

      await render(
        hbs`<Manage::OtherAnswers @title="title" @editablePanel="title" @value={{this.value}} @questions={{this.questions}} />`,
      );

      expect(this.element.querySelectorAll('.form-label')).to.have.length(1);
      expect(this.element.querySelector('.form-label')).to.have.textContent('extra? *');

      expect(this.element.querySelectorAll('input')).to.have.length(1);
      expect(this.element.querySelector('input').value).to.equal('value');
    });

    it('can change an answer', async function (assert) {
      this.set('questions', [
        {
          question: 'extra?',
          type: 'short text',
          name: 'extra',
          isRequired: true,
          isVisible: true,
        },
        {
          question: 'name?',
          type: 'short text',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ]);

      this.set('value', {
        extra: 'value',
      });

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::OtherAnswers @title="title" @editablePanel="title" @onSave={{this.save}} @value={{this.value}} @questions={{this.questions}} />`,
      );

      await fillIn('input', 'new value');

      await click('.btn-submit');

      expect(value).to.deep.equal({
        extra: 'new value',
      });
    });
  });
});
