/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class AddNewsletterController extends Controller {
  @service store;
  @service intl;
  @service router;

  destination = 'manage.dashboards.newsletter';

  get editableModel() {
    return this.model.newsletter;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.newsletter.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new newsletter'),
      capitalize: true,
    });

    return pieces;
  }

  @action
  async save() {
    let result;

    result = await this.editableModel.save();

    this.router.transitionTo(this.destination, result.id);

    return result;
  }
}
