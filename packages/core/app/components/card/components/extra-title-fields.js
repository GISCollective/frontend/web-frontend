/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { toDateTime } from 'models/transforms/calendar-entry-list';
import { DateTime } from 'luxon';

export default class CardComponentsExtraTitleFieldsComponent extends Component {
  get extraFields() {
    return this.args.options?.card?.extraFields ?? this.args.options?.extraFields;
  }

  get nextOccurrence() {
    if (!this.extraFields?.nextOccurrence) {
      return false;
    }

    if (!this.args.record.nextOccurrence) {
      return false;
    }

    return this.args.record.nextOccurrence;
  }

  get icons() {
    if (!this.extraFields?.icons) {
      return false;
    }

    if (!this.args.record.cachedIcons) {
      return false;
    }

    return this.args.record.cachedIcons;
  }

  get nextDateWithLocation() {
    if (!this.extraFields?.nextDateWithLocation) {
      return false;
    }

    if (!this.args.record.nextOccurrence?.begin) {
      return false;
    }

    const begin = toDateTime(this.args.record.nextOccurrence.begin);
    const year = begin.year ?? 0;

    if (year < 1900) {
      return '';
    }

    return begin.toLocaleString(DateTime.DATE_HUGE);
  }
}
