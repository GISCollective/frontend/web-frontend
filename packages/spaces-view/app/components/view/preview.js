/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { getOwner } from '@ember/application';

export default class PageColPreviewComponent extends Component {
  @service store;

  get component() {
    return `view/${this.args.value?.type}`;
  }

  get componentPreview() {
    return `view/${this.args.value?.type}-preview`;
  }

  get hasPreview() {
    const owner = getOwner(this);
    const lookup = owner.lookup('component-lookup:main');

    if (!lookup.componentFor) {
      return !!lookup.lookupFactory(this.componentPreview);
    }

    return !!(
      lookup.componentFor(this.componentPreview, owner) ||
      lookup.layoutFor(this.componentPreview, owner)
    );
  }

  get hasComponent() {
    const owner = getOwner(this);
    const lookup = owner.lookup('component-lookup:main');

    if (!lookup.componentFor) {
      return !!lookup.lookupFactory(this.component);
    }

    return !!(
      lookup.componentFor(this.component, owner) ||
      lookup.layoutFor(this.component, owner)
    );
  }
}
