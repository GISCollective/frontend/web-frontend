/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import PageColPictureComponent from './picture';

export default class PageColPicturePreviewComponent extends PageColPictureComponent {}
