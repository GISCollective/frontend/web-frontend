/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { expect } from 'chai';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | input/calendar-entry-list', function (hooks) {
  setupRenderingTest(hooks);
  let value;

  hooks.beforeEach(function () {
    value = undefined;

    this.set('change', function (v) {
      value = v;
    });
  });

  it('renders the add button when there is no value', async function (assert) {
    await render(hbs`<Input::CalendarEntryList />`);

    expect(this.element.querySelector('.btn-add')).to.exist;
  });

  it('can add a new value', async function (a) {
    const now = new Date();
    now.setSeconds(0, 0);

    await render(hbs`<Input::CalendarEntryList @onChange={{this.change}} />`);

    await click('.btn-add');

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: now.toISOString(),
          end: now.toISOString(),
          intervalEnd: '0001-01-01T00:00:00.000Z',
          repetition: 'norepeat',
          timezone: 'CET',
        },
      ],
    );
  });

  it('shows a value', async function (a) {
    const now = new Date();
    now.setSeconds(0, 0);

    this.set('value', [
      {
        begin: '2023-01-01T00:00:00.000Z',
        end: '2023-01-01T01:00:00.000Z',
        intervalEnd: '2024-05-01T01:00:00.000Z',
        repetition: 'monthly',
      },
    ]);

    await render(hbs`<Input::CalendarEntryList @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector('.value-begin .datepicker-input').value).to.equal(`2023-1-1`);
    expect(this.element.querySelector('.value-end .datepicker-input').value).to.equal(`2023-1-1`);
    expect(this.element.querySelector('.value-repeats').value).to.equal(`monthly`);
    expect(this.element.querySelector('.value-interval-end .datepicker-input').value).to.equal(`2024-05-01`);
  });

  it('can change a value', async function (a) {
    const now = new Date();
    now.setSeconds(0, 0);

    this.set('value', [
      {
        begin: '2023-01-01T00:00:00.000Z',
        end: '2023-01-01T01:00:00.000Z',
        intervalEnd: '2024-05-01T01:00:00.000Z',
        repetition: 'monthly',
        timezone: 'CET',
      },
    ]);

    await render(hbs`<Input::CalendarEntryList @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('.value-begin select.hour').value = '15';
    await triggerEvent('.value-begin select.hour', 'change');

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: '2023-01-01T14:00:00.000Z',
          end: '2023-01-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'monthly',
          timezone: 'CET',
        },
      ],
    );
  });
});
