/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';

export default class AddBaseMapRoute extends AuthenticatedRoute {
  modelsToClear = ['base-map'];

  queryParams = {
    all: { refreshModel: true },
  };

  model(params) {
    if (this.isFastBoot) {
      return;
    }

    const teamParams = { edit: true };

    if (params.all) {
      teamParams.all = params.all;
    }

    return hash({
      baseMap: this.store.createRecord('base-map', {
        name: '',
        icon: '',
        info: new ModelInfo(),
        visibility: {
          isPublic: false,
          team: null,
        },
        layers: [],
      }),
      teams: this.store.query('team', teamParams),
    });
  }
}
