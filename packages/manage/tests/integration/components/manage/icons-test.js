/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { expect } from 'chai';
import { move } from 'core/lib/trigger';

module('Integration | Component | manage/icons', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let icon1Data;
  let icon2Data;
  let iconSet1Data;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    iconSet1Data = server.testData.storage.addDefaultIconSet();

    icon1Data = server.testData.storage.addDefaultIcon('1');
    icon2Data = server.testData.storage.addDefaultIcon('2');
    icon2Data.name = 'icon 2';
    icon2Data.localName = 'icon 2';
  });

  describe('in view mode', function () {
    it('renders a list with one icon', async function (assert) {
      const icon = await this.store.findRecord('icon', icon1Data._id);
      this.set('value', [icon]);

      await render(hbs`<Manage::Icons @value={{this.value}} @title="title" />`);

      expect(this.element.querySelectorAll('.icon-name')).to.have.length(1);
      expect(this.element.querySelector('.icon-name').textContent.trim()).to.equal('Healthy Dining');
    });

    it('renders a list with 2 icons', async function (assert) {
      const icon1 = await this.store.findRecord('icon', icon1Data._id);
      const icon2 = await this.store.findRecord('icon', icon2Data._id);

      this.set('value', [icon1, icon2]);

      await render(hbs`<Manage::Icons @value={{this.value}} @title="title" />`);

      expect(this.element.querySelectorAll('.icon-name')).to.have.length(2);
      expect(this.element.querySelector('.icon-name').textContent.trim()).to.equal('Healthy Dining');
    });

    it('renders a danger message when there are no icons', async function (assert) {
      this.set('value', []);

      await render(hbs`<Manage::Icons @value={{this.value}} @title="title" />`);

      expect(this.element.querySelectorAll('.icon-name')).to.have.length(0);
      expect(this.element.querySelector('.alert')).to.have.classes('alert-danger');
      expect(this.element.querySelector('.alert').textContent.trim()).to.equal('There are no selected icons.');
    });
  });

  describe('in edit mode', function (hooks) {
    hooks.beforeEach(function () {
      this.set('edit', function () {});
    });

    it('can reorder a list with 2 icons', async function (assert) {
      const icon1 = await this.store.findRecord('icon', icon1Data._id);
      const icon2 = await this.store.findRecord('icon', icon2Data._id);

      this.set('value', [icon1, icon2]);

      let value;
      this.set('save', function (v) {
        value = v;
      });

      await render(
        hbs`<Manage::Icons @value={{this.value}} @title="title" @editablePanel="title" @onSave={{this.save}} @onEdit={{this.edit}}/>`,
      );

      expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');

      const list = document.querySelector('.dragSortList');
      await move(list, 0, list, 1, false, 'img');

      await click('.btn-submit');

      expect(value.map((a) => a.id)).to.deep.equal(['2', '1']);
    });

    it('can cancel a change', async function (a) {
      const icon1 = await this.store.findRecord('icon', icon1Data._id);
      const icon2 = await this.store.findRecord('icon', icon2Data._id);

      this.set('value', [icon1, icon2]);

      let value;
      this.set('save', function (v) {
        value = v;
      });

      this.set('cancel', function (v) {
        value = v;
      });

      await render(
        hbs`<Manage::Icons @value={{this.value}} @title="title" @editablePanel="title" @onSave={{this.save}} @onCancel={{this.cancel}} @onEdit={{this.edit}}/>`,
      );

      expect(this.element.querySelector('.btn-cancel')).to.have.attribute('disabled', null);

      const list = document.querySelector('.dragSortList');
      await move(list, 0, list, 1, false, 'img');

      await click('.btn-cancel');

      expect(value).not.to.exist;
      expect([...this.element.querySelectorAll('.icon-name')].map((a) => a.textContent.trim())).to.deep.equal([
        'Healthy Dining',
        'icon 2',
      ]);
    });

    it('can add icons to an empty list', async function (a) {
      const iconSet = await this.store.findRecord('icon-set', iconSet1Data._id);

      this.set('value', []);
      this.set('iconSets', [iconSet]);

      let value;
      this.set('save', function (v) {
        value = v;
      });

      await render(
        hbs`<Manage::Icons @value={{this.value}} @iconSets={{this.iconSets}} @title="title" @editablePanel="title" @onSave={{this.save}} @onEdit={{this.edit}}/>`,
      );

      await click('.icon-list .icon-container');
      await click('.icon-list .icon-container');

      await click('.btn-submit');

      expect(value.map((a) => a.id)).to.deep.equal(['1', '2']);
    });
  });
});
