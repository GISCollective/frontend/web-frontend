/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class ManageCampaignAnswerListItemComponent extends Component {
  get icon() {
    switch (this.args.value?.status) {
      case 0:
        return 'circle';

      case 1:
        return 'clock';

      case 2:
      case 3:
        return 'circle-check';

      case 4:
        return 'circle-xmark';

      default:
        return 'circel-question';
    }
  }
}
