/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { action } from '@ember/object';
import { DateTime } from 'luxon';
import { tracked } from '@glimmer/tracking';

export class CalendarModel {
  @tracked calendar;
  @tracked team;
  @tracked events;
  @tracked day;

  constructor(params, store) {
    this.params = params;
    this.store = store;
  }

  @action
  async reload() {
    this.calendar = await this.store.findRecord('calendar', this.params.id);
    this.team = await this.calendar.visibility.fetchTeam();

    const query = { calendar: this.params.id };

    if (this.params.day) {
      const date = DateTime.fromFormat(this.params.day, 'yyyy-MM-dd');

      query.after = date.startOf('day').toISO();
      query.until = date.endOf('day').toISO();
    }

    this.events = (await this.store.query('event', query)).slice();
  }
}

export default class ManageDashboardsCalendarRoute extends AuthenticatedRoute {
  queryParams = {
    day: {
      refreshModel: true,
    },
  };

  async model(params) {
    const calendarModel = new CalendarModel(params, this.store);
    await calendarModel.reload();

    return calendarModel;
  }
}
