/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageAttributesGroupComponent extends Component {
  @tracked _value;

  get attributes() {
    return this.args.attributes ?? [];
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? {};
  }

  toAttribute(a) {
    return {
      name: a.displayName,
      key: a.name,
      value: this.value[a.name],
      help: a.help,
      isLoading: '',
      format: a.type,
      options: a.options,
    };
  }

  get group() {
    const result = this.attributes.map((a) => this.toAttribute(a));

    return result;
  }

  @action
  save() {
    return this.args.onSave(this.value);
  }

  @action
  cancel() {
    this._value = null;
    return this.args.onCancel();
  }

  @action
  change(key, value) {
    if (!this._value) {
      this._value = this.value;
    }

    this._value[key] = value;
  }
}
