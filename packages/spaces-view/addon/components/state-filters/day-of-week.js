/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { fromPageState, toPageState } from 'core/lib/page-state';

export default class StateFiltersDayOfWeekComponent extends Component {
  @service intl;
  @service clickOutside;

  allDays = [
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
    'sunday',
    'weekday',
    'weekend',
  ];

  get title() {
    return this.args.options?.label ?? this.intl.t('day of week');
  }

  get days() {
    return this.allDays.map((name, value) => ({ name, value }));
  }

  get value() {
    return parseInt(this.state.dow);
  }

  get niceValue() {
    return this.days.find((a) => a.value == this.value)?.name;
  }

  get state() {
    return fromPageState(this.args.state);
  }

  @action
  reset() {
    const newState = this.state;
    delete newState.dow;

    return this.args.onChangeState(toPageState(newState));
  }

  @action
  change(value) {
    const newState = this.state;
    newState.dow = value;

    return this.args.onChangeState(toPageState(newState));
  }
}
