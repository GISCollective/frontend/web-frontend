/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait, changeSelect } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, waitUntil, triggerEvent, fillIn, triggerKeyEvent } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import Service from '@ember/service';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
class MockService extends Service {
  get windowDomain() {
    return MockService._windowDomain;
  }

  name = 'Open Green Map';

  setup() {}

  pagesMap = {};
}

describe('Acceptance | manage/maps/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let map;
  let mapFile;
  let receivedMap;
  let defaultPicture;
  let receivedPicture;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultSpace();
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    map = server.testData.create.map();
    mapFile = server.testData.create.mapFile();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');

    server.testData.storage.addDefaultTeam('000000000000000000000001');

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultPreferences();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultBaseMap();
    server.testData.storage.addDefaultBaseMap('000000000000000000000002');

    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    let team = server.testData.storage.addDefaultTeam();

    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIconSet('000000000000000000000002');

    server.testData.storage.addMapFile(mapFile);
    server.testData.storage.addMap(map);

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/maps/${map._id}`, (request) => {
      receivedMap = JSON.parse(request.requestBody);
      receivedMap.map._id = map._id;

      server.testData.storage.addMap(receivedMap.map);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedMap)];
    });

    server.testData.storage.addStat(`map-views-team-${team._id}`, 20);
    server.testData.storage.addStat(`campaign-contributions-team-${team._id}`, 30);
    server.testData.storage.addStat(`campaign-answer-contributors-team-${team._id}`, 40);
    receivedMap = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should contain all the breadcrumbs links', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Harta Verde București', 'Edit']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
      '/manage/dashboards/map/5ca89e37ef1f7e010007f54c',
    ]);
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/maps/${map._id}`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fmaps%2F5ca89e37ef1f7e010007f54c');
  });

  it('renders a link to the team in the visibility section', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    expect(this.element.querySelector('.container-group-team a')).to.exist;
    expect(this.element.querySelector('.container-group-team a')).to.have.attribute(
      'href',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    );
  });

  it('can change the name', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    await click('.row-article .btn-edit');

    await wait(200);
    await click('.ce-header');
    await fillIn('.ce-header', 'new name');

    await wait(200);
    await triggerKeyEvent('.ce-header', 'keyup', 'Enter');

    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));
    await click('.btn-submit');

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.name).to.equal('new name');
  });

  it('can change the description', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    await wait(200);
    await click('.row-article .btn-edit');

    await click('.ce-paragraph');
    await wait(200);

    await fillIn('.ce-paragraph', 'new description');
    await wait(200);

    await click('.ce-header');

    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
    await click('.btn-submit');

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.description.blocks).to.deep.equal([
      { type: 'header', data: { text: map.name, level: 1 } },
      { type: 'paragraph', data: { text: 'new description' } },
    ]);
  });

  it('can add an icon set', async function (a) {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    await click('.row-icon-sets .btn-edit');
    await changeSelect('.select-use-default-values', '1');

    await click('.row-icon-sets .btn-add-item');
    await click('.row-icon-sets .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    a.deepEqual(receivedMap.map.iconSets, {
      useCustomList: true,
      list: ['5ca7b702ecd8490100cab96f', '000000000000000000000002'],
    });
  });

  it('can show the default icon sets', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    expect(this.element.querySelector('.row-icon-sets').textContent).to.contain('The default icon sets will be used.');
    expect(this.element.querySelector('.row-icon-sets').textContent).to.contain('Green Map® Icons Version 3');
  });

  it('can update the map license', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    await click('.row-license .btn-edit');
    await fillIn('.row-license .input-license-url', 'new url');
    await fillIn('.row-license .input-license-name', 'new name');
    await click('.row-license .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedMap.map.license).to.deep.equal({
      name: 'new name',
      url: 'new url',
    });
  });

  it('can add a base map', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    await click('.row-base-maps .btn-edit');
    await changeSelect('.select-use-default-values', '1');

    await click('.row-base-maps .btn-add-item');
    await click('.row-base-maps .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedMap.map.baseMaps).to.deep.equal({
      useCustomList: true,
      list: ['000000111111222222333333', '000000000000000000000002'],
    });
  });

  it('should render the "is public" label', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain('is public');
    expect(this.element.querySelector('.container-group-is-public label').textContent).to.contain('is public');
  });

  it('should be able to unpublish the map', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-is-public .btn-switch'));

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.visibility.isPublic).to.equal(false);
  });

  it('should be able to set the addFeaturesAsPending flag', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-add-features-as-pending .btn-switch'));

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.addFeaturesAsPending).to.equal(true);
  });

  it('should be able to enable the download links', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-show-download-links .btn-switch'));

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.showPublicDownloadLinks).to.equal(true);
  });

  it('should be able to change the team', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    this.element.querySelector('.value.editing select').value = '000000000000000000000001';
    await triggerEvent('.value.editing select', 'change');

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.visibility.team).to.equal('000000000000000000000001');
  });

  it('should not show the show all teams button', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).not.to.exist;
  });

  it('should not show the hideOnMainMap toggle', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-hide-on-main-map .btn-switch')).not.to.exist;
    expect(this.element.querySelector('.side-bar .value-hide-on-main-map').textContent.trim()).to.equal('no');
  });

  it('should show yes in the hideOnMainMap section when the map is hidden', async function () {
    map = server.testData.create.map('3');
    map.hideOnMainMap = true;
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/maps/3`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-hide-on-main-map .btn-switch')).not.to.exist;
    expect(
      this.element.querySelector('.side-bar .container-group-hide-on-main-map .value').textContent.trim(),
    ).to.equal('yes');
  });

  it('should show yes in the index map section when the map is an index', async function () {
    map = server.testData.create.map('3');
    map.isIndex = true;
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-is-index .btn-switch')).not.to.exist;
    expect(this.element.querySelector('.side-bar .container-group-is-index .value').textContent.trim()).to.equal('yes');
  });

  it('should be able to change the clustering mode', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('#feature-clustering');

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.cluster).to.deep.equal({ mode: 1, map: '' });
  });

  it('should be able to enable a map mask', async function () {
    let maskMap = server.testData.create.map('mask-map');
    server.testData.storage.addMap(maskMap);

    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    await click('.row-mask .btn-edit');
    await click('.row-mask .enable-map');

    this.element.querySelector('.row-mask select').value = 'mask-map';
    await triggerEvent('select', 'change');
    await click('.row-mask .btn-submit');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedMap.map.mask).to.deep.equal({
      isEnabled: true,
      map: 'mask-map',
    });
  });

  it('renders the iframe embed code', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    expect(this.element.querySelector('.row-embedded-map textarea')).to.have.textContent(
      `<iframe src="https://test.giscollective.com/browse/maps/5ca89e37ef1f7e010007f54c/map-view?embed=true&mapInfo=true&showMore=true&filterManyIcons=false&allIcons=false" allowFullScreen="true" allow="fullscreen; geolocation;" title="Harta Verde București" frameborder="0" width="100%" height="500"></iframe>`,
    );
  });

  describe('when the authenticated user is an administrator', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('should be able to toggle the hideOnMainMap field', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click(this.element.querySelector('.side-bar .container-group-hide-on-main-map .btn-switch'));

      await waitUntil(() => receivedMap != null, { timeout: 3000 });

      expect(receivedMap.map.hideOnMainMap).to.equal(true);
    });

    it('should be able to toggle the is index field', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click(this.element.querySelector('.side-bar .container-group-is-index .btn-switch'));

      await waitUntil(() => receivedMap != null, { timeout: 3000 });

      expect(receivedMap.map.isIndex).to.equal(true);
    });

    it('should show all teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?all=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/maps/${map._id}?allTeams=true`);
      expect(server.history).to.contain(`GET /mock-server/teams?all=true`);

      expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).to.exist;
    });

    it('should show owned teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}?allTeams=true`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?edit=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/maps/${map._id}`);
      expect(server.history).to.contain(`GET /mock-server/teams?edit=true`);

      expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).to.exist;
    });
  });

  describe('when there is a private base map', function (hooks) {
    let basemap;
    let iconSet;

    hooks.beforeEach(function () {
      basemap = server.testData.create.baseMap('000000000000000000000002');
      basemap.visibility.isPublic = false;
      server.testData.storage.addBaseMap(basemap);
      server.testData.storage.addDefaultTeam('000000000000000000000002');

      iconSet = server.testData.create.iconSet('000000000000000000000002');
      iconSet.visibility.isPublic = false;
      iconSet.visibility.team = '000000000000000000000002';
      server.testData.storage.addIconSet(iconSet);
    });

    it('should show an alert that the basemaps are private', async function () {
      authenticateSession();

      await visit(`/manage/maps/${map._id}`);

      await click('.row-base-maps .btn-edit');
      await changeSelect('.select-use-default-values', '1');

      await click('.row-base-maps .btn-add-item');
      await click('.row-base-maps .btn-submit');

      await waitUntil(() => !this.element.querySelector('.fa-spinner'));
      expect(this.element.querySelector('.alert-map-with-unpublished-basemaps')).to.exist;
      expect(this.element.querySelector('.alert-map-with-unrelated-basemaps')).not.to.exist;
      expect(this.element.querySelector('.alert-map-with-unrelated-iconsets')).not.to.exist;
    });

    it('shows an alert when a basemap belongs to another team', async function () {
      basemap.visibility.isPublic = true;
      basemap.visibility.team = '000000000000000000000002';
      map.baseMaps.list = [basemap._id];
      map.baseMaps.list.useCustomList = false;

      authenticateSession();

      await visit(`/manage/maps/${map._id}`);
      expect(this.element.querySelector('.alert-map-with-unrelated-basemaps')).to.exist;

      await click('.alert-link');

      await waitUntil(() => !this.element.querySelector('.fa-spinner'));

      expect(receivedMap.map.baseMaps).to.deep.equal({
        useCustomList: false,
        list: [],
      });
    });

    it('shows an alert when an icon set belongs to another team', async function () {
      iconSet.visibility.isPublic = true;
      iconSet.visibility.isDefault = false;
      iconSet.visibility.team = '000000000000000000000002';

      map.iconSets.list = [iconSet._id];
      map.iconSets.list.useCustomList = false;

      authenticateSession();

      await visit(`/manage/maps/${map._id}`);
      expect(this.element.querySelector('.alert-map-with-unrelated-iconsets')).to.exist;

      await click('.alert-link');

      await waitUntil(() => !this.element.querySelector('.fa-spinner'));

      expect(receivedMap.map.iconSets).to.deep.equal({
        useCustomList: false,
        list: [],
      });
    });
  });

  it('does not show the tagline field', async function (a) {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    expect(this.element.querySelector('.row-tag-line')).not.to.exist;
  });

  it('does not show the legacy cover fields', async function (a) {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    expect(this.element.querySelector('.row-cover .input-picture')).not.to.exist;
  });

  it('can change the cover', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    await click('.row-cover .btn-edit');

    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    await waitUntil(() => receivedMap);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Map', id: map._id },
      data: {},
      disableOptimization: false,
    });
    expect(receivedMap.map.cover).to.equal('1');

    await server.waitAllRequests();
    await PageElements.wait(1000);
  });

  describe('on new.opengreenmap.org', function (hooks) {
    hooks.beforeEach(function () {
      this.owner.register('service:space', MockService);
    });

    it('can set a tagline', async function (a) {
      authenticateSession();

      await visit(`/manage/maps/${map._id}`);

      await click('.row-tag-line .btn-edit');
      await fillIn('.row-tag-line input', 'some text value');

      await click('.row-tag-line .btn-submit');

      await waitUntil(() => receivedMap);
      expect(receivedMap.map.tagLine).to.equal('some text value');
    });

    it('can change the map cover', async function () {
      authenticateSession();

      await visit(`/manage/maps/${map._id}`);
      const blob = server.testData.create.pngBlob();

      await triggerEvent(".row-cover .cover input[type='file']", 'change', {
        files: [blob],
      });

      await waitUntil(() => receivedMap);

      expect(receivedPicture.picture.meta).to.deep.equal({
        renderMode: '',
        attributions: '',
        link: { model: 'Map', id: map._id },
        data: {},
        disableOptimization: false,
      });
      expect(receivedMap.map.squareCover).to.equal(null);
      expect(receivedMap.map.cover).to.equal('1');
    });

    it('can change the map square cover', async function () {
      authenticateSession();

      await visit(`/manage/maps/${map._id}`);
      const blob = server.testData.create.pngBlob();

      await triggerEvent(".row-cover .square-cover input[type='file']", 'change', { files: [blob] });

      await waitUntil(() => receivedMap);

      expect(receivedPicture.picture.meta).to.deep.equal({
        renderMode: '',
        attributions: '',
        link: { model: 'Map', id: map._id },
        data: {},
        disableOptimization: false,
      });
      expect(receivedMap.map.squareCover).to.equal('1');
      expect(receivedMap.map.cover).to.equal('5cc8dc1038e882010061545a');
    });
  });
});
