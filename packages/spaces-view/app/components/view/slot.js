/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { getOwner } from '@ember/application';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';

export default class PageColSlotComponent extends Component {
  @service registry;
  @tracked _value;

  get value() {
    if (this._value !== undefined) {
      return this._value;
    }

    return this.args.value;
  }

  get hasType() {
    return (this.args.type || this.value?.type) && this.type;
  }

  get valueClass() {
    const name = this.value?.name ?? '';

    return name.replace(/[^a-z0-9]/gim, '-');
  }

  get type() {
    const type = this.args.type || this.value?.type || 'text';

    let applicationInstance = getOwner(this);
    if (!applicationInstance.resolveRegistration(`component:view/${type}`)) {
      return null;
    }

    return type;
  }

  get hasFillSize() {
    return this.value?.data?.sizing == 'fill';
  }

  get style() {
    if (!this.value?.data?.height) {
      return null;
    }

    return htmlSafe(`height: ${this.value.data.height}px;`);
  }

  @action
  updateProps(event) {
    this._value = event.detail.value;
    this.setup();
  }

  @action
  setup(element) {
    if (!element?.parentNode) {
      return;
    }

    const parent = element.parentNode;
    if (!parent.classList.contains('col')) {
      return;
    }

    if (!this.hasFillSize) {
      parent.classList.remove('size-fill');
      return;
    }

    parent.classList.add('size-fill');
  }
}
