/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ApplicationAdapter from './application';

export default class FileAdapter extends ApplicationAdapter {
  import(model) {
    const url = this.buildURL('file', model.get('id')) + '/importAllItems';
    return this.ajax(url, 'GET');
  }
}
