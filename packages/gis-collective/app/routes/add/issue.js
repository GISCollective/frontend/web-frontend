/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class AddIssueRoute extends Route {
  @service store;

  modelsToClear = ['feature'];

  queryParams = {
    feature: { refreshModel: true },
  };

  async model(params) {
    const feature = await this.store.findRecord('feature', params.feature);
    const maps = await feature.maps;
    const map = await maps[0];

    return hash({
      feature,
      team: map.visibility.fetchTeam(),
    });
  }
}
