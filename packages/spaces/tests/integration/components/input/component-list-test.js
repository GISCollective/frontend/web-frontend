/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | input/component-list", function (hooks) {
  setupRenderingTest(hooks);

  it("triggers the onChange when a component is selected", async function () {
    const pageCols = this.owner.lookup("service:page-cols");

    let value;

    this.set("change", function (v) {
      value = v;
    });

    this.set("groups", pageCols.groups);

    await render(hbs`<Input::ComponentList @value="picture" @groups={{this.groups}} @onChange={{this.change}}/>`);

    expect(this.element.querySelector(".ember-power-select-selected-item .label").textContent.trim()).to.equal(
      "picture",
    );

    await selectSearch(".ember-power-select-trigger", "heading");
    await selectChoose(".ember-power-select-trigger", "heading");

    expect(value).to.equal("heading");
  });
});
