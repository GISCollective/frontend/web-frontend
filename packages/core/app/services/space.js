/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service, { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { toPagePath } from 'core/lib/slug';
import { MatomoTracker } from '../lib/trackers/matomo';

export default class SpaceService extends Service {
  @service store;
  @service fastboot;
  @service session;
  @service tracking;
  @service user;
  @service preferences;

  @tracked currentSpace;
  @tracked defaultSpace;
  @tracked landingPage;
  @tracked summary;
  @tracked isValidSpace = true;

  pagesMap = {};

  get isDefault() {
    return this.currentSpace?.visibility?.isDefault;
  }

  get name() {
    return this.currentSpace?.name || this.summary?.name || '';
  }

  get teamId() {
    return this.currentSpace?.visibility?.teamId;
  }

  get has404() {
    if (!this.currentSpace) {
      return false;
    }

    return Object.keys(this.currentSpace.pagesMap).includes('404');
  }

  get defaultDomain() {
    return this.defaultSpace?.domain ?? '';
  }

  get currentDomain() {
    return this.currentSpace?.domain ?? '';
  }

  get currentLocation() {
    if (this.fastboot.isFastBoot) {
      return '';
    }

    return window.location.toString();
  }

  get windowDomain() {
    let host;
    if (this.fastboot.isFastBoot) {
      host = this.fastboot.request.host;
    } else {
      host = window.location.hostname;
    }

    const portIndex = host.indexOf(':');

    if (portIndex > 0) {
      host = host.substring(0, portIndex);
    }

    return host;
  }

  get hasLegacyMenu() {
    if (this.currentSpace?.cols?.map?.['main-menu'] || this.currentSpace?.cols?.map?.['main menu']) {
      return false;
    }

    return this.currentSpace?.visibility.isDefault && !this.landingPage?.slug;
  }

  get landingPagePath() {
    if (this.currentSpace?.visibility.isDefault && !this.landingPage?.slug) {
      return '/browse/maps/_/map-view';
    }

    if (typeof this.landingPage?.slug == 'string') {
      return toPagePath(this.landingPage?.slug);
    }

    return '/login';
  }

  get globalMapPage() {
    const id = this.currentSpace?.globalMapPageId;

    if (!id) {
      return null;
    }

    let page = this.store.peekRecord('page', id);

    if (!page) {
      page = this.store.findRecord('page', id);
    }

    return page;
  }

  getLinkFor() {
    return this.currentSpace?.getLinkFor(...arguments);
  }

  browseFeatureLink(feature) {
    if (!feature) {
      return ``;
    }

    return `/browse/sites/${feature.id}`;
  }

  browseTeamLink(team) {
    if (!team) {
      return ``;
    }

    return `/browse/teams/${team.id}`;
  }

  isExternalUrl(url) {
    if (!url) {
      return false;
    }

    if (
      this.currentDomain &&
      (url.startsWith(`http://${this.currentDomain}`) || url.startsWith(`https://${this.currentDomain}`))
    ) {
      return false;
    }

    return url.startsWith('http://') || url.startsWith('https://');
  }

  checkTrackers() {
    if (!this.currentSpace?.matomoSiteId || !this.currentSpace?.matomoUrl) {
      return;
    }

    this.tracking.registerTracker(
      'matomo',
      new MatomoTracker(
        this.user,
        {
          matomoSiteId: this.currentSpace.matomoSiteId,
          matomoUrl: this.currentSpace.matomoUrl,
          protocol: this.preferences.protocol,
          domain: this.currentSpace.domain,
          referer: this.preferences.referer,
          uid: this.preferences.uid,
        },
        this.fastboot,
      ),
    );
  }

  async fetchCurrentSpace() {
    let domainSpaces = await this.store.query('space', {
      domain: this.windowDomain,
    });

    if (domainSpaces?.[0]) {
      return domainSpaces?.[0];
    }

    if (this.windowDomain.indexOf('www.') == 0) {
      domainSpaces = await this.store.query('space', {
        domain: this.windowDomain.replace('www.', ''),
      });
    }

    return domainSpaces?.[0];
  }

  async setup() {
    if (!this.isValidSpace) {
      return;
    }

    try {
      this.summary = await this.store.adapterFor('space').fetchSummary(this.windowDomain);
    } catch (err) {
      console.error(err);
      this.isValidSpace = false;
    }

    try {
      this.currentSpace = await this.fetchCurrentSpace();

      const defaultSpaces = await this.store.query('space', {
        default: true,
      });

      this.defaultSpace = defaultSpaces.find((a) => a.visibility.isPublic) || defaultSpaces[0];
    } catch (err) {
      const message = `${err}`;
      console.error('ERROR CODE:', err.code, ' message:', message);
      if (err.code == 'ECONNREFUSED' || message.indexOf('Network') != -1 || message.indexOf('fetch') != -1) {
        return;
      }

      if (this.session.isAuthenticated) {
        console.log('auth INVALIDATE SESSION');
        await this.session.invalidate();
        return this.setup;
      }
    }

    if (this.windowDomain == 'localhost') {
      this.currentSpace = this.defaultSpace;
    }

    if (!this.currentSpace) {
      return;
    }

    await this.currentSpace.getCategories();
    this.pagesMap = await this.currentSpace.getPagesMap();

    try {
      await this.currentSpace.logo;
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('The current space has no logo page.');
    }

    if (this.isFastBoot) {
      this.fastboot.deferRendering(this.pagesMap);
    }

    if (this.currentSpace?.landingPageId) {
      this.landingPage = this.store.peekRecord('page', this.currentSpace?.landingPageId);

      if (!this.landingPage) {
        try {
          this.landingPage = await this.store.findRecord('page', this.currentSpace.landingPageId);
        } catch (err) {
          // eslint-disable-next-line no-console
          console.log('The current space has no landing page.');
        }
      }
    }

    this.checkTrackers();
  }
}
