/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | attributes/view/group', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<Attributes::View::Group />`);

    assert.dom(this.element).hasText('');
  });

  it('renders two values without types', async function () {
    this.set('value', {
      key1: 'value 1',
      key2: 'value 2',
    });

    await render(hbs`<Attributes::View::Group @value={{this.value}} />`);

    const values = [...this.element.querySelectorAll('.attribute')];
    expect(values).to.have.length(2);

    expect(values[0].querySelector('.attribute-name').textContent.trim()).to.equal('key1');
    expect(values[0].querySelector('.attribute-value').textContent.trim()).to.equal('value 1');

    expect(values[1].querySelector('.attribute-name').textContent.trim()).to.equal('key2');
    expect(values[1].querySelector('.attribute-value').textContent.trim()).to.equal('value 2');
  });

  it('renders two values with types', async function () {
    this.set('value', {
      key1: true,
      key2: false,
    });

    this.set('attributes', [
      {
        name: 'key1',
        displayName: 'key 1',
        help: 'is it true?',
        type: 'boolean',
        options: '',
        isPrivate: false,
        from: {},
        isInherited: false,
        isRequired: false,
      },
      {
        name: 'key2',
        displayName: 'key 2',
        help: 'is it false?',
        type: 'boolean',
        options: '',
        isPrivate: false,
        from: {},
        isInherited: false,
        isRequired: false,
      },
    ]);

    await render(hbs`<Attributes::View::Group @value={{this.value}} @attributes={{this.attributes}} />`);

    const values = [...this.element.querySelectorAll('.attribute')];
    expect(values).to.have.length(2);

    expect(values[0].querySelector('.attribute-name').textContent.trim()).to.equal('key 1');
    expect(values[0].querySelector('.attribute-help').textContent.trim()).to.equal('is it true?');
    expect(values[0].querySelector('.attribute-value').textContent.trim()).to.equal('yes');

    expect(values[1].querySelector('.attribute-name').textContent.trim()).to.equal('key 2');
    expect(values[1].querySelector('.attribute-help').textContent.trim()).to.equal('is it false?');
    expect(values[1].querySelector('.attribute-value').textContent.trim()).to.equal('no');
  });
});
