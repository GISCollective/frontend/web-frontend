/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

import { BaseMapLayer } from 'models/transforms/layer';
import { tracked } from '@glimmer/tracking';
import { A } from 'core/lib/array';

export default class InputManageMapLayerListComponent extends Component {
  @tracked values;

  @action
  copyValues() {
    const values = A();

    if (!this.args.values) {
      this.values = values;
      return;
    }

    this.args.values
      .slice()
      .reverse()
      .forEach((layer) => {
        values.push(new BaseMapLayer(layer));
      });

    this.values = values;
  }

  @action
  addLayer() {
    this.values.push(new BaseMapLayer());
  }

  @action
  save() {
    if (this.args.onSave) {
      return this.args.onSave(this.values.slice().reverse());
    }
  }

  @action
  changeLayerType(type, index) {
    this.values[index].type = type;
  }

  @action
  delete(index) {
    this.values.splice(index, 1);
  }

  @action
  moveUp(index) {
    const tmp = this.values[index]

    this.values[index] = this.values[index - 1];
    this.values[index - 1] = tmp;
  }

  @action
  moveDown(index) {
    const tmp = this.values[index]

    this.values[index] = this.values[index + 1];
    this.values[index + 1] = tmp;
  }

  @action
  changeOption(index, options) {
    this.values[index].options = options;
  }
}
