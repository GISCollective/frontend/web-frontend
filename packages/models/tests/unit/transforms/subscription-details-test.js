/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupTest, it } from 'dummy/tests/helpers';
import { expect } from 'chai';

module('Unit | Transform | subscription details', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:subscription-details');
  });

  // Replace this with your real tests.
  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('converts an undefined to an object with all options', function (a) {
    let deserialized = transform.deserialize(undefined);

    a.deepEqual(deserialized.toJSON(), {
      name: '',
      expire: '0001-01-01T00:00:00.000Z',
      domains: [],
      comment: '',
      details: '',
      monthlySupportHours: 0,
      support: [],
      invoices: [],
    });
  });

  it('converts an object with all fields to an object with all options', function (a) {
    let deserialized = transform.deserialize({
      name: 'test',
      expire: '2021-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      details: 'details',
      monthlySupportHours: 1,
      support: [
        {
          hours: 1,
          date: '2022-03-22T00:22:11Z',
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 2,
          date: '2022-03-23T00:22:11Z',
          details: 'details 2',
        },
      ],
    });

    a.deepEqual(deserialized.toJSON(), {
      name: 'test',
      expire: '2021-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      details: 'details',
      monthlySupportHours: 1,
      support: [
        {
          hours: 1,
          date: '2022-03-22T00:22:11.000Z',
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 2,
          date: '2022-03-23T00:22:11.000Z',
          details: 'details 2',
        },
      ],
    });
  });

  it('serializes an object with all properties', function (a) {
    let deserialized = transform.deserialize({
      name: 'test',
      expire: '2021-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      details: 'Details',
      comment: 'other',
      monthlySupportHours: 1,
      support: [
        {
          hours: 1,
          date: '2022-03-22T00:22:11Z',
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 2,
          date: '2022-03-23T00:22:11Z',
          details: 'details 2',
        },
      ],
    });

    a.deepEqual(transform.serialize(deserialized), {
      name: 'test',
      expire: '2021-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      monthlySupportHours: 1,
      details: 'Details',
      support: [
        {
          hours: 1,
          date: '2022-03-22T00:22:11.000Z',
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 2,
          date: '2022-03-23T00:22:11.000Z',
          details: 'details 2',
        },
      ],
    });
  });
});
