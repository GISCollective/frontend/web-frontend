/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { resolveLinkObject } from '../../lib/links';

export default class ComponentsPageColRedirectComponent extends Component {
  get link() {
    return resolveLinkObject(this.args.value?.data?.redirect?.destinations);
  }
}
