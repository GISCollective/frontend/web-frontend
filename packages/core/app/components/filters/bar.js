/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class FiltersBarComponent extends Component {
  @action
  bindScroll(element) {
    this.element = element;

    this.onScroll = () => {
      const topPos = element.getBoundingClientRect().top;

      if (topPos < 0 && !this.element.classList.contains('sticky')) {
        this.element.style.height = this.element.querySelector('.filter-floating-container').offsetHeight + 'px';
        this.element.classList.add('sticky');
      }

      if (topPos >= 0 && this.element.classList.contains('sticky')) {
        this.element.style.height = 'auto';
        this.element.classList.remove('sticky');
      }
    };

    this.onScroll();

    window.addEventListener('scroll', this.onScroll);
  }

  @action
  unbindScroll() {
    window.removeEventListener('scroll', this.onScroll);
  }
}
