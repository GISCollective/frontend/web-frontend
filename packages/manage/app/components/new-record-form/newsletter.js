/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class NewRecordFormNewsletterComponent extends Component {
  @service user;
  @tracked _team;

  get teams() {
    if (this.user.isAdmin) {
      return this.args.teams;
    }

    return this.args.teams?.filter((a) => a.allowNewsletters) ?? [];
  }

  get team() {
    if (this._team) {
      return this._team;
    }

    return this.teamList[0];
  }

  get showTeamSection() {
    return this.teamList.length > 1;
  }

  get teamList() {
    if (this.args.teams?.toArray) {
      return this.args.teams.toArray();
    }

    if (Array.isArray(this.args.teams)) {
      return this.args.teams;
    }

    return [];
  }

  get canSubmit() {
    return this.editableModel?.name;
  }

  get editableModel() {
    return this.args.newsletter;
  }

  @action
  change(field, value) {
    this.editableModel?.set(field, value);
  }

  @action
  changeTeam(value) {
    this._team = value;
  }

  @action
  save() {
    this.editableModel.visibility = {};
    this.editableModel.visibility.team = this.team;
    this.editableModel.visibility.isPublic = false;
    this.editableModel.visibility.isDefault = false;

    return this.args.onSubmit(this.editableModel);
  }
}
