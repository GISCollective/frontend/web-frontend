/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class LoginRegisterController extends Controller {
  @service notifications;
  @service fastboot;
  @service intl;
  @service embed;
  @service router;
  @service store;
  @service space;

  @tracked name = {
    salutation: '',
    title: '',
    firstName: '',
    lastName: '',
  };
  @tracked username = '';
  @tracked email = '';
  @tracked password = '';
  @tracked passwordConfirm = '';
  @tracked response = null;
  @tracked isWaiting = false;
  @tracked acceptTerms;
  @tracked acceptPrivacyPolicy;
  @tracked newsletter = true;

  get hasTerms() {
    return this.space.pagesMap["terms"];
  }

  get hasPrivacyPolicy() {
    return this.space.pagesMap["privacy-policy"];
  }

  get siteKey() {
    if (!this.model.challenge) {
      return null;
    }

    return this.model.challenge.siteKey;
  }

  @action
  formInserted(container) {
    this.emailField = container.querySelector('.input-email');
  }

  get isDisabled() {
    if (this.name.firstName.trim() == '') {
      return true;
    }

    if (this.name.lastName.trim() == '') {
      return true;
    }

    if (this.username.trim() == '') {
      return true;
    }

    if (this.passwordConfirm.trim().length < 10) {
      return true;
    }

    if (this.password.trim() == '') {
      return true;
    }

    if (this.passwordConfirm.trim() != this.password.trim()) {
      return true;
    }

    if (this.email.trim() == '') {
      return true;
    }

    try {
      const isEmailValid = this.emailField.checkValidity();
      if (!isEmailValid) {
        return true;
      }
      // eslint-disable-next-line no-empty
    } catch (err) { }

    if (this.hasTerms && !this.acceptTerms) {
      return true;
    }

    if (this.hasPrivacyPolicy && !this.acceptPrivacyPolicy) {
      return true;
    }

    return this.isWaiting;
  }

  @action
  handleNameChange(value) {
    this.name = value;
  }

  @action
  updateResponse(response) {
    this.response = response;
  }

  @action
  changePasswordValue(value) {
    this.password = value ?? '';
    this.passwordConfirm = value ?? '';
  }

  @action
  async createAccount() {
    this.isWaiting = true;

    try {
      await this.store.adapterFor('user').register(this.name, this.username, this.email.toLowerCase(), this.password, this.response, this.newsletter);

      if (!this.embed.isEnabled) {
        this.notifications.showMessage(this.intl.t('Create account'), this.intl.t('register-success-message'));
        await this.router.transitionTo('login.index');
      } else {
        await this.router.transitionTo('login.register-success');
      }
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.isWaiting = false;
  }
}
