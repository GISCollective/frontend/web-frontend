/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsListComponent extends Component {
  @tracked _textStyle;
  @tracked _iconStyle;
  @tracked _textList;

  iconStyleConfig = [
    {
      name: "name",
      options: [
        "circle-check",
        "check",
        "circle",
        "circle-dot",
        "plus",
        "minus",
        "hashtag",
        "asterisk",
        "check-double",
        "square-check",
        "arrow-right",
        "circle-arrow-right",
        "circle-right",
      ],
    },
    {
      name: "size",
      options: ["1", "2", "3", "4"],
    },
    {
      name: "color",
      type: "color",
    },
  ];

  get textList() {
    if (this._textList) {
      return this._textList;
    }

    return this.args.value?.data?.textList;
  }

  get textStyle() {
    if (this._textStyle) {
      return this._textStyle;
    }

    return this.args.value?.data?.textStyle;
  }

  get iconStyle() {
    if (this._iconStyle) {
      return this._iconStyle;
    }

    return this.args.value?.data?.iconStyle;
  }

  @action
  change(name, value) {
    this[name] = value;

    return this.triggerChange();
  }

  triggerChange() {
    return this.args.onChange({
      textList: this.textList,
      textStyle: this.textStyle,
      iconStyle: this.iconStyle,
    });
  }
}
