/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export function getSizeFromWidth(width) {
  const index = getSizeIndexFromWidth(width);

  return getSizeFromIndex(index);
}

export function getSizeBsFromWidth(width) {
  const index = getSizeIndexFromWidth(width);

  return getBsFromSizeIndex(index);
}

/// deprecated
export function getSizeFromIndex(index) {
  if (index == 0) {
    return 'mobile';
  }

  if (index == 1) {
    return 'tablet';
  }

  return 'desktop';
}

export function getSizeIndexFromWidth(width) {
  if (width <= 576) {
    return 0;
  }

  if (width <= 768) {
    return 1;
  }

  return 2;
}

export function getBsFromSizeIndex(index) {
  if (index == 2) {
    return 'lg';
  }

  if (index == 1) {
    return 'md';
  }

  return 'sm';
}

// deprecated
export function getSizeBsFromName(name) {
  if (name == 'desktop') {
    return 'lg';
  }

  if (name == 'tablet') {
    return 'md';
  }

  return undefined;
}

// deprecated
export function getNameFromBs(name) {
  if (name == 'lg') {
    return 'desktop';
  }

  if (name == 'md') {
    return 'tablet';
  }

  return 'mobile';
}

export function getSizeIndexFromName(name) {
  if (name == 'mobile') {
    return 0;
  }

  if (name == 'tablet') {
    return 1;
  }

  return 2;
}

export function getSizeIndexFromBs(name) {
  const map = {
    sm: 0,
    md: 1,
    lg: 2,
    xl: 3,
    xxl: 4,
    mobile: 0,
    tablet: 1,
    desktop: 2,
  };

  return map[name] ?? 0;
}
