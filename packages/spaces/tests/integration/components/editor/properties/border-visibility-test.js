/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/properties/border-visibility", function (hooks) {
  setupRenderingTest(hooks);

  describe("without a device size", function () {
    it("renders the right option without size", async function () {
      this.set("options", ["border-visibility-yes"]);
      await render(hbs`<Editor::Properties::BorderVisibility @options={{this.options}}/>`);

      expect(this.element.querySelector(".input--border-visibility").value).to.equal("yes");
    });

    it("renders the right option with size", async function () {
      this.set("options", ["border-visibility-md-yes"]);
      await render(hbs`<Editor::Properties::BorderVisibility @size="md" @options={{this.options}}/>`);

      expect(this.element.querySelector(".input-md-border-visibility").value).to.equal("yes");
    });

    it("renders the default value when a value for a different device size is set", async function () {
      this.set("options", ["border-visibility-md-yes"]);
      await render(hbs`<Editor::Properties::BorderVisibility @options={{this.options}}/>`);

      expect(this.element.querySelector(".input--border-visibility").value).to.equal("default");
    });

    it("can change the value", async function () {
      let value;

      this.set("change", (v) => {
        value = v;
      });
      this.set("options", ["border-visibility-yes"]);

      await render(hbs`<Editor::Properties::BorderVisibility @options={{this.options}} @onChange={{this.change}}/>`);

      expect(this.element.querySelector(".input--border-visibility").value).to.equal("yes");

      this.element.querySelector(".input--border-visibility").value = "no";
      await triggerEvent(".input--border-visibility", "change");

      expect(value).to.deep.equal(["border-visibility-no"]);
    });
  });
});
