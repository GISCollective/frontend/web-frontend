/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class InputManageAttributionsListComponent extends Component {
  @tracked _list = null;

  get list() {
    if (this._list) {
      return this._list;
    }

    return this.args.value ?? [];
  }

  set list(value) {
    this._list = value;
  }

  @action
  change() {
    this.args.onChange?.(this.list);
  }

  @action
  remove(index) {
    const tmp = [...this.list];
    this._list = [];

    tmp.splice(index, 1);

    later(() => {
      this._list = [...tmp];
      this.change();
    });
  }

  @action
  addItem() {
    const tmp = this.list;

    tmp.push('');

    this._list = [];

    later(() => {
      this._list = [...tmp];
      this.change();
    });
  }

  @action
  setItem(index, value) {
    if (!this._list) {
      this._list = this.list;
    }

    this._list[index] = value;

    this.change();
  }
}
