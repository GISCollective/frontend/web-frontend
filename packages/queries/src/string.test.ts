import { dasherize } from "./string";

describe("dasherize", () => {
  it("converts the docs strings", () => {
    expect(dasherize("innerHTML")).toEqual("inner-html");
    expect(dasherize("action_name")).toEqual("action-name");
    expect(dasherize("css-class-name")).toEqual("css-class-name");
    expect(dasherize("my favorite items")).toEqual("my-favorite-items");
    expect(dasherize("privateDocs/ownerInvoice")).toEqual("private-docs/owner-invoice");
  });
});
