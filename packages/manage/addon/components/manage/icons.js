/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageIconsComponent extends Component {
  @tracked _value;

  get isDisabled() {
    return !this._value;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? [];
  }

  @action
  change(value) {
    this._value = value;
  }

  @action
  save() {
    return this.args.onSave(this.value);
  }

  @action
  cancel() {
    this._value = null;
    return this.args.onCancel();
  }
}
