/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { toPagePath } from "core/lib/slug";

export default class EditorCardOptionsComponent extends Component {
  @service pageCols;
  @service store;

  @tracked _cardType;
  @tracked _container;
  @tracked _destination;
  @tracked _extraFields;
  @tracked _containerListClasses;
  @tracked _cover;
  @tracked _maxItems;

  coverConfig = [
    {
      name: "defaultCover",
      options: ["default", "from parent"],
    },
  ];

  fields = {
    event: ["nextOccurrence", "nextDateWithLocation", "entries", "schedule", "icons"],
    feature: ["icons"],
  };

  get maxItems() {
    if (this._maxItems) {
      return this._maxItems;
    }

    return this.args.value?.maxItems ?? {};
  }

  get containerLisClasses() {
    return this._containerListClasses ?? this.args.value?.containerList?.classes ?? [];
  }

  get extraFieldsConfig() {
    let result = [
      {
        name: "fullDescription",
        type: "bool",
      },
    ];

    if (this.fields[this.modelType]) {
      const fields = this.fields[this.modelType].map((name) => ({
        name,
        type: "bool",
      }));

      result = [...result, ...fields];
    }

    return result;
  }

  get extraFields() {
    return this._extraFields ?? this.args?.value?.extraFields;
  }

  get cover() {
    return this._cover ?? this.args?.value?.cover;
  }

  get cardType() {
    if (this._cardType) {
      return this._cardType;
    }

    return this.args?.value?.type ?? this.pageCols.cardTypes[0];
  }

  get container() {
    if (this._container) {
      return this._container;
    }

    return this.args.value?.container ?? {};
  }

  get destination() {
    if (this._destination) {
      return this._destination;
    }

    return this.args.value?.destination ?? {};
  }

  get modelType() {
    return this.args.element?.attributes?.getNamedItem?.("data-model-type")?.value;
  }

  get pageList() {
    if (!this.modelType) {
      return Object.keys(this.args.model?.space?.pagesMap ?? {}).map(toPagePath);
    }

    const model = this.store.createRecord(this.modelType);
    const validVars = [...(model.relatedModels ?? []), this.modelType].map((a) => `:${a}-id`);

    const isValidSlug = (slug) => {
      return validVars.find((a) => slug.indexOf(a) >= 0);
    };

    const result = [];

    Object.keys(this.args.model.space.pagesMap)
      .filter(isValidSlug)
      .map(toPagePath)
      .forEach((a) => {
        result.push(a);
      });

    return result;
  }

  @action
  changeMaxItems(size, value) {
    const maxItems = this.maxItems;

    maxItems[size] = value;

    this._maxItems = maxItems;

    this.triggerChange();
  }

  @action
  changeExtraFields(value) {
    this._extraFields = value;

    return this.triggerChange();
  }

  @action
  changeCover(value) {
    this._cover = value;

    return this.triggerChange();
  }

  @action
  changeDestination(destination) {
    this._destination = destination;

    return this.triggerChange();
  }

  @action
  changeCardType(value) {
    this._cardType = value;

    return this.triggerChange();
  }

  @action
  onChangeContainer(value) {
    this._container = value;

    return this.triggerChange();
  }

  @action
  onChangeContainerClasses(value) {
    const result = this.container;
    result.classes = value;

    this._container = result;

    return this.triggerChange();
  }

  @action
  onContainerLisClassesChange(value) {
    this._containerListClasses = value;

    return this.triggerChange();
  }

  @action
  triggerChange() {
    const result = {
      type: this.cardType,
      container: this.container,
      containerList: {
        classes: this.containerLisClasses,
      },
      destination: this.destination,
      extraFields: this.extraFields,
      cover: this.cover,
      maxItems: this.maxItems,
    };

    return this.args.onChange?.(result);
  }
}
