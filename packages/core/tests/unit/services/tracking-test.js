/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupTest } from '../../helpers';

describe('Unit | Service | tracking', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let service = this.owner.lookup('service:tracking');
    expect(service).to.be.ok;
  });
});
