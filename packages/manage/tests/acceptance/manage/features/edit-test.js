/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  waitFor,
  fillIn,
  triggerEvent,
  triggerKeyEvent,
} from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/features/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let map;
  let otherMap;
  let defaultPicture;

  hooks.beforeEach(function () {
    server = new TestServer();
    otherMap = server.testData.create.map('2');
    otherMap.name = 'the other map';

    map = server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultUser(false, '5b8a59caef739394031a3f67');
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addMap(otherMap);
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();
    defaultPicture = server.testData.storage.addDefaultPicture('2');
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultSpace();
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('with a line', function (hooks) {
    let line;

    hooks.beforeEach(function () {
      line = server.testData.create.feature('5ca78e2160780601008f69e6');
      line.position.type = 'LineString';
      line.position.coordinates = [
        [1, 2],
        [3, 4],
      ];

      server.testData.storage.addFeature(line);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
      });
    });

    it('should render the feature', async function () {
      authenticateSession();

      await visit(`/manage/features/edit/${line._id}`);
      await PageElements.waitEditorJs(this.element);

      await server.waitAllRequests();

      expect(this.element.querySelector('.main-container > .error')).not.to.exist;

      expect(this.element.querySelector('.container-group-original-author a')).to.have.attribute(
        'href',
        '/browse/profiles/5b8a59caef739394031a3f67',
      );
      expect(this.element.querySelector('.container-group-original-author a')).to.have.attribute(
        'href',
        '/browse/profiles/5b8a59caef739394031a3f67',
      );

      expect(server.history).to.contain('GET /mock-server/maps/5ca89e37ef1f7e010007f54c');
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();

      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((pair) => [Math.ceil(pair[0]), Math.ceil(pair[1])]);

      expect(coordinates).to.deep.equal([
        [1, 2],
        [3, 4],
      ]);

      await PageElements.wait(100);
    });
  });

  describe('with a multi line', function (hooks) {
    let multiline;

    hooks.beforeEach(function () {
      multiline = server.testData.create.feature('5ca78e2160780601008f69e6');
      multiline.position.type = 'MultiLineString';
      multiline.position.coordinates = [
        [
          [1, 2],
          [3, 4],
        ],
      ];

      server.testData.storage.addFeature(multiline);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
      });
    });

    it('should render the feature', async function () {
      authenticateSession();

      await visit(`/manage/features/edit/${multiline._id}`);

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      expect(this.element.querySelector('.main-container > .error')).not.to.exist;

      expect(server.history).to.contain('GET /mock-server/maps/5ca89e37ef1f7e010007f54c');
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) => a.map((pair) => [Math.ceil(pair[0]), Math.ceil(pair[1])]));

      expect(coordinates).to.deep.equal([
        [
          [1, 2],
          [3, 4],
        ],
      ]);

      await PageElements.wait(100);
    });
  });

  describe('with a polygon', function (hooks) {
    let polygon;

    hooks.beforeEach(function () {
      polygon = server.testData.create.feature('5ca78e2160780601008f69e6');
      polygon.position.type = 'Polygon';
      polygon.position.coordinates = [
        [
          [1, 2],
          [3, 4],
          [4, 5],
          [1, 2],
        ],
      ];

      server.testData.storage.addFeature(polygon);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
      });
    });

    it('should render the map feature', async function () {
      authenticateSession();

      await visit(`/manage/features/edit/${polygon._id}`);

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      expect(this.element.querySelector('.main-container > .error')).not.to.exist;

      expect(server.history).to.contain('GET /mock-server/maps/5ca89e37ef1f7e010007f54c');
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) => a.map((pair) => [Math.ceil(pair[0]), Math.ceil(pair[1])]));

      expect(coordinates).to.deep.equal([
        [
          [1, 2],
          [3, 4],
          [4, 5],
          [1, 2],
        ],
      ]);

      await PageElements.wait(100);
    });
  });

  describe('with a MultiPolygon', function (hooks) {
    let multiPolygon;

    hooks.beforeEach(function () {
      multiPolygon = server.testData.create.feature('5ca78e2160780601008f69e6');
      multiPolygon.position.type = 'MultiPolygon';
      multiPolygon.position.coordinates = [
        [
          [
            [1, 2],
            [3, 4],
            [4, 5],
            [1, 2],
          ],
        ],
      ];

      server.testData.storage.addFeature(multiPolygon);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
      });
    });

    it('should render the map feature', async function () {
      authenticateSession();

      await visit(`/manage/features/edit/${multiPolygon._id}`);

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      expect(this.element.querySelector('.main-container > .error')).not.to.exist;

      expect(server.history).to.contain('GET /mock-server/maps/5ca89e37ef1f7e010007f54c');
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) => a.map((b) => b.map((pair) => [Math.ceil(pair[0]), Math.ceil(pair[1])])));

      expect(coordinates).to.deep.equal([
        [
          [
            [1, 2],
            [3, 4],
            [4, 5],
            [1, 2],
          ],
        ],
      ]);

      await PageElements.wait(100);
    });
  });

  describe('with a submited site', function (hooks) {
    let site;
    let receivedSite;
    let deletedSite;
    let receivedSound;
    let receivedPicture;

    hooks.beforeEach(function () {
      site = server.testData.create.feature('5ca78e2160780601008f69e6');
      site.visibility = 1;
      server.testData.storage.addFeature(site);

      const campaign = server.testData.storage.addDefaultCampaign();

      site.source = {
        type: 'Campaign',
        modelId: campaign._id,
        remoteId: '000000111111222222333333',
      };

      server.put(`/mock-server/features/${site._id}`, (request) => {
        receivedSite = JSON.parse(request.requestBody);
        receivedSite.feature._id = site._id;

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedSite)];
      });

      receivedSound = null;
      server.post('/mock-server/sounds', (request) => {
        receivedSound = JSON.parse(request.requestBody);
        receivedSound.sound._id = 'test';

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedSound)];
      });

      receivedPicture = null;
      server.post(`/mock-server/pictures`, (request) => {
        receivedPicture = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            picture: defaultPicture,
          }),
        ];
      });

      server.put(`/mock-server/pictures/:id`, (request) => {
        const receivedPicture = JSON.parse(request.requestBody);
        receivedPicture.picture._id = request.params.id;

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPicture)];
      });

      server.delete('/mock-server/features/' + site._id, () => {
        deletedSite = true;

        return [204, {}, ''];
      });

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
      });

      deletedSite = false;
      receivedSite = null;
    });

    describe('the breadcrumbs', function (hooks) {
      it('should contain all the browse links with the first site map', async function () {
        authenticateSession();
        await visit(`/manage/features/edit/${site._id}`);

        await PageElements.waitEditorJs(this.element);
        await server.waitAllRequests();

        expect(PageElements.breadcrumbs()).to.deep.equal([
          'Dashboard',
          'Open Green Map',
          'Harta Verde București',
          'Nomadisch Grün - Local Urban Food',
        ]);

        expect(PageElements.breadcrumbLinks()).to.deep.equal([
          '/manage/dashboards',
          '/manage/dashboards/5ca78e2160780601008f69e6',
          '/manage/dashboards/map/5ca89e37ef1f7e010007f54c',
        ]);

        await PageElements.wait(100);
      });

      it('should contain all the browse links without the first site map if the site map list is empty', async function () {
        authenticateSession();
        site.maps = [];
        await visit('/manage/features/edit/5ca78e2160780601008f69e6');

        await PageElements.waitEditorJs(this.element);
        await server.waitAllRequests();

        expect(PageElements.breadcrumbs()).to.deep.equal([
          'Dashboard',
          'unknown',
          'unknown',
          'Nomadisch Grün - Local Urban Food',
        ]);
        expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);

        await PageElements.wait(100);
      });

      it('should contain all the browse links with another map if it is set in the query params', async function () {
        authenticateSession();
        await visit(`/manage/features/edit/5ca78e2160780601008f69e6?parentMap=${otherMap._id}`);
        await PageElements.waitEditorJs(this.element);
        await server.waitAllRequests();

        expect(PageElements.breadcrumbs()).to.deep.equal([
          'Dashboard',
          'unknown',
          'the other map',
          'Nomadisch Grün - Local Urban Food',
        ]);
        expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards', '/manage/dashboards/map/2']);

        await PageElements.wait(100);
      });
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/features/edit/5ca78e2160780601008f69e6');

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Ffeatures%2Fedit%2F5ca78e2160780601008f69e6');

      await PageElements.wait(100);
    });

    it('should render the is public label', async function () {
      authenticateSession();

      await visit('/manage/features/edit/5ca78e2160780601008f69e6');

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      expect(this.element.querySelector('.main-container > .error')).not.to.exist;

      expect(this.element.querySelector('.side-bar').textContent).to.contain('visibility');
      expect(this.element.querySelector('.container-group-visibility .value').textContent).to.contain('public');

      await PageElements.wait(100);
    });

    it('should should be able to update the map list', async function () {
      authenticateSession();

      await visit('/manage/features/edit/5ca78e2160780601008f69e6');

      await server.waitAllRequests();

      const listElements = this.element.querySelectorAll('.row-maps ol li');
      expect(listElements.length).to.equal(1);

      await click('.row-maps .btn-edit');
      await click('.row-maps .btn-add-item');
      await click('.row-maps .btn-submit');

      await waitUntil(() => receivedSite);

      expect(receivedSite.feature.maps).to.deep.equal(['5ca89e37ef1f7e010007f54c', '2']);

      await PageElements.wait(100);
    });

    it('can change the name', async function () {
      authenticateSession();
      await visit(`/manage/features/edit/${site._id}`);

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      await click('.row-article .btn-edit');

      await click('.ce-header');
      await wait(200);
      await fillIn('.ce-header', 'new name');
      await wait(200);

      await triggerKeyEvent('.ce-header', 'keyup', 'Enter');
      await wait(200);

      await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));
      await click('.btn-submit');

      await waitUntil(() => receivedSite);
      expect(receivedSite.feature.name).to.equal('new name');

      await PageElements.wait(100);
    });

    it('can change the description', async function () {
      authenticateSession();

      await visit(`/manage/features/edit/${site._id}`);

      await click('.row-article .btn-edit');

      await waitFor('.editor-is-ready', { timeout: 3000 });

      await fillIn('.ce-paragraph', 'new description');
      await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
      await click('.btn-submit');

      await waitUntil(() => receivedSite);

      expect(receivedSite.feature.description.blocks).to.deep.equal([
        { type: 'header', data: { text: site.name, level: 1 } },
        { type: 'paragraph', data: { text: 'new description' } },
      ]);

      await PageElements.wait(100);
    });

    it('can submit the site', async function () {
      authenticateSession();

      await visit('/manage/features/edit/5ca78e2160780601008f69e6');

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      expect(this.element.querySelector('.main-container > .error')).not.to.exist;

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click(this.element.querySelector('.side-bar .container-group-visibility .btn-edit'));

      const select = this.element.querySelector('.container-group-visibility select');

      select.value = '0';
      await triggerEvent(select, 'change');

      await click(this.element.querySelector('.side-bar .container-group-visibility .btn-submit'));

      await waitUntil(() => receivedSite != null, { timeout: 3000 });

      expect(receivedSite.feature.visibility).to.equal(0);

      await PageElements.wait(100);
    });

    it('can add a new sound', async function () {
      authenticateSession();

      await visit('/manage/features/edit/5ca78e2160780601008f69e6');

      await server.waitAllRequests();

      await click('.container-group-sound-gallery .btn-edit');

      await waitFor('.sound-list-input');

      const blob = server.testData.create.mp3Blob();
      await triggerEvent('.sound-list-input', 'change', { files: [blob] });

      await click('.btn-submit');

      await waitUntil(() => receivedSound != null, { timeout: 3000 });
      await waitUntil(() => receivedSite != null, { timeout: 3000 });

      expect(receivedSite.feature.sounds).to.deep.equal(['test']);

      expect(receivedSound.sound).to.deep.equal({
        name: 'coolsound.mp3',
        sound:
          'data:audio/mpeg;base64,DcOBwrHDlcK0woHCicOJwoXCucKRw6TCgcKJwrHDlcKVw4wBUSVQw4gAAAA8AAANBcKxwrDCgcOlwr3DlcOIwoHCscK9w5nClAFRQRTDhAAAADQAAA05wqXCncKhw5HCscK9w43ClcOJw4wBUU1NFAAAADwAAA0xwoXDmcKYw5TDoMK4w4jDpMK4w4TDgMOAAQVBJQwABQXDkAAADcKlwrXChcKdwpTCvcKpw4HClQ==',
        feature: '5ca78e2160780601008f69e6',
        campaignAnswer: null,
        _id: 'test',
      });

      await PageElements.wait(100);
    });

    it('can add a new picture', async function () {
      authenticateSession();

      await visit('/manage/features/edit/5ca78e2160780601008f69e6');

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      await click('.row-picture-list .btn-edit');

      await waitFor('.input-photo-gallery-input');
      const blob = server.testData.create.pngBlob();
      await triggerEvent('.input-photo-gallery-input', 'change', { files: [blob] });

      await click('.btn-submit');

      await waitUntil(() => receivedPicture != null, { timeout: 3000 });
      await waitUntil(() => receivedSite != null, { timeout: 3000 });

      expect(receivedSite.feature.pictures).to.deep.equal(['5cc8dc1038e882010061545a', '2']);

      receivedPicture.picture.picture = '';

      expect(receivedPicture.picture).to.deep.equal({
        name: '',
        picture: '',
        canEdit: false,
        owner: null,
        meta: {
          renderMode: '',
          attributions: '',
          link: { model: 'Feature', modelId: '5ca78e2160780601008f69e6' },
          data: {},
          disableOptimization: false,
        },
      });

      await PageElements.wait(100);
    });

    it('renders the source section', async function () {
      authenticateSession();

      await visit('/manage/features/edit/5ca78e2160780601008f69e6');

      await PageElements.waitEditorJs(this.element);
      await server.waitAllRequests();

      expect(this.element.querySelector('.source-campaign')).to.exist;

      await PageElements.wait(100);
    });

    describe('with a site with a @system picture', function (hooks) {
      let picture;
      let site;

      hooks.beforeEach(function () {
        const pictureId = '00000000000000000000000001';
        picture = server.testData.create.picture(pictureId);
        picture.owner = '@system';

        site = server.testData.create.feature(pictureId);
        site.pictures = [pictureId];

        server.testData.storage.addPicture(picture);
        server.testData.storage.addFeature(site);
      });

      it('should not have the picture rotate button', async function () {
        authenticateSession();

        await visit('/manage/features/edit/00000000000000000000000001');

        await PageElements.waitEditorJs(this.element);
        await server.waitAllRequests();

        expect(this.element.querySelector('.btn-rotate')).not.to.exist;

        await PageElements.wait(100);
      });
    });

    describe('editing the attributes', function (hooks) {
      let siteCustom;

      hooks.beforeEach(function () {
        siteCustom = server.testData.create.feature('5ca78e2160780601008f6000');
        siteCustom.visibility = 1;
        siteCustom['attributes'] = { 'new test group': { key: 'value' } };

        server.testData.storage.addFeature(siteCustom);

        server.put(`/mock-server/features/${siteCustom._id}`, (request) => {
          receivedSite = JSON.parse(request.requestBody);
          receivedSite.feature._id = siteCustom._id;

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedSite)];
        });

        receivedSite = null;
      });

      it('should be able to add a new custom attribute', async function () {
        authenticateSession();
        await visit(`/manage/features/edit/${site._id}`);

        await PageElements.waitEditorJs(this.element);
        await server.waitAllRequests();

        await click('.container-group-attributes .btn-edit');

        await click('.btn-new-anonymous-group');
        await fillIn('.input-group-name', 'new test group');

        await click('.btn-add-new-attribute');

        await fillIn('.input-attribute-name', 'key');
        await fillIn('.input-attribute-value', 'value');
        await click('.btn-submit');

        await waitUntil(() => receivedSite != null, { timeout: 3000 });

        expect(receivedSite).to.deep.equal({
          feature: {
            _id: '5ca78e2160780601008f69e6',
            name: 'Nomadisch Grün - Local Urban Food',
            description: 'some description',
            position: { type: 'Point', coordinates: [13.433576, 52.495781] },
            visibility: 1,
            attributes: {
              'new test group': { key: 'value' },
            },
            info: {
              createdOn: '2009-10-31T00:30:00.000Z',
              author: '5b8a59caef739394031a3f67',
              lastChangeOn: '2009-10-31T00:30:00.000Z',
              originalAuthor: '5b8a59caef739394031a3f67',
              changeIndex: 3,
            },
            maps: ['5ca89e37ef1f7e010007f54c'],
            pictures: ['5cc8dc1038e882010061545a'],
            icons: ['5ca7bfc0ecd8490100cab980'],
            source: {
              type: 'Campaign',
              modelId: '5ca78aa160780601008f6aaa',
              remoteId: '000000111111222222333333',
              syncAt: '1970-01-01T00:00:00.000Z',
            },
          },
        });

        await PageElements.wait(100);
      });

      it('should be able to delete a custom attribute', async function () {
        authenticateSession();
        await visit(`/manage/features/edit/${siteCustom._id}`);

        await PageElements.waitEditorJs(this.element);
        await server.waitAllRequests();

        await click('.container-group-attributes .btn-edit');
        await click('.container-group-attributes .btn-delete');

        await click('.btn-submit');
        await waitUntil(() => receivedSite != null, { timeout: 3000 });

        expect(receivedSite.feature.attributes).to.deep.equal({
          'new test group': {},
        });

        await PageElements.wait(100);
      });

      it('should be able to delete a custom group', async function () {
        authenticateSession();
        await visit(`/manage/features/edit/${siteCustom._id}`);

        await PageElements.waitEditorJs(this.element);
        await server.waitAllRequests();

        await click('.container-group-attributes .btn-edit');
        await click('.btn-delete-group');

        await click('.btn-submit');
        await waitUntil(() => receivedSite != null, { timeout: 3000 });

        expect(receivedSite.feature.attributes).to.deep.equal({});

        await PageElements.wait(100);
      });
    });
  });
});
