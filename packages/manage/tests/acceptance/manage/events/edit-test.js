/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitUntil, triggerEvent, fillIn } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe('Acceptance | manage/events/edit', function (hooks) {
  setupApplicationTest(hooks);

  let event;
  let server;
  let defaultPicture;
  let receivedEvent;
  let receivedPicture;
  let calendar;

  hooks.beforeEach(function () {
    server = new TestServer();
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    const user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultIcon();
    let iconSet = server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultMap();
    calendar = server.testData.storage.addDefaultCalendar();

    calendar.iconSets = {
      useCustomList: false,
      list: [iconSet._id],
    };

    event = server.testData.storage.addDefaultEvent();

    receivedEvent = null;
    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/events/${event._id}`, (request) => {
      receivedEvent = JSON.parse(request.requestBody);
      receivedEvent.event._id = event._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({ event })];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/events/edit/${event._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fevents%2Fedit%2F5cc8dc1038e882010061221');
  });

  it('should contain all the breadcrumbs links', async function () {
    authenticateSession();
    await visit(`/manage/events/edit/${event._id}`);
    await PageElements.waitEditorJs(this.element);

    expect(PageElements.breadcrumbs()).to.deep.equal([
      'Dashboard',
      'Open Green Map',
      'test calendar',
      'test event',
      'Edit',
    ]);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
      '/manage/dashboards/calendar/5cc8dc1038e882010061221',
    ]);
  });

  it('can change the article', async function () {
    authenticateSession();
    await visit(`/manage/events/edit/${event._id}`);

    await click('.row-article .btn-edit');

    await PageElements.waitEditorJs(this.element);

    await fillIn('.ce-paragraph', 'new description');
    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
    await click('.btn-submit');

    await waitUntil(() => receivedEvent, { timeout: 3000 });

    expect(receivedEvent.event.article.blocks).to.deep.equal([
      { type: 'header', data: { text: event.name, level: 1 } },
      { type: 'paragraph', data: { text: 'new description' } },
    ]);
  });

  it('can change the cover', async function () {
    authenticateSession();

    await visit(`/manage/events/edit/${event._id}`);

    await click('.row-cover .btn-edit');

    const blob = server.testData.create.pngBlob();
    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    await waitUntil(() => receivedEvent);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'event', id: '5cc8dc1038e882010061221' },
      data: {},
      disableOptimization: false,
    });

    expect(receivedEvent.event.cover).to.equal('1');

    await server.waitAllRequests();
    await PageElements.wait(1000);
  });

  it('can change the icons', async function () {
    authenticateSession();

    await visit(`/manage/events/edit/${event._id}`);

    await click('.row-icons .btn-edit');

    await click('.icon-container');
    await click('.btn-submit');

    await waitUntil(() => receivedEvent);

    expect(receivedEvent.event.icons).to.deep.equal(['5ca7bfc0ecd8490100cab980']);
    expect(this.element.querySelector('.row-icons .alert')).not.to.exist;
    expect(this.element.querySelector('.icon-container')).to.exist;

    await server.waitAllRequests();
  });

  it('can not change the team', async function () {
    let map = server.testData.create.map('event-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/events/edit/${event._id}`);
    await PageElements.waitEditorJs(this.element);

    expect(this.element.querySelector('.container-group-team .btn-edit')).not.to.exist;
    expect(this.element.querySelector('.container-group-team .value').textContent.trim()).to.equal('Open Green Map');
  });

  it('can change the entries', async function () {
    authenticateSession();

    await visit(`/manage/events/edit/${event._id}`);

    await click('.row-entries .btn-edit');

    this.element.querySelector('.value-begin select.hour').value = '15';
    await triggerEvent('.value-begin select.hour', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedEvent);

    expect(receivedEvent.event.entries).to.deep.equal([
      {
        begin: '2018-01-01T15:30:00.000Z',
        end: '2018-01-01T13:30:10.000Z',
        intervalEnd: '2018-02-01T13:30:10.000Z',
        repetition: 'monthly',
        timezone: 'UTC',
      },
    ]);

    await server.waitAllRequests();
  });

  it('can change the schedule for a scheduled calendar', async function () {
    calendar.type = 'schedules';
    event.entries = [
      {
        begin: '2023-01-03T13:00:00Z',
        end: '2023-01-03T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      },
    ];

    authenticateSession();

    await visit(`/manage/events/edit/${event._id}`);

    await click('.row-schedule .btn-edit');

    await fillIn('.schedule-begin .datepicker-input', '2023-01-14');

    await click('.btn-submit');

    await waitUntil(() => receivedEvent);

    expect(receivedEvent.event.entries).to.deep.equal([
      {
        begin: '2023-01-14T13:00:00.000Z',
        end: '2023-01-14T18:00:00.000Z',
        intervalEnd: '2024-01-02T18:00:00.000Z',
        repetition: 'weekly',
        timezone: 'UTC',
      },
    ]);

    await server.waitAllRequests();
  });

  it('can change the location', async function () {
    authenticateSession();

    await visit(`/manage/events/edit/${event._id}`);

    await click('.row-location .btn-edit');

    await selectSearch('.input-event-location', 'a new text value');
    await selectChoose('.input-event-location', 'a new text value');

    await click('.btn-submit');

    await waitUntil(() => receivedEvent);

    expect(receivedEvent.event.location).to.deep.equal({ type: 'Text', value: 'a new text value' });

    await server.waitAllRequests();
  });

  it('can change the attributes', async function () {
    authenticateSession();

    await visit(`/manage/events/edit/${event._id}`);

    await click('.row-attributes .btn-edit');
    await fillIn('.attribute-value .ce-paragraph', 'new description');

    await PageElements.wait(600);

    await click('.btn-submit');

    await waitUntil(() => receivedEvent);

    expect(receivedEvent.event.attributes).to.deep.equal({
      key1: { blocks: [{ type: 'paragraph', data: { text: 'new description' } }] },
    });

    await server.waitAllRequests();
  });

  describe('when the calendar does not have icons and attributes', function (hooks) {
    hooks.beforeEach(function () {
      calendar.iconSets = {
        useCustomList: false,
        list: [],
      };

      calendar.attributes = [];
    });

    it('does not render the icons section', async function () {
      authenticateSession();

      await visit(`/manage/events/edit/${event._id}`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.row-icons')).not.to.exist;
    });

    it('does not render the attributes section', async function () {
      authenticateSession();

      await visit(`/manage/events/edit/${event._id}`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.container-group-attributes')).not.to.exist;
    });
  });
});
