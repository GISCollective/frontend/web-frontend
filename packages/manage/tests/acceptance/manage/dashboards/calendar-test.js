/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, fillIn, waitUntil } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/calendar', function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let team;
  let calendar;
  let event;

  hooks.beforeEach(function () {
    server = new TestServer();
    team = server.testData.create.team();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultIcon();

    server.testData.storage.addDefaultCalendar();
    calendar = server.testData.storage.addDefaultCalendar('editable-calendar');
    calendar.name = 'Editable calendar';
    calendar.canEdit = true;

    event = server.testData.storage.addDefaultEvent();
    event.canEdit = true;

    const notifications = this.owner.lookup('service:notifications');
    notifications.ask = () => 'yes';

    const service = this.owner.lookup('service:modal');
    service.confirm = () => 'yes';
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('redirects to the login page when the user is not authenticated', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2Fcalendar%2Feditable-calendar');
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', calendar.name]);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    ]);
  });

  it('renders a custom plus button label when there is a custom record name', async function () {
    authenticateSession();

    calendar.recordName = 'volunteering opportunity';

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    expect(this.element.querySelector('.btn-add-event').textContent.trim()).to.equal('Volunteering opportunity');
  });

  it('displays an event as a card', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    expect(this.element.querySelectorAll('.card-dashboard-event').length).to.equal(1);
    expect(this.element.querySelector('.card-dashboard-event .title').textContent.trim()).to.equal(event.name);
  });

  it('opens the edit event page when a card is selected', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    await click('.card-dashboard-event .title');

    expect(currentURL()).to.equal('/manage/dashboards/calendar/editable-calendar');
  });

  it('opens the edit calendar page when the edit button is pressed', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    await click('.btn-calendar-edit');

    expect(currentURL()).to.equal('/manage/calendars/edit/editable-calendar');
  });

  it('adds an event to the current calendar', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    await click('.btn-add-event');

    expect(currentURL()).to.equal('/manage/events/add?calendar=editable-calendar');
    expect(this.element.querySelector('.row-calendar select')).not.to.exist;
  });

  it('can delete an event using the dropdown', async function () {
    const service = this.owner.lookup('service:notifications');
    service.ask = () => 'yes';

    authenticateSession();
    let deletedEvent = false;
    server.delete(`/mock-server/events/:id`, () => {
      deletedEvent = true;

      return [204, {}, ''];
    });

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    await click('.card-dashboard-event .dropdown-toggle');
    await click('.card-dashboard-event .btn-delete');

    expect(deletedEvent).to.eql(true);
    expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
  });

  it('can delete an event by selecting it', async function () {
    authenticateSession();
    let deletedEvent = false;
    server.delete(`/mock-server/events/:id`, () => {
      deletedEvent = true;
      server.testData.storage.events = {};

      return [204, {}, ''];
    });

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    await click('.card-dashboard-event .chk-selected');
    await click('.actions-selected .btn-delete');

    expect(deletedEvent).to.eql(true);
    expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
  });

  it('can publish an event by selecting it', async function () {
    authenticateSession();

    let receivedData;

    server.put(`/mock-server/events/:id`, (request) => {
      receivedData = JSON.parse(request.requestBody);
      receivedData.event._id = request.params.id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedData)];
    });

    event.visibility.isPublic = false;

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    await click('.card-dashboard-event .chk-selected');

    await click('.actions-selected .btn-publish');

    await waitUntil(() => receivedData);

    expect(receivedData.event.visibility).to.deep.equal({
      isPublic: true,
      isDefault: false,
      team: '5ca78e2160780601008f69e6',
    });
  });

  it('can unpublish an event by selecting it', async function () {
    authenticateSession();

    let receivedData;

    server.put(`/mock-server/events/:id`, (request) => {
      receivedData = JSON.parse(request.requestBody);
      receivedData.event._id = request.params.id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedData)];
    });

    event.visibility.isPublic = true;

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    await click('.card-dashboard-event .chk-selected');

    await click('.actions-selected .btn-unpublish');

    await waitUntil(() => receivedData);

    expect(receivedData.event.visibility).to.deep.equal({
      isPublic: false,
      isDefault: false,
      team: '5ca78e2160780601008f69e6',
    });
  });

  it('can filter events by day', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/calendar/${calendar._id}`);

    server.history = [];
    await fillIn('.datepicker-input', '2022-03-02');
    await click('.manage-container');

    expect(currentURL()).to.equal('/manage/dashboards/calendar/editable-calendar?day=2022-03-02');
    expect(server.history).to.contain(
      'GET /mock-server/events?after=2022-03-02T00%3A00%3A00.000%2B01%3A00&calendar=editable-calendar&until=2022-03-02T23%3A59%3A59.999%2B01%3A00',
    );
  });

  describe('when there are 2 events', function (hooks) {
    let otherEvent;

    hooks.beforeEach(function () {
      otherEvent = server.testData.storage.addDefaultEvent('22');
      otherEvent.canEdit = true;
      otherEvent.visibility.isPublic = false;
    });

    it('can delete all events', async function () {
      authenticateSession();
      let queryParams;
      server.delete(`/mock-server/events`, (req) => {
        queryParams = req.queryParams;
        server.testData.storage.events = {};

        return [204, {}, ''];
      });

      await visit(`/manage/dashboards/calendar/${calendar._id}`);

      await click('.chk-select-all');
      await click('.actions-selected .btn-delete');

      expect(queryParams).to.eql({ calendar: 'editable-calendar', team: '5ca78e2160780601008f69e6' });

      expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
    });

    it('can publish all events', async function () {
      authenticateSession();
      let queryParams;
      server.post(`/mock-server/events/publish`, (req) => {
        queryParams = req.queryParams;
        server.testData.storage.events = {};

        return [204, {}, ''];
      });

      await visit(`/manage/dashboards/calendar/${calendar._id}`);

      await click('.chk-select-all');
      await click('.navbar .btn-publish');

      expect(queryParams).to.eql({ calendar: 'editable-calendar', team: '5ca78e2160780601008f69e6' });

      expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
    });

    it('can unpublish all events', async function () {
      authenticateSession();
      let queryParams;
      server.post(`/mock-server/events/unpublish`, (req) => {
        queryParams = req.queryParams;
        server.testData.storage.events = {};

        return [204, {}, ''];
      });

      await visit(`/manage/dashboards/calendar/${calendar._id}`);

      await click('.chk-select-all');
      await click('.navbar .btn-unpublish');

      expect(queryParams).to.eql({ calendar: 'editable-calendar', team: '5ca78e2160780601008f69e6' });

      expect(this.element.querySelectorAll('.card-dashboard-event')).to.have.length(0);
    });
  });
});
