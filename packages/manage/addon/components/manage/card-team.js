import Component from './card';
import { service } from '@ember/service';

export default class ManageCardTeamComponent extends Component {
  @service intl;

  get firstParagraph() {
    if (this.args.record?.firstParagraph) {
      return this.args.record.firstParagraph;
    }

    return this.intl.t('message-no-team-description');
  }
}
