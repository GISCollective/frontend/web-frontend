/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BrowseRoute from './browse';

export default class AuthenticatedBrowseRoute extends BrowseRoute {
  beforeModel() {
    if (!this.session.isAuthenticated) {
      return this.router.transitionTo('login.index');
    }
  }
}
