/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class BasePageColListComponent extends Component {
  @service store;

  get buckets() {
    const key = this.args.value.modelKey;

    if (!this.args.model) {
      return null;
    }

    return this.args.model[key];
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }
}
