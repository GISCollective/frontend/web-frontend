/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class EditorMapComponent extends Component {
  config = [
    {
      name: "mode",
      options: ["show all features", "use a map"],
    },
    {
      name: "flyOverAnimation",
      type: "bool",
    },
    {
      name: "featureHover",
      options: ["do nothing", "mouse pointer"],
    },
    {
      name: "featureSelection",
      options: ["do nothing", "open details in panel", "show popup", "go to page"],
    },
    {
      name: "enableMapInteraction",
      options: ["no", "always", "automatic"],
    },
    {
      name: "enablePinning",
      type: "bool",
    },
    {
      name: "restrictView",
      type: "bool",
    },
    {
      name: "showUserLocation",
      type: "bool",
    },
    {
      name: "showZoomButtons",
      type: "bool",
    },
    {
      name: "showBaseMapSelection",
      type: "bool",
    },
    {
      name: "showTitle",
      type: "bool",
    },
    {
      name: "enableFullMapView",
      type: "bool",
    },
  ];
}
