/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageBaseMapsEditRoute extends AuthenticatedRoute {
  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params) {
    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    const baseMap = await this.store.findRecord('base-map', params.id);
    const team = await this.store.findRecord('team', baseMap.visibility.teamId);

    return hash({
      maps: this.store.findAll('map'),
      teams: this.store.query('team', teamsParams),
      team,
      baseMap,
    });
  }
}
