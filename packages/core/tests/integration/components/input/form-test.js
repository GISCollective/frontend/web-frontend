/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe('Integration | Component | input/form', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no config', async function () {
    await render(hbs`<Input::Form />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  describe('select fields', function () {
    it('renders a select option when the config has an option list', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          options: ['option1', 'option2'],
        },
      ]);

      await render(hbs`<Input::Form @config={{this.config}} />`);

      const options = [...this.element.querySelectorAll('.some-config option')].map(
        (a) => a.attributes.getNamedItem('value')?.value ?? '',
      );

      expect(this.element.querySelector('.some-config').value).to.equal('option1');
      expect(options).to.deep.equal(['option1', 'option2']);
    });

    it('renders the field value when set', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          options: ['option1', 'option2'],
        },
      ]);

      this.set('value', {
        someConfig: 'option2',
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector('.some-config').value).to.equal('option2');
    });

    it('updates the values when the config is updated', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          options: ['option1', 'option2', 'option3'],
        },
      ]);

      this.set('value', {
        someConfig: 'option2',
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} />`);

      this.set('config', [
        {
          name: 'someConfig',
          options: ['option1', 'option2', 'option3'],
        },
        {
          name: 'otherConfig',
          options: ['option4', 'option5', 'option6'],
        },
      ]);

      this.set('value', {
        someConfig: 'option3',
      });

      expect(this.element.querySelector('.some-config').value).to.equal('option3');
    });

    it('triggers onChange when the select value is updated', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          options: ['option1', 'option2'],
        },
      ]);

      this.set('value', {
        someConfig: 'option2',
      });

      let value;

      this.set('change', (v) => {
        value = v;
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`);

      this.element.querySelector('.some-config').value = 'option1';
      await triggerEvent('.some-config', 'change');

      expect(value).to.deep.equal({ someConfig: 'option1' });
    });
  });

  describe('boolean fields', function () {
    it('renders a boolean option when the config has a boolean type', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'bool',
        },
      ]);

      await render(hbs`<Input::Form @config={{this.config}} />`);

      const options = [...this.element.querySelectorAll('.some-config option')].map(
        (a) => a.attributes.getNamedItem('value')?.value ?? '',
      );

      expect(this.element.querySelector('.some-config').value).to.equal('no');
      expect(options).to.deep.equal(['no', 'yes']);
    });

    it('renders the field value when set', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'bool',
        },
      ]);

      this.set('value', {
        someConfig: true,
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector('.some-config').value).to.equal('yes');
    });

    it('triggers onChange when the select value is updated', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'bool',
        },
      ]);

      this.set('value', {
        someConfig: false,
      });

      let value;

      this.set('change', (v) => {
        value = v;
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`);

      this.element.querySelector('.some-config').value = 'yes';
      await triggerEvent('.some-config', 'change');

      expect(value).to.deep.equal({ someConfig: true });
    });
  });

  describe('number fields', function () {
    it('renders a number input when the config has a number type', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'number',
        },
      ]);

      await render(hbs`<Input::Form @config={{this.config}} />`);

      expect(this.element.querySelector('.some-config').value).to.equal('');
      expect(this.element.querySelector('.some-config')).to.have.attribute('type', 'number');
    });

    it('renders the field value when set', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'number',
        },
      ]);

      this.set('value', {
        someConfig: 23,
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector('.some-config').value).to.equal('23');
    });

    it('triggers onChange when the input value is updated', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'number',
        },
      ]);

      this.set('value', {
        someConfig: 12,
      });

      let value;

      this.set('change', (v) => {
        value = v;
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`);

      await fillIn('.some-config', '22');

      expect(value).to.deep.equal({ someConfig: 22 });
    });
  });

  describe('email fields', function () {
    it('renders a email input when the config has a email type', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'email',
        },
      ]);

      await render(hbs`<Input::Form @config={{this.config}} />`);

      expect(this.element.querySelector('.some-config').value).to.equal('');
      expect(this.element.querySelector('.some-config')).to.have.attribute('type', 'email');
    });

    it('renders the field value when set', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'email',
        },
      ]);

      this.set('value', {
        someConfig: 'a@gmail.com',
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector('.some-config').value).to.equal('a@gmail.com');
    });

    it('triggers onChange when the input value is updated', async function () {
      this.set('config', [
        {
          name: 'someConfig',
          type: 'email',
        },
      ]);

      this.set('value', {
        someConfig: 'a@gmail.com',
      });

      let value;

      this.set('change', (v) => {
        value = v;
      });

      await render(hbs`<Input::Form @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`);

      await fillIn('.some-config', 'b@gmail.com');

      expect(value).to.deep.equal({ someConfig: 'b@gmail.com' });
    });
  });
});
