/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class PageColFeatureCardComponent extends Component {
  @service store;

  get feature() {
    return this.args.value;
  }

  get options() {
    return this.args.options ?? {};
  }
}
