/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class ConnectionTransform extends Transform {
  deserialize(serialized) {
    return new Connection(serialized);
  }

  serialize(deserialized) {
    if (deserialized?.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class Connection {
  @tracked type = '';
  @tracked config = {};

  constructor(value) {
    if (value?.type) {
      this.type = value.type;
    }

    if (value?.config) {
      this.config = value.config;
    }
  }

  toJSON() {
    return {
      type: this.type ?? '',
      config: this.config ?? {},
    };
  }
}
