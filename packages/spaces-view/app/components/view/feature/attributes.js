/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class PageColFeatureAttributesComponent extends Component {
  @tracked feature;

  get showAll() {
    return this.args.value?.data?.attributes?.showAll;
  }

  get attributes() {
    let attributes =
      this.feature?.displayIconAttributes?.filter?.((a) => a) ?? [];
    const other = attributes.find((a) => a.displayName == 'other');

    if (other) {
      attributes = [
        other,
        ...attributes.filter((a) => a.displayName != 'other'),
      ];

      other.displayName = null;
    }

    return attributes;
  }

  @action
  loaded(record) {
    this.feature = record;
  }
}
