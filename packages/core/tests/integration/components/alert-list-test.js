/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import click from '@ember/test-helpers/dom/click';
import AlertMessage from 'dummy/lib/alert-message';

describe('Integration | Component | alert-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the list is not set', async function () {
    await render(hbs`<AlertList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders an alert', async function () {
    this.set('list', [new AlertMessage('first message. second message', 'danger', 'id')]);

    await render(hbs`<AlertList @list={{this.list}} />`);

    expect(this.element.querySelector('.alert.alert-danger.id').textContent.trim()).to.equal(
      'first message. second message',
    );

    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders an alert with fix link', async function () {
    let called;

    this.set('list', [
      new AlertMessage('first message. second message', 'danger', 'id', () => {
        called = true;
      }),
    ]);

    await render(hbs`<AlertList @list={{this.list}} />`);

    const text = this.element
      .querySelector('.alert.alert-danger.id')
      .textContent.split('\n')
      .map((a) => a.trim())
      .join('');

    expect(text).to.equal('first message.second message');

    expect(this.element.querySelector('button').textContent.trim()).to.equal('second message');

    await click('button');

    expect(called).to.be.true;
  });
});
