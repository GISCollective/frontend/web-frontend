import { it, describe, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | view/content-picker', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::ContentPicker as |value|>
      {{value.blocks.length}}
    </View::ContentPicker>`);

    expect(this.element).to.have.textContent('0');
  });

  it('renders nothing when there is a value with no blocks', async function (assert) {
    this.set('value', { blocks: [] });

    await render(hbs`<View::ContentPicker @value={{this.value}} as |value|>
      {{value.blocks.length}}
    </View::ContentPicker>`);

    expect(this.element).to.have.textContent('0');
  });

  it('renders the original content when the mode is all', async function (assert) {
    this.set('value', { blocks: [{}, {}] });
    this.set('options', { mode: 'all' });

    await render(hbs`<View::ContentPicker @value={{this.value}} @options={{this.options}} as |value|>
      {{value.blocks.length}}
    </View::ContentPicker>`);

    expect(this.element).to.have.textContent('2');
  });

  it('renders the first paragraph when mode is paragraph and index 0', async function (assert) {
    this.set('value', {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: 'text 1',
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'text 2',
          },
        },
      ],
    });
    this.set('options', { mode: 'paragraph', paragraphIndex: '0' });

    await render(hbs`<View::ContentPicker @value={{this.value}} @options={{this.options}} as |value|>
      <span class="len">{{value.blocks.length}}</span>
      <span class="text">{{get value.blocks "0.data.text"}}</span>
    </View::ContentPicker>`);

    expect(this.element.querySelector('.len')).to.have.textContent('1');
    expect(this.element.querySelector('.text')).to.have.textContent('text 1');
  });
});
