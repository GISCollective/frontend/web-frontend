/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import PictureMeta from 'models/lib/picture-meta';

export default class ManageCampaignsEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service store;
  @service router;

  @tracked editableField;
  @tracked savingField;
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        model: this.model.campaign.get('id'),
        text: this.model.campaign.name,
        route: 'manage.dashboards.survey',
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  @action
  saveFeatureNamePrefix(value) {
    if (!this.model.campaign.options) {
      this.model.campaign.options = {};
    }

    this.model.campaign.options.featureNamePrefix = value;
    return this.save();
  }

  @action
  saveRegistrationMandatory(value) {
    const options = this.model.campaign.options ?? {};
    options.registrationMandatory = value;

    return this.saveQuestions(options);
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });
  }

  @action
  async saveMap(newValue) {
    this.model.campaign.set('map', newValue);

    await this.save();
    await this.model.reloadIconSets();
    await this.model.reloadIcons();
  }

  @action
  async saveField(field, newValue) {
    this.model.campaign.set(field, newValue);

    return this.save();
  }

  @action
  saveCover(value) {
    this.model.campaign.cover = value;
    return this.save();
  }

  @action
  saveInterval(newStartDate, newEndDate) {
    this.model.campaign.startDate = newStartDate;
    this.model.campaign.endDate = newEndDate;
    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.campaign.visibility.team = newValue;
    this.editableField = this.intl.t('team');

    return this.save();
  }

  @action
  saveIsPublic(newValue) {
    this.model.campaign.visibility.isPublic = newValue;
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.campaign.visibility.isDefault = newValue;
    this.editableField = this.intl.t('is default');

    return this.save();
  }

  @action
  saveArticle(name, article) {
    this.model.campaign.name = name;
    this.model.campaign.article = article;

    return this.save();
  }

  @action
  async saveNotifications(areEnabled) {
    this.editableField = '';

    if (areEnabled) {
      await this.model.campaign.removeNotifications();
    } else {
      await this.model.campaign.addNotifications();
    }

    await this.model.campaign.reload();
  }

  @action
  async save() {
    this.editableField = '';

    await this.model.campaign.save();
  }

  @action
  async delete() {
    try {
      await this.model.campaign.destroyRecord();
      this.router.transitionTo('campaigns');
    } catch (err) {
      this.notifications.handleError(err);
    }
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    this._parentIcon = null;
    return this.model.campaign.rollbackAttributes();
  }
}
