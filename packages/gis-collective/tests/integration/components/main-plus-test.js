/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | main-plus', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let user;

  hooks.beforeEach(function () {
    user = this.owner.lookup('service:user');
  });

  it('renders all models for an admin', async function (a) {
    user.userData = { isAdmin: true };

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    a.deepEqual(models, [
      'Feature',
      'Map',
      'Team',
      'Survey',
      'Icon set',
      'Icon',
      'Base map',
      'Newsletter',
      'Data binding',
      'Calendar',
      'Event',
      'Article',
      'Page',
      'Space',
      'Presentation',
      'Language',
    ]);
  });

  it('does not render admin models for regular users', async function (a) {
    user.userData = { isAdmin: false };

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    a.deepEqual(models, [
      'Feature',
      'Map',
      'Team',
      'Survey',
      'Icon set',
      'Icon',
      'Base map',
      'Article',
      'Page',
      'Space',
    ]);
  });

  it('shows the newsletter option when the user has a team that allows newsletters', async function () {
    user.userData = { isAdmin: false };
    user.teams = [{ allowNewsletters: true }];

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    expect(models).to.contain('Newsletter');
  });

  it('hides the newsletter option when the user has a team that denies newsletters', async function () {
    user.userData = { isAdmin: false };
    user.teams = [{ allowNewsletters: false }];

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    expect(models).not.to.contain('Newsletter');
  });

  it('shows the data binding option when the user has a team that allows data bindings', async function () {
    user.userData = { isAdmin: false };
    user.teams = [{ allowCustomDataBindings: true }];

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    expect(models).to.contain('Data binding');
  });

  it('hides the data binding option when the user has a team that denies data bindings', async function () {
    user.userData = { isAdmin: false };
    user.teams = [{ allowCustomDataBindings: false }];

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    expect(models).not.to.contain('Data binding');
  });

  it('shows the calendar option when the user has a team that allows events', async function () {
    user.userData = { isAdmin: false };
    user.teams = [{ allowEvents: true }];

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    expect(models).to.contain('Calendar');
  });

  it('hides the calendar option when the user has a team that denies data bindings', async function () {
    user.userData = { isAdmin: false };
    user.teams = [{ allowEvents: false }];

    await render(hbs`<MainPlus />`);

    const models = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    expect(models).not.to.contain('Calendar');
  });
});
