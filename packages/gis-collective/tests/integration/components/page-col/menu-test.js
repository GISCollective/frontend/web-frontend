/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, waitFor, waitUntil, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";
import { authenticateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import Service from '@ember/service';

class MockSpace extends Service {
  currentLocation = 'http://localhost:4200/events/cop26#container-4';
}

describe ('Integration | Component | view/menu', function (hooks) {
  setupRenderingTest(hooks);
  let pageService;
  let windowService;
  let searchService;
  let server;
  let store;

  hooks.beforeEach(async function () {
    server = new TestServer();

    this.owner.register('service:space', MockSpace);

    pageService = this.owner.lookup('service:page');
    searchService = this.owner.lookup('service:search');
    windowService = this.owner.lookup('service:window');
    windowService.scrollX = 0;
    windowService.scrollY = 0;

    let spaceData = server.testData.storage.addDefaultSpace();
    spaceData.landingPageId = '61292c4c7bdf9301008fd7be';
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultProfile();
    server.testData.storage.addDefaultPicture();

    server.testData.storage.addDefaultPage('61292c4c7bdf9301008fd7be', {
      slug: 'page--test1',
    });
    server.testData.storage.addDefaultPage('61292c4c7bdf9301008fd7bf', {
      slug: 'page--test2',
    });
    server.testData.storage.addDefaultPage('61292c4c7bdf9301008fd7bg', {
      slug: 'page--test3',
    });

    store = this.owner.lookup('service:store');
    const space = await store.findRecord('space', spaceData._id);
    await space.getPagesMap();

    this.set('space', space);
  });

  it('renders nothing when there is no data', async function () {
    await render(hbs`<View::Menu />`);

    const links = this.element.querySelectorAll('a');

    expect(links).to.have.length(0);
  });

  it('renders the space buttons', async function () {
    this.set('value', {
      data: {
        primaryBackground: 'red',
        primaryText: 'yellow',
        menu: {
          items: [
            {
              link: {
                pageId: '61292c4c7bdf9301008fd7be',
              },
              name: 'About',
            },
            {
              link: {
                pageId: '61292c4c7bdf9301008fd7bf',
              },
              name: 'Browse',
            },
            {
              link: {
                pageId: '61292c4c7bdf9301008fd7bg',
              },
              name: 'Propose a site',
            },
          ],
        },
      },
    });

    await render(hbs`<View::Menu @value={{this.value}} @space={{this.space}} />`);

    expect(this.element.querySelector('.loading')).to.have.class('invisible');

    const links = this.element.querySelectorAll('a');

    expect(links).to.have.length(3);
    expect(links[0].textContent.trim()).to.equal('About');
    expect(links[0]).to.have.attribute('href', '/page/test1');
    expect(links[1].textContent.trim()).to.equal('Browse');
    expect(links[1]).to.have.attribute('href', '/page/test2');
    expect(links[2].textContent.trim()).to.equal('Propose a site');
    expect(links[2]).to.have.attribute('href', '/page/test3');
  });

  it('renders the external link', async function () {
    this.set('value', {
      data: {
        primaryBackground: 'red',
        primaryText: 'yellow',
        menu: {
          items: [
            {
              link: {
                url: 'https://giscollective.com',
              },
              name: 'About',
            },
            {
              link: {
                pageId: '61292c4c7bdf9301008fd7bf',
              },
              name: 'Browse',
            },
          ],
        },
      },
    });

    await render(hbs`<View::Menu @value={{this.value}} />`);

    const links = this.element.querySelectorAll('a');

    expect(links).to.have.length(2);
    expect(links[0].textContent.trim()).to.equal('About');
    expect(links[0]).to.have.attribute('href', 'https://giscollective.com');
  });

  it('renders the loading state while loading', async function () {
    const loading = this.owner.lookup('service:loading');

    loading.isLoading = true;

    this.set('value', {
      data: {
        menu: {
          items: [],
        },
      },
    });

    render(hbs`<View::Menu @value={{this.value}} />`);

    await waitFor('.loading');

    expect(this.element.querySelector('.loading')).to.have.class('visible');

    loading.isLoading = false;

    await waitUntil(() => !this.element.querySelector('.loading.visible'))
    expect(this.element.querySelector('.loading.visible')).not.to.exist;
  });

  describe ('when there is a menu with a submenu',  function (hooks) {
    hooks.beforeEach(async function () {
      this.set('value', {
        data: {
          primaryBackground: 'red',
          primaryText: 'yellow',
          menu: {
            items: [
              {
                name: 'name',
                link: { url: 'https://giscollective.com' },
                dropDown: [
                  {
                    name: 'other name',
                    link: { url: 'https://giscollective.com' },
                  },
                ],
              },
            ],
          },
        },
      });
    });

    it('does not render a link for the first level', async function () {
      await render(hbs`<View::Menu @value={{this.value}} />`);

      const links = this.element.querySelectorAll('.nav-item-level-1 a');

      expect(links).to.have.length(1);
      expect(this.element.querySelector('.nav-item-level-1 .btn-link').textContent.trim()).to.equal('name');
    });

    it('renders the dropdown menu', async function () {
      await render(hbs`<View::Menu @value={{this.value}} />`);

      const links = this.element.querySelectorAll('.dropdown-menu-list a');

      expect(links).to.have.length(1);
      expect(links[0].textContent.trim()).to.equal('other name');
      expect(links[0]).to.have.attribute('href', 'https://giscollective.com');
    });
  });

  describe ('when the menu has no logo or login',  function (hooks) {
    hooks.beforeEach(async function () {
      this.set('value', {
        data: {
          showLogo: false,
          showLogin: false,
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);
    });

    it('does not render the logo', async function () {
      const logo = this.element.querySelector('.logo');
      expect(logo).not.to.exist;
    });

    it('does not render the login button', async function () {
      const loginButton = this.element.querySelector('.btn-log-in');
      expect(loginButton).not.to.exist;

      const userDropDown = this.element.querySelector('.user-drop-down');
      expect(userDropDown).not.to.exist;
    });

    it('hides the user dropdown when the user is authenticated', async function () {
      authenticateSession();

      await waitUntil(() => !this.element.querySelector('.btn-log-in'));

      const userDropDown = this.element.querySelector('.user-drop-down');
      expect(userDropDown).not.to.exist;
    });
  });

  describe ('when the menu has a logo and a login',  function (hooks) {
    hooks.beforeEach(async function () {
      this.set('value', {
        data: {
          showLogo: true,
          showLogin: true,
        },
      });

      await render(hbs`<View::Menu @space={{this.space}} @value={{this.value}} />`);
    });

    it('renders the logo', async function () {
      await waitFor('img');

      const logo = this.element.querySelector('.logo');
      expect(logo).to.have.attribute('alt', 'GISCollective');
      expect(logo).to.have.attribute('src', '/test/5d5aa72acac72c010043fb59.jpg/xs');
    });

    it('renders the login button', async function () {
      const loginButton = this.element.querySelector('.btn-log-in');
      expect(loginButton).to.have.attribute('href', '/login');

      const userDropDown = this.element.querySelector('.user-drop-down');
      expect(userDropDown).not.to.exist;
    });

    it('renders the user dropdown when the user is authenticated', async function () {
      authenticateSession();

      await waitUntil(() => !this.element.querySelector('.btn-log-in'));

      const userDropDown = this.element.querySelector('.user-drop-down');
      expect(userDropDown).to.exist;
    });
  });

  describe ('when the menu has search enabled',  function (hooks) {
    hooks.beforeEach(async function () {
      this.set('value', {
        data: {
          showSearch: true,
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);
    });

    it('renders the search button', async function () {
      expect(this.element.querySelector('.btn-search')).to.exist;
    });

    it('enables the search service when the search button is pressed', async function () {
      await click('.btn-search');

      expect(searchService.isEnabled).to.equal(true);
    });
  });

  describe ('the shadow options',  function (hooks) {
    it('can set the medium option', async function () {
      this.set('value', {
        data: {
          shadow: 'medium',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.navbar-container')).to.have.class('shadow');
    });

    it('can set the small option', async function () {
      this.set('value', {
        data: {
          shadow: 'small',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.navbar-container')).to.have.class('shadow-sm');
    });

    it('can set the large option', async function () {
      this.set('value', {
        data: {
          shadow: 'large',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.navbar-container')).to.have.class('shadow-lg');
    });

    it('uses none as default', async function () {
      this.set('value', {});

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.navbar-container')).to.have.class('shadow-none');
    });
  });

  describe ('the alignment options',  function (hooks) {
    it('can set the left option', async function () {
      this.set('value', {
        data: {
          align: 'left',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.large-menu-buttons')).to.have.class('me-auto');
      expect(this.element.querySelector('.large-menu-buttons')).not.to.have.class('ms-auto');
    });

    it('can set the right option', async function () {
      this.set('value', {
        data: {
          align: 'right',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.large-menu-buttons')).not.to.have.class('me-auto');
      expect(this.element.querySelector('.large-menu-buttons')).to.have.class('ms-auto');
    });

    it('can set the center option', async function () {
      this.set('value', {
        data: {
          align: 'center',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.large-menu-buttons')).to.have.class('me-auto');
      expect(this.element.querySelector('.large-menu-buttons')).to.have.class('ms-auto');
    });
  });

  describe ('the width options',  function (hooks) {
    it('sets the full-size class when the width is full', async function () {
      this.set('value', {
        data: {
          width: 'full',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.page-col-menu')).to.have.class('full-size');
    });

    it('does not set the full-size class when the width is not set', async function () {
      this.set('value', {
        data: {},
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.page-col-menu')).not.to.have.class('full-size');
    });
  });

  describe ('the border options',  function (hooks) {
    it('sets the primary border color', async function () {
      this.set('value', {
        data: {
          primaryBorder: 'red',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.navbar-container')).to.have.class('border-red');
    });
  });

  describe ('a sticky menu',  function (hooks) {
    it('renders a placeholder of the menu size when the menu is scrolled over top', async function () {
      this.set('value', {
        data: {
          position: 'stick',
        },
      });

      await render(
        hbs`<div style="position: fixed; top: -99999999999px">
  <View::Menu @value={{this.value}} />
</div>`
      );

      windowService.scrollY = this.element.querySelector('.page-col-menu').getBoundingClientRect().top * -1;

      const height = this.element.querySelector('.page-col-menu').offsetHeight;

      await waitFor('.page-col-menu-placeholder');

      expect(this.element.querySelector('.page-col-menu-placeholder')).to.have.attribute('style', `height: ${height}px`);

      expect(this.element.querySelector('.page-col-menu')).to.have.attribute('style', `height: ${height}px`);

      expect(this.element.querySelector('.page-col-menu')).to.have.class('sticky');
    });

    it('sticks to top only when the scrollPosition > menu position', async function () {
      this.set('value', {
        data: {
          position: 'stick',
        },
      });

      await render(hbs`
        <div style="height: 6000px">
          <div style="padding-top: 100px">
          </div>

          <View::Menu @value={{this.value}} />
        </div>`);

      expect(this.element.querySelector('.page-col-menu')).not.to.have.class('sticky');
    });

    it('does not stick in edit mode', async function () {
      this.set('value', {
        data: {
          position: 'stick',
        },
      });

      await render(
        hbs`<div style="position: fixed; top: -99999999999px">
  <View::Menu @value={{this.value}} @isEditor={{true}} />
</div>`
      );

      windowService.scrollY = this.element.querySelector('.page-col-menu').getBoundingClientRect().top * -1;

      await PageElements.wait(100);

      expect(this.element.querySelector('.page-col-menu')).not.to.have.class('sticky');
    });
  });

  describe ('an auto menu',  function (hooks) {
    it('renders a placeholder of the menu size when the menu is scrolled over top', async function () {
      this.set('value', {
        data: {
          position: 'auto',
        },
      });

      await render(
        hbs`<div style="position: fixed; top: -99999999999px">
  <View::Menu @value={{this.value}} />
</div>`
      );

      windowService.scrollY = this.element.querySelector('.page-col-menu').getBoundingClientRect().top * -1 - 100;

      const height = this.element.querySelector('.page-col-menu').offsetHeight;

      await waitFor('.page-col-menu-placeholder');

      expect(this.element.querySelector('.page-col-menu-placeholder')).to.have.attribute('style', `height: ${height}px`);

      expect(this.element.querySelector('.page-col-menu')).to.have.attribute('style', `height: ${height}px`);

      expect(this.element.querySelector('.page-col-menu')).to.have.class('auto');
    });

    it('sticks to top only when the scrollPosition > menu position', async function () {
      this.set('value', {
        data: {
          position: 'stick',
        },
      });

      await render(hbs`
        <div style="height: 6000px">
          <div style="padding-top: 100px">
          </div>

          <View::Menu @value={{this.value}} />
        </div>`);

      expect(this.element.querySelector('.page-col-menu')).not.to.have.class('auto');
    });
  });

  describe ('the hamburger button',  function (hooks) {
    it('is not visible by default', async function () {
      this.set('value', {});

      await render(hbs`<View::Menu @value={{this.value}} />`);

      expect(this.element.querySelector('.btn-hamburger')).not.to.exist;
    });

    it('is visible when the size matches the one from the value', async function () {
      this.set('value', {
        data: {
          hamburgerFrom: 'tablet',
        },
      });

      await render(hbs`<View::Menu @value={{this.value}} @deviceSize="tablet" />`);

      expect(this.element.querySelector('.btn-hamburger')).to.exist;
      expect(this.element.querySelector('.large-menu-buttons')).not.to.exist;
    });
  });

  it('sets the primary background color and text color when they are set', async function () {
    this.set('value', {
      data: {
        primaryBackground: 'red',
        primaryText: 'yellow',
        menu: {
          items: [
            {
              link: {
                pageId: '61292c4c7bdf9301008fd7be',
              },
              name: 'About',
            },
          ],
        },
      },
    });

    await render(hbs`<View::Menu @value={{this.value}} />`);

    expect(this.element.querySelector('.navbar-container')).to.have.class('bg-red');
    expect(this.element.querySelector('.page-col-menu ul a')).to.have.class('text-yellow');
  });

  it('is visible when the size is smaller than the one from the value', async function () {
    this.set('value', {
      data: {
        hamburgerFrom: 'tablet',
      },
    });

    await render(hbs`<View::Menu @value={{this.value}} @deviceSize="mobile" />`);

    expect(this.element.querySelector('.btn-hamburger')).to.exist;
    expect(this.element.querySelector('.large-menu-buttons')).not.to.exist;
  });

  it('is not visible when the size is larger than the one from the value', async function () {
    this.set('value', {
      data: {
        hamburgerFrom: 'tablet',
      },
    });

    await render(hbs`<View::Menu @value={{this.value}} @deviceSize="desktop" />`);

    expect(this.element.querySelector('.btn-hamburger')).not.to.exist;
    expect(this.element.querySelector('.navbar-buttons')).to.exist;
  });

  it('opens the menu when the hamburger button is pressed', async function () {
    this.set('value', {
      data: {
        hamburgerFrom: 'tablet',
      },
    });

    await render(hbs`<View::Menu @value={{this.value}} @deviceSize="mobile" />`);

    await click('.btn-hamburger');

    expect(this.element.querySelector('.btn-hamburger')).not.to.exist;
    expect(this.element.querySelector('.navbar-buttons')).to.exist;
    expect(this.element.querySelector('.btn-hamburger-close')).to.exist;
  });

  it('sets the style to the selected main menu page', async function () {
    this.set('value', {
      data: {
        selectedBackground: 'red',
        selectedText: 'yellow',
        menu: {
          items: [
            {
              link: {
                pageId: '61292c4c7bdf9301008fd7be',
              },
              name: 'About',
            },
          ],
        },
      },
    });

    this.set('model', {
      page: {
        id: '61292c4c7bdf9301008fd7be',
      },
    });

    await render(hbs`<View::Menu @model={{this.model}} @value={{this.value}} />`);

    expect(this.element.querySelector('.menu-item a')).to.have.class('bg-red');
    expect(this.element.querySelector('.menu-item a')).to.have.class('text-yellow');
  });

  it('sets the style to the selected dropdown menu page', async function () {
    this.set('value', {
      data: {
        selectedBackground: 'red',
        selectedText: 'yellow',
        menu: {
          items: [
            {
              name: 'name',
              dropDown: [
                {
                  name: 'other name',
                  link: {
                    pageId: '61292c4c7bdf9301008fd7be',
                  },
                },
              ],
            },
          ],
        },
      },
    });

    this.set('model', {
      page: {
        id: '61292c4c7bdf9301008fd7be',
      },
    });

    await render(hbs`<View::Menu @model={{this.model}} @value={{this.value}} />`);

    expect(this.element.querySelector('button.btn-link')).to.have.class('bg-red');
    expect(this.element.querySelector('button.btn-link')).to.have.class('text-yellow');

    expect(this.element.querySelector('.menu-item a')).to.have.class('bg-red');
    expect(this.element.querySelector('.menu-item a')).to.have.class('text-yellow');
  });

  it('sets the style to the selected dropdown menu page url', async function () {
    this.set('value', {
      data: {
        selectedBackground: 'red',
        selectedText: 'yellow',
        menu: {
          items: [
            {
              name: 'name',
              dropDown: [
                {
                  name: 'other name',
                  link: {
                    url: '/events/cop26#container-4',
                  },
                },
              ],
            },
          ],
        },
      },
    });

    this.set('model', {
      page: {
        id: '61292c4c7bdf9301008fd7be',
      },
    });

    await render(hbs`<View::Menu @model={{this.model}} @value={{this.value}} />`);

    expect(this.element.querySelector('button.btn-link')).to.have.class('bg-red');
    expect(this.element.querySelector('button.btn-link')).to.have.class('text-yellow');

    expect(this.element.querySelector('.menu-item a')).to.have.class('bg-red');
    expect(this.element.querySelector('.menu-item a')).to.have.class('text-yellow');
  });
});
