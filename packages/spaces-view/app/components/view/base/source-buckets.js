/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action, get } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { toModelSlug } from 'core/lib/slug';
import { fetch, fetchSourceList } from 'models/lib/fetch-source';
import { service } from '@ember/service';
import LazyList from 'models/lib/lazy-list';
import { PageCol } from 'models/transforms/page-col-list';

export default class PageColBaseSourceBucketsComponent extends Component {
  @service store;
  @service space;
  @service fastboot;

  @tracked _selectedGroup;
  @tracked _lazyList;
  @tracked _record;

  get categoryValue() {
    return {
      ...(this.args.value?.data?.categories ?? {}),
      categoryList: this.groupNames,
    };
  }

  get categoryState() {
    return `cn@${this.selectedGroup}`;
  }

  get selectedGroup() {
    if (this._selectedGroup) {
      return this._selectedGroup;
    }

    return this.args.value?.data?.source?.groups?.[0]?.name ?? '';
  }

  get modelName() {
    return this.args.value?.data?.source?.model ?? '';
  }

  get modelKey() {
    if (this._selectedGroup) {
      return `${this.modelName}_${toModelSlug(this._selectedGroup)}`;
    }

    return this.args.value?.modelKey ?? '';
  }

  async ensureModelRecord() {
    this._record = await this.args.model?.[this.modelKey];
    this.args.model[this.modelKey] = value;
  }

  get record() {
    if (this._record) {
      return this._record;
    }

    const value = this.args.model?.[this.modelKey];

    if (value?.then) {
      this.ensureModelRecord();
      return {};
    }

    return value;
  }

  get lazyList() {
    let result;

    if (this._lazyList) {
      result = this._lazyList;
    }

    if (Array.isArray(this.record)) {
      result = new LazyList([this.record]);
    }

    if (!this._lazyList && this.record && !Array.isArray(this.record)) {
      result = this.record;
      const property = this.args.value?.data?.source?.property;
      const useSelectedModel = this.args.value?.data?.source?.useSelectedModel;

      if (this.record.buckets && !result.model) {
        result = new LazyList(this.record.buckets);
      }

      if (useSelectedModel && property) {
        const items = get(this.record, property);
        result = new LazyList(items);
      }
    }

    if (!result?.buckets && result?.content) {
      result.buckets = new LazyList(result.content);
    }

    if (this.args.maxItems && result?.loadedAny) {
      result = result.limitTo(this.args.maxItems);
    }

    return result;
  }

  get groups() {
    return this.args.value?.data?.source?.groups ?? [];
  }

  get groupNames() {
    return this.groups.map((a) => a.name);
  }

  get canLoadMore() {
    if (!this.buckets?.loadedAny) {
      return false;
    }

    const len = this.buckets.columns.length;

    if (!len) {
      return false;
    }

    const lastColumn = this.buckets.columns[len - 1];

    return lastColumn.length == this.buckets.perPage;
  }

  @action
  setup() {
    this._lazyList = undefined;
    this._record = undefined;
  }

  @action
  async selectGroup(name) {
    this._selectedGroup = name;

    if (!this.args.model?.[this.modelKey]) {
      const group = this.groups.find((a) => a.name == name);
      this.args.model[this.modelKey] = await fetchSourceList(
        this.modelName,
        group,
        this.args.model,
        this.args.model.space,
        this.store,
      );
    }

    this._lazyList = this.args.model[this.modelKey];
  }

  @action
  loadMore() {
    return this.lazyList.loadNext();
  }

  @action
  async updateProps(event) {
    let value = new PageCol(this.args.value);
    value.data.source = event.detail.value;

    let data;

    if (
      value.modelKey &&
      !(value.modelKey == 'selectedModel' && !value.property)
    ) {
      data = await fetch(
        this.args.model ?? {},
        value,
        this.args.model.space,
        this.store,
      );
      this.args.model[value.modelKey] = data;
    }

    this._lazyList = null;
    this._record = data;
  }
}
