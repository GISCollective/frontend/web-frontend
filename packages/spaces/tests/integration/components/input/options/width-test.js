/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | input/options/width", function (hooks) {
  setupRenderingTest(hooks);

  describe("without a device size", function () {
    describe("using the col type", function () {
      it("renders the right option", async function () {
        this.set("col", { classes: ["col-4"] });
        await render(hbs`<Input::Options::Width @value={{this.col}}/>`);

        expect(this.element.querySelector(".input-width-col").value).to.equal("4");
      });

      it("renders the default value when a value for a different device size is set", async function () {
        this.set("col", { options: ["col-md-4"] });
        await render(hbs`<Input::Options::Width @value={{this.col}}/>`);

        expect(this.element.querySelector(".input-width-col").value).to.equal("default");
      });

      it("can change the width", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { classes: ["col-4"] });

        await render(hbs`<Input::Options::Width @value={{this.col}} @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector(".input-width-col").value).to.equal("4");

        this.element.querySelector(".input-width-col").value = "6";
        await triggerEvent(".input-width-col", "change");

        expect(value).to.deep.equal({
          classes: ["col-6"],
          style: { sm: {} },
        });
      });

      it("can change the type", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { classes: ["col-4"] });

        await render(hbs`<Input::Options::Width @value={{this.col}} @onChangeOptions={{this.change}}/>`);

        this.element.querySelector(".input-width-type").value = "px";
        await triggerEvent(".input-width-type", "change");

        expect(this.element.querySelector(".input-width-col")).not.to.exist;
        expect(this.element.querySelector(".input-px-size")).to.exist;

        await fillIn(".input-px-size", "123");

        expect(value).to.deep.equal({
          classes: [],
          style: {
            sm: {
              width: "123px",
            },
          },
        });
      });
    });

    describe("using the px type", function () {
      it("renders the right px option", async function () {
        this.set("col", { style: { sm: { width: "43px" } } });
        await render(hbs`<Input::Options::Width @value={{this.col}}/>`);

        expect(this.element.querySelector(".input-px-size").value).to.equal("43");
      });

      it("renders the default value when a value for a different device size is set", async function () {
        this.set("col", { style: { md: { width: "43px" } } });
        await render(hbs`<Input::Options::Width @value={{this.col}}/>`);

        expect(this.element.querySelector(".input-width-col").value).to.equal("default");
      });

      it("can change the type", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { style: { sm: { width: "43px" } } });

        await render(hbs`<Input::Options::Width @value={{this.col}} @onChangeOptions={{this.change}}/>`);

        this.element.querySelector(".input-width-type").value = "cols";
        await triggerEvent(".input-width-type", "change");

        expect(this.element.querySelector(".input-width-col")).to.exist;
        expect(this.element.querySelector(".input-px-size")).not.to.exist;

        this.element.querySelector(".input-width-col").value = "6";
        await triggerEvent(".input-width-col", "change");

        expect(value).to.deep.equal({ style: { sm: { width: "" } }, classes: ["col-6"] });
      });
    });
  });

  describe("with a md device size", function () {
    describe("using the col type", function () {
      it("renders the right col option", async function () {
        this.set("col", { classes: ["col-md-4"] });
        await render(hbs`<Input::Options::Width @deviceSize="md" @value={{this.col}}/>`);

        expect(this.element.querySelector(".input-px-size")).not.to.exist;
        expect(this.element.querySelector(".input-width-col").value).to.equal("4");
      });

      it("can change the width", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { classes: ["col-md-4"] });

        await render(
          hbs`<Input::Options::Width @deviceSize="md" @value={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        expect(this.element.querySelector(".input-width-col").value).to.equal("4");

        this.element.querySelector(".input-width-col").value = "6";
        await triggerEvent(".input-width-col", "change");

        expect(value).to.deep.equal({ style: { md: {} }, classes: ["col-md-6"] });
      });

      it("can change the type", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { classes: ["col-md-4"] });

        await render(
          hbs`<Input::Options::Width @deviceSize="md" @value={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        this.element.querySelector(".input-width-type").value = "px";
        await triggerEvent(".input-width-type", "change");

        expect(this.element.querySelector(".input-width-col")).not.to.exist;
        expect(this.element.querySelector(".input-px-size")).to.exist;

        await fillIn(".input-px-size", "123");

        expect(value).to.deep.equal({ classes: [], style: { md: { width: "123px" } } });
      });
    });

    describe("using the px type", function () {
      it("renders the right px option", async function () {
        this.set("col", { style: { md: { width: "43px" } } });
        await render(hbs`<Input::Options::Width @deviceSize="md" @value={{this.col}}/>`);

        expect(this.element.querySelector(".input-px-size").value).to.equal("43");
      });

      it("renders the default value when a value for a different device size is set", async function () {
        this.set("col", { classes: ["wpx-4"] });
        await render(hbs`<Input::Options::Width @deviceSize="md" @value={{this.col}}/>`);

        expect(this.element.querySelector(".input-width-col").value).to.equal("default");
      });

      it("can change the type", async function (a) {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { style: { md: { width: "4px" } } });

        await render(
          hbs`<Input::Options::Width @deviceSize="md" @value={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        this.element.querySelector(".input-width-type").value = "cols";
        await triggerEvent(".input-width-type", "change");

        expect(this.element.querySelector(".input-width-col")).to.exist;
        expect(this.element.querySelector(".input-px-size")).not.to.exist;

        this.element.querySelector(".input-width-col").value = "6";
        await triggerEvent(".input-width-col", "change");

        a.deepEqual(value, {
          classes: ["col-md-6"],
          style: {
            md: {
              width: "",
            },
          },
        });
      });

      it("can set the default value", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { style: { md: { width: "4px" } } });

        await render(
          hbs`<Input::Options::Width @deviceSize="md" @value={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        this.element.querySelector(".input-width-type").value = "cols";
        await triggerEvent(".input-width-type", "change");

        expect(value).to.deep.equal({ classes: [], style: { md: { width: "" } } });
      });
    });
  });
});
