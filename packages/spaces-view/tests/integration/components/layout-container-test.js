/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | layout-container', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a blank container by default', async function () {
    await render(hbs`<LayoutContainer>test</LayoutContainer>`);

    expect(
      this.element.querySelector('.background-empty').textContent.trim(),
    ).to.equal('test');
  });

  it('renders the height class when it is "fill"', async function () {
    this.set('container', {
      height: 'fill',
    });
    await render(
      hbs`<LayoutContainer @container={{this.container}}>test</LayoutContainer>`,
    );

    expect(this.element.querySelector('.background-empty')).to.have.class(
      'height-fill',
    );
  });

  it('renders a background color container when set', async function () {
    this.set('container', {
      name: '0',
    });

    this.set('cols', [
      {
        name: '0',
        type: 'background/color',
        data: { background: { color: 'blue' } },
      },
    ]);

    await render(
      hbs`<LayoutContainer @cols={{this.cols}} @container={{this.container}}>test</LayoutContainer>`,
    );

    const layers = [...this.element.querySelectorAll('.layer')];
    expect(layers.length).to.equal(0);

    expect(
      this.element.querySelector('.container-bg-color').textContent.trim(),
    ).to.equal('test');
    expect(this.element.querySelector('.container-bg-color')).to.have.class(
      'bg-blue',
    );
  });

  it('renders two background color layers when set', async function () {
    this.set('container', {
      name: '0',
      layers: {
        count: 2,
        showContentAfter: 2,
      },
    });

    this.set('cols', [
      {
        name: '0',
        type: 'background/color',
        data: { background: { color: 'blue' } },
      },
      {
        name: '0.layer-1',
        type: 'background/color',
        data: { background: { color: 'red' } },
      },
    ]);

    await render(
      hbs`<LayoutContainer @cols={{this.cols}} @container={{this.container}}>test</LayoutContainer>`,
    );

    const layers = [...this.element.querySelectorAll('.layer')];

    expect(layers.length).to.equal(2);
    expect(layers[0].textContent.trim()).to.equal('');
    expect(layers[1].textContent.trim()).to.equal('');

    expect(
      this.element.querySelector('.background-empty').textContent.trim(),
    ).to.equal('test');
  });

  it('renders layers with the same height as the content', async function () {
    this.set('container', {
      name: '0',
      layers: {
        count: 2,
        showContentAfter: 2,
      },
    });

    this.set('cols', [
      {
        name: '0',
        type: 'background/color',
        data: { background: { color: 'blue' } },
      },
      {
        name: '0.layer-1',
        type: 'background/color',
        data: { background: { color: 'red' } },
      },
    ]);

    await render(
      hbs`<LayoutContainer @cols={{this.cols}} @container={{this.container}}>test</LayoutContainer>`,
    );

    const layers = [...this.element.querySelectorAll('.layer')];
    const contentClientRect = this.element
      .querySelector('.background-empty')
      .getBoundingClientRect();

    const contentHeight = contentClientRect.height / 2;

    const heights = layers
      .map((a) => a.getBoundingClientRect())
      .map((a) => a.height);

    expect(heights).to.deep.equal([contentHeight, contentHeight]);
  });

  it('renders layers with the same top coordinate as the content', async function () {
    this.set('container', {
      name: '0',
      layers: {
        count: 2,
        showContentAfter: 2,
      },
    });

    this.set('cols', [
      {
        name: '0',
        type: 'background/color',
        data: { background: { color: 'blue' } },
      },
      {
        name: '0.layer-1',
        type: 'background/color',
        data: { background: { color: 'red' } },
      },
    ]);

    await render(
      hbs`<LayoutContainer @cols={{this.cols}} @container={{this.container}}>test</LayoutContainer>`,
    );

    const layers = [...this.element.querySelectorAll('.layer')];
    const contentClientRect = this.element
      .querySelector('.background-empty')
      .getBoundingClientRect();

    const contentTop = contentClientRect.y;

    const tops = layers.map((a) => a.getBoundingClientRect()).map((a) => a.y);

    expect(tops).to.deep.equal([contentTop, contentTop]);
  });
});
