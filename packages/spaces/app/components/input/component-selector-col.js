/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";

export default class InputComponentSelectorComponent extends Component {
  @service pageCols;

  get value() {
    return this.args.gid || this.args.value;
  }

  get globalComponents() {
    return this.args.space?.cols?.idList?.map((a) => this.pageCols.toButton(a)) ?? [];
  }

  get groups() {
    const result = this.pageCols.groups;

    if (this.globalComponents?.length > 0) {
      result.push({
        groupName: "global",
        options: this.globalComponents,
      });
    }

    return result;
  }
}
