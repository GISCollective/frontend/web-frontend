/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest, changeSelect } from 'dummy/tests/helpers';
import { render, click, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a deactivated switch and no values when there is no default value set', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} />
    `);

    expect(this.element.querySelector('.select-use-default-values').value).to.equal('0');
    const inputs = [...this.element.querySelectorAll('.text-value')].map((a) => a.value);

    expect(inputs).to.deep.equal([]);
  });

  it('shows the default values when set', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    this.set('defaultValues', [
      { id: 3, name: 'name3' },
      { id: 4, name: 'name4' },
    ]);
    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @defaultValues={{this.defaultValues}} />
    `);

    expect(this.element.querySelector('.select-use-default-values').value).to.equal('0');
    const inputs = [...this.element.querySelectorAll('.text-value')].map((a) => a.value);

    expect(inputs).to.deep.equal(['name3', 'name4']);
  });

  it('keeps the values while the is enabled is toggled', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    this.set('defaultValues', [
      { id: 3, name: 'name3' },
      { id: 4, name: 'name4' },
    ]);
    this.set('onChange', (b, c) => {
      this.set('values', c);
    });
    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @onChange={{this.onChange}} @defaultValues={{this.defaultValues}} />
    `);
    await changeSelect('.select-use-default-values', '1');

    let inputs = [...this.element.querySelectorAll('.select-value')].map((a) => a.value);
    expect(inputs).to.deep.equal(['1', '2']);

    await changeSelect('.select-use-default-values', '0');
    await changeSelect('.select-use-default-values', '1');

    inputs = [...this.element.querySelectorAll('.select-value')].map((a) => a.value);
    expect(inputs).to.deep.equal(['1', '2']);
  });

  it('renders the list when the switch is activated', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} />
    `);

    await changeSelect('.select-use-default-values', '1');

    const selects = [...this.element.querySelectorAll('.form-select')].map((a) => a.value);
    expect(selects).to.deep.equal(['1', '1', '2']);
  });

  it('triggers the save with an empty list and in disabled state', async function () {
    let values, isEnabled;

    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    this.set('onChange', function (b, c) {
      isEnabled = b;
      values = c;
    });

    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{true}} @values={{this.values}} @onChange={{this.onChange}}/>
    `);
    await changeSelect('.select-use-default-values', '0');

    expect(values).to.deep.equal([]);
    expect(isEnabled).to.equal(false);
  });

  it('triggers the onChange with the list and an enabled state when the switch is toggled', async function () {
    let values, isEnabled;

    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);

    this.set('onChange', function (b, c) {
      isEnabled = b;
      values = c;
    });

    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @onChange={{this.onChange}}/>
    `);

    await changeSelect('.select-use-default-values', '1');

    expect(values).to.deep.equal(this.values);
    expect(isEnabled).to.equal(true);
  });

  it('renders a list of select input sets to the values', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.values}} @values={{this.values}} />
    `);

    const elements = this.element.querySelectorAll('.select-value');
    expect(elements.length).to.equal(2);
    expect(elements[0].value.trim()).to.equal('1');
    expect(elements[1].value.trim()).to.equal('2');
  });

  it('can remove all options and saves them', async function () {
    let values;

    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.values}} @values={{this.values}} @onChange={{this.save}} />
    `);

    await click('.btn-delete');
    await click('.btn-delete');

    const elements = this.element.querySelectorAll('.select-value');
    expect(elements.length).to.equal(0);

    expect(values).to.deep.equal([]);
  });

  it('can change a value', async function () {
    let values;

    this.set('values', [{ id: '1', name: 'name1' }]);
    this.set('list', [
      { id: '1', name: 'name1' },
      { id: '2', name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @onChange={{this.save}} />
    `);

    await changeSelect('.select-value', '2');

    expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
  });

  it('can add a value', async function () {
    let values;

    this.set('values', []);
    this.set('list', [
      { id: '1', name: 'name1' },
      { id: '2', name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onChange={{this.save}} @editablePanel="title" />
    `);

    await click('.btn-add-item');

    const elements = this.element.querySelectorAll('.select-value');
    expect(elements.length).to.equal(1);

    expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
  });

  it('ignores an invalid value', async function () {
    let values;

    this.set('values', []);
    this.set('list', [
      { id: '1', name: 'name1' },
      { id: '2', name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onChange={{this.save}} @editablePanel="title" />
    `);

    await click('.btn-add-item');
    await waitFor('.select-value');

    await changeSelect('.select-value', '');

    expect(values).to.deep.equal([]);
  });
});
