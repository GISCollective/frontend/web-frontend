/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action, set } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageNewslettersEditController extends Controller {
  @service notifications;
  @service fastboot;
  @service intl;
  @service user;
  @service router;
  @service preferences;

  @tracked allTeams = false;
  @tracked savingField;
  @tracked editableField;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team.name,
        capitalize: true,
        model: this.model.team.id,
      },
      {
        route: 'manage.dashboards.newsletter',
        model: this.model.newsletter.id,
        text: this.model.newsletter.name,
      },
      {
        text: this.intl.t('edit-label'),
        capitalize: true,
      },
    ];
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.newsletter.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      },
    );
  }

  @action
  saveField(name, value) {
    set(this.model.newsletter, name, value);

    return this.save();
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  saveIsPublic(newValue) {
    this.model.newsletter.visibility.isPublic = newValue;
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.newsletter.visibility.team = newValue;
    this.editableField = this.intl.t('team');

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.newsletter.visibility.isDefault = newValue;
    this.editableField = this.intl.t('is default');

    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.newsletter.rollbackAttributes();
  }
}
