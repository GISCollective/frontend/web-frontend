/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageDashboardsDashboardController extends Controller {
  @service router;
  @service user;

  get hasIncompleteProfile() {
    const profile = this.user.profileData;

    if (!profile) {
      return false;
    }

    let picture = profile?.picture;

    if (profile?.get) {
      picture = profile.get('picture.id');
    }

    return !picture || !profile.firstName?.trim?.() || !profile.lastName?.trim?.() || !profile?.bio?.trim?.();
  }

  @action
  browseTeam(id) {
    this.router.transitionTo('manage.dashboards.dashboard', id);
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }
}
