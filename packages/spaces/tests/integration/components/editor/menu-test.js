/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | editor/menu", function (hooks) {
  setupRenderingTest(hooks);
  let changedValue;
  let server;
  let space;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage("000000000000000000000002");

    space = server.testData.storage.addDefaultSpace();
    space.getPagesMap = () => {
      return {
        page1: "000000000000000000000001",
        page2: "000000000000000000000002",
        page3: "000000000000000000000003",
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("space", space);
    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  describe("the options section", function () {
    it("checks all button options when they are set", async function () {
      this.set("value", {
        data: {
          showLogo: true,
          showLogin: true,
          showSearch: true,
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} />`);

      expect(this.element.querySelector(".input-menu-show-logo").checked).to.equal(true);

      expect(this.element.querySelector(".input-menu-show-login").checked).to.equal(true);
    });

    it("can change the logo visibility", async function () {
      this.set("value", {
        data: {
          showLogo: true,
          showLogin: true,
          showSearch: true,
          logo: { style: {} },
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-show-logo");

      expect(changedValue).to.deep.equal({
        showLogo: false,
        showLogin: true,
        showSearch: true,
        logo: { style: {} },
      });
    });

    it("can change the login visibility", async function () {
      this.set("value", {
        data: {
          showLogo: true,
          showLogin: true,
          showSearch: true,
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-show-login");

      expect(changedValue).to.deep.equal({
        showLogo: true,
        showLogin: false,
        showSearch: true,
        logo: { style: {} },
      });
    });

    it("can change the search visibility", async function () {
      this.set("value", {
        data: {
          showLogo: true,
          showLogin: true,
          showSearch: true,
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-show-search");

      expect(changedValue).to.deep.equal({
        showLogo: true,
        showLogin: true,
        showSearch: false,
        logo: { style: {} },
      });
    });

    it("can change the manage visibility", async function () {
      this.set("value", {
        data: {
          showLogo: true,
          showLogin: true,
          showSearch: true,
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-show-login");

      expect(changedValue).to.deep.equal({
        showLogo: true,
        showLogin: false,
        showSearch: true,
        logo: { style: {} },
      });
    });

    it("can create a menu", async function (a) {
      this.set("value", {
        data: {
          showLogo: true,
          showLogin: true,
          showSearch: true,
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".btn-add-menu-item");
      await fillIn(".link-name", "menu-1");

      const selectLinks = this.element.querySelectorAll(".ember-basic-dropdown-trigger");

      await selectSearch(selectLinks[0], "page1");
      await selectChoose(selectLinks[0], "page1");

      this.element.querySelector(".open-in-new-tab").value = "yes";
      await triggerEvent(".open-in-new-tab", "change");

      changedValue.menu = changedValue.menu.toJSON();

      a.deepEqual(changedValue, {
        showLogo: true,
        showLogin: true,
        showSearch: true,
        menu: {
          items: [
            {
              name: "menu-1",
              newTab: true,
              link: { pageId: "000000000000000000000001" },
              dropDown: [],
            },
          ],
        },
        logo: { style: {} },
      });
    });
  });

  it("renders a menu when the values are set", async function () {
    this.set("value", {
      data: {
        showLogo: true,
        showLogin: true,
        menu: {
          items: [{ name: "menu-1", link: { pageId: "/page1" }, dropDown: [] }],
        },
      },
    });

    await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".manage-editors-properties-link-named .link-name").value).to.equal("menu-1");

    expect(
      this.element
        .querySelector(".manage-editors-properties-link-named .ember-power-select-selected-item")
        .textContent.trim(),
    ).to.equal("page1");
  });

  describe("the functionality section", function () {
    it("checks all functionality options when they are set", async function () {
      this.set("value", {
        data: {
          position: "stick",
          hamburgerFrom: "tablet",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} />`);

      const position = this.element.querySelector(".input-menu-position");
      expect(position.options[position.selectedIndex].value).to.equal("stick");

      const hamburgerFrom = this.element.querySelector(".input-menu-hamburger-from");
      expect(hamburgerFrom.options[hamburgerFrom.selectedIndex].value).to.equal("tablet");
    });

    it("can change the position value", async function () {
      this.set("value", {
        data: {
          position: "stick",
          hamburgerFrom: "tablet",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-menu-position").value = "fixed";
      await triggerEvent(".input-menu-position", "change");

      expect(changedValue).to.deep.equal({
        position: "fixed",
        hamburgerFrom: "tablet",
        logo: { style: {} },
      });
    });

    it("can change the hamburgerFrom value", async function () {
      this.set("value", {
        data: {
          position: "stick",
          hamburgerFrom: "tablet",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-menu-hamburger-from").value = "mobile";
      await triggerEvent(".input-menu-hamburger-from", "change");

      expect(changedValue).to.deep.equal({
        position: "stick",
        hamburgerFrom: "mobile",
        logo: { style: {} },
      });
    });
  });

  describe("the style section", function () {
    it("checks all style options when they are set", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} />`);

      const align = this.element.querySelector(".input-menu-align");
      const shadow = this.element.querySelector(".input-menu-shadow");
      const width = this.element.querySelector(".input-menu-width");

      expect(align.options[align.selectedIndex].value).to.equal("center");
      expect(shadow.options[shadow.selectedIndex].value).to.equal("medium");
      expect(width.options[width.selectedIndex].value).to.equal("default");
      expect(this.element.querySelector(".input-menu-primary-background .selected-color").textContent.trim()).to.equal(
        "red",
      );
      expect(this.element.querySelector(".input-menu-primary-text .selected-color").textContent.trim()).to.equal(
        "blue",
      );
      expect(this.element.querySelector(".input-menu-primary-border .selected-color").textContent.trim()).to.equal(
        "green",
      );
      expect(
        this.element.querySelector(".input-menu-secondary-background .selected-color").textContent.trim(),
      ).to.equal("white");
      expect(this.element.querySelector(".input-menu-secondary-text .selected-color").textContent.trim()).to.equal(
        "red",
      );
      expect(this.element.querySelector(".input-menu-secondary-border .selected-color").textContent.trim()).to.equal(
        "green",
      );
    });

    it("can change the align", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-menu-align").value = "right";
      await triggerEvent(".input-menu-align", "change");

      expect(changedValue).to.deep.equal({
        align: "right",
        shadow: "medium",
        width: "default",
        primaryBorder: "green",
        secondaryBorder: "green",
        primaryBackground: "red",
        primaryText: "blue",
        secondaryBackground: "white",
        secondaryText: "red",
        logo: { style: {} },
      });
    });

    it("can change the shadow", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-menu-shadow").value = "none";
      await triggerEvent(".input-menu-shadow", "change");

      expect(changedValue).to.deep.equal({
        align: "center",
        shadow: "none",
        width: "default",
        primaryBackground: "red",
        primaryText: "blue",
        primaryBorder: "green",
        secondaryBackground: "white",
        secondaryText: "red",
        secondaryBorder: "green",
        logo: { style: {} },
      });
    });

    it("can change the width", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-menu-width").value = "full";
      await triggerEvent(".input-menu-width", "change");

      expect(changedValue).to.deep.equal({
        align: "center",
        shadow: "medium",
        width: "full",
        primaryBackground: "red",
        primaryText: "blue",
        primaryBorder: "green",
        secondaryBackground: "white",
        secondaryText: "red",
        secondaryBorder: "green",
        logo: { style: {} },
      });
    });

    it("can change primary background color", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-primary-background .btn-color-yellow");

      expect(changedValue).to.deep.equal({
        align: "center",
        shadow: "medium",
        width: "default",
        primaryBorder: "green",
        secondaryBorder: "green",
        primaryBackground: "yellow",
        primaryText: "blue",
        secondaryBackground: "white",
        secondaryText: "red",
        logo: { style: {} },
      });
    });

    it("can change primary text color", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-primary-text .btn-color-yellow");

      expect(changedValue).to.deep.equal({
        align: "center",
        shadow: "medium",
        width: "default",
        primaryBorder: "green",
        secondaryBorder: "green",
        primaryBackground: "red",
        primaryText: "yellow",
        secondaryBackground: "white",
        secondaryText: "red",
        logo: { style: {} },
      });
    });

    it("can change secondary background color", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-secondary-background .btn-color-yellow");

      expect(changedValue).to.deep.equal({
        align: "center",
        shadow: "medium",
        width: "default",
        primaryBorder: "green",
        secondaryBorder: "green",
        primaryBackground: "red",
        primaryText: "blue",
        secondaryBackground: "yellow",
        secondaryText: "red",
        logo: { style: {} },
      });
    });

    it("can change secondary text color", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-secondary-text .btn-color-yellow");

      expect(changedValue).to.deep.equal({
        align: "center",
        shadow: "medium",
        width: "default",
        primaryBorder: "green",
        secondaryBorder: "green",
        primaryBackground: "red",
        primaryText: "blue",
        secondaryBackground: "white",
        secondaryText: "yellow",
        logo: { style: {} },
      });
    });

    it("can change selected background color", async function () {
      this.set("value", {
        data: {
          align: "center",
          shadow: "medium",
          width: "default",
          primaryBackground: "red",
          primaryText: "blue",
          primaryBorder: "green",
          secondaryBackground: "white",
          secondaryText: "red",
          secondaryBorder: "green",
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      await click(".input-menu-selected-background .btn-color-yellow");

      expect(changedValue).to.deep.equal({
        align: "center",
        shadow: "medium",
        width: "default",
        primaryBackground: "red",
        primaryText: "blue",
        primaryBorder: "green",
        secondaryBackground: "white",
        secondaryText: "red",
        secondaryBorder: "green",
        selectedBackground: "yellow",
        logo: { style: {} },
      });
    });
  });

  it("can change selected text color", async function () {
    await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".input-menu-selected-text .btn-color-yellow");

    expect(changedValue).to.deep.equal({
      selectedText: "yellow",
      logo: { style: {} },
    });
  });

  describe("the logo section", function () {
    it("allows setting a custom height", async function (a) {
      this.set("value", {
        logo: {
          style: {
            lg: { height: "3px" },
            md: { height: "2px" },
            sm: { height: "1px" },
          },
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      const elements = this.element.querySelectorAll(".option-panel-logo .input-px-size");

      expect(elements[0].value).to.equal("1");

      await fillIn(elements[0], "12");

      expect(changedValue.logo).to.deep.equal({
        style: { lg: { height: "3px" }, md: { height: "2px" }, sm: { height: "12px" } },
      });
    });

    it("allows setting a custom height (legacy)", async function (a) {
      this.set("value", {
        data: {
          logo: {
            style: {
              lg: { height: "3px" },
              md: { height: "2px" },
              sm: { height: "1px" },
            },
          },
        },
      });

      await render(hbs`<Editor::Menu @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

      const elements = this.element.querySelectorAll(".option-panel-logo .input-px-size");

      expect(elements[0].value).to.equal("1");

      await fillIn(elements[0], "12");

      expect(changedValue.logo).to.deep.equal({
        style: { lg: { height: "3px" }, md: { height: "2px" }, sm: { height: "12px" } },
      });
    });
  });
});
