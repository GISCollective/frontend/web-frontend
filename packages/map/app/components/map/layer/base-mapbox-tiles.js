/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import mapboxgl, { Map } from 'mapbox-gl';
import * as Sentry from '@sentry/browser';
import { transform } from 'ol/proj';
import { debounce, later } from '@ember/runloop';

export default class MapLayerBaseMapBoxTilesComponent extends Component {
  @tracked mapBoxInstance;

  change() {
    if (!this.mapBoxInstance) {
      return;
    }

    const view = this._map.getView();
    const zoom = view.getZoom();
    const rotation = view.getRotation();

    const projection = view.getProjection();
    const center = transform(view.getCenter(), projection, 'EPSG:4326');

    if (!center || isNaN(center[0]) || isNaN(center[1])) {
      return;
    }

    this.mapBoxInstance.jumpTo({
      center: { lon: center[0], lat: center[1] },
      zoom: zoom - 1,
      bearing: (-rotation * 180) / Math.PI,
      animate: false,
    });
  }

  setupMapBox(container) {
    try {
      mapboxgl.accessToken = this._layer?.options?.token;
      this.mapBoxInstance = new Map({
        container,
        style: this._layer?.options?.style,
        attributionControl: false,
        boxZoom: false,
        doubleClickZoom: false,
        dragPan: false,
        dragRotate: false,
        interactive: false,
        keyboard: false,
        pitchWithRotate: false,
        scrollZoom: false,
        touchZoomRotate: false,
      });

      const view = this.args.map.getView();
      this._change = () => this.change(...arguments);

      view.on('change', this._change);
      this.args.map.on('precompose', this._change);
      this._change();

      this.resizeObserver = new ResizeObserver(() => {
        window?.requestAnimationFrame(() => {
          debounce(this.mapBoxInstance, this.mapBoxInstance.resize, 5);
        });
      });

      this.resizeObserver.observe(container);
    } catch (err) {
      Sentry.captureException(err);

      this.args.onSetupError?.(this.args.layer);
    }
  }

  @action
  update() {
    if (this.args.layer === this._layer && this._map === this.args.map) {
      return;
    }

    if (this._layer?.options?.style == this.args.layer?.options?.style) {
      return;
    }

    if (!this.args.layer || !this.args.map) {
      return;
    }

    this.destroyMap();

    this._layer = this.args.layer;
    this._map = this.args.map;
    this.mapBack = this.args.map
      .getTargetElement()
      .parentElement.querySelector('.map-back');

    later(() => {
      this.setupMapBox(this.mapBack);
    });
  }

  destroyMap() {
    this.mapBoxInstance?.remove?.();

    if (!this._map) {
      return;
    }

    const view = this._map.getView();
    view.on('change', this._change);
    this._map.on('precompose', this._change);
    this.resizeObserver?.disconnect?.();
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.destroyMap();
  }
}
