import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | card-dashboard', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let mapData;
  let campaignData;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.after(async function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.testData.storage.addDefaultPicture();
    mapData = server.testData.storage.addDefaultMap();
    campaignData = server.testData.storage.addDefaultCampaign();
    server.testData.storage.addDefaultPicture();

    const space = this.owner.lookup('service:space');

    let spaceData = server.testData.storage.addDefaultSpace();

    const currentSpace = await this.store.findRecord('space', spaceData._id);
    space.currentSpace = currentSpace;
  });

  it('renders a map card', async function (assert) {
    let map = await this.store.findRecord("map", mapData._id);
    this.set("map", map);

    await render(hbs`<Manage::CardDashboard @record={{this.map}}/>`);

    expect(this.element.querySelector(".name").textContent.trim()).to.equal("Harta Verde București");
    expect(this.element.querySelector(".btn-main-action").textContent.trim()).to.equal("open");
    expect(this.element.querySelector(".btn-main-action")).to.have.attribute("href", "/manage/dashboards/map/5ca89e37ef1f7e010007f54c");
    expect(this.element.querySelector(".cover")).to.have.attribute("style", "background-image: url(\'/test/5d5aa72acac72c010043fb59.jpg/md\')");
    expect(this.element.querySelector(".btn-open-options")).not.to.exist;
  });

  it('renders a map card with a dropdown when it is editable', async function (assert) {
    let map = await this.store.findRecord("map", mapData._id);
    map.canEdit = true;

    this.set("map", map);

    await render(hbs`<Manage::CardDashboard @record={{this.map}}/>`);

    expect(this.element.querySelector(".btn-open-options")).to.exist;
  });

  it('renders a survey card', async function (assert) {
    let survey = await this.store.findRecord("campaign", campaignData._id);
    this.set("survey", survey);

    await render(hbs`<Manage::CardDashboard @record={{this.survey}}/>`);

    expect(this.element.querySelector(".name").textContent.trim()).to.equal("Campaign 1");
    expect(this.element.querySelector(".btn-main-action").textContent.trim()).to.equal("open");
    expect(this.element.querySelector(".btn-main-action")).to.have.attribute("href", "/manage/dashboards/survey/5ca78aa160780601008f6aaa");
  });
});
