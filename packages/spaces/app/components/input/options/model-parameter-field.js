/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { knownQueryParameters } from "../../../lib/known-query-parameters";
import { debounce } from "@ember/runloop";

export default class InputOptionsModelParameterFieldComponent extends Component {
  elementId = `model-parameters-${guidFor(this)}`;

  @tracked _idList;
  @tracked _parameterSource;
  @tracked _textValue;

  get type() {
    return knownQueryParameters[this.modelName]?.[this.args.field]?.type;
  }

  get optionList() {
    return knownQueryParameters[this.modelName]?.[this.args.field]?.values;
  }

  get parameterModel() {
    if (knownQueryParameters[this.modelName]?.[this.args.field]?.model) {
      return knownQueryParameters[this.modelName]?.[this.args.field]?.model;
    }
    return this.args.field;
  }

  get canUseSelectedModel() {
    return this.args.selectedModel && this.args.selectedModel === this.parameterModel;
  }

  get parameterSources() {
    const list = ["ignored"];

    if (this.canUseSelectedModel) {
      list.push("selected model");
    }

    list.push("default");
    list.push("state");
    list.push("fixed");

    return list;
  }

  get value() {
    if (!this.args.value) {
      return {};
    }

    if (this.args.value == "selected-model") {
      return { source: "selected-model" };
    }

    if (typeof this.args.value == "string" && this.args.value.indexOf("state-") == 0) {
      return {
        source: "state",
        state: this.parameterModel,
      };
    }

    if (typeof this.args.value == "string" && this.parameterSources.includes(this.args.value)) {
      return { source: this.args.value };
    }

    if (typeof this.args.value == "string" && !this.parameterSources.includes(this.args.value)) {
      return { source: "fixed", value: this.args.value };
    }

    return this.args.value ?? {};
  }

  get textValue() {
    if (typeof this._textValue == "string") {
      return this._textValue;
    }

    return this.value.value ?? "";
  }

  get modelName() {
    return this.args.modelName ?? "";
  }

  get parameterSource() {
    if (this._parameterSource) {
      return this._parameterSource;
    }

    if (this.value.source == "selected-model") {
      return "selected model";
    }

    if (
      this.value.source == `state-${this.parameterModel}` ||
      this.value.source == `state` ||
      this.value.source == `state value`
    ) {
      return "state";
    }

    if (this.value.source == "fixed") {
      return "fixed";
    }

    if (this.value.source == "default") {
      return "default";
    }

    return this.parameterSources[0];
  }

  @action
  changeParameterSource(newValue) {
    switch (newValue) {
      case "selected model":
        this._parameterSource = "selected-model";
        break;

      case "state":
        this._parameterSource = `state`;
        break;

      case "ignored":
        this._parameterSource = undefined;
        break;

      default:
        this._parameterSource = newValue;
    }

    debounce(this, this.triggerChange, 500);
  }

  get idList() {
    return this.textValue?.split?.(",")?.filter?.((a) => a) ?? [];
  }

  @action
  changeIdList(newValue) {
    this._idList = newValue.filter((a) => a);

    this._textValue = this._idList.join(",");

    debounce(this, this.triggerChange, 500);
  }

  @action
  changeTextValue(newValue) {
    this._textValue = newValue;

    debounce(this, this.triggerChange, 500);
  }

  @action
  triggerChange() {
    if (this.parameterSource == "ignored") {
      return this.args.onChange?.();
    }

    const result = {
      source: this.parameterSource,
      value: this.textValue,
    };

    if (this.parameterSource == "state") {
      result.state = this.parameterModel;
    }

    this.args.onChange?.(result);
  }
}
