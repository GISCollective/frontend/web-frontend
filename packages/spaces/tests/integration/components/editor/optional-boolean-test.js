/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/optional-boolean", function (hooks) {
  setupRenderingTest(hooks);

  it("renders a true value", async function () {
    this.set("value", true);

    await render(hbs`<Editor::OptionalBoolean @value={{this.value}} />`);

    expect(this.element.querySelector("select").value).to.equal("true");
  });

  it("renders a false value", async function () {
    this.set("value", false);

    await render(hbs`<Editor::OptionalBoolean @value={{this.value}} />`);

    expect(this.element.querySelector("select").value).to.equal("false");
  });

  it("renders an unset value", async function () {
    this.set("value", undefined);

    await render(hbs`<Editor::OptionalBoolean @value={{this.value}} />`);

    expect(this.element.querySelector("select").value).to.equal("unknown");
  });

  it("can change the value to true", async function () {
    this.set("value", undefined);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::OptionalBoolean @value={{this.value}} @onChange={{this.change}}/>`);

    const select = this.element.querySelector("select");

    select.value = "true";
    await triggerEvent(select, "change");

    expect(value).to.equal(true);
  });

  it("can change the value to false", async function () {
    this.set("value", undefined);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::OptionalBoolean @value={{this.value}} @onChange={{this.change}}/>`);

    const select = this.element.querySelector("select");

    select.value = "false";
    await triggerEvent(select, "change");

    expect(value).to.equal(false);
  });

  it("can change the value to undefined", async function () {
    this.set("value", true);

    let value = true;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::OptionalBoolean @value={{this.value}} @onChange={{this.change}}/>`);

    const select = this.element.querySelector("select");

    select.value = "unknown";
    await triggerEvent(select, "change");

    expect(value).to.equal(undefined);
  });
});
