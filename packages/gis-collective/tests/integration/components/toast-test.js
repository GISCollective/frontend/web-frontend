/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe ('Integration | Component | toast',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Toast />`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      <Toast>
        template block text
      </Toast>
    `);

    expect(this.element.querySelector('.toast-header')).not.to.exist;
    expect(this.element.querySelector('svg')).not.to.exist;
    expect(this.element.textContent.trim()).to.equal('template block text');
  });

  it('can set the title', async function () {
    await render(hbs`
      <Toast @title="some title">
        template block text
      </Toast>
    `);

    expect(this.element.querySelector('.toast-header')).to.exist;
    expect(this.element.querySelector('svg')).not.to.exist;
    expect(
      this.element.querySelector('.toast-header').textContent.trim()
    ).to.equal('some title');
  });

  it('can set the fa icon', async function () {
    await render(hbs`
      <Toast @title="some title" @icon="newspaper">
        template block text
      </Toast>
    `);

    expect(this.element.querySelector('svg.fa-newspaper')).to.exist;
    expect(
      this.element.querySelector('.toast-header').textContent.trim()
    ).to.equal('some title');
  });

  it('can set the title level', async function () {
    await render(hbs`
      <Toast @title="some title" @level="danger">
        template block text
      </Toast>
    `);

    expect(this.element.querySelector('.toast-header')).to.have.class(
      'text-danger'
    );
  });
});
