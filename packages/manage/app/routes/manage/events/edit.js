/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

/**
  Copyright: © 2015-2023 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { addIconGroupToStore } from '../../../lib/icons';
import { hash } from 'rsvp';

export default class ManageEventsEditRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const event = await this.store.findRecord('event', params.id);
    const calendar = await event?.calendar;
    const team = await calendar?.visibility?.fetchTeam();

    const map = await calendar.map.fetch();

    let iconSets = [];
    let icons = { icons: [] };

    if (calendar.hasIcons) {
      iconSets = await calendar.iconSets.fetch();
      icons = await this.store.adapterFor('icon').group({ iconSet: iconSets.map((a) => a.id).join() });
      addIconGroupToStore(icons, this.store);
    }

    await event.icons;

    return hash({
      event,
      calendar,
      team,
      iconSets,
      icons,
      map,
    });
  }
}
