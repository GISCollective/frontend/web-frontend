/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { service } from '@ember/service';

export default class AccessConfigService extends Service {
  @service user;
  @service preferences;

  get isMultiProjectMode() {
    if (this.user.isAdmin) {
      return true;
    }

    if (!this.preferences.isMultiProjectMode) {
      return false;
    }

    return true;
  }
}
