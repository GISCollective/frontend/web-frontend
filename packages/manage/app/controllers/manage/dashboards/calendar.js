/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ManageDashboardsCalendarController extends Controller {
  @service router;
  @service intl;

  @tracked day;
  queryParams = ['day'];

  @action
  changeDay(value) {
    this.day = value.toFormat('yyyy-MM-dd');
  }

  @action
  browseEvent(id) {
    this.router.transitionTo('manage.events.edit', id);
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }

  get recordName() {
    if (this.model.calendar.recordName) {
      return this.model.calendar.recordName;
    }

    return this.intl.t('event');
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        text: this.model.calendar.name,
      },
    ];
  }

  get query() {
    let result = {
      calendar: this.model.calendar.id,
      team: this.model.team.id,
    };

    if (this.day) {
      result.day = this.day;
    }

    return result;
  }
}
