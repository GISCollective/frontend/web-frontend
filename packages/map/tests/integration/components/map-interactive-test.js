/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, settled, click } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { expect } from 'chai';
import { transform } from 'ol/proj';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | map-interactive', function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultBaseMap('1');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders the default base maps when there is no value', async function (assert) {
    await render(hbs`<MapInteractive />`);

    expect(this.element.querySelector('.map-layer-base')).to.have.attribute(
      'data-id',
      '1',
    );
  });

  it('renders the world view with interactions when there is no user location', async function (assert) {
    await render(hbs`<MapInteractive />`);

    const map = this.element.querySelector('.map').olMap;
    const view = map.getView();

    expect(view.getZoom()).to.be.below(3);
    expect(
      map
        .getInteractions()
        .getArray()
        .map((a) => a.constructor.name),
    ).to.deep.equal([
      'DragRotate',
      'DragPan',
      'PinchRotate',
      'PinchZoom',
      'KeyboardPan',
      'KeyboardZoom',
      'MouseWheelZoom',
      'DragZoom',
      'DoubleClickZoom',
    ]);
    expect(this.element.querySelector('.map-overlay .pulse-dot')).not.to.exist;
  });

  describe('the user location with followUserLocation=true', async function () {
    it('zooms to the user location when set', async function () {
      this.set('location', [23.369218111038208, 45.43939480420836]);
      const center = transform(
        [23.369218111038208, 45.43939480420836],
        'EPSG:4326',
        'EPSG:3857',
      );

      await render(hbs`<MapInteractive @followUserLocation={{true}} @userLocation={{this.location}} />`);

      const map = this.element.querySelector('.map').olMap;
      const view = map.getView();

      expect(view.getZoom()).to.equal(18);
      expect(view.getCenter()).to.deep.equal(center);
    });

    it('shows an animated overlay at the user location', async function () {
      this.set('location', [23.369218111038208, 45.43939480420836]);
      const center = transform(
        [23.369218111038208, 45.43939480420836],
        'EPSG:4326',
        'EPSG:3857',
      );

      await render(hbs`<MapInteractive @followUserLocation={{true}} @userLocation={{this.location}} />`);

      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lon',
        `${center[0]}`,
      );
      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lat',
        `${center[1]}`,
      );
      expect(this.element.querySelector('.map-overlay .pulse-dot')).to.exist;
    });

    it('moves the view center when the user location is updated', async function () {
      this.set('location', [23.369218111038208, 45.43939480420836]);
      const center = transform(
        [22.369218111038208, 44.43939480420836],
        'EPSG:4326',
        'EPSG:3857',
      );

      await render(hbs`<MapInteractive @followUserLocation={{true}} @userLocation={{this.location}} />`);
      this.set('location', [22.369218111038208, 44.43939480420836]);

      const map = this.element.querySelector('.map').olMap;
      const view = map.getView();

      expect(view.getZoom()).to.equal(18);
      expect(view.getCenter()).to.deep.equal(center);

      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lon',
        `${center[0]}`,
      );
      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lat',
        `${center[1]}`,
      );
      expect(this.element.querySelector('.map-overlay .pulse-dot')).to.exist;
    });

    it('does not move the view center when the user location is changed if the view was "touched"', async function () {
      this.set('location', [23.369218111038208, 45.43939480420836]);
      const center2 = transform(
        [22.369218111038208, 44.43939480420836],
        'EPSG:4326',
        'EPSG:3857',
      );

      await render(hbs`<MapInteractive @followUserLocation={{true}} @userLocation={{this.location}} />`);

      const map = this.element.querySelector('.map').olMap;
      const view = map.getView();
      view.setCenter([1000, 2000]);

      await settled();

      this.set('location', [22.369218111038208, 44.43939480420836]);

      expect(view.getZoom()).to.equal(18);
      expect(view.getCenter()).to.deep.equal([1000, 2000]);

      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lon',
        `${center2[0]}`,
      );
      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lat',
        `${center2[1]}`,
      );
      expect(this.element.querySelector('.map-overlay .pulse-dot')).to.exist;
    });

    it('moves the view center after the user point is pressed', async function () {
      this.set('location', [23.369218111038208, 45.43939480420836]);
      const center1 = transform(
        [23.369218111038208, 45.43939480420836],
        'EPSG:4326',
        'EPSG:3857',
      );
      const center2 = transform(
        [22.369218111038208, 44.43939480420836],
        'EPSG:4326',
        'EPSG:3857',
      );
      const center3 = transform([22.367, 44.4394], 'EPSG:4326', 'EPSG:3857');

      await render(hbs`<MapInteractive @followUserLocation={{true}} @userLocation={{this.location}} />`);
      await new Promise((resolve) => {
        setTimeout(resolve, 1000);
      });

      const map = this.element.querySelector('.map').olMap;
      const view = map.getView();
      view.setCenter(center3);
      await settled();

      this.set('location', [22.369218111038208, 44.43939480420836]);

      await click('.pulse-dot');

      expect(view.getZoom()).to.equal(18);
      expect(view.getCenter()).to.deep.equal(center2);

      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lon',
        `${center2[0]}`,
      );
      expect(this.element.querySelector('.map-overlay')).to.have.attribute(
        'data-lat',
        `${center2[1]}`,
      );
      expect(this.element.querySelector('.map-overlay .pulse-dot')).to.exist;
    });
  });
});
