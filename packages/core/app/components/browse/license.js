/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class BrowseLicenseComponent extends Component {
  @service space;

  get list() {
    if (Array.isArray(this.args?.value)) {
      return this.args.value;
    }

    if (this.args.value) {
      return [this.args.value];
    }

    return [];
  }
}
