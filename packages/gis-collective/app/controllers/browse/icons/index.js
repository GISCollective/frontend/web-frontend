/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BrowseController from '../../base/browse-controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import AreaFilter from '../../base/filters/area-filter';
import TeamFilter from '../../base/filters/team-filter';

export default class BrowseMapsController extends BrowseController {
  @tracked areaFilter;
  @tracked teamFilter;

  @tracked areas = [];
  @tracked teams = [];

  setupFilters() {
    this.areaFilter = new AreaFilter(this.store, this.intl);
    this.areaFilter.updateArea = (newValue) => {
      this.area = newValue;
    };
    this.areaFilter.updateAreas = (newValue) => {
      this.areas = newValue;
    };

    this.teamFilter = new TeamFilter(this.store, this.intl);
    this.teamFilter.updateTeam = (newValue) => {
      this.team = newValue;
    };
    this.teamFilter.updateTeams = (newValue) => {
      this.teams = newValue;
    };
  }

  get breadcrumbs() {
    const list = [{ route: 'browse.index', text: this.intl.t('browse'), capitalize: true }];

    list.push({ text: this.intl.t('icons'), capitalize: true });

    return list;
  }

  @action
  select(items) {
    const records = items.map((id) => this.store.peekRecord('map', id)).filter((a) => a);

    this.hasPublic = records.filter((a) => a.visibility.isPublic).length > 0;
    this.hasPrivate = records.filter((a) => !a.visibility.isPublic).length > 0;
  }

  @action
  handleAction(actionName, idList) {
    if (actionName == 'publish') {
      return this.updatePublish(true, idList);
    }

    if (actionName == 'unpublish') {
      return this.updatePublish(false, idList);
    }
  }
}
