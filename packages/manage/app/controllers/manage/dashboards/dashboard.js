/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

const models = ["maps", "campaigns", "calendars", "newsletters", "iconSets", "basemaps", "spaces", "dataBindings", "presentations"];

export default class ManageDashboardsDashboardController extends Controller {
  @service router;
  @service intl;

  @tracked category = "";
  queryParams = ["category"];

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }

  @action
  selectCategory(category) {
    this.category = category;
  }

  get categories() {
    const result = [];

    for(let modelName of models) {
      if(this.model[modelName]?.length) {
        result.push(modelName);
      }
    }

    return result;
  }

  get showCreateMapAlert() {
    if (this.model.team.isPublisher) {
      return false;
    }

    return this.model.maps.length == 0;
  }

  get hasArticles() {
    const articles = this.model.articles?.filter((a) => a.slug.indexOf('notification-') != 0) ?? [];

    return articles.length > 0;
  }

  get articles() {
    let groups = {};

    this.model.articles
      ?.filter((a) => a.slug.indexOf('notification-') != 0)
      .forEach((article) => {
        let key = article.slug.split('--')[0] ?? 'other';

        if (!groups[key]) {
          groups[key] = [];
        }

        groups[key].push(article);
      });

    return groups;
  }

  get notifications() {
    return this.model.articles?.filter((a) => a.slug.indexOf('notification-') == 0);
  }

  get breadcrumbs() {
    if (!this.model.team) {
      return [];
    }

    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        text: this.model.team.name,
      },
    ];
  }
}
