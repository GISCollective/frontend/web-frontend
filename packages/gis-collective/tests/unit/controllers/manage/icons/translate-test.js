/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe('Unit | Controller | manage/icons/translate', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  it('exists', function() {
    let controller = this.owner.lookup('controller:manage/icons/translate');
    expect(controller).to.be.ok;
  });
});
