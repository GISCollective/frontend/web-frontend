/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class AppStoreLinkComponent extends Component {
  get smallText() {
    if (this.args.storeType == 'android') {
      return 'GET IT ON';
    }

    return 'Download on the';
  }

  get largeText() {
    if (this.args.storeType == 'android') {
      return 'Google Play';
    }

    return 'App Store';
  }

  get icon() {
    if (this.args.storeType == 'android') {
      return 'google-play';
    }

    return 'apple';
  }
}
