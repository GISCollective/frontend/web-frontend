/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '../../base/manage-edit-controller';

export default class ManagePresentationsEditController extends Controller {
  get editableModel() {
    return this.model.presentation;
  }
}
