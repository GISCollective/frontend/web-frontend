import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorTextColor extends Component {
  @tracked _color;

  get color() {
    if (this._color) {
      return this._color;
    }

    return this.args.value?.color ?? "";
  }

  @action
  changeColor(value) {
    this._color = value;

    this.triggerChange();
  }

  triggerChange() {
    const newOptions = { color: this.color };

    return this.args.onChange?.(newOptions);
  }
}
