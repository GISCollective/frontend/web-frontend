/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | view/currency-selector', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the CurrencySelector', async function () {
    this.set('value', {
      data: {
        categoryList: ['USD', 'EUR'],
        button: {
          classes: ['btn-green'],
        },
      },
    });

    await render(hbs`<View::CurrencySelector @value={{this.value}} />`);

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons).to.have.length(2);
    expect(buttons[0].innerText.trim()).to.equal('USD');
    expect(buttons[1].innerText.trim()).to.equal('EUR');
    expect(
      this.element.querySelector('.btn:not(.btn-selected)'),
    ).to.have.classes('btn-green');
  });

  it('renders the selected category', async function (a) {
    this.set('value', {
      data: {
        categoryList: ['USD', 'EUR'],
        button: {
          classes: ['btn-green'],
        },
      },
    });

    this.set('state', 'cu@eur');

    await render(
      hbs`<View::CurrencySelector @value={{this.value}} @state={{this.state}} />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons[1]).to.have.classes('btn-selected');
  });

  it('triggers on change state event when a category is pressed', async function (a) {
    this.set('state', 'cu@usd');
    let value;

    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    this.set('value', {
      data: {
        categoryList: ['usd', 'eur'],
      },
    });

    await render(
      hbs`<View::CurrencySelector @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    await click(buttons[1]);

    expect(value).to.equal('cu@eur');
  });
});
