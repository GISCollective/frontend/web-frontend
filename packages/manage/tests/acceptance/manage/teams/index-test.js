/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, fillIn, click } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';

describe('Acceptance | manage/teams/index', function (hooks) {
  setupApplicationTest(hooks);
  let server;


  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultSpace();
  });


  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/teams`);
    expect(currentURL()).to.contain('/login');
  });

  describe('for an admin', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('should show the team list with 1 column', async function () {
      authenticateSession();

      await visit(`/manage/teams`);

      expect(currentURL()).to.equal(`/manage/teams`);

      expect(this.element.querySelector('.error')).to.not.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('All teams');

      expect(this.element.querySelectorAll('.team-buckets tr').length).to.equal(2);
    });

    it('should search teams by name', async function () {
      authenticateSession();

      await visit(`/manage/teams`);
      expect(currentURL()).to.equal(`/manage/teams`);

      await fillIn('.filters-bar .input-search', 'search me');

      server.history = [];
      await click('.btn-search');

      expect(currentURL()).to.equal(`/manage/teams?search=search%20me`);

      expect(server.history.filter((a) => a.indexOf(' /mock-server/teams') != -1)[0]).to.contain('&term=search%20me');
    });
  });
});
