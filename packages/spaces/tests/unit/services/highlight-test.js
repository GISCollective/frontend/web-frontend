/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module, test } from "qunit";
import { setupTest } from "dummy/tests/helpers";

module("Unit | Service | highlight", function (hooks) {
  setupTest(hooks);

  // TODO: Replace this with your real tests.
  test("it exists", function (assert) {
    let service = this.owner.lookup("service:highlight");
    assert.ok(service);
  });
});
