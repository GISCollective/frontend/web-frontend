/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service, { service } from '@ember/service';

export default class PageColsService extends Service {
  @service intl;

  otherComponents = [
    'blocks-preview',
    'feature-visibility',
    'geometry',
    'icon-attribute',
    'map-name-list',
    'optional-boolean',
    'options-with-other',
    'options',
    'user',
  ];

  defaultComponents = [
    'title-with-buttons',
    'heading',
    'button',
    'blocks',
    'picture',
    'pictures',
    'gallery',
    'sounds',
    'menu',
    'menu-light',
    'card-list',
    'contact-form',
    'link-list',
    'stats',
    'filters',
    'breadcrumb',
    'newsletter',
    'horizontal-line',
    'categories',
    'list',
    'price-offer',
    'currency-selector',
    'attribute',
    'attributes-group',
    'fragment',
    'redirect',
    'social-media',
    'embedded-video',
    'card'
  ];

  articleComponents = [
    'article',
    'article/title',
    'article/content',
    'article/date',
  ];
  eventComponents = ['event/dates', 'event/location', 'event/schedule'];
  campaignComponents = ['campaign-card-list', 'campaign-form'];
  iconComponents = ['icons', 'icon/list', 'icon/grid-with-categories'];
  mapComponents = ['map/map-view', 'map/feature'];
  featureComponents = [
    'feature/attributes',
    'feature/issue-button',
    'feature/campaign-contribution-button',
  ];
  profileComponents = ['profile/card'];
  backgroundComponents = [
    'background/color',
    'background/image',
    'background/map',
  ];

  cardTypes = [
    'cover-title-description',
    'cover-title-overlay-description',
    'cover-title',
    'title-description',
    'cover',
    'cover-title-description-list',
  ];

  get availableTypes() {
    return [
      ...this.defaultComponents,
      ...this.articleComponents,
      ...this.mapComponents,
      ...this.featureComponents,
      ...this.campaignComponents,
      ...this.iconComponents,
      ...this.profileComponents,
      ...this.otherComponents,
      ...this.eventComponents,
    ];
  }

  get defaultButtons() {
    return this.defaultComponents.map((a) => this.toButton(a));
  }

  get articleButtons() {
    return this.articleComponents.map((a) => this.toButton(a));
  }

  get campaignButtons() {
    return this.campaignComponents.map((a) => this.toButton(a));
  }

  get iconButtons() {
    return this.iconComponents.map((a) => this.toButton(a));
  }

  get profileButtons() {
    return this.profileComponents.map((a) => this.toButton(a));
  }

  get mapButtons() {
    return this.mapComponents.map((a) => this.toButton(a));
  }

  get featureButtons() {
    return this.featureComponents.map((a) => this.toButton(a));
  }

  get eventButtons() {
    return this.eventComponents.map((a) => this.toButton(a));
  }

  get groups() {
    return [
      {
        groupName: 'generic',
        options: this.defaultButtons,
      },
      {
        groupName: 'article',
        options: this.articleButtons,
      },
      {
        groupName: 'map',
        options: this.mapButtons,
      },
      {
        groupName: 'feature',
        options: this.featureButtons,
      },
      {
        groupName: 'campaign',
        options: this.campaignButtons,
      },
      {
        groupName: 'icon',
        options: this.iconButtons,
      },
      {
        groupName: 'profile',
        options: this.profileButtons,
      },
      {
        groupName: 'event',
        options: this.eventButtons,
      },
    ];
  }

  toButton(name) {
    const icons = {
      blocks: 'paragraph',
      picture: 'image',
      pictures: 'images',
      sounds: 'file-audio',
      article: 'stream',
      icons: 'chess-pawn',
      'campaign-card-list': 'pencil-alt',
      menu: 'bars',
    };

    let label = name
      .split('-')
      .flatMap((a) => a.split('/'))
      .join(' ');
    const cls = label.split(' ').join('-');

    if (this.intl.exists(label)) {
      label = this.intl.t(label);
    }

    return {
      name,
      label,
      cls,
      icon: icons[name],
    };
  }
}
