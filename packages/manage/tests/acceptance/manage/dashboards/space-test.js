/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, fillIn, waitUntil } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/space', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let space;
  let team;
  let receivedPage;
  let pageData;

  hooks.beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultSpace();
    let otherSpace = server.testData.storage.addDefaultSpace('2');
    otherSpace.name = 'other space';

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    space = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture();
    team = server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');
    server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');
    server.testData.storage.addDefaultIcon();

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.get(`/mock-server/layouts/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    receivedPage = null;
    server.post('/mock-server/pages', (request) => {
      receivedPage = JSON.parse(request.requestBody);
      pageData = server.testData.storage.addDefaultPage();

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({ page: pageData })];
    });
  });

  hooks.afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/space/${space._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2Fspace%2F6227c131624b2cf1626dd029');
  });

  it('should show the dashboard page when the user is authenticated', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/space/${space._id}`);
    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('header').textContent.trim()).to.contain(space.name);
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/space/${space._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'GISCollective']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/61292c4c7bdf9301008fd7b6',
    ]);
  });

  it('can add a new page using the toolbar', async function (a) {
    space.matomoUrl = 'http://test.com';
    authenticateSession();

    await visit(`/manage/dashboards/space/${space._id}`);
    expect(this.element.querySelector('.btn-analytics')).to.exist;
    expect(this.element.querySelector('.btn-analytics')).to.have.attribute('href', 'http://test.com');
  });

  it('shows a button to open the space in a new tab', async function (a) {
    authenticateSession();

    await visit(`/manage/dashboards/space/${space._id}`);

    expect(this.element.querySelector('.btn-open')).to.have.attribute('href', 'https://test.giscollective.com');
  });

  it('can add a new page using the toolbar', async function (a) {
    authenticateSession();

    await visit(`/manage/dashboards/space/${space._id}`);
    expect(this.element.querySelector('.btn-analytics')).not.to.exist;
    await click('.btn-add-page');

    expect(currentURL()).to.equal(`/manage/pages/add?spaceId=${space._id}`);

    expect(this.element.querySelector('.row-space')).not.to.exist;
    await fillIn('.input-name', 'test name');
    await fillIn('.input-path', '/test name');
    await click('.btn-submit');

    await waitUntil((a) => receivedPage);

    expect(receivedPage.page.space).to.equal(space._id);
  });

  describe('when the space has a page', function (hooks) {
    let page;

    hooks.beforeEach(function () {
      page = server.testData.create.page('5ca89e27ef1f7e010007f4af');
      page.canEdit = true;
      page.visibility.team = team._id;
      page.description = 'this is the test page';

      server.testData.storage.addPage(page);
    });

    it('should display the page as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/space/${space._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-page').length).to.equal(1);
      expect(this.element.querySelector('.card-dashboard-page .title').textContent.trim()).to.equal(page.name);

      expect(this.element.querySelector('.page-description').textContent.trim()).to.equal('this is the test page');
      expect(this.element.querySelector('.page-relative-path').textContent.trim()).to.equal('/page/test');
    });

    it('should be able to delete the page using the dropdown menu', async function () {
      authenticateSession();
      let deletedPage = false;
      server.delete(`/mock-server/pages/${page._id}`, () => {
        deletedPage = true;

        return [204, {}, ''];
      });

      const service = this.owner.lookup('service:notifications');
      service.ask = () => 'yes';

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.card-dashboard-page .dropdown-toggle');
      await click('.card-dashboard-page .btn-delete');

      expect(deletedPage).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-page')).not.to.exist;
    });

    it('allows deleting a page using the checkbox selection', async function () {
      authenticateSession();
      let deletedPage = false;
      server.delete(`/mock-server/pages/${page._id}`, () => {
        deletedPage = true;
        server.testData.storage.pages = {};

        return [204, {}, ''];
      });

      const service = this.owner.lookup('service:modal');
      service.confirm = () => 'yes';

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.manage-card .chk-selected');

      await click('.actions-selected .btn-delete');

      expect(deletedPage).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-page')).not.to.exist;
    });

    it('allows unpublishing a page using the checkbox selection', async function () {
      authenticateSession();
      let unpublish = false;
      server.post(`/mock-server/pages/${page._id}/unpublish`, (req) => {
        unpublish = true;
        return [204, {}, ''];
      });

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.manage-card .chk-selected');

      await click('.actions-selected .btn-unpublish');

      expect(unpublish).to.eql(true);
    });

    it('allows publishing a page using the checkbox selection', async function () {
      authenticateSession();

      let publish = false;
      page.visibility.isPublic = false;

      server.post(`/mock-server/pages/${page._id}/publish`, (req) => {
        publish = true;
        return [204, {}, ''];
      });

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.manage-card .chk-selected');
      await click('.actions-selected .btn-publish');

      expect(publish).to.eql(true);
    });

    it('navigates to the page content edit when the edit button is pressed', async function () {
      server.testData.storage.addDefaultSpace();
      authenticateSession();

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.card-dashboard-page .dropdown-toggle');
      await click('.card-dashboard-page .dropdown-item-edit');

      expect(currentURL()).to.equal('/manage/pages/5ca89e27ef1f7e010007f4af/content');
    });

    it('navigates to the page when the title is clicked', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.card-dashboard-page .title .btn');

      expect(currentURL()).to.equal('/manage/pages/5ca89e27ef1f7e010007f4af/content');
    });
  });

  describe('when the space has many pages', function (hooks) {
    hooks.beforeEach(function () {
      let page = server.testData.create.page('1');
      page.canEdit = true;
      page.visibility.team = team._id;
      page.slug = 'group1--page1';

      server.testData.storage.addPage(page);

      page = server.testData.create.page('2');
      page.canEdit = true;
      page.visibility.team = team._id;
      page.visibility.isPublic = false;
      page.slug = 'group1--page2';

      server.testData.storage.addPage(page);

      page = server.testData.create.page('3');
      page.canEdit = true;
      page.visibility.team = team._id;
      page.slug = 'group3--page3';

      server.testData.storage.addPage(page);
    });

    it('displays pages grouped by first slug', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/space/${space._id}`);

      expect(this.element.querySelectorAll('.card-dashboard-page').length).to.equal(3);

      expect([...this.element.querySelectorAll('h4')].map((a) => a.textContent.trim())).to.deep.equal([
        'group1',
        'group3',
      ]);
    });

    it('allows deleting all pages', async function () {
      authenticateSession();
      let queryParams;
      server.delete(`/mock-server/pages/`, (req) => {
        queryParams = req.queryParams;

        return [204, {}, ''];
      });

      const service = this.owner.lookup('service:modal');
      service.confirm = () => 'yes';

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.chk-select-all');

      await click('.actions-selected .btn-delete');

      expect(queryParams).to.eql({ space: '6227c131624b2cf1626dd029', team: '61292c4c7bdf9301008fd7b6' });
    });

    it('allows unpublishing all pages', async function () {
      authenticateSession();

      let query;
      server.post(`/mock-server/pages/unpublish`, (req) => {
        query = req.queryParams;

        return [204, {}, ''];
      });

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.chk-select-all');

      await click('.actions-selected .btn-unpublish');

      expect(query).to.eql({ space: '6227c131624b2cf1626dd029', team: '61292c4c7bdf9301008fd7b6' });
    });

    it('allows publishing all pages', async function () {
      authenticateSession();

      let query;
      server.post(`/mock-server/pages/publish`, (req) => {
        query = req.queryParams;

        return [204, {}, ''];
      });

      await visit(`/manage/dashboards/space/${space._id}`);

      await click('.chk-select-all');

      await click('.actions-selected .btn-publish');

      expect(query).to.eql({ space: '6227c131624b2cf1626dd029', team: '61292c4c7bdf9301008fd7b6' });
    });
  });
});
