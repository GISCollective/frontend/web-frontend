/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { attr, belongsTo } from '@ember-data/model';
import Model from './base/primary-model';
import Polygon from 'ol/geom/Polygon';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';
import { generateBlur, idToHash } from '../lib/blur';
import { getOwner } from '@ember/application';

export default class MapModel extends Model {
  @attr('string') name;
  @attr description;
  @attr('string') tagLine;
  @attr('geo') area;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @belongsTo('picture', { async: true, inverse: null })
  squareCover;

  @attr('boolean') canEdit;
  @attr('boolean') hideOnMainMap;
  @attr('boolean') showPublicDownloadLinks;
  @attr('boolean') isIndex;
  @attr('boolean') addFeaturesAsPending;
  @attr license;
  @attr('optional-map') mask;
  @attr('cluster') cluster;
  @attr('model-list-with-default', { model: 'icon-set' }) iconSets;
  @attr('model-list-with-default', { model: 'base-map' }) baseMaps;
  @attr() meta;
  @attr('icon-sprite') sprites;

  @attr('date', {
    defaultValue() {
      let value = new Date();
      value.setFullYear(0);
      return value;
    },
  })
  startDate;

  @attr('date', {
    defaultValue() {
      let value = new Date();
      value.setFullYear(0);
      return value;
    },
  })
  endDate;

  async reloadMeta() {
    try {
      this.meta = await this.store.adapterFor(this.constructor.modelName).getMeta(this.id);
    } catch (err) {
      console.error(err);
    }

    return this.meta;
  }

  async getCover() {
    return await this.cover;
  }

  async loadRelations() {
    const promises = [];

    promises.push(this.visibility.fetchTeam());
    promises.push(this.baseMaps.fetch());
    promises.push(this.iconSets.fetch());
    promises.push(this.reloadMeta());

    await Promise.all(promises);
  }

  get hashBg() {
    return generateBlur(idToHash(this.id), 4, 4);
  }

  get viewbox() {
    if (!this.area) {
      return '';
    }

    const geometry = new Polygon(this.area.coordinates.slice());
    return geometry.getExtent().join(',');
  }

  get hasCover() {
    if (!this.cover && !this.squareCover) {
      return false;
    }

    const coverName = this.cover ? this.cover.get('name') : 'default';
    const squareCoverName = this.squareCover ? this.squareCover.get('name') : 'default';

    const coverPicture = this.cover ? this.cover.get('picture') : null;
    const squareCoverPicture = this.squareCover ? this.squareCover.get('picture') : null;

    return (coverPicture && coverName != 'default') || (squareCoverPicture && squareCoverName != 'default');
  }

  // @deprecated
  get coverPicture() {
    if (!this.hasCover) {
      return '';
    }

    const coverName = this.cover ? this.cover.get('name') : 'default';
    if (coverName != 'default' && this.cover.get('picture')) {
      return this.cover.get('picture');
    }

    const squareCoverName = this.squareCover ? this.squareCover.get('name') : 'default';
    if (squareCoverName != 'default' && this.squareCover.get('picture')) {
      return this.squareCover.get('picture');
    }

    return '';
  }

  // @deprecated
  get squareCoverPicture() {
    if (!this.hasCover) {
      return '';
    }
    const squareCoverName = this.squareCover ? this.squareCover.get('name') : 'default';
    if (squareCoverName != 'default' && this.squareCover.get('picture')) {
      return this.squareCover.get('picture');
    }

    const coverName = this.cover ? this.cover.get('name') : 'default';
    if (coverName != 'default' && this.cover.get('picture')) {
      return this.cover.get('picture');
    }

    return '';
  }

  get cover() {
    return this.squareCoverPicture();
  }

  get hasStartDate() {
    return this.startDate?.getFullYear() > 1;
  }

  get hasEndDate() {
    return this.endDate?.getFullYear() > 1;
  }

  get tagLineContentBlocks() {
    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.tagLine,
          },
        },
      ],
    };
  }

  get title() {
    return this.name;
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  set descriptionContentBlocks(content) {
    this.description = toContentBlocks(content, this.name);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async fillMetaInfo(metaInfo) {
    const cover = await this.cover;
    const squareCover = await this.squareCover;

    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;
    metaInfo.author = this.visibility?.team?.name;
    metaInfo.imgSrc = squareCover?.picture || cover?.picture;

    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }

  get iconSetsFilter() {
    return this.iconSets?.idList ?? [];
  }

  get tilesUrl() {
    return `${this.config.siteTilesUrl}/{z}/{x}/{y}?format=vt&map=${this.id}`;
  }

  get config() {
    return getOwner(this).resolveRegistration('config:environment');
  }

  getAttributeValues(attribute, term = '', limit = 5) {
    return this.store.adapterFor(this.constructor.modelName).getAttributeValues(this.id, attribute, term, limit);
  }
}
