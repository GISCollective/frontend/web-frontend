/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/attributes-group', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders the value', async function (assert) {
      this.set('value', {
        key1: true,
        key2: false,
      });

      this.set('attributes', [
        {
          name: 'key1',
          displayName: 'key 1',
          help: 'is it true?',
          type: 'boolean',
          options: '',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
        {
          name: 'key2',
          displayName: 'key 2',
          help: 'is it false?',
          type: 'boolean',
          options: '',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
      ]);

      await render(
        hbs`<Manage::AttributesGroup @value={{this.value}} @attributes={{this.attributes}} @title="attributes" />`,
      );

      const values = [...this.element.querySelectorAll('.attribute')];
      expect(values).to.have.length(2);

      expect(values[0].querySelector('.attribute-name').textContent.trim()).to.equal('key 1');
      expect(values[0].querySelector('.attribute-help').textContent.trim()).to.equal('is it true?');
      expect(values[0].querySelector('.attribute-value').textContent.trim()).to.equal('yes');

      expect(values[1].querySelector('.attribute-name').textContent.trim()).to.equal('key 2');
      expect(values[1].querySelector('.attribute-help').textContent.trim()).to.equal('is it false?');
      expect(values[1].querySelector('.attribute-value').textContent.trim()).to.equal('no');
    });
  });

  describe('in edit mode', function () {
    it('renders the value', async function (assert) {
      this.set('value', {
        key1: true,
        key2: 'c',
      });

      this.set('attributes', [
        {
          name: 'key1',
          displayName: 'key 1',
          help: 'is it true?',
          type: 'boolean',
          options: '',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
        {
          name: 'key2',
          displayName: 'key 2',
          help: 'is it false?',
          type: 'options',
          options: 'a,b,c,d',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
      ]);

      await render(
        hbs`<Manage::AttributesGroup @value={{this.value}} @attributes={{this.attributes}} @editablePanel="attributes" @title="attributes" />`,
      );

      const values = [...this.element.querySelectorAll('.attribute-value')];

      expect(values.length).to.equal(2);

      expect(values[0].querySelector('.btn-true')).to.have.class('btn-success');
      expect(values[1].querySelector('.name-list').textContent.trim()).to.equal('c');
    });

    it('can edit the entry', async function (assert) {
      let value;

      this.set('value', {
        key1: true,
        key2: 'c',
      });

      this.set('attributes', [
        {
          name: 'key1',
          displayName: 'key 1',
          help: 'is it true?',
          type: 'boolean',
          options: '',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
        {
          name: 'key2',
          displayName: 'key 2',
          help: 'is it false?',
          type: 'options',
          options: 'a,b,c,d',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
      ]);

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::AttributesGroup @onSave={{this.save}} @value={{this.value}} @attributes={{this.attributes}} @editablePanel="attributes" @title="attributes" />`,
      );

      let values = [...this.element.querySelectorAll('.attribute-value')];

      await click(values[0].querySelector('.btn-false'));

      values = [...this.element.querySelectorAll('.attribute-value')];

      await click(values[1].querySelector('.btn-change'));
      const options = values[1].querySelectorAll('.list-group-item-action');

      await click(options[1]);
      await click('.btn-submit');

      expect(value).to.deep.equal({ key1: false, key2: 'b' });
    });

    it('can cancel the changes', async function (assert) {
      let value;

      this.set('value', {
        key1: true,
        key2: 'c',
      });

      this.set('attributes', [
        {
          name: 'key1',
          displayName: 'key 1',
          help: 'is it true?',
          type: 'boolean',
          options: '',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
        {
          name: 'key2',
          displayName: 'key 2',
          help: 'is it false?',
          type: 'options',
          options: 'a,b,c,d',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
      ]);

      this.set('save', (v) => {
        value = v;
      });

      let cancelCalled;
      this.set('cancel', () => {
        cancelCalled = true;
      });

      await render(
        hbs`<Manage::AttributesGroup @onSave={{this.save}} @onCancel={{this.cancel}} @value={{this.value}} @attributes={{this.attributes}} @editablePanel="attributes" @title="attributes" />`,
      );

      let values = [...this.element.querySelectorAll('.attribute-value')];

      await click(values[0].querySelector('.btn-false'));

      values = [...this.element.querySelectorAll('.attribute-value')];

      await click(values[1].querySelector('.btn-change'));
      const options = values[1].querySelectorAll('.list-group-item-action');

      await click(options[1]);

      await click('.btn-cancel');

      expect(value).not.to.exist;
      expect(cancelCalled).to.equal(true);
    });
  });
});
