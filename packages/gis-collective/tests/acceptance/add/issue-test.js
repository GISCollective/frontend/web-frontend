/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import {
  visit,
  currentURL,
  click,
  fillIn,
  waitUntil,
  triggerEvent,
  waitFor,
} from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";

describe  ('Acceptance | add/issue',   function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let featureData;
  let receivedIssue;

  hooks.afterEach(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultSpace();

    featureData = server.testData.storage.addDefaultFeature();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultBaseMap();
    server.testData.storage.addDefaultProfile();

    receivedIssue = null;

    server.post('/mock-server/issues', (request) => {
      receivedIssue = JSON.parse(request.requestBody);
      receivedIssue.issue._id = '1';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedIssue),
      ];
    });
  });

  describe('adding an issue without a type', function (hooks) {
    it('can visit /add/issue', async function () {
      await visit(`/add/issue?feature=${featureData._id}`);

      expect(currentURL()).to.equal(`/add/issue?feature=${featureData._id}`);
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'New issue'
      );
      expect(this.element.querySelector('.error')).not.to.exist;
    });

    it('redirects to the feature page on submit', async function () {
      await visit(
        `/add/issue?feature=${featureData._id}&next=/browse/sites/${featureData._id}`
      );

      await fillIn('.input-issue-title', 'title');
      await fillIn('.input-issue-description', 'description');

      await waitFor('.btn-submit:enabled');

      await click('.btn-submit');

      await waitUntil(() => receivedIssue);

      expect(receivedIssue.issue).to.deep.contain({
        _id: '1',
        title: 'title',
        description: 'description',
        type: 'none',
        feature: featureData._id,
      });

      expect(currentURL()).to.equal(`/browse/sites/${featureData._id}`);
    });

    it('redirects to the index page when next is not set', async function () {
      await visit(`/add/issue?feature=${featureData._id}`);

      await fillIn('.input-issue-title', 'title');
      await fillIn('.input-issue-description', 'description');

      await click('.btn-submit');

      await waitUntil(() => receivedIssue);

      expect(receivedIssue.issue).to.deep.contain({
        _id: '1',
        title: 'title',
        description: 'description',
        type: 'none',
        feature: featureData._id,
      });

      expect(currentURL()).to.equal(`/browse/maps/_/map-view`);
    });

    it('shows the feature name linked to the browse page', async function () {
      await visit(`/add/issue?feature=${featureData._id}`);

      expect(
        this.element.querySelector('.feature-name').textContent.trim()
      ).to.equal('Nomadisch Grün - Local Urban Food');
      expect(this.element.querySelector('.feature-name')).to.have.attribute(
        'href',
        '/browse/sites/5ca78e2160780601008f69e6'
      );
    });

    it('shows the team name linked to the browse page when it exists', async function () {
      server.testData.storage.addDefaultTeam();
      await visit(`/add/issue?feature=${featureData._id}`);

      expect(
        this.element.querySelector('.team-name').textContent.trim()
      ).to.equal('Open Green Map');
      expect(this.element.querySelector('.team-name')).to.have.attribute(
        'href',
        '/browse/teams/5ca78e2160780601008f69e6'
      );
    });

    it('does not show the team paragraph when the team does not exist', async function () {
      await visit(`/add/issue?feature=${featureData._id}`);
      expect(this.element.querySelector('.team-name')).not.to.exist;
    });
  });

  describe('adding an issue with a picture type', function (hooks) {
    it('can visit /add/issue?type=photo', async function () {
      await visit(`/add/issue?type=photo&feature=${featureData._id}`);

      expect(currentURL()).to.equal(
        `/add/issue?type=photo&feature=${featureData._id}`
      );

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'Suggest photo'
      );
    });

    it('shows the feature name linked to the browse page', async function () {
      await visit(`/add/issue?type=photo&feature=${featureData._id}`);

      expect(
        this.element.querySelector('.feature-name').textContent.trim()
      ).to.equal('Nomadisch Grün - Local Urban Food');
      expect(this.element.querySelector('.feature-name')).to.have.attribute(
        'href',
        '/browse/sites/5ca78e2160780601008f69e6'
      );
    });

    it('shows the team name linked to the browse page when it exists', async function () {
      server.testData.storage.addDefaultTeam();
      await visit(`/add/issue?type=photo&feature=${featureData._id}`);

      expect(
        this.element.querySelector('.team-name').textContent.trim()
      ).to.equal('Open Green Map');
      expect(this.element.querySelector('.team-name')).to.have.attribute(
        'href',
        '/browse/teams/5ca78e2160780601008f69e6'
      );
    });

    it('does not show the team paragraph when the team does not exist', async function () {
      await visit(`/add/issue?type=photo&feature=${featureData._id}`);
      expect(this.element.querySelector('.team-name')).not.to.exist;
    });

    it('redirects to the feature page on submit', async function () {
      await visit(
        `/add/issue?type=photo&feature=${featureData._id}&next=/browse/sites/${featureData._id}`
      );

      await fillIn('.input-issue-title', 'title');
      await fillIn('.input-issue-description', 'description');
      await fillIn('.input-issue-attributions', 'attributions');

      const blob = server.testData.create.pngBlob();

      await triggerEvent("input[type='file']", 'change', {
        files: [blob],
      });

      await waitFor('.btn-submit:enabled');
      await click('.btn-submit');

      await waitUntil(() => receivedIssue);

      expect(receivedIssue.issue).to.deep.contain({
        _id: '1',
        title: 'title',
        description: 'description',
        type: 'none',
        feature: featureData._id,
      });

      expect(currentURL()).to.equal(`/browse/sites/${featureData._id}`);
    });
  });
});
