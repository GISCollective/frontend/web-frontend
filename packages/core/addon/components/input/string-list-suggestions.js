/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';

export default class InputStringListSuggestionsComponent extends Component {
  @tracked _list = null;
  @tracked filter = '';

  get primaryValue() {
    if (!this.args.primaryOptions?.length) {
      return null;
    }

    if (this._list) {
      return this._list.find((a) => this.args.primaryOptions.includes(a));
    }

    return this.args.value.find((a) => this.args.primaryOptions.includes(a));
  }

  get otherValue() {
    const primary = this.primaryValue;

    if (this._list) {
      return this._list.filter((a) => a != primary);
    }

    return this.args.value.filter((a) => a != primary);
  }

  get list() {
    if (this._list) {
      return this._list;
    }

    if (this.args.primaryOptions?.length) {
      return A([this.primaryValue, ...this.otherValue]);
    }

    return A(this.args.value);
  }

  set list(value) {
    this._list = value;
  }

  get options() {
    return this.args.options?.filter((a) => a.toLowerCase().includes(this.filter));
  }

  @action
  setupPrimaryValue() {
    this.args.onPrimaryChange?.(this.primaryValue);
  }

  @action
  setPrimaryValue(value) {
    this.setItem(0, value);
    this.args.onPrimaryChange?.(value);
  }

  @action
  applyValue(index) {
    if (!this.filter) {
      return;
    }

    this.setItem(index, this.filter);
  }

  @action
  change() {
    this.args.onChange?.(this.list.slice());
  }

  @action
  remove(index) {
    if (!this._list) {
      this._list = A(this.list);
    }

    this._list.splice(index, 1);

    this.change();
  }

  @action
  addItem() {
    if (!this._list) {
      this._list = A(this.list);
    }

    this._list.push('');
    this.change();
  }

  @action
  search(index, value) {
    this.filter = value;
  }

  @action
  setItem(index, value) {
    if (!this._list) {
      this._list = A(this.list);
    }

    this._list.splice(index, 1, value);

    this.change();

    this.filter = '';
  }
}
