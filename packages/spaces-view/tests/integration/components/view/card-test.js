import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | view/card component', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let pictureData;

  hooks.before(function () {
    server = new TestServer();
    pictureData = server.testData.storage.addDefaultPicture('67c5afb5193b86089fef045e');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      picture,
    });

    this.set("value", {
      modelKey: 'picture',
      data: {
      "style": {
        "color": "green-600",
        "classes": [
          "ff-fredoka-one"
        ],
        "minLines": {
          "md": 0,
          "lg": 0,
          "sm": 0
        },
        "text": "some description"
      },
      "source": {
        "useSelectedModel": false,
        "useDefaultModel": false,
        "id": "67c5afb5193b86089fef045e",
        "model": "picture"
      },
      "picture": {
        "size": {
          "sizing": "proportional",
          "proportion": "1:1"
        },
        "container": {}
      },
      "titleStyle": {
        "color": "green-400",
        "classes": [],
        "minLines": {
          "md": 0,
          "lg": 0,
          "sm": 0
        },
        "text": "some title",
        "heading": "1"
      },
      "button": {
        "style": {
          "classes": [
            "btn-primary"
          ]
        },
        "pictureOptions": {},
        "content": {
          "link": {
            "url": "https://giscollective.com"
          },
          "name": "a button"
        },
        "containerStyle": {}
      }}
    });

    await render(hbs`<View::Card @value={{this.value}} @model={{this.model}} />`);

    expect(this.element.querySelector(".picture-bg")).to.have.attribute("style", "background-image: url(/test/5d5aa72acac72c010043fb59.jpg.lg.jpg);aspect-ratio: 1/1;");
    expect(this.element.querySelector("h1").textContent.trim()).to.equal("some title");
    expect(this.element.querySelector("h1")).to.have.classes("text-green-400");
    expect(this.element.querySelector("p").textContent.trim()).to.equal("some description");
    expect(this.element.querySelector("p")).to.have.classes("ff-fredoka-one text-green-600");
    expect(this.element.querySelector(".btn-primary").textContent.trim()).to.equal("a button");
  });
});
