/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { getOwner } from '@ember/application';
import { service } from '@ember/service';

export default class ModelActionsRecordComponent extends Component {
  @service space;

  get componentName() {
    return `model-actions/${this.recordType}`;
  }

  get hasOptions() {
    let applicationInstance = getOwner(this);
    return !!applicationInstance.resolveRegistration(`component:${this.componentName}`);
  }

  get viewPage() {
    return this.space.getLinkFor(this.recordType, this.args.record);
  }

  get recordType() {
    return this.args.record?.constructor?.modelName;
  }
}
