/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import CookieStore from 'ember-simple-auth/session-stores/cookie';
import { service } from '@ember/service';

export default class ApplicationSessionStore extends CookieStore {
  @service fastboot;

  clear() {
    if(this.fastboot.isFastBoot) {
      return super.clear(...arguments);
    }

    localStorage.setItem("auth", "{}");

    console.log("auth clear");

    return super.clear(...arguments);
  }

  async restore() {
    let result = await super.restore();

    if(this.fastboot.isFastBoot) {
      return result;
    }

    if(!result?.authenticated?.access_token) {
      try {
        result = JSON.parse(localStorage.getItem("auth"));
      } catch(err) {
        console.error(err);
      }
    }

    return result;
  }

  persist(data) {
    if(this.fastboot.isFastBoot) {
      return super.persist(...arguments);
    }

    localStorage.setItem("auth", JSON.stringify(data));

    console.log("auth persist");

    return super.persist(...arguments);
  }
}
