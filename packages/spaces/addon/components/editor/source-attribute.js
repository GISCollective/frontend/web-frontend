/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorSourceAttributeComponent extends Component {
  @tracked _attributeField;

  get attributeField() {
    if (this._attributeField) {
      return this._attributeField;
    }

    return this.args.value?.field ?? "";
  }

  triggerChange() {
    return this.args.onChange?.({
      field: this.attributeField,
    });
  }

  @action
  change(key, value) {
    this[`_${key}`] = value;

    this.triggerChange();
  }
}
