/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | state-filters/day-of-week', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the pill when no value is set', async function (assert) {
    await render(hbs`<StateFilters::DayOfWeek />`);

    assert.dom(this.element.querySelector('.btn-pill')).hasText('day of week');
  });

  it('renders the custom label when it is set', async function (assert) {
    this.set('options', {
      label: 'date',
    });

    await render(hbs`<StateFilters::DayOfWeek @options={{this.options}} />`);

    assert.dom(this.element.querySelector('.btn-pill')).hasText('date');
  });

  it('shows all options when it is clicked', async function (assert) {
    this.set('options', {
      label: 'date',
    });

    await render(hbs`<StateFilters::DayOfWeek @options={{this.options}} />`);

    await click('.btn-pill');

    const btns = this.element.querySelectorAll('.btn-day');

    expect([...btns].map((a) => a.textContent.trim())).to.deep.equal([
      'Mo',
      'Tu',
      'We',
      'Th',
      'Fr',
      'Sa',
      'Su',
      'Weekday',
      'Weekend',
    ]);
  });

  it('triggers on change when one option it is clicked', async function (assert) {
    this.set('options', {
      label: 'date',
    });
    let value;

    this.set('state', '');
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    await render(
      hbs`<StateFilters::DayOfWeek @options={{this.options}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    await click('.btn-pill');

    const btns = this.element.querySelectorAll('.btn-day');

    await click(btns[3]);

    expect(value).to.equal('dow@3');
  });

  it('shows the selected day', async function (assert) {
    this.set('options', {
      label: 'date',
    });

    this.set('state', 'dow@1');

    await render(
      hbs`<StateFilters::DayOfWeek @state={{this.state}} @options={{this.options}} />`,
    );

    await click('.btn-pill');

    assert.dom(this.element.querySelector('.btn-pill')).hasText('Tuesday');

    const btns = this.element.querySelectorAll('.btn-day.btn-success');
    expect([...btns].map((a) => a.textContent.trim())).to.deep.equal(['Tu']);
  });

  it('can reset the day value', async function (assert) {
    this.set('options', {
      label: 'date',
    });

    this.set('state', 'dow@1');

    let value;
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    await render(
      hbs`<StateFilters::DayOfWeek @state={{this.state}} @options={{this.options}} @onChangeState={{this.onChangeState}} />`,
    );

    await click('.btn-reset');

    expect(value).to.equal('');
  });
});
