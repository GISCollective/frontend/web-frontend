import Application from 'embedded/app';
import config from 'embedded/config/environment';
import * as QUnit from 'qunit';
import { setApplication } from '@ember/test-helpers';
import { setup } from 'qunit-dom';
import chai from 'chai';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

setup(QUnit.assert);

start();

const elementFrom = (target) => {
  if (typeof target === 'object') return target;
  return find(target);
};

chai.Assertion.addMethod('attribute', function (attribute, expectedAttribute) {
  let actualAttribute = elementFrom(this._obj)?.getAttribute?.(attribute);

  if (typeof actualAttribute === 'string') {
    actualAttribute = actualAttribute
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a != '')
      .join(' ');
  }

  this.assert(
    actualAttribute === expectedAttribute,
    `expected #{this} to have ${attribute} to #{exp}, but got #{act}`,
    `expected #{this} to not have ${attribute} to #{act}`,
    expectedAttribute,
    actualAttribute,
  );
});

chai.Assertion.addMethod('textContent', function (expectedText) {
  let actualText = elementFrom(this._obj)
    .textContent.split('\n')
    .map((a) => a.trim())
    .filter((a) => a)
    .join(' ');

  this.assert(
    actualText === expectedText,
    `expected #{this} to have text to #{exp}, but got #{act}`,
    `expected #{this} to not have text to #{act}`,
    expectedText,
    actualText,
  );
});
