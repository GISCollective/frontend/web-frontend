/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class MainPlusComponent extends Component {
  @service user;

  get canAddNewsletter() {
    const teamWithNewsletter = this.user.teams?.find((a) => a.allowNewsletters);

    return teamWithNewsletter || this.user.isAdmin;
  }

  get canAddDataBinding() {
    const teamWithDataBinding = this.user.teams?.find((a) => a.allowCustomDataBindings);

    return teamWithDataBinding || this.user.isAdmin;
  }

  get canAddEvents() {
    const teamWithEvents = this.user.teams?.find((a) => a.allowEvents);

    return teamWithEvents || this.user.isAdmin;
  }
}
