/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, click, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/properties/source-group-style", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the values", async function (a) {
    this.set("value", {
      classes: ["text-center"],
      color: "green",
      accentColor: "blue",
      maxItems: {
        sm: 2,
        md: 4,
        lg: 6,
      },
    });

    await render(hbs`<Editor::Properties::SourceGroupStyle @value={{this.value}} />`);

    expect(this.element.querySelector(".input-max-items-sm").value).to.equal("2");
    expect(this.element.querySelector(".input-max-items-md").value).to.equal("4");
    expect(this.element.querySelector(".input-max-items-lg").value).to.equal("6");
  });

  it("can change the values", async function (a) {
    this.set("value", {
      classes: ["text-center"],
      color: "green",
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Properties::SourceGroupStyle @value={{this.value}} @onChange={{this.onChange}}/>`);

    this.element.querySelector(".input-sm-text").value = "end";
    await triggerEvent(".input-sm-text", "change");

    await click(".bs-color-picker.color");
    await click(".bs-color-picker.color .btn-color-warning");

    await click(".bs-color-picker.accent-color");
    await click(".bs-color-picker.accent-color .btn-color-primary");

    await fillIn(".input-max-items-sm", "4");
    await fillIn(".input-max-items-md", "6");
    await fillIn(".input-max-items-lg", "8");

    a.deepEqual(value, {
      classes: ["text-end"],
      color: "warning",
      accentColor: "primary",
      maxItems: {
        sm: 4,
        md: 6,
        lg: 8,
      },
    });
  });
});
