/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/authenticated-admin-route';
import { service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminRegistrationRoute extends Route {
  @service user;
  @service preferences;

  model() {
    return hash({
      spacesDomains: this.preferences.requestPreference('spaces.domains'),
      registerEnabled: this.preferences.requestPreference('register.enabled'),
      registerEmailDomains: this.preferences.requestPreference('register.emailDomains'),
      registerMandatory: this.preferences.requestPreference('register.mandatory'),
      registerURL: this.preferences.requestPreference('register.url'),
    });
  }
}
