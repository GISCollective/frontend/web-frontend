/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/dashboard/team-stats', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let team;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    team = server.testData.storage.addDefaultTeam();
    team.id = team._id;

    server.testData.storage.addStat(`map-views-team-${team._id}`, 20);
    server.testData.storage.addStat(`campaign-contributions-team-${team._id}`, 30);
    server.testData.storage.addStat(`campaign-answer-contributors-team-${team._id}`, 40);

    this.set('team', team);
  });

  it('renders the map views stat value', async function () {
    await render(hbs`<Manage::Dashboard::TeamStats @team={{this.team}} />`);

    const titles = this.element.querySelectorAll('.stat-title');
    const values = this.element.querySelectorAll('.stat-value');

    expect(titles[0].textContent.trim()).to.equal('Map views');
    expect(values[0].textContent.trim()).to.equal('20');
  });

  it('renders the survey contributions stat value', async function () {
    await render(hbs`<Manage::Dashboard::TeamStats @team={{this.team}} />`);

    const titles = this.element.querySelectorAll('.stat-title');
    const values = this.element.querySelectorAll('.stat-value');

    expect(titles[1].textContent.trim()).to.equal('Survey contributions');
    expect(values[1].textContent.trim()).to.equal('30');
  });

  it('renders the campaign contributors stat value', async function () {
    await render(hbs`<Manage::Dashboard::TeamStats @team={{this.team}} />`);

    const titles = this.element.querySelectorAll('.stat-title');
    const values = this.element.querySelectorAll('.stat-value');

    expect(titles[2].textContent.trim()).to.equal('Survey contributors');
    expect(values[2].textContent.trim()).to.equal('40');
  });
});
