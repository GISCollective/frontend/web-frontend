/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import Service from '@ember/service';

class MockSession extends Service {
  isAuthenticated = true;
}

class MockUser extends Service {
  isAdmin = false;
}

class MockAdminUser extends Service {
  isAdmin = true;
}

describe ('main-bar', function (hooks) {
  setupRenderingTest(hooks);

  it('contains the sign in button', async function () {
    this.set('value', 'some value');

    await render(hbs`<MainBar @value={{this.value}} @currentAction="" />`);

    expect(this.element.textContent).to.contain('sign in');
  });

  it('renders links with the public access', async function () {
    await render(hbs`<MainBar @currentAction="" />`);
    const linkTitles = [...this.element.querySelectorAll('a, .btn-link')].map((a) => a.textContent.trim());

    expect(linkTitles).to.deep.equal(['', '', 'Updates', 'FAQ', 'Terms of Service', 'Privacy Policy', 'Report issues on Gitlab', 'Email a new issue', 'sign in']);

    expect(this.element.querySelector('.dropdown-help')).to.exist;
    expect(this.element.querySelector('.dropdown-add')).not.to.exist;
    expect(this.element.querySelector('.dropdown-manage')).not.to.exist;
    expect(this.element.querySelector('.dropdown-authenticated')).not.to.exist;
    expect(this.element.querySelector('.user-admin-icon')).not.to.exist;
  });

  it('renders links with the public access', async function () {
    await render(hbs`<MainBar @currentAction="" />`);
    const linkTitles = [...this.element.querySelectorAll('a, .btn-link')].map((a) => a.textContent.trim());

    expect(linkTitles).to.deep.equal(['', '', 'Updates', 'FAQ', 'Terms of Service', 'Privacy Policy', 'Report issues on Gitlab', 'Email a new issue', 'sign in']);

    expect(this.element.querySelector('.dropdown-help')).to.exist;
    expect(this.element.querySelector('.dropdown-add')).not.to.exist;
    expect(this.element.querySelector('.dropdown-manage')).not.to.exist;
    expect(this.element.querySelector('.dropdown-authenticated')).not.to.exist;
    expect(this.element.querySelector('.user-admin-icon')).not.to.exist;
  });

  it('renders the campaigns link when showCampaigns is true', async function () {
    await render(hbs`<MainBar @currentAction="" @showCampaigns={{true}} />`);
    const linkTitles = [...this.element.querySelectorAll('a, .btn-link')].map((a) => a.textContent.trim());

    expect(linkTitles).to.contain('Surveys');
  });

  describe ('when an user without admin right is authenticated',  function (hooks) {
    hooks.beforeEach(async function () {
      this.owner.register('service:session', MockSession);
      this.owner.register('service:user', MockUser);
      await render(hbs`<MainBar @currentAction="" />`);
    });

    it('shows the publisher add pages when one of the teams is publisher', async function () {
      this.set('teams', [
        { id: '1', name: 'team 1' },
        { id: '2', name: 'team 2', isPublisher: true },
      ]);
      await render(hbs`<MainBar @teams={{this.teams}} />`);

      expect(this.element.querySelector('.dropdown-item-new-page')).to.exist;
      expect(this.element.querySelector('.dropdown-item-new-article')).to.exist;
      expect(this.element.querySelector('.dropdown-item-new-presentation')).not.to.exist;
    });

    it('renders links labels for the public and private access', async function (assert) {
      const linkTitles = [...this.element.querySelectorAll('a, .btn-link')].map((a) => a.textContent.trim());

      assert.deepEqual(linkTitles, [
        '',
        '',
        'Feature',
        'Map',
        'Team',
        'Survey',
        'Icon set',
        'Icon',
        'Base map',
        "Article",
        "Page",
        "Space",
        '',
        'Updates',
        'FAQ',
        'Terms of Service',
        'Privacy Policy',
        'Report issues on Gitlab',
        'Email a new issue',
        '',
        'dashboard',
        'preferences',
        'sign out',
      ]);
    });

    it('renders the right menu drop downs', function () {
      expect(this.element.querySelector('.dropdown-help')).to.exist;
      expect(this.element.querySelector('.dropdown-add')).to.exist;
      expect(this.element.querySelector('.dropdown-authenticated')).to.exist;
      expect(this.element.querySelector('.user-admin-icon')).not.to.exist;

      expect(this.element.querySelector('.dropdown-item-languages')).not.to.exist;
      expect(this.element.querySelector('.dropdown-item-new-language')).not.to.exist;
    });

    it('does not render the `manage` and `plus` when the allowManageWithoutTeams is false', async function (a) {
      await render(hbs`<MainBar @allowManageWithoutTeams="false" />`);
      const linkTitles = [...this.element.querySelectorAll('a, .btn-link')].map((a) => a.textContent.trim());

      a.deepEqual(linkTitles, ['', '', 'Updates', 'FAQ', 'Terms of Service', 'Privacy Policy', 'Report issues on Gitlab', 'Email a new issue', '', 'dashboard', 'preferences', 'sign out']);
    });

    it('does not render the `manage` when the allowManageWithoutTeams is true and there are no teams', async function () {
      await render(hbs`<MainBar @allowManageWithoutTeams="true" />`);
      const linkTitles = [...this.element.querySelectorAll('a, .btn-link')].map((a) => a.textContent.trim());

      expect(linkTitles).not.to.contain('Manage');
    });

    it('shows the list of teams when they are set', async function () {
      this.set('teams', [
        { id: '1', name: 'team 1' },
        { id: '2', name: 'team 2' },
      ]);
      await render(hbs`<MainBar @teams={{this.teams}} />`);
      const linkTitles = [...this.element.querySelectorAll('.dropdown-item-dashboard')].map((a) => a.textContent.trim());

      expect(linkTitles).to.deep.equal(['team 1', 'team 2']);
      expect(this.element.querySelector('.dropdown-manage .dropdown-divider')).not.to.exist;
    });

    it('shows the list of teams when they are set and allowManageWithoutTeams is false', async function () {
      this.set('teams', [
        { id: '1', name: 'team 1' },
        { id: '2', name: 'team 2' },
      ]);
      await render(hbs`<MainBar @teams={{this.teams}} @allowManageWithoutTeams="false" />`);
      const linkTitles = [...this.element.querySelectorAll('.dropdown-item-dashboard')].map((a) => a.textContent.trim());

      expect(linkTitles).to.deep.equal(['team 1', 'team 2']);
      expect(this.element.querySelector('.dropdown-manage .dropdown-divider')).not.to.exist;
    });
  });

  describe ('when an user without admin right is authenticated and it has a team',  function (hooks) {
    hooks.beforeEach(async function () {
      this.owner.register('service:session', MockSession);
      this.owner.register('service:user', MockUser);

      this.set('teams', [{}]);
      await render(hbs`<MainBar @currentAction="" @teams={{this.teams}} />`);
    });

    it('renders the right menu drop downs', function () {
      expect(this.element.querySelector('.dropdown-help')).to.exist;
      expect(this.element.querySelector('.dropdown-add')).to.exist;
      expect(this.element.querySelector('.dropdown-manage')).to.exist;
      expect(this.element.querySelector('.dropdown-authenticated')).to.exist;
      expect(this.element.querySelector('.user-admin-icon')).not.to.exist;

      expect(this.element.querySelector('.dropdown-item-languages')).not.to.exist;
      expect(this.element.querySelector('.dropdown-item-new-language')).not.to.exist;
    });
  });

  describe ('when an admin user authenticated',  function (hooks) {
    hooks.beforeEach(async function () {
      this.owner.register('service:session', MockSession);
      this.owner.register('service:user', MockAdminUser);
      await render(hbs`<MainBar @currentAction="" />`);
    });

    it('renders links with the public and private access', async function (a) {
      const linkTitles = [...this.element.querySelectorAll('a, .btn-link')].map((a) => a.textContent.trim());

      a.deepEqual(linkTitles, [
        '',
        'Manage',
        'Teams',
        'Languages',
        '',
        'Feature',
        'Map',
        'Team',
        'Survey',
        'Icon set',
        'Icon',
        'Base map',
        'Newsletter',
        'Data binding',
        'Calendar',
        'Event',
        'Article',
        'Page',
        'Space',
        'Presentation',
        'Language',
        '',
        'Updates',
        'FAQ',
        'Terms of Service',
        'Privacy Policy',
        'Report issues on Gitlab',
        'Email a new issue',
        '',
        'dashboard',
        'preferences',
        'settings',
        'sign out'
      ]);
    });

    it('the main drop downs and private access', async function () {
      expect(this.element.querySelector('.dropdown-help')).to.exist;
      expect(this.element.querySelector('.dropdown-add')).to.exist;
      expect(this.element.querySelector('.dropdown-manage')).to.exist;
      expect(this.element.querySelector('.dropdown-authenticated')).to.exist;
    });

    it('does not render the dropdown separator', function () {
      expect(this.element.querySelector('.dropdown-manage .dropdown-divider')).not.to.exist;
    });

    it('renders the user tie icon', function () {
      expect(this.element.querySelector('.fa-user-tie')).to.exist;
    });

    it('renders the new buttons', async function () {
      expect(this.element.querySelector('.dropdown-item-new-language')).to.exist;
      expect(this.element.querySelector('.dropdown-item-new-base-map')).to.exist;
    });

    it('renders the `plus` button when the allowManageWithoutTeams is false', async function () {
      await render(hbs`<MainBar @allowManageWithoutTeams="false" />`);

      expect(this.element.querySelector('.dropdown-add')).to.exist;
    });
  });
});
