import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | time-tracking-table', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a message when there is no value', async function (assert) {
    await render(hbs`<TimeTrackingTable @emptyMessage="some message" />`);

    expect(this.element.querySelector('.alert-time-tracking-empty')).to.have.textContent('some message');
  });

  it('renders a table when it has records', async function (assert) {
    this.set('value', [
      {
        hours: 2,
        date: new Date('2024-03-12T02:22:33Z'),
        details: 'some details',
      },
    ]);

    await render(hbs`<TimeTrackingTable @value={{this.value}} />`);

    const cells = this.element.querySelectorAll('.row-record td');

    expect(cells).to.have.length(3);

    expect(cells[0]).to.have.textContent('2');
    expect(cells[1]).to.have.textContent('Tue Mar 12 2024');
    expect(cells[2]).to.have.textContent('some details');
  });
});
