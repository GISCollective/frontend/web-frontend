/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ButtonComponent extends Component {
  @tracked _value;

  get value() {
    return this._value ?? this.args.value ?? {};
  }

  get space() {
    return this.args?.model?.space ?? this.args.space;
  }

  get link() {
    return this.value?.content?.link ?? this.value.link;
  }

  get name() {
    return this.value?.content?.name ?? this.value.name ?? this.args.defaultName;
  }

  get type() {
    return this.value?.content?.type ?? this.value.type;
  }

  get storeType() {
    return this.value?.content?.storeType ?? this.value.storeType;
  }

  get newTab() {
    return this.value?.content?.newTab ?? this.value.newTab;
  }

  get enablePicture() {
    return this.value?.content?.enablePicture;
  }

  get pictureId() {
    return this.value?.pictureOptions?.picture;
  }

  get classes() {
    return this.value?.style?.classes ?? this.value.classes;
  }

  get hasContent() {
    if(!this.enablePicture) {
      return !!this.name;
    }

    return !!this.name || !!this.pictureId;
  }

  @action
  updateProps(event) {
    this._value = event.detail.value;
  }
}
