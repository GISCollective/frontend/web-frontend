/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action, set } from '@ember/object';

export default class ManageCampaignAnswersEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service router;

  toQuestion(attribute) {
    return {
      question: attribute.displayName,
      type: attribute.type,
      name: attribute.name,
      isRequired: false,
      isVisible: true,
    };
  }

  get attributes() {
    if (this.model.attributes) {
      return this.model.attributes;
    }

    return this.editableModel.attributes;
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team?.get('id'),
        text: this.model.team?.get('name'),
        capitalize: true,
      },
      {
        model: this.model.campaign.get('id'),
        text: this.model.campaign.name,
        route: 'manage.dashboards.survey',
      },
      {
        text: this.model.campaignAnswer.name,
      },
    ];
  }

  get editableModel() {
    return this.model.campaignAnswer;
  }

  get campaignIcons() {
    let result = {
      categories: {},
    };

    if (this.model.campaign?.icons?.length) {
      result.categories.mandatory = {
        icons: this.model.campaign.icons.slice(),
      };
    }

    if (this.model.campaign?.optionalIcons?.length) {
      result.categories.optional = {
        icons: this.model.campaign.optionalIcons.slice(),
      };
    }

    return result;
  }

  get isDisabled() {
    return !this.editableModel.canEdit;
  }

  get attributeSections() {
    const iconsWithAttributes = this.model.answerIcons.filter((a) => a.attributes.length > 0) ?? [];

    return iconsWithAttributes.map((a) => ({
      name: a.name,
      list: a.attributes.map((a) => this.toQuestion(a)),
      value: this.attributes[a.name],
    }));
  }

  @action
  saveField(field, value) {
    this.editableModel[field] = value;

    return this.save();
  }

  @action
  save() {
    this.model.attributes = this.editableModel.attributes;

    return this.editableModel.save();
  }

  @action
  async updateSounds(sounds) {
    this.savingField = this.editableField;

    const items = sounds.map((a) => a.save());
    try {
      await Promise.all(items);
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.editableModel.sounds = sounds;
    await this.editableModel.save();

    this.editableField = '';
    this.savingField = '';
  }

  @action
  async iconsChanged(value) {
    this.model.answerIcons = value;
    this.editableModel.icons = value;
    this.editableField = 'icons';

    return this.save();
  }

  @action
  async updatePictures(pictures) {
    this.savingField = this.editableField;

    const items = pictures.map((a) => a.save());
    try {
      await Promise.all(items);
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.editableModel.pictures = pictures;
    return this.save();
  }

  // @action
  saveField(name, value) {
    set(this.editableModel, name, value);

    return this.save();
  }

  @action
  saveArticle(title, content) {
    this.editableModel.title = title;
    this.editableModel.content = content;

    return this.save();
  }

  @action
  saveAttribute(name, value) {
    return this.saveField(`attributes.${name}`, value);
  }
}
