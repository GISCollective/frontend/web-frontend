/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './calendar-attribute';
import { action } from '@ember/object';
import { cancel, later } from '@ember/runloop';

export default class StateFiltersFeatureAttributeComponent extends Component {
  @action
  search(term) {
    cancel(this.queryTimer);
    if (!term || term.length < 3) {
      return;
    }

    this.queryTimer = later(async () => {
      try {
        this.results = await this.parentRecord?.getAttributeValues(
          this.attributeName,
          term,
          10,
        );
      } catch (err) {
        console.log(err);
        this.results = [];
      }
    }, 500);
  }

  @action
  async setup() {
    const mapId = this.args.options?.map?.id || this.args.options?.map;

    if (!mapId) {
      return;
    }

    try {
      this.parentRecord = await this.store.findRecord('map', mapId);
      this.results = await this.parentRecord.getAttributeValues(
        this.attributeName,
        '',
        this.hasSearch ? 5 : 50,
      );
    } catch (err) {
      console.log(err);
      this.results = [];
    }
  }
}
