/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Model | team', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('article', {});
    expect(model).to.be.ok;
  });

  it('fills in the meta info object', async function () {
    let store = this.owner.lookup('service:store');

    let model = store.createRecord('team', {
      isPublic: true,
      members: [],
      owners: [],
      _id: '5ca88c9cecd8490100caba81',
      about: 'A Greenmap shows locations i.e. a place where you can get eco-products and services.',
      leaders: [],
      name: 'Greenmap Berlin',
      guests: [],
      isDefault: false,
      pictures: [],
    });

    model.logo = store.createRecord('picture', {
      _id: '5ca89e37ef1f7e010007f333',
      picture: 'http://some-url.com',
    });

    const metaInfo = await model.fillMetaInfo({});

    expect(metaInfo).to.deep.equal({
      title: 'Greenmap Berlin',
      description: `A Greenmap shows locations i.e. a place where you can get eco-products and services.`,
      imgSrc: 'http://some-url.com/lg',
    });
  });
});
