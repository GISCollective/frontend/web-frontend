/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class NewRecordFormSpaceComponent extends Component {
  @service user;
  @tracked _team;
  @tracked template;

  get teamList() {
    if (this.args.teams?.toArray) {
      return this.args.teams.toArray();
    }

    if (Array.isArray(this.args.teams)) {
      return this.args.teams;
    }

    return [];
  }

  get canSubmit() {
    return this.editableModel?.name && this.editableModel?.domain;
  }

  get editableModel() {
    return this.args.space;
  }

  get showSpaces() {
    return this.user.isAdmin || this.args.spaces?.length > 1;
  }

  get team() {
    if (this._team) {
      return this._team;
    }

    return this.teamList[0];
  }

  get showTeamSection() {
    return this.teamList.length > 1;
  }

  get models() {
    const defaultModels = this.template?.defaultModels?.toJSON?.() ?? [];

    return Object.entries(defaultModels)
      .filter((a) => a[1])
      .map((a) => a[0]);
  }

  @action
  change(field, value) {
    this.editableModel?.set(field, value);
  }

  @action
  changeTeam(value) {
    this._team = value;
  }

  @action
  changeTemplate(value, space) {
    this.args.space.template = value;
    this.template = space;
  }

  @action
  setup() {
    if (this.args.spaces?.length && this.args.page && !this.args.page?.space) {
      this.args.page.space = this.args.spaces[0];
      this.hasSpace = true;
    }
  }

  @action
  save() {
    const pieces = this.args.space.domain.split('.');

    this.args.space.slug = pieces[0];
    this.args.space.visibility = {};
    this.args.space.visibility.team = this.team;
    this.args.space.visibility.isPublic = false;
    this.args.space.visibility.isDefault = false;

    return this.args.onSubmit(this.args.space);
  }
}
