/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click, typeIn, fillIn, waitFor } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';

describe.skip('Acceptance | browse/maps/map-view/icon-set', function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    let space = server.testData.storage.addDefaultSpace();
    space.landingPageId = '';
    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultPage('000000000000000000000001');
    server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
    server.testData.storage.addDefaultIconSet('1');
    server.testData.storage.addDefaultIconSet('2');

    space.landingPageId = '';

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('when the server returns icons', function (hooks) {
    hooks.beforeEach(function () {
      let icon1 = server.testData.storage.addDefaultIcon('1');
      server.testData.storage.addDefaultIcon('2');

      icon1.name = 'some icon';
      icon1.localName = 'some icon';
    });

    it('shows a list of icon sets', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');

      expect(this.element.querySelector('.icon-set-wrapper')).to.exist;
      expect(this.element.querySelector('.icon-name')).to.exist;
      expect(this.element.querySelector('.btn-spotlight-back')).to.exist;
      expect(this.element.querySelector('h5.icon-set-title').textContent.trim()).to.equal(
        'Green Map® Icons Version 3',
      );
    });

    it('does not change the path when the search input is focused', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');
      await click('.input-search');

      expect(this.element.querySelector('.icon-set-wrapper')).to.exist;
    });

    it('selects an icon when clicked', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');

      await click('.btn-icon');

      expect(currentURL()).to.contain('?icons=1');
    });

    it('deselects the prev icon when a new one is clicked', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?icons=1');

      const icons = this.element.querySelectorAll('.icon-selector .btn-icon');

      await click(icons[1]);

      expect(currentURL()).to.contain('?icons=2');
    });

    it('keeps the prev icon when a new one is clicked and filterManyIcons=true', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?icons=1&filterManyIcons=true');

      const icons = this.element.querySelectorAll('.icon-selector .btn-icon');

      await click(icons[1]);

      expect(currentURL()).to.contain('?filterManyIcons=true&icons=1%2C2');
    });

    it('removes only one icon when one is deselected and filterManyIcons=true', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?icons=1%2C2&filterManyIcons=true');

      const icons = this.element.querySelectorAll('.icon-selector .btn-icon');

      await click(icons[1]);

      expect(currentURL()).to.contain('?filterManyIcons=true&icons=1');
    });

    it('navigates to the spotlight when the btn back button is pressed', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');
      await waitFor('.btn-spotlight-back');
      await click('.btn-spotlight-back');

      expect(currentURL()).to.equal('/browse/maps/_/map-view/spotlight');
    });

    it('filters icons by name', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');

      await fillIn('.input-search', 'some i');

      expect(this.element.querySelectorAll('.icon-name').length).to.equal(1);
      expect(this.element.querySelector('.icon-name').textContent.trim()).to.equal('some icon');

      expect(currentURL()).to.equal('/browse/maps/_/map-view/icon-set/1?search=some%20i');
    });

    it('hides the menu and fills in the search term when is loaded', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?search=some%20i');

      await waitFor('.bar-invisible');

      expect(this.element.querySelector('.input-search').value).to.equal('some i');
      expect(this.element.querySelectorAll('.icon-name').length).to.equal(1);
      expect(this.element.querySelector('.icon-name').textContent.trim()).to.equal('some icon');
    });

    it.skip('removes the term when going back', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?search=some%20i');

      await fillIn('.input-search', '');
      await typeIn('.input-search', 'some i');

      await waitFor('.btn-spotlight-back');
      await click('.btn-spotlight-back');

      expect(this.element.querySelector('.input-search').value).to.equal('');
      expect(currentURL()).to.equal('/browse/maps/_/map-view/spotlight');
    });

    it('removes the term when an icon is selected', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?search=some%20i');

      await fillIn('.input-search', 'some i');
      await click('.btn-icon');

      expect(this.element.querySelector('.input-search').value).to.equal('');
      expect(currentURL()).to.equal('/browse/maps/_/map-view?icons=1');
    });
  });
});
