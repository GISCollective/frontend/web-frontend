import Component from '@glimmer/component';
import { action } from '@ember/object';
import { buildWaiter } from '@ember/test-waiters';

let waiter = buildWaiter('input-text-file-waiter');

export default class InputTextFileComponent extends Component {
  @action
  readFile(event) {
    const selectedFile = event.target.files[0];

    let token = waiter.beginAsync();
    const reader = new FileReader();
    reader.onload = async (e) => {
      try {
        await this.args.onChange?.(atob(e.target.result.split('base64,')[1]));
      } catch (err) {
        console.error(err);
      } finally {
        waiter.endAsync(token);
      }
    };

    reader.onerror = () => {
      waiter.endAsync(token);
    };

    reader.readAsDataURL(selectedFile);
  }
}
