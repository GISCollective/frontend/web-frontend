/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorPictureWithOptionsComponent extends Component {
  @tracked _size = null;
  @tracked _container = null;

  get size() {
    if (this._size) {
      return this._size;
    }

    return this.args.value?.size ?? {};
  }

  get container() {
    if (this._container) {
      return this._container;
    }

    return this.value.container ?? {};
  }

  get value() {
    return this.args.value ?? {};
  }

  triggerChange() {
    const data = {
      size: this.size,
      container: this.container,
    };

    this.args.onChange?.(data);
  }

  @action
  changeContainer(value) {
    this._container = value;
    this.triggerChange();
  }

  @action
  changeSize(value) {
    this._size = value;
    this.triggerChange();
  }
}
