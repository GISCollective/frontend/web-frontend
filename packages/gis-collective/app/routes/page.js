/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from './base/app-route';
import { service } from '@ember/service';
import { toPageId, toPageSlug } from 'core/lib/slug';
import { PageModel } from '../lib/page-model';

export default class PageRoute extends Route {
  @service store;
  @service space;
  @service tracking;

  knownController = {};

  queryParams = {
    s: {
      refreshModel: true,
      replace: true,
    },
  };

  toPageSlug() {
    return toPageSlug(...arguments);
  }

  toPageId(id) {
    return toPageId(id, this.space);
  }

  getPathFromTransition(transition, params) {
    let path = transition?.intent?.url || params?.wildcard;

    if (typeof path != 'string') {
      return { page: null };
    }

    const questionMarkIndex = path.indexOf('?');

    if (questionMarkIndex != -1) {
      path = path.substring(0, questionMarkIndex);
    }

    if (!path.startsWith('/')) {
      path = `/${path}`;
    }

    return path;
  }

  async beforeModel(transition) {
    if(this.space.summary && !this.space.summary.isPublic && !this.space.currentSpace) {
      this.router.transitionTo('login.index', {
        queryParams: {
          redirect: transition?.intent?.url,
        },
      });

      return;
    }

    return super.beforeModel(...arguments);
  }

  async model(params, transition) {
    let path = this.getPathFromTransition(transition, params);
    const redirect = this.space.currentSpace?.redirects?.value?.[path];

    if (redirect) {
      return {
        type: "redirect",
        redirect
      }
    }

    const pageModel = await PageModel.getPageModelFromPath(path, params.s, this.store, this.space.currentSpace, this.router, this.tracking);

    const isSamePage = transition.from?.params?.wildcard == transition.to?.params?.wildcard;

    this.knownController.preserveScrollPosition = isSamePage;

    if (!pageModel && this.space.has404) {
      this.router.transitionTo("/404");
    }

    if(pageModel?.main?.data?.changeCount == 1) {
      await this.tracking.addView("page", pageModel?.main?.pageId);
    }

    return pageModel;
  }

  async afterModel(model) {
    try {
      await this.space.currentSpace?.logo;

      await model?.main?.data?.selectedModel?.loadRelations?.();

      if (typeof model?.main?.data?.selectedModel?.fillMetaInfo == 'function') {
        await model.main.data.selectedModel.fillMetaInfo(this.headData);
        return;
      }

      if (typeof model?.main?.page?.fillMetaInfo == 'function') {
        await model.main.page.fillMetaInfo(this.headData);
        return;
      }
    } catch (err) {
      console.error("Can't set the page meta headers.", err);
    }
  }

  setupController(controller, model) {
    super.setupController(controller, model);
    this.knownController = controller;
    controller.space = this.space;

    controller.set('preserveScrollPosition', this.space.currentLocation.includes('#'));
  }
}
