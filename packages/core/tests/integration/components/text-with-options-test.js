/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render, settled } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | text-with-options', function (hooks) {
  setupRenderingTest(hooks);

  it('updates the link to open in a new tab', async function () {
    await render(hbs`<TextWithOptions>
      <a href="giscollective.com">link</a>
    </TextWithOptions>`);

    expect(this.element.querySelector('a')).to.have.attribute('target', '_blank');
  });

  it('does not show a placeholder when the content is empty in view mode', async function () {
    await render(hbs`<TextWithOptions></TextWithOptions>`);

    expect(this.element.querySelector('p')).not.to.have.class('empty');
  });

  it('shows a placeholder when the content is empty in edit mode', async function () {
    await render(hbs`<TextWithOptions @isEditor={{true}}></TextWithOptions>`);

    expect(this.element.querySelector('p')).to.have.class('empty');
  });

  it('shows a placeholder when the content is empty after the first render in edit mode', async function () {
    this.set('visible', true);

    await render(hbs`<TextWithOptions @isEditor={{true}}>
      {{#if this.visible}}hello{{/if}}
    </TextWithOptions>`);

    expect(this.element.querySelector('p')).not.to.have.class('empty');

    this.set('visible', false);
    await settled();

    expect(this.element.querySelector('p')).to.have.class('empty');
  });
});
