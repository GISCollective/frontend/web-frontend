/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later, debounce } from '@ember/runloop';

export default class InputIconsParentIconComponent extends Component {
  @tracked _value;
  @tracked renderList = true;

  @action
  searchTask(term) {
    if (!term || term.length <= 3) {
      return [];
    }

    return this.args.store.query('icon', { term: term });
  }

  @action
  search(term) {
    debounce(this, this.searchTask, term, 200);
  }

  get value() {
    if (this._value === null) {
      return null;
    }

    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  handleChange(icon) {
    this._value = icon;
    this.args.onChange?.(icon);
  }

  @action
  removeIcon() {
    this._value = null;
    this.args.onChange?.();
    this.renderList = false;

    later(() => {
      this.renderList = true;
    });
  }
}
