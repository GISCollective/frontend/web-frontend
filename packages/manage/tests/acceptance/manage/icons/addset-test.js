/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, fillIn, triggerEvent, waitUntil, waitFor } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/icons/add-icon-set', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedIconSet;
  let iconSet;

  hooks.beforeEach(function () {
    server = new TestServer();
    iconSet = server.testData.create.iconSet();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();

    server.post('/mock-server/iconsets', (request) => {
      server.testData.storage.addDefaultIconSet();

      receivedIconSet = JSON.parse(request.requestBody);
      receivedIconSet.iconSet._id = iconSet._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedIconSet)];
    });

    receivedIconSet = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('when there is a team available', function (hooks) {
    let team;

    hooks.beforeEach(function () {
      team = server.testData.create.team();
      server.testData.storage.addTeam(team);
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);
      expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Ficons%2Fadd');
    });

    it('can visit /manage/icons/add', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      expect(currentURL()).to.equal('/manage/icons/add');
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('New icon set');

      expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to.exist;

      expect(this.element.querySelector('.row-description label').textContent.trim()).to.contain('Description');
      expect(this.element.querySelector('.row-description .editor-js')).to.exist;
      expect(this.element.querySelector('.ce-paragraph').textContent).to.contain(
        'This is our new icon set designed to be used in our new projects.',
      );

      expect(this.element.querySelector('.row-team label').textContent.trim()).to.contain('Team');
      expect(this.element.querySelector('.row-team select')).to.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;

      expect(this.element.querySelector('.alert-danger .btn-add-team')).not.to.exist;
      expect(this.element.querySelector('.alert-danger')).not.to.exist;
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'New icon set']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'New icon set']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
      ]);
    });

    it('does not make a "/iconsets/categories" request when navigating from the add page to Browse', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);

      await visit('/browse');

      expect(server.history).to.not.contain('GET /mock-server/iconsets/categories');
    });

    it('can create an icon set', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      expect(currentURL()).to.equal('/manage/icons/add');
      await fillIn('.row-name input.form-control', 'test name');
      await fillIn('.ce-paragraph', 'test description');

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      await PageElements.wait(1000);
      await click('.btn-submit');
      await PageElements.waitEditorJs(this.element);

      expect(receivedIconSet.iconSet.description.blocks).to.deep.equal([
        {
          type: 'header',
          data: {
            text: 'test name',
            level: 1,
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'test description',
          },
        },
      ]);

      expect(receivedIconSet.iconSet).to.deep.equal({
        _id: iconSet._id,
        cover: null,
        name: 'test name',
        sprites: {},
        description: {
          blocks: [
            {
              type: 'header',
              data: {
                text: 'test name',
                level: 1,
              },
            },
            {
              type: 'paragraph',
              data: {
                text: 'test description',
              },
            },
          ],
        },
        visibility: { isDefault: false, isPublic: false, team: team._id },
      });

      expect(currentURL()).to.equal('/manage/icons/edit/' + iconSet._id);
    });

    it('can create an icon set without changing the default description', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      expect(currentURL()).to.equal('/manage/icons/add');
      await fillIn('.row-name input.form-control', 'test name');

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      await PageElements.wait(1000);

      await click('.btn-submit');
      await PageElements.waitEditorJs(this.element);

      expect(receivedIconSet.iconSet.description.blocks).to.deep.equal([
        {
          type: 'header',
          data: {
            text: 'test name',
            level: 1,
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'This is our new icon set designed to be used in our new projects.',
          },
        },
      ]);

      expect(receivedIconSet.iconSet).to.deep.equal({
        _id: iconSet._id,
        cover: null,
        name: 'test name',
        sprites: {},
        description: {
          blocks: [
            {
              type: 'header',
              data: {
                text: 'test name',
                level: 1,
              },
            },
            {
              type: 'paragraph',
              data: {
                text: 'This is our new icon set designed to be used in our new projects.',
              },
            },
          ],
        },
        visibility: { isDefault: false, isPublic: false, team: team._id },
      });

      expect(currentURL()).to.equal('/manage/icons/edit/' + iconSet._id);
    });

    it('has the Add button disabled when there is no name set', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal('/manage/icons/add');

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      expect(this.element.querySelector('.btn.btn-submit')).to.have.class('disabled');
      expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');
    });

    it('has the Add button disabled when there is no team selected', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal('/manage/icons/add');

      await fillIn('.row-name input.form-control', 'test name');

      expect(this.element.querySelector('.btn.btn-submit')).to.have.class('disabled');
      expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');
    });

    it('should load only teams that the user can edit', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);
      await waitUntil(() => server.history.filter((a) => a.indexOf('teams') != -1).length > 0);
      expect(server.history).to.contain('GET /mock-server/teams?all=false&edit=true');
    });
  });

  describe('when no team is available', function (hooks) {
    it('should show a button to guide the user to create a team first', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to.exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=manage.icons.add');
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await PageElements.waitEditorJs(this.element);

      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=manage.icons.add');
    });
  });
});
