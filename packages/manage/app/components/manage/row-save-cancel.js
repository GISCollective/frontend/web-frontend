/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageRowSaveCancelComponent extends Component {
  @tracked _isDisabled;

  get isDisabled() {
    if (this._isDisabled) {
      return true;
    }

    if (this.args.ignoreValidation) {
      return false;
    }

    if (this.args.isDisabled) {
      return this.args.isDisabled;
    }

    if (!this.args.model) {
      return false;
    }

    return !this.args.model.hasDirtyAttributes;
  }

  @action
  async onSave() {
    this._isDisabled = true;

    let result;

    try {
      result = await this.args.onSave(...arguments);
    } catch (err) {
      throw err;
    } finally {
      this._isDisabled = false;
    }

    return result;
  }
}
