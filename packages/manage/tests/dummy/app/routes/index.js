/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';

export default class ApplicationRoute extends Route {
  queryParams = {
    redirect: {},
  };
}
