/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | view/categories', function (hooks) {
  setupRenderingTest(hooks);

  it('uses the data object when it has a page col value', async function () {
    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
        button: {
          classes: ['fw-bold', 'text-size-09'],
        },
      },
    });

    await render(
      hbs`<View::Categories @isEditor={{true}} @value={{this.value}} @path="cols.9" />`,
    );

    expect(this.element.querySelector('.group-selector')).to.have.attribute(
      'data-path',
      'cols.9.data.container',
    );
    expect(
      this.element.querySelector('.btn:not(.btn-selected)'),
    ).to.have.attribute('data-path', 'cols.9.data.button');
    expect(this.element.querySelector('.btn-selected')).to.have.attribute(
      'data-path',
      'cols.9.data.activeButton',
    );
    expect(this.element.querySelector('[data-type=text]')).to.have.attribute(
      'data-path',
      'cols.9.data.categoryList.0',
    );
  });

  it('does not use the data object when it has a data property as path', async function () {
    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
        button: {
          classes: ['fw-bold', 'text-size-09'],
        },
      },
    });

    await render(
      hbs`<View::Categories @isEditor={{true}} @value={{this.value}} @path="cols.9.data.categories" />`,
    );

    expect(this.element.querySelector('.group-selector')).to.have.attribute(
      'data-path',
      'cols.9.data.categories.container',
    );
    expect(
      this.element.querySelector('.btn:not(.btn-selected)'),
    ).to.have.attribute('data-path', 'cols.9.data.categories.button');
    expect(this.element.querySelector('.btn-selected')).to.have.attribute(
      'data-path',
      'cols.9.data.categories.activeButton',
    );
    expect(this.element.querySelector('[data-type=text]')).to.have.attribute(
      'data-path',
      'cols.9.data.categories.categoryList.0',
    );
  });

  it('renders the categories', async function () {
    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
        button: {
          classes: ['fw-bold', 'text-size-09'],
        },
      },
    });

    await render(hbs`<View::Categories @value={{this.value}} />`);

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons).to.have.length(2);
    expect(buttons[0].innerText.trim()).to.equal('category 1');
    expect(buttons[1].innerText.trim()).to.equal('category 2');
  });

  it('renders the selected category', async function (a) {
    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
        categoryStyle: {
          color: 'green',
          hoverColor: 'red',
          activeColor: 'blue',
          classes: ['fw-bold', 'text-size-09'],
        },
      },
    });

    this.set('state', 'cn@category 1');

    await render(
      hbs`<View::Categories @value={{this.value}} @state={{this.state}} />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons[0]).to.have.classes('btn btn-group-name btn-selected');
  });

  it('renders no selected category when none is selected', async function (a) {
    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
        categoryStyle: {},
      },
    });

    this.set('state', '');

    await render(
      hbs`<View::Categories @value={{this.value}} @state={{this.state}} />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons[0]).not.to.have.classes('btn-selected');
  });

  it('renders the first category as selected when it is an editor', async function (a) {
    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
        categoryStyle: {
          color: 'green',
          hoverColor: 'red',
          activeColor: 'blue',
          classes: ['fw-bold', 'text-size-09'],
        },
      },
    });

    this.set('state', 'cn@category 1');

    await render(
      hbs`<View::Categories @isEditor={{true}} @value={{this.value}} @state={{this.state}} />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons[0]).to.have.classes('btn btn-group-name btn-selected');
  });

  it('renders nothing when it is an editor and it has no category', async function (a) {
    this.set('value', {
      data: {
        categoryList: [],
        categoryStyle: {
          color: 'green',
          hoverColor: 'red',
          activeColor: 'blue',
          classes: ['fw-bold', 'text-size-09'],
        },
      },
    });

    await render(
      hbs`<View::Categories @isEditor={{true}} @value={{this.value}}  />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons).to.have.length(0);
  });

  it('renders an empty button when there is a undefined category', async function (a) {
    this.set('value', {
      data: {
        categoryList: [undefined],
        categoryStyle: {
          color: 'green',
          hoverColor: 'red',
          activeColor: 'blue',
          classes: ['fw-bold', 'text-size-09'],
        },
      },
    });

    await render(
      hbs`<View::Categories @isEditor={{true}} @value={{this.value}} />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    expect(buttons).to.have.length(1);
    expect(buttons[0].textContent.trim()).to.equal('---');
  });

  it('triggers on change state event when a category is pressed', async function (a) {
    this.set('state', 'cn@category 1');
    let value;

    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
      },
    });

    await render(
      hbs`<View::Categories @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    await click(buttons[1]);

    expect(value).to.equal('cn@category 2');
  });

  it('triggers an event with the default category on the first render', async function (a) {
    let value;

    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    this.set('value', {
      data: {
        defaultCategory: { value: 'category 2' },
        categoryList: ['category 1', 'category 2'],
      },
    });

    await render(
      hbs`<View::Categories @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    expect(value).to.equal('cn@category 2');
  });

  it('does not trigger an event with the default category on the first render when a model is using that category', async function (a) {
    let value;

    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    this.set('value', {
      data: {
        defaultCategory: { value: 'category 2' },
        categoryList: ['category 1', 'category 2'],
      },
    });

    this.set('model', {
      articleList: {
        query: { category: 'category 1', sortBy: 'order' },
      },
    });

    await render(
      hbs`<View::Categories @model={{this.model}} @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    expect(value).not.to.exist;
  });

  it('does not trigger an event with the default category when skip setup is true', async function (a) {
    let value;

    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    this.set('value', {
      data: {
        defaultCategory: { value: 'category 2' },
        categoryList: ['category 1', 'category 2'],
      },
    });

    await render(
      hbs`<View::Categories @skipSetup={{true}} @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    expect(value).not.to.exist;
  });

  it('triggers nothing on render when there is no default value', async function (a) {
    let value;

    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    this.set('value', {
      data: {
        categoryList: ['category 1', 'category 2'],
      },
    });

    await render(
      hbs`<View::Categories @value={{this.value}} @state={{this.state}} @onChangeState={{this.onChangeState}} />`,
    );

    expect(value).not.to.exist;
  });
});
