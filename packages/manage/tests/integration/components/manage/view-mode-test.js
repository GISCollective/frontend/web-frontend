import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | manage/view-mode', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the selected list button', async function (assert) {
    this.set('value', {
      viewMode: 'list',
    });

    await render(hbs`<Manage::ViewMode @value={{this.value}}/>`);

    expect(this.element.querySelector('.btn-list')).to.have.class('btn-primary');
    expect(this.element.querySelector('.btn-grid')).not.to.have.class('btn-primary');
  });

  it('renders the selected grid button', async function (assert) {
    this.set('value', {
      viewMode: 'grid',
    });

    await render(hbs`<Manage::ViewMode @value={{this.value}}/>`);

    expect(this.element.querySelector('.btn-list')).not.to.have.class('btn-primary');
    expect(this.element.querySelector('.btn-grid')).to.have.class('btn-primary');
  });

  it('can change the selected option on click', async function (assert) {
    this.set('value', {
      viewMode: 'grid',
    });

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Manage::ViewMode @onChange={{this.change}} @value={{this.value}}/>`);

    await click('.btn-list');

    expect(value).to.deep.equal({
      viewMode: 'list',
    });
  });
});
