import { fromPageState, toPageState } from 'core/lib/page-state';
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class PageViewComponent extends Component {
  @service headData;

  get state() {
    return fromPageState(this.s);
  }

  @action
  closePanel() {
    const newState = {
      ...this.state,
      fid: undefined,
    };

    this.args.onChangeState?.(toPageState(newState));
  }

  get hasMapView() {
    const cols = this.args.model.main?.page?.cols ?? [];

    const hasCol = cols.find((a) => a.type == 'map/map-view');

    if (hasCol) {
      return true;
    }

    return !!this.args.model.panel?.page?.id;
  }

  get redirectCol() {
    return {
      type: 'redirect',
      data: {
        redirect: {
          destination: this.args.model.redirect,
        },
      },
    };
  }
}
