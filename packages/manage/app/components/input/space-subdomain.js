/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class InputSpaceSubdomainComponent extends Component {
  @service preferences;
  @tracked domainList;
  @tracked _value;
  @tracked _domain;

  get valueDomain() {
    const pieces = this.args.value?.trim().toLowerCase().split('.') ?? [];

    if (pieces.length <= 2) {
      return pieces.join('.');
    }

    return pieces.slice(1).join('.');
  }

  get hasInvalidDomain() {
    return this.valueDomain !== '' && !this.domainList?.includes(this.valueDomain);
  }

  get hasInvalidSubdomain() {
    return this.value != '' && !this.value.match(/^[0-9a-z\-]+$/);
  }

  get domain() {
    if (typeof this._domain == 'string') {
      return this._domain;
    }

    if (!this.domainList?.length) {
      return '';
    }

    return this.domainList[0];
  }

  get hasDomainList() {
    return this.domainList?.length > 1;
  }

  get selectedDomain() {
    if (!this.domainList?.length) {
      return '';
    }

    return this.domainList[0];
  }

  get value() {
    if (typeof this._value == 'string') {
      return this._value;
    }

    const pieces = this.args.value?.trim().toLowerCase().split('.');

    if (!pieces?.length) {
      return '';
    }

    return pieces[0];
  }

  set value(newValue) {
    this._value = newValue;

    this.triggerChange();
  }

  triggerChange() {
    return this.args.onChange?.(`${this.value}.${this.domain}`);
  }

  @action
  domainChanged(newValue) {
    this._domain = newValue;

    this.triggerChange();
  }

  @action
  async setup() {
    const domainPreference = await this.preferences.getPreference('spaces.domains');

    this.domainList = domainPreference.value
      .split(',')
      .map((a) => a.trim().toLowerCase())
      .map((a) => a);
  }
}
