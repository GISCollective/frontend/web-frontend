/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { moduleFor, test } from 'ember-qunit';

moduleFor('service:editor-js', 'Unit | Service | editor-js', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

// TODO: Replace this with your real tests.
test('it exists', function (assert) {
  let service = this.subject();
  assert.ok(service);
});
