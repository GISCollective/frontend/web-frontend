/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/data-binding/type',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the selected options', async function () {
    await render(hbs`<Input::DataBinding::Type @value="airtable" />`);

    expect(this.element.querySelector('.select-type').value).to.equal(
      'airtable'
    );
  });

  it('triggers onChange when a new type is selected', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::DataBinding::Type @team={{this.team}} @onChange={{this.change}}/>`
    );

    this.element.querySelector('.select-type').value = 'google sheet';
    await triggerEvent('.select-type', 'change');

    expect(value).to.equal('google sheet');
  });
});
