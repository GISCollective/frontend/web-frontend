/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';
import { service } from '@ember/service';

export default class SpaceAdapter extends Adapter {
  @service store;

  queryRecord(store, schema, query, options) {
    if (typeof query == 'string') {
      const url = this.buildURL('space', query);
      return this.ajax(url, 'GET');
    }

    return super.queryRecord(...arguments);
  }

  verifyDomain(id) {
    const url = this.buildURL('space', id) + '/verify';
    return this.ajax(url, 'POST');
  }

  getPagesMap(id) {
    const url = this.buildURL('space', id) + '/pages';
    return this.ajax(url, 'GET');
  }

  categories(id) {
    const url = this.buildURL('space', id) + '/categories';
    return this.ajax(url, 'GET');
  }

  async fetchSummary(domain) {
    const url = this.buildURL('space', '_') + `/summary?domain=${domain}`;
    const response = await this.ajax(url, 'GET');

    return response.spaceSummary;
  }
}
