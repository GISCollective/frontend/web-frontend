/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import VectorSource from 'ol/source/Vector';
import Vector from 'ol/layer/Vector';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class MapLayerVectorComponent extends Component {
  @tracked vectorSource;
  @tracked vectorLayer;
  @tracked map;

  @action
  setup() {
    if (!this.args.map) {
      return;
    }

    this.map = this.args.map;
    this.vectorSource = new VectorSource({});
    this.vectorLayer = new Vector({
      source: this.vectorSource,
      renderMode: 'vector',
      useSpatialIndex: true,
      style: this.args.style,
    });

    this.vectorLayer.on('change', () => {
      this.args.onChange?.(this.vectorLayer);
    });

    this.args.map.addLayer(this.vectorLayer);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (!this.map) {
      return;
    }

    this.map.removeLayer(this.vectorLayer);
  }
}
