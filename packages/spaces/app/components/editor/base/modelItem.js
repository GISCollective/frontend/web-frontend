/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { guidFor } from "@ember/object/internals";

export default class ManageEditorsBaseModelItemComponent extends Component {
  elementId = `manage-editors-${guidFor(this)}`;

  @tracked _source;

  get source() {
    if (this._source) {
      return this._source;
    }

    return this.args.value?.data?.source ?? {};
  }

  @action
  changeSource(value) {
    this._source = value;

    return this.triggerChange?.();
  }
}
