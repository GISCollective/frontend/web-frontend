import { buildWaiter } from '@ember/test-waiters';
import { modifier } from 'ember-modifier';
import { later } from '@ember/runloop';

let waiter = buildWaiter('dom-changed-waiter');

export default modifier(function domChanged(element, [fn, ...args]) {
  if (!MutationObserver || !fn) {
    return () => {};
  }

  const mutationObserver = new MutationObserver(() => {
    let token = waiter.beginAsync();

    later(async () => {
      try {
        await fn(element, args);
      } catch (err) {
        console.error(err);
      } finally {
        waiter.endAsync(token);
      }
    });
  });

  // have the observer observe for changes in children
  mutationObserver.observe(element, { childList: true, subtree: true, characterData: true });

  return () => {
    mutationObserver.disconnect();
  };
});
