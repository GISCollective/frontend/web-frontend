/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { service } from "@ember/service";
import { getGeometryExtent } from "../../lib/extents";
import { getBottomLeft, getBottomRight, getTopLeft, getTopRight } from "ol/extent";

export default class ManageEditorsGeometryComponent extends Component {
  @service store;
  @tracked baseMaps = [];
  @tracked _value;
  @tracked extent;

  getExtent() {
    const extent = getGeometryExtent(this.value);

    if (!extent) {
      return null;
    }

    let point1 = getBottomLeft(extent);
    let point2 = getBottomRight(extent);
    let point3 = getTopRight(extent);
    let point4 = getTopLeft(extent);

    return {
      type: "Polygon",
      coordinates: [[point1, point2, point3, point4, point1]],
    };
  }

  get value() {
    if (!this._value) {
      return this.args.value;
    }

    return this._value;
  }

  @action
  async setup() {
    this.baseMaps = await this.store.query("base-map", { default: true });
    this.extent = this.getExtent();
  }

  @action
  change(value) {
    this._value = value;
    this.args.onChange?.(value);
  }
}
