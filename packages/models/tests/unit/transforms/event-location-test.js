/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';

import { setupTest, it } from 'dummy/tests/helpers';

module('Unit | Transform | event location', function (hooks) {
  setupTest(hooks);
  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:event-location');
  });

  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('transforms an undefined', function (a) {
    let deserialized = transform.deserialize(undefined);

    a.deepEqual(deserialized.toJSON(), {
      type: 'text',
      value: '',
    });
  });

  it('transforms an object', function (a) {
    let deserialized = transform.deserialize({
      type: 'Feature',
      value: 'feature_id',
    });

    a.deepEqual(deserialized.toJSON(), {
      type: 'Feature',
      value: 'feature_id',
    });
  });

  it('serializes an object', function (a) {
    let deserialized = transform.deserialize({
      type: 'Feature',
      value: 'feature_id',
    });

    a.deepEqual(transform.serialize(deserialized), {
      type: 'Feature',
      value: 'feature_id',
    });
  });
});
