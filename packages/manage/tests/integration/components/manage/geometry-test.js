/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, click, waitUntil, typeIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/geometry', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Manage::Geometry />`);
    await waitUntil(() => this.element.querySelector('.map').olMap);
  });

  it('renders the input geometry component in edit mode', async function () {
    await render(hbs`<Manage::Geometry @editablePanel="geometry"/>`);

    expect(this.element.querySelector('.input-geometry')).to.exist;
  });

  it('calls the on edit event', async function () {
    let value;

    this.set('handleEdit', function (v) {
      value = v;
    });

    await render(hbs`<Manage::Geometry @onEdit={{this.handleEdit}}/>`);
    await click('.btn-edit');

    expect(value).to.equal('geometry');
  });

  it('calls the on save event', async function () {
    let value;

    this.set('handleSave', (v) => {
      value = v;
      this.set('editablePanel', '');
    });

    this.set('editablePanel', 'geometry');

    await render(hbs`<Manage::Geometry @onSave={{this.handleSave}} @editablePanel={{this.editablePanel}} />`);
    await waitUntil(() => this.element.querySelector('.map').olMap);

    await click('.btn-paste');

    const editor = this.element.querySelector('.input-geo-json');
    editor.value = '';
    await typeIn(editor, `{ "type": "Point", "coordinates": [1, 2] }`);

    await click('.btn-submit');

    expect(value).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });
  });
});
