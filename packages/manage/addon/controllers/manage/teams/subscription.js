import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action, set } from '@ember/object';

export default class ManageTeamsSubscriptionController extends Controller {
  @service intl;
  @service user;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team.name,
        model: this.model.team.id,
        query: {},
        capitalize: true,
      },
      { text: this.intl.t('subscription.title') },
    ];
  }

  @action
  save(field, value) {
    set(this.model.team, field, value);

    return this.model.team.save();
  }
}
