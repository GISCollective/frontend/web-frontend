import { visit, currentURL, click, waitUntil, fillIn, settled } from '@ember/test-helpers';
import { setupApplicationTest, it, describe } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import { expect } from 'chai';

describe('Acceptance | manage/newsletters/import', function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let team;
  let newsletter;
  let receivedImport;

  hooks.beforeEach(async function () {
    server = new TestServer();

    server.testData.storage.addDefaultArticle();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultSpace();
    newsletter = server.testData.storage.addDefaultNewsletter();
    team = server.testData.storage.addDefaultTeam();
    server.testData.storage.addStat(`newsletter-subscribers-newsletter-${newsletter._id}`, 20);
    server.testData.storage.addStat(`newsletter-emails-newsletter-${newsletter._id}`, 20);
    server.testData.storage.addStat(`newsletter-open-newsletter-${newsletter._id}`, 20);
    server.testData.storage.addStat(`newsletter-interactions-newsletter-${newsletter._id}`, 20);

    team.subscription = {
      name: 'test',
      expire: '2021-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      details: 'details',
      monthlySupportHours: 1,
      support: [
        {
          hours: 1,
          date: '2022-03-22T00:22:11Z',
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 2,
          date: '2022-03-23T00:22:11Z',
          details: 'details 2',
        },
      ],
    };

    receivedImport = null;
    server.post(`/mock-server/newsletters/${newsletter._id}/import-emails`, (request) => {
      receivedImport = JSON.parse(request.requestBody);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('redirects to login when the user is not authenticated', async function (assert) {
    invalidateSession();

    await visit(`/manage/newsletters/import/${newsletter._id}`);

    assert.strictEqual(currentURL(), `/login?redirect=%2Fmanage%2Fnewsletters%2Fimport%2F5ca78e2160780601008f69ff`);
  });

  it('shows the page to an authenticated user', async function (assert) {
    authenticateSession();

    await visit(`/manage/newsletters/import/${newsletter._id}`);
    expect(this.element.querySelector('.btn-edit')).not.to.exist;

    assert.strictEqual(currentURL(), `/manage/newsletters/import/${newsletter._id}`);
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/newsletters/import/${newsletter._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal([
      'Dashboard',
      'Open Green Map',
      'My first newsletter',
      'Import emails',
    ]);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
      '/manage/dashboards/newsletter/5ca78e2160780601008f69ff',
    ]);
  });

  it('can import emails', async function () {
    authenticateSession();

    await visit(`/manage/newsletters/import/${newsletter._id}`);

    await click('.col-copy-paste');

    await fillIn('.input-copy-paste', 'a@b.c,c@c.d');

    await click('.btn-submit');

    await waitUntil(() => receivedImport);
    await settled();

    expect(receivedImport).to.deep.equal({ emails: ['a@b.c', 'c@c.d'], replace: false });
  });
});
