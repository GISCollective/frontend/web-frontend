/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";
import waitFor from '@ember/test-helpers/dom/wait-for';

describe ('Integration | Component | view/picture-preview',  function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPicture('1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::PicturePreview />`);

    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('img')).not.to.exist;
  });

  it('renders a picture when there is an id', async function () {
    this.set('value', {
      type: 'picture',
      record: {
        picture:
          'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
      },
      data: {
        id: '1',
      },
    });

    await render(hbs`<View::PicturePreview @value={{this.value}}/>`);
    await waitFor('.picture-preview img');

    expect(
      this.element.querySelector('.picture-preview img')
    ).to.have.attribute(
      'src',
      'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm'
    );
  });
});
