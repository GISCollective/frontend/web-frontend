/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { fromPageState, toPageState } from '../../lib/page-state';

export default class StateFiltersLocationComponent extends Component {
  @service store;
  @service clickOutside;
  @tracked results;

  @action
  select(value) {
    const newState = toPageState({
      ...this.state,
      l: value?.name,
    });

    this.args.onChangeState?.(newState);
    this.clickOutside.unsubscribe();
  }

  get state() {
    return fromPageState(this.args.state);
  }

  get niceValue() {
    return this.state.l;
  }

  @action
  reset() {
    const newState = toPageState({
      ...this.state,
      l: undefined,
    });

    this.args.onChangeState?.(newState);
  }

  @action
  async search(query) {
    if (!query || query.length < 4) {
      this.results = [];
      return;
    }

    try {
      this.results = await this.store.query('geocoding', {
        query,
      });
    } catch (err) {
      this.results = [];
    }
  }
}
