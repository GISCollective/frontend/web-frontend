/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import Swiper from 'swiper/bundle';
import { service } from '@ember/service';

export default class SwiperComponent extends Component {
  @service fastboot;

  @action
  setup(element) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    let initialSlide = this.args.initialSlide;
    const slides = element.querySelectorAll('.swiper-slide');

    slides.forEach((slide, index) => {
      if (slide.classList.contains('selected')) {
        initialSlide = index;
      }
    });

    const options = {
      autoplay: this.autoplay,
      initialSlide,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
      },
      keyboard: {
        enabled: true,
        onlyInViewport: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    };

    this.mySwiper = new Swiper(element, options);
    this.args.onSetup?.(this.mySwiper);
  }

  get autoplay() {
    if (!this.args.autoplay) {
      return undefined;
    }

    return {
      delay: this.args.autoplay,
      disableOnInteraction: false,
    };
  }

  willDestroy() {
    super.willDestroy(...arguments);

    if (this.fastboot.isFastBoot) {
      return;
    }

    try {
      this.mySwiper.destroy();
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  }
}
