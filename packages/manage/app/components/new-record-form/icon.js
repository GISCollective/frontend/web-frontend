/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class NewRecordFormIconComponent extends Component {
  @service intl;
  @service store;

  @tracked _description = null;
  @tracked _name;
  @tracked iconSet;
  @tracked parentIcon;

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
    this.args.model.name = value;
  }

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-icon'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  get imageUrl() {
    if (!this.args.model.image.value) {
      return '';
    }

    if (this.args.model.image.value.indexOf('data:') == 0) {
      return this.args.model.image.value;
    }

    return this.host + '/' + this.args.model.image.value;
  }

  get categories() {
    var result = [];

    this.args.model.icons.forEach((element) => {
      const category = element.get('category');

      if (result.indexOf(category) == -1) {
        result.push(category);
      }
    });

    return result;
  }

  get subcategories() {
    var result = [];
    const category = this.args.model.category;

    this.args.model.icons
      .filter((a) => a != category)
      .forEach((element) => {
        const subcategory = element.get('subcategory');

        if (result.indexOf(subcategory) == -1) {
          result.push(subcategory);
        }
      });

    return result;
  }

  get isInvalid() {
    const image = this.args.model?.image ?? {};
    const hasImage = image.useParent || image.value;

    return `${this.args.model?.name}`.trim() == '' || !hasImage || !this.args.model?.iconSet?.get?.('id');
  }

  @action
  parentIconChanged(icon) {
    this.parentIcon = icon;
    this.args.model.set('parent', icon?.id ?? '');
  }

  @action
  changeImage(image) {
    this.args.model.image = image;
  }

  @action
  changeIconSet(iconSet) {
    this.args.model.iconSet = iconSet;
    this.iconSet = iconSet;
  }

  @action
  changeDescriptionValue(title, description) {
    this.description = description;
  }

  @action
  save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];

    this.args.model.description = {
      blocks,
    };

    return this.args.onSubmit?.(this.args.model);
  }
}
