/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import Version from '../version';

export default class ManageController extends Controller {
  @service fullscreen;
  @service user;
  @service theme;

  get version() {
    return Version.version;
  }
}
