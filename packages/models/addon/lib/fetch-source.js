/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { later } from '@ember/runloop';
import { get } from '@ember/object';

import queries from "queries";

export const fetchSourceList = queries.fetchSourceList;

export async function fetchSourceRecord(source, space, store) {
  if (!store || !source?.model) {
    return;
  }

  let record;

  try {
    record = store.peekRecord(source.model, source.id);

    if (record) {
      return record;
    }

    const query = { id: source.id };

    if (source.model == 'article') {
      query.team = space?.get ? space.get('teamId') : space?.teamId;
    }

    record = await store.queryRecord(source.model, query);

    await record?.loadRelations?.();
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
  }

  return record;
}

export async function fetchDefault(model, space, store) {
  if (!space) {
    console.error("The 'space' is not set!");
    return null;
  }

  const defaultModels = space.get ? space.get('defaultModels') : space.defaultModels;
  const visibility = space.get ? space.get('visibility') : space.visibility;
  const id = defaultModels?.[model];

  if (model == 'team') {
    try {
      return await visibility.fetchTeam();
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  if (!id) {
    return null;
  }

  let record = store.peekRecord(model, id);

  if (record) {
    return record;
  }

  try {
    return await store.findRecord(model, id);
  } catch (err) {
    console.error(err);
    return null;
  }
}

export function restoreLegacySource(pageCol) {
  return {
    model: pageCol.data?.source?.model ?? pageCol.data?.model,
    ids: pageCol.data?.source?.ids ?? pageCol.data?.ids,
  };
}

async function fetchSourceListPageCol(modelData, pageCol, space, store) {
  try {
    pageCol.currentQuery = JSON.stringify(pageCol.query);

    pageCol.buckets = await fetchSourceList(pageCol.data.source.model, pageCol.query, modelData, space, store);
  } catch (err) {
    if (!pageCol.buckets?.length) {
      pageCol.buckets = null;
    }
  }

  later(() => {
    pageCol.data = {
      ...pageCol.data,
    };
  });

  return pageCol.buckets;
}

async function fetchProperty(modelData, source) {
  if (!source.property) {
    throw new Error('The property is not set for ' + JSON.stringify(source));
  }

  if (!modelData) {
    return;
  }

  let model = {};

  if (source.useSelectedModel) {
    model = modelData['selectedModel'];
  }

  if (!model) {
    return;
  }

  if (typeof model?.[source.property] == 'function') {
    return await model[source.property]();
  }

  let result = get(model, source.property);

  return result;
}

export async function fetch(modelData, pageCol, space, store) {
  if (!pageCol) {
    return null;
  }

  let source = (await pageCol?.data?.source) ?? {};

  // fix the old property selection behavior
  if (pageCol.type == 'gallery' && source.property) {
    pageCol.data.source.property = undefined;
  }

  if (pageCol?.data?.model) {
    source = restoreLegacySource(pageCol);
    pageCol.data.source = source;
  }

  if (!source.model && !source.property) {
    return null;
  }

  if (source?.useDefaultModel) {
    return fetchDefault(source.model, space, store);
  }

  if (pageCol.data?.source?.property) {
    return fetchProperty(modelData, source);
  }

  if (pageCol.data.source?.id) {
    return fetchSourceRecord(source, space, store);
  }

  if (pageCol.data?.source) {
    return fetchSourceListPageCol(modelData, pageCol, space, store);
  }

  return null;
}
