/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  setupApplicationTest as upstreamSetupApplicationTest,
  setupRenderingTest as upstreamSetupRenderingTest,
  setupTest as upstreamSetupTest,
} from 'ember-qunit';
import QUnit from 'qunit';
import TestServer from 'models/test-support/gis-collective/test-server';
import { triggerEvent } from '@ember/test-helpers';
import missingMesage from 'ogm/utils/intl/missing-message';

// This file exists to provide wrappers around ember-qunit's
// test setup functions. This way, you can easily extend the setup that is
// needed per test type.

function setupTest(hooks, options) {
  upstreamSetupTest(hooks, options);

  hooks.beforeEach(function () {
    this.store = this.owner.lookup('service:store');
    this.intl = this.owner.lookup('service:intl');
    this.intl.setLocale('en-us');
    this.intl.setOnMissingTranslation(missingMesage);

  });

  hooks.beforeEach(function (assert) {
    assert.ok(1);
  });

  return hooks;
}

function setupRenderingTest(hooks, options) {
  QUnit.config.testTimeout = 15000;
  QUnit.config.failOnZeroTests = false;

  upstreamSetupRenderingTest(hooks, options);

  hooks.beforeEach(function (assert) {
    if (options?.server) {
      this.server = new TestServer();
      this.storage = this.server.testData.storage;
    }

    this.intl = this.owner.lookup('service:intl');
    this.intl.setLocale('en-us');
    this.intl.setOnMissingTranslation(missingMesage);

    this.store = this.owner.lookup('service:store');
    this.router = this.owner.lookup('service:router');
    this.editorJs = this.owner.lookup('service:editor-js');

    assert.ok(1);
  });

  hooks.afterEach(async function () {
    if (options?.server) {
      this.server.shutdown();
    }
  });

  return hooks;
}


function setupApplicationTest(hooks, options) {
  QUnit.config.testTimeout = 15000;
  QUnit.config.failOnZeroTests = false;

  upstreamSetupApplicationTest(hooks, options);

  hooks.beforeEach(function (assert) {
    assert.ok(1);
  });

  hooks.beforeEach(function () {
    if (options?.server) {
      this.server = new TestServer();
      this.storage = this.server.testData.storage;
    }

    this.store = this.owner.lookup('service:store');
    this.router = this.owner.lookup('service:router');

    this.intl = this.owner.lookup('service:intl');
    this.intl.setLocale('en-us');
    this.intl.setOnMissingTranslation(missingMesage);
  });

  hooks.afterEach(function () {
    if (options?.server) {
      this.server.shutdown();
    }
  });
}

import { module, test } from 'qunit';

function wait(duration) {
  return new Promise((resolve) => {
    setTimeout(resolve, duration);
  });
}

function changeSelect(selector, value) {
  let element = selector;

  if(typeof selector == 'string') {
    element = document.querySelector("#ember-testing-container").querySelector(selector);
  }

  element.value = value;
  return triggerEvent(element, 'change');
}

export { setupApplicationTest, setupRenderingTest, setupTest, module as describe, test as it, wait, changeSelect };
