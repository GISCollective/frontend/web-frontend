/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action, set } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AddArticleController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service store;
  @service router;

  @tracked team;

  destination = 'manage.basemaps.edit';

  get canSubmit() {
    return this.editableModel.name && this.hasTeam;
  }

  get editableModel() {
    return this.model.baseMap;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.editableModel.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new base map'),
      capitalize: true,
    });

    return pieces;
  }

  get canSubmit() {
    return this.team && this.editableModel.name;
  }

  @action
  changeTeam(team) {
    this.team = team;
    set(this.editableModel, 'visibility.team', team);
  }

  @action
  change(name, value) {
    set(this.editableModel, name, value);
  }

  @action
  async save() {
    let result;

    try {
      result = await this.editableModel.save();
    } catch (err) {
      return this.notifications.handleError(err);
    }

    this.router.transitionTo('manage.basemaps.edit', result.id);

    return result;
  }
}
