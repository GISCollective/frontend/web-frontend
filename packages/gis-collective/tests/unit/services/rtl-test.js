/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe ('Unit | Service | rtl',  function (hooks) {
  setupTest(hooks);

  let service;

  hooks.beforeEach(function () {
    document.querySelector('html').classList.remove('translated-rtl');
    service = this.owner.lookup('service:rtl');
  });

  it('exists', function () {
    expect(service).to.be.ok;
  });

  it('has the is rtl flag true when the html tag has the translated-rtl class', function () {
    document.querySelector('html').classList.add('translated-rtl');

    expect(service.isRTL).to.equal(true);
  });

  it('has the is rtl flag false when the html tag has the translated-rtl class', function () {
    document.querySelector('html').classList.remove('translated-rtl');

    expect(service.isRTL).to.equal(false);
  });

  it('has the is rtl flag true when the isRTL is set to true', function () {
    service.isRTL = true;

    expect(service.isRTL).to.equal(true);
  });

  describe ('updateHtml',  function (hooks) {
    it('sets dir="rtl" on the <html> element when isRTL is true', function () {
      service.isRTL = true;

      service.updateHtml();

      expect(document.querySelector('html')).to.have.attribute('dir', 'rtl');
    });

    it('removes dir="rtl" on the <html> element when isRTL is false', function () {
      service.isRTL = false;

      const html = document.querySelector('html');
      html.setAttribute('dir', 'rtl');

      service.updateHtml();

      expect(document.querySelector('html')).not.to.have.attribute(
        'dir',
        'rtl'
      );
    });
  });
});
