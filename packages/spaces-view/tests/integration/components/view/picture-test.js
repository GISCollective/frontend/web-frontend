/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | view/picture', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let pictureData;
  let store;
  let campaign;

  hooks.beforeEach(function () {
    server = new TestServer();

    pictureData = server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultPicture('1');
    campaign = server.testData.storage.addDefaultCampaign();

    store = this.owner.lookup('service:store');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders a picture with a link when the destination is set', async function () {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      picture,
    });

    this.set('value', {
      modelKey: 'picture',
      data: {
        source: {
          useSelectedModel: false,
          model: 'picture',
          id: pictureData._id,
        },
        destination: {
          link: { url: 'https://giscollective.com' },
        },
      },
    });

    await render(
      hbs`<View::Picture @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a')).to.exist;
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'https://giscollective.com',
    );
  });

  it('renders a picture with a target when newTab=true', async function () {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      picture,
    });

    this.set('value', {
      modelKey: 'picture',
      data: {
        source: {
          useSelectedModel: false,
          model: 'picture',
          id: pictureData._id,
        },
        destination: {
          link: { url: 'https://giscollective.com' },
          newTab: true,
        },
      },
    });

    await render(
      hbs`<View::Picture @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a')).to.exist;
    expect(this.element.querySelector('a')).to.have.attribute(
      'target',
      '_blank',
    );
  });

  it('renders a picture without a link when the destination is not set', async function () {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', {
      picture,
    });

    this.set('value', {
      modelKey: 'picture',
      data: {
        source: {
          useSelectedModel: false,
          model: 'picture',
          id: pictureData._id,
        },
        destination: {
          link: { url: '' },
        },
      },
    });

    await render(
      hbs`<View::Picture @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::Picture />`);

    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('img')).not.to.exist;
  });

  describe('using auto size', function (hooks) {
    it('renders a picture when there is an id', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          id: '1',
          sizing: 'auto',
          proportion: '4:3',
          model: 'picture',
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      await waitFor('.page-col-picture img');

      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/lg',
      );
    });

    it('renders a picture using the img tag', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          id: '1',
          sizing: 'auto',
          proportion: '4:3',
          model: 'picture',
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      const element = this.element.querySelector('.page-col-picture');

      expect(element).to.have.class('picture-with-options');
      expect(element).to.have.class('size-auto');
    });

    it('renders the image options with legacy props', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          id: '1',
          sizing: 'auto',
          proportion: '4:3',
          model: 'picture',
          classes: ['rounded-3'],
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      expect(
        this.element.querySelector('.page-col-picture img'),
      ).to.have.attribute('class', 'rounded-3');
    });

    it('renders the image options with new props', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          id: '1',
          size: {
            sizing: 'auto',
            proportion: '4:3',
          },
          model: 'picture',
          classes: ['rounded-3'],
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      expect(
        this.element.querySelector('.page-col-picture img'),
      ).to.have.attribute('class', 'rounded-3');
    });
  });

  describe('using non auto sizes', function (hooks) {
    it('renders a picture when there is a model key and the model with cover is set', async function () {
      this.set('value', {
        type: 'picture',
        data: {
          sizing: 'auto',
          proportion: '4:3',
        },
        modelKey: 'someModel',
      });

      const campaignModel = await store.findRecord('campaign', campaign._id);

      this.set('model', {
        someModel: campaignModel,
      });

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitFor('.page-col-picture img');

      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        '/test/5d5aa72acac72c010043fb59.jpg.lg.jpg',
      );
    });

    it('renders a picture when there is a model key and the model is a picture', async function () {
      this.set('value', {
        type: 'picture',
        data: {
          sizing: 'auto',
          proportion: '4:3',
        },
        modelKey: 'someModel',
      });

      const picture = await store.findRecord('picture', '1');

      this.set('model', {
        someModel: picture,
      });

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitFor('.page-col-picture img');

      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        '/test/5d5aa72acac72c010043fb59.jpg.lg.jpg',
      );
    });

    it('renders nothing when there is no record or model on the modelKey', async function () {
      this.set('value', {
        type: 'picture',
        data: {
          sizing: 'auto',
          proportion: '4:3',
        },
        modelKey: 'someModel',
      });

      this.set('model', {});

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );

      expect(this.element.querySelector('img')).not.to.exist;
      expect(this.element.querySelector('.picture-bg')).not.to.exist;
    });

    it('renders a picture with the proportional size', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          id: '1',
          sizing: 'proportional',
          proportion: '4:3',
          model: 'picture',
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      await waitFor('.loaded');

      expect(this.element.querySelector('.page-col-picture')).to.have.class(
        'size-proportional',
      );

      expect(this.element.querySelector('.page-col-picture img')).to.have.class(
        'd-none',
      );
      expect(this.element.querySelector('.picture-bg')).to.have.attribute(
        'style',
        'background-image: url(https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/lg);aspect-ratio: 4/3;',
      );
    });

    it('renders a picture with the fill size', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: { sizing: 'fill', proportion: '4:3' },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      await waitFor('.loaded');

      expect(this.element.querySelector('.page-col-picture')).to.have.class(
        'size-fill',
      );

      expect(this.element.querySelector('.page-col-picture img')).to.have.class(
        'd-none',
      );
      expect(this.element.querySelector('.picture-bg')).to.have.attribute(
        'style',
        'background-image: url(https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/lg);',
      );
    });

    it('renders the image options', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          sizing: 'fill',
          proportion: '4:3',
          classes: ['rounded-3'],
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      expect(this.element.querySelector('.picture-bg')).to.have.attribute(
        'class',
        'picture-bg rounded-3',
      );
    });

    it('renders the blur hash', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          sizing: 'fill',
          proportion: '4:3',
          classes: ['rounded-3'],
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);
      const style = this.element
        .querySelector('.picture-hash')
        .attributes.getNamedItem('style').value;

      expect(style.split(' ').join('')).to.startWith(
        'background-image:linear-gradient(',
      );
    });

    it('renders only the container style', async function () {
      this.set('value', {
        type: 'picture',
        record: {
          picture:
            'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
        },
        data: {
          sizing: 'fill',
          proportion: '4:3',
          classes: ['rounded-3'],
        },
      });

      await render(hbs`<View::Picture @value={{this.value}}/>`);

      expect(
        this.element.querySelectorAll('style[data-selector]'),
      ).to.have.length(1);
    });
  });

  describe('using fixed height', function (hooks) {
    it('renders a picture when there is a model key and the model with cover is set using legacy props', async function (a) {
      this.set('value', {
        type: 'picture',
        data: {
          sizing: 'fixed height',
          proportion: '4:3',
          customStyle: {
            sm: {
              height: '100px',
              width: 'auto',
            },
            md: {
              height: '200px',
              width: 'auto',
            },
            lg: {
              height: '300px',
              width: 'auto',
            },
          },
        },
        modelKey: 'someModel',
      });

      const campaignModel = await store.findRecord('campaign', campaign._id);

      this.set('model', {
        someModel: campaignModel,
      });

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );
      await waitFor('.page-col-picture img');

      const id = this.element
        .querySelector('.picture-with-options')
        .attributes.getNamedItem('id').textContent;

      expect(
        this.element.querySelector(`[data-selector*="img"]`).textContent.trim(),
      ).to.contain(
        `#${id} img,#${id} .picture-hash-container {height: 100px;width: auto}`,
      );
    });

    it('renders a picture when there is a model key and the model with cover is set using new props', async function () {
      this.set('value', {
        type: 'picture',
        data: {
          size: {
            sizing: 'fixed height',
            proportion: '4:3',
            customStyle: {
              sm: {
                height: '100px',
                width: 'auto',
              },
              md: {
                height: '200px',
                width: 'auto',
              },
              lg: {
                height: '300px',
                width: 'auto',
              },
            },
          },
        },
        modelKey: 'someModel',
      });

      const campaignModel = await store.findRecord('campaign', campaign._id);

      this.set('model', {
        someModel: campaignModel,
      });

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitFor('.page-col-picture img');

      const id = this.element
        .querySelector('.page-col-picture')
        .attributes.getNamedItem('id').textContent;

      expect(
        this.element
          .querySelector(`[data-selector*="#${id} img"]`)
          .textContent.trim(),
      ).to.contain(
        `#${id} img,#${id} .picture-hash-container {height: 100px;width: auto}`,
      );
    });
  });

  describe('using fixed width', function (hooks) {
    it('renders a picture when there is a model key and the model with cover is set', async function () {
      this.set('value', {
        type: 'picture',
        data: {
          sizing: 'fixed width',
          proportion: '4:3',
          customStyle: {
            sm: {
              width: '100px',
              height: 'auto',
            },
            md: {
              width: '200px',
              height: 'auto',
            },
            lg: {
              width: '300px',
              height: 'auto',
            },
          },
        },
        modelKey: 'someModel',
      });

      const campaignModel = await store.findRecord('campaign', campaign._id);

      this.set('model', {
        someModel: campaignModel,
      });

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitFor('.page-col-picture img');

      const id = this.element
        .querySelector('.page-col-picture')
        .attributes.getNamedItem('id').textContent;

      expect(
        this.element
          .querySelector(`[data-selector*="#${id} img"]`)
          .textContent.trim(),
      ).to.contain(
        `#${id} img,#${id} .picture-hash-container {width: 100px;height: auto}`,
      );
    });

    it('renders a picture when there is a model key and the model with cover is set', async function () {
      this.set('value', {
        type: 'picture',
        data: {
          containerClasses: ['text-center'],
          sizing: 'fixed width',
          proportion: '4:3',
          customStyle: {
            sm: {
              width: '100px',
              height: 'auto',
            },
            md: {
              width: '200px',
              height: 'auto',
            },
            lg: {
              width: '300px',
              height: 'auto',
            },
          },
        },
        modelKey: 'someModel',
      });

      const campaignModel = await store.findRecord('campaign', campaign._id);

      this.set('model', {
        someModel: campaignModel,
      });

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );

      expect(this.element.querySelector('.picture-with-options')).to.have.class(
        'text-center',
      );
    });
  });

  describe('using the container properties', function (hooks) {
    it('renders all the properties', async function () {
      this.set('value', {
        type: 'picture',
        data: {
          container: {
            classes: [
              'pt-5',
              'pb-5',
              'ps-5',
              'pe-5',
              'border-radius-3',
              'border-color-warning',
              'border-visibility-yes',
              'border-size-2',
            ],
            color: 'danger',
          },
        },
        modelKey: 'someModel',
      });

      const campaignModel = await store.findRecord('campaign', campaign._id);

      this.set('model', {
        someModel: campaignModel,
      });

      await render(
        hbs`<View::Picture @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitFor('.page-col-picture img');

      expect(
        this.element.querySelector('.picture-with-options'),
      ).to.have.classes([
        'page-col-base-container',
        'bg-danger',
        'pt-5',
        'pb-5',
        'ps-5',
        'pe-5',
        'border-radius-3',
        'border-color-warning',
        'border-visibility-yes',
        'border-size-2',
        'container-align-undefined',
        'picture-with-options',
        'size-auto',
        'page-col-picture',
      ]);
    });
  });
});
