/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class BrowseMapsMapViewSpotlightController extends Controller {
  @service router;
  @service embed;
  @tracked allIcons;

  get hasData() {
    return Object.keys(this.icons).length > 0;
  }

  get icons() {
    const map = {};

    Object.keys(this.model.icons).forEach((name) => {
      const list = this.model.icons[name].slice();

      if (!list.length) {
        return;
      }

      let limitedList = list;

      if (!this.embed.allIcons) {
        limitedList = list.slice(0, 8);
      }

      map[name] = {
        list,
        limitedList,
        hasMore: this.model.iconSets.length != 1 && list.length > 8 && !this.embed.allIcons,
      };
    });

    return map;
  }

  @action
  onSelectIcon(icon) {
    let icons = [];

    if (this.parent.filterManyIcons) {
      icons = this.parent.selectedIconList.map((a) => a.id);
    }

    icons.push(icon.id);

    this.router.transitionTo('browse.maps.map-view', this.router.currentRoute.parent.params.id, {
      queryParams: {
        icons: icons.join(','),
        search: '',
      },
    });
  }

  @action
  onDeselectIcon(icon) {
    let icons = [];

    if (this.parent.filterManyIcons) {
      icons = this.parent.selectedIconList.map((a) => a.id).filter((a) => a != icon.id);
    }

    this.router.transitionTo('browse.maps.map-view', this.router.currentRoute.parent.params.id, {
      queryParams: {
        icons: icons.join(','),
        search: '',
      },
    });
  }
}
