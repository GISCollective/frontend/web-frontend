/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import { CalendarEntry } from 'models/transforms/calendar-entry-list';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';
import { DateTime } from 'luxon';

describe('Integration | Component | input/schedule', function (hooks) {
  setupRenderingTest(hooks);
  let value;

  hooks.beforeEach(function () {
    value = null;

    this.set('change', (v) => {
      value = v;
    });
  });

  it('renders a button to initialize the value when it is not set', async function (a) {
    await render(hbs`<Input::Schedule />`);

    expect(this.element.querySelector('.schedule-interval')).not.to.exist;
    expect(this.element.querySelector('.btn-initialize-schedule')).to.exist;

    await click('.btn-initialize-schedule');

    await waitFor('.select-date-interval');

    expect(this.element.querySelector('.select-date-interval').value).to.equal('false');
    expect(this.element.querySelector('.schedule-begin')).not.to.exist;
    expect(this.element.querySelector('.schedule-end')).not.to.exist;

    this.element.querySelector('.select-date-interval').value = 'true';
    await triggerEvent(this.element.querySelector('.select-date-interval'), 'change');

    expect(this.element.querySelector('.schedule-begin')).to.exist;
    expect(this.element.querySelector('.schedule-end')).to.exist;
    expect(this.element.querySelector('.btn-initialize-schedule')).not.to.exist;

    const now = DateTime.now();

    const leadingZeroMonth = now.month <= 9 ? '0' : '';
    const leadingZeroDay = now.day <= 9 ? '0' : '';

    expect(this.element.querySelector('.schedule-begin .datepicker-input').value).to.equal(
      `${now.year}-${leadingZeroMonth}${now.month}-${leadingZeroDay}${now.day}`,
    );

    const nextYear = now.plus({ year: 1 });

    expect(this.element.querySelector('.schedule-end .datepicker-input').value).to.equal(
      `${nextYear.year}-${leadingZeroMonth}${nextYear.month}-${leadingZeroDay}${nextYear.day}`,
    );

    expect(this.element.querySelectorAll('.interval-begin')).to.have.length(1);

    expect(this.element.querySelector('.interval-begin .hour').value).to.equal(`9`);
    expect(this.element.querySelector('.interval-begin .minute').value).to.equal(`0`);

    expect(this.element.querySelector('.interval-end .hour').value).to.equal(`18`);
    expect(this.element.querySelector('.interval-end .minute').value).to.equal(`0`);

    expect(
      this.element.querySelector('.input-timezone .ember-power-select-selected-item .name').textContent.trim(),
    ).to.equal('CET');
  });

  it('renders a list with two entries in the same day', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'EET',
      }),
      new CalendarEntry({
        begin: '2023-01-02T13:00:00Z',
        end: '2023-01-02T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'EET',
      }),
    ]);

    await render(hbs`<Input::Schedule @value={{this.value}}/>`);

    expect(this.element.querySelector('.schedule-begin .datepicker-input').value).to.equal('2023-01-02');

    expect(this.element.querySelector('.schedule-end .datepicker-input').value).to.equal('2024-01-02');

    const beginIntervals = this.element.querySelectorAll('.interval-begin');
    const endIntervals = this.element.querySelectorAll('.interval-end');

    expect(beginIntervals).to.have.length(2);

    expect(beginIntervals[0].querySelector('.hour').value).to.equal(`12`);
    expect(beginIntervals[0].querySelector('.minute').value).to.equal(`0`);

    expect(beginIntervals[1].querySelector('.hour').value).to.equal(`15`);
    expect(beginIntervals[1].querySelector('.minute').value).to.equal(`0`);

    expect(endIntervals[0].querySelector('.hour').value).to.equal(`14`);
    expect(endIntervals[0].querySelector('.minute').value).to.equal(`0`);

    expect(endIntervals[1].querySelector('.hour').value).to.equal(`20`);
    expect(endIntervals[1].querySelector('.minute').value).to.equal(`0`);

    expect(
      this.element.querySelector('.input-timezone .ember-power-select-selected-item .name').textContent.trim(),
    ).to.equal('EET');
  });

  it('can change the timezone', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
      new CalendarEntry({
        begin: '2023-01-02T13:00:00Z',
        end: '2023-01-02T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
    ]);

    await render(hbs`<Input::Schedule @value={{this.value}} @onChange={{this.change}}/>`);

    await selectSearch('.input-timezone', 'europe');
    await selectChoose('.input-timezone', 'Europe/Bucharest');

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: '2023-01-02T08:00:00.000Z',
          end: '2023-01-02T10:00:00.000Z',
          intervalEnd: '2024-01-02T16:00:00.000Z',
          repetition: 'weekly',
          timezone: 'Europe/Bucharest',
        },
        {
          begin: '2023-01-02T11:00:00.000Z',
          end: '2023-01-02T16:00:00.000Z',
          intervalEnd: '2024-01-02T16:00:00.000Z',
          repetition: 'weekly',
          timezone: 'Europe/Bucharest',
        },
      ],
    );
  });

  it('can change the time', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
      new CalendarEntry({
        begin: '2023-01-02T13:00:00Z',
        end: '2023-01-02T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
    ]);

    await render(hbs`<Input::Schedule @value={{this.value}} @onChange={{this.change}}/>`);

    let beginIntervals = this.element.querySelectorAll('.interval-begin');
    let endIntervals = this.element.querySelectorAll('.interval-end');

    beginIntervals[0].querySelector('.hour').value = '4';
    await triggerEvent(beginIntervals[0].querySelector('.hour'), 'change');

    endIntervals = this.element.querySelectorAll('.interval-end');
    endIntervals[0].querySelector('.hour').value = '5';
    await triggerEvent(endIntervals[0].querySelector('.hour'), 'change');

    beginIntervals = this.element.querySelectorAll('.interval-begin');
    beginIntervals[1].querySelector('.hour').value = '6';
    await triggerEvent(beginIntervals[1].querySelector('.hour'), 'change');

    endIntervals = this.element.querySelectorAll('.interval-end');
    endIntervals[1].querySelector('.hour').value = '7';
    await triggerEvent(endIntervals[1].querySelector('.hour'), 'change');

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: '2023-01-02T04:00:00.000Z',
          end: '2023-01-02T05:00:00.000Z',
          intervalEnd: '2024-01-02T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
        {
          begin: '2023-01-02T06:00:00.000Z',
          end: '2023-01-02T07:00:00.000Z',
          intervalEnd: '2024-01-02T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
      ],
    );
  });

  it('can disable the time interval', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
      new CalendarEntry({
        begin: '2023-01-02T13:00:00Z',
        end: '2023-01-02T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
    ]);

    await render(hbs`<Input::Schedule @value={{this.value}} @onChange={{this.change}}/>`);

    let beginIntervals = this.element.querySelectorAll('.interval-begin');
    let endIntervals = this.element.querySelectorAll('.interval-end');

    beginIntervals[0].querySelector('.hour').value = '4';
    await triggerEvent(beginIntervals[0].querySelector('.hour'), 'change');

    endIntervals = this.element.querySelectorAll('.interval-end');
    endIntervals[0].querySelector('.hour').value = '5';
    await triggerEvent(endIntervals[0].querySelector('.hour'), 'change');

    beginIntervals = this.element.querySelectorAll('.interval-begin');
    beginIntervals[1].querySelector('.hour').value = '6';
    await triggerEvent(beginIntervals[1].querySelector('.hour'), 'change');

    endIntervals = this.element.querySelectorAll('.interval-end');
    endIntervals[1].querySelector('.hour').value = '7';
    await triggerEvent(endIntervals[1].querySelector('.hour'), 'change');

    this.element.querySelector('.select-date-interval').value = 'false';
    await triggerEvent(this.element.querySelector('.select-date-interval'), 'change');

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: '2023-01-02T04:00:00.000Z',
          end: '2023-01-02T05:00:00.000Z',
          intervalEnd: '2100-01-02T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
        {
          begin: '2023-01-02T06:00:00.000Z',
          end: '2023-01-02T07:00:00.000Z',
          intervalEnd: '2100-01-02T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
      ],
    );
  });

  it('can change the date interval', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
      new CalendarEntry({
        begin: '2023-01-03T13:00:00Z',
        end: '2023-01-03T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
    ]);

    await render(hbs`<Input::Schedule @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn('.schedule-begin .datepicker-input', '2023-01-14');
    await fillIn('.schedule-end .datepicker-input', '2024-01-15');

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: '2023-01-14T10:00:00.000Z',
          end: '2023-01-14T12:00:00.000Z',
          intervalEnd: '2024-01-15T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
        {
          begin: '2023-01-15T13:00:00.000Z',
          end: '2023-01-15T18:00:00.000Z',
          intervalEnd: '2024-01-16T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
      ],
    );
  });

  it('can delete an interval', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
      new CalendarEntry({
        begin: '2023-01-03T13:00:00Z',
        end: '2023-01-03T18:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
    ]);

    await render(hbs`<Input::Schedule @value={{this.value}} @onChange={{this.change}}/>`);

    await click('.btn-delete-interval');

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: '2023-01-03T13:00:00.000Z',
          end: '2023-01-03T18:00:00.000Z',
          intervalEnd: '2024-01-02T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
      ],
    );
  });

  it('can add an interval', async function (a) {
    this.set('value', [
      new CalendarEntry({
        begin: '2023-01-02T10:00:00Z',
        end: '2023-01-02T12:00:00Z',
        intervalEnd: '2024-01-02T18:00:00Z',
        repetition: 'weekly',
        timezone: 'UTC',
      }),
    ]);

    await render(hbs`<Input::Schedule @value={{this.value}} @onChange={{this.change}}/>`);

    const buttons = this.element.querySelectorAll('.btn-add-time-interval');

    await click(buttons[5]);

    a.deepEqual(
      value.map((a) => a.toJSON()),
      [
        {
          begin: '2023-01-02T10:00:00.000Z',
          end: '2023-01-02T12:00:00.000Z',
          intervalEnd: '2024-01-02T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
        {
          begin: '2023-01-07T09:00:00.000Z',
          end: '2023-01-07T18:00:00.000Z',
          intervalEnd: '2024-01-07T18:00:00.000Z',
          repetition: 'weekly',
          timezone: 'UTC',
        },
      ],
    );
  });
});
