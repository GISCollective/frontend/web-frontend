/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/button-style', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a button', async function () {
    this.set('value', {
      classes: ['btn-primary', 'size-btn-lg', 'w-50'],
    });

    await render(
      hbs`<View::ButtonStyle class="btn" @value={{this.value}}>some text</View::ButtonStyle>`,
    );

    expect(this.element.querySelector('.btn').textContent.trim()).to.equal(
      'some text',
    );

    expect(this.element.querySelector('.btn')).to.have.classes(
      'btn btn-primary size-btn-lg w-50',
    );
  });
});
