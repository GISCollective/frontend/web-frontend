/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

import { selectSearch } from "ember-power-select/test-support";

describe("Integration | Component | editor/icon-container", function (hooks) {
  setupRenderingTest(hooks);

  let value;

  hooks.beforeEach(function () {
    value = undefined;

    this.set("onChange", (v) => {
      value = v;
    });
  });

  it("renders the values", async function (a) {
    this.set("value", {
      style: { sm: { height: "10px" }, md: { height: "20px" }, lg: { height: "30px" } },
      classes: ["mt-4"],
      destination: { path: "/page" },
    });

    await render(hbs`<Editor::IconContainer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".sm-size-px .input-px-size").value).to.equal("10");
    expect(this.element.querySelector(".md-size-px .input-px-size").value).to.equal("20");
    expect(this.element.querySelector(".lg-size-px .input-px-size").value).to.equal("30");

    expect(this.element.querySelector(".sm-top-margin").value).to.equal("4");
    expect(this.element.querySelector(".ember-power-select-trigger").textContent.trim()).to.equal("/page");
  });

  it("can change the values", async function (a) {
    await render(hbs`<Editor::IconContainer @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".sm-size-px .input-px-size", "1");
    await fillIn(".md-size-px .input-px-size", "2");
    await fillIn(".lg-size-px .input-px-size", "3");

    this.element.querySelector(".sm-top-margin").value = "5";
    await triggerEvent(".sm-top-margin", "change");

    await selectSearch(".ember-power-select-trigger", "/login");

    expect(value).to.deep.equal({
      style: { sm: { height: "1px" }, md: { height: "2px" }, lg: { height: "3px" } },
      classes: ["mt-5"],
      destination: { path: "/login" },
    });
  });
});
