/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class CardComponentsExtraFieldsComponent extends Component {
  get extraFields() {
    return this.args.options?.card?.extraFields ?? this.args.options?.extraFields;
  }

  get entries() {
    if (!this.extraFields?.entries) {
      return false;
    }

    if (!this.args.record.entries) {
      return false;
    }

    return this.args.record.entries;
  }

  get schedule() {
    if (!this.extraFields?.schedule) {
      return false;
    }

    if (!this.args.record.entries) {
      return false;
    }

    return this.args.record.entries;
  }
}
