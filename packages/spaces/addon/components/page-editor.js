import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export class Selection {
  @tracked type;
  @tracked title;
  @tracked data;

  constructor(type, title, data) {
    if (typeof type == "object") {
      this.type = type.type;
      this.title = type.title;
      this.data = type.data ?? {};

      return;
    }

    this.type = type;
    this.title = title;
    this.data = data ?? {};
  }
}

export default class PageEditor extends Component {
  @tracked selection = new Selection("page", "page");
  @tracked deviceSize;
  @tracked conditions = ["user:unauthenticated"];
  @tracked _hasSpaceUpdate;
  @tracked hasRestoredPage;

  get hasSpaceUpdate() {
    return this._hasSpaceUpdate || this.args.model.space.hasDirtyAttributes;
  }

  @action
  selectPage() {
    this.selection = new Selection("page", "page");
  }

  get deviceSizeClass() {
    if (!this.deviceSize) {
      return "";
    }

    return `enforce-${this.deviceSize}-size`;
  }

  @action
  changeDeviceSize(value) {
    this.deviceSize = value;
  }

  @action
  changeConditions(newConditions) {
    this.conditions = newConditions;
  }

  @action
  spaceChange(value) {
    this.args.model.space = value;
    this.args.model.page.space = value;
    this._hasSpaceUpdate = true;
  }

  @action
  select(selection) {
    if (selection?.isHighlight) {
      this.selection = selection;
      return;
    }

    if (this.selection && this.selection.title == selection?.title) {
      return;
    }

    if (selection?.type == "col") {
      selection.data = this.args.model.page.upsertPageColByName(selection.data.name);
    }

    this.selection = new Selection(selection);

    if (selection?.type == "col") {
      this.selection.data.store = this.store;
    }
  }

  @action
  async revertPage(revisionId) {
    this.args.model.page = await this.args.model.page.loadRevision(revisionId);
    this.hasRestoredPage = true;
  }

  @action
  async save() {
    if (this.hasSpaceUpdate) {
      await this.args.model.space.save();
      this._hasSpaceUpdate = false;
    }

    for (const key in this.args.model) {
      const resolvedRecord = await this.args.model[key];

      if (resolvedRecord?.shouldSave && key != "page") {
        await resolvedRecord.save();
      }
    }

    this.args.model.page.space = this.args.model.space;

    const promise = this.args.model.page.save();
    this.hasRestoredPage = false;

    return promise;
  }
}
