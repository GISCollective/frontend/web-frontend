/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, fillIn, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/manage/surveys/questions', function (hooks) {
  setupRenderingTest(hooks, { server: true });

  let icon1;
  let icon2;
  let map;

  hooks.beforeEach(async function () {
    icon1 = this.server.testData.storage.addDefaultIcon('1');
    icon2 = this.server.testData.storage.addDefaultIcon('2');
    map = this.server.testData.storage.addDefaultMap();

    icon1 = await this.store.findRecord('icon', '1');
    icon2 = await this.store.findRecord('icon', '2');
  });

  describe ('in view mode when a map is set', function (hooks) {
    hooks.beforeEach(async function () {
      this.set('map', await this.store.findRecord('map', map._id));
    });

    describe ('with the default values', function (hooks) {
      it('renders the default name values', async function () {
        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="questions" @map={{this.map}} />`
        );

        expect(
          this.element.querySelector('.name-label').textContent.trim()
        ).to.equal('Name');

        expect(
          this.element.querySelector('.alert-name-field').textContent.trim()
        ).to.equal(
          'The feature name will be generated on submit and the user will not be prompted for a name.'
        );
      });

      it('renders the default description values', async function () {
        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="questions" @map={{this.map}} />`
        );

        expect(
          this.element.querySelector('.description-label').textContent.trim()
        ).to.equal('Description');

        expect(
          this.element
            .querySelector('.alert-description-field')
            .textContent.trim()
        ).to.equal('The user will not be prompted for a description.');
      });

      it('renders the default icon values', async function () {
        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="questions" @map={{this.map}} />`
        );

        expect(
          this.element.querySelector('.icons-label').textContent.trim()
        ).to.equal('Icons');

        expect(
          this.element.querySelector('.alert-icons-field').textContent.trim()
        ).to.equal('The user will not be prompted to select icons.');
      });

      it('renders the default location values', async function () {
        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="questions" @map={{this.map}} />`
        );

        expect(
          this.element.querySelector('.location-label').textContent.trim()
        ).to.equal('Location');

        expect(
          this.element
            .querySelector('.location-field-message')
            .textContent.trim()
        ).to.equal('The user will be able to select a site:');

        let options = [
          ...this.element.querySelectorAll('.location-option-list li'),
        ].map((a) => a.textContent.trim());

        expect(options).to.deep.equal([
          'by using their current location',
          'by selecting a point on a map',
          'by searching an address',
        ]);
      });

      it('renders the default picture values', async function () {
        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="questions" @map={{this.map}} />`
        );

        expect(
          this.element.querySelector('.pictures-label').textContent.trim()
        ).to.equal('Photos');

        expect(
          this.element
            .querySelector('.alert-pictures-field')
            .textContent.trim()
        ).to.equal('The user can add photos. They are not mandatory.');
      });
    });

    describe ('with the custom values', function (hooks) {
      hooks.beforeEach(async function () {
        this.set('icons', [icon1, icon2]);
        this.set('value', {
          name: {
            customQuestion: true,
            label: 'name label',
          },

          description: {
            customQuestion: true,
            label: 'description label',
          },

          pictures: {
            customQuestion: true,
            label: 'pictures label',
          },

          icons: {
            customQuestion: true,
            label: 'icons label',
          },

          location: {
            customQuestion: true,
            label: 'location label',
            allowGps: true,
            allowManual: true,
            allowAddress: true,
            allowExistingFeature: true,
          },
        });

        this.set('icons', [icon1, icon2]);

        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="settings" @value={{this.value}} @optionalIcons={{this.icons}} @map={{this.map}} />`
        );
      });

      it('renders the custom name question', async function () {
        expect(
          this.element
            .querySelector('.alert-name-field .message')
            .textContent.trim()
        ).to.equal('The user will be prompted with the question:');
        expect(
          this.element.querySelector('.name-label-value').textContent.trim()
        ).to.equal('name label');
      });

      it('renders the custom description question', async function () {
        expect(
          this.element
            .querySelector('.alert-description-field .message')
            .textContent.trim()
        ).to.equal('The user will be prompted with the question:');
        expect(
          this.element
            .querySelector('.description-label-value')
            .textContent.trim()
        ).to.equal('description label');
      });

      it('renders the custom icons question', async function () {
        expect(
          this.element
            .querySelector('.alert-icons-field .message')
            .textContent.trim()
        ).to.equal(
          'The user can select at least one icon and will be prompted with the question:'
        );
        expect(
          this.element.querySelector('.icons-label-value').textContent.trim()
        ).to.equal('icons label');
      });

      it('renders the icons and the default question when the icons are set and the label not', async function () {
        this.set('value', {
          showNameQuestion: true,
          nameLabel: 'name label',
          showDescriptionQuestion: true,
          descriptionLabel: 'description label',
        });

        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="settings" @value={{this.value}} @optionalIcons={{this.icons}} @map={{this.map}} />`
        );

        expect(
          this.element
            .querySelector('.alert-icons-field .message')
            .textContent.trim()
        ).to.equal(
          'The user can select at least one icon and will be prompted with the question:'
        );
        expect(
          this.element.querySelector('.icons-label-value').textContent.trim()
        ).to.equal('Please select an icon from the list');
      });

      it('renders the location question when set', async function () {
        expect(
          this.element
            .querySelector('.alert-location-field .message')
            .textContent.trim()
        ).to.equal('The user will be asked:');
        expect(
          this.element.querySelector('.location-label-value').textContent.trim()
        ).to.equal('location label');
      });

      it('renders all the location options when they are all set to true', async function () {
        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="settings" @value={{this.value}} @optionalIcons={{this.icons}} @map={{this.map}} />`
        );

        let options = [
          ...this.element.querySelectorAll('.location-option-list li'),
        ].map((a) => a.textContent.trim());

        expect(options).to.deep.equal([
          'by using their current location',
          'by selecting a point on a map',
          'by searching an address',
          'by using an existing map feature',
        ]);
      });

      it('renders the custom picture label', async function () {
        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="settings" @value={{this.value}} @optionalIcons={{this.icons}} @map={{this.map}} />`
        );

        expect(this.element.querySelector(".pictures-label-value").textContent.trim()).to.equal("pictures label");

        expect(
          this.element.querySelector('.pictures-label').textContent.trim()
        ).to.equal('Photos');

        expect(this.element.querySelector(".message-question").textContent.trim()).to.equal("The user will be prompted with the question:");

        expect(
          this.element
            .querySelector('.message-pictures')
            .textContent.trim()
        ).to.equal('The user can add photos. They are not mandatory.');
      });

      it('renders the mandatory picture label', async function () {
        this.set('value', {
          name: {
            customQuestion: true,
            label: 'name label',
          },

          description: {
            customQuestion: true,
            label: 'description label',
          },

          pictures: {
            customQuestion: true,
            isMandatory: true,
            label: 'pictures label',
          },

          icons: {
            customQuestion: true,
            label: 'icons label',
          },

          location: {
            customQuestion: true,
            label: 'location label',
            allowGps: true,
            allowManual: true,
            allowAddress: true,
            allowExistingFeature: true,
          },
        });

        await render(
          hbs`<Input::Manage::Campaigns::Questions @title="settings" @value={{this.value}} @optionalIcons={{this.icons}} @map={{this.map}} />`
        );

        expect(this.element.querySelector(".pictures-label-value").textContent.trim()).to.equal("pictures label");

        expect(
          this.element.querySelector('.pictures-label').textContent.trim()
        ).to.equal('Photos');

        expect(this.element.querySelector(".message-question").textContent.trim()).to.equal("The user will be prompted with the question:");

        expect(
          this.element
            .querySelector('.message-pictures')
            .textContent.trim()
        ).to.equal('The user can add photos. They are required.');
      });
    });
  });

  describe ('edit mode', function (hooks) {
    it('renders the default values', async function () {
      await render(
        hbs`<Input::Manage::Campaigns::Questions @title="settings" @editablePanel="settings" />`
      );

      expect(
        this.element.querySelector('.name-options .custom-question').value
      ).to.equal('no');
      expect(this.element.querySelector('.name-options input.label')).not.to.exist;

      expect(
        this.element.querySelector('.description-options .custom-question')
          .value
      ).to.equal('no');
      expect(this.element.querySelector('.description-options input.label')).not.to
        .exist;

      expect(
        this.element.querySelector('.location-options .custom-question').value
      ).to.equal('no');

      expect(
        this.element.querySelector('.location-options .custom-question').value
      ).to.equal('no');
      expect(
        this.element.querySelector('.location-options .allow-gps').value
      ).to.equal('yes');
      expect(
        this.element.querySelector('.location-options .allow-manual').value
      ).to.equal('yes');
      expect(
        this.element.querySelector('.location-options .allow-address').value
      ).to.equal('yes');
      expect(
        this.element.querySelector('.location-options .allow-existing-feature')
          .value
      ).to.equal('no');
    });

    it('renders the custom location values', async function () {
      this.set('value', {
        showNameQuestion: true,
        nameLabel: 'name label',
        showDescriptionQuestion: true,
        descriptionLabel: 'description label',
        iconsLabel: 'icons label',

        location: {
          customQuestion: true,
          label: 'location label',
          allowGps: false,
          allowManual: false,
          allowAddress: false,
          allowExistingFeature: true,
        },
      });
      await render(
        hbs`<Input::Manage::Campaigns::Questions @optionalIcons={{this.icons}} @value={{this.value}} @title="settings" @editablePanel="settings" />`
      );
      expect(
        this.element.querySelector('.location-options .custom-question').value
      ).to.equal('yes');
      expect(
        this.element.querySelector('.location-options input.label').value
      ).to.equal('location label');
      expect(
        this.element.querySelector('.location-options .allow-gps').value
      ).to.equal('no');
      expect(
        this.element.querySelector('.location-options .allow-manual').value
      ).to.equal('no');
      expect(
        this.element.querySelector('.location-options .allow-address').value
      ).to.equal('no');
      expect(
        this.element.querySelector('.location-options .allow-existing-feature')
          .value
      ).to.equal('yes');
    });

    it('renders the custom icon values', async function () {
      this.set('icons', [icon1, icon2]);
      this.set('availableIcons', { icons: [icon1, icon2] });

      this.set('value', {
        showNameQuestion: true,
        nameLabel: 'name label',
        showDescriptionQuestion: true,
        descriptionLabel: 'description label',
        iconsLabel: 'icons label',

        icons: {
          customQuestion: true,
          label: 'icons label',
        },
      });
      await render(
        hbs`<Input::Manage::Campaigns::Questions @availableIcons={{this.availableIcons}} @optionalIcons={{this.icons}} @value={{this.value}} @title="settings" @editablePanel="settings" />`
      );

      expect(this.element.querySelectorAll('.icons-order img').length).to.equal(
        2
      );
    });

    it('renders the custom name values', async function () {
      this.set('value', {
        name: {
          customQuestion: true,
          label: 'name label',
        },
      });

      await render(
        hbs`<Input::Manage::Campaigns::Questions @optionalIcons={{this.icons}} @value={{this.value}} @title="settings" @editablePanel="settings" />`
      );

      expect(
        this.element.querySelector('.name-options .custom-question').value
      ).to.equal('yes');
      expect(this.element.querySelector('.name-options input.label').value).to.equal(
        'name label'
      );
    });

    it('renders the custom description values', async function () {
      this.set('value', {
        description: {
          customQuestion: true,
          label: 'description label',
        },
      });
      await render(
        hbs`<Input::Manage::Campaigns::Questions @optionalIcons={{this.icons}} @value={{this.value}} @title="settings" @editablePanel="settings" />`
      );

      expect(
        this.element.querySelector('.description-options .custom-question')
          .value
      ).to.equal('yes');
      expect(
        this.element.querySelector('.description-options input.label').value
      ).to.equal('description label');
    });

    it('can change the default values', async function () {
      let value;
      let icons;
      this.set('icons', { icons: [icon1, icon2] });

      this.set('save', (v, i) => {
        value = v;
        icons = i;
      });

      await render(
        hbs`<Input::Manage::Campaigns::Questions @availableIcons={{this.icons}} @onSave={{this.save}} @title="settings" @editablePanel="settings" />`
      );

      /// name
      this.element.querySelector('.name-options .custom-question').value =
        'yes';
      await triggerEvent('.name-options .custom-question', 'change');
      await fillIn('.name-options input.label', 'value-2');

      /// description
      this.element.querySelector(
        '.description-options .custom-question'
      ).value = 'yes';
      await triggerEvent('.description-options .custom-question', 'change');
      await fillIn('.description-options input.label', 'value-2');

      /// pictures
      this.element.querySelector(
        '.pictures-options .custom-question'
      ).value = 'yes';
      await triggerEvent('.pictures-options .custom-question', 'change');
      await fillIn('.pictures-options input.label', 'value-3');
      this.element.querySelector(
        '.pictures-options .is-mandatory'
      ).value = 'yes';
      await triggerEvent('.pictures-options .is-mandatory', 'change');

      /// icons
      await click('.icon-select');
      await click('.btn-icon');
      await click('.modal .btn-success');

      this.element.querySelector('.icons-options .custom-question').value =
        'yes';
      await triggerEvent('.icons-options .custom-question', 'change');

      await fillIn('.icons-options input.label', 'value-2');

      await click('.btn-submit');

      expect(value.toJSON()).to.deep.equal({
        registrationMandatory: false,
        featureNamePrefix: '',
        name: { customQuestion: true, label: 'value-2' },
        description: { customQuestion: true, label: 'value-2' },
        icons: { customQuestion: true, label: 'value-2' },
        pictures: { customQuestion: true, label: 'value-3', isMandatory: true },
        location: {
          customQuestion: false,
          label: '',
          allowGps: true,
          allowManual: true,
          allowAddress: true,
          allowExistingFeature: false,
        },
      });
      expect(icons.map((a) => a.id)).to.deep.equal(['1']);
    });

    it('renders the changed dropdown values', async function () {
      let value;
      let icons;
      this.set('icons', { icons: [icon1, icon2] });

      this.set('save', (v, i) => {
        value = v;
        icons = i;
      });

      await render(
        hbs`<Input::Manage::Campaigns::Questions @availableIcons={{this.icons}} @onSave={{this.save}} @title="settings" @editablePanel="settings" />`
      );

      this.element.querySelector('.name-options .custom-question').value =
        'yes';
      await triggerEvent('.name-options .custom-question', 'change');

      expect(
        this.element.querySelector('.name-options .custom-question').value
      ).to.equal('yes');
    });

    it('resets the values when cancel is pressed', async function () {
      let isCalled;

      this.set('title', 'settings');
      this.set('cancel', () => {
        isCalled = true;
        this.set('title', '');
      });

      await render(
        hbs`<Input::Manage::Campaigns::Questions @onCancel={{this.cancel}} @title={{this.title}} @editablePanel="settings" />`
      );

      this.element.querySelector('.name-options .custom-question').value =
        'yes';
      await triggerEvent('.name-options .custom-question', 'change');
      await fillIn('.name-options input.label', 'value-2');

      this.element.querySelector(
        '.description-options .custom-question'
      ).value = 'yes';
      await triggerEvent('.description-options .custom-question', 'change');
      await fillIn('.description-options input.label', 'value-2');

      await click('.btn-cancel');

      expect(isCalled).to.equal(true);
    });
  });
});
