/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class CampaignAnswerAdapter extends Adapter {
  get modelName() {
    return 'campaign-answer';
  }

  approve(id) {
    const url = this.buildURL('campaign-answer', id) + '/approve';
    return this.ajax(url, 'POST');
  }

  reject(id) {
    const url = this.buildURL('campaign-answer', id) + '/reject';
    return this.ajax(url, 'POST');
  }

  revert(id) {
    const url = this.buildURL('campaign-answer', id) + '/restore';
    return this.ajax(url, 'POST');
  }

  deleteMany(query) {
    const url = [this.urlForQuery(query, this.modelName).toLowerCase(), this.getParams(query)]
      .filter((a) => a)
      .join('?');
    return this.ajax(url, 'DELETE');
  }
}
