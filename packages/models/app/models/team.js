/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, hasMany, belongsTo } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';
import { getOwner } from '@ember/application';
import { tracked } from '@glimmer/tracking';

export default class TeamModel extends Model {
  @attr('string') name;
  @attr about;

  @attr('boolean') isPublic;

  @hasMany('user', { async: true, inverse: null })
  owners;

  @hasMany('user', { async: true, inverse: null })
  leaders;

  @hasMany('user', { async: true, inverse: null })
  members;

  @hasMany('user', { async: true, inverse: null })
  guests;

  @belongsTo('picture', { async: true, inverse: null })
  logo;

  @hasMany('picture', { async: true, inverse: null })
  pictures;

  @attr('social-media-links')
  socialMediaLinks;

  @attr('string', { defaultValue: '' })
  contactEmail;

  @attr('boolean') canEdit;
  @attr('boolean') isPublisher;
  @attr('boolean') allowCustomDataBindings;
  @attr('boolean') allowNewsletters;
  @attr('boolean') allowEvents;
  @attr('boolean') allowReports;
  @attr('subscription-details') subscription;
  @attr('invitation-list') invitations;

  @attr('string-list') categories;

  @tracked currentUserRole;

  get cover() {
    return this.logo;
  }

  get hasLogo() {
    if (!this.logo) {
      return false;
    }

    const name = this.logo.get('name');

    return name != 'default';
  }

  get title() {
    return this.name;
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  get contentBlocks() {
    return toContentBlocks(this.about, this.name);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async updateCurrentUserRole() {
    const owner = getOwner(this);
    const user = owner.lookup('service:user');

    const id = user?.userData?.id;

    const keys = ['owners', 'leaders', 'members', 'guests'];

    this.currentUserRole = 'unknown';

    for (const key of keys) {
      const list = await this[key];
      const idList = list.map((a) => a.id);

      if (idList.includes(id)) {
        this.currentUserRole = key;
        return;
      }
    }
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;

    if (this.hasLogo) {
      metaInfo.imgSrc = this.logo?.get('picture');
    }
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    return metaInfo;
  }

  async invite(emailList, role) {
    return await this.store.adapterFor(this.constructor.modelName).invite(this.id, emailList, role);
  }
}
