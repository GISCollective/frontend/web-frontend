/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr } from '@ember-data/model';

export default class PreferenceModel extends Model {
  @attr('string') name;
  @attr value;
}
