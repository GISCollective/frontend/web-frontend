/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, before, after } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Acceptance | presentation/index',  function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPresentation(`name`);
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('can visit an existing presentation', async function () {
    await visit('/presentation/name');

    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('.presentation-slider')).to.exist;
    expect(currentURL()).to.equal('/presentation/name');
  });
});
