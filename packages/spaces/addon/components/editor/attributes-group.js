import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class EditorAttributesGroup extends Component {
  get groups() {
    return this.args.value?.groups ?? [];
  }

  @action
  change(name, value) {
    this.args.onChange?.({
      [name]: value,
    });
  }
}
