/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class CampaignAdapter extends Adapter {
  getAttributeValues(calendarId, attribute, term = '', limit = 5) {
    const url =
      this.buildURL('calendar', calendarId) + `/attributes?attribute=${attribute}&term=${term}&limit=${limit}`;
    return this.ajax(url, 'GET');
  }
}
