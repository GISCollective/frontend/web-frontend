/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageTeamsAddController extends Controller {
  queryParams = ['next'];

  @service notifications;
  @service user;
  @service intl;
  @service router;

  @tracked isSaving = false;
  @tracked _description;
  @tracked next;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        text: this.intl.t('new team'),
        capitalize: true,
      },
    ];
  }

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-team'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  get isInvalid() {
    return String(this.model.name).trim() == '';
  }

  @action
  changeAboutValue(title, description) {
    this.description = description;
  }

  @action
  async save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];

    this.model.about = {
      blocks,
    };

    try {
      const result = await this.model.save();
      await this.user.loadTeams();

      if (this.next) {
        return this.router.transitionTo(this.next);
      }

      return this.router.transitionTo('manage.teams.edit', result.id);
    } catch (err) {
      this.notifications.handleError(err);
    }
  }
}
