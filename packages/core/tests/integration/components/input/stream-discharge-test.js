import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | input/stream-discharge', function (hooks) {
  setupRenderingTest(hooks);

  it('calculates the value', async function () {
    let newValue;
    this.set('change', (v) => {
      newValue = v;
    });

    await render(hbs`<Input::StreamDischarge @onChange={{this.change}}/>`);

    await fillIn('.input-velocity', '2');
    await fillIn('.input-width', '3');
    await fillIn('.input-depth', '4');

    const value = this.element.querySelector('.input-result').value;

    expect(value).to.equal('24');
    expect(newValue).to.deep.equal({
      type: 'stream discharge',
      data: {
        velocity: 2,
        width: 3,
        depth: 4,
        result: 24,
      },
    });
  });

  it('renders a value', async function () {
    this.set('value', {
      type: 'stream discharge',
      data: {
        velocity: 20,
        width: 30,
        depth: 40,
        result: 24,
      },
    });

    await render(hbs`<Input::StreamDischarge @value={{this.value}} @onChange={{this.change}}/>`);

    expect(this.element.querySelector('.input-velocity').value).to.equal('20');
    expect(this.element.querySelector('.input-width').value).to.equal('30');
    expect(this.element.querySelector('.input-depth').value).to.equal('40');
    expect(this.element.querySelector('.input-result').value).to.equal('24000');
  });

  it('can render a disabled value', async function () {
    this.set('value', {
      type: 'stream discharge',
      data: {
        velocity: 20,
        width: 30,
        depth: 40,
        result: 24,
      },
    });

    await render(hbs`<Input::StreamDischarge @disabled={{true}} @value={{this.value}} @onChange={{this.change}}/>`);

    expect(this.element.querySelector('.input-velocity')).to.have.attribute('disabled', '');
    expect(this.element.querySelector('.input-width')).to.have.attribute('disabled', '');
    expect(this.element.querySelector('.input-depth')).to.have.attribute('disabled', '');
    expect(this.element.querySelector('.input-result')).to.have.attribute('disabled', '');
  });
});
