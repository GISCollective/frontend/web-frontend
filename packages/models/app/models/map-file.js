/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { belongsTo, attr } from '@ember-data/model';
import FileMeta from 'models/lib/file-meta';
import { tracked } from '@glimmer/tracking';

export default class MapFile extends Model {
  @belongsTo('map', { async: true, inverse: null })
  map;

  @attr('string') file;
  @attr('string') name;
  @attr('number', { defaultValue: 0 }) size;
  @attr('boolean', { defaultValue: true }) canEdit;

  @attr('map-file-options') options;

  @tracked meta;

  import() {
    return this.store.adapterFor(this.constructor.modelName).import(this.id);
  }

  cancelImport() {
    return this.store.adapterFor(this.constructor.modelName).cancelImport(this.id);
  }

  async reloadMeta() {
    const meta = await this.store.adapterFor(this.constructor.modelName).getMeta(this.id);

    this.meta = new FileMeta(meta);

    return this.meta;
  }

  metaPreview() {
    return this.store.adapterFor(this.constructor.modelName).metaPreview(this.id);
  }

  analyze() {
    return this.store.adapterFor(this.constructor.modelName).analyze(this.id);
  }

  getLog() {
    if (this.store.isDestroyed) {
      return;
    }
    return this.store.adapterFor(this.constructor.modelName).getLog(this.id);
  }

  get isRunning() {
    if (!this.meta) {
      return false;
    }

    return this.meta.isRunning;
  }

  set isRunning(value) {
    if (!this.meta) {
      this.meta = new FileMeta();
    }

    this.meta.isRunning = value;
  }
}
