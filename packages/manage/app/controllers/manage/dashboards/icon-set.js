/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ManageDashboardsDashboardController extends Controller {
  @service router;
  @service intl;

  queryParams = ['c', 'sc'];

  @tracked c;
  @tracked sc;

  @action
  browseIcon(id) {
    this.router.transitionTo('manage.icons.edit', this.model.iconSet.id, id);
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }

  @action
  setCategory(value) {
    this.c = value;
  }

  @action
  setSubcategory(value) {
    this.sc = value;
  }

  get categories() {
    const list = Object.keys(this.model.iconTree.categories ?? {});

    return list.filter((a) => a);
  }

  get subcategories() {
    const list = Object.keys(this.model.iconTree?.categories?.[this.category]?.categories ?? {});

    return list.filter((a) => a);
  }

  get category() {
    if (this.c && this.categories.includes(this.c)) {
      return this.c;
    }

    if (this.model.iconTree.icons?.length > 0 && this.categories.length == 0) {
      return '';
    }

    return this.categories[0] ?? '';
  }

  get subcategory() {
    const category = this.category;

    if (!category) {
      return '';
    }

    const categoryNode = this.model.iconTree.categories[category];

    if (!categoryNode) {
      return '';
    }

    if (categoryNode.icons?.length > 0 && this.subcategories.length == 0) {
      return '';
    }

    if (this.sc && this.subcategories.includes(this.sc)) {
      return this.sc;
    }

    return this.subcategories?.[0] ?? '';
  }

  get selectedIcons() {
    const uncategorisedList = this.model.iconTree.icons ?? [];
    const categoryList = this.model.iconTree?.categories?.[this.category]?.icons ?? [];
    const subcategoryList =
      this.model.iconTree?.categories?.[this.category]?.categories?.[this.subcategory]?.icons ?? [];

    return [...subcategoryList, ...categoryList, ...uncategorisedList];
  }

  get query() {
    const result = {
      iconSet: this.model.iconSet.id,
    };

    if (this.category) {
      result.category = this.category;
    }

    if (this.subcategory) {
      result.subcategory = this.subcategory;
    }

    return result;
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        text: this.model.iconSet?.name,
      },
    ];
  }
}
