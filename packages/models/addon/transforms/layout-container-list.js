/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { LayoutRow } from './layout-row-list';
import { A } from '../lib/array';

export default class LayoutContainerListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return A([]);
    }

    return A(serialized.map((a, i) => new LayoutContainer(a, i)));
  }

  serialize(deserialized) {
    return deserialized?.map((a) => (a.toJSON ? a.toJSON() : a)) ?? [];
  }
}

export class LayoutContainer {
  @tracked options = [];
  @tracked visibility = 'always';
  @tracked rows = [];
  @tracked layers = { count: 1, effect: 'none', showContentAfter: 1 };
  @tracked name;
  @tracked gid;
  @tracked anchor;
  @tracked height;

  constructor(serialized, containerIndex) {
    if (!serialized) {
      return;
    }

    if (Array.isArray(serialized.options)) {
      this.options = serialized.options;
    }

    if (Array.isArray(serialized.rows)) {
      serialized.rows.forEach((element, index) => {
        if (!element.name && typeof containerIndex == 'number') {
          element.name = `${containerIndex}.${index}`;
        }
      });
      this.rows = serialized.rows.map((a) => new LayoutRow(a));
    }

    if (serialized.visibility) {
      this.visibility = serialized.visibility;
    }

    if (serialized.gid) {
      this.gid = serialized.gid;
    }

    if (serialized.anchor) {
      this.anchor = serialized.anchor;
    }

    if (serialized.height) {
      this.height = serialized.height;
    }

    if (typeof containerIndex == 'number') {
      this.name = `${containerIndex}`;
    }

    if (typeof serialized.layers == 'object') {
      this.layers.count = serialized.layers.count ?? 1;
      this.layers.effect = serialized.layers.effect ?? 'none';
      this.layers.showContentAfter = serialized.layers.showContentAfter ?? 1;
    }
  }

  get names() {
    return this.rows.flatMap((a) => [a.name, ...a.cols.flatMap((b) => b.names)]);
  }

  contains(name) {
    return name == this.name || this.names.includes(name);
  }

  updateIndex(newIndex) {
    this.name = `${newIndex}`;

    for (const row of this.rows) {
      row.updateContainerIndex(newIndex);
    }
  }

  get cls() {
    let options = this.options.slice();

    if (options.indexOf('container-fluid') != -1) {
      options = options.filter((a) => a != 'container');
    }

    if (options.indexOf('container-fluid') == -1 && options.indexOf('container') == -1) {
      options.push('container');
    }

    return options.join(' ');
  }

  isVisible(conditions = []) {
    if (!conditions) {
      return true;
    }

    if (conditions.includes(this.visibility)) {
      return true;
    }

    return this.visibility == 'always';
  }

  toJSON() {
    const result = {
      rows: this.rows?.map((a) => a.toJSON()) ?? [],
      options: this.options,
      visibility: this.visibility,
      layers: this.layers,
    };

    if (this.gid) {
      result.gid = this.gid;
    }

    if (this.anchor) {
      result.anchor = this.anchor;
    }

    if (this.height) {
      result.height = this.height;
    }

    return result;
  }
}
