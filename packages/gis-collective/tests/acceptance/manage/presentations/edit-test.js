/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click, fillIn, triggerEvent, waitUntil } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe ('Acceptance | manage/presentations/edit',  function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let presentation;
  let receivedPresentation;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
    server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');

    presentation = server.testData.storage.addDefaultPresentation();

    receivedPresentation = null;
    server.put(`/mock-server/presentations/${presentation._id}`, (request) => {
      receivedPresentation = JSON.parse(request.requestBody);
      receivedPresentation.presentation._id = presentation._id;

      server.testData.storage.addDefaultPresentation(receivedPresentation.presentation);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPresentation)];
    });
  });

  hooks.afterEach(async function () {
    await server.waitAllRequests();
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/presentations/${presentation._id}`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fpresentations%2F000000000000000000000001');
  });

  it('renders the form for a presentation', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    expect(PageElements.containerGroupTitles()).to.deep.equal(['team', 'original author', 'Name', 'Slug', 'Content']);
  });

  it('can update the name', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    await click('.row-name .btn-edit');
    await fillIn('.row-name input', 'new name');

    await click('.btn-submit');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.name).to.deep.equal('new name');
  });

  it('can update the slug', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    await click('.row-slug .btn-edit');
    await fillIn('.row-slug input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.slug).to.deep.equal('new name');
  });

  it('can update the Content', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    await click('.container-group-content .btn-edit');

    await click('.btn-add-row');
    await click('.btn-submit');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.cols).to.deep.equal([
      { container: +0, col: +0, row: +0, name: '0.0.0', type: 'article', data: { source: { id: '5ca78e2160780601008f69e6', model: 'article' } } },
      { container: +0, col: 1, row: +0, name: '0.0.1', type: 'picture', data: { id: '5cc8dc1038e882010061545a', source: { id: '5cc8dc1038e882010061545a', model: 'picture' } } },
      { container: +0, col: +0, row: 1, name: '0.1.0', type: 'article', data: {} },
      { container: +0, col: 1, row: 1, name: '0.1.1', type: 'picture', data: {} },
    ]);
  });
});
