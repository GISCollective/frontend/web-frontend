/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';

export default class LabelComponent extends Component {
  elementId = 'label-' + guidFor(this);
}
