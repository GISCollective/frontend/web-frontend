import Component from "@glimmer/component";

export default class EditorMenuComponents extends Component {
  panel = [
    {
      name: "showLogo",
      type: "bool",
      label: "show logo",
    },
    {
      name: "showLogin",
      type: "bool",
      label: "show login",
    },
    {
      name: "showSearch",
      type: "bool",
      label: "show search",
    },
  ];
}
