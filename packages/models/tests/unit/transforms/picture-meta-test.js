/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | picture meta', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let transform = this.owner.lookup('transform:picture-meta');
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize(null);

    expect(deserialized.renderMode).to.equal('');
  });

  it('can deserialize an empty object', function () {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({});

    expect(deserialized.renderMode).to.equal('');
  });

  it('can deserialize an object with the renderMode property', function () {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({ renderMode: '360' });

    expect(deserialized.renderMode).to.equal('360');
  });

  it('can deserialize an object with the width and height properties', function () {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({ width: 1, height: 2 });

    expect(deserialized.width).to.equal(1);
    expect(deserialized.height).to.equal(2);
  });

  it('can deserialize an object with the width and height properties', function () {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({ mime: 'a', format: 'b' });

    expect(deserialized.mime).to.equal('a');
    expect(deserialized.format).to.equal('b');
  });

  it('can deserialize an object with the exif properties', function () {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({ properties: { a: 'b' } });

    expect(deserialized.properties).to.deep.equal({ a: 'b' });
  });

  it('can serialize an object', function () {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({
      renderMode: '360',
      attributions: 'a',
      link: 'b',
      data: {},
      disableOptimization: true,
    });

    expect(transform.serialize(deserialized)).to.deep.equal({
      renderMode: '360',
      attributions: 'a',
      link: 'b',
      data: {},
      disableOptimization: true,
    });
  });
});
