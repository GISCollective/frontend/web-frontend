/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/contact-form', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let spaceData;
  let receivedMessage;
  let pageData;
  let teamData;

  hooks.before(function () {
    server = new TestServer();
    spaceData = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultTeam();
    pageData = server.testData.storage.addDefaultPage();
    teamData = server.testData.storage.addDefaultTeam(
      '61292c4c7bdf9301008fd7b6',
    );

    server.post(
      `/mock-server/pages/000000000000000000000001/contact`,
      (request) => {
        receivedMessage = JSON.parse(request.requestBody);
        return [201, {}, ''];
      },
    );
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    this.set('space', await this.store.findRecord('space', spaceData._id));
    this.set('model', {});
  });

  it('can render the form with floating labels', async function () {
    this.set('value', {
      data: {
        form: {
          floatingLabels: true,
          sendTo: 'test@test.test',
          emailLabel: 'email:',
          nameLabel: 'name:',
          messageLabel: 'message:',
          subjectLabel: 'subject:',
          labelSuccess: 'Thank you for your message. We will reply soon.',
          submitButtonLabel: 'sign up',
          subject: 'subject',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
        },
        button: {
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
      },
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    const labels = [
      ...this.element.querySelectorAll('.form-floating label'),
    ].map((a) => a.textContent.trim());

    expect(labels).to.deep.equal(['name:', 'email:', 'subject:', 'message:']);

    expect(
      this.element.querySelector('.btn-contact-submit').textContent.trim(),
    ).to.equal('sign up');
  });

  it('can render the form with floating labels (legacy)', async function () {
    this.set('value', {
      data: {
        form: {
          floatingLabels: true,
          sendTo: 'a@gmail.com',
          emailLabel: 'Your email address:',
          nameLabel: 'Your name:',
          subjectLabel: 'Subject:',
          messageLabel: 'Your message:',
          labelSuccess: 'Thank you for your message. We will reply soon.',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
        },
        button: {
          label: 'sign up',
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
      },
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    const labels = [
      ...this.element.querySelectorAll('.form-floating label'),
    ].map((a) => a.textContent.trim());

    expect(labels).to.deep.equal([
      'Your name:',
      'Your email address:',
      'Subject:',
      'Your message:',
    ]);

    expect(
      this.element.querySelector('.btn-contact-submit').textContent.trim(),
    ).to.equal('sign up');
  });

  it('can render the form without floating labels (legacy)', async function () {
    this.set('value', {
      data: {
        form: {
          floatingLabels: false,
          sendTo: 'a@gmail.com',
          emailLabel: 'Your email address:',
          nameLabel: 'Your name:',
          subjectLabel: 'Subject:',
          messageLabel: 'Your message:',
          labelSuccess: 'Thank you for your message. We will reply soon.',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
        },
        button: {
          label: 'sign up',
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
      },
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    const labels = [...this.element.querySelectorAll('label')].map((a) =>
      a.textContent.trim(),
    );

    expect(this.element.querySelectorAll('.form-floating').length).to.equal(0);
    expect(labels).to.deep.equal([
      'Your name:',
      'Your email address:',
      'Subject:',
      'Your message:',
    ]);

    expect(
      this.element.querySelector('.btn-contact-submit').textContent.trim(),
    ).to.equal('sign up');
  });

  it('renders a disabled submit form when the fields are not filled', async function () {
    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.alert')).not.to.exist;

    expect(this.element.querySelector('.btn-contact-submit')).to.have.attribute(
      'disabled',
      '',
    );
  });

  it('sets the subject from the current model when it has a title template', async function (a) {
    this.set('value', {
      data: {
        form: {
          floatingLabels: true,
          emailLabel: 'Your email address:',
          nameLabel: 'Your name:',
          messageLabel: 'Your message:',
          labelSuccess: 'Thank you for your message. We will reply soon.',
          subject: 'some subject: {title}',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
          minLines: { lg: 0, md: 0, sm: 0 },
        },
        button: {
          label: 'sign up',
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
      },
    });

    this.set('model', {
      selectedModel: {
        title: 'some title',
      },
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.contact-subject').value).to.equal(
      'some subject: some title',
    );
  });

  it('enables the form when the fields are filled', async function () {
    this.set('value', {
      name: 'some-name',
      data: {
        form: {
          labelSuccess: 'Custom message.',
          sendTo: 'hello@giscollective.com',
        },
      },
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    await fillIn('.contact-name', 'some name');
    await fillIn('.contact-email', 'me@giscollective.com');
    await fillIn('.contact-subject', 'my subject');
    await fillIn('.contact-message', 'my message');

    expect(
      this.element.querySelector('.btn-contact-submit'),
    ).not.to.have.attribute('disabled', '');
  });

  it('enables the form when the contact email is set to the team', async function () {
    teamData.contactEmail = 'test@test.com';
    spaceData.visibility.team = '61292c4c7bdf9301008fd7b6';

    server.testData.storage.addSpace(spaceData);
    server.testData.storage.addTeam(teamData);

    this.set('value', {
      name: 'some-name',
      data: {
        form: {
          labelSuccess: 'Custom message.',
        },
      },
    });

    await render(
      hbs`<View::ContactForm @space={{this.space}} @value={{this.value}} @model={{this.model}} />`,
    );

    await fillIn('.contact-name', 'some name');
    await fillIn('.contact-email', 'me@giscollective.com');
    await fillIn('.contact-subject', 'my subject');
    await fillIn('.contact-message', 'my message');

    expect(
      this.element.querySelector('.btn-contact-submit'),
    ).not.to.have.attribute('disabled', '');
  });

  it('keeps the disabled the form when the fields are filled and send to is not set', async function () {
    this.set('value', {
      name: 'some-name',
      data: {
        form: {
          labelSuccess: 'Custom message.',
        },
      },
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    await fillIn('.contact-name', 'some name');
    await fillIn('.contact-email', 'me@giscollective.com');
    await fillIn('.contact-subject', 'my subject');
    await fillIn('.contact-message', 'my message');

    expect(this.element.querySelector('.alert')).not.to.exist;
    expect(this.element.querySelector('.btn-contact-submit')).to.have.attribute(
      'disabled',
      '',
    );
  });

  it('shows an alert when the send to is not set in edit mode', async function () {
    this.set('value', {
      name: 'some-name',
      data: {
        form: {
          labelSuccess: 'Custom message.',
        },
      },
    });

    await render(
      hbs`<View::ContactForm @isEditor={{true}} @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.alert')).to.have.textContent(
      "The form is disabled because the 'send to' property is not set.",
    );
  });

  it('can submit the form', async function () {
    const page = await this.store.findRecord('page', pageData._id);

    this.set('value', {
      name: 'some-name',
      data: {
        form: {
          sendTo: 'hello@giscollective.com',
        },
      },
    });

    this.set('model', {
      page,
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    await fillIn('.contact-name', 'some name');
    await fillIn('.contact-email', 'me@giscollective.com');
    await fillIn('.contact-subject', 'my subject');
    await fillIn('.contact-message', 'my message');

    await click('.btn-contact-submit');

    await waitUntil(() => receivedMessage);

    expect(
      this.element.querySelector('.thank-you-message').textContent.trim(),
    ).to.equal('Your message has been successfully sent. We will reply soon.');

    expect(receivedMessage).to.deep.equal({
      message: {
        colName: 'some-name',
        name: 'some name',
        email: 'me@giscollective.com',
        subject: 'my subject',
        message: 'my message',
      },
    });
  });

  it('can show a custom success message', async function () {
    const page = await this.store.findRecord('page', pageData._id);

    this.set('value', {
      name: 'some-name',
      data: {
        form: {
          labelSuccess: 'Custom message.',
          sendTo: 'hello@giscollective.com',
        },
      },
    });

    this.set('model', {
      page,
    });

    await render(
      hbs`<View::ContactForm @value={{this.value}} @model={{this.model}} />`,
    );

    await fillIn('.contact-name', 'some name');
    await fillIn('.contact-email', 'me@giscollective.com');
    await fillIn('.contact-subject', 'my subject');
    await fillIn('.contact-message', 'my message');

    await click('.btn-contact-submit');

    await waitUntil(() => receivedMessage);

    expect(
      this.element.querySelector('.thank-you-message').textContent.trim(),
    ).to.equal('Custom message.');
  });
});
