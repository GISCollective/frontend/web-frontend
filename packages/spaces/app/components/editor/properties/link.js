/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { buildWaiter } from "@ember/test-waiters";
import { action } from "@ember/object";
import { later, debounce } from "@ember/runloop";

let waiter = buildWaiter("dom-changed-waiter");

export default class ManageEditorsPropertiesLinkComponent extends Component {
  @tracked _value;
  @tracked _pagesMap;

  @action
  deferSearch(term) {
    return new Promise((res) =>
      later(() => {
        debounce(this, this.searchTask, term, res, 150);
      }),
    );
  }

  @action
  searchTask(term, res) {
    const lowerTerm = term.toLowerCase().trim();
    const match = Object.entries(this.pagesMap)
      .filter((a) => a[0].toLowerCase().indexOf(lowerTerm) != -1)
      .map((a) => a[0]);

    if (
      lowerTerm.indexOf("http:") === 0 ||
      lowerTerm.indexOf("https:") === 0 ||
      lowerTerm.includes("#") ||
      lowerTerm.includes("/")
    ) {
      later(() => {
        this.handleChange(term);
      });
      match.push(term);
    }

    return res(match);
  }

  get options() {
    return Object.entries(this.pagesMap).map((a) => a[0]);
  }

  get pagesMap() {
    return this._pagesMap ?? {};
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args?.value ?? {};
  }

  get pageId() {
    return this.value?.pageId;
  }

  get slug() {
    return this.value?.slug;
  }

  get textValue() {
    const match = Object.entries(this.pagesMap).find((a) => a[1] == this.pageId || a[0] == this.slug);

    if (match) {
      return match[0];
    }

    if (this.value?.url) {
      return this.value.url;
    }

    if (this.value?.path) {
      return this.value.path;
    }

    return "";
  }

  triggerChange() {
    return this.args.onChange(this.value);
  }

  @action
  deferChange(value) {
    later(() => {
      debounce(this, this.handleChange, value, 150);
    });
  }

  @action
  async handleChange(value) {
    let token = waiter.beginAsync();

    const match = Object.entries(this.pagesMap).find((a) => a[0] == value);

    if (match) {
      this._value = {
        pageId: match[1],
      };
    }

    const lowerValue = value.toLowerCase().trim();

    if (lowerValue.indexOf("/") === 0) {
      this._value = {
        path: value,
      };
    }

    if (lowerValue.indexOf("http:") === 0 || lowerValue.indexOf("https:") === 0 || lowerValue.includes("#")) {
      this._value = {
        url: value,
      };
    }

    await this.triggerChange();

    later(() => {
      waiter.endAsync(token);
    });
  }

  @action
  async setup() {
    this._pagesMap = await this.args.space?.getPagesMap();
  }
}
