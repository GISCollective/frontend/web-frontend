import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageNewslettersImportController extends Controller {
  @service intl;
  @service router;
  @service user;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team.name,
        model: this.model.team.id,
        query: {},
        capitalize: true,
      },
      {
        route: 'manage.dashboards.newsletter',
        text: this.model.newsletter.name,
        model: this.model.newsletter.id,
        query: {},
        capitalize: true,
      },
      { text: this.intl.t('import emails'), capitalize: true },
    ];
  }

  @action
  onSave() {
    this.router.transitionTo('manage.dashboards.newsletter', this.model.newsletter.id);
  }
}
