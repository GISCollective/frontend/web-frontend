/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

describe("Integration | Component | editor/source-stats", function (hooks) {
  setupRenderingTest(hooks);

  it("allows changing the stats source", async function (a) {
    let value;

    this.set("value", {
      id: "published-campaigns",
      model: "stat",
    });

    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::SourceStats @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".stats-name").value).to.equal("published-campaigns");

    this.element.querySelector(".stats-name").value = "campaign-answer-contributors";
    await triggerEvent(".stats-name", "change");

    expect(value).to.deep.equal({
      id: "campaign-answer-contributors",
      model: "stat",
    });
  });
});
