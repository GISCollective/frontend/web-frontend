/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent, typeIn, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe('Integration | Component | new-record-form/event', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let receivedEvent;
  let receivedPicture;
  let eventData;


  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.beforeEach(async function () {
    let team = server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPreferences();
    let calendarData1 = server.testData.storage.addDefaultCalendar('1');
    let calendarData2 = server.testData.storage.addDefaultCalendar('2');
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIcon();

    eventData = server.testData.storage.addDefaultEvent();
    server.testData.storage.addDefaultCalendar();

    let calendar1 = await this.store.findRecord('calendar', calendarData1._id);
    let calendar2 = await this.store.findRecord('calendar', calendarData2._id);

    let event = this.store.createRecord('event', {
      calendar: calendar1,
    });

    receivedPicture = null;
    server.post('/mock-server/pictures', (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = '1';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPicture)];
    });

    this.set('teams', [team._id]);
    this.set('event', event);
    this.set('calendars', [calendar1, calendar2]);
    this.set('save', (v, p) => {
      receivedEvent = v.serialize();
      receivedPicture = p?.serialize();
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders a disabled add button by default', async function () {
    await render(hbs`<NewRecordForm::Event />`);

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');
  });

  it('hides the icons and attributes when the calendar has none', async function () {
    let calendar = server.testData.storage.addDefaultCalendar('1');
    calendar.name = 'no icons and attributes';
    calendar.iconSets = {
      useCustomList: false,
      list: [],
    };

    calendar.attributes = [];

    await render(hbs`<NewRecordForm::Event @event={{this.event}} @teams={{this.teams}} @onSubmit={{this.save}} />`);

    expect(this.element.querySelector('.row-icons')).not.to.exist;
    expect(this.element.querySelector('.row-attributes')).not.to.exist;

    this.element.querySelector('.row-calendar select').value = '2';
    await triggerEvent('.row-calendar select', 'change');

    expect(this.element.querySelector('.row-icons')).to.exist;
    expect(this.element.querySelector('.row-attributes')).to.exist;
  });

  it('hides the calendar when the calendar is set and hideCalendar is true', async function () {
    let calendar = server.testData.storage.addDefaultCalendar('1');
    calendar.name = 'no icons and attributes';
    calendar.iconSets = {
      useCustomList: false,
      list: [],
    };

    calendar.attributes = [];

    await render(
      hbs`<NewRecordForm::Event @event={{this.event}} @teams={{this.teams}} @hideCalendar={{true}} @onSubmit={{this.save}} />`,
    );

    expect(this.element.querySelector('.row-calendar')).not.to.exist;
  });

  it('renders a schedule for a calendar with schedule type', async function () {
    let calendar = server.testData.storage.addDefaultCalendar('1');
    calendar.name = 'no icons and attributes';
    calendar.iconSets = {
      useCustomList: false,
      list: [],
    };

    calendar.attributes = [];
    calendar.type = 'schedules';
    this.event.visibility = { team: 'some-team' };

    await render(hbs`<NewRecordForm::Event @event={{this.event}} @teams={{this.teams}} @onSubmit={{this.save}} />`);

    await PageElements.waitEditorJs(this.element);
    await fillIn('.form-control-name', 'new calendar');

    expect(this.element.querySelector('.input-schedule')).to.exist;
    await click('.btn-initialize-schedule');

    await selectSearch('.input-event-location', 'a new text value');
    await selectChoose('.input-event-location', 'a new text value');

    await click('.btn-submit');
    await waitUntil(() => receivedEvent);

    expect(receivedEvent.entries).to.have.length(1);
  });

  it('renders a custom heading when a record name is set', async function () {
    let calendar = server.testData.storage.addDefaultCalendar('1');
    calendar.name = 'no icons and attributes';
    calendar.recordName = 'volunteering opportunity';
    calendar.iconSets = {
      useCustomList: false,
      list: [],
    };

    calendar.attributes = [];
    calendar.type = 'schedules';
    this.event.visibility = { team: 'some-team' };

    await render(hbs`<NewRecordForm::Event @event={{this.event}} @teams={{this.teams}} @onSubmit={{this.save}} />`);

    expect(this.element.querySelector('h1').textContent.trim()).to.equal('New volunteering opportunity');
    expect(this.element.querySelector('.row-name input')).to.have.attribute(
      'placeholder',
      'what is the volunteering opportunity name?',
    );
  });

  it('can set all fields', async function (a) {
    server.testData.storage.addDefaultCalendar('1');
    server.testData.storage.addDefaultCalendar('2');

    await render(hbs`<NewRecordForm::Event @event={{this.event}} @teams={{this.teams}} @onSubmit={{this.save}} />`);
    await PageElements.waitEditorJs(this.element);
    await fillIn('.form-control-name', 'new calendar');

    await fillIn('.ce-paragraph', '');
    await typeIn('.ce-paragraph', 'test message');
    await PageElements.wait(510);

    await click('.row-when .btn-add');

    this.element.querySelector('.row-calendar select').value = '2';
    await triggerEvent('.row-calendar select', 'change');

    await click('.input-icons-selector .icon-container');

    /// begin date
    await fillIn('.input-calendar-entry .value-begin .datepicker-input', '2029-05-20');

    this.element.querySelector('.input-calendar-entry .value-begin .hour').value = '20';
    await triggerEvent('.input-calendar-entry .value-begin .hour', 'change');

    this.element.querySelector('.input-calendar-entry .value-begin .minute').value = '0';
    await triggerEvent('.input-calendar-entry .value-begin .minute', 'change');

    /// end date
    await fillIn('.input-calendar-entry .value-end .datepicker-input', '2029-05-20');

    this.element.querySelector('.input-calendar-entry .value-end .hour').value = '21';
    await triggerEvent('.input-calendar-entry .value-end .hour', 'change');

    this.element.querySelector('.input-calendar-entry .value-end .minute').value = '0';
    await triggerEvent('.input-calendar-entry .value-end .minute', 'change');

    /// cover
    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });

    await selectSearch('.input-event-location', 'a new text value');
    await selectChoose('.input-event-location', 'a new text value');

    await fillIn('.row-attributes .form-control', 'value');

    await click('.btn-submit');

    await waitUntil(() => receivedPicture);
    await waitUntil(() => receivedEvent);

    a.deepEqual(receivedEvent, {
      name: 'new calendar',
      article: { blocks: [{ type: 'paragraph', data: { text: 'test message' } }] },
      entries: [
        {
          begin: '2029-05-20T10:00:00.000Z',
          end: '2029-05-20T10:00:00.000Z',
          intervalEnd: '0001-01-01T11:42:30.000Z',
          repetition: 'norepeat',
          timezone: 'CET',
        },
      ],
      location: { type: 'Text', value: 'a new text value' },
      nextOccurrence: undefined,
      allOccurrences: [],
      attributes: { key1: 'value' },
      visibility: { isPublic: true, isDefault: false, team: '5ca78e2160780601008f69e6' },
      info: undefined,
      calendar: '2',
      icons: ['5ca7bfc0ecd8490100cab980'],
      cover: null,
    });
  });

  it('can add a picture in the article', async function (a) {
    server.testData.storage.addDefaultCalendar('1');
    server.testData.storage.addDefaultCalendar('2');

    await render(hbs`<NewRecordForm::Event @event={{this.event}} @teams={{this.teams}} @onSubmit={{this.save}} />`);
    await PageElements.waitEditorJs(this.element);

    await click('.ce-paragraph');
    await click('.ce-toolbar__plus');
    await click(".ce-popover-item[data-item-name='image']");

    await waitUntil(() => document.querySelector("body > [type='file']"));

    const blob = server.testData.create.pngBlob();
    await triggerEvent(document.querySelector("body > [type='file']"), 'change', { files: [blob] });

    await waitUntil(() => receivedPicture);

    a.deepEqual(receivedPicture.picture.meta, {
      renderMode: '',
      attributions: '',
      link: {},
      data: {},
      disableOptimization: false,
    });
  });
});
