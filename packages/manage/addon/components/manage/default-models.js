import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { defaultModelNames } from 'models/transforms/default-models';
import { action } from '@ember/object';

export default class ManageDefaultModelsComponent extends Component {
  @service store;

  @tracked map = null;
  @tracked campaign = null;
  @tracked calendar = null;
  @tracked newsletter = null;

  get pairs() {
    const result = [];

    for (const name of defaultModelNames) {
      result.push({ name, value: this[name]?.title ?? 'not set', model: this[name] });
    }

    return result;
  }

  @action
  async setup() {
    const value = this.args.value;

    for (const name of defaultModelNames) {
      if (value?.[name]) {
        let resolvedValue = this.store.peekRecord(name, value[name]);

        if (!resolvedValue) {
          try {
            resolvedValue = await this.store.findRecord(name, value[name]);
          } catch (err) {
            console.error(err);
          }
        }

        this[name] = resolvedValue;
      }
    }
  }
}
