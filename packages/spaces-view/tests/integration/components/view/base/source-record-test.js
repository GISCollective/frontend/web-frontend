/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/base/source-record', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<View::Base::SourceRecord />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('adds the highlight attributes when the path is set', async function (a) {
    this.set('value', {
      name: 'some name',
      gid: 'some gid',
    });
    await render(
      hbs`<View::Base::SourceRecord @value={{this.value}} @path="some.path" />`,
    );

    const container = this.element.querySelector('.source-record');

    expect(container).to.have.attribute('data-name', 'some name');
    expect(container).to.have.attribute('data-type', 'data source');
    expect(container).to.have.attribute('data-path', 'some.path.data.source');
    expect(container).to.have.attribute('data-editor', 'source-record');
  });
});
