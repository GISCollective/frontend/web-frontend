/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it } from "dummy/tests/helpers";
import { setupRenderingTest } from "dummy/tests/helpers";
import { click, render, fillIn, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { move } from "core/lib/trigger";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | input/menu", function (hooks) {
  setupRenderingTest(hooks);
  let pageId;

  hooks.before(function () {
    pageId;
  });

  it("adds a new item when the add item is pressed", async function () {
    let value;

    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::Menu @onChange={{this.change}} />`);

    await click(".btn-add-menu-item");

    expect(value.toJSON()).to.deep.equal({
      items: [
        {
          name: "",
          link: {},
          dropDown: [],
        },
      ],
    });
  });

  it("renders an item when its set", async function () {
    this.set("value", { items: [{ name: "name", link: "" }] });

    await render(hbs`<Input::Menu @value={{this.value}} />`);

    expect(this.element.querySelectorAll(".manage-editors-properties-link-named")).to.have.length(1);
  });

  it("adds a new item when the add item is pressed", async function () {
    let value;

    this.set("change", (v) => {
      value = v;
    });

    this.set("value", {
      items: [{ name: "name", link: { url: "https://test.com" } }],
    });

    await render(hbs`<Input::Menu @value={{this.value}} @onChange={{this.change}} />`);

    await click(".btn-add-menu-item");
    expect(value.toJSON()).to.deep.equal({
      items: [
        { name: "name", link: { url: "https://test.com" }, dropDown: [] },
        { name: "", link: {}, dropDown: [] },
      ],
    });
  });

  it("can update an item values", async function () {
    let value;

    this.set("change", (v) => {
      value = v;
    });

    this.set("value", {
      items: [
        {
          name: "name",
          link: { url: "https://giscollective.com" },
          dropDown: [],
        },
      ],
    });

    await render(hbs`<Input::Menu @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn(".link-name", "new name");

    const selectLinks = this.element.querySelectorAll(".ember-basic-dropdown-trigger");
    await selectSearch(selectLinks[0], "https://test.com");
    await selectChoose(selectLinks[0], "https://test.com");

    expect(value.toJSON()).to.deep.equal({
      items: [{ name: "new name", link: { url: "https://test.com" }, dropDown: [] }],
    });
  });

  describe("when there is an item on the menu", function (hooks) {
    let value;

    hooks.beforeEach(async function () {
      this.set("change", (v) => {
        value = v;
      });

      this.set("value", {
        items: [{ name: "name", link: { url: "https://giscollective.com" } }],
      });

      await render(hbs`<Input::Menu @value={{this.value}} @onChange={{this.change}}/>`);
    });

    it("renders an item value when there is one", async function () {
      expect(this.element.querySelectorAll(".manage-editors-properties-link-named")).to.have.length(1);
    });

    it("can add a new submenu item with an external link", async function (a) {
      await click(".btn-add-sub-menu-item");

      this.element.querySelector(".open-in-new-tab").value = "yes";
      await triggerEvent(".open-in-new-tab", "change");

      const selectLinks = this.element.querySelectorAll(".ember-basic-dropdown-trigger");

      await selectSearch(selectLinks[0], "https://test.com");
      await selectChoose(selectLinks[0], "https://test.com");

      a.deepEqual(value.toJSON(), {
        items: [
          {
            name: "name",
            link: { url: "https://giscollective.com" },
            dropDown: [{ name: undefined, link: { url: "https://test.com" }, newTab: true }],
          },
        ],
      });
    });
  });

  describe("when there is an item with a submenu on the menu", function (hooks) {
    let value;

    hooks.beforeEach(async function () {
      this.set("change", (v) => {
        value = v;
      });

      this.set("value", {
        items: [
          {
            name: "name",
            link: { url: "https://giscollective.com" },
            dropDown: [
              {
                name: "other name",
                link: { url: "https://giscollective.com" },
              },
            ],
          },
        ],
      });

      await render(hbs`<Input::Menu @value={{this.value}} @onChange={{this.change}}/>`);
    });

    it("renders an item value when there is one", async function () {
      expect(this.element.querySelectorAll(".manage-editors-properties-link-named")).to.have.length(1);

      expect(this.element.querySelectorAll(".input-menu-subitem-group")).to.have.length(1);

      expect(this.element.querySelector(".input-menu-subitem-group .link-name").value).to.equal("other name");
      expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
        "https://giscollective.com",
      );
    });

    it("does not render the menu item link", async function () {
      expect(
        this.element.querySelector(
          ".manage-editors-properties-link-named > .manage-editors-properties-link-named .text-link",
        ),
      ).not.to.exist;
    });

    it("can update a sub item values", async function () {
      await fillIn(".input-menu-subitem-group .link-name", "new name");

      const selectLinks = this.element.querySelectorAll(".ember-basic-dropdown-trigger");

      await selectSearch(selectLinks[0], "https://test.com");
      await selectChoose(selectLinks[0], "https://test.com");

      expect(value.toJSON()).to.deep.equal({
        items: [
          {
            name: "name",
            link: { url: "https://giscollective.com" },
            dropDown: [{ name: "new name", link: { url: "https://test.com" }, newTab: false }],
          },
        ],
      });
    });

    it("can delete a sub item", async function () {
      await click(".input-menu-subitem-group .btn-delete");

      expect(value.toJSON()).to.deep.equal({
        items: [
          {
            name: "name",
            link: { url: "https://giscollective.com" },
            dropDown: [],
          },
        ],
      });
    });

    it("can delete an item", async function () {
      await click(".btn-delete");

      expect(value.toJSON()).to.deep.equal({
        items: [],
      });
    });
  });

  describe("when there are two items on the menu", function (hooks) {
    let value;

    hooks.beforeEach(async function () {
      this.set("change", (v) => {
        value = v;
      });

      this.set("value", {
        items: [
          {
            name: "item 1",
            link: { url: "https://giscollective.com" },
            dropDown: [],
          },
          {
            name: "item 2",
            link: { url: "https://giscollective.com" },
            dropDown: [],
          },
        ],
      });

      await render(hbs`<Input::Menu @value={{this.value}} @onChange={{this.change}}/>`);
    });

    it("allows sorting the elements", async function () {
      const list = document.querySelector(".input-menu-first-level");
      await move(list, 0, list, 1, false, ".handler");

      expect(value.toJSON()).to.deep.equal({
        items: [
          {
            name: "item 2",
            link: { url: "https://giscollective.com" },
            dropDown: [],
          },
          {
            name: "item 1",
            link: { url: "https://giscollective.com" },
            dropDown: [],
          },
        ],
      });
    });
  });

  describe("when there are two subitems items on the menu", function (hooks) {
    let value;

    hooks.beforeEach(async function () {
      this.set("change", (v) => {
        value = v;
      });

      this.set("value", {
        items: [
          {
            name: "item",
            link: { url: "https://giscollective.com" },
            dropDown: [
              {
                name: "sub item 1",
                link: { url: "https://giscollective.com" },
              },
              {
                name: "sub item 2",
                link: { url: "https://giscollective.com" },
              },
            ],
          },
        ],
      });

      await render(hbs`<Input::Menu @value={{this.value}} @onChange={{this.change}}/>`);
    });

    it("allows sorting the elements", async function () {
      const list = document.querySelector(".input-menu-second-level");
      await move(list, 0, list, 1, false, ".handler-sub");

      expect(value.toJSON()).to.deep.equal({
        items: [
          {
            name: "item",
            link: { url: "https://giscollective.com" },
            dropDown: [
              {
                name: "sub item 2",
                link: { url: "https://giscollective.com" },
                newTab: false,
              },
              {
                name: "sub item 1",
                link: { url: "https://giscollective.com" },
                newTab: false,
              },
            ],
          },
        ],
      });
    });
  });
});
