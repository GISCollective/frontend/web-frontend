/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { triggerEvent, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/icon/list", function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultFeature("1");
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the selected source", async function () {
    this.set("value", {
      data: { source: { model: "feature", id: "1" } },
    });

    await render(hbs`<Editor::Icon::List @value={{this.value}} />`);

    expect(this.element.querySelector(".use-current-model").value).to.equal("no");
    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "Nomadisch Grün - Local Urban Food",
    );
  });

  it("can change the value", async function () {
    this.set("value", {
      data: { source: { model: "feature", id: "1" } },
    });

    let changedValue;
    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Icon::List @value={{this.value}} @onChange={{this.onChange}} />`);

    const options = [...this.element.querySelectorAll(".model-name option")].map((a) => a.textContent.trim());
    expect(options).to.deep.equal(["feature", "map", "campaign"]);

    this.element.querySelector(".use-current-model").value = "yes";
    await triggerEvent(".use-current-model", "change");

    expect(changedValue).to.deep.equal({
      source: {
        useSelectedModel: true,
      },
    });
  });
});
