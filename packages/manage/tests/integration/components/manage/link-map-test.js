/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { LinkMap } from 'models/transforms/link-map';
import { expect } from 'chai';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

module('Integration | Component | manage/link-map', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders the hash value', async function (a) {
      this.set(
        'value',
        new LinkMap({
          a: {
            route: 'b',
          },
          c: {
            path: 'd',
          },
        }),
      );

      await render(hbs`<Manage::LinkMap @value={{this.value}} @title="redirects" />`);

      const items = [...this.element.querySelectorAll('.redirect-item')].map((a) =>
        a.textContent
          .split(' ')
          .map((b) => b.trim())
          .filter((a) => a)
          .join(' '),
      );

      expect(items[0]).to.equal('from a to route b');
      expect(items[1]).to.equal('from c to path d');
    });
  });

  describe('in edit mode', function (hooks) {
    let value;

    hooks.beforeEach(function () {
      value = undefined;

      this.set('change', function (v) {
        value = v;
      });
    });

    it('renders the hash value', async function (a) {
      this.set(
        'value',
        new LinkMap({
          a: {
            url: 'https://giscollective.com',
          },
          c: {
            path: 'd',
          },
        }),
      );

      await render(hbs`<Manage::LinkMap @value={{this.value}} @title="redirects" @editablePanel="redirects" />`);

      expect(this.element.querySelector('.redirect-item input').value).to.equal('a');
      expect(
        this.element.querySelector('.redirect-item .ember-power-select-selected-item').textContent.trim(),
      ).to.equal('https://giscollective.com');
    });

    it('can change a source value', async function (a) {
      this.set(
        'value',
        new LinkMap({
          a: {
            url: 'https://giscollective.com',
          },
          c: {
            path: 'd',
          },
        }),
      );

      await render(
        hbs`<Manage::LinkMap @value={{this.value}} @onSave={{this.change}} @title="redirects" @editablePanel="redirects" />`,
      );

      await fillIn('.redirect-item input', 'link-1');
      await click('.btn-submit');

      expect(value.toJSON()).to.deep.equal({
        'link-1': { url: 'https://giscollective.com' },
        c: { path: 'd' },
      });
    });

    it('can change a destination value', async function (a) {
      this.set(
        'value',
        new LinkMap({
          a: {
            url: 'https://giscollective.com',
          },
          c: {
            path: 'd',
          },
        }),
      );

      await render(
        hbs`<Manage::LinkMap @value={{this.value}} @onSave={{this.change}} @title="redirects" @editablePanel="redirects" />`,
      );

      await selectSearch('.ember-power-select-trigger', 'https://other.com');
      await selectChoose('.ember-power-select-trigger', 'https://other.com');

      await click('.btn-submit');

      expect(value.toJSON()).to.deep.equal({
        a: { url: 'https://other.com' },
        c: { path: 'd' },
      });
    });

    it('can delete a row', async function (a) {
      this.set(
        'value',
        new LinkMap({
          a: {
            url: 'https://giscollective.com',
          },
          c: {
            path: 'd',
          },
        }),
      );

      await render(
        hbs`<Manage::LinkMap @value={{this.value}} @onSave={{this.change}} @title="redirects" @editablePanel="redirects" />`,
      );

      await click('.btn-delete');
      await click('.btn-submit');

      expect(value.toJSON()).to.deep.equal({
        c: { path: 'd' },
      });
    });

    it('can cancel the changes', async function (a) {
      this.set(
        'value',
        new LinkMap({
          a: {
            url: 'https://giscollective.com',
          },
          c: {
            path: 'd',
          },
        }),
      );

      let called;
      this.set('cancel', () => {
        called = true;
      });

      await render(
        hbs`<Manage::LinkMap @value={{this.value}} @onSave={{this.change}} @onCancel={{this.cancel}} @title="redirects" @editablePanel="redirects" />`,
      );

      await selectSearch('.ember-power-select-trigger', 'https://other.com');
      await selectChoose('.ember-power-select-trigger', 'https://other.com');

      await click('.btn-cancel');

      expect(value).not.to.exist;
      expect(called).to.equal(true);
    });
  });
});
