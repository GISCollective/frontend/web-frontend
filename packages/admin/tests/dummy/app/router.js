/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('login', function () {
  });

  this.route('admin', function () {
    this.route('appearance');
    this.route('registration');

    this.route('users', function () {
      this.route('edit', { path: ':id' });
    });
    this.route('smtp');
    this.route('captcha');
    this.route('access');
    this.route('location-services');
    this.route('integrations');
  });

  this.route('browse', function () {
    this.route('profiles', function () {
      this.route('profile', { path: ':id' });
    });
  });
});
