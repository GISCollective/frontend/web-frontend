/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddCalendarController extends Controller {
  @service intl;
  @service router;
  @tracked team;

  destination = 'manage.calendars.edit';

  get editableModel() {
    return this.model.calendar;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.team || this.model.calendar.visibility?.team;

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new calendar'),
      capitalize: true,
    });

    return pieces;
  }

  @action
  changeTeam(team) {
    this.team = team;
  }

  @action
  async save() {
    let result;

    try {
      result = await this.editableModel.save();
    } catch (err) {
      return this.notifications.handleError(err);
    }

    this.router.transitionTo('manage.calendars.edit', result.id);

    return result;
  }
}
