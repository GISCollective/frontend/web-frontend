/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export const validBsSizes = ['sm', 'md', 'lg', 'xl', 'xxl'];

function countDashes(value) {
  return value.split('-').length - 1;
}

export function isValidBsSize(size) {
  return validBsSizes.indexOf(size) > -1;
}

export function hasBsSize(value, size) {
  return value.indexOf(`-${size}-`) != -1;
}

export function isPropertyWithoutSize(value) {
  const sizesWithDashes = validBsSizes.map((a) => `-${a}-`);

  const exists = sizesWithDashes.find((a) => value.indexOf(a) != -1);

  return !exists;
}

export function setDefaultBsProperty(property, value, options) {
  const newOptions =
    options?.slice().filter?.((a) => !(a.indexOf(`${property}-`) == 0 && isPropertyWithoutSize(a))) ?? [];

  if (value != 'default') {
    newOptions.push(`${property}-${value}`);
  }

  return newOptions.filter((a) => a.indexOf('-undefined') == -1);
}

export function withoutBsSize(value) {
  const pieces = value.split('-');

  return pieces.filter((a) => !validBsSizes.find((b) => a == b)).join('-');
}

export function isAnyOfBsProperty(value, propertyList) {
  return propertyList.find((a) => value.indexOf(`${a}-`) == 0);
}

export function extractProperty(value) {
  if (isPropertyWithoutSize(value)) {
    return null;
  }

  const pieces = value.split('-');
  const result = [];

  for (const item of pieces) {
    if (validBsSizes.find((a) => a == item)) {
      break;
    }

    result.push(item);
  }

  return result.join('-');
}

export function hasBsSizedVersion(property, allProperties) {
  const propertyPieces = property.split('-');
  const sizedProperties = allProperties.filter((a) => !isPropertyWithoutSize(a));

  for (let currentProperty of sizedProperties) {
    const currentPieces = currentProperty.split('-');

    const matchedPieces = currentPieces.map((a) => propertyPieces.find((b) => b == a)).filter((a) => a);

    if (matchedPieces.join('-') == property) {
      return true;
    }
  }

  return false;
}

export function setSizedBsProperty(property, size, value, options) {
  if (!isValidBsSize(size) || size == 'sm') {
    return setDefaultBsProperty(property, value, options);
  }

  const expectedDashes = countDashes(property) + 2;

  const newOptions =
    options?.slice().filter((a) => !(a.indexOf(`${property}-${size}-`) == 0 && countDashes(a) == expectedDashes)) ?? [];

  if (value != 'default') {
    newOptions.push(`${property}-${size}-${value}`);
  }

  return newOptions;
}

export function defaultBsProperty(property, options) {
  if (!options) {
    return 'default';
  }

  const key = `${property}-`;

  const defaultCols = options.filter?.((a) => a.indexOf(key) == 0 && isPropertyWithoutSize(a)) ?? [];

  if (!defaultCols.length) {
    return 'default';
  }

  const pieces = defaultCols[0].substr(key.length).split('-');

  return pieces.join('-');
}

export function sizedBsProperty(property, size, options) {
  if (!isValidBsSize(size) || size == 'sm') {
    return defaultBsProperty(property, options);
  }

  const defaultCols = options?.filter?.((a) => a.indexOf(`${property}-${size}-`) == 0) ?? [];

  if (!defaultCols.length) {
    return 'default';
  }

  const pieces = defaultCols[0].split('-');

  return pieces[pieces.length - 1];
}
