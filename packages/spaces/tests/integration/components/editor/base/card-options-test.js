/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, click, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/base/card-options", function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let spaceData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam("1");
    server.testData.storage.addDefaultTeam("2");
    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.testData.create.pagesMap = {
      campaigns: "1",
      "campaigns--:campaign-id": "2",
      "help--terms": "3",
    };

    this.set("space", await this.store.findRecord("space", spaceData._id));

    this.space._getPagesMap = {
      campaigns: "1",
      "campaigns--:campaign-id": "2",
      "help--terms": "3",
    };
  });

  it("allows to change the columns value for each size", async function () {
    this.set("value", {});

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Base::CardOptions @deviceSize={{this.deviceSize}} @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    this.element.querySelector(".sm-width-select .input-width-col").value = "2";
    await triggerEvent(".sm-width-select .input-width-col", "change");

    this.element.querySelector(".md-width-select .input-width-col").value = "3";
    await triggerEvent(".md-width-select .input-width-col", "change");

    this.element.querySelector(".lg-width-select .input-width-col").value = "4";
    await triggerEvent(".lg-width-select .input-width-col", "change");

    expect(value.columns.classes).to.deep.equal(["col-2", "col-md-3", "col-lg-4"]);
  });

  it("does not show the title and description options for the cover cards", async function () {
    this.set("value", {
      components: { cardType: "cover" },
    });

    await render(
      hbs`<Editor::Base::CardOptions @deviceSize={{this.deviceSize}} @value={{this.value}} @space={{this.space}} />`,
    );

    expect(this.element.querySelector(".title-customization")).not.to.exist;
    expect(this.element.querySelector(".description-customization")).not.to.exist;
  });

  it("allows to change the card type", async function (a) {
    this.set("value", {
      components: { cardType: "cover" },
      picture: { classes: [] },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Base::CardOptions @deviceSize={{this.deviceSize}} @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    expect(this.element.querySelector(".btn-card-cover")).to.have.class("selected");

    await click(".btn-card-cover-title");

    expect(this.element.querySelector(".btn-card-cover-title")).to.have.class("selected");

    a.deepEqual(value, {
      components: { cardType: "cover-title" },
      destination: { slug: undefined },
      title: {},
      description: {},
      columns: {},
      extraFields: {
        fullDescription: false,
      },
      picture: { proportion: "1:1", classes: [], defaultCover: "default" },
    });
  });

  it("allows to change the card padding", async function (a) {
    this.set("value", {
      components: { cardType: "cover" },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Base::CardOptions @deviceSize={{this.deviceSize}} @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    this.element.querySelector(".sm-top-padding").value = "1";
    await triggerEvent(".sm-top-padding", "change");

    a.deepEqual(value.columns, { classes: ["pt-1"] });
  });

  it("allows to change column justify", async function (a) {
    this.set("value", {});

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Base::CardOptions @deviceSize={{this.deviceSize}} @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    this.element.querySelector(".input-sm-justify-content").value = "center";
    await triggerEvent(".input-sm-justify-content", "change");

    this.element.querySelector(".input-md-justify-content").value = "start";
    await triggerEvent(".input-md-justify-content", "change");

    this.element.querySelector(".input-lg-justify-content").value = "end";
    await triggerEvent(".input-lg-justify-content", "change");

    a.deepEqual(value.columns.containerClasses, [
      "justify-content-center",
      "justify-content-md-start",
      "justify-content-lg-end",
    ]);
  });

  it("allows customizing the title", async function (a) {
    this.set("value", {
      components: {
        cover: true,
        name: true,
        description: true,
      },
      title: {
        classes: ["fw-bold", "text-center"],
        color: "green",
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Base::CardOptions @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    expect(this.element.querySelector(".title-customization .input-sm-fw").value).to.equal("bold");
    expect(this.element.querySelector(".title-customization .input-sm-text").value).to.equal("center");
    expect(this.element.querySelector(".title-customization .bs-color-picker .selected-value")).to.have.class(
      "bg-green",
    );

    this.element.querySelector(".title-customization .input-sm-fw").value = "light";
    await triggerEvent(".title-customization .input-sm-fw", "change");

    this.element.querySelector(".title-customization .input-sm-text").value = "end";
    await triggerEvent(".title-customization .input-sm-text", "change");

    await click(".title-customization .bs-color-picker");
    await click(".title-customization .btn-color-warning");

    a.deepEqual(value, {
      components: { cardType: "cover-title-description" },
      title: {
        classes: ["fw-light", "text-end"],
        color: "warning",
        minLines: { lg: 0, md: 0, sm: 0 },
      },
      description: {},
      destination: { slug: undefined },
      columns: {},
      picture: { proportion: "1:1", defaultCover: "default" },
      extraFields: {
        fullDescription: false,
      },
    });
  });

  it("allows customizing the description", async function (a) {
    this.set("value", {
      components: {
        cover: true,
        name: true,
        description: true,
      },
      description: {
        classes: ["fw-bold", "text-center"],
        color: "green",
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Base::CardOptions @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    expect(this.element.querySelector(".description-customization .input-sm-fw").value).to.equal("bold");
    expect(this.element.querySelector(".description-customization .input-sm-text").value).to.equal("center");
    expect(this.element.querySelector(".description-customization .bs-color-picker .selected-value")).to.have.class(
      "bg-green",
    );

    this.element.querySelector(".description-customization .input-sm-fw").value = "light";
    await triggerEvent(".description-customization .input-sm-fw", "change");

    this.element.querySelector(".description-customization .input-sm-text").value = "end";
    await triggerEvent(".description-customization .input-sm-text", "change");

    await click(".description-customization .bs-color-picker");
    await click(".description-customization .btn-color-warning");

    a.deepEqual(value, {
      components: { cardType: "cover-title-description" },
      description: { classes: ["fw-light", "text-end"], color: "warning", minLines: { lg: 0, md: 0, sm: 0 } },
      destination: { slug: undefined },
      columns: {},
      title: {},
      picture: { proportion: "1:1", defaultCover: "default" },
      extraFields: {
        fullDescription: false,
      },
    });
  });

  it("allows customizing the cover", async function () {
    this.set("value", {
      components: {
        cover: true,
        name: true,
        description: true,
      },
      picture: {
        proportion: "1:2",
        classes: ["border-visibility-no", "border-size-2", "border-radius-3", "border-color-success"],
      },
      extraFields: {
        fullDescription: false,
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Base::CardOptions @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}}/>`,
    );

    const coverCustomization = this.element.querySelector(".cover-customization");

    expect(coverCustomization.querySelector(".picture-proportion").value).to.equal("1:2");
    expect(coverCustomization.querySelector(".input-sm-border-radius").value).to.equal("3");
    expect(coverCustomization.querySelector(".input-sm-border-size").value).to.equal("2");
    expect(coverCustomization.querySelector(".bs-color-picker .selected-value")).to.have.class("bg-success");

    await fillIn(".picture-proportion", "3:4");

    coverCustomization.querySelector(".input-sm-border-visibility").value = "yes";
    await triggerEvent(".cover-customization .input-sm-border-visibility", "change");

    this.element.querySelector(".cover-customization .input-sm-border-size").value = "4";
    await triggerEvent(".cover-customization .input-sm-border-size", "change");

    this.element.querySelector(".cover-customization .input-sm-border-radius").value = "4";
    await triggerEvent(".cover-customization .input-sm-border-radius", "change");

    await click(".cover-customization .bs-color-picker");
    await click(".cover-customization .btn-color-warning");

    this.element.querySelector(".cover-customization .default-cover").value = "from parent";
    await triggerEvent(".cover-customization .default-cover", "change");

    expect(value).to.deep.equal({
      components: { cardType: "cover-title-description" },
      description: {},
      destination: { slug: undefined },
      columns: {},
      title: {},
      picture: {
        proportion: "3:4",
        defaultCover: "from parent",
        classes: ["border-visibility-yes", "border-size-4", "border-radius-4", "border-color-warning"],
      },
      extraFields: {
        fullDescription: false,
      },
    });
  });

  describe("destination", async function (hooks) {
    hooks.beforeEach(async function () {
      server.testData.create.pagesMap = {
        campaigns: "1",
        "campaigns--:campaign-id": "2",
        "help--terms": "3",
        "campaigns--:event-id": "4",
        "campaigns--:feature-id": "5",
      };

      this.set("space", await this.store.findRecord("space", spaceData._id));

      this.space._getPagesMap = {
        campaigns: "1",
        "campaigns--:campaign-id": "2",
        "help--terms": "3",
        "campaigns--:event-id": "4",
        "campaigns--:feature-id": "5",
      };
    });

    it("allows changing the destination page", async function (a) {
      this.set("value", {});

      let value;
      this.set("onChange", (v) => {
        value = v;
      });

      await render(
        hbs`<Editor::Base::CardOptions @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      const options = [...this.element.querySelector(".options-select")].map((a) => a.value);

      this.element.querySelector(".options-select").value = "/campaigns/:campaign-id";
      await triggerEvent(".options-select", "change");

      expect(options).to.deep.equal([
        "",
        "/campaigns",
        "/campaigns/:campaign-id",
        "/help/terms",
        "/campaigns/:event-id",
        "/campaigns/:feature-id",
      ]);
      expect(value).to.deep.equal({
        destination: { slug: "/campaigns/:campaign-id" },
        columns: {},
        components: { cardType: "cover-title-description" },
        title: {},
        description: {},
        picture: { proportion: "1:1", defaultCover: "default" },
        extraFields: {
          fullDescription: false,
        },
      });
    });

    it("shows only the model pages when the model name is set", async function (a) {
      this.set("value", {});

      let value;
      this.set("onChange", (v) => {
        value = v;
      });

      await render(
        hbs`<Editor::Base::CardOptions @modelType="campaign" @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      const options = [...this.element.querySelector(".options-select")].map((a) => a.value);

      this.element.querySelector(".options-select").value = "/campaigns/:campaign-id";
      await triggerEvent(".options-select", "change");

      expect(options).to.deep.equal(["", "/campaigns/:campaign-id"]);
    });

    it("shows both the event and feature urls for an event id", async function (a) {
      this.set("value", {});

      let value;
      this.set("onChange", (v) => {
        value = v;
      });

      await render(
        hbs`<Editor::Base::CardOptions @modelType="event" @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      const options = [...this.element.querySelector(".options-select")].map((a) => a.value);

      this.element.querySelector(".options-select").value = "/campaigns/:campaign-id";
      await triggerEvent(".options-select", "change");

      expect(options).to.deep.equal(["", "/campaigns/:event-id", "/campaigns/:feature-id"]);
    });

    it("renders the destination value when is set", async function () {
      this.set("value", {
        destination: { slug: "/campaigns/:campaign-id" },
      });

      this.set("onChange", () => {});

      await render(
        hbs`<Editor::Base::CardOptions @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`,
      );

      expect(this.element.querySelector(".options-select").value).to.equal("/campaigns/:campaign-id");
    });
  });
});
