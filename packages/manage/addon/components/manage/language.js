import Component from '@glimmer/component';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageLanguage extends Component {
  @service user;
  @tracked _value;

  get value() {
    let value = this._value || this.args.value;

    return this.user.languages.find((a) => a.locale == value);
  }

  @action
  change(value) {
    this._value = value;
  }

  @action
  save() {
    return this.args.onSave?.(this.value.locale);
  }

  @action
  cancel() {
    this._value = undefined;
    return this.args.onCancel();
  }
}
