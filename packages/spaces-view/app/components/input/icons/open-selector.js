/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './selector';

export default class InputIconsOpenSelectorComponent extends Component {
  get icons() {
    return this.group(this.args.icons);
  }

  group(icons) {
    const list = [];
    for (const categoryName of Object.keys(icons?.categories ?? {})) {
      const groupList = this.group(icons.categories[categoryName]);

      const sum = groupList
        .map((a) => a.icons.length)
        .reduce((a, b) => a + b, 0);

      if (sum <= 4) {
        list.push({
          name: [categoryName],
          icons: groupList.flatMap((a) => a.icons),
        });
      }

      if (sum > 4) {
        for (const group of groupList) {
          list.push({
            name: [categoryName, ...group.name],
            icons: group.icons,
          });
        }
      }
    }

    if (icons?.icons?.length > 0) {
      list.push({
        name: [],
        icons: icons.icons,
      });
    }

    return list;
  }
}
