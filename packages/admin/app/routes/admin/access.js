/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/authenticated-admin-route';
import { hash } from 'rsvp';

export default class AdminAccessRoute extends Route {
  model() {
    return hash({
      allowManageWithoutTeams: this.preferences.requestPreference('access.allowManageWithoutTeams'),
      isMultiProjectMode: this.preferences.requestPreference('access.isMultiProjectMode'),
    });
  }
}
