/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  setupApplicationTest as upstreamSetupApplicationTest,
  setupRenderingTest as upstreamSetupRenderingTest,
  setupTest as upstreamSetupTest,
} from 'ember-qunit';
import QUnit from 'qunit';
import TestServer from 'models/test-support/gis-collective/test-server';

// This file exists to provide wrappers around ember-qunit's
// test setup functions. This way, you can easily extend the setup that is
// needed per test type.

function setupTest(hooks, options) {
  upstreamSetupTest(hooks, options);
  setupIntl(hooks, "en-us");

  hooks.beforeEach(function () {
    this.store = this.owner.lookup('service:store');
  });

  hooks.beforeEach(function (assert) {
    assert.ok(1);
  });

  return hooks;
}

function setupRenderingTest(hooks, options) {
  QUnit.config.testTimeout = 15000;
  QUnit.config.failOnZeroTests = false;

  upstreamSetupRenderingTest(hooks, options);

  hooks.beforeEach(function (assert) {
    if (options?.server) {
      this.server = new TestServer();
      this.storage = this.server.testData.storage;
    }

    const intl = this.owner.lookup('service:intl');
    intl.setLocale('en-us');

    this.store = this.owner.lookup('service:store');
    this.router = this.owner.lookup('service:router');
    this.editorJs = this.owner.lookup('service:editor-js');

    assert.ok(1);
  });

  hooks.afterEach(async function () {
    if (options?.server) {
      this.server.shutdown();
    }
  });

  return hooks;
}


function setupApplicationTest(hooks, options) {
  QUnit.config.testTimeout = 15000;
  QUnit.config.failOnZeroTests = false;

  upstreamSetupApplicationTest(hooks, options);

  hooks.beforeEach(function (assert) {
    assert.ok(1);
  });

  hooks.beforeEach(function () {
    if (options?.server) {
      this.server = new TestServer();
      this.storage = this.server.testData.storage;
    }

    this.store = this.owner.lookup('service:store');
    this.router = this.owner.lookup('service:router');
  });

  hooks.afterEach(function () {
    if (options?.server) {
      this.server.shutdown();
    }
  });
}

import { module, test } from 'qunit';

function wait(duration) {
  return new Promise((resolve) => {
    setTimeout(resolve, duration);
  });
}

export { setupApplicationTest, setupRenderingTest, setupTest, module as describe, test as it, wait };
