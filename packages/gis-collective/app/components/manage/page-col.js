/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { PageCol } from 'models/transforms/page-col-list';
import { service } from '@ember/service';
import { getOwner } from '@ember/application';

export default class ManagePageColComponent extends Component {
  @service pageCols;
  @service store;
  @service spaces;
  @tracked _type = null;

  get componentName() {
    return 'editor/' + this.type;
  }

  get hasEditor() {
    const owner = getOwner(this);
    const lookup = owner.lookup('component-lookup:main');
    if (!lookup.componentFor) {
      return !!lookup.lookupFactory(this.componentName);
    }

    return !!(lookup.componentFor(this.componentName, owner) || lookup.layoutFor(this.componentName, owner));
  }

  get type() {
    if (this._type) {
      return this._type;
    }

    return this.args.value?.type;
  }

  onChange(newCol) {
    if (!isNaN(this.args.container)) {
      newCol.container = this.args.container;
    }

    if (!isNaN(this.args.row)) {
      newCol.row = this.args.row;
    }

    if (!isNaN(this.args.col)) {
      newCol.col = this.args.col;
    }

    return this.args.onChange?.(newCol);
  }

  @action
  typeChanged(type) {
    if (this.type == type) {
      return;
    }

    this._type = type;
    const col = new PageCol(this.args.value);
    col.data = {};
    col.type = type;

    this.onChange(col);
  }

  @action
  dataChanged(data) {
    if (JSON.stringify(this.args.value?.data) == JSON.stringify(data)) {
      return;
    }

    const col = new PageCol(this.args.value);
    col.data = data;
    col.type = this.type;

    this.onChange(col);
  }
}
