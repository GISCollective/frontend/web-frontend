import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/survey-questions', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders a warning message by default', async function (assert) {
      this.set('value', []);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.alert.alert-warning')).to.have.textContent(
        'This survey has no general questions',
      );
    });

    it('renders the position question when it is set', async function (assert) {
      this.set('value', [
        {
          type: 'geo json',
          name: 'position',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.question')).to.have.textContent('Location *');
      expect(this.element.querySelector('.type')).to.have.textContent('geo json');
    });

    it('renders the name question when it is set', async function (assert) {
      this.set('value', [
        {
          type: 'short text',
          name: 'name',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.question')).to.have.textContent("What's the name of this place? *");
      expect(this.element.querySelector('.type')).to.have.textContent('short text');
    });

    it('renders the description question when it is set', async function (assert) {
      this.set('value', [
        {
          type: 'long text',
          name: 'description',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.question')).to.have.textContent('What do you like about this place? *');
      expect(this.element.querySelector('.type')).to.have.textContent('long text');
    });

    it('renders the icons question when it is set', async function (assert) {
      this.set('value', [
        {
          type: 'icons',
          name: 'icons',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.question')).to.have.textContent('Please select an icon from the list *');
      expect(this.element.querySelector('.type')).to.have.textContent('icons');
    });

    it('renders the pictures question when it is set', async function (assert) {
      this.set('value', [
        {
          type: 'pictures',
          name: 'pictures',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.question')).to.have.textContent('Pictures *');
      expect(this.element.querySelector('.type')).to.have.textContent('pictures');
    });

    it('renders the sounds question when it is set', async function (assert) {
      this.set('value', [
        {
          type: 'sounds',
          name: 'sounds',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.question')).to.have.textContent('Sounds *');
      expect(this.element.querySelector('.type')).to.have.textContent('sounds');
    });
  });

  describe('in edit mode', function () {
    it('renders the default questions when there are none', async function (assert) {
      this.set('value', []);

      await render(hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} @editablePanel="test" />`);

      const alerts = [...this.element.querySelectorAll('.alert')];

      expect(alerts[0]).to.have.textContent("The user won't be asked to set a location.");
      expect(alerts[1]).to.have.textContent("The user won't be asked to enter a name.");
      expect(alerts[2]).to.have.textContent("The user won't be asked to enter a description.");
      expect(alerts[3]).to.have.textContent("The user won't be asked to pick icons.");
      expect(alerts[4]).to.have.textContent("The user won't be asked to upload pictures.");
      expect(alerts[5]).to.have.textContent("The user won't be asked to upload sounds.");
    });

    it('can enable all questions', async function (assert) {
      this.set('value', []);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} @editablePanel="test" @onSave={{this.save}} />`,
      );

      const buttons = [...this.element.querySelectorAll('.btn-toggle-visibility')];

      await Promise.all(buttons.map((a) => click(a)));

      await click('.btn-submit');

      expect(value.map((a) => ({ name: a.name, isVisible: a.isVisible }))).to.deep.equal([
        { name: 'position', isVisible: true },
        { name: 'name', isVisible: true },
        { name: 'description', isVisible: true },
        { name: 'icons', isVisible: true },
        { name: 'pictures', isVisible: true },
        { name: 'sounds', isVisible: true },
      ]);
    });

    it('render a value', async function (assert) {
      this.set('value', [
        {
          type: 'geo json',
          name: 'position',
          isRequired: true,
          isVisible: true,
        },
      ]);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::SurveyQuestions @title="test" @value={{this.value}} @editablePanel="test" @onSave={{this.save}} />`,
      );

      expect(this.element.querySelector('.text-question').value).to.equal('Location');
    });
  });
});
