import Component from './card';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { dasherize } from '@ember/string';

export default class CardDashboard extends Component {
  @service intl;
  @tracked cover;

  get route() {
    const name = this.args.record.constructor.modelName;
    const dasherizedName = dasherize(name);

    if(dasherizedName == "campaign") {
      return "manage.dashboards.survey";
    }

    if(dasherizedName == "base-map") {
      return "manage.basemaps.edit";
    }

    if(dasherizedName == "data-binding") {
      return "manage.databindings.edit";
    }

    if(dasherizedName == "presentation") {
      return "manage.presentations.edit";
    }

    return `manage.dashboards.${dasherizedName}`;
  }

  get firstParagraph() {
    if (!this.args?.record?.firstParagraph) {
      return this.intl.t('message-no-feature-description');
    }

    return this.args?.record?.firstParagraph;
  }

  get visibilityClass() {
    if (this.args.record?.visibility?.isPublic == 1) {
      return 'success';
    }

    return 'warning';
  }

  get hasCover() {
    return this.cover?.picture;
  }

  get bgCover() {
    if (!this.cover) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${this.cover.picture}/md')`);
  }

  get visibilityText() {
    if (this.args.record?.visibility?.isPublic == 1) {
      return 'visibility-public';
    }

    return 'visibility-private';
  }

  @action
  async setup() {
    this.cover = await this.args.record?.getCover?.();
  }
}
