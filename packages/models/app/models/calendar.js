/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import PrimaryModel from './base/primary-model';
import { attr, belongsTo } from '@ember-data/model';
import { OptionalMap } from 'models/transforms/optional-map';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';

export default class CalendarModel extends PrimaryModel {
  @attr('string') name;
  @attr article;

  @attr('model-list-with-default', { model: 'icon-set' })
  iconSets;

  @attr('optional-map', { async: true, inverse: null, defaultValue: () => new OptionalMap() })
  map;

  @attr('string', { defaultValue: 'events' })
  type;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr('icon-attribute-definition-list')
  attributes;

  @attr('string')
  recordName;

  get hasIcons() {
    return this.iconSets?.idList?.length;
  }

  get title() {
    return this.name;
  }

  get contentBlocks() {
    return toContentBlocks(this.article, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  set descriptionContentBlocks(content) {
    this.description = toContentBlocks(content, this.name);
  }

  get articleContentBlocks() {
    return toContentBlocks(this.article);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  getAttributeValues(attribute, term = '', limit = 5) {
    return this.store.adapterFor(this.constructor.modelName).getAttributeValues(this.id, attribute, term, limit);
  }
}
