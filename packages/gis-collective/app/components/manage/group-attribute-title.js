/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class ManageGroupAttributeTitleComponent extends Component {
  get icon() {
    if (!this.args.group || !this.args.icons?.length) {
      return null;
    }

    const lowerGroup = this.args.group.trim().toLowerCase();
    const result = this.args.icons.filter((a) => {
      const otherNames = a.otherNames ? a.otherNames : [];

      return (
        [a.name, ...otherNames]
          .filter((a) => a)
          .map((a) => a.trim().toLowerCase())
          .indexOf(lowerGroup) != -1
      );
    });

    if (result.length == 0) {
      return null;
    }

    return result[0];
  }
}
