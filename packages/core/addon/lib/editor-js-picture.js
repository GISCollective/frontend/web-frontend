/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { buildWaiter } from '@ember/test-waiters';
import PictureMeta from 'models/lib/picture-meta';
const waiter = buildWaiter('picture-upload-waiter');

export async function onPictureUpload(value, type, store, link, disableOptimization, onPictureCreated) {
  if (type == 'url') {
    return {
      success: 1,
      file: {
        url: value,
      },
    };
  }
  let token = waiter.beginAsync();

  return new Promise((resolve) => {
    var reader = new FileReader();
    reader.readAsDataURL(value);

    reader.onload = async (event) => {
      const picture = store.createRecord('picture', {
        name: value.name,
        picture: event.target.result,
        meta: new PictureMeta({
          link,
          disableOptimization,
        }),
      });

      let success = 1;
      let url = '';

      try {
        await picture.save();
        url = picture.picture + '/lg';
        await onPictureCreated?.(picture);
      } catch (err) {
        console.error(err);
        success = 0;
      }

      resolve({
        success,
        file: { url },
      });
    };

    reader.onerror = function () {
      resolve({
        success: 0,
        file: {
          url: '',
        },
      });
    };
  }).finally(() => {
    waiter.endAsync(token);
  });
}
