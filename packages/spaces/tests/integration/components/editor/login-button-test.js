import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

describe("Integration | Component | editor/login-button", function (hooks) {
  setupRenderingTest(hooks);

  it("renders a value", async function (assert) {
    this.set("value", {
      style: {
        classes: ["btn-primary"],
      },
    });

    await render(hbs`<Editor::LoginButton @value={{this.value}}/>`);

    expect(this.element.querySelector(".input-sm-btn").value).to.equal("primary");
  });

  it("can change the value", async function (a) {
    this.set("value", {
      style: {
        classes: ["btn-primary"],
      },
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::LoginButton @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".input-sm-btn").value = "secondary";
    await triggerEvent(".input-sm-btn", "change");

    a.deepEqual(value, {
      style: {
        classes: ["btn-secondary"],
      },
    });
  });
});
