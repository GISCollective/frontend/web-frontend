const cache: Record<string, string> = {};

export function dasherize(str: string) {
  if (!cache[str]) {
    cache[str] = str
      .replace(/([a-z\d])([A-Z])/g, "$1-$2")
      .replace(/[ _]/g, "-")
      .toLowerCase();
  }

  return cache[str];
}
