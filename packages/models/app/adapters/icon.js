/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ApplicationAdapter from './application';

export default class IconAdapter extends ApplicationAdapter {
  modelName = 'icon';

  group(query = {}) {
    query['group'] = true;
    const url = this.buildURL('icon');

    return this.ajax(url, 'GET', { data: query });
  }
}
