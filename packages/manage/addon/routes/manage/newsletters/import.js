/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageNewslettersImportRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.isFastBoot) {
      return null;
    }

    const newsletter = await this.store.findRecord('newsletter', params.id);

    return hash({
      newsletter,
      team: newsletter.visibility.fetchTeam(),
    });
  }
}
