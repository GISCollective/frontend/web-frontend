/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class ManageEditorsPropertiesSourceGroupStyleComponent extends Component {
  @tracked _classes;
  @tracked _color;
  @tracked _accentColor;
  @tracked _maxItems;

  get color() {
    if (this._color) {
      return this._color;
    }

    return this.args.value?.color ?? "";
  }

  get maxItems() {
    if (this._maxItems) {
      return this._maxItems;
    }

    return this.args.value?.maxItems ?? {};
  }

  get accentColor() {
    if (this._accentColor) {
      return this._accentColor;
    }

    return this.args.value?.accentColor ?? "";
  }

  get classes() {
    if (this._classes) {
      return this._classes;
    }

    return this.args.value?.classes ?? [];
  }

  @action
  changeClasses(value) {
    this._classes = value;

    this.triggerChange();
  }

  @action
  changeColor(value) {
    this._color = value;

    this.triggerChange();
  }

  @action
  changeAccentColor(value) {
    this._accentColor = value;

    this.triggerChange();
  }

  @action
  changeMaxItems(size, value) {
    const maxItems = this.maxItems;

    maxItems[size] = value;

    this._maxItems = maxItems;

    this.triggerChange();
  }

  triggerChange() {
    const maxItems = {};

    for (const key in this.maxItems) {
      const val = this.maxItems[key];

      if (!isNaN(val)) {
        maxItems[key] = parseInt(val);
      }
    }

    return this.args.onChange?.({
      color: this.color,
      accentColor: this.accentColor,
      classes: this.classes,
      maxItems,
    });
  }
}
