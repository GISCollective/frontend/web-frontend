/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/feature-visibility', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::FeatureVisibility />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders "public" when the value is 1', async function () {
    await render(hbs`<View::FeatureVisibility @value={{1}}/>`);
    expect(this.element.textContent.trim()).to.equal('public');
  });

  it('renders "private" when the value is 0', async function () {
    await render(hbs`<View::FeatureVisibility @value={{0}}/>`);
    expect(this.element.textContent.trim()).to.equal('private');
  });

  it('renders "public" when the value is 1', async function () {
    await render(hbs`<View::FeatureVisibility @value={{-1}}/>`);
    expect(this.element.textContent.trim()).to.equal('pending');
  });
});
