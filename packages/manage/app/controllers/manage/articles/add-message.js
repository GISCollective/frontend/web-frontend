/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class AddArticleController extends Controller {
  @service router;
  @service intl;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        route: 'manage.dashboards.newsletter',
        text: this.model.newsletter.name,
        model: this.model.newsletter?.id,
      },
      {
        text: this.intl.t('new message'),
        capitalize: true,
      },
    ];
  }

  @action
  save() {
    this.router.transitionTo('manage.dashboards.newsletter', this.model.newsletter.id);
  }
}
