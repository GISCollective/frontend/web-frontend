/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class ProfilePictureComponent extends Component {
  get style() {
    if (!this.args.picture || !this.args.picture.get('picture')) {
      return htmlSafe(`background-image: url('/img/user.svg')`);
    }

    const size = this.args.size ? `/${this.args.size}` : '';

    return htmlSafe(`background-image: url('${this.args.picture.get('picture')}${size}')`);
  }
}
