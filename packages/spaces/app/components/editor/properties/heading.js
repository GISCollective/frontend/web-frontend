/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";

export default class ManageEditorsPropertiesHeadingComponent extends Component {
  elementId = `editor-property-${guidFor(this)}`;

  allHeadings = ["1", "2", "3", "4", "5", "6"];

  get value() {
    return `${this.args.value ?? "1"}`;
  }

  @action
  change(value) {
    this.args.onChange?.(parseInt(value));
  }
}
