import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, waitFor, waitUntil } from '@ember/test-helpers';
import { transform } from 'ol/proj';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { expect } from 'chai';
import Service from '@ember/service';


class MockPosition extends Service {
  hasPosition = true;
  center = [0, 0];

  setPosition(value) {
    this.hasPosition = true;
    this.center = value;
  }
}

describe('Integration | Component | map-selectable', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let map;
  let feature;
  let tileHeaders;

  hooks.beforeEach(function () {
    const tiles = this.owner.lookup('service:tiles');
    tiles.url = "/mock-server/tiles";

    server = new TestServer();
    tileHeaders = null;
    server.get("/mock-server/tiles/:z/:x/:y", request => {
      tileHeaders = request.requestHeaders;
      return [200, {}, ""]
    });

    server.testData.storage.addDefaultBaseMap();
    map = server.testData.storage.addDefaultMap();
    feature = server.testData.storage.addDefaultFeature();
    this.owner.register('service:position', MockPosition);
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no map id', async function () {
    await render(hbs`<MapSelectable />`);

    expect(this.element.querySelector(".map-plain-container")).not.to.exist;
  });

  it('renders a map with a vector layer when the id is set', async function () {
    this.set("id", map._id);

    await render(hbs`<MapSelectable @id={{this.id}} />`);
    await waitUntil(() => tileHeaders);

    expect(this.element.querySelector(".map-plain-container")).to.exist;
    expect(this.element.querySelector(".d-none.vector-tiles")).to.have.textContent("/mock-server/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c");
    expect(tileHeaders).to.deep.equal({});
  });

  it('uses the bearer token in the tiles request when set', async function () {
    const tiles = this.owner.lookup('service:tiles');
    tiles.bearer = "Bearer test-token";

    this.set("id", map._id);

    await render(hbs`<MapSelectable @id={{this.id}} />`);
    await waitUntil(() => tileHeaders);

    expect(tileHeaders).to.deep.equal({ Authorization: 'Bearer test-token' });
  });

  it('triggers the onSelect event when a feature is selected', async function () {
    let value;

    this.set("id", map._id);
    this.set("select", (v) => {
      value = v;
    });

    await render(hbs`<MapSelectable @id={{this.id}} @onSelect={{this.select}}/>`);

    await waitFor(".d-none.vector-tiles");
    await waitUntil(() => this.element.querySelector(".d-none.vector-tiles").selectFeature);

    const layer = this.element.querySelector(".d-none.vector-tiles");
    layer.selectFeature({
      getProperties: () => ({
        _id: feature._id
      })
    });

    expect(value).to.deep.equal([feature._id]);
  });

  it('triggers the onSelect event when a group is selected', async function () {
    let value;

    this.set("id", map._id);
    this.set("select", (v) => {
      value = v;
    });

    await render(hbs`<MapSelectable @id={{this.id}} @onSelect={{this.select}}/>`);

    await waitFor(".d-none.vector-tiles");
    await waitUntil(() => this.element.querySelector(".d-none.vector-tiles").selectFeature);

    const layer = this.element.querySelector(".d-none.vector-tiles");
    layer.selectFeature({
      getProperties: () => ({
        layer: "groups",
        idList: "1,2,3"
      })
    });

    expect(value).to.deep.equal(["1", "2", "3"]);
  });

  it('focuses on the extent location when user location is outside the extent', async function () {
    let value;

    this.set("id", map._id);
    this.set("select", (v) => {
      value = v;
    });

    const position = this.owner.lookup('service:position');
    position.setPosition([-22.369218111038208, -44.43939480420836]);

    this.set('extent', {
      type: 'Polygon',
      coordinates: [
        [
          [-10.0, -10.0],
          [-10.0, 50.0],
          [50.0, 50.0],
          [50.0, -10.0],
          [-10.0, -10.0],
        ],
      ],
    });

    this.owner.lookup('service:position').center = [60, 60];

    await render(hbs`<MapSelectable @extent={{this.extent}} @id={{this.id}} @onSelect={{this.select}}/>`);
    const olMap = this.element.querySelector('.map').olMap;

    const center = transform(
      olMap.getView().getCenter(),
      'EPSG:3857',
      'EPSG:4326',
    ).map(a => Math.round(a * 100) / 100);

    expect(center).to.deep.equal([ 20, 23.26 ]);
    expect(olMap.getView().getZoom()).to.be.closeTo(3, 1);
  });

  it('focuses on the user location when it is inside the extent', async function () {
    let value;

    this.set("id", map._id);
    this.set("select", (v) => {
      value = v;
    });

    const position = this.owner.lookup('service:position');
    position.setPosition([22.369218111038208, 44.43939480420836]);

    this.set('extent', {
      type: 'Polygon',
      coordinates: [
        [
          [-10.0, -10.0],
          [-10.0, 50.0],
          [50.0, 50.0],
          [50.0, -10.0],
          [-10.0, -10.0],
        ],
      ],
    });

    await render(hbs`<MapSelectable @extent={{this.extent}} @id={{this.id}} @onSelect={{this.select}}/>`);

    const center = transform(
      [22.369218111038208, 44.43939480420836],
      'EPSG:4326',
      'EPSG:3857',
    );

    const olMap = this.element.querySelector('.map').olMap;

    expect(olMap.getView().getCenter()).to.deep.equal(center);
    expect(olMap.getView().getZoom()).to.equal(18);
  });
});
