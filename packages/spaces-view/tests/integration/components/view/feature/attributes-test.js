/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/feature/attributes', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let feature;
  let store;
  let storedFeature;
  let icon1;

  hooks.beforeEach(async function () {
    server = new TestServer();
    storedFeature = server.testData.create.feature('1');
    storedFeature.attributes = {
      icon1: {
        key1: 'value1',
        key2: 'value2',
      },
      icon2: {
        key3: 'value3',
        key4: 'value4',
      },
    };

    storedFeature.icons = ['1', '2'];

    icon1 = server.testData.create.icon('1');
    const icon2 = server.testData.create.icon('2');
    icon1.attributes = [
      { name: 'key1', displayName: 'local attribute1', type: 'short text' },
      { name: 'key2', displayName: 'local attribute2', type: 'short text' },
    ];
    icon2.attributes = [
      { name: 'key3', displayName: 'local attribute3', type: 'short text' },
      { name: 'key4', displayName: 'local attribute4', type: 'short text' },
    ];

    server.testData.storage.addIcon(icon1);
    server.testData.storage.addIcon(icon2);
    server.testData.storage.addFeature(storedFeature);

    store = this.owner.lookup('service:store');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Feature::Attributes />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a feature attributes when set', async function () {
    feature = await store.findRecord('feature', '1');

    this.set('value', {
      data: {},
      modelKey: 'feature_1',
    });

    this.set('model', {
      feature_1: feature,
    });

    await render(
      hbs`<View::Feature::Attributes @value={{this.value}} @model={{this.model}} />`,
    );

    const headers = [...this.element.querySelectorAll('.card-header')].map(
      (a) => a.textContent.trim(),
    );

    expect(headers).to.deep.equal(['icon1', 'icon2']);

    expect(this.element.textContent.trim()).to.contain('key1');
    expect(this.element.textContent.trim()).to.contain('value1');
    expect(this.element.querySelector('.index')).not.to.exist;
  });

  it('renders the other section without a title', async function () {
    feature = await store.findRecord('feature', '1');
    feature.attributes = {
      other: {
        key1: 'value1',
        key2: 'value2',
      },
    };
    server.testData.storage.addFeature(storedFeature);

    this.set('value', {
      data: {},
      modelKey: 'feature_1',
    });

    this.set('model', {
      feature_1: feature,
    });

    await render(
      hbs`<View::Feature::Attributes @value={{this.value}} @model={{this.model}} />`,
    );

    const headers = [...this.element.querySelectorAll('.card-header')];

    expect(headers).to.have.length(0);

    expect(this.element.textContent.trim()).to.contain('key1');
    expect(this.element.textContent.trim()).to.contain('value1');
    expect(this.element.querySelector('.index')).not.to.exist;
  });

  it('does not render the icons when values are not present', async function () {
    feature = await store.findRecord('feature', '1');
    feature.attributes = {};

    this.set('value', {
      data: {},
      modelKey: 'feature_1',
    });

    this.set('model', {
      feature_1: feature,
    });

    await render(
      hbs`<View::Feature::Attributes @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.textContent.trim()).not.to.contain('icon1');
    expect(this.element.textContent.trim()).not.to.contain('key1');
  });

  it('renders all when there is an list of values for an attribute', async function () {
    feature = await store.findRecord('feature', '1');
    feature.attributes = {
      icon1: [
        { key1: '1', key2: '2' },
        { key1: '3', key2: '4' },
      ],
    };
    icon1.allowMany = true;

    this.set('value', {
      data: {},
      modelKey: 'feature_1',
    });

    this.set('model', {
      feature_1: feature,
    });

    await render(
      hbs`<View::Feature::Attributes @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelectorAll('.card-header').length).to.equal(2);
    expect(
      this.element.querySelectorAll('ul.browse-attributes').length,
    ).to.equal(2);
    expect(this.element.querySelectorAll('.index').length).to.equal(2);
  });
});
