/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { get } from '@ember/object';

export default class PageColContentComponent extends Component {
  get sourcePath() {
    if (this.args.disableSource) {
      return '';
    }
    const pieces = this.path.split('.data.');

    return pieces[0];
  }

  get path() {
    if (!this.args.path) {
      return '.data.style';
    }

    if (this.args.path.includes('.data')) {
      return this.args.path;
    }

    return `${this.args.path}.data.style`;
  }

  get style() {
    if (!this.path) {
      return {};
    }

    const pieces = this.path.split('.data.');

    const result = get(this.args.value, `data.${pieces[1]}`);

    if (typeof result == 'object' && !result.paragraphStyle) {
      result.paragraphStyle = JSON.parse(JSON.stringify(result));
    }

    return result;
  }

  get containerStyle() {
    if (!this.path) {
      return {};
    }

    const pieces = this.path.split('.data.');

    const result = get(this.args.value, `data.${pieces[1]}.container`);

    return result;
  }
}
