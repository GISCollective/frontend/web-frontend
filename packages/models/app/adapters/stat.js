/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import RESTAdapter from './application';

export default class ApplicationAdapter extends RESTAdapter {
  urlForQueryRecord(options, modelName) {
    let url = super.buildURL(modelName, options.id);

    options.id = undefined;

    return url;
  }
}
