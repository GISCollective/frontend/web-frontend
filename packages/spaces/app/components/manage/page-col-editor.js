/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";
import { service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { later, cancel } from "@ember/runloop";
import { getOwner } from "@ember/application";

export default class ManagePageColEditorComponent extends Component {
  @service pageCols;

  @tracked newValue;
  @tracked newType;
  @tracked _gid;
  @tracked showChangeGlobalType;

  @tracked isReady = true;
  prevPageCol = "";

  elementId = `page-col-editor-${guidFor(this)}`;

  get componentName() {
    return "editor/" + this.type;
  }

  get hasEditor() {
    const owner = getOwner(this);
    const lookup = owner.lookup("component-lookup:main");
    if (!lookup.componentFor) {
      return !!lookup.lookupFactory(this.componentName);
    }

    return !!(lookup.componentFor(this.componentName, owner) || lookup.layoutFor(this.componentName, owner));
  }

  get gid() {
    if (this._gid !== undefined) {
      return this._gid;
    }

    return this.value?.gid;
  }

  set gid(value) {
    this._gid = value;
  }

  get value() {
    if (this.args.isSimple) {
      return this.args.value?.data;
    }

    return this.args.value;
  }

  get availableTypes() {
    return this.pageCols.availableTypes;
  }

  get type() {
    if (this.newType === null) {
      return false;
    }

    let type = this.newType || this.args.value?.type;

    if (this.availableTypes.indexOf(type) == -1 && /* sheet hack :) */ type != "image-list") {
      // eslint-disable-next-line no-console
      console.log(`The type "${type}" is an invalid editor type.`);
      return false;
    }

    return type;
  }

  get hasUpdate() {
    return typeof this.newValue != "undefined" || this.newType || this.newType === null;
  }

  get space() {
    return this.args.model?.space;
  }

  get selectedComponent() {
    if (this.gid) {
      return this.gid;
    }

    return this.type;
  }

  change(gid) {
    return this.args.onChange?.(this.newValue ?? this.value?.data ?? {}, this.type, gid || this.gid);
  }

  @action
  async changeGid(id) {
    await this.change(id);
    this.gid = id;
  }

  @action
  removeGid() {
    this.gid = null;

    return this.change();
  }

  @action
  async typeChanged(value) {
    if (!value) {
      return this.args.onRemoveCol?.();
    }

    if (this.space?.hasPageCol(value)) {
      const col = this.space?.getPageCol(value);
      this.newValue = col.data;
      this.newType = col.type;
      this.gid = value;

      return this.change();
    }

    this.newType = value;
    this.gid = null;

    return this.change();
  }

  @action
  async dataChanged(value) {
    this.newValue = value;
    return this.change();
  }

  @action
  changeGlobalType(newType) {
    if (newType) {
      this.newType = newType;
    }

    this.showChangeGlobalType = false;

    return this.change();
  }

  @action
  toggleChangeGlobalType() {
    this.showChangeGlobalType = !this.showChangeGlobalType;
  }

  @action
  reload() {
    const title = this.args.value?.name ?? "";

    if (title == this.prevPageCol) {
      return;
    }

    this.isReady = false;
    this.newType = undefined;
    this.newValue = undefined;
    this.gid = undefined;
    this.prevPageCol = title;

    cancel(this.timer);
    this.timer = later(() => {
      this.isReady = true;
    });
  }
}
