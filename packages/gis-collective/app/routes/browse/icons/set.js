/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BrowseRoute from '../../base/browse';
import LazyList from 'models/lib/lazy-list';
import { hash } from 'rsvp';

export default class BrowseIconsSetRoute extends BrowseRoute {
  async model(params) {
    const iconParams = { iconSet: params.id };

    if (params['search']) {
      iconParams['term'] = params['search'];
    }

    const icons = new LazyList('icon', iconParams, this.store);
    await icons.loadNext();

    return hash({
      iconSet: this.store.findRecord('icon-set', params.id),
      icons,
      loadedAny: icons.loadedAny
    });
  }
}
