/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch } from "ember-power-select/test-support";

describe("Integration | Component | editor/link component", function (hooks) {
  setupRenderingTest(hooks);

  let value;
  let space;
  let server;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage("000000000000000000000002");

    space = server.testData.storage.addDefaultSpace();
    space.getPagesMap = () => {
      return {
        page1: "000000000000000000000001",
        page2: "000000000000000000000002",
        page3: "000000000000000000000003",
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    value = null;

    this.set("space", space);

    this.set("change", (v) => {
      value = v;
    });
  });

  it("renders nothing when there is no value", async function (a) {
    await render(hbs`<Editor::Link />`);

    expect(this.element.querySelector(".ember-basic-dropdown-trigger").textContent.trim()).to.equal("");
    expect(this.element.querySelector(".open-in-new-tab").value).to.equal("no");
  });

  it("renders the value when set", async function (a) {
    this.set("value", { link: { pageId: "000000000000000000000001" }, newTab: true });

    await render(hbs`<Editor::Link @value={{this.value}} @space={{this.space}} />`);

    expect(this.element.querySelector(".open-in-new-tab").value).to.equal("yes");
    expect(this.element.querySelector(".ember-basic-dropdown-trigger").textContent.trim()).to.equal("page1");
  });

  it("can change the value", async function (a) {
    let value;

    this.set("change", (v) => {
      value = v;
    });

    this.set("value", { name: "name", link: { pageId: "000000000000000000000001" }, newTab: true });

    await render(hbs`<Editor::Link @onChange={{this.change}} @value={{this.value}} @space={{this.space}} />`);

    this.element.querySelector(".open-in-new-tab").value = "no";
    await triggerEvent(".open-in-new-tab", "change");

    await selectSearch(".ember-basic-dropdown-trigger", "https://giscollective.com");

    expect(value).to.deep.equal({ name: "name", link: { url: "https://giscollective.com" }, newTab: false });
  });
});
