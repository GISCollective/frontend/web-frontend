/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class CampaignAdapter extends Adapter {
  createNotification(campaignId) {
    const url = this.buildURL('campaign', campaignId) + '/createNotification';
    return this.ajax(url, 'POST');
  }

  removeNotification(campaignId) {
    const url = this.buildURL('campaign', campaignId) + '/removeNotification';
    return this.ajax(url, 'POST');
  }
}
