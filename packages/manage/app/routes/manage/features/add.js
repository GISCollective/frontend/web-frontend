/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';

export default class AddFeatureRoute extends AuthenticatedRoute {
  modelsToClear = ['feature'];

  queryParams = {
    mapId: { refreshModel: true },
    all: { refreshModel: true },
  };

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const mapParams = {};

    if (params.all) {
      mapParams.all = params.all;
    } else {
      mapParams.canAdd = true;
    }

    let map;
    let maps = [];

    if (params.mapId) {
      map = await this.store.findRecord('map', params.mapId);
      maps = [map];
    }

    const feature = this.store.createRecord('feature', {
      name: '',
      description: '',
      visibility: 0,
      info: new ModelInfo(),
      maps,
    });

    return hash({
      feature,
      baseMaps: this.store.query('baseMap', { default: true }),
      maps: this.store.query('map', mapParams),
      map,
    });
  }
}
