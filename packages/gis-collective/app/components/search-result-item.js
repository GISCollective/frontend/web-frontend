/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import sanitizeHtml from 'sanitize-html';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { toPageState } from 'core/lib/page-state';

const modelName = {
  "Icon": "icon",
  "Article": "article",
  "Event": "event"
}

export default class SearchResultItemComponent extends Component {
  @service store;
  @service space;
  @tracked record;

  fixText(text) {
    return sanitizeHtml(text, {
      allowedTags: [],
      allowedAttributes: [],
    })
      .replaceAll('&amp;', '&')
      .replaceAll('&nbsp;', ' ');
  }

  get coverValue() {
    return {
      size: {
        sizing: 'proportional'
      }
    }
  }

  get title() {
    return this.fixText(this.args.value.title);
  }

  get description() {
    return this.fixText(this.args.value.description);
  }

  get link() {
    return this.args.space.getLinkFor(this.args.value.relatedModel, this.record ?? this.args.value.relatedId);
  }

  get featureLink() {
    if (this.args.value.relatedModel != 'Feature') {
      return null;
    }

    return this.link;
  }

  get mapLink() {
    if (this.args.value.relatedModel != 'Feature') {
      return null;
    }

    if (!this.args.value.feature?.maps?.[0]) {
      return null;
    }

    const [lon, lat] = this.args.value.feature?.centroid?.coordinates ?? [];

    const url = this.space.getLinkFor('Map', this.args.value.feature.maps[0]);

    if (!url || url.indexOf("?") >= 0) {
      return null;
    }

    return url + '?s=' + toPageState({ fid: this.args.value.relatedId, lon, lat, z: 15 });
  }

  @action
  click() {
    return this.args.onClick?.();
  }

  @action
  async setup() {
    const model = modelName[this.args.value?.relatedModel];

    if (model) {
      this.record = this.store.peekRecord(model, this.args.value.relatedId);

      if (!this.record) {
        this.record = await this.store.findRecord(model, this.args.value.relatedId);
      }

      await this.record.loadRelations?.();
    }
  }
}
