/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BrowseGeometryComponent from '../browse/geometry';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';
import { getCenter } from 'ol/extent';
import { service } from '@ember/service';
import { transform, transformExtent } from 'ol/proj';
import { later } from '@ember/runloop';
import { GeoJson } from 'core/lib/geoJson';

export default class InputGeometryComponent extends BrowseGeometryComponent {
  @service fastboot;

  @tracked extentM;
  @tracked disabled;
  @tracked _mode;
  @tracked gpxType;
  @tracked fileError;
  @tracked _jsonStringValue;
  tooltips = [];

  get jsonStringValue() {
    if (this._jsonStringValue) {
      return this._jsonStringValue;
    }

    if (!this.jsonValue || typeof this.jsonValue != 'object') {
      return '';
    }

    try {
      return JSON.stringify(this.jsonValue, null, 2);
    } catch (err) {
      return '';
    }
  }

  set jsonStringValue(value) {
    try {
      const objVal = JSON.parse(value);

      if (objVal && value.includes('{')) {
        this.args.onChange?.(objVal);
        this._jsonStringValue = null;
        this._coordinates = null;
      }
    } catch (err) {
      this._jsonStringValue = value;
    }
  }

  get mode() {
    if (this.disabled) {
      return '';
    }

    return this._mode;
  }

  get showToolbar() {
    return !this.args.disabled;
  }

  get extent() {
    if (!this.args.extent || !this.args.extent.type || this.args.extent.type != 'Polygon') {
      return null;
    }

    return this.args.extent;
  }

  updateCoordinates() {
    if (!this._coordinates || !this.isPoint) {
      return;
    }

    this.args.onChange?.({
      type: this.value.type,
      coordinates: this._coordinates,
    });
  }

  get isNewLine() {
    return this.type == 'LineString' && this.args.value.coordinates.length == 0;
  }

  get isNewPolygon() {
    return this.type == 'Polygon' && this.args.value.coordinates.length == 0;
  }

  get isEditPath() {
    const value = this.args.value;
    return this.type != 'Point' && value && value.coordinates && value.coordinates.length > 0;
  }

  get isEdit() {
    return !this.isPaste && !this.isFile;
  }

  get isPaste() {
    return this.mode == 'paste';
  }

  get isFile() {
    return this.mode == 'file';
  }

  get value() {
    if (this.args.value) {
      return this.args.value;
    }

    return this.defaultPoint;
  }

  get defaultPoint() {
    let coordinates = [0, 0];

    if (this.extentM) {
      coordinates = getCenter(this.extentM);
    }

    if (this._coordinates) {
      coordinates = this._coordinates;
    }

    return {
      type: 'Point',
      coordinates,
    };
  }

  get focusExtent() {
    return this.args.focusExtent;
  }

  @action
  setup() {
    later(() => {
      super.setup();
    });

    this.disabled = this.args.disabled;
  }

  @action
  updateExtent() {
    if (!this.focusExtent) {
      return;
    }

    if (!this.args.extent?.toOlFeature) {
      return;
    }

    const newExtent = this.args.extent.toOlFeature().getGeometry().getExtent();

    if (`${newExtent}` == `${this.extentM}`) {
      return;
    }

    this.extentM = newExtent;
    const extent = transformExtent(this.extentM, 'EPSG:4326', 'EPSG:3857');

    this.map?.getView().fit(extent);
  }

  @action
  checkPointCoordinates() {
    if (!this.extentM || !this.isPoint) {
      return;
    }

    if (this._coordinates?.length != 2) {
      return this.createPoint();
    }

    const x = this._coordinates[0];
    const y = this._coordinates[1];
    const minX = Math.min(this.extentM[0], this.extentM[2]);
    const maxX = Math.max(this.extentM[0], this.extentM[2]);
    const minY = Math.min(this.extentM[1], this.extentM[3]);
    const maxY = Math.max(this.extentM[1], this.extentM[3]);

    if (x < minX || x > maxX || y < minY || y > maxY) {
      this.createPoint();
    }
  }

  @action
  updateEditState() {
    if (!this.map) {
      return;
    }

    const geoJson = new GeoJson(this.args.value);

    const center = transform(geoJson.center.coordinates, 'EPSG:4326', 'EPSG:3857');

    const viewCenter = this.map.getView().getCenter();

    const p1 = this.map.getPixelFromCoordinate(center);
    const p2 = this.map.getPixelFromCoordinate(viewCenter);

    let distance = 0;

    if (isNaN(p1) || isNaN(p2)) {
      distance = Math.sqrt(Math.pow(p2[0] - p1[0], 2) + Math.pow(p2[1] - p1[1], 2));
    }

    const duration = Math.min(distance / 2, 2000);

    this.map.getView().animate({
      center,
      duration,
    });

    later(
      () => {
        this.disabled = this.args.disabled;
      },
      this.args.disabled ? 0 : duration,
    );
  }

  @action
  changeMapView(map) {
    this.map = map;
    if (this.args.disabled) {
      return;
    }

    const center = transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');

    this.updatePointCoordinates(center);
  }

  @action
  newPolygon(ev) {
    const coordinates = ev.feature.getGeometry().transform('EPSG:3857', 'EPSG:4326').getCoordinates();

    this.args.onChange?.({
      type: 'Polygon',
      coordinates,
    });
  }

  @action
  featureChange(ev) {
    const feature = ev.features.item(0);
    const coordinates = feature.getGeometry().transform('EPSG:3857', 'EPSG:4326').getCoordinates();

    if (JSON.stringify(this._coordinates) == JSON.stringify(coordinates)) {
      return;
    }

    this.args.onChange?.({
      type: this.type,
      coordinates,
    });
  }

  @action
  updatePointCoordinates(coordinates) {
    if (JSON.stringify(this._coordinates) == JSON.stringify(coordinates)) {
      return;
    }

    this._coordinates = coordinates;

    debounce(this, this.updateCoordinates, 500);
  }

  @action
  newLine(ev) {
    const coordinates = ev.feature.getGeometry().transform('EPSG:3857', 'EPSG:4326').getCoordinates();

    this.args.onChange?.({
      type: 'LineString',
      coordinates,
    });
  }

  @action
  selectMode(mode) {
    this._mode = mode;
  }

  @action
  createPoint() {
    this.args.onChange?.(this.defaultPoint);
  }

  @action
  createLine() {
    this.args.onChange?.({
      type: 'LineString',
      coordinates: [],
    });
  }

  @action
  createPolygon() {
    this.args.onChange?.({
      type: 'Polygon',
      coordinates: [],
    });
  }

  @action
  textContent(value) {
    const geoJson = new GeoJson(value);
    this.extentM = geoJson.toExtent('EPSG:4326');

    return this.args.onChange?.(geoJson);
  }

  @action
  fileContent(name, value) {
    this.fileError = '';

    const pieces = name.toLowerCase().split('.');
    const ext = pieces[pieces.length - 1];

    if (ext == 'gpx') {
      const hasTrack = value.indexOf('trk') != -1;
      const hasRoute = value.indexOf('rte') != -1;

      if ((hasTrack && !hasRoute) || (hasTrack && hasRoute)) {
        this.gpxType = 'gpx.track';
      }

      if (!hasTrack && hasRoute) {
        this.gpxType = 'gpx.route';
      }

      if (!hasTrack && !hasRoute) {
        this.gpxType = 'gpx.route';
      }

      if (!hasTrack && !hasRoute) {
        this.fileError = this.intl.t('invalid-gpx-missing-track-or-route');
        return this.args.onChange?.(null);
      }

      return this.args.onChange?.({ type: this.gpxType, value });
    }

    if (ext == 'json' || ext == 'geojson') {
      try {
        var jsonValue = JSON.parse(value);
      } catch (err) {
        this.fileError = this.intl.t('invalid-json-file-message');
        return this.args.onChange?.(null);
      }

      const validGeometries = ['Point', 'LineString', 'MultiLineString', 'Polygon'];
      let geometry = jsonValue;

      if (geometry.type == 'Feature') {
        geometry = jsonValue.geometry;
      }

      if (!geometry || !geometry.type) {
        this.fileError = this.intl.t('invalid-json-feature-message');
        return this.args.onChange?.(null);
      }

      let type = geometry.type;
      if (validGeometries.indexOf(type) == -1) {
        this.fileError = this.intl.t('invalid-json-feature-type-message', {
          type,
        });
        return this.args.onChange?.(null);
      }

      return this.args.onChange?.(jsonValue);
    }

    this.args.onChange?.(null);
    this.fileError = this.intl.t('invalid-file-type-message');
  }
}
