/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import waitUntil from '@ember/test-helpers/wait-until';

describe ('Integration | Component | timer',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    let value = 1;

    this.set('interval', 100);
    this.set('tick', () => {
      value++;
    });

    render(hbs`<Timer @interval={{this.interval}} @onTick={{this.tick}} />`);

    await waitUntil(() => {
      return value >= 3;
    });

    this.set('interval', 0);
  });
});
