/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | pagecol/base/source-field', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when no value is set', async function () {
    await render(hbs`<View::Base::SourceField />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('selects the default "description" field name when none is matched', async function (a) {
    this.set('value', {
      modelKey: 'map',
    });

    this.set('record', {
      constructor: {
        modelName: 'map',
      },
    });

    let values = [];
    this.set('loaded', (v) => {
      values.push(v);
    });

    await render(hbs`<View::Base::SourceField @record={{this.record}} @onLoad={{this.loaded}} @value={{this.value}} @for='description' as |fieldName|>
      {{fieldName}}
    </View::Base::SourceField>`);

    expect(this.element.textContent.trim()).to.equal('description');
    a.deepEqual(values, ['description']);
  });

  it('updates the default "description" field name when updated', async function (a) {
    this.set('value', {
      modelKey: 'map',
    });

    this.set('record', {
      constructor: {
        modelName: 'map',
      },
    });

    let values = [];
    this.set('loaded', (v) => {
      values.push(v);
    });

    await render(hbs`<View::Base::SourceField @record={{this.record}} @onLoad={{this.loaded}} @value={{this.value}} @for='description' as |fieldName|>
      {{fieldName}}
    </View::Base::SourceField>`);

    this.set('record', {
      constructor: {
        modelName: 'map',
      },
    });

    a.deepEqual(values, ['description']);
  });

  it('selects the default "cover" field name when none is matched', async function () {
    this.set('value', {
      modelKey: 'map',
    });

    this.set('record', {
      constructor: {
        modelName: 'map',
      },
    });

    await render(hbs`<View::Base::SourceField @record={{this.record}} @value={{this.value}} @for='cover' as |fieldName|>
      {{fieldName}}
    </View::Base::SourceField>`);

    expect(this.element.textContent.trim()).to.equal('cover');
  });

  it('selects the field name when the value has one defined', async function () {
    this.set('value', {
      data: { field: { name: 'tagLine' } },
      modelKey: 'map',
    });

    this.set('record', {
      constructor: {
        modelName: 'map',
      },
    });

    await render(hbs`<View::Base::SourceField @record={{this.record}} @value={{this.value}} @for='description' as |fieldName|>
      {{fieldName}}
    </View::Base::SourceField>`);

    expect(this.element.textContent.trim()).to.equal('tagLine');
  });

  it('adds the empty class when the selected field is an empty list', async function () {
    this.set('value', {
      data: { field: { name: 'pictures' } },
      modelKey: 'map',
    });

    this.set('record', {
      constructor: {
        modelName: 'map',
      },
      pictures: [],
    });

    await render(hbs`<View::Base::SourceField @isEditor={{true}} @record={{this.record}} @value={{this.value}} @for='pictures' as |fieldName|>
      {{fieldName}}
    </View::Base::SourceField>`);

    expect(this.element.querySelector('.source-field')).to.have.class('empty');
  });

  it('does not add the empty class when is not edit mode', async function () {
    this.set('value', {
      data: { field: { name: 'pictures' } },
      modelKey: 'map',
    });

    this.set('record', {
      constructor: {
        modelName: 'map',
      },
      pictures: [],
    });

    await render(hbs`<View::Base::SourceField @record={{this.record}} @value={{this.value}} @for='pictures' as |fieldName|>
      {{fieldName}}
    </View::Base::SourceField>`);

    expect(this.element.querySelector('.source-field')).not.to.have.class(
      'empty',
    );
  });

  describe('for a date field', async function () {
    it('selects the "info.createdOn" field name when none is matched', async function () {
      this.set('value', {
        modelKey: 'map',
      });

      this.set('record', {
        constructor: {
          modelName: 'map',
        },
      });

      await render(hbs`<View::Base::SourceField @record={{this.record}} @value={{this.value}} @for='date' as |fieldName|>
        {{fieldName}}
      </View::Base::SourceField>`);

      expect(this.element.textContent.trim()).to.equal('info.createdOn');
    });
  });
});
