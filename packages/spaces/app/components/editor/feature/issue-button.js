/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "../base/modelItem";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsFeatureIssueButtonComponent extends Component {
  @tracked _styleClasses;
  @tracked _buttonLabel;
  @tracked _buttonIssueType;

  issueTypes = ["generic", "photo"];

  get buttonIssueType() {
    if (this._buttonIssueType) {
      return this._buttonIssueType;
    }

    return this.args.value?.issueType ?? "generic";
  }

  triggerChange() {
    return this.args.onChange({
      issueType: this.buttonIssueType,
    });
  }

  @action
  changeButtonIssueType(value) {
    this._buttonIssueType = value;

    return this.triggerChange();
  }
}
