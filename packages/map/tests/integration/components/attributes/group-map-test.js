/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, click, fillIn, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { transform } from 'ol/proj';

describe('Integration | Component | attributes/group-map', function (hooks) {
  setupRenderingTest(hooks);

  it('shows the select button', async function () {
    await render(hbs`<Attributes::GroupMap />`);
    expect(this.element.querySelector('.btn-edit')).to.exist;
  });

  it('triggers a change when the edit button is pressed', async function () {
    let position;
    let details;

    this.set('change', function (p, d) {
      position = p;
      details = d;
    });

    this.set('position', { type: 'Point', coordinates: [1, 2] });

    await render(
      hbs`<Attributes::GroupMap @position={{this.position}} @onChange={{this.change}} />`,
    );
    await click('.btn-edit');

    expect(position.toJSON()).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });
    expect(details.toJSON()).to.deep.equal({
      altitude: +0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
      type: 'manual',
    });

    await click('.btn-dismiss');
    expect(position.toJSON()).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });

    expect(details.toJSON()).to.deep.equal({
      capturedUsingGPS: false,
      altitude: 0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
    });
  });

  it('allows to search for geocoding records', async function () {
    let position;
    let details;
    let term;

    this.set('change', function (p, d) {
      position = p;
      details = d;
    });

    this.set('search', function (t) {
      term = t;
    });

    this.set('position', { type: 'Point', coordinates: [1, 2] });

    await render(
      hbs`<Attributes::GroupMap @position={{this.position}} @onChange={{this.change}} @onSearch={{this.search}} />`,
    );
    await click('.btn-edit');
    await fillIn('.input-search', 'term');
    await click('.btn-search');

    expect(term).to.equal('term');
    expect(position.toJSON()).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });
    expect(details.toJSON()).to.deep.equal({
      altitude: +0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
      type: 'manual',
    });
  });

  it('can set the center to a result', async function () {
    let position;
    let details;
    let term;

    this.set('change', function (p, d) {
      position = p;
      details = d;
    });

    this.set('search', function (t) {
      term = t;
    });

    this.set('searchResults', [
      {
        name: 'address 2',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [7, 48],
              [7, 32],
              [27, 32],
              [27, 48],
              [7, 48],
            ],
          ],
        },
      },
    ]);

    await render(
      hbs`<Attributes::GroupMap @searchResults={{this.searchResults}} @onChange={{this.change}} @onSearch={{this.search}} />`,
    );
    await click('.btn-edit');
    await fillIn('.input-search', 'term');
    await click('.btn-search');
    await click('.list-group-item');

    expect(term).to.be.null;

    await click('.btn-submit');

    expect(position.toJSON()).to.deep.equal({
      type: 'Point',
      coordinates: [17, 40.47413040436973],
    });
    expect(details.toJSON()).to.deep.equal({
      altitude: 0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
      type: 'manual',
    });
  });

  it('renders the select point in the middle of the map', async function () {
    this.set('position', {
      type: 'Point',
      coordinates: [1, 5],
    });

    await render(hbs`<Attributes::GroupMap @position={{this.position}} />`);

    const map = this.element.querySelector('.map').olMap;
    const view = map.getView();

    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

    expect(center.map((a) => parseInt(a))).to.deep.equal([1, 5]);
  });

  it('shows an alert when the point is outside the extent', async function () {
    this.set('position', {
      type: 'Point',
      coordinates: [1, 5],
    });
    this.set('area', {
      type: 'Polygon',
      coordinates: [
        [
          [13.18114152285044, 52.35153342989699],
          [13.77573361727183, 52.35153342989699],
          [13.77573361727183, 52.70871792128128],
          [13.18114152285044, 52.70871792128128],
          [13.18114152285044, 52.35153342989699],
        ],
      ],
    });

    await render(hbs`<Attributes::GroupMap
        @extent={{this.area}}
        @position={{this.position}}
      />`);

    expect(this.element.querySelector('.alert-outside-extent')).to.exist;
  });

  it('does not show an alert when the point is inside the extent', async function () {
    this.set('position', {
      type: 'Point',
      coordinates: [13.2, 52.4],
    });
    this.set('area', {
      type: 'Polygon',
      coordinates: [
        [
          [13.18114152285044, 52.35153342989699],
          [13.77573361727183, 52.35153342989699],
          [13.77573361727183, 52.70871792128128],
          [13.18114152285044, 52.70871792128128],
          [13.18114152285044, 52.35153342989699],
        ],
      ],
    });

    await render(hbs`<Attributes::GroupMap
        @extent={{this.area}}
        @position={{this.position}}
      />`);

    expect(this.element.querySelector('.alert-outside-extent')).not.to.exist;
  });

  it('opens the edit mode when the type is changed to manual', async function () {
    this.set('details', {
      type: 'gps',
    });

    await render(hbs`<Attributes::GroupMap @details={{this.details}} />`);

    this.set('details', {
      type: 'manual',
    });

    await waitFor('.map-container.fullscreen');

    expect(this.element.querySelector('.view-mode')).not.to.exist;
  });

  it('does not open the edit mode when the type is changed to gps', async function () {
    this.set('details', {});

    await render(hbs`<Attributes::GroupMap @details={{this.details}} />`);

    this.set('details', {
      type: 'gps',
    });

    expect(this.element.querySelector('.view-mode')).to.exist;
  });
});
