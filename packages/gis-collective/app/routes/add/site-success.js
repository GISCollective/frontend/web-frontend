/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/app-route';
import { hash } from 'rsvp';

export default class AddSiteSuccessRoute extends Route {
  queryParams = {
    feature: { refreshModel: false },
    map: { refreshModel: false },
  };

  model(params) {
    let map;
    let feature;

    if (params.map) {
      map = this.store.findRecord('map', params.map);
    }

    if (params.feature) {
      feature = this.store.findRecord('feature', params.feature);
    }

    return hash({ map, feature });
  }
}
