/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | source', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:source');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized.toJSON()).to.deep.equal({
      type: '',
      modelId: '',
      remoteId: '',
      syncAt: '1970-01-01T00:00:00.000Z',
    });
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized.toJSON()).to.deep.equal({
      type: '',
      modelId: '',
      remoteId: '',
      syncAt: '1970-01-01T00:00:00.000Z',
    });
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize({
      type: 'a',
      modelId: 'b',
      remoteId: 'c',
      syncAt: '2020-01-01T00:00:00.000Z',
    });

    expect(deserialized.toJSON()).to.deep.equal({
      type: 'a',
      modelId: 'b',
      remoteId: 'c',
      syncAt: '2020-01-01T00:00:00.000Z',
    });
  });

  it('can serialize an object', function () {
    const source = {
      type: 'a',
      modelId: 'b',
      remoteId: 'c',
      syncAt: '2022-01-01T00:00:00.000Z',
    };

    let deserialized = transform.deserialize(source);

    expect(transform.serialize(deserialized)).to.deep.equal(source);
  });
});
