/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { service } from '@ember/service';
import Controller from '@ember/controller';
import config from '../../../config/environment';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { GeoJson } from '../../../lib/geoJson';
import AlertMessage from 'core/lib/alert-message';
import { Cluster } from '../../../transforms/cluster';
import { uniqBy } from 'core/lib/array';

export default class ManageMapEditController extends Controller {
  @service notifications;
  @service fastboot;
  @service intl;
  @service user;
  @service router;
  @service preferences;
  @service space;

  @tracked allTeams = false;
  @tracked savingField;

  queryParams = ['viewbox', 'icons', 'search', 'hideLoading', 'baseMap', 'allTeams'];

  get showLegacyFields() {
    return this.space.name == 'Open Green Map';
  }

  get baseMaps() {
    return this.model.map.baseMaps?.list ?? [];
  }

  get iconSets() {
    return this.model.map.iconSets?.list ?? [];
  }

  get alertList() {
    const list = [];

    const unpublishedBaseMaps =
      this.baseMaps
        .filter((a) => a.get?.('isLoaded'))
        .filter((a) => !a.get?.('visibility.isPublic'))
        .map((a) => a.get?.('name')) ?? [];

    const teamId = this.model.map.get?.('visibility.team.id');

    const otherBaseMaps =
      this.baseMaps
        .filter((a) => a.get?.('isLoaded'))
        .filter((a) => !a.get?.('visibility.isDefault'))
        .filter((a) => a.get?.('visibility.team.id') != teamId)
        .map((a) => a.get?.('name')) ?? [];

    const otherIconSets =
      this.iconSets
        .filter((a) => a.get?.('isLoaded'))
        .filter((a) => !a.get?.('visibility.isDefault'))
        .filter((a) => a.get?.('visibility.team.id') != teamId)
        .map((a) => a.get?.('name')) ?? [];

    if (this.model.map.visibility.isPublic && unpublishedBaseMaps.length > 0) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-map-with-unpublished-basemaps', {
            names: unpublishedBaseMaps.join(', '),
          }),
          'danger',
          'alert-map-with-unpublished-basemaps',
        ),
      );
    }

    if (otherBaseMaps.length > 0) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-map-with-unrelated-basemaps', {
            names: otherBaseMaps.join(', '),
          }),
          'danger',
          'alert-map-with-unrelated-basemaps',
          () => {
            this.fixUnrelatedBaseMaps();
          },
        ),
      );
    }

    if (otherIconSets.length > 0) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-map-with-unrelated-iconsets', {
            names: otherIconSets.join(', '),
          }),
          'danger',
          'alert-map-with-unrelated-iconsets',
          () => {
            this.fixUnrelatedIcons();
          },
        ),
      );
    }

    return list;
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        text: this.model.map.name,
        route: 'manage.dashboards.map',
        model: this.model.map.get('id'),
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  get hasClusteringEnabled() {
    return this.model.map.cluster?.mode ? true : false;
  }

  @action
  saveHasClusteringEnabled(value) {
    if (!this.model.map.cluster) {
      this.model.map.cluster = new Cluster();
    }

    this.model.map.cluster.mode = value ? 1 : 0;

    return this.save();
  }

  @action
  save() {
    return this.model.map.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      },
    );
  }

  get apiUrl() {
    if (config.apiUrl.indexOf('http') == 0) {
      return config.apiUrl;
    }

    return 'https://' + this.domain + config.apiUrl;
  }

  get domain() {
    return this.preferences.domain;
  }

  fixUnrelatedIcons() {
    const teamId = this.model.map.get?.('visibility.team.id');

    this.model.map.iconSets.list =
      this.model.map.iconSets.list
        .filter((a) => a.get?.('isLoaded'))
        .filter((a) => a.get?.('visibility.isDefault') || a.get?.('visibility.team.id') == teamId) ?? [];

    return this.save();
  }

  fixUnrelatedBaseMaps() {
    const teamId = this.model.map.get?.('visibility.team.id');

    this.model.map.baseMaps.list =
      this.model.map.baseMaps.list
        .filter((a) => a.get?.('isLoaded'))
        .filter((a) => a.get?.('visibility.isDefault') || a.get?.('visibility.team.id') == teamId) ?? [];

    return this.save();
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  createImage() {
    return this.store.createRecord('picture', { name: '', picture: '' });
  }

  @action
  saveIsPublic(newValue) {
    this.model.map.visibility.isPublic = newValue;

    return this.save();
  }

  @action
  saveAddFeaturesAsPending(newValue) {
    this.model.map.addFeaturesAsPending = newValue;

    return this.save();
  }

  @action
  saveHideOnMainMap(newValue) {
    this.model.map.hideOnMainMap = newValue;

    return this.save();
  }

  @action
  saveIsIndex(newValue) {
    this.model.map.isIndex = newValue;

    return this.save();
  }

  @action
  saveStartDate(value) {
    this.model.map.startDate = value;
    return this.save();
  }

  @action
  saveEndDate(value) {
    this.model.map.endDate = value;
    return this.save();
  }

  @action
  saveTagLine(newValue) {
    this.model.map.tagLine = newValue;
    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.map.visibility.team = newValue;

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.map.visibility.isDefault = newValue;

    return this.save();
  }

  @action
  saveLicense(newValue) {
    this.model.map.license = newValue;
    return this.save();
  }

  @action
  saveMask(newValue) {
    this.model.map.set('mask', newValue);
    return this.save();
  }

  @action
  saveShowPublicDownloadLinks(newValue) {
    this.model.map.showPublicDownloadLinks = newValue;

    return this.save();
  }

  @action
  saveCover(value) {
    this.model.map.cover = value;
    return this.save();
  }

  @action
  saveLegacyCover(index, value) {
    this.model.map.cover = value;
    return this.save();
  }

  @action
  saveSquareCover(index, value) {
    this.model.map.squareCover = value;
    return this.save();
  }

  @action
  saveIconSets(title, value, useCustomList) {
    this.model.map.iconSets.list = value;
    this.model.map.iconSets.useCustomList = useCustomList;

    return this.save();
  }

  @action
  saveBaseMaps(title, value, useCustomList) {
    this.model.map.baseMaps.list = uniqBy(value, 'id');
    this.model.map.baseMaps.useCustomList = useCustomList;

    return this.save();
  }

  @action
  saveArticle(title, description) {
    this.model.map.name = title;
    this.model.map.description = description;

    return this.save();
  }

  @action
  changeExtent(value) {
    this.model.map.area = new GeoJson(value);
    return this.save();
  }

  @action
  delete() {
    const teamId = this.model.map.visibility.teamId;
    return this.model.map
      .destroyRecord()
      .then(() => {
        return this.router.transitionTo('manage.dashboards.dashboard', teamId);
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
