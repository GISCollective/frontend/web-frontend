/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, fillIn, triggerEvent } from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';

describe ('Acceptance | admin/users',  function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultSpace();
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe ('Accessing the route', function (hooks) {
    it('should redirect to index if the user is not an admin', async function () {
      server.testData.storage.addDefaultUser(false);
      authenticateSession();

      await visit('/admin/users');
      expect(currentURL()).to.equal('/');
    });

    it('should redirect to index if the user is not authenticated', async function () {
      await visit('/admin/users');
      expect(currentURL()).to.equal('/login');
    });

    it('should let administrators visit /admin/users', async function () {
      authenticateSession();

      await visit('/admin/users');
      expect(currentURL()).to.equal('/admin/users');

      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;
    });

    it('should show the activation status', async function () {
      authenticateSession();

      await visit('/admin/users');
      expect(
        this.element.querySelector('.is-active').textContent.trim()
      ).to.equal('no');
    });

    it('links the user profile', async function () {
      authenticateSession();

      await visit('/admin/users');
      expect(
        this.element.querySelector('.profile-name').textContent.trim()
      ).to.equal('gedaiu');
      expect(this.element.querySelector('.profile-name')).to.have.attribute(
        'href',
        '/browse/profiles/5b870669796da25424540deb'
      );
    });
  });

  it('should search users', async function () {
    authenticateSession();

    await visit('/admin/users?search=gedaiu');

    await fillIn('.input-search', 'gedaiu');
    await triggerEvent('.input-search', 'change');

    const userRequests = server.history.filter(
      (a) => a.indexOf('users?') != -1
    );

    userRequests.forEach((request) => {
      expect(request).to.contain('term=gedaiu');
    });
  });
});
