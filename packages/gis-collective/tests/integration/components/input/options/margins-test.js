/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/options/margins',  function (hooks) {
  setupRenderingTest(hooks);

  let value;

  hooks.beforeEach(async function () {
    value = undefined;
    this.set('change', (v) => {
      value = v;
    });

    this.set('options', ['mt-1', 'mb-2', 'ms-3', 'me-4']);

    await render(
      hbs`<Input::Options::Margins @options={{this.options}} @size="sm" @onChangeOptions={{this.change}}/>`
    );
  });

  it('can set the margin top', async function () {
    expect(this.element.querySelector('.sm-top-margin').value).to.equal('1');

    this.element.querySelector('.sm-top-margin').value = '5';
    await triggerEvent('.sm-top-margin', 'change');

    expect(value).to.deep.equal(['mb-2', 'ms-3', 'me-4', 'mt-5']);
  });

  it('can set the margin bottom', async function () {
    expect(this.element.querySelector('.sm-bottom-margin').value).to.equal('2');

    this.element.querySelector('.sm-bottom-margin').value = '5';
    await triggerEvent('.sm-bottom-margin', 'change');

    expect(value).to.deep.equal(['mt-1', 'ms-3', 'me-4', 'mb-5']);
  });

  it('can set the margin start', async function () {
    expect(this.element.querySelector('.sm-start-margin').value).to.equal('3');

    this.element.querySelector('.sm-start-margin').value = '5';
    await triggerEvent('.sm-start-margin', 'change');

    expect(value).to.deep.equal(['mt-1', 'mb-2', 'me-4', 'ms-5']);
  });

  it('can set the margin end', async function () {
    expect(this.element.querySelector('.sm-end-margin').value).to.equal('4');

    this.element.querySelector('.sm-end-margin').value = '5';
    await triggerEvent('.sm-end-margin', 'change');

    expect(value).to.deep.equal(['mt-1', 'mb-2', 'ms-3', 'me-5']);
  });
});
