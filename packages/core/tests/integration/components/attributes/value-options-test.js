/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/value-options', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a message without change button when no attribute is set', async function () {
    await render(hbs`<Attributes::ValueOptions />`);

    expect(this.element.textContent.trim()).to.equal('there is nothing to choose');
    expect(this.element.querySelector('.btn')).not.to.exist;
  });

  it('renders a message without change button when the value is an empty string', async function () {
    await render(hbs`<Attributes::ValueOptions @value=""/>`);

    expect(this.element.textContent.trim()).to.equal('there is nothing to choose');
    expect(this.element.querySelector('.btn')).not.to.exist;
  });

  it('renders a message without change button when the value is an empty array', async function () {
    this.set('value', []);
    await render(hbs`<Attributes::ValueOptions @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.equal('there is nothing to choose');
    expect(this.element.querySelector('.btn')).not.to.exist;
  });

  it('renders the string value when is set', async function () {
    await render(hbs`<Attributes::ValueOptions @value="string value"/>`);

    expect(this.element.textContent.trim()).to.equal('string value');
    expect(this.element.querySelector('.btn')).not.to.exist;
  });

  it('renders the add message when the value is not set but there are object options available', async function () {
    this.set('options1', [
      { id: 1, name: 'map1' },
      { id: 2, name: 'map2' },
    ]);
    await render(hbs`<Attributes::ValueOptions @options={{this.options1}} />`);

    expect(this.element.querySelector('.btn-change')).not.to.exist;
    expect(this.element.querySelector('.btn-add')).to.exist;
    expect(this.element.querySelector('.btn-add').textContent.trim()).to.equal('Select a value from the list');
  });

  it('selects an option and returns it as a value', async function () {
    let selected;

    this.set('options5', [
      { id: 1, name: 'map1' },
      { id: 2, name: 'map2' },
    ]);
    this.set('selectedAction2', (value) => {
      selected = value;
    });

    await render(hbs`<Attributes::ValueOptions @options={{this.options5}} @onChange={{this.selectedAction2}}/>`);
    await click('.btn-add');

    const options = this.element.querySelectorAll('.list-group-item');
    await click(options[0]);

    expect(selected).to.deep.equal({ id: 1, name: 'map1' });
  });

  it('shows the string value as selected when the options are opened', async function () {
    this.set('options6', 'map1,map2');

    await render(hbs`<Attributes::ValueOptions @value="map2" @options={{this.options6}}/>`);

    await click('.btn-change');

    const activeElements = this.element.querySelectorAll('.list-group-item.active');

    expect(activeElements.length).to.equal(1);
    expect(activeElements[0]).to.contain.text('map2');
  });

  it('shows the object value as selected when the options are opened', async function () {
    this.set('options7', [
      { id: 1, name: 'map1' },
      { id: 2, name: 'map2' },
    ]);
    this.set('value7', { id: 2, name: 'map2' });

    await render(hbs`<Attributes::ValueOptions @value={{this.value7}} @options={{this.options7}}/>`);

    await click('.btn-change');

    const activeElements = this.element.querySelectorAll('.list-group-item.active');

    expect(activeElements.length).to.equal(1);
    expect(activeElements[0]).to.contain.text('map2');
  });

  it('selects an option from a string list and marks it as selected', async function () {
    let selected;

    this.set('options8', 'map1,map2');
    this.set('selectedAction8', (value) => {
      selected = value;
    });

    await render(hbs`<Attributes::ValueOptions @options={{this.options8}} @onChange={{this.selectedAction8}}/>`);
    await click('.btn-add');

    const options = this.element.querySelectorAll('.list-group-item');
    await click(options[0]);

    expect(this.element.querySelectorAll('.list-group-item.active')).to.contain.text('map1');

    expect(selected).to.equal('map1');
  });
});
