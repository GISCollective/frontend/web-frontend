/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export class ValueManager {
  @tracked value;

  @action
  change(newValue) {
    this.value = newValue;
  }
}

export default class ManageFieldContainerComponent extends Component {
  @tracked isDisabled;
  @tracked manager = new ValueManager();

  get isEditMode() {
    return this.args.title == this.args.editablePanel;
  }

  get hasEditValueReady() {
    if (this.args.value === undefined) {
      return true;
    }

    return this.manager.value !== undefined;
  }

  @action
  async onSave() {
    this.isDisabled = true;

    let result;

    try {
      result = await this.args.onSave(this.manager.value);
    } catch (err) {
      console.error(err);
      throw err;
    } finally {
      this.isDisabled = false;
    }

    this.args.onEdit?.('');

    return result;
  }

  @action
  setupManager() {
    if (this.manager.value) {
      return;
    }

    later(() => {
      this.manager.value = this.args.value;
    });
  }

  @action
  toggleMode() {
    if (this.isEditMode) {
      this.manager.value = undefined;
      return this.args.onCancel(this.args.title);
    }

    this.manager.value = this.args.value;
    return this.args.onEdit(this.args.title);
  }
}
