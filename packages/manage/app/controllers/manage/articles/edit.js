/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action, set } from '@ember/object';
import { service } from '@ember/service';
import { onPictureUpload } from '../../../lib/editor-js-picture';
import { tracked } from '@glimmer/tracking';

export default class ManageArticlesEditController extends Controller {
  @service store;
  @service user;
  @service intl;
  @service notifications;

  @tracked _existingCategories;

  get existingCategories() {
    if (this._existingCategories) {
      return this._existingCategories;
    }

    return this.model.existingCategories;
  }

  get breadcrumbs() {
    const result = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        capitalize: true,
        model: this.model.team.id,
      },
    ];

    if (this.model.newsletter) {
      result.push({
        route: 'manage.dashboards.newsletter',
        model: this.model.newsletter.id,
        text: this.model.newsletter.name,
      });
    } else {
      result.push({
        route: 'manage.dashboards.articles',
        text: this.intl.t('articles'),
        capitalize: true,
        model: this.model.team.id,
      });
    }

    result.push({
      text: this.model.article.name,
    });

    result.push({
      text: this.intl.t('edit'),
      capitalize: true,
    });

    return result;
  }

  get editableModel() {
    return this.model.article;
  }

  get isNewsletterWelcome() {
    if (this.user.isAdmin) {
      return false;
    }

    return this.model.article.type == 'newsletter-welcome-message';
  }

  get isSlugVisible() {
    const slug = this.model.article?.slug ?? '';

    if (this.user.isAdmin) {
      return true;
    }

    if (slug.indexOf('notification-') != -1) {
      return false;
    }

    if (this.isNewsletterWelcome) {
      return false;
    }

    return true;
  }

  get showMessageFields() {
    const type = this.model.article?.type ?? '';

    return type.includes('newsletter');
  }

  get allowVideo() {
    const type = this.model.article?.type ?? '';
    const slug = this.model.article?.slug ?? '';

    return !type.includes('newsletter') && !slug.includes('notification');
  }

  @action
  async handlePrimaryChange(value) {
    const result = await this.store.adapterFor('article').categories(this.model.team.id, value);
    this._existingCategories = result.categories ?? [];
  }

  @action
  async saveField(field, value) {
    if (typeof value.toArray == 'function') {
      value = value.toArray();
    }

    set(this.model.article, field, value);

    return this.save();
  }

  @action
  saveCover(value) {
    this.editableModel.cover = value;

    return this.save();
  }

  @action
  async savePictures(pictures) {
    const promises = pictures.map((a) => a.trySave(3));
    let savedPictures = [];

    try {
      savedPictures = await Promise.all(promises);
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.editableModel.pictures = savedPictures;

    return this.save();
  }

  @action
  onPictureUpload(value, type) {
    return onPictureUpload(
      value,
      type,
      this.store,
      { model: 'Article', modelId: this.model.article?.id },
      this.model.article.type == 'newsletter-article',
    );
  }

  @action
  saveIsPublic(newValue) {
    this.model.article.visibility.isPublic = newValue;

    return this.save();
  }

  @action
  saveArticle(name, article) {
    this.model.article.title = name;
    this.model.article.content = article;

    return this.save();
  }

  @action
  async save() {
    let result;

    try {
      result = await this.model.article.save();
    } catch (err) {
      console.error(err);
    }

    this.editableField = '';
    this.savingField = '';

    return result;
  }
}
