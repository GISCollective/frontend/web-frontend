/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { service } from '@ember/service';
import { Location } from '../transforms/location';
import { tracked } from '@glimmer/tracking';

export default class UserProfileFormComponent extends Component {
  @service store;
  @tracked email;

  elementId = 'user-profile-' + guidFor(this);

  get name() {
    return {
      salutation: this.args.profile?.salutation,
      title: this.args.profile?.title,
      firstName: this.args.profile?.firstName,
      lastName: this.args.profile?.lastName,
    };
  }

  get simpleLocation() {
    return this.args.profile?.location?.simple ?? '';
  }

  set simpleLocation(value) {
    this.args.profile.location = new Location(value);
  }

  @action
  async setup() {
    if(!this.args.profile?.id) {
      return;
    }

    try {
      const user = await this.store.findRecord("user", this.args.profile.id);
      this.email = user.email;
    } catch(err) {
      console.error(err);
    }
  }

  @action
  async updatePicture(index, picture) {
    this.args.profile.picture = picture;

    return this.update();
  }

  @action
  changeBio(value) {
    this.args.profile.bio = value;
  }

  @action
  handleNameChange(value) {
    this.args.profile.title = value.title;
    this.args.profile.salutation = value.salutation;
    this.args.profile.firstName = value.firstName;
    this.args.profile.lastName = value.lastName;
  }

  @action
  changeDetailedLocation(value) {
    this.args.profile.location = new Location({
      isDetailed: true,
      detailedLocation: value,
    });
  }

  @action
  update() {
    return this.args.onUpdate?.();
  }
}
