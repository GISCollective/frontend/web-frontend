import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | manage/language', function (hooks) {
  setupRenderingTest(hooks);

  it('shows a message when the space has no value', async function (assert) {
    await render(hbs`<Manage::Language @title="language" />`);

    expect(this.element.querySelector('.alert')).to.have.textContent(
      'The language is not selected. English will be used by default.',
    );
  });

  it('shows a language when set', async function (assert) {
    const service = this.owner.lookup('service:user');
    service.languages = [
      { name: 'English', locale: 'en-us' },
      { name: 'Romana', locale: 'ro-ro' },
      { name: 'French', locale: 'fr-fr' },
    ];

    await render(hbs`<Manage::Language @title="language" @value="ro-ro" />`);

    expect(this.element.querySelector('.alert')).not.to.exist;
    expect(this.element.querySelector('.value')).to.have.textContent('Romana');
  });

  it('can change the value', async function (assert) {
    const service = this.owner.lookup('service:user');
    service.languages = [
      { name: 'English', locale: 'en-us' },
      { name: 'Romana', locale: 'ro-ro' },
      { name: 'French', locale: 'fr-fr' },
    ];

    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Language @title="language" @value="ro-ro" @editablePanel="language" @onSave={{this.save}} />`,
    );

    this.element.querySelector('select').value = 'en-us';
    await triggerEvent('select', 'change');
  });
});
