/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "../map/map-view";
import { action } from "@ember/object";

export default class ManageEditorsBackgroundMapComponent extends Component {
  mapConfig = [
    {
      name: "mode",
      options: ["show all features", "use a map"],
    },
  ];

  @action
  changeMap(value) {
    this._map = {
      ...value,
      flyOverAnimation: false,
      featureHover: "do nothing",
      featureSelection: "do nothing",
      enableMapInteraction: "no",
      restrictView: false,
      showUserLocation: false,
      showZoomButtons: false,
      showBaseMapSelection: false,
    };

    return this.triggerChange();
  }
}
