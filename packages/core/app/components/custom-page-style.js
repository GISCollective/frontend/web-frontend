/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { sizedBsProperty } from 'core/lib/bs-cols';
import { htmlSafe } from '@ember/template';

export default class CustomPageStyleComponent extends Component {
  get style() {
    if (!this.args.value || !this.args.value.flatMap) {
      return '';
    }

    const colOptions = this.args.value
      .flatMap((a) => a?.rows ?? [])
      .flatMap((a) => a?.cols ?? [])
      .map((a) => a.options);

    const rowOptions = this.args.value.flatMap((a) => a?.rows ?? []).map((a) => a.options);

    const containerOptions = this.args.value.map((a) => a.options);

    const optionsLists = [...colOptions, ...rowOptions, ...containerOptions];

    let result = ``;
    const sizes = [undefined, 'md', 'lg'];
    const mediaQueries = {
      md: '@media (min-width: 768px) { ',
      lg: '@media (min-width: 992px) { ',
    };

    for (const options of optionsLists.filter((a) => a)) {
      for (const size of sizes) {
        const value = sizedBsProperty('wpx', size, options);
        if (!value || value == 'default') {
          continue;
        }

        let prefix = '';
        let suffix = '';

        if (mediaQueries[size]) {
          prefix = mediaQueries[size];
          suffix = ' }';
        }

        let option = `wpx-${size ? size : ''}${size ? '-' : ''}${value}`;

        result += `${prefix}.${option} { flex: 0 0 auto; width: ${value}px; }${suffix}\n`;
        result += `.enforce-${size ?? 'sm'}-size .${option}, .wpx-${value} { flex: 0 0 auto; width: ${value}px; }\n\n`;
      }
    }

    return htmlSafe(result);
  }
}
