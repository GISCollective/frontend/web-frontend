/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { guidFor } from "@ember/object/internals";

export default class InputOptionsModelFieldComponent extends Component {
  @tracked _field;

  elementId = `model-field-${guidFor(this)}`;

  fieldOptionsMap = {
    map: { description: ["description", "tagLine"] },
    article: {
      date: ["releaseDate", "info.lastChangeOn", "info.createdOn"],
      pictures: ["pictures", "coverAndPictures"],
    },
  };

  get fieldOptions() {
    return this.fieldOptionsMap[this.modelName]?.[this.args.for] ?? [];
  }

  get field() {
    if (typeof this._field == "string") {
      return this._field;
    }

    return this.args?.value?.name ?? this.fieldOptions[0];
  }

  get hasManyFields() {
    return this.fieldOptions.length > 0;
  }

  get modelName() {
    const key = this.args.modelKey ?? this.args.col?.modelKey ?? "";

    return this.args?.model?.[key]?.constructor?.modelName;
  }

  @action
  fieldChanged(value) {
    this._field = value;

    return this.args?.onChange?.({
      name: value,
    });
  }
}
