/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ManagePhotoGalleryComponent from './photo-gallery';

export default class ManageSoundListComponent extends ManagePhotoGalleryComponent {}
