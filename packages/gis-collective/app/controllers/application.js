/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ApplicationController extends Controller {
  @service accessConfig;
  @service articles;
  @service fullscreen;
  @service mapStyles;
  @service preferences;
  @service router;
  @service search;
  @service searchStorage;
  @service session;
  @service space;
  @service store;
  @service user;
  @service theme;
  @service('embed') embedService;

  queryParams = ['embed', 'allIcons'];

  @tracked embed;

  get routeName() {
    return this.router.currentRoute?.name ?? '';
  }

  get section() {
    if (['manage.pages.content', 'manage.pages.layout'].includes(this.routeName)) {
      return 'content';
    }

    return this.routeName.split('.')[0];
  }

  get isEmbedded() {
    return this.embed === 'true' || this.embed === true || this.embedService.isEnabled;
  }

  get showCampaigns() {
    const item = this.model?.models?.find((a) => a.name === 'Campaign');

    return item?.itemCount;
  }

  get showManage() {
    return this.preferences.allowManageWithoutTeams || this.user.teams?.length > 0;
  }

  get showBrowse() {
    if (!this.model?.models?.filter) {
      return false;
    }

    if (!this.accessConfig.isMultiProjectMode) {
      return false;
    }

    const items = this.model.models
      .filter((a) => a.name !== 'Campaign')
      .map((a) => a.itemCount)
      .reduce((a, b) => a + b, 0);

    return items > 0;
  }

  get isMenuRendered() {
    if (this.routeName.indexOf('manage.') != -1) {
      return false;
    }

    if (this.routeName.indexOf('add.') != -1 && this.routeName != 'add.site') {
      return false;
    }

    if (this.isEmbedded) {
      return false;
    }

    return this.session.isAuthenticated || !this.preferences.isPrivate;
  }

  get isLegacyMenuRendered() {
    if(!this.isMenuRendered) {
      return false;
    }

    if (!this.space.hasLegacyMenu) {
      return false;
    }

    if (this.space.currentSpace?.cols?.map?.["main-menu"]) {
      return false;
    }

    if (this.space.currentSpace?.cols?.map?.["main menu"]) {
      return false;
    }

    if (this.routeName.indexOf('page') != -1) {
      return false;
    }

    return true;
  }

  get globalMenu() {
    if(!this.isMenuRendered) {
      return false;
    }

    if (this.space.hasLegacyMenu) {
      return false;
    }

    if (!this.isMenuVisible) {
      return false;
    }

    if (this.routeName.indexOf('page') != -1 || this.routeName == "index") {
      return false;
    }

    const map = this.space.currentSpace?.cols?.map;

    return map?.["main-menu"] || map?.["main menu"];
  }

  get isMenuVisible() {
    if (this.fullscreen.isEnabled) {
      return false;
    }

    if (this.session.isAuthenticated) {
      return true;
    }

    return !this.preferences.isPrivate;
  }

  @action
  async onSignOut() {
    console.log("auth invalidate");
    await this.session.invalidate();
    this.store.unloadAll();
  }

  @action
  onSearch(value) {
    this.router.transitionTo({ queryParams: { search: value, pages: null } });
  }

  @action
  goTo(path) {
    if (path === 'index') {
      this.router.transitionTo('/');
      return;
    }

    this.router.transitionTo(path);
  }

  @action
  onSelectLocale(locale) {
    this.router.transitionTo({ queryParams: { locale } });
  }

  @action
  onMapStyle(element) {
    this.mapStyles.mapStyleElement = element;
  }
}
