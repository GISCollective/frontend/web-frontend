/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { A } from 'models/lib/array';

export default class InputListComponent extends Component {
  elementId = 'select-list-' + guidFor(this);
  @tracked values = A([]);
  @tracked _isEnabled = null;

  enableValues = [
    { id: 1, name: 'no' },
    { id: 0, name: 'yes' },
  ];

  get haveEnabledSwitch() {
    return this.args.isEnabled !== undefined;
  }

  get isEnabled() {
    if (!this.haveEnabledSwitch) {
      return true;
    }

    if (this._isEnabled === null) {
      return this.args.isEnabled;
    }

    return this._isEnabled;
  }

  set isEnabled(value) {
    this._isEnabled = value;
    this.raiseChange();
  }

  get isEnabledId() {
    return this.isEnabled ? 1 : 0;
  }

  get list() {
    return this.args.list ?? [];
  }

  get canAdd() {
    return this.list.length > this.values.length;
  }

  get firstObject() {
    const items = this.list.filter((a) => this.values.indexOf(a) == -1);

    return items.pop();
  }

  raiseChange() {
    let values = this.isEnabled ? this.values : [];

    const validValues = values.filter((a) => a !== null && a !== undefined);

    this.args.onChange?.(this.isEnabled, validValues);
  }

  @action
  async setup() {
    this.values = A((await this.args.values) ?? []);
  }

  @action
  changeIsEnabled(newValue) {
    if (!newValue.id) {
      this.prevList = this.values;
    }

    if (newValue.id && this.prevList) {
      this.values = this.prevList;
    }

    this._isEnabled = newValue.id ? true : false;

    return this.raiseChange();
  }

  @action
  removeItem(index) {
    if (!this.values) {
      this.values = A(this.args.values.slice());
    }

    this.values.splice(index, 1);
    this.raiseChange();
  }

  @action
  updateItem(index, value) {
    if (!this.values) {
      this.values = this.args.values.slice();
    }

    this.values[index] = value;
    this.values = A([...this.values]);
    this.raiseChange();
  }

  @action
  addItem() {
    if (!this.values) {
      this.values = A(this.args.values.slice());
    }

    this.values.push(this.firstObject);
    this.raiseChange();
  }
}
