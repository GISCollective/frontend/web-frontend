/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageDashboardRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.isFastBoot) {
      return {};
    }

    return hash({
      team: this.store.findRecord('team', params.id),
      maps: this.store.query('map', { team: params.id }),
      iconSets: this.store.query('icon-set', { team: params.id }),
      basemaps: this.store.query('base-map', { team: params.id }),
      campaigns: this.store.query('campaign', { team: params.id }),
      newsletters: this.store.query('newsletter', { team: params.id }),
      pages: this.store.query('page', { team: params.id }),
      presentations: this.store.query('presentation', { team: params.id }),
      articles: this.store.query('article', { team: params.id }),
      spaces: this.store.query('space', { team: params.id }),
      calendars: this.store.query('calendar', { team: params.id }),
      dataBindings: this.store.query('data-binding', { team: params.id }),
    });
  }
}
