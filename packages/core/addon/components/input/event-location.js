/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class InputEventLocationComponent extends Component {
  @service store;
  @tracked _selected;
  @tracked options;

  @action
  change(value) {
    this._selected = {
      type: value.type,
      value: value.value,
      feature: value.feature,
    };

    this.args.onChange({
      type: value.type,
      value: value.value,
    });
  }

  @action
  async performSearchRecord(value) {
    this.change({
      type: 'Text',
      value,
    });

    let query = { term: value };

    if (this.args.map?.id) {
      query.map = this.args.map?.id;
    }

    let list = [];

    try {
      const results = await this.store.query('feature', query);
      list = results.slice();
    } catch (err) {
      list = [];
    }

    const featureOptions = list.map((feature) => ({
      type: 'Feature',
      value: feature.id,
      feature,
    }));

    this.options = [this._selected, ...featureOptions];

    return this.options;
  }

  @action
  search(value) {
    if (value.length >= 4) {
      return this.performSearchRecord(value);
    }

    this.change({
      type: 'Text',
      value,
    });

    this.options = [this._selected];
    this.change(value);

    return this.options;
  }

  get selected() {
    let value = this.args.value ?? {};

    if (this._selected) {
      value = this._selected;
    }

    if (value.type == 'Feature' && !value.feature) {
      value.feature = this.store.peekRecord('feature', value.value);
    }

    return value;
  }
}
