/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupIntl } from 'ember-intl/test-support';

describe('Integration | Component | manage/model-info', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-us');

  it('renders', async function () {
    await render(hbs`<Manage::ModelInfo @onEdit={{true}} @onSave={{true}} />`);

    expect(this.element.textContent.trim()).to.contain('original author');
    expect(this.element.textContent.trim()).to.contain('edit');
    expect(this.element.textContent.trim()).to.contain('last change');
  });
});
