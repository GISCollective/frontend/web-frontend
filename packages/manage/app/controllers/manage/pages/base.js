/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { getSizeBsFromName } from 'core/lib/responsive';
import { debounce } from '@ember/runloop';

export default class ManagePagesBaseController extends Controller {
  @tracked deviceSize;
  @tracked selectedId;
  @service router;
  @service intl;

  queryParams = ['selectedId'];

  get deviceSizeClass() {
    if (!this.deviceSize) {
      return '';
    }

    return `enforce-${this.deviceSize}-size`;
  }

  get size() {
    return getSizeBsFromName(this.deviceSize);
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        route: 'manage.dashboards.space',
        text: this.model.space.name,
        model: this.model.space?.id,
      },
      {
        text: this.model.page.name,
      },
    ];
  }

  updateSelectedId(value) {
    this.selectedId = value;
  }

  @action
  onModelIdChange(value) {
    debounce(this, this.updateSelectedId, value, 500);
  }

  @action
  changeDeviceSize(value) {
    this.deviceSize = value;
  }
}
