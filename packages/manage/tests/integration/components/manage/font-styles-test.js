/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { FontStyles } from 'models/transforms/font-styles';

describe('Integration | Component | manage/font-styles', function (hooks) {
  setupRenderingTest(hooks);

  it('renders shows a nice message when there are no custom styles', async function () {
    this.set('value', new FontStyles({}));

    await render(hbs`<Manage::FontStyles @title="title" @value={{this.value}} />`);

    expect(this.element.querySelector('.alert-info').textContent.trim()).to.equal('There is no customized font style.');
  });

  it('renders the values in view mode', async function () {
    this.set(
      'value',
      new FontStyles({
        h1: ['fw-bold', 'text-size-32', 'ff-lato'],
      }),
    );

    await render(hbs`<Manage::FontStyles @title="title" @value={{this.value}} />`);

    expect(this.element.querySelector('.alert-info')).not.to.exist;

    const labels = [...this.element.querySelectorAll('.style-name')].map((a) => a.textContent.trim());
    const classes = [...this.element.querySelectorAll('em')].map((a) => a.textContent.trim());

    expect(labels).to.deep.equal(['Heading 1']);
    expect(this.element.querySelector('.preview-text')).to.have.classes('fw-bold text-size-32 ff-lato');
    expect(classes).to.deep.equal(['fw-bold', 'text-size-32', 'ff-lato']);
  });

  it('can change the values in edit mode', async function () {
    this.set(
      'value',
      new FontStyles({
        h1: ['fw-bold', 'text-size-32', 'ff-lato'],
      }),
    );

    let savedValue;

    this.set('save', (val) => {
      savedValue = val;
    });

    await render(
      hbs`<Manage::FontStyles @title="title" @editablePanel="title" @onSave={{this.save}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.alert-info')).not.to.exist;

    const element = this.element.querySelector('.style-item');

    element.querySelector('.input--ff').value = 'anton';
    await triggerEvent('.input--ff', 'change');

    element.querySelector('.input--fw').value = 'light';
    await triggerEvent('.input--fw', 'change');

    element.querySelector('.input--text-size').value = '18';
    await triggerEvent('.input--text-size', 'change');

    await click('.btn-submit');

    expect(savedValue.toJSON()).to.deep.equal({
      h1: ['ff-anton', 'fw-light', 'text-size-18'],
      h2: [],
      h3: [],
      h4: [],
      h5: [],
      h6: [],
      paragraph: [],
    });
  });
});
