/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { onPictureUpload } from '../../lib/editor-js-picture';

export default class NewRecordFormArticleMessageComponent extends Component {
  @service store;

  @tracked title = '';
  @tracked article = {};
  @tracked subject;
  @tracked releaseDate = new Date();
  pictures = [];

  get canSubmit() {
    return this.article && this.title && this.subject && this.releaseDate;
  }

  get newsletterVisibility() {
    return {
      isPublic: false,
      sDefault: false,
      team: this.args.newsletter.visibility.teamId,
    };
  }

  @action
  onPictureUpload(value, type) {
    return onPictureUpload(value, type, this.store, {}, true, (picture) => {
      this.pictures.push(picture);
    });
  }

  createArticle() {
    return this.store.createRecord('article', {
      id: null,
      title: this.title,
      content: this.article,
      slug: '',
      subject: this.subject,
      type: 'newsletter-article',
      relatedId: this.args.newsletter.id,
      status: 'draft',
      releaseDate: this.releaseDate,
      visibility: this.newsletterVisibility,
    });
  }

  @action
  onChangeDate(value) {
    this.releaseDate = value;
  }

  @action
  changeArticleValue(title, value) {
    this.title = title;
    this.article = value;
    delete this.article.time;
  }

  @action
  async save() {
    const article = this.createArticle();
    await article.save();

    return this.args.onSave?.(article);
  }
}
