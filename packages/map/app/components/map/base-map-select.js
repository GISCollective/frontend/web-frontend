/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default class MapBaseMapSelectComponent extends Component {
  get unselectedItems() {
    const selectedId = this.args.selected?.get
      ? this.args.selected.get('id')
      : this.args.selected?._id;

    return (
      this.args.list?.filter?.((a) => {
        const id = a.get ? a.get('id') : a._id;

        return id != selectedId;
      }) ?? []
    );
  }

  get styles() {
    return (
      this.unselectedItems?.map?.((baseMap) => {
        const picture = baseMap?.get
          ? baseMap.get('cover.picture')
          : baseMap?.cover?.picture;

        const size = picture?.indexOf('.jpg') > 0 ? '.md.jpg' : '/md';

        return {
          baseMap,
          style: htmlSafe(`background-image: url(${picture}${size});`),
        };
      }) ?? []
    );
  }

  @action
  select(item) {
    this.args.onSelect?.(item);
  }
}
