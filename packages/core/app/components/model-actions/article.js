/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ModelActionsArticleComponent extends Component {
  @service notifications;
  @service intl;

  @action
  publish() {
    return this.args.record?.publish?.();
  }

  @action
  async unpublish() {
    await this.notifications.ask({
      title: this.intl.t('unpublish newsletter'),
      question: this.intl.t('message-newsletter-unpublish'),
    });

    return this.args.record?.unpublish?.();
  }

  @action
  sendTestEmail() {
    return this.args.record?.sendTestEmail?.();
  }

  @action
  async sendToAll() {
    await this.notifications.ask({
      title: this.intl.t('send newsletter'),
      question: this.intl.t('message-newsletter-send-to-all'),
    });

    this.args.record.status = 'pending';

    return this.args.record?.save?.();
  }
}
