/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/authenticated-admin-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class AdminAppearanceRoute extends Route {
  @service user;

  async model() {
    const appearanceName = await this.preferences.requestPreference('appearance.name');
    const appearanceLogo = await this.preferences.requestPreference('appearance.logo');
    const appearanceCover = await this.preferences.requestPreference('appearance.cover');
    const appearanceExtent = await this.preferences.requestPreference('appearance.mapExtent');
    const appearanceShowWelcomePresentation = await this.preferences.requestPreference('appearance.showWelcomePresentation');

    return hash({
      appearanceName,
      appearanceLogo,
      appearanceCover,
      appearanceExtent,
      appearanceShowWelcomePresentation,
      logoImage: this.store.findRecord('picture', appearanceLogo.value),
      coverImage: this.store.findRecord('picture', appearanceCover.value),
      baseMaps: this.store.query('base-map', { default: true }),
    });
  }
}
