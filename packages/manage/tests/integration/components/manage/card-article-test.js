/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { A } from 'core/lib/array';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/card-article', function (hooks) {
  setupRenderingTest(hooks);
  let articleData;
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    articleData = server.testData.storage.addDefaultArticle();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders an article card', async function () {
    this.set('article', await this.store.findRecord('article', articleData._id));

    await render(hbs`<Manage::CardArticle @article={{this.article}} />`);

    expect(this.element.querySelector('.title').textContent.trim()).to.equal('title');
    expect(this.element.querySelector('.description').textContent.trim()).to.equal('some content');
    expect(this.element.querySelector('.slug').textContent.trim()).to.equal('5ca78e2160780601008f69e6');
  });

  it('does not render the slug when it is not set', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.slug = '';

    this.set('article', article);

    await render(hbs`<Manage::CardArticle @article={{this.article}} />`);
    expect(this.element.querySelector('.slug')).not.to.exist;
  });

  it('does not render the slug when the article is a newsletter article', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.type = 'newsletter-article';

    this.set('article', article);

    await render(hbs`<Manage::CardArticle @article={{this.article}} />`);
    expect(this.element.querySelector('.slug')).not.to.exist;
  });

  it('renders the subject as title when it is set', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.subject = 'the subject';

    this.set('article', article);

    await render(hbs`<Manage::CardArticle @article={{this.article}} />`);

    expect(this.element.querySelector('.title').textContent.trim()).to.equal('the subject');
  });

  describe('the selection checkbox', function () {
    it('renders an unselected checkbox when the record can be edited', async function () {
      const article = await this.store.findRecord('article', articleData._id);
      article.canEdit = true;

      this.set('article', article);

      await render(hbs`<Manage::CardArticle @onSelect={{true}} @article={{this.article}} />`);

      expect(this.element.querySelector('.chk-selected').checked).to.equal(false);
    });

    it('renders a selected checkbox when the record can be edited and has the id in the selection list', async function () {
      const article = await this.store.findRecord('article', articleData._id);
      article.canEdit = true;

      this.set('article', article);
      this.set('selection', [article.id]);

      await render(
        hbs`<Manage::CardArticle @article={{this.article}} @onSelect={{true}} @selection={{this.selection}} />`,
      );

      expect(this.element.querySelector('.chk-selected').checked).to.equal(true);
    });

    it('renders a disabled selected checkbox when the record can be edited and all are selected', async function () {
      const article = await this.store.findRecord('article', articleData._id);
      article.canEdit = true;

      this.set('article', article);
      this.set('selection', 'all');

      await render(
        hbs`<Manage::CardArticle @article={{this.article}} @onSelect={{true}} @selection={{this.selection}} />`,
      );

      expect(this.element.querySelector('.chk-selected').checked).to.equal(true);
      expect(this.element.querySelector('.chk-selected')).to.have.attribute('disabled', '');
    });

    it('does not render a checkbox when the record is not editable', async function () {
      const article = await this.store.findRecord('article', articleData._id);
      article.canEdit = false;

      this.set('article', article);

      await render(hbs`<Manage::CardArticle @article={{this.article}} @selection={{this.selection}} />`);

      expect(this.element.querySelector('.chk-selected')).not.to.exist;
    });

    it('triggers an event when the checkbox is selected', async function () {
      const article = await this.store.findRecord('article', articleData._id);
      article.canEdit = true;

      this.set('article', article);

      let value;
      this.set('select', (v) => {
        value = v;
        this.set('selection', v);
      });

      await render(
        hbs`<Manage::CardArticle @article={{this.article}} @selection={{this.selection}} @onSelect={{this.select}} />`,
      );

      await click('.chk-selected');

      expect(value).to.deep.equal([article.id]);
      expect(this.element.querySelector('.chk-selected').checked).to.equal(true);
    });

    it('triggers an event when the checkbox is deselected', async function () {
      const article = await this.store.findRecord('article', articleData._id);
      article.canEdit = true;

      this.set('article', article);
      this.set('selection', A([article.id]));

      let value;
      this.set('select', (v) => {
        value = v;
        this.set('selection', v);
      });

      await render(
        hbs`<Manage::CardArticle @article={{this.article}} @selection={{this.selection}} @onSelect={{this.select}} />`,
      );

      await click('.chk-selected');

      expect(value).to.deep.equal([]);
      expect(this.element.querySelector('.chk-selected').checked).to.equal(false);
    });
  });
});
