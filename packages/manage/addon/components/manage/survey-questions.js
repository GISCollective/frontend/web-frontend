/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { A } from 'core/lib/array';

export default class ManageSurveyQuestionsComponent extends Component {
  fields = ['sounds', 'pictures', 'icons', 'description', 'name', 'position'];

  get value() {
    return this.args.value ?? [];
  }

  get viewQuestions() {
    return this.questions.filter((a) => a?.isVisible);
  }

  get questions() {
    const existingQuestions = this.value.map((a) => this.mapQuestion(a));

    let newQuestions = [];

    for (const name of this.fields.reverse()) {
      if (!existingQuestions.find((a) => a.name === name)) {
        newQuestions.push(this[`${name}Question`]);
      }
    }


    return A([...newQuestions, ...existingQuestions]);
  }

  mapQuestion(question) {
    for (const name of this.fields) {
      if (question.name == name) {
        return this[`${name}Question`];
      }
    }

    return question;
  }

  defaultQuestion(name, type, question) {
    const item = this.args.value?.find((a) => a.name == name) ?? {};
    const exists = !!this.args.value?.find((a) => a.name == name);

    return {
      question: item.question ?? question,
      help: item.help,
      type,
      name,
      isRequired: exists ? item.isRequired : false,
      isVisible: exists ? item.isVisible : false,
      options: item.options,
      disableDelete: true,
      hasVisibility: true,
    };
  }

  get positionQuestion() {
    return this.defaultQuestion('position', 'geo json', 'Location');
  }

  get nameQuestion() {
    return this.defaultQuestion('name', 'short text', "What's the name of this place?");
  }

  get descriptionQuestion() {
    return this.defaultQuestion('description', 'long text', 'What do you like about this place?');
  }

  get iconsQuestion() {
    return this.defaultQuestion('icons', 'icons', 'Please select an icon from the list');
  }

  get picturesQuestion() {
    return this.defaultQuestion('pictures', 'pictures', 'Pictures');
  }

  get soundsQuestion() {
    return this.defaultQuestion('sounds', 'sounds', 'Sounds');
  }
}
