/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Model | icon', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('icon', {});
    expect(model).to.be.ok;
  });

  it('fills in the meta info object', async function () {
    let store = this.owner.lookup('service:store');

    const iconSet = store.createRecord('icon-set', {
      _id: '5ca7b702ecd8490100cab96f',
    });

    let model = store.createRecord('icon', {
      parent: '',
      otherNames: ['Eco expert'],
      image: {
        useParent: false,
        value: 'http://localhost:9091/icons/5ca7bfd2ecd8490100cab9b5/image/value',
      },
      localName: 'Expert Eco',
      maxRelevanceLevel: 4,
      subcategory: 'Justice and Activism',
      verticalIndex: 0,
      _id: '5ca7bfd2ecd8490100cab9b5',
      category: 'Culture and Society - Green Map Icons',
      canEdit: true,
      name: 'Eco Expert',
      description: 'Place to get excellent information on environmental issues.',
      attributes: [],
      iconSet,
    });

    const metaInfo = await model.fillMetaInfo({});

    expect(metaInfo).to.deep.equal({
      title: 'Expert Eco',
      description: 'Place to get excellent information on environmental issues.',
      imgSrc: 'http://localhost:9091/icons/5ca7bfd2ecd8490100cab9b5/image/value/lg',
    });
  });
});
