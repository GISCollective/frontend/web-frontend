/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class InputLayoutUserVisibilityButtonComponent extends Component {
  get hasAuthenticatedAction() {
    return !!this.args.conditions?.find((a) => a == "user:authenticated");
  }

  get iconName() {
    return this.hasAuthenticatedAction ? "user" : "user-slash";
  }

  @action
  toggle() {
    const newValues =
      this.args.conditions?.map((a) => {
        if (a == "user:authenticated") {
          return "user:unauthenticated";
        }

        if (a == "user:unauthenticated") {
          return "user:authenticated";
        }

        return a;
      }) ?? [];

    this.args.onChange?.(newValues);
  }
}
