/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | input/global-layout-container', function (hooks) {
  let spaceData;

  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    spaceData = server.testData.storage.addDefaultSpace();
    spaceData.layoutContainers = {
      'container 1': {},
      'container 2': {},
    };
  });

  it('renders the space global layout options', async function () {
    let store = this.owner.lookup('service:store');

    this.set('space', await store.findRecord('space', spaceData._id));

    await render(
      hbs`<Input::GlobalLayoutContainer @space={{this.space}} @container={{this.container}} @onChange={{this.changeContainer}} />`,
    );

    await selectSearch('.ember-power-select-trigger', '');

    const labels = this.element.querySelectorAll('.ember-power-select-options .label');

    expect(labels).to.have.length(2);
    expect(labels[0].textContent.trim()).to.equal('container 1');
    expect(labels[1].textContent.trim()).to.equal('container 2');
  });

  it('selects the right component when the container gid is in the space layout containers', async function () {
    this.set('space', await this.store.findRecord('space', spaceData._id));
    this.set('container', { gid: 'container 2' });

    await render(
      hbs`<Input::GlobalLayoutContainer @space={{this.space}} @container={{this.container}} @onChange={{this.changeContainer}} />`,
    );

    expect(this.element.querySelector('.ember-power-select-selected-item .label').textContent.trim()).to.equal(
      'container 2',
    );
  });

  it('triggers onChange when a container is selected', async function () {
    this.set('space', await this.store.findRecord('space', spaceData._id));
    this.set('container', { gid: 'container 2' });

    let value;
    this.set('onChange', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::GlobalLayoutContainer @space={{this.space}} @container={{this.container}} @onChange={{this.onChange}} />`,
    );

    await selectSearch('.ember-power-select-trigger', 'container 1');
    await selectChoose('.ember-power-select-trigger', 'container 1');

    expect(value?.gid).to.equal('container 1');
  });

  it('triggers onChange with a cloned container and no gid when a global container is deselected', async function () {
    this.set('space', await this.store.findRecord('space', spaceData._id));
    this.set('container', { gid: 'container 2' });

    let value;
    this.set('onChange', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::GlobalLayoutContainer @space={{this.space}} @container={{this.container}} @onChange={{this.onChange}} />`,
    );

    await selectSearch('.ember-power-select-trigger', 'container 2');
    await selectChoose('.ember-power-select-trigger', 'container 2');

    await click('.btn-none');

    expect(value).not.to.equal(this.container);
    expect(value?.gid).to.equal('');
  });
});
