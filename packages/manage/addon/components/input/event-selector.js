import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class InputEventSelector extends Component {
  @tracked list;
  @service store;

  @action
  async setup() {
    this.list = await this.store.query('event', { team: this.args.team, sortBy: 'name', sortOrder: 'asc' });
  }
}
