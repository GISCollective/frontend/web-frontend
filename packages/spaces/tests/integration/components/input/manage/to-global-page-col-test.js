/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { click, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | input/manage/to-global-page-col", function (hooks) {
  setupRenderingTest(hooks);

  it("renders", async function () {
    await render(hbs`<Input::Manage::ToGlobalPageCol />`);

    expect(this.element.querySelector(".btn-to-global-page-col").textContent.trim()).to.equal(
      "convert to global component",
    );
  });

  it("can enter an id for the global component", async function () {
    let id;

    this.set("submit", (a) => {
      id = a;
    });

    await render(hbs`<Input::Manage::ToGlobalPageCol @onSubmit={{this.submit}}/>`);

    await click(".btn-to-global-page-col");
    await fillIn(".input-global-page-col-id", "some-id");

    await click(".btn-submit");

    expect(id).to.equal("some-id");
    expect(this.element.querySelector(".btn-to-global-page-col")).to.exist;
  });

  it("shows an error when the submit event throws", async function () {
    let id;

    this.set("submit", () => {
      throw new Error("the component exist");
    });

    await render(hbs`<Input::Manage::ToGlobalPageCol @onSubmit={{this.submit}}/>`);

    await click(".btn-to-global-page-col");
    await fillIn(".input-global-page-col-id", "some-id");

    await click(".btn-submit");

    expect(this.element.querySelector(".invalid-feedback").textContent.trim()).to.equal("the component exist");

    expect(id).not.to.exist;
    expect(this.element.querySelector(".btn-to-global-page-col")).not.to.exist;
  });

  it("shows an error when the gid already exists", async function () {
    this.set("space", { hasPageCol: () => true });

    await render(hbs`<Input::Manage::ToGlobalPageCol @space={{this.space}} @onSubmit={{this.submit}}/>`);

    await click(".btn-to-global-page-col");
    await fillIn(".input-global-page-col-id", "some-id");

    expect(this.element.querySelector(".invalid-feedback").textContent.trim()).to.equal(
      "the id is already used by another component",
    );
    expect(this.element.querySelector(".btn-to-global-page-col")).not.to.exist;
  });
});
