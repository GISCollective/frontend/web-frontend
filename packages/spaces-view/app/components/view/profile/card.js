/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class PageColProfileCardComponent extends Component {
  @service store;

  @tracked profile;
  @tracked picture;

  get profilePath() {
    return `/browse/profiles/${this.profile.id}`;
  }

  @action
  async recordLoaded(record) {
    const recordId = record?.info?.originalAuthor;

    if (!recordId) {
      this.profile = null;

      return;
    }

    try {
      this.profile = await this.store.findRecord('user-profile', recordId);
    } catch (err) {
      this.profile = null;
    }

    try {
      this.picture = await this.profile.picture;
    } catch (err) {
      this.picture = null;
    }
  }
}
