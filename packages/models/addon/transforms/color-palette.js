/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { A } from "../lib/array";

export default class ColorPaletteTransform extends Transform {
  deserialize(serialized) {
    return new ColorPalette(serialized);
  }

  serialize(deserialized) {
    if (deserialized?.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export const standardColors = ['blue', 'indigo', 'purple', 'pink', 'red', 'orange', 'yellow', 'green', 'teal', 'cyan'];

export class CustomColor {
  @tracked name;
  @tracked lightValue;
  @tracked darkValue;

  constructor(value) {
    if (value?.name) {
      this.name = value.name;
    }

    if (value?.lightValue) {
      this.lightValue = value.lightValue;
    }

    if (value?.darkValue) {
      this.darkValue = value.darkValue;
    }
  }

  toJSON() {
    return { name: this.name, lightValue: this.lightValue, darkValue: this.darkValue };
  }
}

export class ColorPalette {
  @tracked blue = '#3961d0';
  @tracked indigo = '#2a1a68';
  @tracked purple = '#663eff';
  @tracked pink = '#ff3e76';
  @tracked red = '#e50620';
  @tracked orange = '#e55b06';
  @tracked yellow = '#e5ca06';
  @tracked green = '#107353';
  @tracked teal = '#5d7388';
  @tracked cyan = '#159ea7';
  @tracked customColors = A();

  constructor(value) {
    if (!value || typeof value != 'object') {
      return;
    }

    for (const color of standardColors) {
      if (value[color]) {
        this[color] = value[color];
      }
    }

    if(Array.isArray(value?.customColors)) {
      this.customColors = value.customColors.map(a => new CustomColor(a));
    }
  }

  toJSON() {
    const result = {};

    for (const color of standardColors) {
      result[color] = this[color];
    }

    result.customColors = this.customColors.map(a => a.toJSON())

    return result;
  }
}
