/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import Map from 'ol/Map';
import View from 'ol/View';
import { service } from '@ember/service';
import * as olInteraction from 'ol/interaction';
import { later } from '@ember/runloop';
import { buildWaiter } from '@ember/test-waiters';
const waiter = buildWaiter('map-plain');

export default class MapPlainComponent extends Component {
  @service loading;
  @tracked map;
  @tracked isReady;

  get center() {
    return this.args.center ?? [0, 0];
  }

  @action
  async setupMapElement(element) {
    if (this.isDestroyed || this.isDestroying) {
      return;
    }

    this.isLoading = true;

    let token = waiter.beginAsync();
    this.mapElement = element;

    let interactions = this.args.disableInteractions
      ? []
      : olInteraction.defaults({
          doubleClickZoom: false,
        });

    this.map = new Map({
      controls: [],
      target: element,
      loadTilesWhileAnimating: true,
      loadTilesWhileInteracting: true,
      interactions,
      layers: [],
      view: new View({
        center: this.center,
        zoom: 2,
      }),
    });

    this.mapElement.olMap = this.map;

    later(() => {
      this.args.onReady?.(this.map);
      waiter.endAsync(token);
      this.isLoading = false;
      this.isReady = true;
    }, 100);
  }
}
