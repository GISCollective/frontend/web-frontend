/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/value-boolean-required', function (hooks) {
  setupRenderingTest(hooks);

  it('does not render the unknown option', async function () {
    await render(hbs`<Attributes::ValueBooleanRequired />`);

    expect(this.element.querySelector('.btn-unknown')).not.to.exist;
    expect(this.element.querySelector('.btn-true')).not.to.have.class('btn-secondary');
    expect(this.element.querySelector('.btn-false')).not.to.have.class('btn-secondary');
  });
});
