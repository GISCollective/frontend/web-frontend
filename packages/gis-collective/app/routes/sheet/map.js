/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../base/authenticated-route';
import { service } from '@ember/service';
import { A } from "core/lib/array";
import { tracked } from '@glimmer/tracking';

export default class PresentationIndexRoute extends AuthenticatedRoute {
  @service fastboot;

  queryParams = {
    sortBy: {
      refreshModel: true,
    },
    isAscending: {
      refreshModel: true,
    },
    icon: {
      refreshModel: true,
    },
  };

  async getIcons(params) {
    if (!this._icons || params.id != this.loadedMap) {
      try {
        this._icons = await this.store.query('icon', {
          map: params.id,
        });
      } catch (err) {
        this._icons = [];
      }
    }

    return this._icons;
  }

  async getRecords(params, meta, currentSort) {
    if (!this._records || params.id != this.loadedMap) {
      this._records = A([]);

      for (let i = 0; i < meta?.totalFeatures ?? 0; i++) {
        this._records.push({});
      }
    }

    if (currentSort != this.currentSort) {
      for (let i = 0; i < meta?.totalFeatures ?? 0; i++) {
        this._records[i] = {};
      }
    }

    return this._records;
  }

  async getMap(params) {
    let map = this.store.peekRecord('map', params.id);

    if (!map) {
      map = await this.store.findRecord('map', params.id);
    }

    return map;
  }

  async getMeta(params) {
    if (!this._meta || params.id != this.loadedMap) {
      let map = this.store.peekRecord('map', params.id);
      this._meta = await map.reloadMeta();
    }

    return this._meta;
  }

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    const result = new SheetMapModel();

    result.map = await this.getMap(params);
    result.meta = await this.getMeta(params);

    const currentSort = `${params.sortBy}-${params.isAscending}`;

    result.records = await this.getRecords(params, result.meta, currentSort);
    result.icons = await this.getIcons(params);

    this.loadedMap = params.id;
    this.currentSort = currentSort;

    return result;
  }

  resetController(controller, isExiting, transition) {
    if (isExiting && transition.targetName !== 'error') {
      return
    }

    return controller.loadFeatures();
  }
}

class SheetMapModel {
  @tracked map;
  @tracked meta;
  @tracked records;
  @tracked icons;
}