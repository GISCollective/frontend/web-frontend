/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/options/col-options',  function (hooks) {
  setupRenderingTest(hooks);

  it('can change the col name', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', { name: 'name', options: ['col-lg-4'] });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}} />`
    );

    expect(this.element.querySelector('.input-col-name').value).to.equal(
      'name'
    );

    await fillIn('.input-col-name', 'new name');

    expect(value).to.deep.equal({
      options: ['col-lg-4'],
      name: 'new name',
      componentCount: 1,
    });
  });

  it('can change the content direction', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', {
      name: 'name',
      options: ['col-lg-4', 'display-content-block'],
    });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}} />`
    );

    expect(this.element.querySelector('.input-display-mode').value).to.equal(
      'block'
    );

    this.element.querySelector('.input-display-mode').value = 'inline-block';
    await triggerEvent('.input-display-mode', 'change');

    expect(value).to.deep.equal({
      options: ['col-lg-4', 'display-content-inline-block'],
      componentCount: 1,
      name: 'name',
    });
  });

  it('can change the number of components', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', {
      name: 'name',
      componentCount: 3,
      options: [],
    });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}} />`
    );

    expect(
      this.element.querySelector('.input-col-component-count').value
    ).to.equal('3');

    await fillIn('.input-col-component-count', 4);

    expect(value).to.deep.equal({
      options: [],
      name: 'name',
      componentCount: 4,
    });
  });

  it('can not set the number of components to 0', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', {
      name: 'name',
      componentCount: 3,
      options: [],
    });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}} />`
    );

    expect(
      this.element.querySelector('.input-col-component-count').value
    ).to.equal('3');

    await fillIn('.input-col-component-count', 0);

    expect(value).to.deep.equal({
      options: [],
      name: 'name',
      componentCount: 1,
    });
  });

  it('can change the distance between components', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', {
      name: 'name',
      componentCount: 3,
      options: [],
    });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}} />`
    );

    this.element.querySelector('.input-col-component-margin').value = '2';
    await triggerEvent('.input-col-component-margin', 'change');

    expect(value).to.deep.equal({
      options: ['component-m-2'],
      name: 'name',
      componentCount: 3,
    });
  });

  it('can change the order', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ColOptions @onChangeOptions={{this.change}} />`
    );

    expect(this.element.querySelector('.input-sm-order').value).to.equal(
      'default'
    );

    this.element.querySelector('.input-sm-order').value = '4';
    await triggerEvent('.input-sm-order', 'change');

    expect(value).to.deep.equal({
      options: ['order-4'],
      name: '',
      componentCount: 1,
    });
  });

  it('shows the order value', async function () {
    this.set('col', { options: ['order-3'] });

    await render(hbs`<Input::Options::ColOptions @col={{this.col}} />`);

    expect(this.element.querySelector('.input-sm-order').value).to.equal('3');
  });
});
