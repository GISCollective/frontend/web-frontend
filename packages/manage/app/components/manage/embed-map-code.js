/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';

export default class BrowseMapEmbedCodeComponent extends Component {
  elementId = `browse-map-embed-${guidFor(this)}`;

  @service space;
  @tracked showMore = true;
  @tracked mapInfo = true;
  @tracked filterManyIcons = false;
  @tracked showAllIcons = false;

  get domain() {
    return this.space.currentDomain;
  }
}
