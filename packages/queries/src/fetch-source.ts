/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { LazyList } from "./lazy-list";
import { StoreType } from "./StoreType";
import { dasherize } from "./string";

export type ModelData = Record<string, any>;

export interface Visibility {

}

export interface SpaceType {
  get(jey: string): any;
  teamId: string;
  defaultModels: Record<string, string>;
  visibility: Visibility;
}

export interface SourceParameter {
  source: string;
  value: string;
}

export interface Query {
  parameters?: Record<string, string | Record<string, string | number | boolean>> | SourceParameter;
  ids?: string[];
}

export interface PageColType {
  name: string;
  data?: {
    source?: SourceData;

    // legacy
    ids?: string[];
    model?: string;
  };

  type: string;
  buckets?: any;
  currentQuery?: string;
  query: Record<string, string>;
}

export interface SourceData {
  model?: string;
  id?: string;
  ids?: string[];
  property?: string;
  useSelectedModel?: boolean;
  useDefaultModel?: boolean;
}

function get<T extends Object>(obj: T, path: string | string[]) {
  if (typeof path == "string") {
    return get(obj, path.split("."));
  }

  const [head, ...tail] = path;
  //@ts-ignore
  const value = obj[head];

  if (tail.length == 0 && typeof head == "string") {
    return value;
  }

  return get(value, tail);
}

export async function fetchSourceList<T extends Object>(
  modelName: string,
  query: Query,
  modelData: ModelData,
  space: SpaceType,
  store: StoreType,
) {
  const modelQuery: Record<string, any> = {};
  const selectedId = modelData?.selectedId;

  if (query.parameters) {
    for (const key of Object.keys(query.parameters)) {
      //@ts-ignore
      const parameter = query.parameters[key] ?? {};

      let value: string | string[] = "";

      if (typeof parameter == "string") {
        value = parameter;

        /// deprecated
        //@ts-ignore
        if (parameter == "selected-model") {
          value = selectedId;
        }

        /// deprecated
        //@ts-ignore
        if (parameter?.indexOf?.("state-") === 0) {
          //@ts-ignore
          value = modelData[parameter];
        }

        if(value) {
          modelQuery[key] = value;
        }

        continue;
      }

      if (parameter.source == "fixed") {
        //@ts-ignore
        value = parameter.value;
      }

      //@ts-ignore
      if (parameter.source == "selected-model") {
        value = selectedId;
      }

      //@ts-ignore
      if (parameter.source == "default") {
        const record = await fetchDefault(key, space, store);
        value = record.id;
      }

      //@ts-ignore
      if (parameter.source == "state") {
        //@ts-ignore
        const state = query.parameters[key].state;
        const dashedState = dasherize(state);

        //@ts-ignore
        value = modelData[`state-${state}`] || modelData[`state-${dashedState}`] || query.parameters[key].value;
      }

      if (Array.isArray(value)) {
        value = value.join(",");
      }

      if(value) {
        modelQuery[key] = value;
      }
    }
  }

  if (query.ids) {
    const records: (T | undefined)[] = await Promise.all(
      query.ids.map(async (id) => {
        let record = store.peekRecord<T>(modelName, id);

        try {
          return record ?? await store.findRecord<T>(modelName, id);
        } catch(err) {
          console.error(err);
        }
      }),
    );

    const list = new LazyList<T>([records.filter(a => a) as T[]], {}, store);
    list.model = modelName;
    return list;
  }

  const list = new LazyList(modelName, modelQuery, store);
  await list.loadNext();

  return list;
}

export async function fetchSourceRecord<T extends Object>(
  source: SourceData,
  space: SpaceType | undefined,
  store: StoreType,
) {
  if (!store || !source?.model) {
    return;
  }

  let record;

  try {
    if (source.id) {
      record = store.peekRecord(source.model, source.id);
    }

    if (record) {
      return record;
    }

    const query: Record<string, string> = { id: source.id ?? "" };

    if (source.model == "article") {
      query.team = space?.get ? space.get("teamId") : space?.teamId;
    }

    record = await store.queryRecord<T>(source.model, query);

    if (record && "loadRelations" in record && typeof record.loadRelations == "function") {
      await record?.loadRelations?.();
    }
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
  }

  return record;
}

export async function fetchDefault(model: string, space?: SpaceType, store?: StoreType) {
  if (!space) {
    console.error("The 'space' is not set!");
    return null;
  }

  const defaultModels = space.get ? space.get("defaultModels") : space.defaultModels;
  const visibility = space.get ? space.get("visibility") : space.visibility;
  const id = defaultModels?.[model];

  if (model == "team") {
    try {
      return await visibility.fetchTeam();
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  if (!id) {
    return null;
  }

  let record = store?.peekRecord(model, id);

  if (record) {
    return record;
  }

  try {
    return await store?.findRecord(model, id);
  } catch (err) {
    console.error(err);
    return null;
  }
}

export function restoreLegacySource(pageCol: PageColType) {
  return {
    model: pageCol.data?.source?.model ?? pageCol.data?.model,
    ids: pageCol.data?.source?.ids ?? pageCol.data?.ids,
  };
}

async function fetchSourceListPageCol(modelData: ModelData, pageCol: PageColType, space: SpaceType, store: StoreType) {
  try {
    pageCol.currentQuery = JSON.stringify(pageCol.query);

    if (pageCol.data?.source?.model) {
      pageCol.buckets = await fetchSourceList(pageCol.data.source.model, pageCol.query, modelData, space, store);
    }
  } catch (err) {
    if (!pageCol.buckets?.length) {
      pageCol.buckets = null;
    }
  }

  if (pageCol.data) {
    await new Promise((resolve) =>
      setTimeout(() => {
        // @ts-ignore
        pageCol.data = {
          ...pageCol.data,
        };
        resolve(null);
      }, 1),
    );
  }

  return pageCol.buckets;
}

async function fetchProperty(modelData: ModelData, source: SourceData) {
  if (!source.property) {
    throw new Error("The property is not set for " + JSON.stringify(source));
  }

  if (!modelData) {
    return;
  }

  let model = {};

  if (source.useSelectedModel) {
    model = modelData["selectedModel"];
  }

  if (!model) {
    return;
  }

  //@ts-ignore
  if (typeof model?.[source.property] == "function") {
    //@ts-ignore
    return await model[source.property]();
  }

  let result = get(model, source.property);

  return result;
}

export async function fetch(modelData: ModelData, pageCol: PageColType, space: SpaceType, store: StoreType) {
  if (!pageCol) {
    return null;
  }

  let source = (await pageCol?.data?.source) ?? {};

  // fix the old property selection behavior
  if (pageCol.type == "gallery" && source.property && pageCol.data && pageCol.data.source) {
    pageCol.data.source.property = undefined;
  }

  if (pageCol?.data?.model) {
    source = restoreLegacySource(pageCol);
    pageCol.data.source = source;
    delete pageCol.data.model;
    delete pageCol.data.ids;
  }

  if (!source.model && !source.property) {
    return null;
  }

  if (source?.useDefaultModel && source.model) {
    return fetchDefault(source.model, space, store);
  }

  if (pageCol.data?.source?.property) {
    return fetchProperty(modelData, source);
  }

  if (pageCol.data?.source?.id) {
    return fetchSourceRecord(source, space, store);
  }

  if (pageCol.data?.source) {
    return fetchSourceListPageCol(modelData, pageCol, space, store);
  }

  return null;
}
