/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from './base';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { debounce } from '@ember/runloop';

export class Selection {
  @tracked type;
  @tracked title;
  @tracked data;

  constructor(type, title, data) {
    if (typeof type == 'object') {
      this.type = type.type;
      this.title = type.title;
      this.data = type.data ?? {};

      return;
    }

    this.type = type;
    this.title = title;
    this.data = data ?? {};
  }
}

export default class ManagePagesContentController extends Controller {
  @service store;
  @service router;

  @tracked selection;
  @tracked hasRestoredPage = undefined;
  @tracked conditions = ['user:unauthenticated'];
  @tracked _hasSpaceUpdate;
  @tracked s;
  @tracked selectedId;

  queryParams = ['selectedId'];

  async reset() {
    this.selection = new Selection('page', 'page');

    this.hasRestoredPage = undefined;
    this.selectedContainerName = undefined;
  }

  @action
  async saveContent() {
    if (this.hasSpaceUpdate) {
      await this.model.space.save();
      this._hasSpaceUpdate = false;
    }

    for (const key in this.model) {
      const resolvedRecord = await this.model[key];

      if (resolvedRecord?.shouldSave && key != 'page') {
        await resolvedRecord.save();
      }
    }

    this.model.page.space = this.model.space;
    this.model.page.cols = this.cols;

    const promise = this.model.page.save();
    this.hasRestoredPage = false;

    return promise;
  }

  @action
  async cloneContent() {
    const newPage = await this.model.page.clone();

    this.model.page.unloadRecord();

    this.router.transitionTo('manage.pages.content', newPage.id);
  }

  @action
  changeState(s) {
    this.s = s;
  }

  @action
  changeConditions(newConditions) {
    this.conditions = newConditions;
  }

  updateModelId(value) {
    this.router.transitionTo({ queryParams: { selectedId: value } });
    this.selectedId = value;
  }

  @action
  modelIdChange(value) {
    debounce(this, this.updateModelId, value, 500);
  }

  @action
  async revertPage(revisionId) {
    this.model.page = await this.model.page.loadRevision(revisionId);
    this.colsMap = undefined;
    this.hasRestoredPage = true;
  }

  async updateModel(col) {
    const key = col.modelKey;
    const hasModelKey = !!key;
    result = await fetch(this.model, col, this.model.space, this.store);

    if (hasModelKey && key != 'currentModel' && result) {
      this.model[key] = result;
    }
  }
}
