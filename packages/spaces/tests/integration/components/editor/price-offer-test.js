/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from "qunit";
import { setupRenderingTest, it } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent, click } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

module("Integration | Component | editor/price-offer", function (hooks) {
  let changedValue;
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders the price list options", async function (a) {
    this.set("value", {
      data: {
        priceList: [{ value: 1, currency: "usd", details: "details 1" }],
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".text-value-0").value).to.equal("1");
    expect(this.element.querySelector(".text-currency-0").value).to.equal("usd");
    expect(this.element.querySelector(".text-details-0").value).to.equal("details 1");
  });

  it("can change the price list options", async function () {
    this.set("value", {
      data: {
        priceList: [{ value: 1, currency: "usd", details: "details 1" }],
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".text-value-0", "22");
    await fillIn(".text-currency-0", "EUR");
    await fillIn(".text-details-0", "details 22");

    expect(changedValue.priceList).to.deep.equal([{ value: "22", currency: "EUR", details: "details 22" }]);
  });

  it("renders the title options", async function (a) {
    this.set("value", {
      data: {
        title: {
          text: "some title",
          classes: ["text-center"],
          color: "green",
        },
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".title-style .input-title-text").value).to.equal("some title");
    expect(this.element.querySelector(".title-style .text-color-selector .selected-color").textContent.trim()).to.equal(
      "green",
    );
    expect(this.element.querySelector(".title-style select.input-sm-text").value).to.equal("center");
  });

  it("can change the title options", async function () {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".title-style select.input-sm-text").value = "end";
    await triggerEvent(".title-style select.input-sm-text", "change");

    expect(changedValue.title).to.deep.equal({
      color: "",
      classes: ["text-end"],
      text: "",
      minLines: { sm: +0, md: +0, lg: +0 },
    });
  });

  it("renders the subtitle options", async function (a) {
    this.set("value", {
      data: {
        subtitle: {
          text: "some subtitle",
          classes: ["text-center"],
          color: "blue",
        },
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".subtitle-style .input-text-value").value).to.equal("some subtitle");
    expect(
      this.element.querySelector(".subtitle-style .text-color-selector .selected-color").textContent.trim(),
    ).to.equal("blue");
    expect(this.element.querySelector(".subtitle-style select.input-sm-text").value).to.equal("center");
  });

  it("can change the subtitle options", async function () {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".subtitle-style select.input-sm-text").value = "end";
    await triggerEvent(".subtitle-style select.input-sm-text", "change");

    expect(changedValue.subtitle).to.deep.equal({
      color: "",
      classes: ["text-end"],
      text: "",
      minLines: { lg: 0, md: 0, sm: 0 },
    });
  });

  it("renders the price options", async function (a) {
    this.set("value", {
      data: {
        price: {
          classes: ["text-center"],
          color: "red",
        },
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".price-style .text-color-selector .selected-color").textContent.trim()).to.equal(
      "red",
    );
    expect(this.element.querySelector(".price-style select.input-sm-text").value).to.equal("center");
  });

  it("can change the price options", async function (a) {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".price-style .input--ff").value = "anton";
    await triggerEvent(".price-style .input--ff", "change");

    a.deepEqual(changedValue.price, {
      classes: ["ff-anton"],
      color: "",
      minLines: { lg: 0, md: 0, sm: 0 },
    });
  });

  it("renders the price details options", async function (a) {
    this.set("value", {
      data: {
        priceDetails: {
          classes: ["text-center"],
          color: "red",
        },
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(
      this.element.querySelector(".price-details-style .text-color-selector .selected-color").textContent.trim(),
    ).to.equal("red");
    expect(this.element.querySelector(".price-details-style select.input-sm-text").value).to.equal("center");
  });

  it("can change the price details options", async function () {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".price-details-style select.input-sm-text").value = "end";
    await triggerEvent(".price-details-style select.input-sm-text", "change");

    expect(changedValue.priceDetails).to.deep.equal({
      color: "",
      classes: ["text-end"],
      minLines: { lg: 0, md: 0, sm: 0 },
    });
  });

  it("renders the button options", async function (a) {
    this.set("value", {
      data: {
        button: {
          name: "first button",
          link: "https://giscollective.com/page1",
          newTab: false,
          classes: ["btn-primary", "size-btn-lg"],
          type: "default",
          storeType: "apple",
        },
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".button-style .input-button-name").value).to.equal("first button");
    expect(this.element.querySelector(".button-style .input-sm-size-btn").value).to.equal("lg");
  });

  it("can change the button options", async function (a) {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".input-button-name", "button name");

    this.element.querySelector(".button-style select.input-sm-size-btn").value = "sm";
    await triggerEvent(".button-style .input-sm-size-btn", "change");

    a.deepEqual(changedValue.button, {
      classes: ["size-btn-sm"],
      link: undefined,
      name: "button name",
      newTab: undefined,
      storeType: undefined,
      type: undefined,
      enablePicture: undefined,
    });
  });

  it("renders the container options", async function (a) {
    this.set("value", {
      data: {
        container: {
          verticalSize: "fill",
          verticalAlign: "top",
          color: "blue-500",
          classes: ["ps-3"],
        },
      },
    });

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".container-section .sm-start-padding").value).to.equal("3");

    expect(this.element.querySelector(".container-section .bs-color-picker .selected-value.bg-blue-500")).to.exist;
  });

  it("can change the container options", async function () {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".container-section .bs-color-picker");
    await click(".container-section .btn-color-warning");

    expect(changedValue.container).to.deep.equal({
      color: "warning",
    });
  });

  it("can change the container paddings", async function (a) {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".container-section .sm-top-padding").value = "1";
    await triggerEvent(this.element.querySelector(".container-section .sm-top-padding"), "change");

    this.element.querySelector(".container-section .sm-bottom-padding").value = "2";
    await triggerEvent(this.element.querySelector(".container-section .sm-bottom-padding"), "change");

    this.element.querySelector(".container-section .sm-start-padding").value = "3";
    await triggerEvent(this.element.querySelector(".container-section .sm-start-padding"), "change");

    this.element.querySelector(".container-section .sm-end-padding").value = "4";
    await triggerEvent(this.element.querySelector(".container-section .sm-end-padding"), "change");

    a.deepEqual(changedValue.container, {
      classes: ["pt-1", "pb-2", "ps-3", "pe-4"],
    });
  });

  it("can change the border options", async function (a) {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".border-style .bs-color-picker");
    await click(".border-style .bs-color-picker .btn-color-danger");

    this.element.querySelector(".border-style .input-sm-border-visibility").value = "yes";
    await triggerEvent(this.element.querySelector(".border-style .input-sm-border-visibility"), "change");

    a.deepEqual(changedValue.container, {
      classes: ["border-color-danger", "border-visibility-yes"],
    });
  });

  it("can change the border and padding options", async function (a) {
    this.set("value", {});

    await render(hbs`<Editor::PriceOffer @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".border-style .bs-color-picker");
    await click(".border-style .bs-color-picker .btn-color-danger");

    this.element.querySelector(".container-section .sm-top-padding").value = "1";
    await triggerEvent(this.element.querySelector(".container-section .sm-top-padding"), "change");

    this.element.querySelector(".border-style .input-sm-border-visibility").value = "yes";
    await triggerEvent(this.element.querySelector(".border-style .input-sm-border-visibility"), "change");

    this.element.querySelector(".container-section .sm-end-padding").value = "4";
    await triggerEvent(this.element.querySelector(".container-section .sm-end-padding"), "change");

    a.deepEqual(changedValue.container, {
      classes: ["border-color-danger", "pt-1", "border-visibility-yes", "pe-4"],
    });
  });
});
