/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { action } from '@ember/object';
import Controller from '@ember/controller';
import { service } from '@ember/service';

export default class ManageIconsAddSetController extends Controller {
  @service notifications;
  @service router;
  @service intl;

  queryParams = ['c', 'sc'];

  get breadcrumbs() {
    const result = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    if (this.model.icon.get('iconSet.visibility.team')) {
      result.push({
        route: 'manage.dashboards.dashboard',
        text: this.model.icon.get('iconSet.visibility.team.name'),
        model: this.model.icon.get('iconSet.visibility.team.id'),
      });
    }

    if (this.model.icon.get('iconSet.id')) {
      result.push({
        route: 'manage.dashboards.icon-set',
        text: this.model.icon.get('iconSet.name'),
        model: this.model.icon.get('iconSet.id'),
      });
    }

    result.push({
      text: this.intl.t('new icon'),
      capitalize: true,
    });

    return result;
  }

  @action
  async save(record) {
    await record.save();

    return this.router.transitionTo('manage.icons.edit', record.iconSet.get('id'), record.get('id'));
  }
}
