/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class WizardImportFileSelectRoute extends Route {
  async model(params) {
    const map = await this.store.findRecord('map', params.id);

    return hash({
      map,
    });
  }
}
