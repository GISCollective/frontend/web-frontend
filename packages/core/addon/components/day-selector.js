/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { Datepicker } from 'vanillajs-datepicker';
import { action } from '@ember/object';
import { DateTime } from 'luxon';
import { toDateTime } from 'models/transforms/calendar-entry-list';

export default class DaySelectorComponent extends Component {
  get value() {
    if (this.args.allowClear && !this.args.value) {
      return null;
    }

    return toDateTime(this.args.value) ?? DateTime.now();
  }

  get defaultViewDate() {
    if (!this.value) {
      return DateTime.now().toFormat('yyyy-MM-dd');
    }

    return this.value.toFormat('yyyy-MM-dd');
  }

  @action
  change() {
    const value = toDateTime(this.datepicker.getDate());

    return this.args.onChange?.(value);
  }

  @action
  setValue() {
    if (!this.value) {
      return this.datepicker.setDate({ clear: true });
    }

    this.datepicker.setDate(this.defaultViewDate);
  }

  @action
  setup(element) {
    this.dateElement = element;

    this.datepicker = new Datepicker(element, {
      buttonClass: 'btn',
      format: 'yyyy-mm-dd',
      defaultViewDate: this.defaultViewDate,
      clearButton: this.args.allowClear,
    });

    this.setValue();
  }
}
