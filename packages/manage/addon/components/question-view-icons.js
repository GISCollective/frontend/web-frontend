import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class QuestionViewIconsComponent extends Component {
  @tracked icons = [];
  @service store;

  @action
  async setup() {
    if (!Array.isArray(this.args.value?.options)) {
      return;
    }

    let result = [];

    for (const id of this.args.value.options) {
      let icon = this.store.peekRecord('icon', id);

      if (!icon) {
        icon = await this.store.findRecord('icon', id);
      }

      if (icon) {
        result.push(icon);
      }
    }

    this.icons = result;
  }
}
