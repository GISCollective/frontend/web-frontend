/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, waitFor, waitUntil } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/image-list", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let picture1;
  let picture2;
  let pageData;
  let pictureRequest;

  hooks.before(function () {
    server = new TestServer();

    picture1 = server.testData.create.picture("1");
    picture2 = server.testData.create.picture("2");

    server.testData.storage.addPicture(picture1);
    server.testData.storage.addPicture(picture2);

    pageData = server.testData.storage.addDefaultPage();

    pictureRequest = null;
    server.post("/mock-server/pictures", (request) => {
      pictureRequest = JSON.parse(request.requestBody);
      pictureRequest.picture["_id"] = "5cc8dc1038e882010061545a";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(pictureRequest)];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders a list of pictures", async function () {
    this.set("value", {
      model: "picture",
      ids: ["1", "2"],
    });

    await render(hbs`<Editor::ImageList @value={{this.value}} />`);

    await waitFor(".image-list-input");

    expect(this.element.querySelector("img")).to.have.attribute(
      "src",
      "/test/5d5aa72acac72c010043fb59.jpg.sm.jpg?rnd=",
    );
  });

  it("can add a new picture", async function (a) {
    this.set("value", {
      model: "picture",
      ids: ["1", "2"],
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    let page = await this.store.findRecord("page", pageData._id);
    this.set("model", { page });

    await render(hbs`<Editor::ImageList @model={{this.model}} @value={{this.value}} @onChange={{this.change}} />`);

    await waitFor(".image-list-input");

    const blob = server.testData.create.pngBlob();
    await triggerEvent(".image-list-input[type='file']", "change", {
      files: [blob],
    });

    await waitUntil(() => value);

    expect(value.ids).to.deep.equal(["1", "2", "5cc8dc1038e882010061545a"]);
    expect(pictureRequest.picture.meta).to.deep.equal({
      renderMode: "",
      attributions: "",
      link: { model: "Page", id: "000000000000000000000001" },
      data: {},
      disableOptimization: false,
    });
  });

  it("does not show the style options", async function () {
    this.set("value", [picture1, picture2]);

    await render(hbs`<Editor::ImageList @onChange={{this.change}} />`);

    await waitFor(".image-list-input");

    expect(this.element.querySelector(".sm-top-padding")).not.to.exist;
  });
});
