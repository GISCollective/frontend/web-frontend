/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class ManageRevisionListComponent extends Component {
  @action
  select(revision) {
    return this.args.onSelect?.(revision?.id ?? null);
  }
}
