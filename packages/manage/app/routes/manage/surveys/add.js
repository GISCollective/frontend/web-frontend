/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';

export default class AddFeatureRoute extends AuthenticatedRoute {
  modelsToClear = ['campaign'];

  queryParams = {
    all: { refreshModel: true },
  };

  model(params) {
    if (this.isFastBoot) {
      return;
    }

    const teamParams = { edit: true };

    if (params.all) {
      teamParams.all = params.all;
    }

    return hash({
      campaign: this.store.createRecord('campaign', {
        name: '',
        description: '',
        info: new ModelInfo(),
        visibility: {
          isPublic: false,
          team: null,
        },
      }),
      teams: this.store.query('team', teamParams),
    });
  }
}
