/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class InputComponentSelectorComponent extends Component {
  @tracked areComponentsVisible = false;

  @action
  toggleComponentsList() {
    this.areComponentsVisible = !this.areComponentsVisible;
  }

  @action
  change(value) {
    this.areComponentsVisible = false;
    this.args.onChange?.(value);
  }
}
