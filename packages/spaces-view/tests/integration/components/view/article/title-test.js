/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/article/title', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Article::Title />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the data with the selected style when set', async function () {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        style: { color: 'green', classes: ['fw-bold', 'display-2'] },
      },
      modelKey: 'article',
    });

    this.set('model', {
      article: { name: 'some title' },
    });

    await render(
      hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'some title',
    );
    expect(this.element.querySelector('h1')).to.have.classes(
      'text-with-options fw-bold display-2 text-green',
    );
  });

  it('uses the "data.style" path when the path is not inside the data object', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        style: { color: 'green', classes: ['fw-bold', 'display-2'] },
      },
      modelKey: 'article',
    });

    this.set('model', {
      article: { name: 'some title' },
    });

    await render(
      hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} @path="col.0"/>`,
    );

    expect(this.element.querySelector('h1')).to.have.attribute(
      'data-path',
      'col.0.data.style',
    );
  });

  it('uses the provided path when it is inside the data object', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        title: { color: 'green', classes: ['fw-bold', 'display-2'] },
      },
      modelKey: 'article',
    });

    this.set('model', {
      article: { name: 'some title' },
    });

    await render(
      hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} @path="col.0.data.title"/>`,
    );

    expect(this.element.querySelector('h1')).to.have.classes(
      'text-with-options fw-bold display-2 text-green',
    );

    expect(this.element.querySelector('h1')).to.have.attribute(
      'data-path',
      'col.0.data.title',
    );
  });

  it('does not add the source path when disableSource is true', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        title: { color: 'green', classes: ['fw-bold', 'display-2'] },
      },
      modelKey: 'article',
    });

    this.set('model', {
      article: { name: 'some title' },
    });

    await render(
      hbs`<View::Article::Title @disableSource={{true}} @model={{this.model}} @value={{this.value}} @path="col.0.data.title"/>`,
    );

    expect(this.element.querySelector('.source-record')).to.have.attribute(
      'data-path',
      null,
    );
  });

  describe('when the selected model is a feature', async function (hooks) {
    let featureData;
    let receivedFeature;

    hooks.beforeEach(async function () {
      store = this.owner.lookup('service:store');

      featureData = server.testData.storage.addDefaultFeature();
      featureData.canEdit = true;

      server.testData.storage.addDefaultPreferences();

      this.set('value', {
        data: {},
        modelKey: 'feature',
      });

      receivedFeature = null;

      server.put(`/mock-server/features/:id`, (request) => {
        receivedFeature = JSON.parse(request.requestBody);
        receivedFeature.feature._id = request.params.id;

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedFeature),
        ];
      });
    });

    it('renders the edit button when it is editable', async function () {
      const feature = await store.findRecord('feature', featureData._id);
      this.set('model', {
        feature,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.dropdown-item-edit')).to.exist;
    });

    it('renders the make private button when it is editable and public', async function () {
      featureData.visibility = 1;

      const feature = await store.findRecord('feature', featureData._id);
      this.set('model', {
        feature,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.btn-manage-unpublish')).to.exist;
      expect(this.element.querySelector('.btn-manage-publish')).not.to.exist;

      await click('.btn-manage-unpublish');
      await waitUntil(() => receivedFeature);

      expect(receivedFeature.feature.visibility).to.equal(0);
    });

    it('renders the make public button when it is editable and private', async function () {
      featureData.visibility = 0;

      const feature = await store.findRecord('feature', featureData._id);
      this.set('model', {
        feature,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.btn-manage-unpublish')).not.to.exist;
      expect(this.element.querySelector('.btn-manage-publish')).to.exist;

      await click('.btn-manage-publish');
      await waitUntil(() => receivedFeature);

      expect(receivedFeature.feature.visibility).to.equal(1);
    });

    it('renders the make public button when it is editable and pending', async function () {
      featureData.visibility = -1;

      const feature = await store.findRecord('feature', featureData._id);
      this.set('model', {
        feature,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.btn-manage-unpublish')).not.to.exist;
      expect(this.element.querySelector('.btn-manage-publish')).to.exist;

      await click('.btn-manage-publish');
      await waitUntil(() => receivedFeature);

      expect(receivedFeature.feature.visibility).to.equal(1);
    });

    it('does not render the edit button when it is not editable', async function () {
      featureData.canEdit = false;
      const feature = await store.findRecord('feature', featureData._id);
      this.set('model', {
        feature,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.dropdown-item-edit')).not.to.exist;
    });
  });

  describe('when the selected model is a campaign', async function (hooks) {
    let campaignData;
    let receivedCampaign;

    hooks.beforeEach(async function () {
      store = this.owner.lookup('service:store');

      campaignData = server.testData.storage.addDefaultCampaign();
      campaignData.canEdit = true;

      server.testData.storage.addDefaultPreferences();

      this.set('value', {
        data: {},
        modelKey: 'campaign',
      });

      receivedCampaign = null;

      server.put(`/mock-server/campaigns/:id`, (request) => {
        receivedCampaign = JSON.parse(request.requestBody);
        receivedCampaign.campaign._id = request.params.id;

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedCampaign),
        ];
      });
    });

    it('renders the edit button when it is editable', async function () {
      const campaign = await store.findRecord('campaign', campaignData._id);
      this.set('model', {
        campaign,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.dropdown-item-edit')).to.exist;
    });

    it('renders the make private button when it is editable and public', async function () {
      campaignData.visibility = {
        isPublic: true,
      };

      const campaign = await store.findRecord('campaign', campaignData._id);
      this.set('model', {
        campaign,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.btn-manage-unpublish')).to.exist;
      expect(this.element.querySelector('.btn-manage-publish')).not.to.exist;

      await click('.btn-manage-unpublish');
      await waitUntil(() => receivedCampaign);

      expect(receivedCampaign.campaign.visibility.isPublic).to.equal(false);
    });

    it('renders the make public button when it is editable and private', async function () {
      campaignData.visibility = {
        isPublic: false,
      };

      const campaign = await store.findRecord('campaign', campaignData._id);
      this.set('model', {
        campaign,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.btn-manage-unpublish')).not.to.exist;
      expect(this.element.querySelector('.btn-manage-publish')).to.exist;

      await click('.btn-manage-publish');
      await waitUntil(() => receivedCampaign);

      expect(receivedCampaign.campaign.visibility.isPublic).to.equal(true);
    });

    it('does not render the edit button when it is not editable', async function () {
      campaignData.canEdit = false;
      const campaign = await store.findRecord('campaign', campaignData._id);
      this.set('model', {
        campaign,
      });

      await render(
        hbs`<View::Article::Title @model={{this.model}} @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.dropdown-item-edit')).not.to.exist;
    });
  });
});
