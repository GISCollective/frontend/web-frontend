import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class ViewFragmentComponent extends Component {
  @tracked page;
  ignoredTypes = ['fragment'];

  @action
  setup(value) {
    this.page = value;
  }

  @action
  ensureColExists(name, page) {
    later(() => {
      page.upsertPageColByName(name);
    });
  }
}
