/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { transform } from 'ol/proj';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { later } from '@ember/runloop';

export default class MapGeojsonFeatureComponent extends Component {
  @service position;

  get pointLocation() {
    if (this.args.value?.type != 'Point') {
      return false;
    }

    return transform(this.args.value.coordinates, 'EPSG:4326', 'EPSG:3857');
  }

  @action
  setupPoint(map) {
    later(() => {
      if (!this.pointLocation) {
        return;
      }

      map.getView().setCenter(this.pointLocation);
      map.getView().setZoom(18);
    });
  }
}
