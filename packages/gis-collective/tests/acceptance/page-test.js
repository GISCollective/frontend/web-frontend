/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click,  waitFor, waitUntil, settled } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import { PageModel } from 'ogm/lib/page-model';

describe ('Acceptance | page-view', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let featureData;
  let featureOtherData;
  let page;
  let spaceData;
  let receivedMetrics;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    spaceData = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultGeocoding();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultPage("000000000000000000000001");
    server.testData.storage.addDefaultTeam("000000000000000000000001");
    featureData = server.testData.storage.addDefaultFeature();
    featureOtherData = server.testData.storage.addDefaultFeature('other');
    featureOtherData.name = 'Other';

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.get(`/mock-server/layouts/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);

    server.post(`/mock-server/metrics`, (request) => {
      const receivedMetric = JSON.parse(request.requestBody);
      receivedMetric.metric._id = '1';

      receivedMetrics.push(receivedMetric.metric);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedMetric),
      ];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    receivedMetrics = [];
    PageModel.clearCache();
  });

  it('shows an error when the page is not found', async function () {
    await visit('/not-found');
    expect(currentURL()).to.equal('/not-found');
    expect(this.element.querySelector('.error')).to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal('Page Not Found');
  });

  describe ('when there is a page that matches a generic slug', function (hooks) {
    let page;

    hooks.beforeEach(function () {
      page = server.testData.create.page('section--page');
      page.description = 'page description';
      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      server.testData.create.pagesMap['section--page'] = 'section--page';
    });

    it('sets the current model name, description and cover to the headData', async function () {
      await visit('/section/page');

      const headData = this.owner.lookup('service:headData');

      expect(headData.title).to.equal('test');
      expect(headData.description).to.equal('page description');
    });

    it('triggers a page view metric', async function() {
      await visit('/section/page');

      await waitUntil(() => receivedMetrics.length);

      expect(receivedMetrics).to.have.length(1);
      expect(receivedMetrics[0]).to.deep.contain({
        name: 'section--page',
        type: 'pageView',
        value: 1,
        labels: {},
        _id: '1'
      });
    });
  });

  describe ('when there is a container with user:unauthenticated visibility', function (hooks) {
    let page;

    hooks.beforeEach(function () {
      page = server.testData.create.page('section--page');
      page.description = 'page description';

      page.layoutContainers[0].visibility = 'user:unauthenticated';

      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      server.testData.create.pagesMap['section--page'] = 'section--page';

      server.testData.storage.addDefaultUser();
    });

    it('renders the container when the user is not authenticated', async function () {
      await visit('/section/page');

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
    });

    it('does not render the container when the user is authenticated', async function () {
      authenticateSession();
      await visit('/section/page');

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).not.to.exist;
      expect(this.element.querySelector('.col')).not.to.exist;
    });
  });

  describe ('when there is a container with user:authenticated visibility', function (hooks) {
    let page;

    hooks.beforeEach(function () {
      page = server.testData.create.page('section--page');
      page.description = 'page description';

      page.layoutContainers[0].visibility = 'user:authenticated';

      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      server.testData.create.pagesMap['section--page'] = 'section--page';

      server.testData.storage.addDefaultUser();
    });

    it('renders the container when the user is authenticated', async function () {
      authenticateSession();
      await visit('/section/page');

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
    });

    it('does not render the container when the user is not authenticated', async function () {
      await visit('/section/page');

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).not.to.exist;
      expect(this.element.querySelector('.col')).not.to.exist;
    });
  });

  describe ('when there is a page that matches a variable slug', function (hooks) {
    let page;
    let feature;

    hooks.beforeEach(function () {
      page = server.testData.create.page('feature--:feature-id');
      page.slug = 'feature--:feature-id';
      page.cols[0].data.source = { useSelectedModel: true };
      page.cols[1].data.source = { useSelectedModel: true };

      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');

      server.testData.create.pagesMap['feature--:feature-id'] = 'feature--:feature-id';

      feature = server.testData.storage.addDefaultFeature();
    });

    it('sets the current model name, description and cover to the headData', async function () {
      await visit(`/feature/${feature._id}`);

      const headData = this.owner.lookup('service:headData');

      expect(headData.title).to.equal('Nomadisch Grün - Local Urban Food');
      expect(headData.description).to.equal('some description');
    });
  });

  describe.skip('the side panel', function (hooks) {
    let article;

    hooks.beforeEach(async function () {
      authenticateSession();
      server.testData.storage.addDefaultUser();

      const panelPage = server.testData.create.page('_panel--feature--:feature-id');
      server.testData.create.pagesMap['_panel--feature--:feature-id'] = '_panel--feature--:feature-id';
      panelPage.slug = '_panel--feature--:feature-id';
      panelPage.cols = [
        {
          name: 'article',
          data: {
            source: {
              useSelectedModel: true,
            },
          },
          type: 'article',
          container: 0,
        },
      ];
      panelPage.layoutContainers = [
        {
          layers: {
            showContentAfter: 1,
            effect: 'none',
            count: 1,
          },
          rows: [
            {
              options: [],
              cols: [
                {
                  type: '',
                  componentCount: 1,
                  options: [],
                  name: 'main picture',
                },
              ],
            },
          ],
          options: [],
        },
        {
          layers: {
            showContentAfter: 1,
            effect: 'none',
            count: 1,
          },
          rows: [
            {
              options: [],
              cols: [
                {
                  type: '',
                  componentCount: 1,
                  options: [],
                  name: 'article',
                },
              ],
            },
          ],
          options: [],
        },
      ];

      server.testData.storage.addPage(panelPage);

      page = server.testData.storage.addDefaultPage('000000000000000000000002');
      page.visibility.isPublic = false;
      page.canEdit = true;
      page.cols[1].type = 'map/map-view';

      server.testData.create.pagesMap['section--page'] = '000000000000000000000002';

      server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultIconSet();
      server.testData.storage.addDefaultBaseMap();
      server.testData.storage.addDefaultMap('6241b7a8d63fa5010056589b');
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      article = server.testData.storage.addDefaultArticle();
    });

    it('is shown when a map feature from an article is selected', async function () {
      article.content = {
        blocks: [
          {
            type: 'map/map-view',
            data: {
              sizing: 'fill',
              height: '500',
              source: {
                useSelectedModel: false,
                id: '6241b7a8d63fa5010056589b',
                model: 'map',
              },
              map: {
                featureHover: 'mouse pointer',
                restrictView: false,
                mode: 'use a map',
                featureSelection: 'open details in panel',
                enablePinning: false,
                showUserLocation: false,
                enableMapInteraction: 'automatic',
                showBaseMapSelection: false,
                flyOverAnimation: false,
                showZoomButtons: false,
              },
            },
          },
        ],
      };
      page.cols[1] = {};

      await visit(`/section/page`);

      const map = this.element.querySelector('.page-col-map-view');

      map.selectFeature({
        getProperties: () => ({
          _id: featureData._id,
        }),
      });

      await wait(100);

      await waitFor('.sidebar-content');
      await waitUntil(() => this.element.querySelector('.sidebar-content h1').textContent.trim() != '', {
        timeout: 4000,
      });

      expect(this.element.querySelector('.sidebar-content h1').textContent.trim()).to.equal(
        'Nomadisch Grün - Local Urban Food'
      );

      await PageElements.wait(100);
    });

    it('updates the panel data when a second feature is selected', async function () {
      article.content = {
        blocks: [
          {
            type: 'map/map-view',
            data: {
              sizing: 'fill',
              height: '500',
              source: {
                useSelectedModel: false,
                id: '6241b7a8d63fa5010056589b',
                model: 'map',
              },
              map: {
                featureHover: 'mouse pointer',
                restrictView: false,
                mode: 'use a map',
                featureSelection: 'open details in panel',
                enablePinning: false,
                showUserLocation: false,
                enableMapInteraction: 'automatic',
                showBaseMapSelection: false,
                flyOverAnimation: false,
                showZoomButtons: false,
              },
            },
          },
        ],
      };
      page.cols[1] = {};

      await visit(`/section/page`);

      const map = this.element.querySelector('.page-col-map-view');

      map.selectFeature({
        getProperties: () => ({
          _id: featureData._id,
        }),
      });

      await PageElements.wait(100);

      await waitFor('.sidebar-content', { timeout: 4000 });
      await waitUntil(
        () =>
          this.element.querySelector('.sidebar-content h1').textContent.trim() == 'Nomadisch Grün - Local Urban Food',
        { timeout: 4000 }
      );

      map.selectFeature({
        getProperties: () => ({
          _id: featureOtherData._id,
        }),
      });

      await settled();

      await waitUntil(
        () =>
          this.element.querySelector('.sidebar-content h1').textContent.trim() != 'Nomadisch Grün - Local Urban Food',
        { timeout: 4000 }
      );

      expect(this.element.querySelector('.sidebar-content h1').textContent.trim()).to.equal('Other');
    });
  });
});
