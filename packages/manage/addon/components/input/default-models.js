import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { defaultModelNames } from 'models/transforms/default-models';
import { action } from '@ember/object';
import { DefaultModels } from 'models/transforms/default-models';

class Pair {
  @tracked name = '';
  @tracked value = '';
  @tracked valueList = '';

  constructor(props) {
    this.name = props.name;
    this.value = props.value;
    this.valueList = props.valueList;
  }
}

export default class InputDefaultModelsComponent extends Component {
  @service store;

  @tracked _value;

  @tracked map = null;
  @tracked campaign = null;
  @tracked calendar = null;
  @tracked newsletter = null;

  @tracked mapList = [];
  @tracked campaignList = [];
  @tracked calendarList = [];
  @tracked newsletterList = [];

  get models() {
    return this.args.models || defaultModelNames;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  get team() {
    return this.args.team?.id;
  }

  get pairs() {
    const result = [];

    for (const name of this.models) {
      result.push(new Pair({ name, value: this[name], valueList: this[`${name}List`] }));
    }

    return result;
  }

  @action
  change(name, value) {
    if (!this._value) {
      this._value = new DefaultModels(this.value);
    }

    this._value[name] = value.id;

    return this.args.onChange(this.value);
  }

  @action
  clear(pair) {
    if (!this._value) {
      this._value = new DefaultModels(this.value);
    }

    this._value[pair.name] = '';
    pair.value = '';

    return this.args.onChange(this.value);
  }

  @action
  async setup() {
    const value = this.args.value;

    for (const name of this.models) {
      if (value?.[name]) {
        let resolvedValue = this.store.peekRecord(name, value[name]);

        if (!resolvedValue) {
          try {
            resolvedValue = await this.store.findRecord(name, value[name]);
          } catch (err) {
            console.error(err);
          }
        }

        this[name] = resolvedValue;
      }

      this[`${name}List`] = await this.store.query(name, { team: this.team });
    }
  }
}
