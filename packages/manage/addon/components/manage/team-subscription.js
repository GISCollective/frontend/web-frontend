/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { A } from 'core/lib/array';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { DateTime } from 'luxon';
import { toDateTime } from 'models/transforms/calendar-entry-list';

export default class ManageTeamSubscriptionComponent extends Component {
  @service intl;

  get expire() {
    return toDateTime(this.args.value.expire).toLocaleString(DateTime.DATE_FULL);
  }

  get usedSupportByMonth() {
    const value = {};

    for (const entry of this.args.value.support) {
      const key = toDateTime(entry.date).toFormat('yyyy-LL');

      if (!value[key]) {
        value[key] = 0;
      }

      value[key] += parseFloat(entry.hours);
    }

    return value;
  }

  get billableHoursByMonth() {
    const monthlySupportHours = parseFloat(this.args.value.monthlySupportHours);
    const value = this.usedSupportByMonth;

    for (const key in value) {
      value[key] -= monthlySupportHours;
    }

    return value;
  }

  get paidSupport() {
    return this.args.value.invoices.map((a) => parseFloat(a.hours)).reduce((a, b) => a + b, 0);
  }

  get usedSupport() {
    return Object.values(this.usedSupportByMonth).reduce((a, b) => a + b, 0);
  }

  get billableHours() {
    return Object.values(this.billableHoursByMonth).reduce((a, b) => a + b, 0);
  }

  get usedSupportHours() {
    const now = DateTime.now().toFormat('yyyy-LL');

    return this.usedSupportByMonth[now] ?? 0;
  }

  get billableSupport() {
    let value = 0;
    const monthlyHours = this.usedSupportByMonth;
    const monthlySupportHours = this.args.value.monthlySupportHours;

    for (const hours of Object.values(monthlyHours)) {
      value += Math.max(hours - monthlySupportHours, 0);
    }

    value -= this.paidSupport;

    if (value <= 0) {
      return '';
    }

    return this.intl.t('subscription.hours', { value });
  }

  get remainingSupport() {
    let value = this.paidSupport - this.billableHours;

    if (value > 0) {
      return this.intl.t('subscription.hours', { value });
    }

    const monthlySupportHours = parseFloat(this.args.value.monthlySupportHours);
    value = Math.max(monthlySupportHours - this.usedSupportHours, 0);

    return this.intl.t('subscription.hours', { value });
  }
}
