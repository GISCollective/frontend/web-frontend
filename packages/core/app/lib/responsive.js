/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export {
  getSizeFromWidth,
  getSizeBsFromWidth,
  getSizeFromIndex,
  getSizeIndexFromWidth,
  getSizeBsFromName,
  getNameFromBs,
  getSizeIndexFromName,
  getSizeIndexFromBs,
  getBsFromSizeIndex,
} from 'core/lib/responsive';
