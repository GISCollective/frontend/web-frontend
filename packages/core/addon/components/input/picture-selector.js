import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import PictureMeta from 'models/lib/picture-meta';
import { buildWaiter } from '@ember/test-waiters';
import { later } from '@ember/runloop';

let waiter = buildWaiter('picture-selector-waiter');

export default class InputPictureSelector extends Component {
  @service store;
  @tracked picture;

  get src() {
    if (!this.picture) {
      return;
    }

    let pieces = [this.picture.picture];

    if (this.args.size && !this.picture.isNew) {
      pieces.push(this.args.size);
    }

    return pieces.join('/');
  }

  async cameraSuccess(result, mime) {
    const value = this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta({
        link: {
          model: this.args.model,
          id: this.args.modelId,
        },
      }),
    });

    value.set('picture', result);

    const maxSize = 1024 * 4;

    try {
      await value.localResize(maxSize, maxSize, mime);
    } catch (err) {
      this.toaster.handleError(err);
    }

    this.picture = value;

    await this.args.onChange?.(this.picture);

    later(() => {
      waiter.endAsync(this.token);
    });
  }

  @action
  async setup() {
    if (!this.args.id) {
      this.picture = null;
      return;
    }

    this.picture = await this.store.peekRecord('picture', this.args.id);

    if (!this.picture) {
      this.picture = await this.store.findRecord('picture', this.args.id);
    }
  }

  @action
  selectPicture() {
    this.inputElement.click();
  }

  @action
  async removePicture() {
    this.picture = null;
    await this.args.onChange?.(this.picture);
  }

  @action
  inputInserted(element) {
    this.inputElement = element;
  }

  @action
  fileSelected(event) {
    this.token = waiter.beginAsync();

    const file = event.target.files[0];

    if (!file) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = (event) => {
      this.cameraSuccess(event.target.result, file.type);
    };

    reader.onerror = console.error;
  }
}
