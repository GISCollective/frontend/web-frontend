/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | manage/cover', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let receivedPictures = [];

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    receivedPictures = [];
    server.post(
      '/mock-server/pictures',
      (request) => {
        const pictureRequest = JSON.parse(request.requestBody);
        pictureRequest.picture['_id'] = receivedPictures.length + 1;
        receivedPictures.push(pictureRequest);

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(pictureRequest)];
      },
      false,
    );
  });

  describe('in view mode', () => {
    it('renders an alert when there is no value', async function (a) {
      await render(hbs`<Manage::Cover @title="cover" />`);

      expect(this.element.querySelector('.alert.alert-danger')).to.have.textContent('There is no picture.');
    });

    it('renders the cover when set', async function (a) {
      this.set('value', {
        _id: '000000000000000000000001',
        owner: '1234',
        picture: 'http://localhost:9091/pictures/000000000000000000000001/picture',
        name: '',

        get(key) {
          return this[key];
        },
      });

      await render(hbs`<Manage::Cover @title="cover" @value={{this.value}} />`);

      expect(this.element.querySelector('.alert.alert-danger')).not.to.exist;
      expect(this.element.querySelector('.manage-cover-view').attributes.getNamedItem('style').value).to.contain(
        'http://localhost:9091/pictures/000000000000000000000001/picture/lg?rnd=',
      );
    });
  });

  describe('in edit mode', () => {
    it('renders an input with no value', async function (a) {
      await render(hbs`<Manage::Cover @title="cover" @editablePanel="cover" />`);

      expect(this.element.querySelector('.alert.alert-danger')).not.to.exist;
      expect(this.element.querySelector('.image')).to.have.attribute('style', '');
    });

    it('renders an input with a value', async function (a) {
      this.set('value', {
        _id: '000000000000000000000001',
        owner: '1234',
        picture: 'http://localhost:9091/pictures/000000000000000000000001/picture',
        name: '',

        get(key) {
          return this[key];
        },
      });

      await render(hbs`<Manage::Cover @title="cover" @editablePanel="cover" @value={{this.value}} />`);

      expect(this.element.querySelector('.alert.alert-danger')).not.to.exist;
      expect(this.element.querySelector('.image')).to.have.attribute(
        'style',
        "background-image: url('http://localhost:9091/pictures/000000000000000000000001/picture/md?rnd=undefined')",
      );
    });

    it('can change a value', async function (a) {
      let value;

      this.set('handleSave', (v) => {
        value = v;
        this.set('editablePanel', '');
      });

      await render(
        hbs`<Manage::Cover @title="cover" @editablePanel="cover" @value={{this.value}} @onSave={{this.handleSave}} />`,
      );

      const blob = server.testData.create.pngBlob();
      await triggerEvent("[type='file']", 'change', {
        files: [blob],
      });

      await click('.btn-submit');

      expect(value.id).to.deep.equal('1');
    });
  });
});
