import { setupRenderingTest, it, describe, wait } from 'dummy/tests/helpers';
import {
  click,
  fillIn,
  render,
  triggerEvent,
  waitUntil,
  waitFor,
} from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | page-view', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let featureData;
  let featureOtherData;
  let pageData;
  let spaceData;
  let router;
  let lastTransition;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    spaceData = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultGeocoding();
    server.testData.storage.addDefaultIcon();
    pageData = server.testData.storage.addDefaultPage(
      '000000000000000000000001',
    );
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    featureData = server.testData.storage.addDefaultFeature();
    featureOtherData = server.testData.storage.addDefaultFeature('other');
    featureOtherData.name = 'Other';

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.get(`/mock-server/layouts/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    this.router.transitionTo = (v) => {
      lastTransition = v;
    };
    lastTransition = null;
  });

  it('renders the page not found page when there is no model', async function (assert) {
    await render(hbs`<PageView />`);

    expect(this.element.querySelector('.page-not-found')).to.exist;
  });

  it('redirects to the page when the space has a redirect to that page', async function () {
    spaceData.redirects = {
      '/a': {
        path: '/section/page',
      },
    };

    const space = await this.store.findRecord('space', spaceData._id);
    const redirect = '/section/page';

    this.set('model', {
      redirect,
      space,
    });

    await render(hbs`<PageView @model={{this.model}} />`);

    expect(this.element.querySelector('.message')).to.have.textContent(
      'This page has been moved to: /section/page',
    );

    await waitUntil(() => lastTransition, { timeout: 3000 });

    expect(lastTransition).to.equal('/section/page');
    await wait(1000);
  });

  describe('when there is a page', function (hooks) {
    let page;

    hooks.beforeEach(async function () {
      pageData.description = 'page description';
      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      server.testData.create.pagesMap['section--page'] = 'section--page';

      const space = await this.store.findRecord('space', spaceData._id);
      page = await this.store.findRecord('page', pageData._id);

      this.set('model', {
        main: { page },
        space,
      });
    });

    it('can be rendered', async function () {
      await render(hbs`<PageView @model={{this.model}} />`);

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'title',
      );
    });

    it('renders a 100vh container in a flex container', async function () {
      page.layoutContainers[0].options = [
        'pt-5',
        'pb-5',
        'justify-content-center',
        'min-vh-100',
      ];

      await render(
        hbs`<div class="d-flex" style="display: flex"><PageView @model={{this.model}} /></div>`,
      );

      const container = this.element.querySelector('.container');
      const frameRect = container.getBoundingClientRect().toJSON();

      expect(frameRect.height).to.be.closeTo(window.innerHeight / 2, 2);
    });

    it('can be rendered when there is no column', async function () {
      page.cols = [];

      await render(hbs`<PageView @model={{this.model}} />`);

      expect(
        this.element.querySelector('.page-col.empty').textContent.trim(),
      ).to.equal('');
    });

    it('can render the background color component', async function () {
      page.cols.push({
        container: 0,
        col: -1,
        row: -1,
        name: '0',
        type: 'background/color',
        data: {
          background: {
            color: 'blue',
          },
        },
      });

      await render(hbs`<PageView @model={{this.model}} />`);
      expect(this.element.querySelector('.container-bg-color')).to.have.class(
        'bg-blue',
      );
    });

    it('applies the state when is changed using a filter', async function () {
      let state;

      this.set('changeState', (s) => {
        state = s;
      });

      page.cols[0] = {
        name: '0.0.0',
        gid: '',
        data: {
          filters: [{ type: 'icons' }, { type: 'location' }],
        },
        type: 'filters',
      };

      await render(
        hbs`<PageView @model={{this.model}} @onChangeState={{this.changeState}} />`,
      );

      await click('.btn-pill-location');

      await fillIn('.input-filter', 'some place');
      await triggerEvent('.input-filter', 'change');

      await click('.search-suggestions .list-group-item');

      expect(state).to.equal(
        'l@Berlin, Coos County, New Hampshire, 03570, United States',
      );
    });
  });

  describe('when there is a container with user:unauthenticated visibility', function (hooks) {
    let page;

    hooks.beforeEach(async function () {
      pageData.layoutContainers[0].visibility = 'user:unauthenticated';

      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      server.testData.create.pagesMap['section--page'] = 'section--page';

      server.testData.storage.addDefaultUser();

      const space = await this.store.findRecord('space', spaceData._id);
      page = await this.store.findRecord('page', pageData._id);

      this.set('model', {
        main: { page },
        space,
      });
    });

    it('renders the container when the user is not authenticated', async function (a) {
      this.set('conditions', ['user:unauthenticated']);

      await render(
        hbs`<PageView @model={{this.model}} @conditions={{this.conditions}} />`,
      );

      a.notOk(this.element.querySelector('.error'));
      a.ok(this.element.querySelector('.row'));
      a.ok(this.element.querySelector('.col'));
      a.equal(this.element.querySelector('h1').textContent.trim(), 'title');
    });

    it('does not render the container when the user is authenticated', async function () {
      this.set('conditions', ['user:authenticated']);

      await render(
        hbs`<PageView @model={{this.model}} @conditions={{this.conditions}}/>`,
      );

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).not.to.exist;
      expect(this.element.querySelector('.col')).not.to.exist;
    });
  });

  describe('when there is a container with user:authenticated visibility', function (hooks) {
    let page;

    hooks.beforeEach(async function () {
      pageData.layoutContainers[0].visibility = 'user:authenticated';

      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      server.testData.create.pagesMap['section--page'] = 'section--page';

      server.testData.storage.addDefaultUser();

      const space = await this.store.findRecord('space', spaceData._id);
      page = await this.store.findRecord('page', pageData._id);

      this.set('model', {
        main: { page },
        space,
      });
    });

    it('renders the container when the user is authenticated', async function () {
      this.set('conditions', ['user:authenticated']);

      await render(
        hbs`<PageView @model={{this.model}} @conditions={{this.conditions}} />`,
      );

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'title',
      );
    });

    it('does not render the container when the user is not authenticated', async function () {
      this.set('conditions', ['user:unauthenticated']);

      await render(
        hbs`<PageView @model={{this.model}} @conditions={{this.conditions}} />`,
      );

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).not.to.exist;
      expect(this.element.querySelector('.col')).not.to.exist;
    });
  });

  describe('the side panel', function (hooks) {
    let article;
    let page;
    let panelPage;
    let selectedModel;

    hooks.beforeEach(async function () {
      const panelPageData = server.testData.create.page(
        '_panel--feature--:feature-id',
      );
      server.testData.create.pagesMap['_panel--feature--:feature-id'] =
        '_panel--feature--:feature-id';
      panelPageData.slug = '_panel--feature--:feature-id';
      panelPageData.cols = [
        {
          name: 'article',
          data: {
            source: {
              useSelectedModel: true,
            },
          },
          type: 'article',
          container: 0,
        },
      ];
      panelPageData.layoutContainers = [
        {
          layers: {
            showContentAfter: 1,
            effect: 'none',
            count: 1,
          },
          rows: [
            {
              options: [],
              cols: [
                {
                  type: '',
                  componentCount: 1,
                  options: [],
                  name: 'main picture',
                },
              ],
            },
          ],
          options: [],
        },
        {
          layers: {
            showContentAfter: 1,
            effect: 'none',
            count: 1,
          },
          rows: [
            {
              options: [],
              cols: [
                {
                  type: '',
                  componentCount: 1,
                  options: [],
                  name: 'article',
                },
              ],
            },
          ],
          options: [],
        },
      ];

      server.testData.storage.addPage(panelPageData);

      pageData.visibility.isPublic = false;
      pageData.canEdit = true;
      pageData.cols[1].type = 'map/map-view';

      server.testData.create.pagesMap['section--page'] =
        '000000000000000000000002';

      server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultIconSet();
      server.testData.storage.addDefaultBaseMap();
      server.testData.storage.addDefaultMap('6241b7a8d63fa5010056589b');
      server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
      article = server.testData.storage.addDefaultArticle();

      selectedModel = await this.store.findRecord('feature', featureData._id);

      const space = await this.store.findRecord('space', spaceData._id);
      page = await this.store.findRecord('page', pageData._id);
      panelPage = await this.store.findRecord('page', panelPageData._id);

      const data = {
        selectedModel,
      };

      this.set('model', {
        main: { page, data },
        panel: { page: panelPage, data },
        space,
      });
    });

    it('is shown when the state has a fid value and the page has a map', async function (a) {
      this.set('state', `fid@${featureData._id}`);
      await render(
        hbs`<PageView @model={{this.model}} @state={{this.state}} />`,
      );
      await wait(1000);

      a.ok(this.element.querySelector('.sidebar-model'));

      expect(
        this.element
          .querySelector('.sidebar-model .article h1')
          .textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
      expect(
        this.element
          .querySelector('.sidebar-model .article p')
          .textContent.trim(),
      ).to.equal('some description');
    });

    it('can close the panel', async function () {
      this.set('state', `fid@${featureData._id}`);
      let state;

      this.set('changeState', (s) => {
        state = s;
      });

      await render(
        hbs`<PageView @model={{this.model}} @state={{this.state}} @onChangeState={{this.changeState}} />`,
      );
      await wait(1000);

      await click('.btn-close-panel');

      await waitUntil(() => typeof state == 'string');

      expect(state).to.equal('');
    });
  });
});
