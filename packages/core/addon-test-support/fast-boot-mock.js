/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';

export default class FastBootMock extends Service {
  constructor() {
    super(...arguments);
    this.request.host = `${window.location.hostname}:${window.location.port}`;
  }

  isFastBoot = true;
  request = {
    host: '',
    path: '/',
    protocol: 'http:',
    headers: {
      set: (key, value) => {
        this.request.headers[key] = [value];
      },
    },
  };

  response = {
    headers: {
      set: (key, value) => {
        this.response.headers[key] = [value];
      },
    },
  };

  deferRendering() {}
}
