/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe, wait } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | map/marker/feature', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let feature;
  let picture;

  hooks.before(async function () {
    server = new TestServer();
    feature = server.testData.storage.addDefaultFeature();
    picture = server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultIcon();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is not feature id', async function (assert) {
    await render(hbs`<Map::Marker::Feature />`);

    assert.dom(this.element).hasText('');
  });

  describe('for a feature without pictures', function (hooks) {
    hooks.beforeEach(function () {
      feature.pictures = [];
    });

    it('renders the feature title and description when the id is set', async function (a) {
      this.set('feature', feature);
      await render(
        hbs`<Map::Marker::Feature @featureId={{this.feature._id}} />`,
      );

      expect(
        this.element.querySelector('.feature-title').textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
      expect(
        this.element.querySelector('.feature-description').textContent.trim(),
      ).to.equal('some description');
      expect(this.element.querySelector('.picture-with-options')).not.to.exist;
    });

    it('renders the feature icons', async function (a) {
      this.set('feature', feature);

      await render(
        hbs`<Map::Marker::Feature @featureId={{this.feature._id}} />`,
      );

      expect(this.element.querySelector('.icon')).to.have.attribute(
        'src',
        '/test/5ca7bfbfecd8490100cab97d.jpg',
      );
    });

    it('renders a close button', async function (a) {
      this.set('feature', feature);
      let closed;

      this.set('close', () => {
        closed = true;
      });

      await render(
        hbs`<Map::Marker::Feature @onClose={{this.close}} @featureId={{this.feature._id}} />`,
      );

      await click('.btn-close-marker');

      expect(closed).to.equal(true);
    });

    it('renders an open button when the space has a feature page', async function (a) {
      this.set('feature', feature);
      let closed;

      this.set('close', () => {
        closed = true;
      });

      const spaceService = this.owner.lookup('service:space');
      spaceService.getLinkFor = () => '/browse/feature-id';

      await render(
        hbs`<Map::Marker::Feature @onClose={{this.close}} @featureId={{this.feature._id}} />`,
      );

      await click('.btn-close-marker');

      expect(closed).to.equal(true);
    });
  });

  describe('for a feature with a picture', function (hooks) {
    hooks.beforeEach(function () {
      feature.pictures = [picture._id];
    });

    it('renders the feature cover, title and description when the id is set', async function (a) {
      this.set('feature', feature);
      await render(
        hbs`<Map::Marker::Feature @featureId={{this.feature._id}} />`,
      );

      expect(
        this.element.querySelector('.feature-title').textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');
      expect(
        this.element.querySelector('.feature-description').textContent.trim(),
      ).to.equal('some description');
      expect(
        this.element.querySelector('.picture-with-options .picture-bg'),
      ).to.have.attribute(
        'style',
        'background-image: url(/test/5d5aa72acac72c010043fb59.jpg.md.jpg);aspect-ratio: 4/3;',
      );
    });
  });

  describe('for a feature with 3 pictures', function (hooks) {
    let otherFeature;

    hooks.beforeEach(function () {
      const otherPicture = server.testData.storage.addDefaultPicture('other');
      const other2Picture = server.testData.storage.addDefaultPicture('other2');

      feature.pictures = [picture._id, otherPicture._id, other2Picture._id];

      otherFeature = server.testData.storage.addDefaultFeature('other');
      otherFeature.pictures = [picture._id, otherPicture._id];
    });

    it('renders the feature slider, title and description when the id is set', async function (a) {
      this.set('feature', feature);
      await render(
        hbs`<Map::Marker::Feature @featureId={{this.feature._id}} />`,
      );

      const pictures = this.element.querySelectorAll(
        '.picture-with-options .picture-bg',
      );

      expect(pictures).to.have.length(3);
    });

    it('resets the feature slider when the featureId is updated', async function (a) {
      this.set('featureId', feature._id);
      await render(hbs`<Map::Marker::Feature @featureId={{this.featureId}} />`);

      await click('.swiper-button-next');
      await click('.swiper-button-next');

      expect(
        this.element.querySelector('.swiper-slide-active'),
      ).to.have.attribute('aria-label', '3 / 3');
      await wait(100);

      this.set('featureId', otherFeature._id);

      await wait(100);
      expect(
        this.element.querySelector('.swiper-slide-active'),
      ).to.have.attribute('aria-label', '1 / 2');
    });
  });
});
