/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe ('Integration | Component | input/data-binding/destination',  function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let team;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultMap('1');
    server.testData.storage.addDefaultMap('2');
    team = server.testData.storage.addDefaultTeam();

    store = this.owner.lookup('service:store');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders the value', async function () {
    this.set('team', await store.findRecord('team', team._id));
    this.set('value', {
      type: 'Map',
      modelId: '1',
      deleteNonSyncedRecords: true,
    });

    this.team = await render(
      hbs`<Input::DataBinding::Destination @team={{this.team}} @value={{this.value}}/>`
    );

    expect(this.element.querySelector('.btn-switch').checked).to.equal(true);
    expect(this.element.querySelector('.select-map').value).to.equal('1');
  });

  it('no options when the team is not set', async function () {
    this.set('value', await store.findRecord('map', '1'));

    this.team = await render(
      hbs`<Input::DataBinding::Destination @team={{this.team}} @value={{this.value}}/>`
    );

    expect(this.element.querySelectorAll('options')).to.have.length(0);
  });

  it('triggers onChange when a new map is selected', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('team', await store.findRecord('team', team._id));
    this.set('value', await store.findRecord('map', '1'));

    await render(
      hbs`<Input::DataBinding::Destination @team={{this.team}} @value={{this.value}} @onChange={{this.change}}/>`
    );

    this.element.querySelector('.select-map').value = '2';
    await triggerEvent('.select-map', 'change');

    expect(value).to.deep.equal({
      type: 'Map',
      modelId: '2',
      deleteNonSyncedRecords: false,
    });
  });

  it('triggers onChange when the checkbox is clicked', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('team', await store.findRecord('team', team._id));
    this.set('value', await store.findRecord('map', '1'));

    await render(
      hbs`<Input::DataBinding::Destination @team={{this.team}} @value={{this.value}} @onChange={{this.change}}/>`
    );

    await click('.btn-switch');

    expect(value.toJSON()).to.deep.equal({
      type: 'Map',
      modelId: '1',
      deleteNonSyncedRecords: true,
    });
  });

  it('can set deleteNonSyncedRecords to false', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('team', await store.findRecord('team', team._id));
    this.set('value', {
      type: 'Map',
      modelId: '1',
      deleteNonSyncedRecords: true,
    });

    await render(
      hbs`<Input::DataBinding::Destination @team={{this.team}} @value={{this.value}} @onChange={{this.change}}/>`
    );

    await click('.btn-switch');

    expect(value.toJSON()).to.deep.equal({
      type: 'Map',
      modelId: '1',
      deleteNonSyncedRecords: false,
    });
  });
});
