/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class TimeIntervalTransform extends Transform {
  deserialize(serialized) {
    return new TimeInterval(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() ?? deserialized;
  }
}

export class TimeInterval {
  @tracked begin;
  @tracked end;

  constructor(obj) {
    if (obj?.begin) {
      this.begin = new Date(obj.begin);
    } else {
      this.begin = new Date('0001-01-01T00:00:00');
    }

    if (obj?.end) {
      this.end = new Date(obj.end);
    } else {
      this.end = new Date('0001-01-01T00:00:00');
    }
  }

  toJSON() {
    return {
      begin: this.begin.toISOString(),
      end: this.end.toISOString(),
    };
  }
}
