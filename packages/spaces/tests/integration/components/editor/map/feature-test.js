/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, click } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/map/feature", function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultFeature("1");
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the selected source value", async function () {
    this.set("value", {
      data: {
        source: { model: "article", id: "1" },
      },
    });

    await render(hbs`<Editor::Map::Feature @value={{this.value}} />`);

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "Nomadisch Grün - Local Urban Food",
    );
  });

  it("can change the source value", async function () {
    this.set("value", {
      data: {
        source: { model: "article", id: "1" },
      },
    });

    let changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Map::Feature @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".use-current-model").value = "yes";
    await triggerEvent(".use-current-model", "change");

    expect(changedValue).to.deep.equal({
      source: { useSelectedModel: true },
      border: { classes: [] },
      elements: {
        fullscreen: false,
        zoom: false,
        download: false,
        mapLicense: false,
        baseMapLicense: false,
      },
    });
  });

  it("renders the border value", async function () {
    this.set("value", {
      data: {
        source: { model: "article", id: "1" },
        border: { classes: ["border-visibility-yes"] },
      },
    });

    await render(hbs`<Editor::Map::Feature @value={{this.value}} />`);

    expect(this.element.querySelector(".input-sm-border-visibility").value).to.equal("yes");
  });

  it("can change the border value", async function () {
    this.set("value", {
      data: {},
    });

    let changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Map::Feature @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input-sm-border-visibility").value = "yes";
    await triggerEvent(".input-sm-border-visibility", "change");

    expect(changedValue).to.deep.equal({
      source: {},
      border: { classes: ["border-visibility-yes"] },
      elements: {
        fullscreen: false,
        zoom: false,
        download: false,
        mapLicense: false,
        baseMapLicense: false,
      },
    });
  });

  it("renders the elements value", async function () {
    this.set("value", {
      data: {
        source: { model: "article", id: "1" },
        border: { classes: ["border-visibility-yes"] },
        elements: {
          fullscreen: true,
          zoom: true,
          mapLicense: true,
          baseMapLicense: true,
          download: true,
        },
      },
    });

    await render(hbs`<Editor::Map::Feature @value={{this.value}} />`);

    expect(this.element.querySelector(".input-fullscreen").checked).to.equal(true);
    expect(this.element.querySelector(".input-zoom").checked).to.equal(true);
    expect(this.element.querySelector(".input-download").checked).to.equal(true);
    expect(this.element.querySelector(".input-map-license").checked).to.equal(true);
    expect(this.element.querySelector(".input-base-map-license").checked).to.equal(true);
  });

  it("can change the elements values", async function () {
    this.set("value", {
      data: {},
    });

    let changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Map::Feature @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".input-fullscreen");
    await click(".input-zoom");
    await click(".input-download");
    await click(".input-map-license");
    await click(".input-base-map-license");

    expect(changedValue).to.deep.equal({
      source: {},
      border: { classes: [] },
      elements: {
        fullscreen: true,
        zoom: true,
        download: true,
        mapLicense: true,
        baseMapLicense: true,
      },
    });
  });
});
