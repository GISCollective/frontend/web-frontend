/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class BrowseFullscreenSwiperButtonComponent extends Component {
  @tracked isFullscreen;

  @action
  enableFullScreen() {
    this.isFullscreen = true;
  }

  @action
  disableFullScreen() {
    this.isFullscreen = false;
  }

  @action
  fullScreen(value) {
    this.isFullscreen = value;
  }
}
