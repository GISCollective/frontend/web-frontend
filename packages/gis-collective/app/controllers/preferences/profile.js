/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import PictureMeta from 'models/lib/picture-meta';
import { service } from '@ember/service';

export default class PreferencesProfileController extends Controller {
  @service fastboot;
  @service store;

  @action
  update() {
    return this.model.profile.save();
  }

  get detailedLocation() {
    if (typeof this.model.detailedLocation?.value == 'string') {
      return this.model.detailedLocation?.value.toLowerCase() == 'true';
    }

    return this.model.detailedLocation?.value;
  }

  @action
  async createImage() {
    this.model.profile.picture = this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });

    return await this.model.profile.picture;
  }
}
