/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, fillIn, waitUntil } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/pages/edit', function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let page;
  let receivedPage;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');
    page = server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultArticle('5ca78e2160780601008f69e6');

    receivedPage = null;
    server.put(`/mock-server/pages/${page._id}`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = page._id;

      server.testData.storage.addPage(receivedPage.page);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPage)];
    });

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/pages/edit/${page._id}`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fpages%2Fedit%2F000000000000000000000001');
  });

  it('renders the form for a page', async function () {
    authenticateSession();
    await visit(`/manage/pages/edit/${page._id}`);

    expect(PageElements.containerGroupTitles()).to.deep.equal(['team', 'original author', 'Name', 'Slug']);
  });

  it('can update the name', async function () {
    authenticateSession();
    await visit(`/manage/pages/edit/${page._id}`);

    await click('.row-name .btn-edit');
    await fillIn('.row-name input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.name).to.equal('new name');
  });

  it('can update the slug', async function () {
    authenticateSession();
    await visit(`/manage/pages/edit/${page._id}`);

    await click('.row-slug .btn-edit');
    await fillIn('.row-slug input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.slug).to.equal('new name');
  });
});
