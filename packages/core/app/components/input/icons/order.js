/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';
import { later } from '@ember/runloop';

export default class InputIconsOrderComponent extends Component {
  @service fastboot;
  @tracked value = A();

  get showIconTypeLabel() {
    if (this.args.showIconTypeLabel === undefined) {
      return true;
    }

    return this.args.showIconTypeLabel;
  }

  @action
  async setup() {
    let value = (await this.args.value) ?? [];
    value = value.slice();

    if (this.args.onSelect) {
      value.push({ isAdd: true });
    }

    later(() => {
      this.value = value;
    });
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    return this.triggerOnChange(sourceList);
  }

  @action
  primary(index) {
    if (index == 0 || this.args.disabled) {
      return;
    }

    return this.dragEnd({
      sourceList: this.value,
      sourceIndex: index,
      targetList: this.value,
      targetIndex: 0,
    });
  }

  @action
  remove(index) {
    this.value.splice(index, 1);
    this.triggerOnChange(this.value);
  }

  @action
  select() {
    if (this.args.onSelect) {
      return this.args.onSelect();
    }
  }

  async triggerOnChange(newValue) {
    if (!this.args.onChange) {
      return;
    }

    this.isLoading = true;
    let result;

    try {
      result = await this.args.onChange(newValue.filter((a) => !a.isAdd));
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
    }

    this.isLoading = false;

    return result;
  }
}
