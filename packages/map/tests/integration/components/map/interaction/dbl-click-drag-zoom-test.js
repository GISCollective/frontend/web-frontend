import { module, test } from 'qunit';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module(
  'Integration | Component | map/interaction/dbl-click-drag-zoom',
  function (hooks) {
    setupRenderingTest(hooks);

    test('it renders', async function (assert) {
      await render(hbs`<Map::Interaction::DblClickDragZoom />`);
      assert.dom(this.element).hasText('');
    });
  },
);
