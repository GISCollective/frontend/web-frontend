/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';

export default class ManageTeamsAddRoute extends AuthenticatedRoute {
  model() {
    if (this.isFastBoot) {
      return;
    }

    this.modelsToClear = ['team'];

    return this.store.createRecord('team', {
      name: '',
      about: '',
      isPublic: false,
      owners: [],
      leaders: [],
      members: [],
      guests: [],
    });
  }
}
