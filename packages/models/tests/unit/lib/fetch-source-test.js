/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { fetch } from 'models/lib/fetch-source';
import { PageCol } from 'models/transforms/page-col-list';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Unit | Transform | fetch function', function (hooks) {
  setupTest(hooks);

  let store;
  let server;
  let spaceData;
  let mapData;
  let teamData;
  let pictureData;

  hooks.beforeEach(function () {
    server = new TestServer();

    mapData = server.testData.storage.addDefaultMap();
    pictureData = server.testData.storage.addDefaultPicture();
    spaceData = server.testData.storage.addDefaultSpace();
    teamData = server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');

    spaceData.defaultModels = {
      map: mapData._id,
    };

    server.testData.storage.addDefaultArticle();

    store = this.owner.lookup('service:store');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('does nothing when there is no argument', async function (a) {
    const result = await fetch();

    expect(result).not.to.exist;
  });

  describe('with the default flag', function () {
    it('returns a default map when it is a source', async function () {
      let space = await this.store.findRecord('space', spaceData._id);

      const pageCol = {
        name: '0.0.0',
        data: {
          source: {
            model: 'map',
            useDefaultModel: true,
            useSelectedModel: false,
          },
        },
      };

      const result = await fetch({}, pageCol, space, this.store);

      expect(result.id).to.equal(space.defaultModels.map);
    });

    it('returns null when there is no source id', async function () {
      spaceData.defaultModels = {};

      let space = await this.store.findRecord('space', spaceData._id);

      const pageCol = {
        name: '0.0.0',
        data: {
          source: {
            model: 'map',
            useDefaultModel: true,
            useSelectedModel: false,
          },
        },
      };

      const result = await fetch({}, pageCol, space, this.store);

      expect(result).not.to.exist;
    });

    it('returns the space team when the default team is a source', async function () {
      let space = await this.store.findRecord('space', spaceData._id);

      const pageCol = {
        name: '0.0.0',
        data: {
          source: {
            model: 'team',
            useDefaultModel: true,
            useSelectedModel: false,
          },
        },
      };

      const result = await fetch({}, pageCol, space, this.store);

      expect(result.id).to.equal(spaceData.visibility.team);
    });
  });

  describe('restoreLegacySource', function () {
    it('fixes the source and returns a bucket list', async function () {
      let space = await this.store.findRecord('space', spaceData._id);

      const pageCol = new PageCol({
        name: '0.0.0',
        data: {
          model: 'map',
          ids: [mapData._id],
        },
      });

      const result = await fetch({}, pageCol, space, this.store);

      expect(result.items[0].id).to.equal(mapData._id);
    });
  });

  describe('fetchSourceListPageCol', function () {
    it('returns a bucket list when the ids key is present', async function () {
      let space = await this.store.findRecord('space', spaceData._id);

      const pageCol = new PageCol({
        name: '0.0.0',
        data: {
          source: {
            model: 'map',
            ids: [mapData._id],
          },
        },
      });

      const result = await fetch({}, pageCol, space, this.store);

      expect(result.items[0].id).to.equal(mapData._id);
    });
  });

  describe('fetchSourceRecord', function () {
    it('returns a record when the id key is present', async function () {
      let space = await this.store.findRecord('space', spaceData._id);

      const pageCol = new PageCol({
        name: '0.0.0',
        data: {
          source: {
            model: 'team',
            id: teamData._id,
          },
        },
      });

      const result = await fetch({}, pageCol, space, this.store);

      expect(result.id).to.equal(teamData._id);
    });
  });

  describe('fetchProperty', function () {
    it('returns a record when the id key is present', async function () {
      let space = await this.store.findRecord('space', spaceData._id);
      let map = await this.store.findRecord('map', mapData._id);

      const pageCol = new PageCol({
        name: '0.0.0',
        data: {
          source: {
            property: 'cover',
            useSelectedModel: true,
          },
        },
      });

      const result = await fetch({ selectedModel: map }, pageCol, space, this.store);

      expect(result.id).to.equal(pictureData._id);
    });
  });
});
