/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | view/stats', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Stats />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the stat value and label with the proper styles', async function () {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        label: {
          color: 'green',
          classes: ['text-center', 'fw-light'],
          text: 'some label',
        },
        valueStyle: { color: 'red', classes: ['display-3', 'fw-bold'] },
      },
      modelKey: 'stat_published-campaigns',
    });

    this.set('model', {
      'stat_published-campaigns': { value: 33 },
    });

    await render(
      hbs`<View::Stats @model={{this.model}} @value={{this.value}} />`,
    );

    expect(
      this.element.querySelector('.page-col-stats-label').textContent.trim(),
    ).to.equal('some label');
    expect(this.element.querySelector('.page-col-stats-label')).to.have.classes(
      'page-col-stats-label text-center fw-light text-green',
    );
    expect(
      this.element.querySelector('.page-col-stats-value').textContent.trim(),
    ).to.equal('33');
    expect(this.element.querySelector('.page-col-stats-value')).to.have.classes(
      'page-col-stats-value display-3 fw-bold text-red',
    );
  });
});
