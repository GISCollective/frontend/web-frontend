/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ApplicationAdapter from './application';

export default class UserProfileAdapter extends ApplicationAdapter {
  contributions(id) {
    const url = this.buildURL('user-profile', id) + `/contributions`;

    return this.ajax(url, 'GET');
  }
}
