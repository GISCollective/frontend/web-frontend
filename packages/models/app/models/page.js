/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';
import { LayoutContainer } from '../transforms/layout-container-list';
import { LayoutRow, LayoutCol } from 'models/transforms/layout-row-list';
import { PageCol } from 'models/transforms/page-col-list';
import { toPagePath } from 'models/lib/model-slug';
import { A } from 'models/lib/array';
import { getOwner } from '@ember/application';
import { fetch } from 'models/lib/fetch-source';
import { tracked } from '@glimmer/tracking';

export default class PageModel extends Model {
  @attr('model-info') info;
  @attr('string') name;
  @attr('string') slug;
  @attr('string') description;

  @belongsTo('space', { async: true, inverse: null })
  space;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr('page-col-list') cols;
  @attr('visibility') visibility;
  @attr('boolean') canEdit;
  @attr('layout-container-list') layoutContainers;
  @attr('string-list') categories;

  @tracked _shouldSave;

  async clone() {
    const jsonPage = this.serialize();
    jsonPage.space = this.store.peekRecord('space', jsonPage.space);

    const page = this.store.createRecord('page', jsonPage);
    page.name = `${jsonPage.name} - copy`;
    page.slug = `${jsonPage.slug}-copy`;
    page.visibility.isPublic = false;

    return page.save();
  }

  get shouldSave() {
    return (this.isNew || this.hasDirtyAttributes || this._shouldSave) ?? false;
  }

  set shouldSave(value) {
    return (this._shouldSave = value);
  }

  async save() {
    const result = await super.save();

    this._shouldSave = false;

    return result;
  }

  ensureArrayCol() {
    if (this.cols.push) {
      return;
    }

    this.cols = A(this.cols);
  }

  upsertPageColByName(name) {
    if (!this.existsCol(name)) {
      throw new Error(`There is no column named "${name}".`);
    }

    this.ensureArrayCol();

    let col = this.cols.find((a) => a.name == name);

    if (!col) {
      const space = getOwner(this.store).lookup('service:space');
      col = new PageCol({ name });
      this.cols.push(col);
    }

    return col;
  }

  replacePageCol(pageCol) {
    this.cols = A(this.cols.filter((a) => a.name != pageCol.name));
    this.cols.push(pageCol);
  }

  existsCol(name) {
    for (const container of this.layoutContainers) {
      for (const row of container.rows) {
        if (row.name === name) {
          return 'row';
        }

        for (const col of row.cols) {
          if (col.name === name) {
            return 'col';
          }
        }
      }
    }

    if (!isNaN(name)) {
      return parseInt(name) < this.layoutContainers.length ? 'container' : false;
    }

    if (!name) {
      return false;
    }

    const pieces = name.split('.');

    if (pieces.length == 2 && pieces[1].indexOf('layer-') == 0) {
      return 'container';
    }

    if (this.existsCol(pieces.slice(0, -1).join('.')) == 'col') {
      return isNaN(pieces[pieces.length - 1]) ? false : 'col';
    }

    return false;
  }

  renameCol(from, to) {
    if (!this.existsCol(from)) {
      throw new Error(`The source name "${from}" does not match any column.`);
    }

    if (this.existsCol(to)) {
      throw new Error(`The destination name "${to}" is already used.`);
    }

    for (const col of this.cols ?? []) {
      if (col.name === from) {
        col.name = to;
      }
    }

    for (const container of this.layoutContainers) {
      for (const row of container.rows) {
        if (row.name === from) {
          row.name = to;
          this.checkGlobalLayoutUpdate(container);
        }

        for (const col of row.cols) {
          if (col.name === from) {
            col.name = to;
            this.checkGlobalLayoutUpdate(container);
          }
        }
      }
    }
  }

  getRevisions() {
    return this.store.adapterFor(this.constructor.modelName).getRevisions(this.id);
  }

  contact(message) {
    return this.store.adapterFor(this.constructor.modelName).contact(this.id, message);
  }

  addLayout(container, row, col) {
    if (!this.layoutContainers) {
      this.layoutContainers = [];
    }

    while (this.layoutContainers.length <= container) {
      const len = this.layoutContainers.length;
      this.layoutContainers.push(new LayoutContainer({ name: `${len}` }));
    }

    if (isNaN(row)) {
      return;
    }

    while (this.layoutContainers[container].rows.length <= row) {
      const len = this.layoutContainers[container].rows.length;

      this.layoutContainers[container].rows.push(new LayoutRow({ name: `${container}.${len}` }));
    }

    if (isNaN(col)) {
      return;
    }

    while (this.layoutContainers[container].rows[row].cols.length <= col) {
      const len = this.layoutContainers[container].rows[row].cols.length;
      this.layoutContainers[container].rows[row].cols.push(new LayoutCol({ name: `${container}.${row}.${len}` }));
    }

    this.checkGlobalLayoutUpdate(container);
  }

  checkGlobalLayoutUpdate(container) {
    if (!this.layoutContainers[container]?.gid) {
      return;
    }

    const spaceContainers = this.space.get ? this.space.get('layoutContainers') : this.space.layoutContainers;

    spaceContainers.setContainer(this.layoutContainers[container].gid, this.layoutContainers[container]);
  }

  getLayoutByName(name) {
    const container = this.layoutContainers.find((a) => a.contains(name));
    const row = container?.rows?.find((a) => a.contains(name));
    const col = row?.cols?.find((a) => a.contains(name));

    return col ?? row ?? container;
  }

  getContainerFor(name) {
    return this.layoutContainers.find((a) => a.contains(name));
  }

  getRowFor(name) {
    return this.getContainerFor(name)?.rows.find((a) => a.contains(name));
  }

  addLayoutContainerBefore(container) {
    if (!this.layoutContainers?.[container]) {
      throw new Error(`Can't add a container because the index '${container}' does not exist.`);
    }

    for (let i = container; i < this.layoutContainers.length; i++) {
      this.layoutContainers[i].updateIndex(i + 1);
    }

    this.layoutContainers.splice(container, 0, new LayoutContainer({}));

    for (const col of this.cols ?? []) {
      col.incrementContainerNameAfter(container);
    }
  }

  addLayoutRowBefore(container, row) {
    if (!this.layoutContainers?.[container]) {
      throw new Error(`Can't add a row because the container '${container}' does not exist.`);
    }

    if (!this.layoutContainers?.[container]?.rows[row]) {
      throw new Error(`Can't add a row because the index '${row}' does not exist.`);
    }

    for (let i = row; i < this.layoutContainers[container].rows.length; i++) {
      this.layoutContainers[container].rows[i].updateIndex(i + 1);
    }

    this.layoutContainers[container].rows.splice(row, 0, new LayoutRow({ name: `${container}.${row}` }));

    for (const col of this.cols ?? []) {
      col.incrementRowNameAfter(container, row);
    }

    this.checkGlobalLayoutUpdate(container);
  }

  addLayoutColBefore(container, row, col) {
    if (!this.layoutContainers?.[container]?.rows[row]?.cols[col]) {
      throw new Error(`Can't add a col because the index '${col}' does not exist.`);
    }

    for (let i = row; i < this.layoutContainers[container].rows[row].cols.length; i++) {
      this.layoutContainers[container].rows[row].cols[i].updateIndex(i + 1);
    }

    this.layoutContainers[container].rows[row].cols.splice(
      col,
      0,
      new LayoutCol({ name: `${container}.${row}.${col}` }),
    );

    for (const col of this.cols ?? []) {
      col.incrementColNameAfter(container, row, col);
    }

    this.checkGlobalLayoutUpdate(container);
  }

  deleteContainer(container) {
    if (!this.layoutContainers?.[container]) {
      throw new Error(`The container '${container}' does not exist.`);
    }

    this.cols = A(this.cols?.filter((a) => parseInt(a.name.split('.')[0]) !== container));

    for (const col of this.cols ?? []) {
      col.incrementContainerNameAfter(container, -1);
    }

    this.layoutContainers.splice(container, 1);
    this.layoutContainers?.forEach((a, index) => a.updateIndex(index));
  }

  deleteRow(container, row) {
    if (!this.layoutContainers?.[container]?.rows?.[row]) {
      throw new Error(`The row '${container}.${row}' does not exist.`);
    }

    const newCols = this.cols?.filter((a) => {
      const pieces = a.name.split('.').map((a) => parseInt(a));
      return pieces[0] !== container || pieces[1] !== row;
    });

    this.cols = A(newCols);

    for (const col of this.cols ?? []) {
      col.incrementRowNameAfter(container, row, -1);
    }

    this.layoutContainers[container].rows.splice(row, 1);
    this.layoutContainers[container].rows.forEach((a, index) => a.updateIndex(index));

    this.checkGlobalLayoutUpdate(container);
  }

  deleteCol(container, row, col) {
    if (!this.layoutContainers?.[container]?.rows?.[row]?.cols[col]) {
      throw new Error(`The col '${container}.${row}.${col}' does not exist.`);
    }

    this.cols = A(
      this.cols?.filter((a) => {
        const pieces = a.name.split('.').map((a) => parseInt(a));
        return pieces[0] !== container || pieces[1] !== row || pieces[2] !== col;
      }),
    );

    this.cols?.forEach((a) => a.incrementColNameAfter(container, row, col, -1));

    this.layoutContainers[container].rows[row].cols.splice(col, 1);
    this.layoutContainers[container].rows[row].cols.forEach((a, index) => a.updateIndex(index));

    this.checkGlobalLayoutUpdate(container);
  }

  async loadRevision(revisionId) {
    return this.store.adapterFor(this.constructor.modelName).loadRevision(this.id, revisionId);
  }

  pathForRecord(record) {
    const slug = this.slug ?? '';
    let pieces = slug.split('--').filter((a) => a != '');

    if (record?.id) {
      pieces = pieces.map((a) => (a.indexOf(':') === 0 ? record.id : a));
    }

    return `/${pieces.join('/')}`;
  }

  get path() {
    return toPagePath(this.slug);
  }

  get title() {
    return this.name;
  }

  recordTitle(record) {
    if (record?.get) {
      return record.get('name');
    }

    return this.name;
  }

  get relatedModelType() {
    let pieces = this.slug?.split?.(':') ?? [];

    if (pieces.length != 2) {
      return undefined;
    }

    if (pieces[1].includes('_')) {
      pieces = pieces[1].split('_');
    } else if (pieces[1].includes('-')) {
      pieces = pieces[1].split('-');
    }

    return pieces[0];
  }

  get knownSlots() {
    const result = [];

    for (const container of this.layoutContainers) {
      result.push(container.name);

      for(let name of container.names) {
        result.push(name);
      }
    }

    return result;
  }

  get visibleCols() {
    const knownSlots = this.knownSlots;

    return A(this.cols.filter((a) => knownSlots.includes(a.name)));
  }

  get colsWithoutParams() {
    return A(this.visibleCols.filter((col) => !col.data?.source?.parameters && !col.data?.source?.property) ?? []);
  }

  get colsWithParams() {
    return A(this.visibleCols.filter((col) => col.data?.source?.parameters || col.data?.source?.property) ?? []);
  }

  async fetchColsWithoutParams(model) {
    const space = await this.space;

    for (let col of this.colsWithoutParams) {
      const hasSource = col.data?.source || col.model || col.data.model;

      if (!col.modelKey && hasSource) {
        console.error('model key is not set!', col);
        continue;
      }

      if (!model[col.modelKey] && hasSource) {
        model[col.modelKey] = await fetch(model, col, space, this.store);
      }
    }
  }

  async fetchColsWithParams(model) {
    const space = await this.space;

    for (let col of this.colsWithParams) {
      const hasSource = col.data?.source || col.model || col.data.model;

      if (!model[col.modelKey]) {
        if (!col.modelKey && hasSource) {
          console.error('model key is not set!', col);
          continue;
        }

        if (!model[col.modelKey] && hasSource) {
          model[col.modelKey] = await fetch(model, col, space, this.store);
        }
      }
    }
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.name;
    metaInfo.description = this.description;
    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }

  get category() {
    const landingPageId = this.space?.get?.('landingPageId');

    if (landingPageId && landingPageId == this.id) {
      return 'landing page';
    }

    if (!this.slug) {
      return 'other';
    }

    const pieces = this.slug.split('--');

    if (pieces[0].indexOf('_') == 0) {
      return 'special';
    }

    if (pieces.length == 1) {
      return 'top-level';
    }

    return pieces[0];
  }

  deleteMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.deleteMany(...arguments);
  }

  get isPublished() {
    return this.visibility.isPublic;
  }

  get availableActions() {
    return [this.isPublished ? 'unpublish' : 'publish'];
  }

  async publish() {
    if (this.isPublished) {
      return false;
    }

    await this.store.adapterFor(this.constructor.modelName).publish(this.id);
    await this.reload();
  }

  async publishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.publishMany(...arguments);
  }

  async unpublish() {
    if (!this.isPublished) {
      return false;
    }

    await this.store.adapterFor(this.constructor.modelName).unpublish(this.id);
    await this.reload();
  }

  async unpublishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.unpublishMany(...arguments);
  }

  get canShowFeaturePanel() {
    const mapCols = this.cols?.filter((a) => a.data?.map?.featureSelection) ?? [];
    const mapsWithPanel = mapCols?.find((a) => a.data.map.featureSelection == 'open details in panel');

    if (mapsWithPanel) {
      return true;
    }

    return mapCols.length == 0;
  }
}
