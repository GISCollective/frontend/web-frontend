export class Factories {
  static instance: Factories = new Factories();

  A<T>(input?: T[] | undefined): T[] {
    return input || [];
  }
}
