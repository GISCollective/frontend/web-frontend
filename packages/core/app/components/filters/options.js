/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class FiltersOptionsComponent extends Component {
  @service clickOutside;
  @tracked showSuggestions = false;

  @action
  setup(element) {
    this.element = element;
  }

  @action
  hide() {
    if (this.showSuggestions) {
      this.showSuggestions = false;
    }
  }

  @action
  reset() {
    this.args.onChange?.(null);
  }

  @action
  toggleVisibility() {
    this.showSuggestions = !this.showSuggestions;

    if (this.showSuggestions) {
      this.clickOutside.subscribe(this.element, () => {
        this.showSuggestions = false;
      });
    }
  }
}
