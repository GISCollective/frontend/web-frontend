/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | campaign questions', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:campaign-questions');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized.toJSON()).to.deep.equal({
      registrationMandatory: false,
      featureNamePrefix: '',
      name: { customQuestion: false, label: '' },
      description: { customQuestion: false, label: '' },
      icons: { customQuestion: false, label: '' },
      pictures: { customQuestion: false, label: '', isMandatory: false },
      location: {
        customQuestion: false,
        label: '',
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: false,
      },
    });
  });

  it('can deserialize an object with all values', function () {
    let values = {
      registrationMandatory: true,
      featureNamePrefix: '--',
      name: { customQuestion: true, label: '1' },
      description: { customQuestion: true, label: '2' },
      icons: { customQuestion: true, label: '3' },
      pictures: { customQuestion: true, label: 'pictures message', isMandatory: true },
      location: {
        customQuestion: true,
        label: '4',
        allowGps: false,
        allowManual: false,
        allowAddress: false,
        allowExistingFeature: true,
      },
    };

    let deserialized = transform.deserialize(values);

    expect(deserialized.toJSON()).to.deep.equal(values);
  });

  it('can use the old properties', function () {
    let values = {
      featureNamePrefix: '0',
      registrationMandatory: true,
      name: { customQuestion: true, label: '1' },
      description: { customQuestion: true, label: '2' },
      icons: { customQuestion: true, label: '3' },
      location: {
        customQuestion: true,
        label: '4',
        allowGps: false,
        allowManual: false,
        allowAddress: false,
        allowExistingFeature: true,
      },
    };

    let deserialized = transform.deserialize(values);

    expect(deserialized.registrationMandatory).to.equal(true);
    expect(deserialized.featureNamePrefix).to.equal('0');

    expect(deserialized.showNameQuestion).to.equal(true);
    expect(deserialized.nameLabel).to.equal('1');

    expect(deserialized.showDescriptionQuestion).to.equal(true);
    expect(deserialized.descriptionLabel).to.equal('2');

    expect(deserialized.iconsLabel).to.equal('3');
  });
});
