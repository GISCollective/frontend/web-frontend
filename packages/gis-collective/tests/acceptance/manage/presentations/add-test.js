/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
} from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe ('Acceptance | manage/presentations/add',  function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedPresentation;
  let team;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultPresentation('000000000000000000000001');
    server.testData.storage.addDefaultArticle('5ca78e2160780601008f69e6');

    receivedPresentation = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/presentations/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fpresentations%2Fadd');
  });

  describe ('when an user is authenticated and no team is available',  function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser();
    });

    it('can visit the presentation with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/presentations/add`);
      expect(currentURL()).to.equal('/manage/presentations/add');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'New presentation'
      );

      expect(
        this.element.querySelector('.row-name label').textContent.trim()
      ).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-slug label').textContent.trim()
      ).to.contain('Slug');
      expect(this.element.querySelector('.row-slug input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-team label').textContent.trim()
      ).to.contain('Team');
      expect(this.element.querySelector('.row-team select')).to.not.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to
        .exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit(`/manage/presentations/add`);
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.presentation');
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit(`/manage/presentations/add`);
      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.presentation');
    });
  });

  describe ('when an user is authenticated and a team is available',  function (hooks) {
    hooks.beforeEach(function () {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser();
    });

    it('can visit the presentation with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/presentations/add`);
      expect(currentURL()).to.equal('/manage/presentations/add');

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit(`/manage/presentations/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Dashboard', 'New presentation'
      ]);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
      ]);
    });

    describe ('when the server request is successful',  function (hooks) {
      let presentation;

      hooks.beforeEach(function () {
        team = server.testData.storage.addDefaultTeam();
        presentation = server.testData.storage.addDefaultPresentation('000000000000000000000001');
        server.testData.storage.addDefaultTeam('000000000000000000000001');
        server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');

        server.post('/mock-server/presentations', (request) => {
          receivedPresentation = JSON.parse(request.requestBody);
          receivedPresentation.presentation._id = presentation._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedPresentation),
          ];
        });
      });

      it('redirects to the edit presentation after the request response is received', async function () {
        authenticateSession();

        await visit(`/manage/presentations/add`);

        await fillIn('.input-name', 'new presentation name');
        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedPresentation.presentation.name).to.equal(
          'new presentation name'
        );
        expect(receivedPresentation.presentation.slug).to.equal('new-slug');
        expect(receivedPresentation.presentation.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        });

        expect(currentURL()).to.equal(
          `/manage/presentations/${presentation._id}`
        );
      });
    });

    describe ('when the server request fails',  function (hooks) {
      hooks.beforeEach(function () {
        server.post('/mock-server/presentations', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description: 'The `presentation.slug` field is required.',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function () {

        authenticateSession();

        await visit(`/manage/presentations/add`);

        await fillIn('.input-name', 'new presentation name');
        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/manage/presentations/add`);

        await Modal.waitToDisplay();

        expect(Modal.title()).to.equal('Validation error');
        expect(Modal.message()).to.equal(
          'The `presentation.slug` field is required.'
        );
      });
    });
  });

  describe ('when an admin user is authenticated',  function (hooks) {
    hooks.beforeEach(() => {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser(true);
    });

    it('can visit the presentation with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/presentations/add`);
      expect(currentURL()).to.equal('/manage/presentations/add');
    });

    describe ('when the server request is successfully',  function (hooks) {
      let presentation;

      hooks.beforeEach(function () {
        presentation = server.testData.storage.addDefaultPage();
        server.post('/mock-server/presentations', (request) => {
          receivedPresentation = JSON.parse(request.requestBody);
          receivedPresentation.presentation._id = presentation._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedPresentation),
          ];
        });
      });

      it('queries the teams without the all query param', async function () {
        authenticateSession();

        await visit(`/manage/presentations/add`);

        await fillIn('.input-name', 'new presentation name');
        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedPresentation.presentation.name).to.deep.equal(
          'new presentation name'
        );
        expect(currentURL()).to.equal(
          `/manage/presentations/${presentation._id}`
        );
      });

      it('queries the teams with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/manage/presentations/add`);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain(
          'GET /mock-server/teams?all=true&edit=true'
        );
      });
    });
  });
});
