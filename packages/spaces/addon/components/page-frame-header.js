import Component from "@glimmer/component";

export default class PageFrameHeader extends Component {
  get externalUrl() {
    return `https://${this.args.model?.page?.space?.get("domain")}${this.args.model?.page?.path}`;
  }
}
