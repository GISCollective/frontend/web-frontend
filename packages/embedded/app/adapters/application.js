/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import RESTAdapter from 'models/adapters/application';
import { service } from '@ember/service';
import { classify } from '@ember/string';

export default class ApplicationAdapter extends RESTAdapter {
  @service embeddedClient;
  requestIndex = 0;

  buildURL(modelName, id, snapshot, requestType, query) {
    if (!window.hasEmbeddedStore) {
      return super.buildURL(...arguments);
    }

    return JSON.stringify({
      modelName: classify(modelName),
      id,
      requestType,
      query,
    });
  }

  async ajax(url, type, options) {
    if (!window.hasEmbeddedStore) {
      return super.ajax(...arguments);
    }

    const info = JSON.parse(url);
    const result = await this.embeddedClient.sendMessage('storeFetch', {
      info,
      type,
      options,
    });

    return result;
  }
}
