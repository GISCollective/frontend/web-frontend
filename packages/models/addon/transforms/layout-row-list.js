/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class LayoutRowListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map((a) => new LayoutRow(a));
  }

  serialize(deserialized) {
    return deserialized.map((a) => a.toJSON());
  }
}

export class LayoutRow {
  @tracked options = [];
  @tracked cols = [];
  @tracked name = '';
  @tracked data;

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (serialized.name) {
      this.name = serialized.name;

      serialized.cols?.forEach?.((element, index) => {
        if (!element.name) {
          element.name = `${this.name}.${index}`;
        }
      });
    }

    if (Array.isArray(serialized.options)) {
      this.options = serialized.options;
    }

    if (Array.isArray(serialized.cols)) {
      this.cols = serialized.cols.map((a) => new LayoutCol(a));
    }

    this.data = new LayoutColData(serialized.data);
  }

  get cls() {
    return this.options.join(' ');
  }

  get classes() {
    const value = this.options ?? [];
    const containerClasses = this.data?.container?.classes ?? [];

    return [...value, ...containerClasses].filter((a) => a);
  }

  set classes(value) {
    this.options = value;

    if (!this.data) {
      this.data = new LayoutColData(serialized.data);
    }

    this.data.container.classes = value;
  }

  updateContainerIndex(newIndex) {
    const pieces = this.name?.split('.') ?? [];

    if (this.hasDefaultName) {
      this.name = `${newIndex}.${pieces[1]}`;
    }

    for (const col of this.cols) {
      col.updateContainerIndex(newIndex);
    }
  }

  updateIndex(newIndex) {
    const pieces = this.name?.split('.') ?? [];

    if (this.hasDefaultName) {
      this.name = `${pieces[0]}.${newIndex}`;
    }

    for (const col of this.cols) {
      col.updateRowIndex(newIndex);
    }
  }

  contains(name) {
    if (name == this.name) {
      return true;
    }

    return !!this.cols.find((a) => a.contains(name));
  }

  get hasDefaultName() {
    const pieces = this.name?.split('.') ?? [];
    return pieces.length == 2 && !isNaN(pieces[0]) && !isNaN(pieces[1]);
  }

  toJSON() {
    return {
      name: this.name,
      cols: this.cols.map((a) => a.toJSON()),
      options: this.options,
      data: this.data.toJSON(),
    };
  }
}

export class LayoutCol {
  @tracked type = '';
  @tracked data;
  @tracked isSelected;
  @tracked name = '';
  @tracked componentCount = 1;
  @tracked options = [];
  isLayoutCol = true;

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (Array.isArray(serialized.options)) {
      this.options = serialized.options;
    }

    if (typeof serialized.name == 'string') {
      this.name = serialized.name.trim();
    }

    if (serialized.type) {
      this.type = serialized.type;
    }

    this.data = new LayoutColData(serialized.data);

    if (!isNaN(serialized.componentCount)) {
      this.componentCount = parseInt(serialized.componentCount);
    }
  }

  get cls() {
    return this.options.join(' ');
  }

  get names() {
    const result = [this.name];

    for (let i = 1; i < this.componentCount; i++) {
      result.push(`${this.name}.${i}`);
    }

    return result;
  }

  contains(name) {
    return name == this.name || this.names.includes(name);
  }

  updateContainerIndex(newIndex) {
    const pieces = this.name.split('.');

    if (this.hasDefaultName) {
      this.name = `${newIndex}.${pieces[1]}.${pieces[2]}`;
    }
  }

  updateRowIndex(newIndex) {
    const pieces = this.name.split('.');

    if (this.hasDefaultName) {
      this.name = `${pieces[0]}.${newIndex}.${pieces[2]}`;
    }
  }

  updateIndex(newIndex) {
    const pieces = this.name?.split('.') ?? [];

    if (this.hasDefaultName) {
      this.name = `${pieces[0]}.${pieces[1]}.${newIndex}`;
    }
  }

  get hasDefaultName() {
    const pieces = this.name?.split('.') ?? [];

    return pieces.length == 3 && !isNaN(pieces[0]) && !isNaN(pieces[1]) && !isNaN(pieces[2]);
  }

  toJSON() {
    return {
      type: this.type,
      data: this.data.toJSON(),
      name: this.name,
      componentCount: this.componentCount,
      options: this.options,
    };
  }
}

export class LayoutColData {
  @tracked container;

  constructor(obj) {
    if (typeof obj == 'object') {
      for (const key in obj) {
        this[key] = obj[key];
      }
    }
  }

  toJSON() {
    const result = {};

    if (typeof this.container == 'object' && Object.keys(this.container).length > 0) {
      result.container = this.container;
    }

    return result;
  }
}
