/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import Vector from 'ol/layer/Vector';
import Map from 'ol/Map';

describe ('Integration | Component | map/interaction/select',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when map and layer is not set', async function () {
    await render(hbs`<Map::Interaction::Select />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders', async function () {
    const map = new Map();
    this.set('map', map);
    this.set(
      'layer',
      new Vector({
        renderMode: 'vector',
      })
    );

    // Template block usage:
    await render(hbs`
      <Map::Interaction::Select @map={{this.map}} @layer={{this.layer}} as | interaction features |>
        {{interaction}}-{{features}}
      </Map::Interaction::Select>
    `);

    expect(this.element.textContent.trim()).to.equal('[object Object]-');
  });

  it('creates a new interaction in the map', async function () {
    const map = new Map();
    this.set('map1', map);
    this.set(
      'layer1',
      new Vector({
        renderMode: 'vector',
      })
    );

    const interactionCount = map.getInteractions().getLength();

    await render(
      hbs`<Map::Interaction::Select @map={{this.map1}} @layer={{this.layer1}} />`
    );
    expect(this.map1.getInteractions().getLength()).to.equal(
      interactionCount + 1
    );
  });

  it('removes the interaction when the component is removed', async function () {
    const map = new Map();
    this.set(
      'layer2',
      new Vector({
        renderMode: 'vector',
      })
    );

    this.set('map2', map);
    this.set('show', true);
    const interactionCount = map.getInteractions().getLength();

    await render(
      hbs`{{#if this.show}}<Map::Interaction::Select @map={{this.map2}} @layer={{this.layer2}} />{{/if}}`
    );
    expect(map.getInteractions().getLength()).to.equal(interactionCount + 1);

    this.set('show', false);
    expect(map.getInteractions().getLength()).to.equal(interactionCount);
  });
});
