/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/campaigns-preview', function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam('1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders all teams when there is no value', async function () {
    await render(hbs`<View::CampaignsPreview />`);
    expect(this.element.textContent.trim()).to.equal('surveys for: all teams');
  });

  it('renders the team name when is set', async function () {
    this.set('value', { data: { query: { team: '1' } } });
    await render(hbs`<View::CampaignsPreview @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal(
      'surveys for: Open Green Map',
    );
  });
});
