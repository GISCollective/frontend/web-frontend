/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";

export default class ManageEditorsPropertiesAttributionsLocationComponent extends Component {
  elementId = `editor-property-${guidFor(this)}`;

  allValues = ["default", "hidden", "below"];

  get value() {
    return `${this.args.value ?? "default"}`;
  }

  @action
  change(value) {
    this.args.onChange?.(value);
  }
}
