/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';

import { setupTest, it } from 'dummy/tests/helpers';
import { expect } from 'chai';

module('Unit | Transform | icon sprite', function (hooks) {
  setupTest(hooks);
  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:icon-sprite');
  });

  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('deserialize a null to an empty object', function () {
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({});
  });

  it('deserialize an object with all properties', function () {
    const deserialized = transform
      .deserialize({
        small: 'sm',
        large: 'lg',
      })
      .toJSON();

    expect(deserialized).to.deep.equal({
      small: 'sm',
      large: 'lg',
    });
  });

  it('serializes an object with all properties', function () {
    const deserialized = transform.deserialize({
      small: 'sm',
      large: 'lg',
    });

    const serialized = transform.serialize(deserialized);

    expect(serialized).to.deep.equal({
      small: 'sm',
      large: 'lg',
    });
  });
});
