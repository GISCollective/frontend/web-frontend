/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/properties/responsive-value", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the block for each supported bs size", async function () {
    await render(hbs`
      <Editor::Properties::ResponsiveValue as |size|>
        {{size}}
      </Editor::Properties::ResponsiveValue>
    `);

    const responsiveValues = this.element.querySelectorAll(".responsive-value-item");

    expect(responsiveValues).to.have.length(3);
    expect(responsiveValues[0]).to.have.class("responsive-value-sm");
    expect(responsiveValues[0].textContent.trim()).to.equal("sm");

    expect(responsiveValues[1]).to.have.class("responsive-value-md");
    expect(responsiveValues[1].textContent.trim()).to.equal("md");

    expect(responsiveValues[2]).to.have.class("responsive-value-lg");
    expect(responsiveValues[2].textContent.trim()).to.equal("lg");
  });

  it("has nothing selected by default", async function () {
    await render(hbs`
      <Editor::Properties::ResponsiveValue as |size|>
        {{size}}
      </Editor::Properties::ResponsiveValue>`);

    expect(this.element.querySelector(".text-primary")).not.to.exist;
  });

  it("has the md options selected when deviceSize is sm", async function () {
    await render(hbs`
      <Editor::Properties::ResponsiveValue @deviceSize="sm" as |size|>
        {{size}}
      </Editor::Properties::ResponsiveValue>`);

    expect(this.element.querySelector(".fa-mobile")).to.have.class("text-primary");

    expect(this.element.querySelector(".fa-tablet")).not.to.have.class("text-primary");

    expect(this.element.querySelector(".fa-desktop")).not.to.have.class("text-primary");
  });

  it("has the md options selected when deviceSize is md", async function () {
    await render(hbs`
      <Editor::Properties::ResponsiveValue @deviceSize="md" as |size|>
        {{size}}
      </Editor::Properties::ResponsiveValue>`);

    expect(this.element.querySelector(".fa-mobile")).not.to.have.class("text-primary");

    expect(this.element.querySelector(".fa-tablet")).to.have.class("text-primary");

    expect(this.element.querySelector(".fa-desktop")).not.to.have.class("text-primary");
  });

  it("has the md options selected when deviceSize is lg", async function () {
    await render(hbs`
      <Editor::Properties::ResponsiveValue @deviceSize="lg" as |size|>
        {{size}}
      </Editor::Properties::ResponsiveValue>`);

    expect(this.element.querySelector(".fa-mobile")).not.to.have.class("text-primary");

    expect(this.element.querySelector(".fa-tablet")).not.to.have.class("text-primary");

    expect(this.element.querySelector(".fa-desktop")).to.have.class("text-primary");
  });
});
