/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { addIconGroupToStore } from '../../../lib/icons';
import { tracked } from '@glimmer/tracking';

export default class ManageCampaignsEditRoute extends AuthenticatedRoute {
  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const teamsParams = {};

    teamsParams.all = !!params.allTeams;
    teamsParams.edit = !params.allTeams;

    const campaign = await this.store.findRecord('campaign', params.id);

    const editCampaignModel = new EditCampaignModel(campaign, teamsParams, this.store);

    return editCampaignModel.fetch();
  }
}

class EditCampaignModel {
  @tracked icons;
  @tracked maps;
  @tracked teams;
  @tracked team;
  @tracked campaign;
  @tracked map;
  @tracked iconSets;

  constructor(campaign, teamsParams, store) {
    this.campaign = campaign;
    this.teamsParams = teamsParams;
    this.store = store;
  }

  async fetch() {
    const campaignIcons = await this.campaign.get('icons');
    await Promise.all(campaignIcons.slice?.() ?? []);
    this.teams = await this.store.query('team', this.teamsParams);
    this.team = await this.campaign.get('visibility').fetchTeam();
    this.maps = await this.store.query('map', { team: this.team.id });
    this.map = null;

    await this.reloadIconSets();
    await this.reloadIcons();

    return this;
  }

  async reloadIconSets() {
    this.iconSets = null;

    if (this.campaign.map?.isEnabled) {
      try {
        this.iconSets = await this.campaign.map?.map?.iconSets.fetch();
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
      }
    }

    if (!this.iconSets) {
      this.iconSets = await this.store.query('icon-set', { default: true });
    }
  }

  async reloadIcons() {
    this.icons = await this.store
      .adapterFor('icon')
      .group({
        iconSetsCampaign: this.campaign.id,
      })
      .then((icons) => {
        addIconGroupToStore(icons, this.store);
        return icons;
      });
  }
}
