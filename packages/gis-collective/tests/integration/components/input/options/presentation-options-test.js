/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, fillIn, render, triggerEvent, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/options/presentation-options',  function (hooks) {
  setupRenderingTest(hooks, { server: true });

  let receivedPicture;

  it('can change the presentation name', async function () {
    this.set('presentation', {
      name: 'some name',
      slug: 'some-name',
    });

    await render(
      hbs`<Input::Options::PresentationOptions @presentation={{this.presentation}}/>`
    );

    expect(this.element.querySelector('.page-name').value).to.equal(
      'some name'
    );
    await fillIn('.page-name', 'new name');

    expect(this.presentation.name).to.equal('new name');
  });

  it('can change the presentation slug', async function () {
    this.set('presentation', {
      slug: 'some-slug',
    });

    await render(
      hbs`<Input::Options::PresentationOptions @presentation={{this.presentation}}/>`
    );

    expect(this.element.querySelector('.page-slug').value).to.equal(
      'some-slug'
    );
    await fillIn('.page-slug', 'new slug');

    expect(this.presentation.slug).to.equal('new slug');
  });

  it('can change the presentation hasIntro flag', async function () {
    this.set('presentation', {
      hasIntro: true,
      set(key, value) {
        this[key] = value;
      },
    });

    await render(
      hbs`<Input::Options::PresentationOptions @presentation={{this.presentation}}/>`
    );

    await click('.chk-has-intro input');

    expect(this.presentation.hasIntro).to.equal(false);
  });

  it('can change the presentation hasEnd flag', async function () {
    this.set('presentation', {
      hasEnd: true,
      set(key, value) {
        this[key] = value;
      },
    });

    await render(
      hbs`<Input::Options::PresentationOptions @presentation={{this.presentation}}/>`
    );

    await click('.chk-has-end input');

    expect(this.presentation.hasEnd).to.equal(false);
  });

  it('can change the is public flag', async function () {
    this.set('presentation', {
      set(key, value) {
        this[key] = value;
      },
    });

    await render(
      hbs`<Input::Options::PresentationOptions @presentation={{this.presentation}}/>`
    );

    await click('.chk-visibility input');

    expect(this.presentation['visibility.isPublic']).to.equal(true);
  });

  it('can change the introTimeout when the presentation hasIntro flag is set', async function () {
    this.set('presentation', {
      hasIntro: true,
    });

    await render(
      hbs`<Input::Options::PresentationOptions @presentation={{this.presentation}}/>`
    );

    await fillIn('.input-intro-timeout', '2000');

    expect(this.presentation).to.deep.equal({
      hasIntro: true,
      introTimeout: '2000',
    });
  });

  it('can change the cover', async function () {
    this.server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      const picture = this.server.testData.storage.addDefaultPicture();

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture,
        }),
      ];
    });

    this.set('presentation', { id: "some-id", hasIntro: true, hasEnd: true });

    await render(hbs`<Input::Options::PresentationOptions @presentation={{this.presentation}}/>`);

    const blob = this.server.testData.create.pngBlob();

    await triggerEvent("input[type='file']", 'change', {
      files: [blob],
    });

    await waitUntil(() => receivedPicture);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Presentation', id: "some-id" },
      data: {},
      disableOptimization: false
    });
  });
});
