/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';

export default class IssueModel extends Model {
  @belongsTo('feature', { async: true, inverse: null })
  feature;

  @attr('string') title;
  @attr('string') description;
  @attr('string') attributions;
  @attr('string') file;
  @attr('string') type;
  @attr('string') status;
  @attr('date') creationDate;
  @attr('string', { defaultValue: '@anonymous' }) author;

  get authorObject() {
    const id = this.author;

    if (id.indexOf('@') != -1) {
      return {
        name: id,
      };
    }

    return this.store.findRecord('user', id);
  }

  resolve() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.resolve(this);
  }

  reject() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.reject(this);
  }
}
