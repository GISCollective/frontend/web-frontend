/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr } from '@ember-data/model';
import { tracked } from '@glimmer/tracking';

export default class SoundModel extends Model {
  @attr('string') name;
  @attr('string') sound;
  @attr('visibility') visibility;
  @attr('model-info') info;
  @attr('boolean') canEdit;
  @attr('string') feature;
  @attr('string') campaignAnswer;

  @tracked progress;
}
