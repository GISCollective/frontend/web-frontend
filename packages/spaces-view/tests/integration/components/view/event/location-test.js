/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/event/location', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let eventData;
  let featureData;

  hooks.before(function () {
    server = new TestServer();
    eventData = server.testData.storage.addDefaultEvent('1');
    featureData = server.testData.storage.addDefaultFeature();
    server.testData.storage.addDefaultIcon();

    server.testData.storage.addDefaultPicture();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (a) {
    await render(hbs`<View::Event::Location />`);

    a.dom(this.element).hasText('');
  });

  it('renders a text value when set', async function (a) {
    this.set('value', {
      data: {
        source: { model: 'event', id: '1' },
        picture: {
          classes: [
            'border-visibility-yes',
            'border-radius-2',
            'border-color-green-700',
          ],
          proportion: '16:26',
        },
        components: { cover: true, name: true, description: true },
        title: { options: [], color: 'green' },
        description: { options: [], color: '' },
        destination: { slug: '/custom-page/:campaign-id' },
      },
      modelKey: 'event',
    });

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Location @model={{this.model}} @value={{this.value}} />`,
    );

    expect(
      this.element.querySelector('.location-text').textContent.trim(),
    ).to.equal('some location');
    expect(this.element.querySelector('.location-text')).to.have.classes(
      'text-with-options text-green event-location location-text',
    );
  });

  it('renders a card when a feature is set', async function (a) {
    this.set('value', {
      data: {
        source: { model: 'event', id: '1' },
        picture: {
          classes: [
            'border-visibility-yes',
            'border-radius-2',
            'border-color-green-700',
          ],
          proportion: '16:26',
        },
        components: { cover: true, name: true, description: true },
        name: { options: [], color: 'green' },
        description: { options: [], color: '' },
        destination: { slug: '/custom-page/:campaign-id' },
      },
      modelKey: 'event',
    });

    const event = await this.store.findRecord('event', eventData._id);
    event.location = {
      type: 'Feature',
      value: featureData._id,
    };

    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Location @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.card')).to.exist;
    expect(
      this.element.querySelector('.card-title').textContent.trim(),
    ).to.equal('Nomadisch Grün - Local Urban Food');
  });
});
