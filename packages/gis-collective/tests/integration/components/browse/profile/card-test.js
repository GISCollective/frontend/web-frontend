/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | browse/profile/card',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Browse::Profile::Card />`);

    expect(
      this.element.querySelector('.profile-name').textContent.trim()
    ).to.equal('anonymous');
    expect(this.element.querySelector('.profile-role')).not.to.exist;
  });

  it('renders the full name', async function () {
    this.set('profile', {
      id: 'id',
      fullName: 'name',
    });

    await render(hbs`<Browse::Profile::Card @profile={{this.profile}}/>`);

    expect(
      this.element.querySelector('.profile-name').textContent.trim()
    ).to.equal('name');
  });

  it('renders the role', async function () {
    this.set('profile', {
      id: 'id',
      role: 'member',
    });

    await render(hbs`<Browse::Profile::Card @profile={{this.profile}}/>`);

    expect(
      this.element.querySelector('.profile-role').textContent.trim()
    ).to.equal('member');
  });
});
