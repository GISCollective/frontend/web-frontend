/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/view/value', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (a) {
    await render(hbs`<Attributes::View::Value />`);

    a.dom(this.element).hasText('');
    expect(this.element.querySelector('.attributes-view-value')).to.exist;
  });

  it('renders a value when it has no format', async function (a) {
    await render(hbs`<Attributes::View::Value @value="some value"/>`);

    a.dom(this.element).hasText('some value');
  });

  describe('the boolean format', function () {
    it('renders "yes" when it is true', async function (a) {
      await render(hbs`<Attributes::View::Value @value={{true}} @format="boolean" />`);

      expect(this.element.querySelector('.attributes-view-value').textContent.trim()).to.equal('yes');
    });

    it('renders "no" when it is false', async function (a) {
      await render(hbs`<Attributes::View::Value @value={{false}} @format="boolean" />`);

      expect(this.element.querySelector('.attributes-view-value').textContent.trim()).to.equal('no');
    });

    it('renders "unknown" when it is undefined', async function (a) {
      await render(hbs`<Attributes::View::Value @value={{undefined}} @format="boolean" />`);

      expect(this.element.querySelector('.attributes-view-value').textContent.trim()).to.equal('unknown');
    });
  });

  describe('the boolean-required format', function () {
    it('renders "yes" when it is true', async function (a) {
      await render(hbs`<Attributes::View::Value @value={{true}} @format="boolean-required" />`);

      expect(this.element.querySelector('.attributes-view-value').textContent.trim()).to.equal('yes');
    });

    it('renders "no" when it is false', async function (a) {
      await render(hbs`<Attributes::View::Value @value={{false}} @format="boolean-required" />`);

      expect(this.element.querySelector('.attributes-view-value').textContent.trim()).to.equal('no');
    });

    it('renders "unknown" when it is undefined', async function (a) {
      await render(hbs`<Attributes::View::Value @value={{undefined}} @format="boolean-required" />`);

      expect(this.element.querySelector('.attributes-view-value').textContent.trim()).to.equal('unknown');
    });
  });

  describe('the long text value', function () {
    it('renders a blocks value without a format', async function () {
      this.set('value', {
        blocks: [{ type: 'paragraph', data: { text: 'description' } }],
      });

      await render(hbs`<Attributes::View::Value @value={{this.value}}/>`);

      expect(this.element.querySelector('p').textContent.trim()).to.equal('description');
    });

    it('renders a blocks value with a boolean format', async function () {
      this.set('value', {
        blocks: [{ type: 'paragraph', data: { text: 'description' } }],
      });

      await render(hbs`<Attributes::View::Value @value={{this.value}} @format="boolean-required" />`);

      expect(this.element.querySelector('p').textContent.trim()).to.equal('description');
    });

    it('renders a md value with a long text format', async function () {
      this.set('value', 'description');

      await render(hbs`<Attributes::View::Value @value={{this.value}} @format="long text" />`);

      expect(this.element.querySelector('p').textContent.trim()).to.equal('description');
    });
  });
});
