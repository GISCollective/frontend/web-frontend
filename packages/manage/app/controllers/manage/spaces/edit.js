/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { set } from '@ember/object';

export default class ManageSpacesEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service fastboot;

  @tracked savingField = '';
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        route: 'manage.dashboards.space',
        text: this.model.space.name,
        model: this.model.space?.id,
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  @action
  async saveField(key, value) {
    try {
      set(this.model.space, key, value);
      await this.model.space.save();
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.savingField = '';
  }

  @action
  verifyDomain() {
    return this.model.space.verifyDomain();
  }

  @action
  async saveAnalytics(url, id) {
    try {
      this.model.space.matomoUrl = url;
      this.model.space.matomoSiteId = id;

      await this.model.space.save();
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.savingField = '';
  }

  @action
  changeAll(value) {
    this.allTeams = value;
  }
}
