/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitUntil, settled } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/newsletter', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let newsletter;
  let newsletterArticle;
  let receivedArticle;
  let team;
  let unpublished;

  hooks.beforeEach(function () {
    server = new TestServer();
    team = server.testData.create.team();
    team.allowNewsletters = true;

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultIcon();
    let article = server.testData.storage.addDefaultArticle();
    article.canEdit = true;

    newsletter = server.testData.storage.addDefaultNewsletter();
    newsletterArticle = server.testData.storage.addDefaultArticle('newsletter-1');
    newsletterArticle.title = 'newsletter article';
    newsletterArticle.type = 'newsletter-article';
    newsletterArticle.relatedId = newsletter._id;
    newsletterArticle.releaseDate = '2022-02-03T02:33:22Z';
    newsletterArticle.status = 'pending';
    newsletterArticle.canEdit = true;

    server.testData.storage.addStat(`newsletter-subscribers-newsletter-${newsletter._id}`, 20);
    server.testData.storage.addStat(`newsletter-emails-newsletter-${newsletter._id}`, 30);
    server.testData.storage.addStat(`newsletter-open-newsletter-${newsletter._id}`, 40);
    server.testData.storage.addStat(`newsletter-interactions-newsletter-${newsletter._id}`, 50);

    server.get(`/mock-server/articles/_/categories`, () => {
      return [200, { 'Content-Type': 'application/json' }, '{ "categories": ["category 1", "category 2"] }'];
    });

    receivedArticle = null;
    server.put('/mock-server/articles/newsletter-1', (request) => {
      receivedArticle = JSON.parse(request.requestBody);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({ article: newsletterArticle })];
    });

    unpublished = null;
    server.post('/mock-server/articles/newsletter-1/unpublish', (request) => {
      unpublished = true;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });
  });

  hooks.afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('redirects to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2Fnewsletter%2F5ca78e2160780601008f69ff');
  });

  it('shows the dashboard page when the user is authenticated', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);
    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('header').textContent.trim()).to.contain(newsletter.name);
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'my first newsletter']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    ]);
  });

  it('can edit the welcome message', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.btn-edit-welcome');

    expect(currentURL()).to.equal('/manage/articles/5ca78e2160780601008f69e6');
  });

  it('does shows the checkboxes', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);
    expect(this.element.querySelector('.chk-selected')).to.exist;

    await click('.btn-grid');
    expect(this.element.querySelector('.chk-selected')).to.exist;
  });

  it('lists a newsletter article as card when the newsletter has one', async function () {
    newsletter.messages = [
      {
        article: 'newsletter-1',
        status: 'pending',
        deliveryDate: '2022-02-02T00:23:22.000Z',
      },
    ];

    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.btn-grid');

    expect(this.element.querySelector('.newsletter-article-card .title').textContent.trim()).to.equal(
      'newsletter article',
    );

    await click('.newsletter-article-card .btn-open-options');
    await click('.dropdown-item-edit');

    expect(this.element.querySelector('h1')).to.have.textContent('Edit article');
  });

  it('lists a newsletter article as list when the newsletter has one', async function () {
    newsletter.messages = [
      {
        article: 'newsletter-1',
        status: 'pending',
        deliveryDate: '2022-02-02T00:23:22.000Z',
      },
    ];

    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    expect(this.element.querySelector('.row-dashboard-article .record-name').textContent.trim()).to.equal(
      'newsletter article',
    );

    await click('.row-dashboard-article .btn-open-options');

    await click('.dropdown-item-edit');

    expect(this.element.querySelector('h1')).to.have.textContent('Edit article');
  });

  it('can add a new article to the list', async function () {
    newsletter.canEdit = true;
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.btn-add-article');

    expect(currentURL()).to.equal(`/manage/articles/add-message?newsletter=5ca78e2160780601008f69ff`);
  });

  it('can publish a newsletter', async function () {
    newsletterArticle.status = 'draft';
    newsletter.canEdit = true;
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.row-dashboard-article .btn-open-options');
    await click('.dropdown-item-send-to-all');

    await waitUntil(() => receivedArticle);

    expect(receivedArticle.article.status).to.equal(`pending`);
  });

  it('can dismiss publishing a newsletter', async function () {
    const notifications = this.owner.lookup('service:notifications');
    notifications.ask = () => {
      throw new Error('test error');
    };

    newsletterArticle.status = 'draft';
    newsletter.canEdit = true;
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.row-dashboard-article .btn-open-options');
    await click('.dropdown-item-send-to-all');

    expect(receivedArticle).not.to.exist;
  });

  it('can cancel publishing a newsletter', async function () {
    const notifications = this.owner.lookup('service:notifications');
    notifications.ask = () => '';

    newsletterArticle.status = 'pending';
    newsletter.canEdit = true;
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.row-dashboard-article .btn-open-options');
    await click('.dropdown-item-unpublish');

    expect(unpublished).to.equal(true);
  });

  it('can dismiss unpublishing a newsletter', async function () {
    const notifications = this.owner.lookup('service:notifications');
    notifications.ask = () => {
      throw new Error('test error');
    };

    newsletterArticle.status = 'pending';
    newsletter.canEdit = true;
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.row-dashboard-article .btn-open-options');
    await click('.dropdown-item-unpublish');

    expect(unpublished).not.to.exist;
  });

  it('can navigate to the import emails page', async function () {
    newsletterArticle.status = 'pending';
    newsletter.canEdit = true;
    authenticateSession();

    await visit(`/manage/dashboards/newsletter/${newsletter._id}`);

    await click('.btn-import-emails');

    expect(this.element.querySelector('h1')).to.have.textContent('Import emails');
  });
});
