/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { htmlSafe } from '@ember/template';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';

export default class PageColBackgroundImageComponent extends Component {
  @service store;

  @tracked _picture;

  get picture() {
    if (this._picture) {
      return this._picture;
    }

    const key = this.args.value?.modelKey;
    return this.args.model?.[key];
  }

  get elementId() {
    return this.args?.id || `page-col-background-image-${guidFor(this)}`;
  }

  get options() {
    const options = this.args.value.data?.options ?? [];
    return options.join(' ');
  }

  get style() {
    if (this.picture?.get) {
      return htmlSafe(
        `background-image: url('${this.picture.get('picture')}')`,
      );
    }

    return htmlSafe(`background-image: url('${this.picture?.picture}')`);
  }

  @action
  setupRecord(value) {
    this._picture = value;
  }
}
