#!/bin/bash -v

fpm -s dir -t deb \
  -p ogm-frontend-$1-ubuntu-22.deb \
  --verbose \
  --log info \
  -n ogm-frontend \
  -v $1 \
  -m "hello@giscollective.com" \
  --description "GISCollective frontend" \
  ./dist/=/var/www/ogm/
