/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Application from '@ember/application';
import Resolver from 'ember-resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';
import { startSentry } from './sentry';
import Version from './version';
import { Chart, registerables} from 'chart.js';
Chart.register(...registerables);
import * as _set from 'lodash/set';
import {
  TrackedArray,
} from 'tracked-built-ins';
import "./deprecation-workflow";
import { Factories } from 'queries';

if (!Intl) {
  // eslint-disable-next-line no-console
  console.error('Intl is not supported in this environment');
}

import '@formatjs/intl-getcanonicallocales/polyfill';
import '@formatjs/intl-locale/polyfill';

import '@formatjs/intl-relativetimeformat/polyfill';
import '@formatjs/intl-relativetimeformat/locale-data/en'; // Add English data
import '@formatjs/intl-relativetimeformat/locale-data/ro'; // Add Romanian data
import '@formatjs/intl-relativetimeformat/locale-data/fr'; // Add French data
import '@formatjs/intl-relativetimeformat/locale-data/es'; // Add Spanish data

import '@formatjs/intl-pluralrules/polyfill';
import '@formatjs/intl-pluralrules/locale-data/en';
import '@formatjs/intl-pluralrules/locale-data/ro';
import '@formatjs/intl-pluralrules/locale-data/fr';
import '@formatjs/intl-pluralrules/locale-data/es';

import '@formatjs/intl-numberformat/polyfill';
import '@formatjs/intl-numberformat/locale-data/en';
import '@formatjs/intl-numberformat/locale-data/ro';
import '@formatjs/intl-numberformat/locale-data/fr';
import '@formatjs/intl-numberformat/locale-data/es';

export default class App extends Application {
  modulePrefix = config.modulePrefix;
  podModulePrefix = config.podModulePrefix;
  Resolver = Resolver;
}

function log() {
  console.log(...arguments);
}
// eslint-disable-next-line no-console
console.log('web-frontend version:', Version.version || 'unknown');

if (config.sentryDsn && typeof URLSearchParams != "undefined") {
  startSentry(config.sentryDsn, Version.version || 'unknown');
}

// eslint-disable-next-line no-undef
var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : this;

if (!commonjsGlobal.document) {
  commonjsGlobal.document = {
    querySelector: (selector) => {
      log('---> querySelector:', selector);
      return undefined;
    },
    querySelectorAll: (selector) => {
      log('---> querySelectorAll:', selector);
      return [];
    },
    get window() {
      return commonjsGlobal.document;
    },
    get body() {
      return commonjsGlobal.document;
    },
    get documentElement() {
      return commonjsGlobal.document;
    },
    get parentNode() {
      return commonjsGlobal.document;
    },
    closest: (selector) => {
      log('---> closest:', selector);
      return undefined;
    },
    hasAttribute: (selector) => {
      log('---> hasAttribute:', selector);
      return undefined;
    },
    addEventListener: (selector) => {
      log('---> addEventListener:', selector);
      return undefined;
    },
    createElement: (selector) => {
      log('---> createElement:', selector);
      return undefined;
    },
    classList: {
      // eslint-disable-next-line prettier/prettier
      add: () => { },
    },
    children: [],
  };

  if (!commonjsGlobal.AbortController) {
    console.warn("AbortController does not exist. Setting up a mock class.")
    class AbortControllerMock {
      signal = {
        aborted: false
      }
    }

    commonjsGlobal.AbortController = AbortControllerMock;
  }

  if (window && !window.addEventListener) {
    // eslint-disable-next-line prettier/prettier
    window.addEventListener = () => { };
  }

}

if (window.crypto && !window.crypto.randomUUID) {
  // https://stackoverflow.com/a/2117523/2800218
  // LICENSE: https://creativecommons.org/licenses/by-sa/4.0/legalcode
  // eslint-disable-next-line prettier/prettier
  window.crypto.randomUUID = () => "10000000-1000-4000-8000-100000000000".replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}

loadInitializers(App, config.modulePrefix);

Factories.instance = {
  A: (a) => TrackedArray.from(a ?? [])
};