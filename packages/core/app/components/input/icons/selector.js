/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { A } from 'core/lib/array';

export default class InputIconSelectorComponent extends Component {
  elementId = `input-icon-selector-${guidFor(this)}`;
  @tracked filterValue = '';
  @tracked value = [];

  get allIcons() {
    return this.flatten(this.args.icons);
  }

  flatten(group) {
    const icons = [];

    if (group && Array.isArray(group['icons']) && group['icons'].length > 0) {
      group['icons'].forEach((icon) => {
        icons.push(icon);
      });
    }

    if (group && group['categories']) {
      Object.keys(group['categories']).forEach((a) => {
        this.flatten(group['categories'][a]).forEach((icon) => {
          icons.push(icon);
        });
      });
    }

    return icons;
  }

  get hasSearchValue() {
    return this.filterValue.length > 2;
  }

  get hasNoResults() {
    return this.hasSearchValue && this.filteredIcons.length == 0;
  }

  get filteredIcons() {
    if (!this.hasSearchValue) {
      return [];
    }

    const value = this.filterValue.toLowerCase();

    return this.allIcons.filter((a) => a.localName.toLowerCase().indexOf(value) != -1);
  }

  @action
  onSelectIcon(icon) {
    if (!this.args.onSelect) {
      return;
    }

    const newList = this.value.filter((a) => a.id !== icon.id);

    newList.push(icon);

    return this.args.onSelect(newList);
  }

  @action
  onDeselectIcon(icon) {
    console.log('onSelect');
    if (!this.args.onSelect) {
      return;
    }

    const newList = this.value.filter((a) => a.id !== icon.id);

    return this.args.onSelect(newList);
  }

  @action
  async setup() {
    const tmp = (await this.args.value) ?? [];

    this.value = A(tmp.slice());
  }
}
