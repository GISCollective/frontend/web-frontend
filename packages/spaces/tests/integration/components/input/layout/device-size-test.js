/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import click from "@ember/test-helpers/dom/click";

describe("Integration | Component | input/layout/device-size", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the unselected buttons when no value is set", async function () {
    await render(hbs`<Input::Layout::DeviceSize />`);

    const buttons = this.element.querySelectorAll(".btn-unselected");
    expect(buttons).to.have.length(3);
  });

  it("renders the lg button selected when the value is lg", async function () {
    await render(hbs`<Input::Layout::DeviceSize @value="lg"/>`);

    expect(this.element.querySelector(".btn-option-lg")).to.have.class("btn-secondary");

    const buttons = this.element.querySelectorAll(".btn-unselected");
    expect(buttons).to.have.length(2);
  });

  it("renders the md button selected when the value is md", async function () {
    await render(hbs`<Input::Layout::DeviceSize @value="md"/>`);

    expect(this.element.querySelector(".btn-option-md")).to.have.class("btn-secondary");

    const buttons = this.element.querySelectorAll(".btn-unselected");
    expect(buttons).to.have.length(2);
  });

  it("renders the sm button selected when the value is sm", async function () {
    await render(hbs`<Input::Layout::DeviceSize @value="sm"/>`);

    expect(this.element.querySelector(".btn-option-sm")).to.have.class("btn-secondary");

    const buttons = this.element.querySelectorAll(".btn-unselected");
    expect(buttons).to.have.length(2);
  });

  it("triggers onChange when the lg button is clicked", async function () {
    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::Layout::DeviceSize @onChange={{this.change}}/>`);
    await click(".btn-option-lg");

    expect(value).to.equal("lg");
  });

  it("triggers onChange when the md button is clicked", async function () {
    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::Layout::DeviceSize @onChange={{this.change}}/>`);
    await click(".btn-option-md");

    expect(value).to.equal("md");
  });

  it("triggers onChange when the sm button is clicked", async function () {
    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::Layout::DeviceSize @onChange={{this.change}}/>`);
    await click(".btn-option-md");

    expect(value).to.equal("md");
  });

  it("triggers onChange with null when the selected value is clicked", async function () {
    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::Layout::DeviceSize @onChange={{this.change}} @value="md" />`);
    await click(".btn-option-md");

    expect(value).to.equal(null);
  });
});
