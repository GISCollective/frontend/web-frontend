/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { helper } from '@ember/component/helper';

export function positionFormat([value]) {
  return value.coordinates.join(', ');
}

export default helper(positionFormat);
