/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | input/options/panel", function (hooks) {
  setupRenderingTest(hooks);

  it("renders nothing when there is no config", async function () {
    await render(hbs`<Input::Options::Panel />`);
    expect(this.element.textContent.trim()).to.equal("");
  });

  describe("select fields", function () {
    it("renders a select option when the config has an option list", async function () {
      this.set("config", [
        {
          name: "someConfig",
          options: ["option1", "option2"],
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      const options = [...this.element.querySelectorAll(".some-config option")].map(
        (a) => a.attributes.getNamedItem("value")?.value ?? "",
      );

      expect(this.element.querySelector(".some-config").value).to.equal("option1");
      expect(options).to.deep.equal(["option1", "option2"]);

      expect(this.element.querySelector(".label").textContent.trim()).to.equal("some-config");
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          name: "someConfig",
          options: ["option1", "option2"],
        },
      ]);

      this.set("value", {
        someConfig: "option2",
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".some-config").value).to.equal("option2");
    });

    it("updates the values when the config is updated", async function () {
      this.set("config", [
        {
          name: "someConfig",
          options: ["option1", "option2", "option3"],
        },
      ]);

      this.set("value", {
        someConfig: "option2",
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      this.set("config", [
        {
          name: "someConfig",
          options: ["option1", "option2", "option3"],
        },
        {
          name: "otherConfig",
          options: ["option4", "option5", "option6"],
        },
      ]);

      this.set("value", {
        someConfig: "option3",
      });

      expect(this.element.querySelector(".some-config").value).to.equal("option3");
    });

    it("triggers onChange when the select value is updated", async function () {
      this.set("config", [
        {
          name: "someConfig",
          options: ["option1", "option2"],
        },
      ]);

      this.set("value", {
        someConfig: "option2",
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      this.element.querySelector(".some-config").value = "option1";
      await triggerEvent(".some-config", "change");

      expect(value).to.deep.equal({ someConfig: "option1" });
    });
  });

  describe("boolean fields", function () {
    it("renders a boolean option when the config has a boolean type", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "bool",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      const options = [...this.element.querySelectorAll(".some-config option")].map(
        (a) => a.attributes.getNamedItem("value")?.value ?? "",
      );

      expect(this.element.querySelector(".some-config").value).to.equal("no");
      expect(options).to.deep.equal(["no", "yes"]);

      expect(this.element.querySelector(".label").textContent.trim()).to.equal("some-config");
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "bool",
        },
      ]);

      this.set("value", {
        someConfig: true,
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".some-config").value).to.equal("yes");
    });

    it("triggers onChange when the select value is updated", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "bool",
        },
      ]);

      this.set("value", {
        someConfig: false,
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      this.element.querySelector(".some-config").value = "yes";
      await triggerEvent(".some-config", "change");

      expect(value).to.deep.equal({ someConfig: true });
    });
  });

  describe("number fields", function () {
    it("renders a number input when the config has a number type", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "number",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".some-config").value).to.equal("");
      expect(this.element.querySelector(".some-config")).to.have.attribute("type", "number");
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "number",
        },
      ]);

      this.set("value", {
        someConfig: 23,
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".some-config").value).to.equal("23");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "number",
        },
      ]);

      this.set("value", {
        someConfig: 12,
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      await fillIn(".some-config", "22");

      expect(value).to.deep.equal({ someConfig: 22 });
    });
  });

  describe("email fields", function () {
    it("renders a email input when the config has a email type", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "email",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".some-config").value).to.equal("");
      expect(this.element.querySelector(".some-config")).to.have.attribute("type", "email");
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "email",
        },
      ]);

      this.set("value", {
        someConfig: "a@gmail.com",
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".some-config").value).to.equal("a@gmail.com");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          name: "someConfig",
          type: "email",
        },
      ]);

      this.set("value", {
        someConfig: "a@gmail.com",
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      await fillIn(".some-config", "b@gmail.com");

      expect(value).to.deep.equal({ someConfig: "b@gmail.com" });
    });
  });

  describe("color fields", function () {
    it("renders a color selector when the config has a color type", async function () {
      this.set("config", [
        {
          name: "textColor",
          type: "color",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".bs-color-picker")).to.exist;
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          name: "textColor",
          type: "color",
        },
      ]);

      this.set("value", {
        textColor: "red-100",
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".selected-value")).to.have.class("bg-red-100");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          name: "textColor",
          type: "color",
        },
      ]);

      this.set("value", {
        textColor: "red",
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      await click(".bs-color-picker");
      await click(".btn-color-orange");

      expect(value).to.deep.equal({ textColor: "orange" });
    });
  });

  describe("text-style fields", function () {
    it("renders a text style selector when the config has a text-style type", async function () {
      this.set("config", [
        {
          type: "text-style",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);
      expect(this.element.querySelector(".manage-editors-properties-text")).to.exist;
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          type: "text-style",
        },
      ]);

      this.set("value", {
        classes: ["fw-light"],
        color: "red-100",
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".input-sm-fw").value).to.equal("light");
      expect(this.element.querySelector(".selected-value")).to.have.class("bg-red-100");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          type: "text-style",
        },
      ]);

      this.set("value", {
        color: "red",
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      await click(".bs-color-picker");
      await click(".btn-color-orange");

      this.element.querySelector(".input-sm-fw").value = "bold";
      await triggerEvent(".input-sm-fw", "change");

      expect(value).to.deep.equal({ color: "orange", classes: ["fw-bold"], minLines: { lg: 0, md: 0, sm: 0 } });
    });
  });

  describe("button-style fields", function () {
    it("renders a button style selector when the config has a button-style type", async function () {
      this.set("config", [
        {
          type: "button-style",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".manage-editors-properties-button-style-options")).to.exist;
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          type: "button-style",
        },
      ]);

      this.set("value", {
        classes: ["btn-primary"],
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".input-sm-btn").value).to.equal("primary");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          type: "button-style",
        },
      ]);

      this.set("value", {
        color: "red",
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      this.element.querySelector(".input-sm-btn").value = "secondary";
      await triggerEvent(".input-sm-btn", "change");

      expect(value).to.deep.equal({ color: "red", classes: ["btn-secondary"] });
    });
  });

  describe("padding fields", function () {
    it("renders a padding selector when the config has a padding type", async function () {
      this.set("config", [
        {
          type: "padding",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".input-options-paddings")).to.exist;
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          type: "padding",
        },
      ]);

      this.set("value", {
        classes: ["ps-2"],
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".sm-start-padding").value).to.equal("2");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          type: "padding",
        },
      ]);

      this.set("value", {});

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      this.element.querySelector(".sm-start-padding").value = "3";
      await triggerEvent(".sm-start-padding", "change");

      expect(value).to.deep.equal({ classes: ["ps-3"] });
    });
  });

  describe("proportion fields", function () {
    it("renders a padding selector when the config has a padding type", async function () {
      this.set("config", [
        {
          type: "padding",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".input-options-paddings")).to.exist;
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          type: "padding",
        },
      ]);

      this.set("value", {
        classes: ["ps-2"],
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".sm-start-padding").value).to.equal("2");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          type: "padding",
        },
      ]);

      this.set("value", {});

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      this.element.querySelector(".sm-start-padding").value = "3";
      await triggerEvent(".sm-start-padding", "change");

      expect(value).to.deep.equal({ classes: ["ps-3"] });
    });
  });

  describe("width fields", function () {
    it("renders a width selector when the config has a width type", async function () {
      this.set("config", [
        {
          type: "width",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".input-width-col")).to.exist;
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          type: "width",
        },
      ]);

      this.set("value", {
        classes: ["col-2", "col-md-3", "col-lg-4"],
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".responsive-value-sm .input-width-col").value).to.equal("2");

      expect(this.element.querySelector(".responsive-value-md .input-width-col").value).to.equal("3");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          type: "width",
        },
      ]);

      this.set("value", {});

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      this.element.querySelector(".responsive-value-md .input-width-col").value = "3";
      await triggerEvent(".responsive-value-md .input-width-col", "change");

      expect(value).to.deep.equal({ classes: ["col-md-3"], style: { md: {} } });
    });
  });

  describe("px fields", function () {
    it("renders a width selector when the config has a width type", async function () {
      this.set("config", [
        {
          type: "px",
          name: "height",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector(".sm-size-px")).to.exist;
    });

    it("renders the field value when set", async function () {
      this.set("config", [
        {
          type: "px",
          name: "height",
        },
      ]);

      this.set("value", {
        style: {
          sm: { height: "2px" },
          md: { height: "3px" },
          lg: { height: "4px" },
        },
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".sm-size-px .input-px-size").value).to.equal("2");

      expect(this.element.querySelector(".md-size-px .input-px-size").value).to.equal("3");

      expect(this.element.querySelector(".lg-size-px .input-px-size").value).to.equal("4");
    });

    it("triggers onChange when the input value is updated", async function () {
      this.set("config", [
        {
          type: "px",
          name: "height",
        },
      ]);

      this.set("value", {});

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}} />`,
      );

      await fillIn(".sm-size-px .input-px-size", "22");
      await fillIn(".md-size-px .input-px-size", "33");
      await fillIn(".lg-size-px .input-px-size", "44");

      expect(value).to.deep.equal({
        style: {
          sm: { height: "22px" },
          md: { height: "33px" },
          lg: { height: "44px" },
        },
      });
    });
  });

  describe("titles", function () {
    it("renders a title", async function () {
      this.set("config", [
        {
          name: "some title",
          type: "title",
        },
      ]);

      await render(hbs`<Input::Options::Panel @config={{this.config}} />`);

      expect(this.element.querySelector("h5").textContent.trim()).to.equal("some title");
    });
  });

  describe("link", function () {
    it("renders an url", async function (a) {
      this.set("config", [
        {
          name: "destination",
          type: "link",
        },
      ]);

      this.set("value", {
        destination: {
          url: "https://giscollective.com",
        },
      });

      await render(hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} />`);

      expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
        "https://giscollective.com",
      );
    });

    it("can update an url", async function (a) {
      this.set("config", [
        {
          name: "destination",
          type: "link",
        },
      ]);

      this.set("value", {
        destination: {
          url: "https://giscollective.com",
        },
      });

      let value;

      this.set("change", (v) => {
        value = v;
      });

      await render(
        hbs`<Input::Options::Panel @config={{this.config}} @value={{this.value}} @onChange={{this.change}}/>`,
      );

      await selectSearch(".ember-power-select-trigger", "https://greenmap.com");
      await selectChoose(".ember-power-select-trigger", "https://greenmap.com");

      expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
        "https://greenmap.com",
      );

      a.deepEqual(value, {
        destination: {
          url: "https://greenmap.com",
        },
      });
    });
  });
});
