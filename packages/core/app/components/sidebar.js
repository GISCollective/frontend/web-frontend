/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class SidebarComponent extends Component {
  @tracked width;
  @tracked diffWidth = 0;
  @tracked left;

  @tracked isTransitionReady;
  @tracked isRunningTransition;

  @tracked willClose;
  @tracked isMoving;

  get style() {
    let result = `width: ${this.width + this.diffWidth}px;`;

    if (this.left) {
      result += `left: ${this.left}px;`;
    }

    return htmlSafe(result);
  }

  @action
  setupIntro(element) {
    this.element = element;
    const rect = element.getBoundingClientRect();

    this.width = rect.width;
    this.left = rect.width * -1;

    later(() => {
      this.isTransitionReady = true;
      this.left = undefined;
    }, 10);
  }

  @action
  runFinishConditions() {
    this.isTransitionReady = false;
    this.isRunningTransition = false;

    if (this.willClose) {
      later(() => {
        this.args.onClose?.();
        this.willClose = false;
      }, 100);
    }
  }

  @action
  handleClose() {
    this.willClose = true;

    const rect = this.element.getBoundingClientRect();
    this.isTransitionReady = true;

    later(() => {
      this.left = rect.width * -1;

      later(() => {
        if (!this.isRunningTransition) {
          this.runFinishConditions();
        }
      }, 10);
    }, 1);
  }

  @action
  finishTransition(evt) {
    if (evt.target != this.element) {
      return;
    }

    this.runFinishConditions();
  }

  @action
  startTransition() {
    this.args?.onStartTransition?.();
  }

  @action
  runningTransition() {
    this.isRunningTransition = true;
  }

  @action
  setupResizer(element) {
    this.resizer = element;
  }

  @action
  mouseDownHandler(e) {
    this.x = e.clientX;
    this.y = e.clientY;
    this.isMoving = true;

    this.globalMouseUpHandler = () => {
      this.mouseUpHandler();
    };

    this.globalMouseMoveHandler = (e) => {
      this.mouseMoveHandler(e);
    };

    document.querySelector('body').addEventListener('mousemove', this.globalMouseMoveHandler);
    document.querySelector('body').addEventListener('mouseup', this.globalMouseUpHandler);
  }

  @action
  mouseUpHandler() {
    this.isMoving = false;

    document.querySelector('body').removeEventListener('mousemove', this.globalMouseMoveHandler);
    document.querySelector('body').removeEventListener('mouseup', this.globalMouseUpHandler);

    this.width = this.width + this.diffWidth;
    this.diffWidth = 0;
  }

  @action
  mouseMoveHandler(e) {
    if (!this.isMoving) {
      return;
    }

    this.diffWidth = e.clientX - this.x;
  }
}
