'use strict';

module.exports = function (environment) {
  const ENV = {
    modulePrefix: 'embedded',
    environment,
    locationType: 'none',
    rootURL: '/',
    EmberENV: {
      EXTEND_PROTOTYPES: false,
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
  };

  if (environment === 'development') {
    ENV.locationType = 'hash';
    ENV.apiUrl = 'http://192.168.1.190:9091';
    ENV.route = 'map-view';
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.apiSiteSocket = '/mock-server';
    ENV.apiUrl = '/mock-server';
    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    ENV.rootURL = './';
  }

  return ENV;
};
