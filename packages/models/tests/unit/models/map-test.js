/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Model | map', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('map', {});
    expect(model).to.be.ok;
  });

  it('fills in the meta info object', async function () {
    let store = this.owner.lookup('service:store');

    const cover = store.createRecord('picture', {
      _id: '5ca89e37ef1f7e010007f333',
      picture: 'http://some-url.com',
    });

    const squareCover = store.createRecord('picture', {
      _id: '5ca89e37ef1f7e010007f333',
      picture: 'http://some-url.com',
    });

    let model = store.createRecord('map', {
      _id: '5ca89e37ef1f7e010007f54c',
      info: {
        lastChangeOn: '2019-10-12T12:07:27Z',
      },
      area: {
        type: 'Polygon',
        coordinates: [
          [
            [12.74259686812192, 52.23616913266036],
            [13.90100395650536, 52.2158954209184],
            [13.91203640496616, 52.45184646188451],
            [13.70241988421106, 52.84008411114561],
            [12.88601869811225, 52.75336490595498],
            [12.80879155888669, 52.5928133710388],
            [12.74259686812192, 52.23616913266036],
          ],
        ],
      },
      visibility: {},
      name: 'Greenmap Berlin',
      endDate: '2001-01-05T00:00:00Z',
      startDate: '2001-01-05T00:00:00Z',
      cover,
      tagLine: '',
      squareCover,
      canEdit: true,
      description: 'A Greenmap shows locations',
    });
    model.info.lastChangeOn = new Date('2019-10-12T12:07:27Z');

    model.visibility.team = store.createRecord('team', {
      _id: '5ca89e37ef1f7e010007f333',
      name: 'team name',
    });

    const metaInfo = await model.fillMetaInfo({});

    expect(metaInfo).to.deep.equal({
      title: 'Greenmap Berlin',
      description: 'A Greenmap shows locations',
      imgSrc: 'http://some-url.com/lg',
      author: 'team name',
      date: '2019-10-12T12:07:27.000Z',
    });
  });
});
