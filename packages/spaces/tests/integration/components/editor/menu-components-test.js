import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

describe("Integration | Component | editor/menu-components", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the values", async function () {
    this.set("value", {
      showLogo: true,
      showSearch: true,
      showLogin: true,
    });

    await render(hbs`<Editor::MenuComponents @value={{this.value}} />`);

    expect(this.element.querySelector(".show-logo").value).to.equal("yes");
    expect(this.element.querySelector(".show-login").value).to.equal("yes");
    expect(this.element.querySelector(".show-search").value).to.equal("yes");
  });

  it("can change the value", async function (a) {
    this.set("value", {
      showLogo: false,
      showSearch: false,
      showLogin: false,
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::MenuComponents @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".show-logo").value = "yes";
    await triggerEvent(".show-logo", "change");

    a.deepEqual(value, {
      showLogo: true,
      showSearch: false,
      showLogin: false,
    });
  });
});
