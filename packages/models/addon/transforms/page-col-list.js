/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { restoreLegacySource } from 'models/lib/fetch-source';
import { toModelSlug } from 'models/lib/model-slug';
import md5 from 'blueimp-md5';
import { modelToStateKey } from 'models/lib/page-state';
import { A } from '../lib/array';

function extractEntries(value) {
  if (typeof value == 'object' && value !== null) {
    return Object.entries(value)
      .sort((a, b) => a[0].localeCompare(b[0]))
      .filter((a) => a[0] && a[1])
      .map(([a, b]) => `${a}_${extractEntries(b)}`)
      .join('_');
  }

  return `${value}`;
}

export default class PageColListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return A([]);
    }

    const result = A(serialized.map((a) => new PageCol(a)));

    return result;
  }

  serialize(deserialized) {
    if (!Array.isArray(deserialized)) {
      return [];
    }

    return deserialized.map((a) => (a.toJSON ? a.toJSON() : a)).filter((a) => a.type || a.gid);
  }
}

export class PageColData {
  @tracked source;
}

export class PageCol {
  @tracked container = 0;
  @tracked col = 0;
  @tracked row = 0;
  @tracked name = '';
  @tracked cls = '';
  @tracked type;
  @tracked gid;
  @tracked data = new PageColData();
  @tracked record;
  @tracked buckets;

  constructor(serialized) {
    this.fromJson(serialized);
  }

  incrementColNameAfter(container, row, col, step = 1) {
    let pieces = this.nameOrKey.split('.').map((a) => parseInt(a));

    if (pieces[0] != container || pieces[1] != row) {
      return;
    }

    if (pieces[2] < col) {
      return;
    }

    pieces[2] += step;
    pieces = pieces.map((a) => (isNaN(a) ? -1 : a));

    this.name = pieces.join('.');
    this.container = pieces[0];
    this.row = pieces[1];
    this.col = pieces[2];
  }

  incrementRowNameAfter(container, row, step = 1) {
    let pieces = this.nameOrKey.split('.').map((a) => parseInt(a));

    if (pieces[0] != container) {
      return;
    }

    if (pieces[1] < row) {
      return;
    }

    pieces[1] += step;
    pieces = pieces.map((a) => (isNaN(a) ? -1 : a));

    this.name = pieces.join('.');
    this.container = pieces[0];
    this.row = pieces[1];
    this.col = !isNaN(pieces[2]) ? pieces[2] : -1;
  }

  incrementContainerNameAfter(container, step = 1) {
    let pieces = this.nameOrKey.split('.').map((a) => parseInt(a));

    if (pieces[0] < container || isNaN(pieces[0])) {
      return;
    }

    pieces[0] += step;
    pieces = pieces.map((a) => (isNaN(a) ? -1 : a));

    this.name = pieces.join('.');
    this.container = pieces[0];
    this.row = !isNaN(pieces[1]) ? pieces[1] : -1;
    this.col = !isNaN(pieces[2]) ? pieces[2] : -1;
  }

  fromJson(serialized) {
    this.record = undefined;
    this.type = serialized?.type;
    this.gid = serialized?.gid;

    if (serialized?.container) {
      this.container = parseInt(serialized.container);
    } else {
      this.container = 0;
    }

    if (serialized?.row) {
      this.row = parseInt(serialized.row);
    } else {
      this.row = 0;
    }

    if (serialized?.col) {
      this.col = parseInt(serialized.col);
    } else {
      this.col = 0;
    }

    if (serialized?.cls) {
      this.cls = serialized.cls;
    }

    if (serialized?.data) {
      this.data = JSON.parse(JSON.stringify(serialized.data));
    } else {
      this.data = {};
    }

    this.name = serialized?.name || `${this.container}.${this.row}.${this.col}`;
  }

  fillState(state) {
    const parameters = this.data?.source?.parameters;

    if (!parameters) {
      return;
    }

    for (const key in parameters) {
      if (parameters[key].source != 'state') {
        continue;
      }

      const stateName = modelToStateKey[parameters[key].state];

      parameters[key].value = state[stateName];
    }
  }

  get nameOrKey() {
    if (this.name) {
      return this.name;
    }

    return this.key;
  }

  get key() {
    return `${this.container}.${this.row}.${this.col}`;
  }

  get indexKey() {
    return `${this.container}.${this.row}.${this.col}`;
  }

  get postfix() {
    if (this.query.name) {
      return this.query.name;
    }

    return extractEntries(this.query);
  }

  get modelKey() {
    // fix the old property selection behavior
    if (this.type == 'gallery' && this.data?.source?.property) {
      this.data.source.property = undefined;
    }

    if (!this.data) {
      return null;
    }

    if (this.data?.source?.useDefaultModel) {
      return `${this.data?.source?.model}_default`;
    }

    /// fix legacy field
    if (this.data.useSelectedModel) {
      if (!this.data.source) {
        this.data.source = {};
      }

      this.data.source.useSelectedModel = true;
      delete this.data.useSelectedModel;
    }

    if (this.data?.source?.useSelectedModel && !this.data?.source?.property) {
      return 'selectedModel';
    }

    if (this.data?.source?.useSelectedModel && this.data?.source?.property) {
      return `selectedModel_${this.data.source.property}`;
    }

    const model = this.data?.source?.model ?? this.data?.model;
    const id = this.data?.source?.id ?? this.data?.id;

    if (!model) {
      return null;
    }

    if (id) {
      return `${model}_${id}`;
    }

    const debug = false;
    const modelSlug = debug ? toModelSlug(this.postfix) : md5(toModelSlug(this.postfix));

    return `${model}_${modelSlug}`;
  }

  get modelType() {
    return this.data?.source?.model;
  }

  get query() {
    let source = this.data?.source;

    if (this.data?.model) {
      source = restoreLegacySource(this);
    }

    if (Array.isArray(this.data?.source?.ids)) {
      return { ids: this.data?.source?.ids };
    }

    if (source?.groups?.length) {
      return source?.groups[0];
    }

    const query = source ?? this.data?.query ?? {};

    return JSON.parse(JSON.stringify(query));
  }

  fetch(modelData) {
    throw new Error('LEGACY: not implemented');
  }

  toGlobalJSON() {
    return {
      type: this.type,
      data: JSON.parse(JSON.stringify(this.data ?? {})),
    };
  }

  toJSON() {
    const result = {
      container: this.container || 0,
      col: this.col || 0,
      row: this.row || 0,
      name: this.name || '',
      type: this.type,
      data: JSON.parse(JSON.stringify(this.data ?? {})),
    };

    if (typeof this.gid == 'string' && this.gid.trim() != '') {
      result.gid = this.gid;
    }

    return result;
  }
}
