/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { A } from '../lib/array';
import { tracked } from '@glimmer/tracking';

export default class QuestionListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return A();
    }

    return A(serialized.map((a) => new Question(a)));
  }

  serialize(deserialized) {
    return deserialized?.map((a) => (a?.toJSON ? a : new Question(a))).map((a) => a.toJSON()) ?? [];
  }
}

export class Question {
  @tracked question = '';
  @tracked help = '';
  @tracked type = 'short text';
  @tracked name = '';
  @tracked isRequired = false;
  @tracked isVisible = true;
  @tracked options = {};

  constructor(value) {
    const fields = ['question', 'help', 'type', 'name', 'isRequired', 'isVisible', 'options'];

    for (const field of fields) {
      if (value[field] !== undefined) {
        this[field] = value[field];
      }
    }
  }

  toJSON() {
    return {
      question: this.question,
      help: this.help,
      type: this.type,
      name: this.name,
      isRequired: this.isRequired,
      isVisible: this.isVisible,
      options: this.options,
    };
  }
}
