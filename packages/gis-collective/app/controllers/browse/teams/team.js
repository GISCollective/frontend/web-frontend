/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class BrowseMapsController extends Controller {
  @service accessConfig;
  @service notifications;
  @service intl;
  @service router;

  get contentBlocks() {
    const blocks = this.model.team.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return {
      blocks,
    };
  }

  get galleryPageColValue() {
    return {
      data: {
        primaryPhoto: {
          mode: 'same as the list',
          classes: [],
        },
      },
      modelKey: 'team',
    };
  }

  @action
  goToMap(id) {
    this.router.transitionTo('browse.map', id);
  }

  @action
  deleteCampaign(item) {
    this.notifications
      .ask({
        title: this.intl.t(`delete survey`, { size: 1 }),
        question: this.intl.t('delete-confirmation-message', {
          name: item.name,
          size: 1,
        }),
      })
      .then(() => {
        return item.destroyRecord();
      })
      .catch(() => {
        if (!this.isDestroying) this.isDeleting = false;
      });
  }
}
