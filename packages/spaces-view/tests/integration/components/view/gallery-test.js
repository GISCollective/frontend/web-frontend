/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render, waitFor, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import {
  normalizedProportion,
  normalizedWidth,
  toPercentageWidth,
} from 'dummy/components/view/gallery';

describe('Integration | Component | view/gallery', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let storedFeature;
  let storedArticle;
  let picture1Data;

  let picture1;
  let picture2;
  let picture3;

  hooks.before(async function () {
    server = new TestServer();
    storedFeature = server.testData.create.feature('1');
    storedArticle = server.testData.create.article('1');

    picture1Data = server.testData.create.picture('1');
    picture1Data.picture = 'primary-picture';
    picture1Data.name = 'primary picture';
    picture1Data.meta.attributions = 'by author';

    const picture2 = server.testData.create.picture('2');
    const picture3 = server.testData.create.picture('3');

    server.testData.storage.addPicture(picture1Data);
    server.testData.storage.addPicture(picture2);
    server.testData.storage.addPicture(picture3);
    server.testData.storage.addFeature(storedFeature);
    server.testData.storage.addArticle(storedArticle);
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');

    picture1 = await store.findRecord('picture', '1');
    picture2 = await store.findRecord('picture', '2');
    picture3 = await store.findRecord('picture', '3');
  });

  hooks.after(function () {
    server.shutdown();
  });

  describe('for a feature with one picture', function (hooks) {
    let pictures;

    hooks.beforeEach(async function () {
      pictures = [picture1];
    });

    it('shows the picture in full screen when is is selected', async function (a) {
      this.set('value', {
        data: {
          gallery: {
            primaryPhoto: 'same as the list',
          },
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures },
      });

      await render(hbs`
        <View::Gallery @value={{this.value}} @model={{this.model}} />
        <div id='gallery-target'></div>
        <div id='slideshow-target'></div>
        <div id='picture-target'></div>`);

      await click('.page-col-picture');

      expect(this.element.querySelector('#gallery-target .large-picture-list'))
        .not.to.exist;
      expect(this.element.querySelector('#picture-target .picture-container'))
        .to.exist;

      let img;

      await waitUntil(
        () => {
          img = this.element.querySelector('#picture-target img');

          return img?.attributes?.getNamedItem?.('src')?.value;
        },
        { timeout: 5000 },
      );

      expect(img.attributes.getNamedItem('src').value).to.contain(
        'primary-picture/',
      );

      await click('.btn-close-modal');

      await waitUntil(
        () => !this.element.querySelector('#picture-target .page-col-gallery'),
      );

      expect(this.element.querySelector('#picture-target .picture-container'))
        .not.to.exist;
    });
  });

  describe('for a feature with two pictures', function (hooks) {
    let feature;
    let feature_pictures;

    hooks.beforeEach(async function () {
      storedFeature.pictures = ['1', '2'];
      feature = await store.findRecord('feature', '1');
      feature_pictures = [picture1, picture2];
    });

    it('renders all pictures when the primary photo is in `same as the list` mode', async function (a) {
      this.set('value', {
        data: {
          gallery: {
            primaryPhoto: 'same as the list',
          },
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(
        hbs`<View::Gallery @value={{this.value}} @model={{this.model}} />`,
      );

      const pictures = [...this.element.querySelectorAll('.page-col-picture')];

      expect(pictures).to.have.length(2);
      expect(pictures[0]).to.have.classes(
        'picture-with-options size-auto selectable page-col-picture',
      );
      expect(pictures[0].querySelector('img')).to.have.classes(
        'border-visibility-yes rounded-2',
      );
      expect(
        pictures[0].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('primary-picture/');

      expect(pictures[1]).to.have.classes(
        'picture-with-options size-auto selectable page-col-picture',
      );
      expect(pictures[1].querySelector('img')).to.have.classes(
        'border-visibility-yes rounded-2',
      );
      expect(
        pictures[1].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('/test/5d5aa72acac72c010043fb59');
    });

    it('renders all pictures when the primary photo is in `unique` mode', async function () {
      this.set('value', {
        data: {
          gallery: {
            primaryPhoto: 'unique',
          },
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(
        hbs`<View::Gallery @value={{this.value}} @model={{this.model}} />`,
      );

      await waitFor('.loaded', { timeout: 3000 });

      const pictures = [...this.element.querySelectorAll('.page-col-picture')];

      expect(pictures).to.have.length(2);
      expect(pictures[0]).to.have.class('picture-with-options');
      expect(pictures[0].querySelector('img')).to.have.class(
        'border-visibility-yes',
      );
      expect(pictures[0].querySelector('img')).to.have.class('rounded-3');
      expect(
        pictures[0].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('primary-picture');

      expect(pictures[1]).to.have.class('picture-with-options');
      expect(pictures[1].querySelector('img')).to.have.class('rounded-2');
      expect(pictures[1].querySelector('img')).to.have.class(
        'border-visibility-yes',
      );

      expect(
        pictures[1].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('/test/5d5aa72acac72c010043fb59');
    });

    it('renders only the second picture when the primary photo is in `hidden` mode', async function () {
      this.set('value', {
        data: {
          gallery: {
            primaryPhoto: 'hidden',
          },
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(
        hbs`<View::Gallery @value={{this.value}} @model={{this.model}} />`,
      );

      const pictures = [...this.element.querySelectorAll('.page-col-picture')];

      expect(pictures).to.have.length(1);

      expect(pictures[0]).to.have.class('picture-with-options');
      expect(pictures[0]).to.have.class('size-auto');
      expect(pictures[0]).to.have.class('selectable');

      expect(pictures[0].querySelector('img')).to.have.classes(
        'border-visibility-yes rounded-2',
      );
      expect(
        pictures[0].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('/test/5d5aa72acac72c010043fb59');
    });

    it('renders only the primary picture when the list is `hidden`', async function () {
      this.set('value', {
        data: {
          gallery: {
            showPhotoList: false,
            primaryPhoto: 'unique',
          },
          photoList: {
            classes: ['border-visibility-yes', 'rounded-2'],
          },
          primaryPhoto: {
            mode: 'unique',
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(
        hbs`<View::Gallery @value={{this.value}} @model={{this.model}} />`,
      );

      const pictures = [...this.element.querySelectorAll('.page-col-picture')];

      expect(pictures).to.have.length(1);

      expect(pictures[0]).to.have.classes(
        'picture-with-options size-auto selectable page-col-picture',
      );
      expect(pictures[0].querySelector('img')).to.have.classes(
        'border-visibility-yes rounded-3',
      );
      expect(pictures[0].querySelector('img').getAttribute('src')).to.contain(
        'primary-picture',
      );
    });

    it('renders the picture list modal when a picture is clicked', async function () {
      this.set('value', {
        data: {
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            mode: 'hidden',
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(
        hbs`<View::Gallery @value={{this.value}} @model={{this.model}} />
<div id='gallery-target'></div>`,
      );

      expect(this.element.querySelector('#gallery-target .page-col-picture'))
        .not.to.exist;
      await click('.page-col-picture');

      const pictures = [
        ...this.element.querySelectorAll('#gallery-target .page-col-picture'),
      ];

      expect(pictures).to.have.length(2);
      expect(
        pictures[0].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('primary-picture/');
      expect(
        pictures[1].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('/test/5d5aa72acac72c010043fb59');

      await click('#gallery-target .btn-close-modal');

      await waitUntil(
        () => !this.element.querySelector('#gallery-target .btn-close-modal'),
      );
      expect(this.element.querySelector('#gallery-target .page-col-picture'))
        .not.to.exist;
    });

    it('renders the feature title in the picture list modal', async function () {
      this.set('value', {
        data: {
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            mode: 'hidden',
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        useSelectedModel: true,
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures, title: feature.title },
        selectedModel: feature,
      });

      await render(
        hbs`<View::Gallery @value={{this.value}} @model={{this.model}} />
<div id='gallery-target'></div>`,
      );

      await click('.page-col-picture');

      expect(
        this.element.querySelector('.gallery-header .title').textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');

      await click('#gallery-target .btn-close-modal');

      await waitUntil(
        () => !this.element.querySelector('#gallery-target .btn-close-modal'),
      );
    });

    it('renders the picture slideshow when a picture from the list is pressed', async function () {
      this.set('value', {
        data: {
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            mode: 'hidden',
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(hbs`
        <View::Gallery @value={{this.value}} @model={{this.model}} />
        <div id='gallery-target'></div>
        <div id='slideshow-target'></div>`);

      await click('.page-col-picture');
      await click('#gallery-target .picture-with-options');

      const pictures = [
        ...this.element.querySelectorAll('#slideshow-target .page-col-picture'),
      ];

      expect(pictures).to.have.length(2);
      expect(
        pictures[0].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('primary-picture/');
      expect(
        pictures[1].querySelector('img').attributes.getNamedItem('src').value,
      ).to.have.contain('/test/5d5aa72acac72c010043fb59');

      await click('#slideshow-target .btn-close-modal');
      await waitUntil(
        () => !this.element.querySelector('#slideshow-target .btn-close-modal'),
      );

      await click('#gallery-target .btn-close-modal');
      await waitUntil(
        () => !this.element.querySelector('#gallery-target .btn-close-modal'),
      );
    });

    it('renders the slideshow slide corresponding to the selected picture', async function () {
      this.set('value', {
        data: {
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            mode: 'hidden',
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(hbs`
        <View::Gallery @value={{this.value}} @model={{this.model}} />
        <div id='gallery-target'></div>
        <div id='slideshow-target'></div>`);

      await click('.page-col-picture');
      await click('#gallery-target .picture-with-options');

      const slides = [
        ...this.element.querySelectorAll('.swiper-slide.selected'),
      ];

      expect(slides).to.have.length(1);
      expect(
        slides[0].querySelector('img').attributes.getNamedItem('src').value,
      ).to.contain('primary-picture/');

      await click('#slideshow-target .btn-close-modal');
      await waitUntil(
        () => !this.element.querySelector('#slideshow-target .btn-close-modal'),
      );

      await click('#gallery-target .btn-close-modal');
      await waitUntil(
        () => !this.element.querySelector('#gallery-target .btn-close-modal'),
      );
    });

    it('renders the picture names in the slideshow', async function () {
      this.set('value', {
        data: {
          photoList: { classes: ['border-visibility-yes', 'rounded-2'] },
          primaryPhoto: {
            mode: 'hidden',
            classes: ['border-visibility-yes', 'rounded-3'],
          },
        },
        modelKey: 'feature',
      });

      this.set('model', {
        feature: { pictures: feature_pictures },
      });

      await render(hbs`
        <View::Gallery @value={{this.value}} @model={{this.model}} />
        <div id='gallery-target'></div>
        <div id='slideshow-target'></div>`);

      await click('.page-col-picture');
      await click('#gallery-target .picture-with-options');

      expect(
        this.element.querySelector('#slideshow-target .picture-name'),
      ).to.have.textContent('primary picture');
      expect(
        this.element.querySelector('#slideshow-target .picture-attributions'),
      ).to.have.textContent('by author');

      await click('#slideshow-target .btn-close-modal');
      await waitUntil(
        () => !this.element.querySelector('#slideshow-target .btn-close-modal'),
      );

      await click('#gallery-target .btn-close-modal');
      await waitUntil(
        () => !this.element.querySelector('#gallery-target .btn-close-modal'),
      );
    });
  });
});

describe('normalized sizes', function () {
  it('returns 1 when the width equals the height', function (a) {
    const result = normalizedProportion(100, 100);

    a.equal(result, 1);
  });

  it('returns 2 when the width is twice than the height', function (a) {
    const result = normalizedProportion(200, 100);

    a.equal(result, 2);
  });

  describe('normalizedWidth', function () {
    it('returns 200 when the height is 100', function (a) {
      const result = normalizedWidth(200, 100);

      a.equal(result, 200);
    });

    it('returns 400 when the height is 50', function (a) {
      const result = normalizedWidth(200, 50);

      a.equal(result, 400);
    });
  });

  describe('toPercentageWidth', function () {
    it('returns an empty list when the list is empty', function (a) {
      const result = toPercentageWidth([]);

      a.deepEqual(result, []);
    });

    it('returns 100 when there is one value', function (a) {
      const result = toPercentageWidth([234]);

      a.deepEqual(result, [100]);
    });

    it('returns 49.787,49.787 (because we want padding) when there are two equal values', function (a) {
      const result = toPercentageWidth([234, 234]);

      a.deepEqual(result, [49.787, 49.787]);
    });

    it('returns proportional percentage widths when there are not equal values', function (a) {
      const result = toPercentageWidth([110, 210]);

      a.deepEqual(result, [34.161, 65.217]);
    });
  });
});
