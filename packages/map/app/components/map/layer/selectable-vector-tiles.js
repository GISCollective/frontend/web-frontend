/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { action } from '@ember/object';
import config from '../../../config/environment';
import { getCenter } from 'ol/extent';
import { FeatureSelection } from '../../../lib/map/feature-selection';
import { later } from '@ember/runloop';

class Overlay {
  @tracked id;
  @tracked isActive;
  @tracked position;
  @tracked icon;

  constructor(props) {
    this.id = props.id;
    this.isActive = props.isActive;
    this.position = props.position;
    this.icon = props.icon;
    this.feature = props.feature;
  }
}

export default class MapLayerSelectableVectorTilesComponent extends Component {
  @service mapStyles;
  @service store;

  debugTiles = config.debugTiles;

  @tracked overlays = [];
  @tracked featureSelection = new FeatureSelection();
  @tracked isLoading;

  @action
  setupSelectedOverlay(element) {
    this.selectedOverlay = element;
  }

  getPosition(position) {
    return getCenter(position.getGeometry().getExtent());
  }

  get selectedId() {
    return this.args.selectedId;
  }

  addOverlay(feature, isActive) {
    const props = feature.getProperties() ?? {};
    const icon = props.icon;
    const id = props.id || props._id;

    if (isActive) {
      for (const overlay of this.overlays.filter(
        (a) => a.id != id && a.isActive,
      )) {
        overlay.isActive = false;
      }
    }

    for (const overlay of this.overlays.filter(
      (a) => a.id != id && !a.isActive,
    )) {
      overlay.hide?.();
    }

    let overlay = this.overlays.find((a) => a.id == id);

    if (!overlay) {
      overlay = new Overlay({
        id,
        isActive,
        position: this.getPosition(feature),
        icon,
        feature,
      });

      this.overlays = [...this.overlays, overlay];
    } else {
      overlay.isActive = isActive ?? false;
    }

    if (isActive) {
      return this.args.onSelect?.(feature);
    }
  }

  @action
  async querySelectedFeature() {
    const id = this.args.selectedId;

    if (!id) {
      return;
    }

    this.isLoading = true;
    let feature = this.store.peekRecord('feature', id);

    if (!this.selectedFeature) {
      feature = await this.store.findRecord('feature', id);
    }

    const icons = await feature.icons;
    const icon = icons[0]?.id;

    let olFeature = feature.position?.toOlFeature?.('EPSG:3857');
    olFeature.setProperties(
      {
        id: feature.id,
        _id: feature.id,
        icon,
        primaryIcon: icon,
      },
      true,
    );

    await this.addOverlay(olFeature, true);

    this.isLoading = false;
  }

  @action
  hover(feature) {
    if (this.overlays.find((a) => a.feature == feature)) {
      return;
    }

    for (const overlay of this.overlays.filter((a) => !a.isActive)) {
      overlay.hide?.();
    }

    if (!feature) {
      return;
    }

    const properties = feature.getProperties() ?? {};

    if (!properties._id || properties.layer == 'debug') {
      return;
    }

    this.addOverlay(feature);

    return this.args.onHover?.(...arguments);
  }

  @action
  selectFeatureFromGroup(olFeature) {
    this.featureSelection.update(olFeature);

    return this.args.onSelect?.(...arguments);
  }

  @action
  select(feature) {
    later(() => {
      this.featureSelection.update(feature);
    });

    if (!feature) {
      return;
    }

    this.addOverlay(feature, true);
  }

  @action
  removeOverlay(overlay) {
    this.overlays = this.overlays.filter((a) => a.id != overlay.id);
  }
}
