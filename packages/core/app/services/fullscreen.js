/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class FullscreenService extends Service {
  @tracked isEnabled = false;
  @tracked frozenIndex = 0;

  get body() {
    return window.document.body;
  }

  freezeBody() {
    this.body?.classList?.add?.('frozen');
    this.frozenIndex++;
  }

  unfreezeBody() {
    if (this.frozenIndex > 0) {
      this.frozenIndex--;
    }

    if (this.frozenIndex <= 0) {
      this.body?.classList?.remove?.('frozen');
      this.frozenIndex = 0;
    }
  }
}
