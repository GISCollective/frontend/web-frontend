/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | color-palette', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:color-palette');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized.toJSON()).to.deep.equal({
      blue: '#3961d0',
      indigo: '#2a1a68',
      purple: '#663eff',
      pink: '#ff3e76',
      red: '#e50620',
      orange: '#e55b06',
      yellow: '#e5ca06',
      green: '#107353',
      teal: '#5d7388',
      cyan: '#159ea7',
      "customColors": [],
    });
  });

  it('can deserialize an object with colors', function (a) {
    let deserialized = transform.deserialize({
      blue: '#1',
      indigo: '#2',
      purple: '#3',
      pink: '#4',
      red: '#5',
      orange: '#6',
      yellow: '#7',
      green: '#8',
      teal: '#9',
      cyan: '#10',
    });

    a.deepEqual(deserialized.toJSON(), {
      blue: '#1',
      indigo: '#2',
      purple: '#3',
      pink: '#4',
      red: '#5',
      orange: '#6',
      yellow: '#7',
      green: '#8',
      teal: '#9',
      cyan: '#10',
      "customColors": [],
    });
  });
});
