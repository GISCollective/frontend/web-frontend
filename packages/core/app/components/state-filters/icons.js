/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { fromPageState, toPageState } from '../../lib/page-state';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class StateFiltersIconsComponent extends Component {
  @tracked _icon;
  @tracked isPickerVisible;
  @tracked icons;
  @tracked isLoading;

  @service fastboot;
  @service store;
  @service intl;

  get title() {
    return this.args.options?.label ?? this.intl.t('icon-label');
  }

  get state() {
    return fromPageState(this.args.state);
  }

  get icon() {
    if (this._icon) {
      return this._icon;
    }

    if (!this.state.if) {
      return null;
    }

    return this.store.peekRecord('icon', this.state.if);
  }

  get label() {
    return this.icon?.localName;
  }

  get selection() {
    if (!this.icon) {
      return [];
    }

    return [this.icon];
  }

  @action
  handleKeyPress(event) {
    if (event.keyCode == 27) {
      this.close();
    }
  }

  @action
  reset() {
    this._icon = null;

    const newState = toPageState({
      ...this.state,
      if: undefined,
    });

    this.args.onChangeState?.(newState);
  }

  @action
  selectionChanged(selectedIcons) {
    let ifValue;

    if (selectedIcons?.length) {
      const selectedIcon = selectedIcons[selectedIcons.length - 1];
      ifValue = selectedIcon.id ?? selectedIcon._id;
      this.isPickerVisible = false;
      this._icon = selectedIcon;
    }

    if (ifValue == this.state.if) {
      ifValue = undefined;
    }

    const newState = toPageState({
      ...this.state,
      if: ifValue,
    });

    this.args.onChangeState?.(newState);
  }

  get iconsQuery() {
    const query = {
      grupset: true,
    };

    if (this.args.options?.source == 'model') {
      const key = this.args.options?.sourceModelKey;
      const model = this.args.model?.[key];
      const modelName = model?.constructor?.modelName;

      if (modelName) {
        query[modelName] = model.id;
      }
    }

    if (this.args.options?.source == 'icon-set') {
      query['iconSet'] = this.args.options?.['icon-set'];
    }

    return query;
  }

  @action
  close() {
    this.isPickerVisible = false;
  }

  @action
  showPicker() {
    this.isPickerVisible = true;

    later(() => {
      document?.querySelector('.input-icons-filter-name')?.focus();
    });
  }

  @action
  async setup() {
    if (Object.keys(this.iconsQuery).length == 1) {
      return;
    }

    if (!this.icons) {
      this.isLoading = true;
      this.icons = await this.store.adapterFor('icon').group(this.iconsQuery);
      this.isLoading = false;
    }

    if (this.state.if && !this.icon) {
      this._icon = await this.store.findRecord('icon', this.state.if);
    }
  }
}
