/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class AttributesViewGroupComponent extends Component {
  get attributes() {
    return this.args.attributes ?? [];
  }

  toItem(name) {
    const item = this.attributes.find((a) => a.name == name);

    return {
      displayName: name,
      ...item,
      value: this.args.value[name],
    };
  }

  get list() {
    return Object.keys(this.args.value ?? {}).map((a) => this.toItem(a));
  }
}
