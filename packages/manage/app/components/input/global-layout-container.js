/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { LayoutContainer } from '../../transforms/layout-container-list';

export default class InputGlobalLayoutContainerComponent extends Component {
  get value() {
    return this.args.container?.gid;
  }

  get groups() {
    const idList = this.args.space?.layoutContainers?.idList ?? [];

    return [
      {
        groupName: 'Global layouts',
        options: idList.map((name, index) => ({
          name,
          isSelected: name == this.value,
          cls: `global-container-${index}`,
        })),
      },
    ];
  }

  @action
  onChange(componentName) {
    if (!componentName) {
      const copy = new LayoutContainer(this.args.container);
      copy.gid = '';
      return this.args.onChange?.(copy);
    }

    this.args.onChange?.(this.args.space?.layoutContainers?.map?.[componentName]);
  }
}
