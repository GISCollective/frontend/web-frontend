/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';
import { Visibility } from '../../../transforms/visibility';

export default class AddNewsletterRoute extends AuthenticatedRoute {
  modelsToClear = ['newsletter'];

  queryParams = {
    all: { refreshModel: true },
  };

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const teamParams = {};

    if (params.all) {
      teamParams.all = params.all;
    } else {
      teamParams.edit = true;
    }

    const teams = await this.store.query('team', teamParams);

    return hash({
      newsletter: this.store.createRecord('newsletter', {
        name: '',
        info: new ModelInfo(),
        visibility: new Visibility({
          isPublic: false,
          team: null,
        }),
      }),
      teams: teams.toArray(),
    });
  }
}
