/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';
import Service, { service } from '@ember/service';
import { getOwner } from '@ember/application';

export default class HeadDataService extends Service {
  @service router;

  get config() {
    const config = getOwner(this).resolveRegistration('config:environment');
    return config['ember-meta'];
  }

  get currentRouteMeta() {
    return this;
  }

  get routeName() {
    return this.router.currentRouteName;
  }

  @tracked articleTitle;
  @tracked author;
  @tracked canonical;
  @tracked categories;
  @tracked content;
  @tracked date;
  @tracked description;
  @tracked imgSrc;
  @tracked jsonld;
  @tracked keywords;
  @tracked siteName;
  @tracked slug;
  @tracked tags;
  @tracked title;
  @tracked twitterUsername;
  @tracked type;
  @tracked url;
  @tracked redirect;
}
