/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/article-preview', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let article;
  let editableArticle;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');

    const editableArticle = server.testData.create.article('2');
    editableArticle.canEdit = true;
    server.testData.storage.addArticle(editableArticle);
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    const store = this.owner.lookup('service:store');
    article = await store.findRecord('article', '1');
    editableArticle = await store.findRecord('article', '2');
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::ArticlePreview />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the title and the first paragraph', async function () {
    this.set('value', {
      type: 'article',
      data: {
        source: {
          useSelectedModel: false,
          model: 'article',
          id: '1',
        },
      },
      modelKey: 'article_1',
    });

    this.set('model', {
      article_1: article,
    });

    await render(
      hbs`<View::ArticlePreview @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('a')).not.to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'title',
    );

    expect(
      this.element.querySelector('.article-preview').textContent.trim(),
    ).to.contain('some content');
  });

  it('renders the edit button when the article is editable', async function () {
    this.set('value', {
      type: 'article',
      data: {
        source: {
          useSelectedModel: false,
          model: 'article',
          id: '2',
        },
      },
      modelKey: 'article_2',
    });

    this.set('model', {
      article_2: editableArticle,
    });

    await render(
      hbs`<View::ArticlePreview @value={{this.value}} @model={{this.model}} />`,
    );
    expect(this.element.querySelector('a')).to.exist;
  });
});
