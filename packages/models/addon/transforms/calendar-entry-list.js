/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { A } from '../lib/array';
import { DateTime } from 'luxon';

export default class CalendarEntryListTransform extends Transform {
  deserialize(serialized) {
    return A(serialized?.map((a) => new CalendarEntry(a)));
  }

  serialize(deserialized) {
    return deserialized?.map((a) => a.toJSON()) ?? [];
  }
}

export function toDateTime(value, tz) {
  let result;

  if (typeof value == 'string') {
    result = DateTime.fromISO(value);
  }

  if (!result && DateTime.isDateTime(value)) {
    result = value;
  }

  if (!result && typeof value?.getMonth === 'function') {
    result = DateTime.fromJSDate(value);
  }

  if (result && tz) {
    result = result.setZone(tz);
  }

  return result;
}

export function beginSchedule(entries) {
  return entries?.[0]?.begin;
}

export function endSchedule(entries) {
  return entries?.[0]?.intervalEnd;
}

export function scheduleGroup(entries) {
  let indexDate = beginSchedule(entries).startOf('day').plus({ days: -1 });
  const result = [];
  const weeklyEntries = entries.filter((a) => a.repetition == 'weekly');

  for (let i = 0; i < 7; i++) {
    indexDate = indexDate.plus({ days: 1 });
    const list = weeklyEntries.filter(
      (a) => a.begin.day == indexDate.day && a.begin.month == indexDate.month && a.begin.year == indexDate.year,
    );

    result.push({
      name: indexDate.weekdayLong,
      list,
    });
  }

  return result;
}

export class CalendarEntry {
  @tracked begin;
  @tracked end;
  @tracked intervalEnd;
  @tracked repetition = '';
  @tracked timezone = 'UTC';

  constructor(serialized) {
    if (serialized?.timezone) {
      this.timezone = serialized.timezone;
    }

    if (serialized?.begin) {
      this.begin = toDateTime(serialized.begin, this.timezone);
    } else {
      this.begin = toDateTime('0001-01-01T00:00:00Z');
    }

    if (serialized?.end) {
      this.end = toDateTime(serialized.end, this.timezone);
    } else {
      this.end = toDateTime('0001-01-01T00:00:00Z');
    }

    if (serialized?.intervalEnd) {
      this.intervalEnd = toDateTime(serialized.intervalEnd, this.timezone);
    } else {
      this.intervalEnd = toDateTime('0001-01-01T00:00:00Z');
    }

    if (serialized?.repetition) {
      this.repetition = serialized?.repetition;
    }
  }

  toJSON() {
    return {
      begin: toDateTime(this.begin).toUTC().toISO(),
      end: toDateTime(this.end).toUTC().toISO(),
      intervalEnd: toDateTime(this.intervalEnd).toUTC().toISO(),
      repetition: this.repetition,
      timezone: this.timezone,
    };
  }
}
