/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { hasMany, attr } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';
import { DetailedAttributeCategory } from '../lib/attribute-category';
import { generateBlur, idToHash } from '../lib/blur';
import { set } from '@ember/object';

export default class FeatureModel extends Model {
  @attr('string') name;
  @attr description;
  @attr('geo') position;
  @attr('number') issueCount;
  @attr('number') visibility;
  @attr('boolean') isMasked;
  @attr('boolean') canEdit;
  @attr('model-info') info;
  @attr('decorators') decorators;
  @attr('source') source;
  @attr attributes;

  @hasMany('picture', { async: true, inverse: null })
  pictures;

  @hasMany('sound', { async: true, inverse: null })
  sounds;

  @hasMany('icon', { async: true, inverse: null })
  icons;

  @hasMany('map', { async: true, inverse: null })
  maps;

  @hasMany('users', { async: true, inverse: null })
  contributors;

  get geometry() {
    return this.position;
  }

  async getCover() {
    if (this._cover) {
      return this._cover;
    }

    let pictures = await this.pictures;

    if (pictures.toArray) {
      pictures = pictures.toArray();
    }

    if (pictures.length == 0) {
      return;
    }
    this._cover = await pictures[0];

    return this._cover;
  }

  get cover() {
    if (this._cover) {
      return this._cover;
    }

    if (this.pictures.length == 0) {
      return;
    }

    let pictures = this.hasMany('pictures').value()

    if (pictures?.slice) {
      pictures = pictures.slice?.();
    }

    return pictures?.[0];
  }

  get pictureCount() {
    return this.hasMany('pictures').ids().length;
  }

  get iconCount() {
    return this.hasMany('icons').ids().length;
  }

  get cachedIcons() {
    return this.hasMany("icons").value();
  }

  get soundCount() {
    return this.hasMany('sounds').ids().length;
  }

  get hashBg() {
    return generateBlur(idToHash(this.id), 4, 4);
  }

  get isPublished() {
    return this.visibility == 1;
  }

  get isPrivate() {
    return this.visibility == 0;
  }

  get isPending() {
    return this.visibility == -1;
  }

  get niceCreatedOn() {
    if (!this.info || !this.info.createdOn) {
      return 'unknown';
    }

    return new Date(Date.parse(this.info.createdOn)).toDateString();
  }

  get originalAuthor() {
    if (!this.info || !this.info.originalAuthor) {
      return '@unknown';
    }

    return this.info.originalAuthor;
  }

  get hasPictures() {
    return this.pictures.length > 0;
  }

  get displayIconAttributes() {
    const categories = this.hasMany("icons").value()?.map((a) => a.asCategory) ?? [];

    if (!this.attributes) {
      return categories;
    }

    Object.keys(this.attributes)
      .filter((key) => !Array.isArray(this.attributes[key]) && typeof this.attributes[key] != 'object')
      .forEach((key) => {
        if (!this.attributes['other']) {
          this.attributes['other'] = {};
        }

        this.attributes['other'][key] = this.attributes[key];
        delete this.attributes[key];
      });

    Object.keys(this.attributes).forEach((key) => {
      const existingCategories = categories.filter((a) => a.canFill(key));

      if (existingCategories.length == 0) {
        const newCategory = new DetailedAttributeCategory();
        newCategory.displayName = key;
        newCategory.names = [key];
        newCategory.isTable = Array.isArray(this.attributes[key]);
        newCategory.fill(this.attributes[key]);

        categories.push(newCategory);
      } else {
        existingCategories[0].fill(this.attributes[key]);
      }
    });

    return categories;
  }

  get iconAttributes() {
    if (!this.attributes || typeof this.attributes !== 'object') {
      return {};
    }

    if (!this.icons || this.icons.isFulfilled === false) {
      if (this.icons.then) {
        this.icons.then(() => {}).catch(() => {});
      }

      return {};
    }

    const validIconNames = [];
    const nameMatch = {};
    const isArray = {};

    this.icons.forEach((icon) => {
      validIconNames.push(icon.name);
      nameMatch[icon.name] = icon.name;
      isArray[icon.name] = icon.allowMany;

      if (icon && icon.otherNames && icon.otherNames.length) {
        icon.otherNames.forEach((name) => {
          nameMatch[name] = icon.name;
          validIconNames.push(name);
        });
      }
    });

    const attributes = {};

    Object.keys(this.attributes)
      .filter((key) => validIconNames.indexOf(key) != -1)
      .filter((key) => isArray[nameMatch[key]] !== true)
      .forEach((key) => {
        isArray[nameMatch[key]] = Array.isArray(this.attributes[key]);
      });

    Object.keys(this.attributes)
      .filter((key) => validIconNames.indexOf(key) != -1)
      .forEach((key) => {
        const iconName = nameMatch[key];

        if (!attributes[iconName]) {
          attributes[iconName] = isArray[iconName] ? [] : {};
        }

        if (isArray[iconName] && Array.isArray(this.attributes[key])) {
          for(const attr of this.attributes[key]) {
            attributes[iconName].push(attr);
          }
        } else if (isArray[iconName] && !Array.isArray(this.attributes[key])) {
          attributes[iconName].push(this.attributes[key]);
        } else {
          attributes[iconName] = Object.assign({}, attributes[iconName], this.attributes[key]);
        }
      });

    return attributes;
  }

  restore() {
    if (this._original_position) {
      this.position.coordinates.clear();
      this.position.coordinates.push(this._original_position);
      this._original_position = false;
    }

    this.rollbackAttributes();
  }

  get resolvedIcons() {
    return this.hasMany('icons').value();
  }

  get firstIcon() {
    if (this.icons?.isDestroying || this.icons?.isDestroyed) {
      return null;
    }

    const icons = this.hasMany('icons').value() ?? [];

    return icons.find((a) => typeof a.get('image') != 'undefined');
  }

  get firstMap() {
    if(this.maps.isDestroyed) {
      return null;
    }

    const maps = this.hasMany('maps').value();

    if (maps.length == 0) {
      return null;
    }

    return maps?.[0];
  }

  get firstSound() {
    if (this.sounds.length == 0) {
      return null;
    }

    return this.sounds.objectAt(0);
  }

  get isInvalid() {
    const coordinates = this.position.coordinates;

    if (this.maps.length == 0) {
      return true;
    }

    if (!this.name) {
      return true;
    }

    if (!this.description) {
      return true;
    }

    if (this.icons.length == 0) {
      return true;
    }

    if (coordinates[0] == 0 || coordinates[1] == 0) {
      return true;
    }

    return false;
  }

  get iconsByCategory() {
    var result = {};

    this.icons.forEach(function (icon) {
      const category = icon.get('category');
      const subcategory = icon.get('subcategory');

      if (!result[category]) {
        result[category] = {};
      }

      if (!result[category][subcategory]) {
        result[category][subcategory] = [];
      }

      result[category][subcategory].push(icon);
    });

    return result;
  }

  get iconSets() {
    return this.maps.then((maps) => {
      const allSetsId = maps.map((a) => a.iconSets.idList);
      const merged = [].concat.apply([], allSetsId);
      const sets = [...new Set(merged)];

      return sets;
    });
  }

  get title() {
    return this.name;
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  set descriptionContentBlocks(content) {
    this.description = toContentBlocks(content, this.name);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async loadRelations() {
    this._icons = await this.icons;
    const pictures = await this.pictures;
    this._cover = pictures?.[0];
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;

    try {
      const pictures = await this.pictures;
      const cover = pictures.slice()[0];
      metaInfo.imgSrc = cover?.picture;
      if (metaInfo.imgSrc) {
        metaInfo.imgSrc += '/lg';
      }
    } catch (err) {
      console.error(err);
    }

    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }

  setListAttribute(icon, key, value) {
    if (!Array.isArray(value)) {
      return;
    }

    if (this.attributes[icon.name] && !Array.isArray(this.attributes[icon.name])) {
      this.attributes[icon.name] = [this.attributes[icon.name]];
    }

    if (!this.attributes[icon.name]) {
      set(this.attributes, icon.name, []);
    }

    while (this.attributes[icon.name].length < value.length) {
      this.attributes[icon.name].push({});
    }

    this.attributes[icon.name].forEach((item, index) => {
      if (index < value.length) {
        set(this.attributes[icon.name][index], key, value[index]);
      } else {
        delete this.attributes[icon.name][index][key];
      }
    });

    this.attributes[icon.name] = this.attributes[icon.name].filter((a) => Object.keys(a).length > 0);
  }

  setAttribute(icon, key, value) {
    if (icon.allowMany) {
      return this.setListAttribute(...arguments);
    }

    if (!this.attributes[icon.name]) {
      set(this.attributes, icon.name, {});
    }

    set(this.attributes[icon.name], key, value);
  }

  relatedModelByType(type) {
    const maps = this.hasMany("maps").value() ?? [];

    if (type == 'map' && maps.length > 0) {
      return maps[0];
    }

    if (type == 'team' && maps.length > 0) {
      return maps[0].get('visibility').team;
    }
  }

  unpublish() {
    this.visibility = 0;
    return this.save();
  }

  publish() {
    this.visibility = 1;
    return this.save();
  }

  async publishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.publishMany(...arguments);
  }

  async unpublishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.unpublishMany(...arguments);
  }

  pending() {
    this.visibility = -1;
    return this.save();
  }

  async pendingMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.pendingMany(...arguments);
  }

  get availableActions() {
    const result = [];

    if (this.isPublished) {
      result.push('unpublish');
      result.push('pending');
    }

    if (this.isPrivate) {
      result.push('publish');
      result.push('pending');
    }

    if (this.isPending) {
      result.push('unpublish');
      result.push('publish');
    }

    return result;
  }

  deleteMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.deleteMany(...arguments);
  }

  get coverAndPictures() {
    return this.pictures;
  }

  get pictureGallery() {
    let list = this.pictures;

    if (list.toArray) {
      list = list.toArray();
    }

    if (list.slice && list.length) {
      list = list.slice(1);
    }

    return list;
  }

  extraFieldsFor(model) {
    if (model == 'picture') {
      return ['coverAndPictures', 'pictureGallery'];
    }

    return [];
  }
}
