/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { later } from '@ember/runloop';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';
import { DateTime } from 'luxon';

export default class InputDateComponent extends Component {
  @tracked _value;
  @tracked elementId = `icon-set-${guidFor(this)}`;

  @action
  setup(element) {
    this.containerElement = element;
  }

  @action
  onChange(value) {
    const pieces = value.split('-').map((a) => parseInt(a));

    let date = this.value;

    const dateObj = { year: pieces[0], month: pieces[1], day: pieces[2], second: 0, millisecond: 0 };

    if (!this.args.allowTime) {
      dateObj.hour = 12;
      dateObj.minute = 0;
    }

    this._value = date.set(dateObj);

    this.triggerChange();
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    const value = this.args.value;

    if (typeof value == 'string') {
      return DateTime.fromISO(value);
    }

    if (DateTime.isDateTime(value)) {
      return value;
    }

    if (typeof value?.getMonth === 'function' && !isNaN(value?.getTime())) {
      return DateTime.fromJSDate(value);
    }

    return DateTime.fromObject({ year: 1, month: 1, day: 1, hour: 0, minute: 0, second: 0 });
  }

  get valueDate() {
    return `${this.value.year}-${this.value.month}-${this.value.day}`;
  }

  get allowDate() {
    if (typeof this.args.allowDate == 'boolean') {
      return this.args.allowDate;
    }

    return true;
  }

  get beginYear() {
    return 1990;
  }

  get endYear() {
    return 2030;
  }

  get hours() {
    const list = [];

    for (let hour = 0; hour <= 23; hour++) {
      list.push({ hour, selected: this.value.hour == hour });
    }

    return list;
  }

  get minutes() {
    const list = [];

    for (let minute = 0; minute <= 59; minute++) {
      list.push({ minute, selected: this.value.minute == minute });
    }

    return list;
  }

  get isNotSet() {
    return !this.isSet;
  }

  get isSet() {
    if (!this.args.allowUnset) {
      return true;
    }

    if (!this.value) {
      return false;
    }

    return this.value.year > 1;
  }

  set isSet(value) {
    this._value = this.value.set({ year: value ? this.beginYear : 0, month: 1, day: 2 });

    this.triggerChange();
  }

  @action
  triggerChange() {
    if (DateTime.isDateTime(this.args.value)) {
      return this.args.onChange?.(this.value);
    }

    return this.args.onChange?.(this.value.toJSDate());
  }

  @action
  onChangeValue() {
    this._value = undefined;

    const year = this.containerElement.querySelector('.year');
    const month = this.containerElement.querySelector('.month');
    const day = this.containerElement.querySelector('.day');

    if (!year || !month || !day) {
      return;
    }

    year.value = this.value.year;
    month.value = this.value.month;
    day.value = this.value.day;

    if (this.args.allowTime) {
      this.containerElement.querySelector('.hour').value = this.value.hour;
      this.containerElement.querySelector('.minute').value = this.value.minute;
    }
  }

  @action
  updateValue() {
    let hour = this.value.hour;
    let minute = this.value.minute;

    let date = this.value;

    if (this.args.allowTime) {
      hour = parseInt(this.containerElement.querySelector('.hour').value);
      minute = parseInt(this.containerElement.querySelector('.minute').value);
    }

    date = date.set({ hour, minute, second: 0, millisecond: 0 });

    this._value = date;
    this.triggerChange();
  }
}
