/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { dasherize } from "@ember/string";

export default class InputBsColorComponent extends Component {
  _predefinedVariables = [
    "primary",
    "secondary",
    "success",
    "danger",
    "warning",
    "info",
    "light",
    "dark",
    "white",
    "black",
  ];

  colors = ["blue", "indigo", "purple", "pink", "red", "orange", "yellow", "green", "teal", "cyan", "gray"];

  get predefinedVariables() {
    const colors = this.args.space?.colorPalette?.customColors?.map(a => a.name.toLowerCase().replace(/[^0-9a-z_-\s]/gi, '').replace(/\s{2,}/g, ' '))?.map(a => dasherize(a)) ?? [];

    return [...this._predefinedVariables, ...colors];
  }

  get currentColor() {
    if (this.predefinedVariables.indexOf(this.args.value) != -1) {
      return this.args.value;
    }

    if (this.colors.indexOf(this.args.value) != -1) {
      return this.args.value;
    }

    if (this.hasShades) {
      return this.args.value;
    }

    return "transparent";
  }

  get baseColor() {
    if (!this.args.value) {
      return false;
    }

    const value = this.colors.filter((a) => this.args.value.indexOf(a) == 0);

    return value[0] ?? "";
  }

  get hasShades() {
    if (!this.args.value) {
      return false;
    }

    return this.colors.filter((a) => this.args.value.indexOf(a) == 0).length == 1;
  }

  get selectedColor() {
    return this.currentColor;
  }

  get shades() {
    if (!this.hasShades) {
      return null;
    }

    let shades = [];

    for (let i = 1; i <= 9; i++) {
      shades.push(`${this.baseColor}-${i}00`);
    }

    return shades;
  }

  @action
  select(value) {
    if (value == this.currentColor) {
      return this.args.onChange?.("");
    }

    this.args.onChange?.(value);
  }
}
