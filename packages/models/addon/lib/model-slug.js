/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export function toModelSlug(value) {
  if (!value) {
    return '';
  }

  const pieces = value
    .replace(/[^a-z0-9]/gim, ' ')
    .replace(/\s+/g, ' ')
    .split(' ');

  return pieces.filter((a) => a != '').join('_');
}

export function toPagePath(slug) {
  if (typeof slug != 'string') {
    return '';
  }

  if (slug.trim() == '') {
    return '/';
  }

  return `/${slug.split('--').join('/')}`;
}
