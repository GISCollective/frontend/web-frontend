/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';

import { setupTest, it } from 'dummy/tests/helpers';
import { expect } from 'chai';

module('Unit | Transform | calendar entry list', function (hooks) {
  setupTest(hooks);
  let transform;
  let timezone;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:calendar-entry-list');
    timezone = 'EET';
  });

  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('deserializes an undefined to an empty list', function () {
    let deserialized = transform.deserialize(undefined);

    expect(deserialized).to.have.length(0);
  });

  it('deserializes an array with one item', function (a) {
    let deserialized = transform.deserialize([
      {
        begin: '2022-01-01T01:00:00Z',
        end: '2022-01-03T01:00:00Z',
        intervalEnd: '2023-01-03T01:00:00Z',
        repetition: 'yearly',
        timezone,
      },
    ]);

    a.deepEqual(
      deserialized.map((a) => a.toJSON()),
      [
        {
          begin: '2022-01-01T01:00:00.000Z',
          end: '2022-01-03T01:00:00.000Z',
          intervalEnd: '2023-01-03T01:00:00.000Z',
          repetition: 'yearly',
          timezone,
        },
      ],
    );
  });

  it('deserializes an array with one item without timezone', function (a) {
    let deserialized = transform.deserialize([
      {
        begin: '2022-01-01T01:00:00Z',
        end: '2022-01-03T02:00:00Z',
        intervalEnd: '2023-01-03T03:00:00Z',
        repetition: 'yearly',
      },
    ]);

    a.deepEqual(
      deserialized.map((a) => a.toJSON()),
      [
        {
          begin: '2022-01-01T01:00:00.000Z',
          end: '2022-01-03T02:00:00.000Z',
          intervalEnd: '2023-01-03T03:00:00.000Z',
          repetition: 'yearly',
          timezone: 'UTC',
        },
      ],
    );
  });

  it('serializes an array with one item', function (a) {
    let deserialized = transform.deserialize([
      {
        begin: '2022-01-01T01:00:00Z',
        end: '2022-01-03T01:00:00Z',
        intervalEnd: '2023-01-03T01:00:00Z',
        repetition: 'yearly',
        timezone,
      },
    ]);

    a.deepEqual(transform.serialize(deserialized), [
      {
        begin: '2022-01-01T01:00:00.000Z',
        end: '2022-01-03T01:00:00.000Z',
        intervalEnd: '2023-01-03T01:00:00.000Z',
        repetition: 'yearly',
        timezone,
      },
    ]);
  });

  it('serializes an array with one item from a different time zone', function (a) {
    let deserialized = transform.deserialize([
      {
        begin: '2022-01-01T01:00:00+0300',
        end: '2022-01-03T01:00:00+0300',
        intervalEnd: '2023-01-03T01:00:00+0300',
        repetition: 'yearly',
        timezone,
      },
    ]);

    a.deepEqual(transform.serialize(deserialized), [
      {
        begin: '2021-12-31T22:00:00.000Z',
        end: '2022-01-02T22:00:00.000Z',
        intervalEnd: '2023-01-02T22:00:00.000Z',
        repetition: 'yearly',
        timezone,
      },
    ]);
  });
});
