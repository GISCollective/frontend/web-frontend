/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class InputSelectBooleanComponent extends Component {
  values = ['no', 'yes'];

  get value() {
    return this.args?.value ?? false;
  }

  get strValue() {
    if (this.value) {
      return this.values[1];
    }

    return this.values[0];
  }

  @action
  change(newValue) {
    return this.args.onChange?.(newValue == this.values[1]);
  }
}
