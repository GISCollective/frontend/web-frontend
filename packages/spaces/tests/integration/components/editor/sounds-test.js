/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, waitFor, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import waitUntil from "@ember/test-helpers/wait-until";

describe("Integration | Component | editor/sounds", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let sound1;
  let sound2;

  hooks.before(function () {
    server = new TestServer();

    sound1 = server.testData.create.sound("1");
    sound2 = server.testData.create.sound("2");

    server.testData.storage.addSound(sound1);
    server.testData.storage.addSound(sound2);
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the value", async function () {
    this.set("value", [sound1, sound2]);
    await render(hbs`<Editor::Sounds @value={{this.value}} />`);

    expect(this.element.querySelectorAll(".sound-container")).to.have.length(2);
  });

  it("can add a new sound", async function (a) {
    let receivedSound;
    server.post("/mock-server/sounds", (request) => {
      const soundRequest = JSON.parse(request.requestBody);
      soundRequest.sound["_id"] = "5cc8dc1038e882010061545a";

      receivedSound = soundRequest.sound;

      return [200, { "Content-Type": "application/json" }, JSON.stringify(soundRequest)];
    });

    this.set("value", [sound1, sound2]);
    this.set("options", { parent: "feature-id" });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    this.set("feature", {
      id: "feature-id",
    });

    await render(
      hbs`<Editor::Sounds @value={{this.value}} @feature={{this.feature}} @options={{this.options}} @onChange={{this.change}} />`,
    );

    await waitFor(".sound-list-input");

    const blob = server.testData.create.mp3Blob();
    await triggerEvent(".sound-list-input", "change", { files: [blob] });

    await waitUntil(() => value);

    expect(value).to.have.length(3);
    a.deepEqual(receivedSound, {
      name: "coolsound.mp3",
      sound:
        "data:audio/mpeg;base64,DcOBwrHDlcK0woHCicOJwoXCucKRw6TCgcKJwrHDlcKVw4wBUSVQw4gAAAA8AAANBcKxwrDCgcOlwr3DlcOIwoHCscK9w5nClAFRQRTDhAAAADQAAA05wqXCncKhw5HCscK9w43ClcOJw4wBUU1NFAAAADwAAA0xwoXDmcKYw5TDoMK4w4jDpMK4w4TDgMOAAQVBJQwABQXDkAAADcKlwrXChcKdwpTCvcKpw4HClQ==",
      feature: "feature-id",
      campaignAnswer: null,
      _id: "5cc8dc1038e882010061545a",
    });
  });

  it("shows an error when an upload error ocurred", async function () {
    server.post("/mock-server/sounds", () => {
      return [400, { "Content-Type": "application/json" }, JSON.stringify({ errors: [{ message: "some error" }] })];
    });

    this.set("value", [sound1, sound2]);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Sounds @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`);

    await waitFor(".sound-list-input");

    const blob = server.testData.create.mp3Blob();
    await triggerEvent(".sound-list-input", "change", { files: [blob] });

    await waitUntil(() => value);

    expect(value).to.have.length(2);
  });
});
