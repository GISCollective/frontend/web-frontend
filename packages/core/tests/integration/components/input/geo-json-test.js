import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | input/geo-json', function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultGeocoding();

    const polygon = server.testData.create.geocoding();
    polygon._id = '2';
    polygon.name = 'address 2';
    polygon.geometry = {
      type: 'Polygon',
      coordinates: [
        [
          [3.86, 36.03],
          [31.28, 36.03],
          [31.28, 48.92],
          [3.86, 48.92],
          [3.86, 36.03],
        ],
      ],
    };
    server.testData.storage.addGeocoding(polygon);
  });

  hooks.beforeEach(function () {
    const position = this.owner.lookup('service:position');
    position.longitude = 1;
    position.latitude = 2;
    position.status = 'WATCHING';
  });

  it('renders all location types by default', async function (assert) {
    await render(hbs`<Input::GeoJson />`);

    const btns = [...this.element.querySelectorAll('.btn-location-option')];

    expect(btns).to.have.length(4);

    expect(btns[0]).to.have.textContent('use my current location');
    expect(btns[1]).to.have.textContent('choose manually on the map');
    expect(btns[2]).to.have.textContent('by address');
    expect(btns[3]).to.have.textContent('select an existing feature');
  });

  it('allows customizing the position type', async function () {
    this.set('typeOptions', {
      allowGps: true,
      allowManual: true,
      allowAddress: false,
      allowExistingFeature: false,
    });

    await render(hbs`<Input::GeoJson @typeOptions={{this.typeOptions}}/>`);

    const btns = [...this.element.querySelectorAll('.btn-location-option')];

    expect(btns).to.have.length(2);

    expect(btns[0]).to.have.textContent('use my current location');
    expect(btns[1]).to.have.textContent('choose manually on the map');
  });

  it('can use the current location', async function (assert) {
    let value;

    this.set('change', (v, vv) => {
      if (!vv) {
        value = v;
      }
    });

    await render(hbs`<Input::GeoJson @onChange={{this.change}} />`);

    await click('.btn-gps');

    expect(value.toJSON()).to.deep.equal({ type: 'Point', coordinates: [1, 2] });
    expect(this.element.querySelector('.position')).to.have.textContent(`{"type":"Point","coordinates":[1,2]}`);
  });

  it('can select a point from the map', async function (assert) {
    let value;

    this.set('change', (v, vv) => {
      if (!vv) {
        value = v;
      }
    });

    await render(hbs`<Input::GeoJson @onChange={{this.change}} />`);

    await click('.btn-manual');

    await click('.mock-select');

    expect(value.toJSON()).to.deep.equal({ type: 'Point', coordinates: [10, 20] });
    expect(this.element.querySelector('.position')).to.have.textContent(`{"type":"Point","coordinates":[10,20]}`);
  });

  it('can select an address', async function (assert) {
    let value;

    this.set('change', (v, vv) => {
      if (!vv) {
        value = v;
      }
    });

    await render(hbs`<Input::GeoJson @onChange={{this.change}} />`);

    await click('.btn-address');

    await fillIn('.search-address-input', 'some address');

    await click('.btn-search-address');

    expect(value.toJSON()).to.deep.equal({ type: 'Point', coordinates: [17.57, 42.475] });
    expect(this.element.querySelector('.position')).to.have.textContent(
      `{"type":"Point","coordinates":[17.57,42.475]}`,
    );
  });

  it('can select an existing feature', async function (assert) {
    let value;

    this.set('change', (v, vv) => {
      if (!vv) {
        value = v;
      }
    });

    await render(hbs`<Input::GeoJson @onChange={{this.change}} />`);

    await click('.btn-feature');

    await click('.mock-select');

    expect(value.toJSON()).to.deep.equal({ type: 'Point', coordinates: [20, 30] });
  });
});
