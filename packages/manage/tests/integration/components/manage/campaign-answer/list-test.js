/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/campaign-answer/list', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let server;
  let answer;
  let user;
  let answerModel;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.after(async function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    user = server.testData.create.user('5b8a59caef739394031a3f67');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    const profile = server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultUser(false, user._id);

    server.testData.storage.addDefaultCampaign();

    let icon = server.testData.storage.addDefaultIcon();

    answer = server.testData.storage.addDefaultCampaignAnswer();
    answer.icons = [icon._id];
    answer.info.originalAuthor = profile._id;
    answer.info.createdOn = new Date().toISOString();

    server.testData.storage.addDefaultUser(answer.info.originalAuthor);

    answerModel = await this.store.findRecord('campaign-answer', answer._id);
    await answerModel.info.fetch();
  });

  it('renders nothing when there is no answer', async function () {
    await render(hbs`<Manage::CampaignAnswer::List />`);

    expect(this.element.querySelectorAll('.campaign-answer-list-item')).to.have.length(0);
  });

  it('renders an answer when the list has one answer', async function () {
    this.set('answers', { buckets: [[answerModel]] });

    await render(hbs`<Manage::CampaignAnswer::List @answers={{this.answers}}/>`);

    expect(this.element.querySelectorAll('.campaign-answer-list-item')).to.have.length(1);
  });
});
