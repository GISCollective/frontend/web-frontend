/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ApplicationAdapter from './application';

export default class IssueAdapter extends ApplicationAdapter {
  categories(model) {
    const url = this.buildURL('icon-set', model.get('id')) + '/categories';

    return this.ajax(url, 'GET');
  }

  subcategories(model, category) {
    const url = this.buildURL('icon-set', model.get('id')) + '/subcategories?category=' + category;

    return this.ajax(url, 'GET');
  }
}
