/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Acceptance | manage/dashboards/index', function (hooks) {
  setupApplicationTest(hooks, { server: true });
  let deleted;
  let profile;
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    const team = server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultUser();
    profile = server.testData.storage.addDefaultProfile('5b870669796da25424540deb');
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultPreferences();

    team.canEdit = true;
    deleted = false;
    server.delete(`/mock-server/teams/${team._id}`, () => {
      deleted = true;

      return [204, {}, ''];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('redirects to login without an auth token it', async function () {
    invalidateSession();

    await visit('/manage/dashboards');
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards');
  });

  it('does not render a profile alert when it has a picture and name', async function () {
    authenticateSession();

    await visit('/manage/dashboards');

    expect(this.element.querySelector('.alert-profile-fill')).not.to.exist;
  });

  it('renders a profile alert when it does not have a picture', async function () {
    authenticateSession();

    profile.picture = null;
    server.testData.storage.addUserProfile(profile);

    await visit('/manage/dashboards');

    expect(this.element.querySelector('.alert-profile-fill')).to.exist;
  });

  it('renders a profile alert when it does not have a first name', async function () {
    authenticateSession();

    profile.firstName = null;
    server.testData.storage.addUserProfile(profile);

    await visit('/manage/dashboards');

    expect(this.element.querySelector('.alert-profile-fill')).to.exist;
  });

  it('renders a profile alert when it does not have a last name', async function () {
    authenticateSession();

    profile.lastName = null;
    server.testData.storage.addUserProfile(profile);

    await visit('/manage/dashboards');

    expect(this.element.querySelector('.alert-profile-fill')).to.exist;
  });

  it('renders a profile alert when it does not have a bio', async function () {
    authenticateSession();

    profile.bio = null;
    server.testData.storage.addUserProfile(profile);

    await visit('/manage/dashboards');

    expect(this.element.querySelector('.alert-profile-fill')).to.exist;
  });

  it('renders a profile alert when the profile fields have white spaces', async function () {
    authenticateSession();

    profile.firstName = ' \n';
    profile.lastName = '  \n';
    profile.bio = ' \n';
    server.testData.storage.addUserProfile(profile);

    await visit('/manage/dashboards');

    expect(this.element.querySelector('.alert-profile-fill')).to.exist;
  });

  it('shows the dashboard page when the user is authenticated', async function () {
    authenticateSession();

    await visit('/manage/dashboards');

    expect(currentURL()).to.equal(`/manage/dashboards`);
    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('header').textContent.trim()).to.contain('Dashboard');
  });

  it('renders a team card when the user is part of a team', async function () {
    authenticateSession();

    await visit('/manage/dashboards');

    expect(this.element.querySelector('.manage-card-team')).to.exist;
    expect(this.element.querySelector('.manage-card-team .name').textContent.trim()).to.equal('Open Green Map');
  });

  it('opens the team dashboard when the title is clicked', async function () {
    authenticateSession();

    await visit('/manage/dashboards');

    await click('.manage-card-team .btn-main-action');
    expect(currentURL()).to.equal('/manage/dashboards/5ca78e2160780601008f69e6');
  });

  it('can delete a team', async function () {
    const service = this.owner.lookup('service:notifications');
    service.ask = () => 'yes';

    authenticateSession();

    await visit('/manage/dashboards');

    await click('.dropdown-item-delete');

    expect(deleted).to.equal(true);
  });
});
