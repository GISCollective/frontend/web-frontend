/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageEventLocationComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  change(v) {
    this._value = v;
  }

  @action
  cancel() {
    this._value = null;

    return this.args.onCancel();
  }

  @action
  save() {
    return this.args.onSave?.(this.value);
  }
}
