# How To Contribute

## Installation

- `git clone <repository-url>`
- `cd map`
- `yarn`

## Linting

- `yarn run lint`
- `yarn run lint:fix`

## Running tests

- `yarn run test` – Runs the test suite on the current Ember version
- `yarn run test:ember -- --server` – Runs the test suite in "watch mode"
- `yarn run test:ember-compatibility` – Runs the test suite against multiple Ember versions

## Running the dummy application

- `yarn run start`
- Visit the dummy application at [http://localhost:4200](http://localhost:4200).

For more information on using ember-cli, visit [https://cli.emberjs.com/release/](https://cli.emberjs.com/release/).
