/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../../../base/app-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class BrowseMapsMapViewFeatureRoute extends Route {
  @service fullscreen;
  @service mapStyles;

  async model(params) {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    let controller = this.controllerFor('browse.maps.map-view');

    controller.mapStyles.selectedId = params.feature;

    let feature = this.store.peekRecord('feature', params.feature) || (await this.store.findRecord('feature', params.feature));

    return hash({ feature, icons: feature.icons, sounds: feature.sounds, cover: feature.getCover() });
  }

  @action
  loading(transition) {
    if (transition.to.params.feature) {
      return !this.store.peekRecord('feature', transition.to.params.feature);
    }

    return true;
  }

  activate() {
    // eslint-disable-next-line no-console
    console.log('feature active');
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    // eslint-disable-next-line no-console
    console.log('feature inactive');
    this.lastSelected = null;
    this.mapStyles.selectedId = null;
  }
}
