import Service from '@ember/service';
import { next } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class DragSortService extends Service {
  @tracked isDragging = false;
  @tracked isDraggingUp = null;

  @tracked draggedItem = null;
  @tracked group = null;

  @tracked sourceList = null;
  @tracked targetList = null;
  @tracked sourceIndex = null;
  @tracked targetIndex = null;

  @tracked lastDragEnteredList = null;
  @tracked isHorizontal = false;
  @tracked listId;

  // ----- Custom methods -----
  startDragging({ additionalArgs, item, index, items, group, isHorizontal, listId }) {
    this.isDragging = true;
    this.isDraggingUp = false;
    this.draggedItem = item;
    this.group = group;
    this.isHorizontal = isHorizontal;
    this.sourceArgs = additionalArgs;
    this.sourceList = items;
    this.targetArgs = additionalArgs;
    this.targetList = items;
    this.sourceIndex = index;
    this.targetIndex = index;
    this.listId = listId;
  }

  draggingOver({ group, index, items, isDraggingUp }) {
    // Ignore hovers over irrelevant groups
    if (group !== this.group) return;

    // Ignore hovers over irrelevant lists
    if (items !== this.targetList) return;

    this.targetIndex = index;
    this.isDraggingUp = isDraggingUp;
  }

  dragEntering({ group, items, isHorizontal, targetArgs, targetIndex = 0 }) {
    // Ignore entering irrelevant groups
    if (group !== this.group) return;

    // Reset index when entering a new list
    if (items !== this.targetList) {
      next(() => {
        this.trigger('move', {
          group,
          sourceArgs: this.sourceArgs,
          sourceList: this.sourceList,
          sourceIndex: this.sourceIndex,
          draggedItem: this.draggedItem,
          oldTargetList: this.targetList,
          newTargetList: items,
          targetArgs,
          targetIndex: targetIndex,
        });
      });

      this.set('targetArgs', targetArgs);
      this.set('targetIndex', targetIndex);
    }

    // Remember entering a new list
    this.setProperties({
      targetList: items,
      lastDragEnteredList: items,
      isHorizontal: isHorizontal,
    });
  }

  get currentEvent() {
    const sourceArgs = this.sourceArgs;
    const sourceList = this.sourceList;
    const sourceIndex = this.sourceIndex;
    const targetArgs = this.targetArgs;
    const targetList = this.targetList;
    let targetIndex = this.targetIndex;
    const isDraggingUp = this.isDraggingUp;
    const group = this.group;
    const draggedItem = this.draggedItem;

    return {
      group,
      draggedItem,
      sourceArgs,
      sourceList,
      sourceIndex,
      targetArgs,
      targetList,
      targetIndex,
    };
  }

  endDragging({ action }) {
    const sourceArgs = this.sourceArgs;
    const sourceList = this.sourceList;
    const sourceIndex = this.sourceIndex;
    const targetArgs = this.targetArgs;
    const targetList = this.targetList;
    let targetIndex = this.targetIndex;
    const isDraggingUp = this.isDraggingUp;
    const group = this.group;
    const draggedItem = this.draggedItem;

    if (sourceList !== targetList || sourceIndex !== targetIndex) {
      // Account for dragged item shifting indexes by one
      if (sourceList === targetList && targetIndex > sourceIndex) targetIndex--;

      // Account for dragging down
      if (
        // Dragging down
        !isDraggingUp &&
        // Target index is not after the last item
        targetIndex < targetList.length &&
        // The only element in target list is not the one dragged
        !(targetList.length === 1 && targetList[0] === draggedItem)
      )
        targetIndex++;

      if ((sourceList !== targetList || sourceIndex !== targetIndex) && typeof action === 'function') {
        next(() => {
          action({
            group,
            draggedItem,
            sourceArgs,
            sourceList,
            sourceIndex,
            targetArgs,
            targetList,
            targetIndex,
          });
        });
      }
    }

    this._reset();

    next(() => {
      this.trigger('end', {
        group,
        draggedItem,
        sourceArgs,
        sourceList,
        sourceIndex,
        targetArgs,
        targetList,
        targetIndex,
      });
    });
  }

  _reset() {
    this.setProperties({
      isDragging: false,
      isDraggingUp: null,

      draggedItem: null,
      group: null,

      sourceArgs: null,
      sourceList: null,
      targetArgs: null,
      targetList: null,
      sourceIndex: null,
      targetIndex: null,

      lastDragEnteredList: null,
    });
  }
}
