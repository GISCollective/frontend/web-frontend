/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { Popover } from 'bootstrap';

export default class RequiredTooltipComponent extends Component {
  @action
  setup(element) {
    const content = element.querySelector('.content');

    this.popover = new Popover(element, {
      animation: true,
      placement: 'top',
      trigger: 'hover',
      html: true,
      content: content.innerHTML,
    });
  }
}
