/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import config from '../../config/environment';

export default class SlideWelcomeIntroComponent extends Component {
  apiUrl = config.apiUrl;

  get timeout() {
    return this.args.timeout ?? 100;
  }

  @action
  setupAnimation() {
    if (this.args.isCurrent !== undefined && !this.args.isCurrent) {
      return;
    }

    later(() => {
      this.args.onReady?.();
    }, this.timeout);
  }
}
