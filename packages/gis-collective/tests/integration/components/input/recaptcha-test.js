/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';

describe ('Integration | Component | input/recaptcha',  function (hooks) {
  setupRenderingTest(hooks);
});
