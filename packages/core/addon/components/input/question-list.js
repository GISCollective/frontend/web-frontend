/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { later } from '@ember/runloop';
import { GeoJsonGps } from 'core/lib/geoJson';

export default class InputQuestionListComponent extends Component {
  @service user;

  @tracked _value;
  @tracked fields;

  get useCurrentUser() {
    return this.args.useCurrentUser ?? false;
  }

  get email() {
    if (!this.useCurrentUser) {
      return null;
    }

    return this.user.userData?.email;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args?.value ?? {};
  }

  get questions() {
    return this.args.questions?.filter((a) => a.isVisible) ?? [];
  }

  get createFields() {
    return this.questions.filter((a) => a.name?.trim()).map((a) => this.mapQuestion(a)) ?? [];
  }

  mapQuestion(question) {
    const val = question.toJSON ? question.toJSON() : question;

    if (question.type == 'email' && question.name == 'email' && this.email) {
      return {
        ...val,
        disabled: !!this.email,
        value: this.value.email ?? this.email,
      };
    }

    return {
      ...val,
      value: this.value[val.name],
    };
  }

  @action
  validate() {
    const value = this.value;

    for (const question of this.questions.filter((a) => a.isRequired)) {
      const name = question.name;

      if (!value[name]) {
        return this.args.onValidate?.(false);
      }
    }

    return this.args.onValidate?.(true);
  }

  @action
  setup() {
    const emailQuestion = this.questions.find((a) => a.type == 'email' && a.name == 'email');

    if (emailQuestion && this.email) {
      later(() => {
        this.change('email', this.email);
      });
    }

    this.fields = this.createFields;
    this.validate();
  }

  @action
  change(field, value) {
    const tmpValue = !(value instanceof GeoJsonGps) && value?.toJSON ? value.toJSON() : value;
    const currentValue = this.value;

    if (currentValue?.[field] === value) {
      return;
    }

    const newValue = {
      ...currentValue,
      [field]: tmpValue,
    };

    this._value = newValue;
    this.validate();

    return this.args.onChange?.(newValue);
  }
}
