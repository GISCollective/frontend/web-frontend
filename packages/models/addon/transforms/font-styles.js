/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export default class FontStylesTransform {
  deserialize(serialized) {
    return new FontStyles(serialized);
  }

  serialize(deserialized) {
    if (deserialized?.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }

  static create() {
    return new this();
  }
}

export class FontStyles {
  h1 = [];
  h2 = [];
  h3 = [];
  h4 = [];
  h5 = [];
  h6 = [];
  paragraph = [];

  static styles = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'paragraph'];

  constructor(value) {
    if (!value) {
      return;
    }

    for (const style of FontStyles.styles) {
      if (Array.isArray(value[style])) {
        this[style] = value[style];
      }
    }
  }

  toJSON() {
    const result = {};

    for (const style of FontStyles.styles) {
      result[style] = [...(this[style] ?? [])];
    }

    return result;
  }
}
