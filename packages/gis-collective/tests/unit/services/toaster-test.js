/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe('Unit | Service | toaster', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  it('exists', function() {
    let service = this.owner.lookup('service:toaster');
    expect(service).to.be.ok;
  });

  it('can add two toasts', function() {
    let service = this.owner.lookup('service:toaster');

    service.addToast({});
    service.addToast({});

    expect(service.toasts).to.deep.equal([{index:0}, {index:1}]);
  });
});
