/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/campaign-status', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let server;
  let answer;
  let profile;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.after(async function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    const user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    profile = server.testData.storage.addDefaultProfile();
    server.testData.storage.addDefaultUser(false, profile._id);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultCampaign();
    server.testData.storage.addDefaultFeature();

    let map = server.testData.storage.addDefaultMap();
    let icon = server.testData.storage.addDefaultIcon();

    answer = server.testData.storage.addDefaultCampaignAnswer();
    answer.icons = [icon._id];
    answer.info.originalAuthor = profile._id;

    server.testData.storage.addDefaultUser(answer.info.originalAuthor);

    let answerModel = await this.store.findRecord('campaign-answer', answer._id);
    let mapModel = await this.store.findRecord('map', map._id);

    this.set('answer', answerModel);
    this.set('map', mapModel);
  });

  it('renders a pending message for an answer without a feature', async function () {
    this.answer.featureId = '';
    await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

    expect(this.element.querySelector('.status-badge').textContent.trim()).to.equal('pending');
    expect(this.element.querySelector('.status-user').textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('.status-message').textContent.trim()).to.equal('wants to contribute to');
    expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');
  });

  it('renders a processing message', async function () {
    this.answer.status = 0;

    await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

    expect(this.element.querySelector('.status-badge').textContent.trim()).to.equal('processing');
    expect(this.element.querySelector('.status-user').textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('.status-message').textContent.trim()).to.equal('wants to contribute to');
    expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');
  });

  it('renders a published message', async function () {
    this.answer.status = 2;

    await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

    expect(this.element.querySelector('.status-badge').textContent.trim()).to.equal('published');
    expect(this.element.querySelector('.status-user').textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('.status-message').textContent.trim()).to.equal('contributed to');
    expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');
  });

  it('renders a unpublished message', async function () {
    this.answer.status = 3;

    await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

    expect(this.element.querySelector('.status-badge').textContent.trim()).to.equal('unpublished');
    expect(this.element.querySelector('.status-user').textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('.status-message').textContent.trim()).to.equal('contributed to');
    expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');
  });

  it('renders a rejected message', async function () {
    this.answer.status = 4;

    await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

    expect(this.element.querySelector('.status-badge').textContent.trim()).to.equal('rejected');
    expect(this.element.querySelector('.status-user').textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('.status-message').textContent.trim()).to.equal('did not contribute to');
    expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');
  });

  it('renders a contribution message when the status is unknown', async function () {
    this.answer.status = 5;

    await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

    expect(this.element.querySelector('.status-badge').textContent.trim()).to.equal('unknown');
    expect(this.element.querySelector('.status-user').textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('.status-message').textContent.trim()).to.equal('wants to contribute to');
    expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');
  });

  describe('for a campaign with a feature id', function (hooks) {
    let feature;

    hooks.beforeEach(async function () {
      feature = server.testData.storage.addDefaultFeature();
      feature.canEdit = true;

      answer.featureId = feature._id;
      answer.info.originalAuthor = profile._id;

      let answerModel = await this.store.findRecord('campaign-answer', answer._id);
      this.set('answer', answerModel);
    });

    it('renders the feature name link in the message when it can be edited', async function () {
      await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

      expect(this.element.querySelector('.status-feature').textContent.trim()).to.equal(
        'Nomadisch Grün - Local Urban Food',
      );
      expect(this.element.querySelector('.status-feature')).to.have.attribute(
        'href',
        '/manage/features/edit/5ca78e2160780601008f69e6',
      );
      expect(this.element.querySelector('.status-map').textContent.trim()).to.equal('Harta Verde București');
    });

    it('renders the feature name label in the message when it can not be edited', async function () {
      feature.canEdit = false;

      await render(hbs`<Manage::CampaignStatus @value={{this.answer}} @map={{this.map}} />`);

      expect(this.element.querySelector('.status-feature').textContent.trim()).to.equal(
        'Nomadisch Grün - Local Urban Food',
      );
      expect(this.element.querySelector('a.status-feature')).not.to.exist;
    });
  });
});
