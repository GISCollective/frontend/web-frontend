/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, click, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | view/tray',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Tray />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders all the components when all args are set', async function () {
    this.set('value', {
      type: 'tray',
      data: {
        title: 'See what is on the menu',
        paragraph: 'some paragraph',
        link: 'https://a.b',
        'link-title': "More questions? there's an FAQ",
      },
    });

    await render(hbs`<View::Tray @value={{this.value}} />`);

    expect(this.element.querySelector('.title').textContent.trim()).to.equal(
      'See what is on the menu'
    );
    expect(
      this.element.querySelector('.paragraph').textContent.trim()
    ).to.equal('some paragraph');
    expect(this.element.querySelector('.link').textContent.trim()).to.equal(
      `More questions? there's an FAQ`
    );
    expect(this.element.querySelector('.collapse')).not.to.have.class('show');
  });

  it('should expand when the expand button is pressed', async function () {
    this.set('value', {
      type: 'tray',
      data: {
        title: 'See what is on the menu',
        paragraph: 'some paragraph',
        link: 'https://a.b',
        'link-title': "More questions? there's an FAQ",
      },
    });

    await render(hbs`<View::Tray @value={{this.value}} />`);
    await click('.btn-expand');
    await waitFor('.collapse.show');
  });
});
