/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { fromPageState, toPageState } from 'core/lib/page-state';
import { cancel, later } from '@ember/runloop';

export default class StateFiltersCalendarAttributeComponent extends Component {
  @service clickOutside;
  @service store;
  @tracked results;
  @tracked parentRecord;

  get teamId() {
    return this.args?.model?.team?.get?.('id');
  }

  get title() {
    return this.args.options?.label;
  }

  get placeholder() {
    return this.args.options?.['placeholder-text'];
  }

  get hasSearch() {
    return this.args.options?.['with-search'];
  }

  get state() {
    return fromPageState(this.args.state);
  }

  get attrState() {
    const at = this.state.at ?? '';
    return fromPageState(at.replace(/\|/gi, '@'));
  }

  get attributeName() {
    return this.args.options?.['attribute-name'];
  }

  get niceValue() {
    return this.attrState[this.attributeName];
  }

  @action
  search(term) {
    cancel(this.queryTimer);
    if (!term || term.length < 3) {
      return;
    }

    this.queryTimer = later(async () => {
      try {
        this.results = await this.parentRecord?.getAttributeValues(
          this.attributeName,
          term,
          10,
        );
      } catch (err) {
        this.results = [];
      }
    }, 500);
  }

  @action
  select(value) {
    const at = toPageState({
      ...this.attrState,
      [this.attributeName]: value,
    }).replace(/@/gi, '|');

    const newState = toPageState({
      ...this.state,
      at,
    });

    this.args.onChangeState?.(newState);
  }

  @action
  reset() {
    this.select();
  }

  @action
  async setup() {
    const calendarId =
      this.args.options?.calendar?.id || this.args.options?.calendar;

    if (!calendarId) {
      return;
    }

    try {
      this.parentRecord = await this.store.findRecord('calendar', calendarId);
      this.results = await this.parentRecord.getAttributeValues(
        this.attributeName,
        '',
        this.hasSearch ? 5 : 50,
      );
    } catch (err) {
      this.results = [];
    }
  }
}
