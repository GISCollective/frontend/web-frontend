/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { toPageState, fromPageState } from 'core/lib/page-state';

export default class PageColFeatureCampaignContributionButtonComponent extends Component {
  get stateFeature() {
    return fromPageState(this.args.state).fid;
  }

  get modelFeature() {
    const id = this.args.model?.selectedId;
    const type = this.args?.model?.selectedType;

    if (type != 'feature') {
      return '';
    }

    return id;
  }

  get state() {
    if (this.stateFeature) {
      return '?s=' + toPageState({ fid: this.stateFeature });
    }

    if (this.modelFeature) {
      return '?s=' + toPageState({ fid: this.modelFeature });
    }

    return '';
  }

  get link() {
    const id = this.args.value?.data?.source?.id;
    const destination = this.args.value?.data?.button?.destination;

    if (!id || !destination) {
      return null;
    }

    return (
      destination.replace(':campaign_id', id).replace(':campaign-id', id) +
      this.state
    );
  }
}
