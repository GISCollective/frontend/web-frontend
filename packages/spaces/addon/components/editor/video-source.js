import Component from "@glimmer/component";

export default class EditorVideoSource extends Component {
  config = [
    {
      name: "url",
      type: "url",
    },
  ];
}
