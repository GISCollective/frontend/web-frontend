/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { ColorPalette } from 'models/transforms/color-palette';

describe('Integration | Component | manage/color-palette', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders the colors when they are set', async function () {
      this.set('value', new ColorPalette());

      await render(hbs`<Manage::ColorPalette @title="title" @value={{this.value}} />`);

      const names = [...this.element.querySelectorAll('.color-name')].map((a) => a.textContent.trim());
      const values = [...this.element.querySelectorAll('.color-block')].map((a) => a.textContent.trim());

      expect(names).to.deep.equal([
        'blue',
        'indigo',
        'purple',
        'pink',
        'red',
        'orange',
        'yellow',
        'green',
        'teal',
        'cyan',
      ]);
      expect(values).to.deep.equal([
        '#3961d0',
        '#2a1a68',
        '#663eff',
        '#ff3e76',
        '#e50620',
        '#e55b06',
        '#e5ca06',
        '#107353',
        '#5d7388',
        '#159ea7',
      ]);
    });

    it('renders a custom blue color', async function () {
      this.set('value', new ColorPalette({ blue: '#3300cc' }));

      await render(hbs`<Manage::ColorPalette @title="title" @value={{this.value}} />`);

      expect(this.element.querySelector('.color-blue').textContent.trim()).to.equal('#3300cc');
    });

    it('renders a custom color', async function () {
      this.set(
        'value',
        new ColorPalette({
          customColors: [
            {
              darkValue: '#0000cc',
              lightValue: '#cc0033',
              name: 'some name',
            },
          ],
        }),
      );

      await render(hbs`<Manage::ColorPalette @title="title" @value={{this.value}} />`);

      expect(this.element.querySelector('.custom-colors label').textContent.trim()).to.equal('some name');

      const colors = [...this.element.querySelectorAll('.custom-colors .color-block')].map((a) => a.textContent.trim());
      expect(colors).to.deep.equal(['#cc0033', '#0000cc']);
    });
  });

  describe('in edit mode', function (hooks) {
    it('can change the red color', async function () {
      this.set('value', new ColorPalette());

      let value;
      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::ColorPalette @allowUnset={{true}} @title="title" @onSave={{this.save}} @value={{this.value}}
  @editablePanel="title" />`,
      );

      await fillIn('.input-color-red', '#cc0033');

      await click('.btn-submit');

      expect(value.toJSON()).to.deep.equal({
        blue: '#3961d0',
        indigo: '#2a1a68',
        purple: '#663eff',
        pink: '#ff3e76',
        red: '#cc0033',
        orange: '#e55b06',
        yellow: '#e5ca06',
        green: '#107353',
        teal: '#5d7388',
        cyan: '#159ea7',
        customColors: [],
      });
    });

    it('can add a custom color', async function (a) {
      this.set('value', new ColorPalette());

      let value;
      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::ColorPalette @allowUnset={{true}} @title="title" @onSave={{this.save}} @value={{this.value}} @editablePanel="title" />`,
      );

      await click('.btn-add-custom-color');

      await fillIn('.input-color-custom-name', 'some name');
      await fillIn('.form-control-color-light', '#cc0033');
      await fillIn('.form-control-color-dark', '#0000cc');

      await click('.btn-submit');

      a.deepEqual(value.toJSON(), {
        blue: '#3961d0',
        customColors: [
          {
            darkValue: '#0000cc',
            lightValue: '#cc0033',
            name: 'some name',
          },
        ],
        cyan: '#159ea7',
        green: '#107353',
        indigo: '#2a1a68',
        orange: '#e55b06',
        pink: '#ff3e76',
        purple: '#663eff',
        red: '#e50620',
        teal: '#5d7388',
        yellow: '#e5ca06',
      });
    });

    it('can delete a custom color', async function (a) {
      this.set(
        'value',
        new ColorPalette({
          blue: '#3961d0',
          customColors: [
            {
              darkValue: '#0000cc',
              lightValue: '#cc0033',
              name: 'some name',
            },
          ],
          cyan: '#159ea7',
          green: '#107353',
          indigo: '#2a1a68',
          orange: '#e55b06',
          pink: '#ff3e76',
          purple: '#663eff',
          red: '#e50620',
          teal: '#5d7388',
          yellow: '#e5ca06',
        }),
      );

      let value;
      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::ColorPalette @allowUnset={{true}} @title="title" @onSave={{this.save}} @value={{this.value}} @editablePanel="title" />`,
      );

      await click('.btn-remove-custom-color');
      await click('.btn-submit');

      a.deepEqual(value.toJSON(), {
        blue: '#3961d0',
        customColors: [],
        cyan: '#159ea7',
        green: '#107353',
        indigo: '#2a1a68',
        orange: '#e55b06',
        pink: '#ff3e76',
        purple: '#663eff',
        red: '#e50620',
        teal: '#5d7388',
        yellow: '#e5ca06',
      });
    });
  });
});
