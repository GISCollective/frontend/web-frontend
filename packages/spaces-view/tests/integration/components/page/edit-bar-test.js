/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page/edit-bar', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the name, visibility edit button when the page is editable', async function () {
    this.set('value', {
      name: 'page-name',
      canEdit: true,
      visibility: {
        isPublic: false,
      },
    });

    await render(hbs`<Page::EditBar @value={{this.value}} />`);

    expect(this.element.querySelector('.btn-edit-page')).to.exist;
    expect(this.element.querySelector('.fa-eye-slash')).to.exist;
    expect(
      this.element.querySelector('.page-title').textContent.trim(),
    ).to.equal('page-name');
  });

  it('renders the team dashboard button', async function () {
    this.set('value', {
      name: 'page-name',
      canEdit: true,
      visibility: {
        isPublic: false,
        teamId: 'some-team',
      },
    });

    await render(hbs`<Page::EditBar @value={{this.value}} />`);

    expect(this.element.querySelector('.btn-team-dashboard')).to.exist;
  });

  it('does not render eye icon when the page is public', async function () {
    this.set('value', {
      name: 'page-name',
      canEdit: true,
      visibility: {
        isPublic: true,
      },
    });

    await render(hbs`<Page::EditBar @value={{this.value}} />`);

    expect(this.element.querySelector('.fa-eye-slash')).not.to.exist;
  });
});
