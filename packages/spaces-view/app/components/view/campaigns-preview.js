/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PageColCampaignsPreviewComponent extends Component {
  @service store;
  @service intl;
  @tracked team;

  @action
  async setup() {
    if (this.args.value?.data?.query?.team) {
      try {
        this.team = await this.store.findRecord(
          'team',
          this.args.value.data.query.team,
        );
      } catch {
        this.team = '';
      }
    }
  }

  get name() {
    if (this.team?.name) {
      return this.team.name;
    }

    return this.intl.t('all teams');
  }
}
