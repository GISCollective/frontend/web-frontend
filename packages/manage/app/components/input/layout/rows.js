/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { getSizeFromWidth } from '../../../lib/responsive';

export default class InputLayoutRowsComponent extends Component {
  @tracked editingStartEdge;
  @tracked editingEndEdge;
  @tracked selectedCol;
  @tracked width;
  @tracked _rows;

  get rows() {
    if (this._rows) {
      return this._rows;
    }

    return this.args.page?.layoutContainers?.[this.args.containerIndex].rows ?? [];
  }

  get size() {
    return getSizeFromWidth(this.width);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.resizeObserver?.disconnect?.();
  }

  get isCurrentContainer() {
    const selectionId = this.args.selectionId ?? '';
    const pieces = selectionId.split('.').map((a) => parseInt(a));

    if (!pieces.length) {
      return false;
    }

    return pieces[0] == this.args.containerIndex;
  }

  @action
  rowRemove() {
    this._rows = this.args.page?.layoutContainers?.[this.args.containerIndex].rows;
  }

  @action
  setupResizeObserver(element) {
    this.resizeObserver = new ResizeObserver(() => {
      window?.requestAnimationFrame(() => {
        this.width = element.clientWidth;
      });
    });

    this.resizeObserver.observe(element);
  }

  @action
  rowEdit(index) {
    const row = this.rows[index];

    this.args.onSelect?.('row', [this.args.containerIndex, index], row);
  }

  @action
  enableRowEdit(index) {
    this.editingStartEdge = index;
    this.editingEndEdge = index + 1;
    this.selectedCol = -1;
    this.rowEdit(index);
  }

  @action
  addRowBefore(index) {
    this.args.page?.addLayoutRowBefore(this.args.containerIndex, index);
    this.args.page?.addLayout(this.args.containerIndex, index, 0);

    this._rows = this.args.page?.layoutContainers?.[this.args.containerIndex].rows;

    this.args.onRowSelect?.();
  }

  @action
  addRowEnd() {
    const rowIndex = this.rows.length;

    this.args.page?.addLayout(this.args.containerIndex, rowIndex, 0);
    this._rows = this.args.page?.layoutContainers?.[this.args.containerIndex].rows;

    this.args.onRowSelect?.();
  }
}
