import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

describe("Integration | Component | editor/content-picker", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the 'full content' option when there is no value", async function (assert) {
    await render(hbs`<Editor::ContentPicker />`);

    expect(this.element.querySelector(".input-mode").value).to.equal("all");
  });

  it("renders a paragraph value when set", async function (assert) {
    this.set("value", {
      mode: "paragraph",
      paragraphIndex: 2,
    });
    await render(hbs`<Editor::ContentPicker @value={{this.value}}/>`);

    expect(this.element.querySelector(".input-mode").value).to.equal("paragraph");
    expect(this.element.querySelector(".text-paragraph-index").value).to.equal("2");
  });

  it("can set the paragraph mode", async function (assert) {
    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::ContentPicker @onChange={{this.change}} />`);

    const select = this.element.querySelector(".input-mode");
    select.value = "paragraph";
    await triggerEvent(select, "change");

    await fillIn(".text-paragraph-index", "2");

    expect(value).to.deep.equal({
      mode: "paragraph",
      paragraphIndex: "2",
    });
  });
});
