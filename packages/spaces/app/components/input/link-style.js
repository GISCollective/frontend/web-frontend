/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class InputLinkStyleComponent extends Component {
  @tracked _style;
  @tracked _hoverColor;
  @tracked _activeColor;

  triggerChange() {
    this.args.onChange?.({
      ...this.style,
      hoverColor: this.hoverColor,
      activeColor: this.activeColor,
    });
  }

  get style() {
    if (this._style) {
      return this._style;
    }

    return this.args.value ?? {};
  }

  get hoverColor() {
    if (this._hoverColor) {
      return this._hoverColor;
    }

    return this.args.value?.hoverColor;
  }

  get activeColor() {
    if (this._activeColor) {
      return this._activeColor;
    }

    return this.args.value?.activeColor;
  }

  @action
  changeStyle(value) {
    this._style = value;
    this.triggerChange();
  }

  @action
  change(key, value) {
    this[key] = value;
    this.triggerChange();
  }
}
