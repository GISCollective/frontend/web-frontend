/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class AddIssueController extends Controller {
  @service router;
  @service space;

  queryParams = ['next', 'type'];

  get title() {
    if (this.type == 'photo') {
      return 'suggest a photo';
    }

    return 'new issue';
  }

  get featureLink() {
    return this.space.browseFeatureLink(this.model.feature);
  }

  get teamLink() {
    return this.space.browseTeamLink(this.model.team);
  }

  get componentName() {
    if (this.type == 'photo') {
      return 'new-record-form/suggest-a-photo';
    }

    return 'new-record-form/issue';
  }

  @action
  async save() {
    try {
      await this.router.transitionTo(this.next ?? '/');
    } catch (err) {
      console.error(err);
    }
  }
}
