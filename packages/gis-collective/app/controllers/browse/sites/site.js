/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '../../base/map-controller';
import { service } from '@ember/service';

export default class BrowseSitesController extends Controller {
  @service store;
  @service modal;
  @service user;
  @service intl;
  @service fastboot;
  @service accessConfig;
  @service preferences;
  @service router;

  get addPhotoLink() {
    return `/add/issue?feature=${this.model?.feature?.id}&type=${this.type}`;
  }

  get breadcrumbs() {
    const result = [];

    if (this.accessConfig.isMultiProjectMode) {
      result.push({
        route: 'browse',
        text: this.intl.t('browse'),
        capitalize: true,
      });

      result.push({
        route: 'browse.maps',
        text: this.intl.t('maps'),
        capitalize: true,
      });
    }

    if (this.model.map) {
      result.push({
        route: 'browse.sites',
        query: { map: this.model.map.id },
        text: this.model.map.name,
      });
    }

    result.push({
      text: this.model.feature.name,
    });

    return result;
  }

  get attributesPageColValue() {
    return {
      data: {},
      modelKey: 'feature',
    };
  }

  get reportProblemPageColValue() {
    return {
      data: {
        button: {
          label: 'Report a problem',
          issueType: 'generic',
        },
        style: {
          classes: ['btn-link', 'p-0'],
        },
      },
      modelKey: 'feature',
    };
  }

  get suggestPhotoPageColValue() {
    return {
      data: {
        button: {
          label: 'Suggest a photo',
          issueType: 'photo',
        },
        style: {
          classes: ['btn-link', 'p-0'],
        },
      },
      modelKey: 'feature',
    };
  }

  get galleryPageColValue() {
    return {
      data: {
        gallery: {
          primaryPhoto: "hidden",
          showPhotoList: false,
        },
        source: {
          useSelectedModel: true,
          property: "pictures"
        },
        primaryPhoto: {
          isHidden: true
        }
      },
      modelKey: 'selectedModel',
    };
  }

  get lastChangePageCol() {
    return {
      type: 'article/date',
      data: {
        date: {
          color: 'green-700',
          classes: ['text-size-08', 'fw-bold'],
          type: 'created',
        },
        label: { color: '', classes: ['text-size-08'] },
      },
      modelKey: 'feature',
    };
  }

  get profilePageCol() {
    return {
      type: 'profile/card',
      data: {
        source: { useSelectedModel: true },
        border: { classes: ['border-radius-2'] },
        background: { color: 'gray-200' },
        name: {
          color: 'success',
          classes: ['text-size-09', 'fw-bold'],
        },
        description: { color: '', classes: ['text-size-09'] },
      },
      modelKey: 'feature',
    };
  }

  get mapListPageCol() {
    return {
      data: {
        destination: '/browse/sites?map=:map-id',
        components: {
          cardType: 'title-description',
        },
        sourceGroupStyle: {
          color: '',
          classes: ['text-start'],
          accentColor: '',
        },
        source: {
          useSelectedModel: true,
          property: 'maps',
        },
        picture: {
          proportion: '1:1',
          classes: [],
        },
        title: {
          color: 'primary',
          classes: ['text-size-09', 'fw-bold'],
        },
        border: {
          classes: ['border-radius-2'],
        },
        name: {
          color: 'success',
          classes: ['text-size-09', 'fw-bold'],
        },
        columns: {
          containerClasses: [],
          cardClasses: ['col-12'],
        },
        background: {
          color: 'gray-100',
        },
        description: {
          color: 'gray-700',
          classes: ['text-null', 'text-line-clamp-3', 'lh-base', 'text-size-09'],
        },
      },
      type: 'card-list',
      modelKey: 'selectedModel_maps',
    };
  }

  get featurePageColValue() {
    return {
      data: {
        elements: {
          fullscreen: true,
          zoom: true,
          download: true,
          mapLicense: false,
          baseMapLicense: false,
        },
      },
      modelKey: 'feature',
    };
  }

  get iconPageColValue() {
    return {
      data: {},
      modelKey: 'feature',
    };
  }

  get coverPageColValue() {
    return {
      data: {
        gallery: {
          primaryPhoto: 'unique',
          showPhotoList: false,
          showPrimaryPhotoAttributions: true
        },
        source: {
          useSelectedModel: true,
          property: "pictures",
        },
        photoList: {
          isHidden: true,
        },
        primaryPhoto: {
          mode: 'unique',
          hasFullWidth: true,
          attributions: 'below',
          sizing: 'auto',
          classes: ['rounded-3'],
        },
      },
      modelKey: 'selectedModel',
    };
  }

  get iconListPageColValue() {
    return {
      data: {},
      modelKey: 'feature',
    };
  }

  get titlePageColValue() {
    return {
      data: {
        source: { useSelectedModel: true },
      },
      modelKey: 'feature',
    };
  }

  get contentPageColValue() {
    return {
      data: {
        source: { useSelectedModel: true },
      },
      modelKey: 'feature',
    };
  }
}
