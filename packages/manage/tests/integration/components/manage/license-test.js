/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, fillIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/license', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the name and url when they are set', async function () {
    this.set('value', {
      name: 'some license',
      url: 'some url',
    });

    await render(hbs`<Manage::License @isMainValue={{true}} @title="license" @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain('some license');
    expect(this.element.textContent.trim()).to.contain('-');
    expect(this.element.querySelector('a').textContent.trim()).to.contain('some url');
  });

  it('renders the name when url is not set', async function () {
    this.set('value', {
      name: 'some license',
    });

    await render(hbs`<Manage::License @isMainValue={{true}} @title="license" @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain('some license');
    expect(this.element.textContent.trim()).not.to.contain('-');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders the url and "The map has no license." when the name is not set', async function () {
    this.set('value', {
      url: 'some url',
    });

    await render(hbs`<Manage::License @isMainValue={{true}} @title="license" @value={{this.value}} />`);

    expect(this.element.querySelector('.alert').textContent.trim()).to.contain('The map has no license.');
  });

  describe('in edit mode', function (hooks) {
    it('should fill the values in the input fields', async function () {
      this.set('value', {
        name: 'some license',
        url: 'some url',
      });

      await render(
        hbs`<Manage::License @isMainValue={{true}} @editablePanel="license" @title="license" @value={{this.value}} />`,
      );

      expect(this.element.querySelector('.input-license-name').value).to.equal('some license');
      expect(this.element.querySelector('.input-license-url').value).to.equal('some url');
    });

    it('should save the values on click on save field', async function () {
      this.set('value', {
        name: 'some license',
        url: 'some url',
      });

      let savedValue;

      this.set('save', (val) => {
        savedValue = val;
      });

      await render(
        hbs`<Manage::License @onSave={{this.save}} @isMainValue={{true}} @editablePanel="license" @title="license" @value={{this.value}} />`,
      );

      await fillIn('.input-license-name', 'new name');
      await fillIn('.input-license-url', 'new url');
      await click('.btn-submit');

      expect(savedValue).to.deep.equal({
        name: 'new name',
        url: 'new url',
      });
    });
  });
});
