/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';

export default class BrowseController extends Controller {
  @service store;
  @service toaster;
  @service notifications;
  @service intl;
  @service user;
  @service searchStorage;
  @service position;

  @tracked icon = '';
  @tracked author = '';
  @tracked tag = '';
  @tracked area = '';
  @tracked team = '';
  @tracked map = '';
  @tracked search = '';
  @tracked preserveScrollPosition = false;

  @tracked category = '';
  @tracked subcategory = '';
  @tracked iconSet;

  @tracked published = null;
  @tracked pages = 1;
  @tracked viewMode = 'card-deck';

  queryParams = ['search', 'visibility', 'icon', 'area', 'map', 'team', 'author', 'category', 'subcategory', 'iconSet', 'pages', 'tag', 'all', 'viewMode', 'issues'];

  @tracked isSearching = false;
  @tracked all = false;
  @tracked hasPublic;
  @tracked hasPrivate;

  get isEditable() {
    return this.user.id !== false;
  }

  get modelActions() {
    const result = [];

    if (this.hasPrivate) {
      result.push({
        name: this.intl.t('publish'),
        key: 'publish',
        icon: 'eye',
        class: 'btn-secondary',
      });
    }

    if (this.hasPublic) {
      result.push({
        name: this.intl.t('unpublish'),
        key: 'unpublish',
        icon: 'eye-slash',
        class: 'btn-secondary',
      });
    }

    return result;
  }

  @action
  changeViewMode(mode) {
    this.viewMode = mode;
  }

  @action
  findSuggestions(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performSuggestions, term, resolve, reject, 600);
    });
  }

  @action
  changeShowAll(value) {
    this.resetBuckets();
    this.all = value;
  }

  @action
  performSearch(term) {
    this.preserveScrollPosition = true;
    if (term == this.search) {
      return;
    }

    this.searchStorage.add(term);
    this.isSearching = true;
    this.search = term;
  }

  @action
  onSelectArea(value) {
    this.resetBuckets();
    this.area = value;
    this.pages = 1;
  }

  didTransition() {
    this.isSearching = false;
    this.resetBuckets();
  }

  @action
  onAreaSearch(term) {
    return this.store.query('area', { term: term }).then((results) => {
      return results.map((a) => a.get('name'));
    });
  }

  @action
  onSelectCategory(category) {
    this.resetBuckets();
    this.category = category;

    if (!category) {
      this.subcategory = null;
    }
  }

  @action
  onSelectSubcategory(subcategory) {
    this.resetBuckets();
    this.subcategory = subcategory;
  }

  @action
  resetBuckets() {
    this.model?.buckets?.reset?.();
  }

  @action
  changePublished(newValue) {
    this.published = newValue;
  }

  @action
  removeTag() {
    this.tag = null;
  }

  @action
  async deleteRecord(item) {
    try {
      await item.destroyRecord();
    } catch (err) {
      item.rollbackAttributes();
      this.notifications.handleError(err);
    }
  }
}
