/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | manage/site',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Manage::Site />`);
    expect(this.element.textContent.trim()).to.equal('');
  });
});
