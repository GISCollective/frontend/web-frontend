/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { addIconGroupToStore } from '../../../lib/icons';
import { tracked } from '@glimmer/tracking';
import { fromPageState, toPageState } from 'core/lib/page-state';

export default class PageColIconMatrixViewComponent extends Component {
  @service store;
  @tracked icons;
  @tracked isLoading;

  get state() {
    const value = fromPageState(this.args.state);

    if (!value.ic) {
      value.ic = this.defaultCategory;
    }

    if (!value.isc) {
      value.isc = this.defaultSubcategory;
    }

    return value;
  }

  get strState() {
    return toPageState(this.state);
  }

  get defaultCategory() {
    const categoryList = Object.keys(this.icons?.categories ?? {});

    return categoryList[0];
  }

  get defaultSubcategory() {
    const category = this.defaultCategory;
    const categoryList = Object.keys(
      this.icons?.categories?.[category]?.categories ?? {},
    );

    return categoryList[0];
  }

  get selectedIcons() {
    const category = this.state.ic;
    const subcategory = this.state.isc;

    if (!category || !subcategory || !this.icons) {
      return;
    }

    return this.icons.categories[category].categories[subcategory]?.icons ?? [];
  }

  get subcategories() {
    const category = this.state.ic;
    const categoryList = Object.keys(
      this.icons?.categories[category]?.categories ?? {},
    );

    return {
      ...(this.args.value?.data?.subcategories ?? {}),
      defaultCategory: { value: categoryList[0] },
      categoryList,
    };
  }

  get categories() {
    const categoryList = Object.keys(this.icons?.categories ?? {});

    return {
      ...(this.args.value?.data?.categories ?? {}),
      defaultCategory: { value: categoryList[0] },
      categoryList,
    };
  }

  @action
  async setup(record) {
    if (!record) {
      return;
    }

    if (!this.icons) {
      this.isLoading = true;
      this.icons = await this.store
        .adapterFor('icon')
        .group({ iconSet: record.id });
      addIconGroupToStore(this.icons, this.store);
      this.isLoading = false;
    }
  }

  @action
  onChangeCategoryState(state) {
    const newState = fromPageState(state);
    const subcategoryList = Object.keys(
      this.icons?.categories?.[newState.ic]?.categories ?? {},
    );

    newState.isc = subcategoryList[0];

    return this.args.onChangeState(toPageState(newState));
  }
}
