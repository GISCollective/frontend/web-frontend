/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action, get } from '@ember/object';

export default class PageColTitleComponent extends Component {
  get sourcePath() {
    if (this.args.disableSource) {
      return '';
    }
    const pieces = this.path.split('.data.');

    return pieces[0];
  }

  get path() {
    if (!this.args.path) {
      return '.data.style';
    }

    if (this.args.path.includes('.data')) {
      return this.args.path;
    }

    return `${this.args.path}.data.style`;
  }

  get style() {
    if (!this.path) {
      return {};
    }

    const pieces = this.path.split('.data.');

    return get(this.args.value, `data.${pieces[1]}`);
  }

  @action
  unpublish(record) {
    return record.unpublish?.();
  }

  @action
  publish(record) {
    return record.publish?.();
  }
}
