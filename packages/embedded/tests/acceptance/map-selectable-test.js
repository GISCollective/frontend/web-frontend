import { module, test } from 'qunit';
import { visit, currentURL, settled, waitUntil } from '@ember/test-helpers';
import { setupApplicationTest } from 'embedded/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { transform } from 'ol/proj';
import { expect } from 'chai';

module('Acceptance | map-selectable', function (hooks) {
  setupApplicationTest(hooks);
  let service;
  let server;
  let map;
  let feature;
  let lastMessage;

  hooks.beforeEach(function () {
    service = this.owner.lookup('service:embedded-client');

    service.setConfig('route', 'map-selectable');
    service.setConfig('apiUrl', '/mock-server');

    const tiles = this.owner.lookup('service:tiles');
    tiles.url = '/mock-server/tiles';

    server = new TestServer();
    server.get('/mock-server/tiles/:z/:x/:y', (request) => [200, {}, '']);

    server.testData.storage.addDefaultBaseMap();
    map = server.testData.storage.addDefaultMap();
    feature = server.testData.storage.addDefaultFeature();

    lastMessage = null;

    window.ReactNativeWebView = {
      postMessage: (message) => (lastMessage = message),
    };
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  test('visiting / when the route is "map-selectable"', async function (assert) {
    await visit('/');

    assert.strictEqual(currentURL(), '/map-selectable');
  });

  test('setting the value updates the component', async function (assert) {
    await visit('/');

    window.setValue({
      mapId: map._id,
      extent: {
        type: 'Polygon',
        coordinates: [
          [
            [-10.0, -10.0],
            [-10.0, 50.0],
            [50.0, 50.0],
            [50.0, -10.0],
            [-10.0, -10.0],
          ],
        ],
      },
    });

    await settled();

    expect(this.element.querySelector('.map-plain-container')).to.exist;
    expect(
      this.element.querySelector('.d-none.vector-tiles'),
    ).to.have.textContent(
      '/mock-server/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c',
    );

    const olMap = this.element.querySelector('.map').olMap;
    const view = olMap.getView();

    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326').map(
      (a) => Math.round(a * 100) / 100,
    );

    expect(center).to.deep.equal([20, 23.26]);
    expect(view.getZoom()).to.be.closeTo(3, 1);
  });

  test('triggers an event when a feature is selected', async function (a) {
    await visit('/');

    window.setValue({
      mapId: map._id,
    });

    await settled();

    const layer = this.element.querySelector('.d-none.vector-tiles');
    layer.selectFeature({
      getProperties: () => ({
        _id: feature._id,
      }),
    });

    await waitUntil(() => lastMessage);

    a.deepEqual(JSON.parse(lastMessage), {
      name: 'selection',
      args: [[feature._id]],
      messageIndex: 0,
    });
  });
});
