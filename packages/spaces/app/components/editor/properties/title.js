/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { guidFor } from "@ember/object/internals";

export default class ManageEditorsPropertiesTextComponent extends Component {
  elementId = `title-${guidFor(this)}`;

  @tracked _classes;
  @tracked _color;
  @tracked _minLines;
  @tracked _text;
  @tracked _heading;

  get value() {
    return { color: this.color, classes: this.classes, minLines: this.minLines };
  }

  @action
  valueChange(value) {
    if (value.color) {
      this.changeColor(value.color);
    }

    if (value.classes) {
      this.classesChanged(value.classes);
    }

    if (value.minLines) {
      this._minLines = value.minLines;
      this.triggerChange();
    }
  }

  get text() {
    if (typeof this._text == "string") {
      return this._text;
    }

    return this.args.value?.text ?? "";
  }

  set text(value) {
    this._text = value;
    this.triggerChange();
  }

  get color() {
    if (this._color) {
      return this._color;
    }

    return this.args.value?.color ?? "";
  }

  get classes() {
    if (this._classes) {
      return this._classes;
    }

    return this.args.value?.classes ?? [];
  }

  get minLines() {
    if (this._minLines) {
      return this._minLines;
    }

    return this.args.value?.minLines;
  }

  get heading() {
    if (this._heading) {
      return this._heading;
    }

    return this.args.value?.heading ?? 1;
  }

  @action
  headingChanged(value) {
    this._heading = value;
    this.triggerChange();
  }

  @action
  classesChanged(value) {
    this._classes = value;

    this.triggerChange();
  }

  @action
  changeColor(value) {
    this._color = value;

    this.triggerChange();
  }

  triggerChange() {
    const newOptions = {
      color: this.color,
      classes: this.classes,
    };

    if (this.minLines) {
      newOptions.minLines = this.minLines;
    }

    if (this.args.enableTextValue) {
      newOptions.text = this.text;
    }

    if (this.args.isHeading) {
      newOptions.heading = this.heading;
    }

    return this.args.onChange?.(newOptions);
  }
}
