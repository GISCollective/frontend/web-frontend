/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from '../../base/app-route';
import { hash } from 'rsvp';

export default class BrowseTeamsRoute extends AppRoute {
  queryParams = {
    index: {
      refreshModel: false,
      replace: true,
    },
  };

  model(params) {
    return this.store.findRecord('team', params.id).then((team) => {
      return hash({
        team,
        pictures: team.get('pictures'),
      });
    });
  }
}
