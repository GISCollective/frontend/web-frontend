/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('error');

  this.route('add', function () {
    this.route('site');
    this.route('site-success');
    this.route('issue');
  });

  this.route('browse', function () {
    this.route('maps', function () {
      this.route('map-view', { path: ':id/map-view' }, function () {
        this.route('feature', { path: ':feature' });
        this.route('search', { path: 's/:term' });
        this.route('filter-icons');
        this.route('spotlight');
        this.route('icon-set', { path: 'icon-set/:set_id' });
      });
    });

    this.route('sites', function () {
      this.route('site', { path: ':id' });
    });

    this.route('teams', function () {
      this.route('team', { path: ':id' });
      this.route('gallery', { path: ':id/gallery' });
    });

    this.route('icons', function () {
      this.route('set', { path: ':id' });
    });

    this.route('profiles', function () {
      this.route('profile', { path: ':id' });
    });
  });

  this.route('sheet', function () {
    this.route('map', { path: 'map/:id' });
  });

  this.route('campaigns', function () {
    this.route('campaign', { path: ':id' });
    this.route('success', { path: ':id/success' });
  });

  this.route('login', function () {
    this.route('reset');
    this.route('activate');
    this.route('register');
    this.route('register-success');
  });

  this.route('preferences', function () {
    this.route('profile');
    this.route('account');
    this.route('tokens');
    this.route('security');
  });

  this.route('manage', function () {
    this.route('articles', function () {
      this.route('add');
      this.route('add-message');
      this.route('edit', { path: ':id' });
    });

    this.route('surveys', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
      this.route('answers', { path: '/answers/:id' });
    });

    this.route('survey-answers', function () {
      this.route('edit', { path: '/:id' });
    });

    this.route('maps', function () {
      this.route('edit', { path: ':id' });
      this.route('import', { path: ':id/import' });
      this.route('add');
    });

    this.route('features', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
    });

    this.route('teams', function () {
      this.route('edit', { path: ':id' });
      this.route('subscription', { path: '/subscription/:id' });
      this.route('add');
    });

    this.route('spaces', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
    });

    this.route('icons', function () {
      this.route('add', { path: '/add/:set_id' });
      this.route('addset', { path: '/add' });
      this.route('edit', { path: '/edit/:set_id/:id' });
      this.route('editset', { path: '/edit/:id' });
      this.route('translate', { path: '/edit/:set_id/:id/translate' });
    });

    this.route('basemaps', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
    });

    this.route('issues', function () { });

    this.route('pages', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
      this.route('layout', { path: ':id/layout' });
      this.route('content', { path: ':id/content' });
    });

    this.route('presentations', function () {
      this.route('add');
      this.route('edit', { path: ':id' });
      this.route('content', { path: ':id/content' });
    });

    this.route('languages', function () {
      this.route('add');
      this.route('edit', { path: ':id' });
    });

    this.route('dashboards', function () {
      this.route('index', { path: '/' });
      this.route('dashboard', { path: ':id' });
      this.route('newsletter', { path: '/newsletter/:id' });
      this.route('space', { path: '/space/:id' });
      this.route('articles', { path: '/articles/:id' });
      this.route('calendar', { path: '/calendar/:id' });
      this.route('survey', { path: '/survey/:id' });
      this.route('map', { path: '/map/:id' });
      this.route('icon-set', { path: '/icon-set/:id' });
    });

    this.route('databindings', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
    });

    this.route('newsletters', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
      this.route('import', { path: '/import/:id' });
    });

    this.route('newsletter', function () {
      this.route('unsubscribe', { path: ':id/unsubscribe/:email_id' });
    });

    this.route('calendars', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
    });

    this.route('events', function () {
      this.route('add');
      this.route('edit', { path: '/edit/:id' });
    });
  });

  this.route('admin', function () {
    this.route('appearance');
    this.route('registration');

    this.route('users', function () {
      this.route('edit', { path: ':id' });
    });
    this.route('smtp');
    this.route('captcha');
    this.route('access');
    this.route('location-services');
    this.route('integrations');
  });

  this.route('page', { path: '/*wildcard' });

  this.route('wizard', function () {
    this.route('import', function () {
      this.route('file-select', { path: '/file/:id/select' });
      this.route('file-fields', { path: '/file/:id/fields' });
      this.route('file-preview', { path: '/file/:id/preview' });
      this.route('file-log', { path: '/file/:id/log' });
    });
  });

  this.route('presentation', function () {
    this.route('index', { path: '/:name' });
  });

  this.route('link-redirect');
});
