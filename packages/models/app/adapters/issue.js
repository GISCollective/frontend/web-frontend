/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ApplicationAdapter from './application';

export default class IssueAdapter extends ApplicationAdapter {
  resolve(model) {
    const url = this.buildURL('issue', model.get('id')) + '/resolve';
    return this.ajax(url, 'GET');
  }

  reject(model) {
    const url = this.buildURL('issue', model.get('id')) + '/reject';
    return this.ajax(url, 'GET');
  }
}
