/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageMinMaxZoomComponent extends Component {
  elementId = `manage-min-max-zoom-${guidFor(this)}`;
  @service intl;

  @tracked _minZoom;
  @tracked _maxZoom;

  get displayValue() {
    return this.intl.t('message-view-min-max-zoom', {
      minZoom: this.minZoom,
      maxZoom: this.maxZoom,
    });
  }

  get minZoom() {
    if (!isNaN(this._minZoom)) {
      return this._minZoom;
    }

    return this.args.value.minZoom ?? 0;
  }

  set minZoom(value) {
    this._minZoom = value;
  }

  get maxZoom() {
    if (!isNaN(this._maxZoom)) {
      return this._maxZoom;
    }

    return this.args.value.maxZoom ?? 0;
  }

  set maxZoom(value) {
    this._maxZoom = value;
  }

  @action
  save() {
    return this.args.onSave(parseInt(this.minZoom), parseInt(this.maxZoom));
  }

  @action
  cancel() {
    this.minZoom = undefined;
    this.maxZoom = undefined;

    return this.args.onCancel?.();
  }
}
