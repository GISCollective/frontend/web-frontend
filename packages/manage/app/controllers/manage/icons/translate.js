/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class ManageIconsTranslateController extends Controller {
  @service intl;

  @tracked editableField = '';
  @tracked savingField = '';

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'browse.icons.set',
        model: this.model.iconSet.get('id'),
        text: this.model.iconSet.get('name'),
      },
      {
        route: 'manage.icons.edit',
        models: [this.model.iconSet.id, this.model.icon.id],
        text: this.intl.t('edit-model.title', { name: this.model.icon.name }),
      },
      {
        text: this.intl.t('translate to', {
          language: this.model.translation.name,
        }),
      },
    ];
  }

  @action
  async save() {
    this.savingField = this.editableField;
    this.editableField = '';

    await this.model.icon.save({
      adapterOptions: {
        locale: this.model.translation.locale,
      },
    });

    this.savingField = '';
    this.editableField = '';
  }

  @action
  saveCategory(newValue) {
    this.model.icon.set('category', newValue);
    return this.save();
  }

  @action
  saveSubcategory(newValue) {
    this.model.icon.set('subcategory', newValue);
    return this.save();
  }

  @action
  saveArticle(localName, description) {
    this.model.icon.localName = localName;
    this.model.icon.description = description;

    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.model.icon.rollbackAttributes();
    this.editableField = '';
    this.savingField = '';
  }
}
