/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class NewRecordFormFeatureComponent extends Component {
  @service user;
  @service router;

  @tracked maps;

  get isInvalid() {
    if (!this.args.feature) {
      return false;
    }

    return (
      !this.args.feature.name ||
      this.args.feature.name.trim() == '' ||
      this.maps.length == 0 ||
      !this.args.feature.position
    );
  }

  get extent() {
    let firstMap = this.map;

    if (!firstMap) {
      return null;
    }

    return firstMap.area;
  }

  get map() {
    return this.maps?.[0];
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  updateMap(value) {
    if (!value) {
      return;
    }

    this.maps = [value];
    this.args.onMapSelect?.(value);
  }

  @action
  changePositionValue(value) {
    if (!this.args.feature) {
      return;
    }

    this.args.feature.position = value;
  }

  @action
  save() {
    if(this.args.feature) {
      this.args.feature.maps = [...this.maps];
    }

    return this.args.onSubmit(this.args.feature);
  }

  @action
  async setup() {
    this.maps = (await this.args.feature?.maps) ?? [];
    this.args.onMapSelect?.(this.maps[0]);
  }
}
