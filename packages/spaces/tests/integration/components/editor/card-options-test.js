/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, click, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/card-options", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let spaceData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam("1");
    server.testData.storage.addDefaultTeam("2");
    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.testData.create.pagesMap = {
      campaigns: "1",
      "campaigns--:campaign-id": "2",
      "help--terms": "3",
    };

    this.set("space", await this.store.findRecord("space", spaceData._id));

    this.space._getPagesMap = {
      campaigns: "1",
      "campaigns--:campaign-id": "2",
      "help--terms": "3",
    };
  });

  it("allows to change the max items for each size", async function () {
    this.set("value", {
      maxItems: {
        sm: 2,
        md: 4,
        lg: 6,
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::CardOptions @value={{this.value}} @onChange={{this.onChange}} @model={{this.model}} />`);

    expect(this.element.querySelector(".input-max-items-sm").value).to.equal("2");
    expect(this.element.querySelector(".input-max-items-md").value).to.equal("4");
    expect(this.element.querySelector(".input-max-items-lg").value).to.equal("6");

    await fillIn(".input-max-items-sm", "4");
    await fillIn(".input-max-items-md", "6");
    await fillIn(".input-max-items-lg", "8");

    expect(value).to.deep.equal({
      type: "cover-title-description",
      container: {},
      containerList: { classes: [] },
      destination: {},
      extraFields: { fullDescription: false },
      cover: { defaultCover: "default" },
      maxItems: { sm: "4", md: "6", lg: "8" },
    });
  });

  it("allows to remove the max items for each size", async function () {
    this.set("value", {
      maxItems: {
        sm: 2,
        md: 4,
        lg: 6,
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::CardOptions @value={{this.value}} @onChange={{this.onChange}} @model={{this.model}} />`);

    expect(this.element.querySelector(".input-max-items-sm").value).to.equal("2");
    expect(this.element.querySelector(".input-max-items-md").value).to.equal("4");
    expect(this.element.querySelector(".input-max-items-lg").value).to.equal("6");

    await fillIn(".input-max-items-sm", "");
    await fillIn(".input-max-items-md", "");
    await fillIn(".input-max-items-lg", "");

    expect(value).to.deep.equal({
      type: "cover-title-description",
      container: {},
      containerList: { classes: [] },
      destination: {},
      extraFields: { fullDescription: false },
      cover: { defaultCover: "default" },
      maxItems: { sm: "", md: "", lg: "" },
    });
  });

  it("allows to change the columns value for each size", async function () {
    this.set("value", {});

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::CardOptions @value={{this.value}} @onChange={{this.onChange}} @model={{this.model}} />`);

    this.element.querySelector(".sm-width-select .input-width-col").value = "2";
    await triggerEvent(".sm-width-select .input-width-col", "change");

    this.element.querySelector(".md-width-select .input-width-col").value = "3";
    await triggerEvent(".md-width-select .input-width-col", "change");

    this.element.querySelector(".lg-width-select .input-width-col").value = "4";
    await triggerEvent(".lg-width-select .input-width-col", "change");

    expect(value).to.deep.equal({
      type: "cover-title-description",
      container: { classes: ["col-2", "col-md-3", "col-lg-4"], style: { sm: {}, md: {}, lg: {} } },
      containerList: { classes: [] },
      destination: {},
      cover: { defaultCover: "default" },
      extraFields: {
        fullDescription: false,
      },
      maxItems: {},
    });
  });

  it("allows to change the card type to cover-title", async function (a) {
    this.set("value", {
      container: {},
      type: "cover",
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::CardOptions @value={{this.value}} @onChange={{this.onChange}} @model={{this.model}} />`);

    expect(this.element.querySelector(".btn-card-cover")).to.have.class("selected");

    await click(".btn-card-cover-title");

    expect(this.element.querySelector(".btn-card-cover-title")).to.have.class("selected");

    a.deepEqual(value, {
      container: {},
      containerList: {
        classes: [],
      },
      cover: {
        defaultCover: "default",
      },
      destination: {},
      type: "cover-title",
      extraFields: {
        fullDescription: false,
      },
      maxItems: {},
    });
  });

  it("allows to change the card type to cover-title-overlay-description", async function (a) {
    this.set("value", {
      container: {},
      type: "cover",
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::CardOptions @value={{this.value}} @onChange={{this.onChange}} @model={{this.model}} />`);

    expect(this.element.querySelector(".btn-card-cover")).to.have.class("selected");

    await click(".btn-card-cover-title-overlay-description");

    expect(this.element.querySelector(".btn-card-cover-title-overlay-description")).to.have.class("selected");

    a.deepEqual(value, {
      container: {},
      containerList: {
        classes: [],
      },
      cover: {
        defaultCover: "default",
      },
      destination: {},
      type: "cover-title-overlay-description",
      extraFields: {
        fullDescription: false,
      },
      maxItems: {},
    });
  });

  it("allows to change the card padding", async function (a) {
    this.set("value", {
      container: {
        classes: ["pt-3"],
      },
      type: "cover-title-description",
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::CardOptions @value={{this.value}} @onChange={{this.onChange}} @model={{this.model}} />`);

    expect(this.element.querySelector(".responsive-value-sm .sm-top-padding").value).to.equal("3");

    this.element.querySelector(".responsive-value-sm .sm-top-padding").value = "1";
    await triggerEvent(".responsive-value-sm .sm-top-padding", "change");

    a.deepEqual(value, {
      container: {
        classes: ["pt-1"],
      },
      containerList: {
        classes: [],
      },
      cover: {
        defaultCover: "default",
      },
      destination: {},
      type: "cover-title-description",
      extraFields: {
        fullDescription: false,
      },
      maxItems: {},
    });
  });

  it("allows to change the extra card components", async function (a) {
    this.set("value", {
      container: {
        classes: ["pt-3"],
      },
      type: "cover-title-description",
      extraFields: {
        entries: true,
        icons: false,
        nextOccurrence: false,
        schedule: false,
        fullDescription: false,
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    this.set("testElement", null);
    const space = await this.store.findRecord("space", spaceData._id);
    this.set("model", { space });

    await render(
      hbs`<div class="test" data-model-type="event"></div><Editor::CardOptions @element={{this.testElement}} @model={{this.model}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.set("testElement", this.element.querySelector(".test"));

    expect(this.element.querySelector(".entries").value).to.equal("yes");

    this.element.querySelector(".next-occurrence").value = "yes";
    await triggerEvent(".next-occurrence", "change");

    a.deepEqual(value, {
      container: {
        classes: ["pt-3"],
      },
      containerList: {
        classes: [],
      },
      cover: {
        defaultCover: "default",
      },
      destination: {},
      extraFields: {
        entries: true,
        icons: false,
        nextDateWithLocation: false,
        nextOccurrence: true,
        schedule: false,
        fullDescription: false,
      },
      type: "cover-title-description",
      maxItems: {},
    });
  });

  it("allows to change the container list options", async function (a) {
    this.set("value", {
      container: {
        classes: ["pt-3"],
      },
      type: "cover-title-description",
      containerList: {
        classes: ["justify-content-center"],
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    const space = await this.store.findRecord("space", spaceData._id);
    this.set("model", { space });

    await render(
      hbs`<div class="test" data-model-type="event"></div><Editor::CardOptions @element={{this.testElement}} @model={{this.model}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    expect(this.element.querySelector(".input-sm-justify-content").value).to.equal("center");

    this.element.querySelector(".input-sm-justify-content").value = "start";
    await triggerEvent(".input-sm-justify-content", "change");

    a.deepEqual(value, {
      container: {
        classes: ["pt-3"],
      },
      containerList: {
        classes: ["justify-content-start"],
      },
      cover: {
        defaultCover: "default",
      },
      destination: {},
      extraFields: {
        fullDescription: false,
      },
      type: "cover-title-description",
      maxItems: {},
    });
  });

  it("allows to change cover source", async function (a) {
    this.set("value", {
      container: {
        classes: ["pt-3"],
      },
      type: "cover-title-description",
      containerList: {
        classes: ["justify-content-center"],
      },
      cover: {
        defaultCover: "from parent",
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    const space = await this.store.findRecord("space", spaceData._id);
    this.set("model", { space });

    await render(
      hbs`<div class="test" data-model-type="event"></div><Editor::CardOptions @element={{this.testElement}} @model={{this.model}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    expect(this.element.querySelector(".default-cover").value).to.equal("from parent");

    this.element.querySelector(".default-cover").value = "default";
    await triggerEvent(".default-cover", "change");

    a.deepEqual(value, {
      container: {
        classes: ["pt-3"],
      },
      containerList: {
        classes: ["justify-content-center"],
      },
      cover: {
        defaultCover: "default",
      },
      destination: {},
      extraFields: {
        fullDescription: false,
      },
      type: "cover-title-description",
      maxItems: {},
    });
  });

  describe("destination", async function (hooks) {
    hooks.beforeEach(async function () {
      server.testData.create.pagesMap = {
        campaigns: "1",
        "campaigns--:campaign-id": "2",
        "help--terms": "3",
        "campaigns--:event-id": "4",
        "campaigns--:feature-id": "5",
      };

      const space = await this.store.findRecord("space", spaceData._id);
      this.set("model", { space });

      this.space._getPagesMap = {
        campaigns: "1",
        "campaigns--:campaign-id": "2",
        "help--terms": "3",
        "campaigns--:event-id": "4",
        "campaigns--:feature-id": "5",
      };
    });

    it("allows changing the destination page", async function (a) {
      this.set("value", {});

      let value;
      this.set("onChange", (v) => {
        value = v;
      });

      await render(
        hbs`<Editor::CardOptions @model={{this.model}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      const options = [...this.element.querySelector(".options-select")].map((a) => a.value);

      expect(this.element.querySelector(".options-select").value).to.equal("");

      this.element.querySelector(".options-select").value = "/campaigns/:campaign-id";
      await triggerEvent(".options-select", "change");

      a.deepEqual(options, [
        "/campaigns",
        "/campaigns/:campaign-id",
        "/help/terms",
        "/campaigns/:event-id",
        "/campaigns/:feature-id",
      ]);
      expect(value).to.deep.equal({
        type: "cover-title-description",
        container: {},
        containerList: { classes: [] },
        destination: "/campaigns/:campaign-id",
        extraFields: {
          fullDescription: false,
        },
        cover: { defaultCover: "default" },
        maxItems: {},
      });
    });

    it("shows only the model pages when the model name is set", async function (a) {
      this.set("value", {});

      let value;
      this.set("onChange", (v) => {
        value = v;
      });
      this.set("testElement", null);

      await render(
        hbs`<div class="test" data-model-type="campaign"></div><Editor::CardOptions @element={{this.testElement}} @model={{this.model}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      this.set("testElement", this.element.querySelector(".test"));

      const options = [...this.element.querySelector(".options-select")].map((a) => a.value);

      this.element.querySelector(".options-select").value = "/campaigns/:campaign-id";
      await triggerEvent(".options-select", "change");

      expect(options).to.deep.equal(["/campaigns/:campaign-id"]);
      expect(value).to.deep.equal({
        type: "cover-title-description",
        container: {},
        containerList: { classes: [] },
        destination: "/campaigns/:campaign-id",
        extraFields: {
          fullDescription: false,
        },
        cover: { defaultCover: "default" },
        maxItems: {},
      });
    });

    it("shows both the event and feature urls for an event id", async function (a) {
      this.set("value", {});

      let value;
      this.set("onChange", (v) => {
        value = v;
      });
      this.set("testElement", null);

      await render(
        hbs`<div class="test" data-model-type="event"></div><Editor::CardOptions @element={{this.testElement}} @model={{this.model}} @value={{this.value}} @onChange={{this.onChange}} />`,
      );

      this.set("testElement", this.element.querySelector(".test"));

      const options = [...this.element.querySelector(".options-select")].map((a) => a.value);

      this.element.querySelector(".options-select").value = "/campaigns/:campaign-id";
      await triggerEvent(".options-select", "change");

      expect(options).to.deep.equal(["/campaigns/:event-id", "/campaigns/:feature-id"]);
      expect(value).to.deep.equal({
        type: "cover-title-description",
        container: {},
        containerList: { classes: [] },
        destination: {},
        extraFields: { fullDescription: false },
        cover: { defaultCover: "default" },
        maxItems: {},
      });
    });

    it("renders the destination value when is set", async function () {
      this.set("value", {
        destination: "/campaigns/:campaign-id",
      });

      this.set("onChange", () => {});

      await render(
        hbs`<Editor::CardOptions @value={{this.value}} @model={{this.model}} @onChange={{this.onChange}} />`,
      );

      expect(this.element.querySelector(".options-select").value).to.equal("/campaigns/:campaign-id");
    });
  });
});
