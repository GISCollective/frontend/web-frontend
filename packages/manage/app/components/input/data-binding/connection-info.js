/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class InputDataBindingConnectionInfoComponent extends Component {
  elementId = 'connection-info-' + guidFor(this);
  values = {};

  @tracked currentFields;

  fields = {
    airtable: [
      { name: '_apiKey', label: 'personal access token' },
      { name: 'baseId', label: 'base id' },
      { name: 'tableName', label: 'table name' },
      { name: 'viewName', label: 'view name' },
    ],
  };

  get hasType() {
    return !!this.args.type;
  }

  get hasValidType() {
    return !!this.fields[this.args.type];
  }

  triggerChange() {
    const values = this.args.value ?? {};
    const value = {};

    for (const field of this.currentFields) {
      value[field.name] = this.values[field.name] ?? values[field.name];
    }

    return this.args.onChange(value);
  }

  @action
  setup() {
    const values = this.args.value ?? {};
    const fields = this.fields ?? {};

    const newFields =
      fields[this.args.type]?.map((a) => ({
        ...a,
        value: this.values[a.name] ?? values[a.name],
      })) ?? [];

    if (this.currentFields?.length != newFields.length) {
      return later(() => {
        this.currentFields = newFields;
      });
    }

    this.currentFields.forEach((field, i) => {
      if (field.name != newFields[i].name) {
        this.currentFields[i] = newFields[i];
      }
    });
  }

  @action
  updateValue(name, ev) {
    this.values[name] = ev.target.value;

    this.triggerChange();
  }
}
