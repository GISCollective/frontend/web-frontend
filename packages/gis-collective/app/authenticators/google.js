/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import config from '../config/environment';
import OAuth2PasswordGrant from 'ember-simple-auth/authenticators/oauth2-password-grant';
import fetch from 'fetch';

export default class GoogleAuthenticator extends OAuth2PasswordGrant {
  serverTokenEndpoint = `${config.apiUrl}/auth/googletoken`;
  serverTokenRevocationEndpoint = `${config.apiUrl}/auth/revoke`;

  async authenticate(options) {
    return await this.makeJsonRequest(this.serverTokenEndpoint, {
      idtoken: options,
    });
  }

  makeJsonRequest(url, data, headers = {}) {
    headers['Content-Type'] = 'application/json';

    const body = JSON.stringify(data);

    const options = {
      body,
      headers,
      method: 'POST',
    };

    return new Promise((resolve, reject) => {
      fetch(url, options)
        .then((response) => {
          response.text().then((text) => {
            try {
              let json = JSON.parse(text);
              if (!response.ok) {
                response.responseJSON = json;
                reject(response);
              } else {
                resolve(json);
              }
            } catch (SyntaxError) {
              response.responseText = text;
              reject(response);
            }
          });
        })
        .catch(reject);
    });
  }
}
