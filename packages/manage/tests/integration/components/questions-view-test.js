/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { expect } from 'chai';

module('Integration | Component | questions-view', function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIcon('1');
    server.testData.storage.addDefaultIcon('2');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<QuestionsView />`);
    assert.dom(this.element).hasText('');
  });

  it('renders one question', async function () {
    this.set('value', [
      {
        question: 'What is your email?',
        help: 'We need your email to let you know if we have questions about your contribution and to let you know about the progress.',
        type: 'email',
        name: 'email',
        isRequired: true,
      },
    ]);
    await render(hbs`<QuestionsView @value={{this.value}} />`);

    const questions = this.element.querySelectorAll('.question-item');

    expect(questions).to.have.length(1);

    expect(this.element.querySelector('.question')).to.have.textContent('What is your email? *');
    expect(this.element.querySelector('.help')).to.have.textContent(
      'We need your email to let you know if we have questions about your contribution and to let you know about the progress.',
    );
    expect(this.element.querySelector('.type')).to.have.textContent('email');
  });

  it('renders an alert for icons without options', async function () {
    this.set('value', [
      {
        question: 'Which are your icons?',
        type: 'icons',
        name: 'icons',
        isRequired: true,
      },
    ]);
    await render(hbs`<QuestionsView @value={{this.value}} />`);

    expect(this.element.querySelector('.alert-danger')).to.have.textContent(
      "You haven't picked any icons. You need to select at least one icon to make the question relevant.",
    );
  });

  it('renders the icons when they are selected', async function () {
    this.set('value', [
      {
        question: 'Which are your icons?',
        type: 'icons',
        name: 'icons',
        isRequired: true,
        options: ['1', '2'],
      },
    ]);
    await render(hbs`<QuestionsView @value={{this.value}} />`);

    expect(this.element.querySelector('.alert-danger')).not.to.exist;
    expect(this.element.querySelectorAll('.icon-container')).to.have.length(2);
  });
});
