/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AttributesValueOptionsComponent from './value-options';

export default class AttributesValueMapListComponent extends AttributesValueOptionsComponent {
  emptyMessage = 'no maps available in this area.';
  selectMessage = 'click to select from list';
}
