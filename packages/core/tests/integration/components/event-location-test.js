/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | event-location', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let spaceService;

  hooks.beforeEach(function () {
    server = new TestServer();
    const feature1 = server.testData.storage.addDefaultFeature('1');
    feature1.name = 'feature 1';

    const feature2 = server.testData.storage.addDefaultFeature('2');
    feature2.name = 'feature 2';

    spaceService = this.owner.lookup('service:space');
    spaceService.getCachedPagesMap = () => ({});
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<EventLocation />`);

    assert.dom(this.element).hasText('');
  });

  it('renders a text value', async function (assert) {
    this.set('value', {
      type: 'Text',
      value: 'some place in our town',
    });

    await render(hbs`<EventLocation @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal('some place in our town');
  });

  it('renders a feature value', async function (a) {
    this.set('value', {
      type: 'Feature',
      value: '1',
    });

    await render(hbs`<EventLocation @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal('feature 1');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders a feature value with a link when the current space has a feature page', async function (a) {
    spaceService.getLinkFor = (a, b) => `http://${a}/${b.id}`;

    this.set('value', {
      type: 'Feature',
      value: '1',
    });

    await render(hbs`<EventLocation @value={{this.value}} />`);

    expect(this.element.querySelector('a').textContent.trim()).to.equal('feature 1');
    expect(this.element.querySelector('a')).to.have.attribute('href', 'http://Feature/1');
  });
});
