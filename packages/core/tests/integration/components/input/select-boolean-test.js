/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { triggerEvent, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/select-boolean', function (hooks) {
  setupRenderingTest(hooks);

  it('renders no when there is no value set', async function () {
    await render(hbs`<Input::SelectBoolean />`);

    expect(this.element.querySelector('.form-select').value).to.equal('no');
  });

  it('renders no when the value is false', async function () {
    await render(hbs`<Input::SelectBoolean @value={{false}} />`);

    expect(this.element.querySelector('.form-select').value).to.equal('no');
  });

  it('renders yes when the value is true', async function () {
    await render(hbs`<Input::SelectBoolean @value={{true}} />`);

    expect(this.element.querySelector('.form-select').value).to.equal('yes');
  });

  it('triggers onChange with false when the value is set to no', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::SelectBoolean @value={{true}} @onChange={{this.change}} />`);

    this.element.querySelector('select').value = 'no';
    await triggerEvent('select', 'change');

    expect(this.element.querySelector('.form-select').value).to.equal('no');
    expect(value).to.equal(false);
  });

  it('triggers onChange with true when the value is set to yes', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::SelectBoolean @value={{false}} @onChange={{this.change}} />`);

    this.element.querySelector('select').value = 'yes';
    await triggerEvent('select', 'change');

    expect(this.element.querySelector('.form-select').value).to.equal('yes');
    expect(value).to.equal(true);
  });
});
