/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/app-route';
import { hash } from 'rsvp';

export default class CampaignsSuccessRoute extends Route {
  async model(params) {
    const campaign = await this.store.findRecord('campaign', params.id);

    return hash({
      campaign,
    });
  }
}
