/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { DateTime } from 'luxon';
import Component from '@glimmer/component';

export default class ManageDashboardMapReportsComponent extends Component {
  get start() {
    return this.end.minus({ months: 1 });
  }

  get end() {
    return DateTime.now().endOf('day');
  }
}
