/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class ManageEditorsOptionalBooleanComponent extends Component {
  options = ["true", "false", "unknown"];

  get value() {
    if (this.args.value === null || this.args.value === undefined) {
      return "unknown";
    }

    return this.args.value ? "true" : "false";
  }

  @action
  change(value) {
    if (value == "true") {
      return this.args.onChange?.(true);
    }

    if (value == "false") {
      return this.args.onChange?.(false);
    }

    return this.args.onChange?.(undefined);
  }
}
