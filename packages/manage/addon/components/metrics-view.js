import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { Chart } from 'chart.js';
import { toDateTime } from 'models/transforms/calendar-entry-list';
import { DateTime } from 'luxon';

export default class MetricsView extends Component {
  @service store;
  @service intl;
  @tracked data;

  @tracked colors = {};

  toDayLabel(time, index) {
    return toDateTime(time).toFormat('dd LLL');
  }

  get defaultStart() {
    return this.defaultEnd.minus({ months: 1 });
  }

  get defaultEnd() {
    return DateTime.now().endOf('day');
  }

  get start() {
    return toDateTime(this.args.start ?? this.defaultStart);
  }

  get end() {
    return toDateTime(this.args.end ?? this.defaultEnd);
  }

  fixGaps(data) {
    let prevItem = this.start;
    let newData = [];

    if (!data?.length) {
      return newData;
    }

    let index = 0;

    const lastTime = toDateTime(data[data.length - 1].time);

    if (data.length > 0 && !lastTime.hasSame(this.end, 'day')) {
      data = data?.toArray?.() ?? data ?? [];

      data.push({
        value: 0,
        time: this.end,
      });
    }

    while (prevItem < this.end && data.length > index) {
      let record = data[index];
      index++;
      let nextDate = toDateTime(record.time);

      let fillRecord = {
        value: 0,
        time: prevItem.plus({ day: 1 }),
      };

      while (!nextDate.hasSame(fillRecord.time, 'day') && nextDate > fillRecord.time) {
        newData.push({ ...fillRecord });
        fillRecord.time = fillRecord.time.plus({ days: 1 });
      }

      newData.push(record);

      prevItem = nextDate;
    }

    return newData;
  }

  @action
  async setup(element) {
    if (!this.args.name || !this.args.type || !this.args.value) {
      return;
    }

    this.data = this.fixGaps(this.args.value).map((a, i) => ({
      value: a.value,
      label: this.toDayLabel(a.time, i),
    }));

    const primary = getComputedStyle(element.querySelector('.primary'));

    this.colors.primary = {
      backgroundColor: primary['background-color'],
      borderColor: primary['background-color'],
      borderWidth: parseInt(primary['border-bottom-width']),
      hoverBorderWidth: parseInt(primary['border-bottom-width']),
    };
  }

  @action
  async renderData(element) {
    const chart = new Chart(element, {
      type: 'line',
      data: {
        labels: this.data.map((a) => a.label),
        datasets: [
          {
            label: this.intl.t('_dashboard.map-views'),
            data: this.data.map((a) => a.value),
          },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        elements: {
          point: {
            radius: 1,
            hitRadius: 20,
            ...this.colors.primary,
          },
          line: this.colors.primary,
        },
        scales: {
          x: {
            display: false,
          },
          y: {
            display: true,
          },
        },
        plugins: {
          legend: {
            display: false,
          },
        },
      },
    });

    this.chart = chart;
    element.chart = chart;
  }
}
