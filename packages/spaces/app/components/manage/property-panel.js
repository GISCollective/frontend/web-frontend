/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action, set, get } from "@ember/object";
import { service } from "@ember/service";
import { PageCol } from "models/transforms/page-col-list";
import { A } from "models/lib/array";
import { tracked } from "@glimmer/tracking";
import { fetch } from "models/lib/fetch-source";

export class Selection {
  @tracked type;
  @tracked title;
  @tracked data;

  constructor(type, title, data) {
    if (typeof type == "object") {
      this.type = type.type;
      this.title = type.title;
      this.data = type.data ?? {};

      return;
    }

    this.type = type;
    this.title = title;
    this.data = data ?? {};
  }
}

export default class ManagePropertyPanelComponent extends Component {
  @service intl;
  @service store;
  @service space;
  @service fastboot;

  get isPageColEditor() {
    return this.args.selection?.type == "col" && !Array.isArray(this.args.selection?.groups);
  }

  get isSectionEditor() {
    return this.args.selection?.type == "col" && Array.isArray(this.args.selection?.groups);
  }

  get value() {
    if (!this.args.selection.isHighlight) {
      return null;
    }

    if (!this.args.selection.path) {
      return null;
    }

    return get(this.args.model, this.args.selection.path);
  }

  globalLayoutPath() {
    const pieces = this.args.selection.path.split(".");

    if (pieces[1] != "layoutContainers") {
      return false;
    }

    const containerPath = [pieces[0], pieces[1], pieces[2]].join(".");
    const container = get(this.args.model, containerPath) ?? {};

    if (!container.gid) {
      return false;
    }

    return { gid: container.gid, index: parseInt(pieces[2]) };
  }

  ensurePath() {
    const pieces = this.args.selection.path.split(".");
    const path = [];

    for (const key of pieces) {
      path.push(key);

      const value = get(this.args.model, path.join("."));

      if (value === null || value === undefined) {
        set(this.args.model, path.join("."), {});
      }
    }
  }

  @action
  changeHighlight(value) {
    const selection = this.args.selection;

    if (selection.element) {
      const event = new CustomEvent("props-update", {
        detail: {
          selection,
          value,
        },
      });

      selection.element.dispatchEvent(event);
    }

    const pieces = selection.path.split(".");
    this.ensurePath();

    set(this.args.model, selection.path, value);
    set(this.args.model, `${pieces[0]}.shouldSave`, true);

    const gid = selection.gid || value?.gid;
    let col;

    if (gid && pieces[0] == "page" && pieces[1] == "cols" && !isNaN(pieces[2])) {
      const index = parseInt(pieces[2]);
      col = this.args.model.page.cols[index];
    }

    if (gid && col) {
      this.args.model.space.cols.setPageCol(gid, col.type, col.data);
      this.args.onSpaceChange?.(this.args.model.space);
    }

    const globalContainer = this.globalLayoutPath();
    if (globalContainer) {
      this.args.model.space.layoutContainers.setContainer(
        globalContainer.gid,
        this.args.model.page.layoutContainers[globalContainer.index],
      );
      this.args.onSpaceChange?.(this.args.model.space);
    }
  }

  @action
  async changeCol(col, newData, newType, gid) {
    col.data = newData || {};
    col.type = newType;
    col.gid = gid;

    if (!col.fromJson) {
      col = new PageCol(col, this.store, this.space, this.fastboot);
      this.args.model.page.replacePageCol(col);
    }

    if (gid) {
      this.args.model.space?.setPageCol(gid, col.type, col.data);
      col.gid = gid;
      this.args.onSpaceChange?.(this.args.model.space);
    }

    await this.updateModel(col);

    return col;
  }

  @action
  changeColByName(name, newData, newType) {
    const col = this.args.model.page.upsertPageColByName(name);

    return this.changeCol(col, newData, newType);
  }

  @action
  async changeSelectedCol(newData, newType, gid) {
    let col = this.args.selection?.data ?? {};

    col = await this.changeCol(col, newData, newType, gid);

    this.args.selection.data = col;

    return col;
  }

  async updateModel(col) {
    const key = col.modelKey;
    const hasModelKey = !!key;

    if (!col.fromJson) {
      col = new PageCol(col);
      this.args.model.page.replacePageCol(col);
    }

    const result = await fetch(this.args.model, col, this.args.model.space, this.store);

    if (hasModelKey && key != "currentModel" && result) {
      this.args.model[key] = result;
    }
  }

  @action
  removeCol() {
    const name = this.args.selection.data?.name;

    if (!name) {
      return;
    }

    this.args.model.page.cols = A(this.args.model.page.cols.filter((a) => a.name != name));
    this.args.selection.data = this.args.model.page.upsertPageColByName(name);
  }

  @action
  async changeGroup(property, value) {
    this.args.selection.data.data[property] = value;

    const isColEdit = this.isPageColEditor || this.isSectionEditor;

    if (this.args.selection.data.gid && isColEdit) {
      const col = this.args.selection.data;

      this.args.model.space?.setPageCol(col.gid, col.type, col.data);

      this.updateModel(this.args.selection.data);
      return;
    }

    if (!isColEdit) {
      const container = this.args.model.page.getContainerFor(this.args.selection.data.name);

      if (container.gid) {
        this.args.model.space.layoutContainers.setContainer(container.gid, container);
        this.args.onSpaceChange?.(this.args.model.space);
      }
    }
  }
}
