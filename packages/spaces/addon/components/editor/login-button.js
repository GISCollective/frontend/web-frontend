import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorLoginButton extends Component {
  @tracked _classes;

  get classes() {
    if (this._classes) {
      return this._classes;
    }

    return this.args.value?.style?.classes ?? [];
  }

  triggerChange() {
    return this.args.onChange?.({
      style: { classes: this.classes },
    });
  }

  @action
  changeClasses(value) {
    this._classes = value;

    return this.triggerChange();
  }
}
