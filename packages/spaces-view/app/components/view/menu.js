/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import {
  getSizeIndexFromWidth,
  getSizeIndexFromBs,
} from '../../lib/responsive';
import { later } from '@ember/runloop';

export default class PageColMenuComponent extends Component {
  @service clickOutside;
  @service space;
  @service session;
  @service fastboot;
  @service window;
  @service search;
  @service loading;

  @tracked element;
  @tracked isHamburgerMenuVisible = false;
  @tracked spinnerStyle;
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value?.data ?? {};
  }

  get isLoading() {
    if (this.loading.isDestroyed) {
      return false;
    }

    return this.fastboot.isFastBoot || this.loading.isLoading;
  }

  get menuItems() {
    return this.value?.menu?.items ?? [];
  }

  get isSticky() {
    if (this.args?.isEditor) {
      return false;
    }

    const position = this.value.position;

    if (position != 'stick' || !this.element || this.window.scrollY == 0) {
      return false;
    }

    const elementTop = this.element.getBoundingClientRect().top;

    return elementTop <= 0;
  }

  get isAuto() {
    const position = this.value.position;

    if (position != 'auto' || !this.element || this.window.scrollY == 0) {
      return false;
    }

    const elementTop = this.element.getBoundingClientRect().top;

    return elementTop + this.elementHeight <= 0;
  }

  get isFull() {
    return this.value.width == 'full';
  }

  get currentSpace() {
    return this.space.currentSpace;
  }

  get deviceSize() {
    if (this.args.deviceSize) {
      return getSizeIndexFromBs(this.args.deviceSize);
    }

    return getSizeIndexFromWidth(this.window.width);
  }

  get hasHamburger() {
    return (
      getSizeIndexFromBs(this.value.hamburgerFrom ?? 'mobile') >=
      this.deviceSize
    );
  }

  get classes() {
    return {
      selected: {
        bgColorClass: `bg-${this.value.selectedBackground}`,
        textColor: `text-${this.value.selectedText}`,
      },
      regular: {
        bgColorClass: this.bgColorClass,
        textColor: this.textColorClass,
      },
      bgColorClass: this.bgColorClass,
      borderColorClass: this.borderColorClass,
      textColor: this.textColorClass,
      shadowClass: this.shadowClass,
    };
  }

  get bgColorClass() {
    let color = this.value.primaryBackground;

    if (this.isAuto) {
      color = this.value.secondaryBackground;
    }

    return `bg-${color}`;
  }

  get borderColorClass() {
    let color = this.value.primaryBorder;

    if (this.isAuto) {
      color = this.value.secondaryBorder;
    }

    return `border-${color}`;
  }

  get textColor() {
    let color = this.value.primaryText;

    if (this.isAuto) {
      color = this.value.secondaryText;
    }

    return color;
  }

  get textColorClass() {
    return `text-${this.textColor}`;
  }

  get shadowClass() {
    if (this.value.shadow == 'medium') {
      return 'shadow';
    }

    if (this.value.shadow == 'small') {
      return 'shadow-sm';
    }

    if (this.value.shadow == 'large') {
      return 'shadow-lg';
    }

    return `shadow-none`;
  }

  get alignmentClass() {
    if (this.value.align == 'right') {
      return 'ms-auto';
    }

    if (this.value.align == 'center') {
      return 'me-auto ms-auto';
    }

    return 'me-auto';
  }

  get elementHeight() {
    if (!this.element || this.fastboot.isFastBoot) {
      return false;
    }

    const navbar = this.element.querySelector('nav');

    if (!navbar) {
      return false;
    }

    return navbar.offsetHeight;
  }

  get placeholderStyle() {
    const height = this.elementHeight;

    if (!height) {
      return null;
    }

    return htmlSafe(`height: ${height}px`);
  }

  get showSignIn() {
    return this.value.showLogin && !this.session.isAuthenticated;
  }

  get showUserMenu() {
    return this.value.showLogin && this.session.isAuthenticated;
  }

  get homePageLink() {
    return {
      pageId: this.args?.space?.landingPageId,
    };
  }

  @action
  updateProps(event) {
    this._value = event.detail.value;
  }

  @action
  setLogoSize(ev) {
    const width = ev.target.clientWidth;
    const height = ev.target.clientHeight;

    this.spinnerStyle = htmlSafe(`width: ${width}px; height: ${height}px;`);
  }

  @action
  enableSearch() {
    if (this.args.isEditor) {
      return;
    }

    this.search.isEnabled = true;
  }

  @action
  toggleHamburgerMenu() {
    if (this.isHamburgerMenuVisible) {
      return this.hideHamburgerMenu();
    }

    return this.showHamburgerMenu();
  }

  @action
  showHamburgerMenu() {
    this.isHamburgerMenuVisible = true;

    later(() => {
      this.clickOutside.subscribe(this.element, () => {
        this.hideHamburgerMenu();
      });
    });
  }

  @action
  hideHamburgerMenu() {
    this.isHamburgerMenuVisible = false;
  }

  @action
  setup(element) {
    this.element = element;
  }
}
