/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { fetch } from 'models/lib/fetch-source';
import { PageCol } from 'models/transforms/page-col-list';

describe('Integration | Component | view/base/source-buckets', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let campaigns = [];

  hooks.before(function () {
    server = new TestServer();

    for (let i = 1; i <= 10; i++) {
      const campaign = server.testData.storage.addDefaultCampaign(`${i}`);
      campaign.id = i;
      campaign._id = i;
      campaign.name = `Campaign ${i}`;

      campaigns.push(campaign);
    }
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders the content when there is no source', async function () {
    await render(hbs`
      <View::Base::SourceBuckets>
        template block text
      </View::Base::SourceBuckets>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
    expect(this.element.querySelector('.group-selector')).not.to.exist;
  });

  it('uses the buckets from the value when is set', async function (a) {
    this.set('value', {
      data: {
        source: { team: '1', model: 'campaign' },
      },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { buckets: [[campaigns[9]]] },
    });

    await render(hbs`
      <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
        {{#each lazyList.buckets as |column|}}
          {{#each column as |record|}}
            <div class="bucket-content">
              {{record.name}}
            </div>
          {{/each}}
        {{/each}}
      </View::Base::SourceBuckets>
    `);

    expect(this.element.querySelectorAll('.bucket-content')).to.have.length(1);
    expect(
      this.element.querySelector('.bucket-content').textContent.trim(),
    ).to.equal('Campaign 10');
    expect(this.element.querySelector('.group-selector')).not.to.exist;
  });

  describe('update props event', function () {
    it('does not update the model when the new value has no model', async function (a) {
      this.set('value', {
        data: {
          source: { team: '1', model: 'campaign' },
        },
        modelKey: 'campaign_1',
        fetch: () => null,
      });

      const model = {
        campaign_1: campaigns[1],
      };
      this.set('model', model);

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      await triggerEvent('.page-col-source', 'props-update', {
        detail: {
          selection: {
            name: 'col-1',
            type: 'data source',
            path: 'cols.0.data.source',
            editor: 'source-buckets',
          },
          value: { ids: [], model: '' },
        },
      });

      a.deepEqual(model, {
        campaign_1: campaigns[1],
      });
    });

    it('does not update the model when the new value is the selected model', async function (a) {
      this.set('value', {
        data: {
          source: { team: '1', model: 'campaign' },
        },
        modelKey: 'campaign_1',
        fetch: () => null,
      });

      const model = {
        campaign_1: campaigns[1],
      };
      this.set('model', model);

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      await triggerEvent('.page-col-source', 'props-update', {
        detail: {
          selection: {
            name: 'col-1',
            type: 'data source',
            path: 'cols.0.data.source',
            editor: 'source-buckets',
          },
          value: { useSelectedModel: true },
        },
      });

      a.deepEqual(model, {
        campaign_1: campaigns[1],
      });
    });
  });

  describe('using a group source', function (hooks) {
    hooks.beforeEach(function () {
      this.set('value', {
        data: {
          source: {
            model: 'campaign',
            groups: [
              { name: 'group 1', ids: ['1', '2', '3'] },
              { name: 'group 2', ids: ['4', '5', '6'] },
              { name: 'group 3', ids: ['7', '8', '9'] },
            ],
          },
          categories: {
            button: {
              classes: ['btn-secondary'],
            },
            activeButton: {
              classes: ['btn-primary'],
            },
          },
        },
        modelKey: 'campaign_group_1',
      });
    });

    it('renders the selected group when there is a group source', async function (a) {
      this.set('model', {
        campaign_group_1: {
          buckets: [[campaigns[0], campaigns[1], campaigns[2]]],
        },
      });

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      const buttons = this.element.querySelectorAll('.btn-group-name');
      expect(buttons[0]).to.have.class('btn-selected');

      const elements = this.element.querySelectorAll('.bucket-content');

      expect(elements).to.have.length(3);
      expect(elements[0].textContent.trim()).to.equal('Campaign 1');
      expect(elements[1].textContent.trim()).to.equal('Campaign 2');
      expect(elements[2].textContent.trim()).to.equal('Campaign 3');
    });

    it('renders the group selector', async function () {
      this.set('model', {
        campaign_group_1: {
          buckets: [[campaigns[0], campaigns[1], campaigns[2]]],
        },
      });

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      expect(this.element.querySelector('.group-selector')).to.exist;

      const buttons = this.element.querySelectorAll('.btn-group-name');

      expect(buttons).to.have.length(3);
      expect(buttons[0].textContent.trim()).to.equal('group 1');
      expect(buttons[1].textContent.trim()).to.equal('group 2');
      expect(buttons[2].textContent.trim()).to.equal('group 3');
    });

    it('changes the buckets when a btn group is pressed', async function () {
      this.set('model', {
        campaign_group_1: {
          buckets: [[campaigns[0], campaigns[1], campaigns[2]]],
        },
        campaign_group_2: {
          buckets: [[campaigns[3], campaigns[4], campaigns[5]]],
        },
      });

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      const buttons = this.element.querySelectorAll('.btn-group-name');

      await click(buttons[1]);

      expect(buttons[1]).to.have.class('btn-selected');

      const elements = this.element.querySelectorAll('.bucket-content');

      expect(elements).to.have.length(3);
      expect(elements[0].textContent.trim()).to.equal('Campaign 4');
      expect(elements[1].textContent.trim()).to.equal('Campaign 5');
      expect(elements[2].textContent.trim()).to.equal('Campaign 6');
    });

    it('applies the accent color for the selected groups', async function (a) {
      this.set('model', {});

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      expect(this.element.querySelector('.btn-selected')).to.have.class(
        'btn-primary',
      );
    });

    it('applies the color to the group-selector container', async function () {
      this.set('model', {});

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      expect(
        this.element.querySelector('.btn:not(.btn-selected)'),
      ).to.have.class('btn-secondary');
    });
  });

  describe('using the selected model', function (hooks) {
    let col;

    hooks.beforeEach(function () {
      col = new PageCol({
        data: {
          source: {
            useSelectedModel: true,
            property: 'maps',
          },
        },
        modelKey: 'selectedModel',
      });

      this.set('value', col);

      server.testData.storage.addDefaultMap();
    });

    it('renders the feature maps', async function () {
      const featureData = server.testData.storage.addDefaultFeature();

      const store = this.owner.lookup('service:store');
      const feature = await store.findRecord('feature', featureData._id);
      await feature.get('maps');

      this.set('model', {
        selectedModel: feature,
        [col.modelKey]: await fetch({ selectedModel: feature }, col),
      });

      await render(hbs`
        <View::Base::SourceBuckets @model={{this.model}} @value={{this.value}} as | lazyList |>
          {{#each lazyList.buckets as |column|}}
            {{#each column as |record|}}
              <div class="bucket-content">
                {{record.name}}
              </div>
            {{/each}}
          {{/each}}
        </View::Base::SourceBuckets>
      `);

      expect(
        this.element.querySelector('.bucket-content').textContent.trim(),
      ).to.equal('Harta Verde București');
    });
  });
});
