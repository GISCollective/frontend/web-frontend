/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { IconStyle } from './icon-style';

export default class InheritedStyleTransform extends Transform {
  deserialize(serialized) {
    return new InheritedStyle(serialized);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class InheritedStyle {
  @tracked hasCustomStyle;
  @tracked types;

  constructor(value) {
    if (!value || typeof value != 'object') {
      value = {};
    }

    this.hasCustomStyle = value.hasCustomStyle || false;
    this.types = new IconStyle(value['types']);
  }

  toJSON() {
    const result = {
      hasCustomStyle: this.hasCustomStyle,
    };

    if (this.hasCustomStyle) {
      result.types = this.types.toJSON();
    }

    return result;
  }
}
