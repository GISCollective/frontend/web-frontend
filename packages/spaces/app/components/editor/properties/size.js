/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class ManageEditorsPropertiesSizingComponent extends Component {
  @tracked _sizing;
  @tracked _proportion = null;
  @tracked _customStyle = {};
  @tracked _containerClasses = null;

  sizingOptions = ["auto", "proportional", "fill", "fixed width", "fixed height"];

  get sizing() {
    if (this._sizing) {
      return this._sizing;
    }

    return this.args.value?.sizing ?? "auto";
  }

  get proportion() {
    if (this._proportion !== null) {
      return this._proportion;
    }

    return this.args.value?.proportion ?? "4:3";
  }

  set proportion(value) {
    this._proportion = value;
    this.triggerChange();
  }

  get customStyle() {
    if (Object.keys(this._customStyle).length > 0) {
      return this._customStyle;
    }

    return this.args.value?.customStyle;
  }

  get containerClasses() {
    if (this._containerClasses) {
      return this._containerClasses;
    }

    return this.args.value?.data?.containerClasses ?? [];
  }

  triggerChange() {
    const newValue = {
      sizing: this.sizing,
    };

    if (this.sizing == "proportional") {
      newValue.proportion = this.proportion;
    }

    if (this.customStyle) {
      newValue.customStyle = this.customStyle;
    }

    if (this.sizing == "fixed width" || this.sizing == "fixed height") {
      newValue.containerClasses = this.containerClasses;
    }

    return this.args.onChange(newValue);
  }

  @action
  containerClassesChanged(value) {
    this._containerClasses = value;
    this.triggerChange();
  }

  @action
  changeCustomStyle(newCustomStyle) {
    this._customStyle = newCustomStyle;

    let autoProp = "height";

    if (this.sizing == "fixed height") {
      autoProp = "width";
    }

    for (const size of ["sm", "md", "lg"]) {
      if (!this._customStyle[size]) {
        this._customStyle[size] = {};
      }

      this._customStyle[size][autoProp] = "auto";
    }

    return this.triggerChange();
  }

  @action
  sizingChanged(value) {
    this._sizing = value;
    this._proportion = "4:3";
    this._customStyle = {};
    this._containerClasses = [];

    this.triggerChange();
  }
}
