/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './base';
import MultiLineString from 'ol/geom/MultiLineString';

export default class MapFeaturesGeojsonMultiLineStringComponent extends Component {
  getGeometry() {
    const coordinates = this.args.value.coordinates?.slice?.() ?? [];
    const geometry = new MultiLineString(coordinates);
    geometry.transform('EPSG:4326', 'EPSG:3857');

    return geometry;
  }
}
