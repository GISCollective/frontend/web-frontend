/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/generic-list", function (hooks) {
  setupRenderingTest(hooks);

  it("renders 0 when there is no value", async function (a) {
    await render(hbs`<Editor::GenericList />`);

    expect(this.element.querySelector(".text-list-count").value).to.equal("0");
  });

  it("renders 1 when there is a list with one item", async function (a) {
    this.set("value", [{}]);

    await render(hbs`<Editor::GenericList @value={{this.value}} />`);

    expect(this.element.querySelector(".text-list-count").value).to.equal("1");
  });

  it("adds an item when the length is increased with one", async function (a) {
    this.set("value", [{}]);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::GenericList @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".text-list-count", "2");

    expect(this.element.querySelector(".text-list-count").value).to.equal("2");
    expect(value).to.deep.equal([{}, null]);
  });

  it("removes an item when the length is decreased by 1", async function (a) {
    this.set("value", [{}]);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::GenericList @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".text-list-count", "0");

    expect(this.element.querySelector(".text-list-count").value).to.equal("0");
    expect(value).to.deep.equal([]);
  });
});
