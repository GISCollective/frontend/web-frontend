/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/background/map', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let map;
  let baseMap1;
  let baseMap2;
  let mapRecord;

  hooks.before(function () {
    server = new TestServer();
    map = server.testData.storage.addDefaultMap('1');
    baseMap1 = server.testData.storage.addDefaultBaseMap('1');
    baseMap2 = server.testData.storage.addDefaultBaseMap('2');
    server.testData.storage.addDefaultPicture();

    baseMap1.attributions = [
      {
        name: '© OpenStreetMap contributors',
        url: 'https://www.openstreetmap.org/copyright',
      },
    ];

    baseMap1['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    baseMap2['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    map.id = map._id;
    map.baseMaps.list = [baseMap1._id, baseMap2._id];
    map.license = {
      name: 'CC BY-NC 4.0',
      url: 'https://creativecommons.org/licenses/by-nc/4.0/',
    };
  });

  hooks.beforeEach(async function () {
    mapRecord = await this.store.findRecord('map', '1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders the map layers when it is using a map source', async function (a) {
    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
        },
        source: {
          useSelectedModel: false,
          model: 'map',
          id: '1',
        },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Background::Map @value={{this.value}} @model={{this.model}}>
            <div style="height: 1px"></div>
          </View::Background::Map>`,
    );

    const olMap = this.element.querySelector('.map').olMap;
    const layers = olMap.getLayers().getArray().length;

    expect(layers).to.equal(2);

    expect(
      this.element.querySelector('.map-layer-base').textContent.trim(),
    ).to.equal(baseMap1._id);
    expect(
      this.element.querySelector('.vector-tiles').textContent.trim(),
    ).to.equal('/api-v1/tiles/{z}/{x}/{y}?format=vt&map=1');
  });

  it('sets the map height to equal the content height', async function () {
    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
        },
        source: {
          useSelectedModel: false,
          model: 'map',
          id: '1',
        },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Background::Map @value={{this.value}} @model={{this.model}}>
        <div style="height: 500px"></div>
      </View::Background::Map>`,
    );

    const contentClientRect = this.element
      .querySelector('.container-bg-map')
      .getBoundingClientRect();

    expect(contentClientRect.height).to.equal(500 / 2);
  });
});
