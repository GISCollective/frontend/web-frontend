/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import click from "@ember/test-helpers/dom/click";

describe("Integration | Component | manage/container-background-layers", function (hooks) {
  setupRenderingTest(hooks);

  let page;
  let pageData;
  let server;

  hooks.beforeEach(async function () {
    server = new TestServer();
    pageData = server.testData.storage.addDefaultPage();
    page = await this.store.findRecord("page", pageData._id);
  });

  it("renders one layer when one is set", async function (a) {
    const col = page.upsertPageColByName("0");

    col.type = "background/color";
    col.data = {
      background: {
        color: "primary",
      },
    };

    this.set("value", page);
    this.set("selectedContainer", { name: 0 });

    await render(
      hbs`<Manage::ContainerBackgroundLayers @value={{this.value}} @selectedContainer={{this.selectedContainer}} />`,
    );

    expect(this.element.querySelector(".selected-color").textContent.trim()).to.equal("primary");
  });

  it("can change a layer type", async function () {
    const col = page.upsertPageColByName("0");

    col.type = "background/color";
    col.data = {
      background: {
        color: "primary",
      },
    };

    this.set("value", page);
    this.set("selectedContainer", { name: 0 });

    let name;
    let data;
    let type;

    this.set("change", (n, d, t) => {
      name = n;
      data = d;
      type = t;
    });

    await render(
      hbs`<Manage::ContainerBackgroundLayers @onChange={{this.change}} @value={{this.value}} @selectedContainer={{this.selectedContainer}} />`,
    );

    await click(".btn-color-danger");

    expect(col.data).to.deep.equal({ background: { color: "danger" } });
    expect(name).to.equal("0");
    expect(data).to.deep.equal({ background: { color: "danger" } });
    expect(type).to.equal("background/color");
  });

  it("renders two layers when they are set", async function () {
    const col1 = page.upsertPageColByName("0");

    col1.type = "background/color";
    col1.data = {
      background: {
        color: "primary",
      },
    };

    page.layoutContainers[0].layers.count = 2;

    this.set("value", page);
    this.set("selectedContainer", { name: 0 });

    await render(
      hbs`<Manage::ContainerBackgroundLayers @value={{this.value}} @selectedContainer={{this.selectedContainer}} />`,
    );

    expect(this.element.querySelector(".selected-color").textContent.trim()).to.equal("primary");
  });
});
