/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class PageColSelectorComponent extends Component {
  get index() {
    const layoutName = this.args.layoutCol?.name ?? '';
    const ignoredTypes = this.args.ignoredTypes ?? [];
    const index = this.args.cols?.findIndex((a) => a.name == layoutName);

    if (index >= 0) {
      return ignoredTypes.includes(this.args.cols[index].type) ? null : index;
    }

    return null;
  }

  get name() {
    return this.args.layoutCol?.name ?? '';
  }

  get col() {
    const index = this.index;

    if (this.args.cols && index >= 0) {
      return this.args.cols[index];
    }

    return null;
  }
}
