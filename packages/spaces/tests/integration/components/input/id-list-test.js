/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | input/id-list", function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam("1");
    server.testData.storage.addDefaultTeam("2");

    server.testData.storage.addDefaultCampaign("1");
    const campaign2 = server.testData.storage.addDefaultCampaign("2");
    const campaign3 = server.testData.storage.addDefaultCampaign("3");

    campaign2.name = "Campaign 2";
    campaign3.name = "Campaign 3";
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("can add a new item", async function (a) {
    this.set("value", []);

    let value;

    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::IdList @model="campaign" @value={{this.value}} @onChange={{this.change}} />`);

    await click(".btn-add-item");

    await selectSearch(".model-id-0", "test");
    await selectChoose(".model-id", "Campaign 1");

    expect(value).to.deep.equal(["1"]);
  });

  it("can add two items", async function () {
    this.set("value", []);

    let value;

    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::IdList @model="campaign" @value={{this.value}} @onChange={{this.change}} />`);

    await click(".btn-add-item");
    await click(".btn-add-item");

    await selectSearch(".model-id-0", "test");
    await selectChoose(".model-id-0", "Campaign 1");

    await selectSearch(".model-id-1", "test");
    await selectChoose(".model-id-1", "Campaign 2");

    expect(value).to.deep.equal(["1", "2"]);
  });

  it("can delete an item", async function () {
    this.set("value", ["1", "2"]);

    let value;

    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::IdList @model="campaign" @value={{this.value}} @onChange={{this.change}} />`);

    await click(".btn-delete");

    expect(value).to.deep.equal(["2"]);
  });
});
