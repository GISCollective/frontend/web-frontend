/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { transform } from 'ol/proj';
import { GeoJson } from 'core/lib/geoJson';
import PositionDetails from 'core/lib/positionDetails';
import { later } from '@ember/runloop';
import { containsCoordinate } from 'ol/extent';

export default class AttributesGroupMapComponent extends Component {
  @service intl;
  @service fullscreen;
  @service mapStyles;
  @service session;

  @tracked editMode = false;
  @tracked _selectedBaseMap;
  @tracked ignoreWebGl;
  @tracked hasUpdatedPos;
  @tracked map;
  @tracked baseMaps;

  get markerMessage() {
    return 'message-map-marker-move';
  }

  get extentLabelPosition() {
    const extent = this.args.extent.toExtent();
    const center = this.args.extent.center.coordinates;

    center[1] = extent[3];
    return center;
  }

  get extentStyle() {
    return this.mapStyles.extent(this.mapStyleElement);
  }

  get outsideExtent() {
    if (
      typeof this.args.extent != 'object' ||
      typeof this.args.position != 'object'
    ) {
      return false;
    }

    const extent = new GeoJson(this.args.extent).toOlFeature().getGeometry();
    const point = new GeoJson(this.args.position)
      .toOlFeature()
      .getGeometry()
      .getCoordinates();

    return !extent.intersectsCoordinate(point);
  }

  @action
  layerSetupError(layer) {
    if (layer.type == 'MapBox') {
      this.ignoreWebGl = true;
    }
  }

  get baseMaps() {
    if (this.ignoreWebGl) {
      return this.args.baseMaps?.filter((a) => !a.needsWebGl) ?? [];
    }

    return this.args.baseMaps ?? [];
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    return this.baseMaps?.[0] ?? null;
  }

  get hasValidPosition() {
    const coordinates = this.args.position?.coordinates;

    if (!coordinates?.length > 0) {
      return false;
    }

    return coordinates[0] && coordinates[1];
  }

  get hasPositionInExtent() {
    if (!this.hasValidPosition) {
      return false;
    }
    const coordinates = this.args.position?.coordinates;
    let extent = this.args.extent?.toExtent?.() ?? [];

    return containsCoordinate(extent, coordinates);
  }

  get mapExtent() {
    if (this.hasPositionInExtent || this.hasUpdatedPos || !this.args.extent) {
      const coordinates = this.args.position?.coordinates ?? [0, 0];

      const radius = 0.005;

      const topRight = [coordinates[0] - radius, coordinates[1] - radius];
      const bottomLeft = [coordinates[0] + radius, coordinates[1] + radius];

      return topRight.concat(bottomLeft);
    }

    if (this.args.extent) {
      const geoJson = new GeoJson(this.args.extent);
      const extent = geoJson.toOlFeature().getGeometry().getExtent();

      return extent;
    }

    return [-114, -41, 42, 72];
  }

  get details() {
    return this.args.details ?? {};
  }

  get position() {
    if (!this.hasValidPosition) {
      return this.args?.campaign?.area?.center;
    }

    return new GeoJson(this.args.position ?? {});
  }

  get bearer() {
    let { access_token } = this.session?.get('data.authenticated');

    if (!access_token) {
      return null;
    }

    return `Bearer ${access_token}`;
  }

  @action
  mapSetup(map) {
    this._map = map;
    this._mapView = map.getView();

    later(() => {
      this.updateCenter();
    });
  }

  @action
  updateCenter() {
    const coordinates = this.position?.coordinates;

    const view = this._map.getView();

    if (!coordinates || !view) {
      return;
    }

    const center = transform(
      coordinates ?? [0, 0],
      'EPSG:4326',
      'EPSG:3857',
    );

    view.setZoom?.(15);
    view.setCenter?.(center);
    this.updateMapSize();
  }

  @action
  edit() {
    this.editMode = true;
    this.fullscreen.freezeBody();

    this.prevPosition = new GeoJson(this.position);
    this.prevDetails = new PositionDetails(this.details);

    if (['feature', 'manual'].includes(this.details.type)) {
      return;
    }

    this.args.onChange?.(
      this.position,
      new PositionDetails({
        ...this.details,
        type: 'manual',
      }),
    );
  }

  @action
  selectResult(result) {
    try {
      const feature = new GeoJson(result.geometry)
        .toOlFeature?.('EPSG:3857')
        .getGeometry();
      if (!feature) {
        return this.search(null);
      }

      this._mapView?.fit?.(feature);

      if (this._mapView?.getZoom?.() > 17) {
        this._mapView.setZoom(17);
      }

      if (this._mapView?.getZoom?.() <= 2) {
        this._mapView.setZoom(5);
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }

    this.search(null);
  }

  @action
  selectBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  search(value) {
    return this.args.onSearch?.(value);
  }

  @action
  confirm() {
    if (this._mapView && this.details.type != 'feature') {
      const center = transform(
        this._mapView.getCenter(),
        'EPSG:3857',
        'EPSG:4326',
      );
      this.args.onChange(
        new GeoJson({ type: 'Point', coordinates: center }),
        new PositionDetails({ ...this.details, type: 'manual' }),
      );
    }

    this.closeFullscreen();
  }

  closeFullscreen() {
    this.editMode = false;
    this.fullscreen.unfreezeBody();
    this.hasUpdatedPos = true;

    this.updateMapSize();
  }

  @action
  dismiss() {
    this.editMode = false;
    this.fullscreen.unfreezeBody();

    this.args.onChange?.(this.prevPosition, this.prevDetails);

    later(() => {
      this.updateMapSize();
    }, 100);
  }

  @action
  updateMapSize() {
    this._map?.updateSize?.();
  }

  @action
  updatePosition(map, view) {
    this._map = map;
    this._mapView = view;
  }

  @action
  checkEditMode() {
    if (
      ['manual', 'feature'].includes(this.args.details?.type) &&
      this.oldType != this.args.details?.type &&
      !this.editMode
    ) {
      later(() => {
        this.edit();
      });
    }

    this.oldType = this.args.details?.type;
  }

  @action
  async setup() {
    if (this.args.ignoreInitialType) {
      this.oldType = this.details.type;
    } else {
      this.checkEditMode();
    }

    if (!this.args.campaign) {
      return;
    }

    try {
      this.map = await this.args.campaign.map.fetch();
    } catch (err) {
      console.error(err);
    }

    try {
      this.baseMaps = (await this.args.campaign.baseMaps?.fetch()) ?? [];
    } catch (err) {
      console.error(err);
    }
  }
}
