/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/background/image', function (hooks) {
  setupRenderingTest(hooks);

  let pictureData;
  let server;
  let receivedPicture;

  hooks.before(function () {
    server = new TestServer();

    receivedPicture = null;
    server.post('/mock-server/pictures', (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = '1';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPicture),
      ];
    });

    pictureData = server.testData.storage.addDefaultPicture();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    const picture = await this.store.findRecord('picture', pictureData._id);

    this.set('model', { picture });
  });

  it('renders all properties when set', async function () {
    this.set('value', {
      modelKey: 'picture',
      data: {
        source: {
          id: pictureData._id,
          model: 'picture',
        },
        options: ['container bg-repeat-no-repeat'],
        style: {
          sm: {
            width: { value: 30, unit: '%' },
            height: { value: 50, unit: '%' },
          },
        },
      },
    });

    await render(hbs`
      <View::Background::Image class="bg-image" @model={{this.model}} @value={{this.value}}>
        template block text
      </View::Background::Image>
    `);

    expect(
      this.element.querySelector('.container-bg-image').textContent.trim(),
    ).to.equal('template block text');
    expect(this.element.querySelector('.container')).to.have.class(
      'bg-repeat-no-repeat',
    );
  });

  it('renders the style element', async function () {
    this.set('value', {
      modelKey: 'picture',
      data: {
        id: pictureData._id,
        model: 'picture',
        options: ['bg-repeat-no-repeat', 'bg-size-fixed'],
        style: {
          sm: {
            'background-size': [
              { value: 30, unit: '%' },
              { value: 50, unit: '%' },
            ],
          },
          md: {
            'background-size': [
              { value: 40, unit: '%' },
              { value: 60, unit: '%' },
            ],
          },
          lg: {
            'background-size': [
              { value: 50, unit: '%' },
              { value: 70, unit: '%' },
            ],
          },
        },
      },
    });

    await render(hbs`
      <View::Background::Image class="bg-image" @model={{this.model}} @value={{this.value}}>
        template block text
      </View::Background::Image>
    `);

    expect(this.element.querySelector('style').textContent).to.contain(
      'background-size: 30% 50%',
    );

    expect(this.element.querySelector('style').textContent).to.contain(
      'background-size: 40% 60%',
    );

    expect(this.element.querySelector('style').textContent).to.contain(
      'background-size: 50% 70%',
    );
  });

  it('renders the content when there is no picture', async function () {
    this.set('value', {});

    await render(hbs`
      <View::Background::Image class="bg-image" @model={{this.model}} @value={{this.value}}>
        template block text
      </View::Background::Image>
    `);

    expect(
      this.element.querySelector('.container-bg-image').textContent.trim(),
    ).to.contain('template block text');
  });
});
