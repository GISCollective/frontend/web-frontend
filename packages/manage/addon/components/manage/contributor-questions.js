/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { A } from 'core/lib/array';

export default class ManageContributorQuestionsComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? [];
  }

  get emailQuestion() {
    const item = this.args.value?.find((a) => a.name == 'email') ?? {};
    const exists = !!this.args.value?.find((a) => a.name == 'email');

    return {
      question: item.question ?? 'What is your email?',
      help:
        item.help ??
        'We need your email to let you know if we have questions about your contribution and to let you know about the progress.',
      type: 'email',
      name: 'email',
      isRequired: exists ? item.isVisible : true,
      isVisible: exists ? item.isVisible : false,
      options: item.options,
      disableDelete: true,
      hasVisibility: true,
    };
  }

  mapQuestion(question) {
    if (question.name == 'email') {
      return this.emailQuestion;
    }

    return question;
  }

  get viewQuestions() {
    return this.questions.filter((a) => a.isVisible);
  }

  get questions() {
    const result = [];

    if (!this.value.find((a) => a.name === 'email')) {
      result.push(this.emailQuestion);
    }

    for(let a of this.value) {
      result.push(this.mapQuestion(a));
    }

    return A(result);
  }

  @action
  save() {
    return this.args?.onSave(this.value);
  }

  @action
  cancel() {
    this._value = undefined;

    return this.args?.onCancel();
  }

  @action
  changeValue(value) {
    this._value = value;
  }
}
