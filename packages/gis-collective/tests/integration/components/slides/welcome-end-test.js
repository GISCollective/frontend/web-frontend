/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | slides/welcome-end',  function (hooks) {
  setupRenderingTest(hooks);

  it('triggers onPrevPage when the back button is pressed', async function () {
    let called;

    this.set('prevPage', () => {
      called = true;
    });

    await render(hbs`<Slides::WelcomeEnd @onPrevPage={{this.prevPage}}/>`);
    await click('.btn-back');

    expect(called).to.equal(true);
  });
});
