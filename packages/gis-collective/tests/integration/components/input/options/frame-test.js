/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/options/frame',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the content', async function () {
    await render(hbs`
      <Input::Options::Frame>
        template block text
      </Input::Options::Frame>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
  });

  it('renders the title', async function () {
    await render(hbs`
      <Input::Options::Frame @title="title">
        template block text
      </Input::Options::Frame>
    `);

    expect(this.element.querySelector('.header').textContent.trim()).to.equal(
      'title'
    );
  });

  it('can toggle the close state', async function () {
    await render(hbs`
      <Input::Options::Frame @title="title">
        template block text
      </Input::Options::Frame>
    `);

    expect(this.element.querySelector('.input-options-frame.closed')).not.to
      .exist;

    await click('.header');
    expect(this.element.querySelector('.input-options-frame.closed')).to.exist;

    await click('.btn-close-frame');
    expect(this.element.querySelector('.input-options-frame.closed')).not.to
      .exist;
  });
});
