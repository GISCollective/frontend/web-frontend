/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class UserChangePasswordComponent extends Component {
  @service modal;
  @tracked _isValid = false;
  @tracked _newPassword;
  @tracked _newPassword2;

  get newPassword() {
    if (this._newPassword === undefined) {
      return this.args.newPassword ?? '';
    }

    return this._newPassword;
  }

  get newPassword2() {
    if (this._newPassword2 === undefined) {
      return this.args.newPassword2 ?? '';
    }

    return this._newPassword2;
  }

  get isValid() {
    if (this.newPassword != this.newPassword2) {
      return false;
    }

    if (this.newPassword.trim().length < 10) {
      return false;
    }

    return true;
  }

  @action
  change(value, newPassword, newPassword2) {
    this._newPassword = newPassword;
    this._newPassword2 = newPassword2;
  }

  @action
  validationChanged(newValue) {
    this._isValid = newValue;
  }

  @action
  async update() {
    const password = await this.modal.confirmWithPassword('change password', 'Enter your current password to continue.');

    return this.args.onUpdate?.(password, this.newPassword);
  }
}
