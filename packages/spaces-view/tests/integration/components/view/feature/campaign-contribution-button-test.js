/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import chai, { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/feature/campaign-contribution-button', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there are no arguments', async function () {
    await render(hbs`<View::Feature::CampaignContributionButton />`);
    chai.expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a button when the label and destination are set', async function () {
    this.set('value', {
      data: {
        source: { model: 'campaign', id: '1' },
        button: {
          label: 'some label',
          destination: '/campaigns/:campaign_id',
        },
        style: {
          classes: ['btn-primary', 'size-btn-lg'],
        },
      },
    });

    await render(
      hbs`<View::Feature::CampaignContributionButton @value={{this.value}} />`,
    );

    expect(this.element.querySelector('a').textContent.trim()).to.equal(
      'some label',
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'class',
      'btn btn-primary size-btn-lg',
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/campaigns/1',
    );
  });

  it('adds the selected feature when it is loaded in the model', async function () {
    this.set('value', {
      data: {
        source: { model: 'campaign', id: '1' },
        button: {
          label: 'some label',
          destination: '/campaigns/:campaign_id',
        },
        style: {
          classes: ['btn-primary', 'size-btn-lg'],
        },
      },
    });

    this.set('model', {
      selectedType: 'feature',
      selectedId: '5ca8c2ecef1f7e01000886f3',
    });

    await render(
      hbs`<View::Feature::CampaignContributionButton @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/campaigns/1?s=fid@5ca8c2ecef1f7e01000886f3',
    );
  });

  it('adds the state feature when the state has one', async function () {
    this.set('value', {
      data: {
        source: { model: 'campaign', id: '1' },
        button: {
          label: 'some label',
          destination: '/campaigns/:campaign_id',
        },
        style: {
          classes: ['btn-primary', 'size-btn-lg'],
        },
      },
    });

    this.set('state', 'fid@1234');

    await render(
      hbs`<View::Feature::CampaignContributionButton @state={{this.state}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/campaigns/1?s=fid@1234',
    );
  });
});
