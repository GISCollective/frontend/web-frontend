/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class TeamAdapter extends Adapter {
  get modelName() {
    return 'team';
  }

  invite(id, emailList, role) {
    const url = this.buildURL('team', id) + '/invite';
    return this.ajax(url, 'POST', { data: { emailList, role } });
  }
}
