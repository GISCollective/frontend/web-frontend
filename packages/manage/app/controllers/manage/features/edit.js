/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { debounce } from '@ember/runloop';
import IconsSubSet from 'models/lib/icons-sub-set';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { imageListUpload } from 'core/lib/image-list-upload';

export default class ManageSitesEditController extends Controller {
  queryParams = ['parentMap'];

  @service store;
  @service notifications;
  @service intl;
  @service fastboot;

  @tracked editableField;
  @tracked savingField;
  @tracked iconsSubset;

  setupIconsSubsets() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    if (!this.iconsSubset) {
      this.iconsSubset = new IconsSubSet();
    }

    this.iconsSubset.feature = this.model.feature;
    this.iconsSubset.store = this.store;
    this.iconsSubset.update();
  }

  get disabled() {
    return this.fastboot.isFastBoot || this.model.feature.isSaving;
  }

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: this.model.team ? 'manage.dashboards.dashboard' : undefined,
        text: this.model.team?.name ?? 'unknown',
        model: this.model.team?.id,
      },
      {
        route: this.model.parentMap?.name ? 'manage.dashboards.map' : undefined,
        text: this.model.parentMap?.name ?? 'unknown',
        model: this.model.parentMap?.id,
      },
      {
        text: this.model.feature.name,
      },
    ];
  }

  progress(picture, percentage) {
    picture.progress = percentage;
  }

  @action
  async updatePictures(pictures) {
    this.savingField = this.editableField;

    try {
      await imageListUpload(this.model.feature, pictures, 'pictures');
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.editableField = '';
    this.savingField = '';
  }

  @action
  async updateSounds(sounds) {
    this.savingField = this.editableField;

    const items = sounds.map((a) => a.save());
    try {
      await Promise.all(items);
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.model.feature.sounds = sounds;
    await this.model.feature.save();

    this.editableField = '';
    this.savingField = '';
  }

  @action
  imageChange(picture) {
    if (picture.isNew) return;

    return picture.save();
  }

  get editTitleAndDescription() {
    return this.editableField == 'titleAndDescription';
  }

  get editLocation() {
    return this.editableField == 'location';
  }

  get savingTitleAndDescription() {
    return this.savingField == 'titleAndDescription';
  }

  get savingLocation() {
    return this.savingField == 'location';
  }

  get savingIcons() {
    return this.savingField == 'icons';
  }

  get niceMapList() {
    return this.model.feature.maps.map((a) => a.get('name')).join(', ');
  }

  get niceIsPublished() {
    return this.model.feature.isPublished ? this.intl.t('yes') : this.intl.t('no');
  }

  performIconSearch(term, resolve, reject) {
    this.store
      .query('icon', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.feature
      .save({
        adapterOptions: {
          validation: false,
        },
      })
      .then(
        () => {
          if (this.savingField == 'Maps') {
            this.iconsSubset.update();
          }

          this.savingField = '';
        },
        (err) => {
          this.model.feature.rollbackAttributes();
          this.notifications.handleError(err);
          this.savingField = '';
        },
      );
  }

  @action
  saveMaps(title, value) {
    this.model.feature.maps = value;

  if (value.length) {
    this.model.area = value[0].area;
  }

    return this.save();
  }

  @action
  edit(key) {
    this.editableField = key;
  }

  @action
  searchIcons(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performIconSearch, term, resolve, reject, 600);
    });
  }

  @action
  iconsChanged(value) {
    this.model.feature.icons = value;
    this.editableField = 'icons';

    return this.save();
  }

  @action
  cancel() {
    this.model.feature.restore();
    this.editableField = '';
  }

  @action
  saveVisibility(newValue) {
    this.model.feature.visibility = newValue;
    this.editableField = this.intl.t('visibility');

    return this.save();
  }

  @action
  saveArticle(name, description) {
    this.model.feature.name = name;
    this.model.feature.description = description;

    return this.save();
  }

  @action
  savePosition(value) {
    this.model.feature.position = value;
    return this.save();
  }

  @action
  saveAttributes(value) {
    this.model.feature.attributes = value;
    return this.save();
  }

  @action
  onDelete() {
    return this.model.feature
      .destroyRecord()
      .then(() => {
        if (this.parentMap) {
          this.router.transitionTo('browse.sites', {
            queryParams: {
              map: this.model.parentMap.id,
            },
          });

          return true;
        }

        this.router.transitionTo('browse.sites');
        return true;
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
