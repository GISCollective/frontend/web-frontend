/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import {
  setupApplicationTest as upstreamSetupApplicationTest,
  setupRenderingTest as upstreamSetupRenderingTest,
  setupTest as upstreamSetupTest,
} from 'ember-qunit';
import QUnit from 'qunit';
import TestServer from 'models/test-support/gis-collective/test-server';

// This file exists to provide wrappers around ember-qunit's
// test setup functions. This way, you can easily extend the setup that is
// needed per test type.

function setupApplicationTest(hooks, options) {
  QUnit.config.testTimeout = 15000;
  QUnit.config.failOnZeroTests = false;
  QUnit.testStart((details) => {
    console.log(`Now running: ${details.module} ${details.name}`);
  });

  upstreamSetupApplicationTest(hooks, options);

  hooks.beforeEach(function (assert) {
    assert.ok(1);
  });

  // Additional setup for application tests can be done here.
  //
  // For example, if you need an authenticated session for each
  // application test, you could do:
  //
  // hooks.beforeEach(async function () {
  //   await authenticateSession(); // ember-simple-auth
  // });
  //
  // This is also a good place to call test setup functions coming
  // from other addons:
  //
  // setupIntl(hooks, 'en-us'); // ember-intl
  // setupMirage(hooks); // ember-cli-mirage
}

function setupRenderingTest(hooks, options) {
  QUnit.config.testTimeout = 15000;
  QUnit.config.failOnZeroTests = false;
  QUnit.testStart((details) => {
    console.log(`Now running: ${details.module} ${details.name}`);
  });

  upstreamSetupRenderingTest(hooks, options);

  hooks.beforeEach(function (assert) {
    const intl = this.owner.lookup('service:intl');
    intl.setLocale('en-us');

    this.store = this.owner.lookup('service:store');
    this.router = this.owner.lookup('service:router');

    if (options?.server) {
      this.server = new TestServer();
    }

    assert.ok(1);
  });

  // Additional setup for rendering tests can be done here.
}

function setupTest(hooks, options) {
  QUnit.config.testTimeout = 15000;
  QUnit.config.failOnZeroTests = false;
  QUnit.testStart((details) => {
    console.log(`Now running: ${details.module} ${details.name}`);
  });

  upstreamSetupTest(hooks, options);

  hooks.beforeEach(function (assert) {
    assert.ok(1);
  });

  // Additional setup for unit tests can be done here.
}

import { module, test } from 'qunit';

function wait(duration) {
  return new Promise((resolve) => {
    setTimeout(resolve, duration);
  });
}

export { setupApplicationTest, setupRenderingTest, setupTest, module as describe, test as it, wait };
