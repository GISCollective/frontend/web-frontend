'use strict';

const browsers = [
  'last 50 Chrome versions',
  'last 50 Firefox versions',
  'last 10 Safari versions',
];

module.exports = {
  browsers,
};
