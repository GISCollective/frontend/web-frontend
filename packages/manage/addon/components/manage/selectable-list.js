/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { A } from 'core/lib/array';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { buildWaiter } from '@ember/test-waiters';

let waiter = buildWaiter('selectable-list-waiter');

export default class ManageSelectableListComponent extends Component {
  @service intl;
  @service modal;

  @tracked selection = A();

  get allValues() {
    if (Array.isArray(this.args.value?.items)) {
      return this.args.value.items;
    }

    return this.args.value ?? [];
  }

  get allSelected() {
    return this.selection == 'all';
  }

  set allSelected(value) {
    this.selection = value ? 'all' : A();
  }

  get canSelectAll() {
    return this.allValues.length > 1 || this.selection == 'all';
  }

  get selectedValues() {
    if (this.allSelected) {
      return this.allValues;
    }

    return this.selection?.map?.((a) => this.allValues.find((b) => b.id == a))?.filter?.((a) => a) ?? [];
  }

  toActionObject(name) {
    if (name == 'publish') {
      return {
        name: this.intl.t('publish'),
        key: 'publish',
        icon: 'eye',
        class: 'btn-secondary',
      };
    }

    if (name == 'unpublish') {
      return {
        name: this.intl.t('unpublish'),
        key: 'unpublish',
        icon: 'eye-slash',
        class: 'btn-secondary',
      };
    }

    if (name == 'pending') {
      return {
        name: this.intl.t('set as pending'),
        key: 'pending',
        icon: 'clock',
        class: 'btn-secondary',
      };
    }
  }

  get actions() {
    const result = [];

    for (const value of this.selectedValues) {
      for (const action of value.availableActions ?? []) {
        if (!result.includes(action)) {
          result.push(action);
        }
      }
    }

    return result.map((a) => this.toActionObject(a)).filter((a) => a);
  }

  get firstRecord() {
    return this.allValues[0];
  }

  get modelName() {
    return this.firstRecord?.constructor?.modelName;
  }

  get value() {
    let result = this.args.value ?? [];

    if (Array.isArray(this.args.value?.items)) {
      result = this.args.value.items;
    }

    return result.filter((a) => !a.isDeleted);
  }

  get isEmpty() {
    return !this.value?.length;
  }

  async doWithWaiter(fn) {
    let token = waiter.beginAsync();

    let result;

    try {
      result = await fn();
    } catch (err) {
      throw err;
    } finally {
      waiter.endAsync(token);
    }

    return result;
  }

  @action
  async action(actionName) {
    if (actionName == 'deleteAll') {
      return this.deleteAll();
    }

    return this.doWithWaiter(async () => {
      if (this.allSelected) {
        await this.selectedValues[0][`${actionName}Many`](this.args.query);
        return this.args.reload?.();
      }

      let promises = [];

      for (const value of this.selectedValues) {
        promises.push(value[actionName]());
      }

      await Promise.all(promises);

      return this.args.reload?.();
    });
  }

  @action
  async deleteAll() {
    try {
      await this.modal.confirm(
        this.intl.t(`delete all ${this.modelName}s`),
        this.intl.t(`delete-all-confirmation-message-${this.modelName}`),
      );
    } catch (err) {
      return false;
    }

    return this.doWithWaiter(async () => {
      await this.firstRecord.deleteMany(this.args.query);
      await this.args.reload?.();
    });
  }

  @action
  async delete() {
    try {
      const name = this.allValues?.find((a) => a.id == this.selection[0]).name ?? '';

      await this.modal.confirm(
        this.intl.t(`delete ${this.modelName}`, { size: this.selection.length }),
        this.intl.t('delete-confirmation-message', {
          size: this.selection.length,
          name,
        }),
      );
    } catch (err) {
      return false;
    }

    return this.doWithWaiter(async () => {
      const promises = this.selection.map((id) => this.allValues.find((a) => a.id == id)).map((a) => a.destroyRecord());

      await Promise.all(promises);
      await this.args.reload?.();
    });
  }

  @action
  onSelect(value) {
    this.selection = value;
  }
}
