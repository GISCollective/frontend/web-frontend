/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | model info', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let transform = this.owner.lookup('transform:model-info');
    expect(transform).to.be.ok;
  });

  it('can deserialize a null', function () {
    let transform = this.owner.lookup('transform:model-info');

    const result = transform.deserialize(null).toJSON();

    expect(result).to.deep.equal({});
  });

  it('can deserialize an object', function () {
    let transform = this.owner.lookup('transform:model-info');

    const result = transform
      .deserialize({
        author: '1',
        changeIndex: 2,
        createdOn: '2010-08-11T14:56:36Z',
        lastChangeOn: '2020-08-11T14:56:36Z',
        originalAuthor: '3',
      })
      .toJSON();

    expect(result).to.deep.equal({
      author: '1',
      changeIndex: 2,
      createdOn: '2010-08-11T14:56:36.000Z',
      lastChangeOn: '2020-08-11T14:56:36.000Z',
      originalAuthor: '3',
    });
  });

  it('can deserialize an object with invalid data', function () {
    let transform = this.owner.lookup('transform:model-info');

    const result = transform
      .deserialize({
        author: {},
        changeIndex: 'invalid',
        createdOn: 'invalid',
        lastChangeOn: 'invalid',
        originalAuthor: [],
      })
      .toJSON();

    expect(result).to.deep.equal({});
  });
});
