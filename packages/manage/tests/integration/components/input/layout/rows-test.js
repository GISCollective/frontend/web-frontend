/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { LayoutContainer } from 'models/transforms/layout-container-list';

describe('Integration | Component | input/layout/rows', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let page;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultPicture('1');

    page = {
      layoutContainers: [
        new LayoutContainer({
          rows: [
            {
              options: [],
              cols: [
                {
                  type: 'type',
                  data: {},
                  options: [],
                },
                {
                  type: 'type',
                  data: {},
                  options: [],
                },
              ],
            },
          ],
          options: [],
        }),
      ],
    };
    this.set('page', page);
  });

  it('allows adding a row when there is nothing', async function () {
    let value;
    page.addLayout = (a, b, c) => {
      value = [a, b, c];
    };

    page.layoutContainers[0].rows = [];

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onLayoutChange={{this.onLayoutChange}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    await click('.btn-add-first-row');

    expect(value).to.deep.equal([0, 0, 0]);
  });

  it('allows adding a row after the first row', async function () {
    let value;
    page.addLayout = (a, b, c) => {
      value = [a, b, c];
    };

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onLayoutChange={{this.onLayoutChange}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    await click('.btn-add-row-1');

    expect(value).to.deep.equal([+0, 1, +0]);
  });

  it('allows adding a row before the first row', async function () {
    let addLayoutRowBefore;
    page.addLayoutRowBefore = (a) => {
      addLayoutRowBefore = a;
    };
    let addLayout;
    page.addLayout = (a, b, c) => {
      addLayout = [a, b, c];
    };

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onLayoutChange={{this.onLayoutChange}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    await click('.row');
    await click('.btn-add-row-0');

    expect(addLayoutRowBefore).to.deep.equal(0);
    expect(addLayout).to.deep.equal([0, 0, 0]);
  });

  it('allows adding a col on the selected row', async function () {
    let value;
    page.addLayout = (a, b, c) => {
      value = [a, b, c];
    };

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onLayoutChange={{this.onLayoutChange}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    await click('.row');
    await click('.btn-add-col');

    expect(value).to.deep.equal([0, 0, 2]);
  });

  it('deletes the row when the last col is deleted', async function () {
    let value;
    page.deleteCol = (a, b, c) => {
      value = [a, b, c];
    };

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onLayoutChange={{this.onLayoutChange}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    await click('.row');
    await click('.btn-delete-col');
    await click('.btn-delete-col');

    expect(value).to.deep.equal([0, 0, 1]);
  });

  it('shows the first and second add row button when the first row is selected', async function () {
    this.set('rows', [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);

    await render(hbs`<Input::Layout::Rows @containerIndex={{0}} @selectionId="0.0.0" @page={{this.page}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    const rows = this.element.querySelectorAll('.row');

    expect(this.element.querySelector('.row-layout-settings.show')).not.to.exist;
    await click(rows[0]);

    expect(this.element.querySelectorAll('.row-layout-settings.show')).to.have.length(2);
    expect(this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-0')).to.have.length(1);
    expect(this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-1')).to.have.length(1);
  });

  it('shows the last two add row buttons when the last row is selected', async function () {
    page.layoutContainers[0].rows = [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ];

    await render(hbs`<Input::Layout::Rows @selectionId="0.0.0" @page={{this.page}} @containerIndex={{0}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    const rows = this.element.querySelectorAll('.row:not(.row-hidden)');

    expect(this.element.querySelector('.row-layout-settings.show')).not.to.exist;
    await click(rows[2]);

    expect(this.element.querySelectorAll('.row-layout-settings.show')).to.have.length(2);
    expect(this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-2')).to.have.length(1);
    expect(this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-3')).to.have.length(1);
  });

  it('shows the first row as selected when is clicked', async function () {
    this.set('rows', [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);

    await render(hbs`<Input::Layout::Rows @containerIndex={{0}} @selectionId="0.0.0" @page={{this.page}} @onLayoutChange={{this.onLayoutChange}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    const rows = this.element.querySelectorAll('.row');

    expect(this.element.querySelectorAll('.row-selected')).to.have.length(0);

    await click(rows[0]);

    expect(this.element.querySelectorAll('.row-selected')).to.have.length(1);
  });

  it('can select a col with a click', async function () {
    page.layoutContainers[0].rows = [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ];

    let type;
    let location;
    let col;

    this.set('onSelect', (t, l, c) => {
      type = t;
      location = l;
      col = c;
    });

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onSelect={{this.onSelect}} as | col |>{{col.data.id}}
</Input::Layout::Rows>`);

    const cols = this.element.querySelectorAll('.layout-col-editable');

    expect(this.element.querySelectorAll('.col-selected')).to.have.length(0);

    await click(cols[1]);

    expect(type).to.equal('col');
    expect(location).to.deep.equal([0, 1, 0]);
    expect(col).to.equal(page.layoutContainers[0].rows[1].cols[0]);
  });

  it('triggers the onSelect even when the edit row button is clicked', async function () {
    page.layoutContainers[0].rows = [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ];

    let type;
    let location;
    let row;

    this.set('onSelect', (t, l, c) => {
      type = t;
      location = l;
      row = c;
    });

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onSelect={{this.onSelect}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    const btns = this.element.querySelectorAll('.btn-edit-row');
    await click(btns[1]);

    expect(type).to.equal('row');
    expect(location).to.deep.equal([0, 1]);
    expect(row).to.equal(page.layoutContainers[0].rows[1]);
  });

  it('triggers the onSelect when the row-hidden element is clicked', async function () {
    page.layoutContainers[0].rows = [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ];

    let type;
    let location;
    let row;

    this.set('onSelect', (t, l, c) => {
      type = t;
      location = l;
      row = c;
    });

    await render(hbs`<Input::Layout::Rows @page={{this.page}} @containerIndex={{0}} @onSelect={{this.onSelect}} as | col |>
  {{col.data.id}}
</Input::Layout::Rows>`);

    const btns = this.element.querySelectorAll('.row-hidden');
    await click(btns[1]);

    expect(type).to.equal('row');
    expect(location).to.deep.equal([0, 1]);
    expect(row).to.equal(page.layoutContainers[0].rows[1]);
  });
});
