/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe ('Unit | Controller | campaigns/index',  function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let controller = this.owner.lookup('controller:campaigns/index');
    expect(controller).to.be.ok;
  });
});
