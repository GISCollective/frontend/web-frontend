/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';

export default class ManageTeamsEditRoute extends AuthenticatedRoute {
  model(params) {
    if (this.isFastBoot) {
      return null;
    }

    return this.store.findRecord('team', params.id);
  }
}
