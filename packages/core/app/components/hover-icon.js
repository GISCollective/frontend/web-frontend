/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { later, cancel } from '@ember/runloop';
import { service } from '@ember/service';

export default class HoverIconComponent extends Component {
  @tracked imageStyle;

  @tracked containerSize = 0;
  @tracked imageSize = 0;

  @tracked icon;
  @service store;

  get containerStyle() {
    const border = this.args.overlay?.isActive ? 3 : 1;
    return htmlSafe(
      `border-width: ${border}px;height: ${this.containerSize}px; width: ${this.containerSize}px; top: -${
        this.containerSize / 2
      }px; left: -${this.containerSize / 2}px; opacity: 1;`,
    );
  }

  @action
  handleClick() {
    return this.args.onClick?.();
  }

  get iconId() {
    return this.args.overlay?.icon;
  }

  @action
  async setup() {
    if (this.args.overlay) {
      this.args.overlay.hide = () => this.hide();
    }

    if (!this.iconId) {
      later(() => {
        this.imageSize = 0;
        this.containerSize = 50;
      }, 10);
      this.icon = null;
      return;
    }

    if (this.iconId) {
      this.icon = this.store.peekRecord('icon', this.iconId);

      if (!this.icon) {
        this.icon = await this.store.findRecord('icon', this.iconId);
      }
    }

    later(() => {
      this.show();
    }, 10);
  }

  @action
  hide() {
    this.containerSize = 0;

    cancel(this.time);
    this.time = later(() => {
      this.args?.onRemove();
    }, 200);
  }

  @action
  show() {
    this.imageSize = (this.icon?.imageStyles?.site?.size ?? 20) * 2;
    this.containerSize = this.imageSize * 1.5;
  }

  willDestroy() {
    super.willDestroy(...arguments);
    cancel(this.time);
  }
}
