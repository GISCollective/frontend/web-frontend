/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupRenderingTest, wait } from 'ogm/tests/helpers';
import { click, fillIn, render, triggerKeyEvent, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | search component',  function (hooks) {
  setupRenderingTest(hooks, { server: true });

  let modelList = ['Feature', 'Map', 'Team', 'Campaign', 'IconSet', 'Icon', 'Event'];

  let map;
  let picture;
  let team;
  let campaign;
  let feature;
  let iconSet;
  let icon;
  let space;
  let page;
  let event;

  hooks.beforeEach(async function () {
    map = this.server.testData.storage.addDefaultMap();
    picture = this.server.testData.storage.addDefaultPicture();
    team = this.server.testData.storage.addDefaultTeam();
    campaign = this.server.testData.storage.addDefaultCampaign();
    feature = this.server.testData.storage.addDefaultFeature();
    iconSet = this.server.testData.storage.addDefaultIconSet();
    icon = this.server.testData.storage.addDefaultIcon();
    space = this.server.testData.storage.addDefaultSpace();
    page = this.server.testData.storage.addDefaultPage();
    event = this.server.testData.storage.addDefaultEvent();

    this.server.testData.storage.addDefaultGeocoding();

    this.server.get(`/mock-server/spaces/:id/categories`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({}),
      ];
    });

    const spaceService = this.owner.lookup('service:space');
    spaceService.currentSpace = space;
  });

  describe ('when the server can return all model types',  function (hooks) {
    hooks.beforeEach(async function () {
      this.server.testData.storage.addSearchMeta('1', {
        title: 'feature title',
        description: 'feature description',

        relatedId: feature._id,
        relatedModel: 'Feature',
        pictures: [picture._id],
      });

      this.server.testData.storage.addSearchMeta('2', {
        title: 'map title',
        description: 'map description',

        relatedId: map._id,
        relatedModel: 'Map',
        cover: picture._id,
      });

      this.server.testData.storage.addSearchMeta('3', {
        title: 'team title',
        description: 'team description',

        relatedId: team._id,
        relatedModel: 'Team',
        logo: picture._id,
      });

      this.server.testData.storage.addSearchMeta('4', {
        title: 'campaign title',
        description: 'campaign description',

        relatedId: campaign._id,
        relatedModel: 'Campaign',
        logo: picture._id,
      });

      this.server.testData.storage.addSearchMeta('5', {
        title: 'icon set title',
        description: 'icon set description',

        relatedId: iconSet._id,
        relatedModel: 'IconSet',
        logo: picture._id,
      });

      this.server.testData.storage.addSearchMeta('6', {
        title: 'icon title',
        description: 'icon description',

        relatedId: icon._id,
        relatedModel: 'Icon',
        logo: picture._id,
      });

      this.server.testData.storage.addSearchMeta('7', {
        title: 'event title',
        description: 'event description',

        relatedId: event._id,
        relatedModel: 'Event',
        logo: picture._id,
      });

      this.server.testData.storage.addDefaultCalendar();
      this.server.testData.storage.addDefaultEvent("event-0");
    });

    it('queries nothing when there is no space provided', async function () {
      await render(hbs`
        <Search />
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      expect(this.server.history).to.deep.equal([]);
    });

    it('renders the features', async function () {
      this.set('space', {
        searchOptions: {
          features: true,
          maps: false,
          campaigns: false,
          iconSets: false,
          teams: false,
          icons: false,
          geocodings: false,
          events: false,
        },
        getLinkFor: () => ""
      });

      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const features = [...this.element.querySelectorAll('.feature-list .search-result-item')];

      expect(features.length).to.equal(1);

      expect(features[0].querySelector('h3').textContent.trim()).to.equal('feature title');
      expect(features[0].querySelector('p').textContent.trim()).to.equal('feature description');

      expect(this.server.history).to.deep.equal([
        'GET /mock-server/searchmetas?limit=6&model=Feature&q=my%20search%20terms&skip=0',
      ]);
    });

    it('renders the maps', async function () {
      this.set('space', {
        searchOptions: {
          maps: true,
        },
        getLinkFor: () => ""
      });

      this.server.history = [];
      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const maps = [...this.element.querySelectorAll('.map-list .search-result-item')];

      expect(maps.length).to.equal(1);

      expect(maps[0].querySelector('h3').textContent.trim()).to.equal('map title');
      expect(maps[0].querySelector('p').textContent.trim()).to.equal('map description');
      expect(this.server.history).to.deep.equal([
        'GET /mock-server/searchmetas?limit=6&model=Map&q=my%20search%20terms&skip=0',
        'GET /mock-server/pictures/5cc8dc1038e882010061545a',
      ]);
    });

    it('renders the campaigns', async function () {
      this.set('space', {
        searchOptions: {
          campaigns: true,
        },
        getLinkFor: () => ""
      });

      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const campaigns = [...this.element.querySelectorAll('.campaign-list .search-result-item')];

      expect(campaigns.length).to.equal(1);

      expect(campaigns[0].querySelector('h3').textContent.trim()).to.equal('campaign title');
      expect(campaigns[0].querySelector('p').textContent.trim()).to.equal('campaign description');
    });

    it('renders the icon sets', async function () {
      this.set('space', {
        searchOptions: {
          iconSets: true,
        },
        getLinkFor: () => ""
      });

      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const iconSets = [...this.element.querySelectorAll('.icon-set-list .search-result-item')];

      expect(iconSets.length).to.equal(1);

      expect(iconSets[0].querySelector('h3').textContent.trim()).to.equal('icon set title');
      expect(iconSets[0].querySelector('p').textContent.trim()).to.equal('icon set description');
    });

    it('renders the teams', async function () {
      this.set('space', {
        searchOptions: {
          teams: true,
        },
        getLinkFor: () => ""
      });

      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const teams = [...this.element.querySelectorAll('.team-list .search-result-item')];

      expect(teams.length).to.equal(1);

      expect(teams[0].querySelector('h3').textContent.trim()).to.equal('team title');
      expect(teams[0].querySelector('p').textContent.trim()).to.equal('team description');
    });

    it('renders the icons', async function () {
      this.set('space', {
        searchOptions: {
          icons: true,
        },
        getLinkFor: () => ""
      });

      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const icons = [...this.element.querySelectorAll('.icon-list .search-result-item')];

      expect(icons.length).to.equal(1);

      expect(icons[0].querySelector('h3').textContent.trim()).to.equal('icon title');
      expect(icons[0].querySelector('p').textContent.trim()).to.equal('icon description');
    });

    it('renders the events', async function () {
      this.set('space', {
        searchOptions: {
          events: true,
        },
        getLinkFor: () => ""
      });

      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const events = [...this.element.querySelectorAll('.event-list .search-result-item')];

      expect(events.length).to.equal(1);

      expect(events[0].querySelector('h3').textContent.trim()).to.equal('event title');
      expect(events[0].querySelector('p').textContent.trim()).to.equal('event description');
    });

    it('does not show the not found message', async function () {
      await render(hbs`
        <Search />
      `);

      this.server.history = [];
      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      expect(this.element.querySelector('.text-no-search-results')).not.to.exist;
    });

    it('triggers a search action when enter is pressed', async function () {
      this.set('space', {
        searchOptions: {
          icons: true,
        },
        getLinkFor: () => ""
      });

      await render(hbs`<Search @space={{this.space}}/>`);

      this.server.history = [];
      await fillIn('.text-search', 'some search');
      await triggerKeyEvent('.text-search', 'keyup', 'Enter');

      expect(this.element.querySelector('.search-meta-list')).to.exist;
    });
  });

  describe ('when the server returns enough features for each model',  function (hooks) {
    hooks.beforeEach(async function () {
      this.set('space', {
        searchOptions: {
          features: true,
          maps: true,
          campaigns: true,
          iconSets: true,
          teams: true,
          icons: true,
          geocodings: true,
          events: true,
        },
        getLinkFor: () => ""
      });

      for (const relatedModel of modelList) {
        for (let i = 0; i < 6; i++) {
          this.server.testData.storage.addSearchMeta(`${relatedModel}-${i}`, {
            title: `title ${i}`,
            description: 'description',

            relatedId: `${relatedModel}-${i}`,
            relatedModel,
            cover: [picture._id],
          });
        }
      }

      for (let i = 0; i < 6; i++) {
        this.server.testData.storage.addDefaultEvent(`event-${i}`);
        this.server.testData.storage.addDefaultIcon(`icon-${i}`);
      }

      this.server.testData.storage.addDefaultCalendar();
    });

    it('enables the search more on features', async function () {
      await render(hbs`
        <Search @space={{this.space}} />
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      await waitFor('.feature-list .btn-load-more');

      let features = [...this.element.querySelectorAll('.feature-list .search-result-item')];

      expect(features).to.have.length(5);

      await click('.feature-list .btn-load-more');

      features = [...this.element.querySelectorAll('.feature-list .search-result-item')];
      expect(features).to.have.length(11);
    });

    it('enables the search more on maps', async function () {
      await render(hbs`
        <Search  @space={{this.space}}/>
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      await waitFor('.feature-list .btn-load-more');

      let maps = [...this.element.querySelectorAll('.map-list .search-result-item')];

      expect(maps).to.have.length(5);

      await click('.map-list .btn-load-more');

      maps = [...this.element.querySelectorAll('.map-list .search-result-item')];
      expect(maps).to.have.length(11);
    });
  });

  describe ('when there are no search results',  function (hooks) {
    it('shows a nice message', async function () {
      this.set('space', {
        searchOptions: {
          features: true,
          maps: true,
          campaigns: true,
          iconSets: true,
          teams: true,
          icons: true,
          geocodings: true,
          events: true,
        },
      });

      await render(hbs`
        <Search @space={{this.space}} />
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      expect(this.element.querySelector('.text-no-search-results').textContent.trim()).to.equal(
        'Unfortunately we could not find any results for « my search terms »'
      );
    });
  });

  describe ('when the mapId property is set',  function (hooks) {
    hooks.beforeEach(async function () {
      this.set('space', {
        searchOptions: {
          features: true,
          maps: true,
          campaigns: true,
          iconSets: true,
          teams: true,
          icons: true,
          geocodings: true,
          events: true,
        },
        getLinkFor: () => ""
      });

      for (const relatedModel of modelList) {
        for (let i = 0; i < 6; i++) {
          this.server.testData.storage.addSearchMeta(`${relatedModel}-${i}`, {
            title: `title ${i}`,
            description: 'description',

            relatedId: `${relatedModel}-${i}`,
            relatedModel,
            cover: [picture._id],
          });
        }
      }
    });

    it('renders the map name in the search field', async function () {
      this.set('mapId', map._id);

      await render(hbs`
        <Search @mapId={{this.mapId}} @space={{this.space}} />
      `);

      expect(this.element.querySelector('.map-name-text').textContent.trim()).to.equal('Harta Verde București');
    });

    it('search for features and geo codings when the map id is set', async function () {
      this.set('mapId', map._id);

      await render(hbs`
        <Search @mapId={{this.mapId}} @space={{this.space}} />
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const titles = this.element.querySelectorAll('h2');

      expect(titles.length).to.equal(2);
      expect(titles[0].textContent.trim()).to.equal('Features');
      expect(titles[1].textContent.trim()).to.equal('Places');
    });

    it('sets the geocoding id in the state', async function () {
      let query;
      let closed;

      this.set('close', () => {
        closed = true;
      });

      this.router.transitionTo = (q) => {
        query = q;
      };

      this.set('mapId', map._id);

      await render(hbs`
        <Search @mapId={{this.mapId}} @onClose={{this.close}} @space={{this.space}} />
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');
      await click('.place-name');

      expect(closed).to.equal(true);
      expect(query).to.deep.equal({
        queryParams: { s: 'gec@609e48af7f7737bb2db1a6bc' },
      });
    });
  });

  describe ('using a non-default space',  function (hooks) {
    it('search for records belonging to the team', async function () {
      this.set('space', {
        visibility: {
          isPublic: true,
          isDefault: false,
          teamId: "5ca78e2160780601008f69e6"
        },
        searchOptions: {
          features: true,
          maps: true,
          campaigns: true,
          iconSets: true,
          teams: true,
          icons: true,
          geocodings: true,
          events: true,
        },
      });

      await render(hbs`
        <Search @space={{this.space}} />
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      expect(this.server.history).to.deep.equal([
        'GET /mock-server/searchmetas?limit=6&model=Feature&q=my%20search%20terms&skip=0&team=5ca78e2160780601008f69e6',
        'GET /mock-server/searchmetas?limit=6&model=Map&q=my%20search%20terms&skip=0&team=5ca78e2160780601008f69e6',
        'GET /mock-server/searchmetas?limit=6&model=Campaign&q=my%20search%20terms&skip=0&team=5ca78e2160780601008f69e6',
        'GET /mock-server/searchmetas?limit=6&model=IconSet&q=my%20search%20terms&skip=0&team=5ca78e2160780601008f69e6',
        'GET /mock-server/searchmetas?limit=6&model=Team&q=my%20search%20terms&skip=0&team=5ca78e2160780601008f69e6',
        'GET /mock-server/searchmetas?limit=6&model=Icon&q=my%20search%20terms&skip=0&team=5ca78e2160780601008f69e6',
        'GET /mock-server/searchmetas?limit=6&model=Event&q=my%20search%20terms&skip=0&team=5ca78e2160780601008f69e6'
      ]);
    });
  });

  describe ('when the space does not have a global map page',  function (hooks) {
    it('does not search for geo codings', async function () {
      space['globalMapPageId'] = '';

      await render(hbs`
        <Search />
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const titles = this.element.querySelectorAll('h2');

      expect(titles.length).to.equal(0);
    });
  });

  describe ('when the space has a global map page',  function (hooks) {
    it('searches for geo codings', async function () {
      this.set('space', {
        searchOptions: {
          features: true,
          maps: true,
          campaigns: true,
          iconSets: true,
          teams: true,
          icons: true,
          geocodings: true,
          events: true,
        },
        globalMapPageId: page
      });

      await render(hbs`
        <Search @space={{this.space}}/>
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');

      const titles = this.element.querySelectorAll('h2');

      expect(titles.length).to.equal(1);
      expect(titles[0].textContent.trim()).to.equal('Places');
    });

    it('sets the place link to the global map page when there is no mapId set', async function () {
      this.set('space', {
        searchOptions: {
          features: true,
          maps: true,
          campaigns: true,
          iconSets: true,
          teams: true,
          icons: true,
          geocodings: true,
          events: true,
        },
        globalMapPageId: page
      });

      let query;
      let path;
      let closed;

      this.router.transitionTo = (p, q) => {
        path = p;
        query = q;
      };

      this.set('close', () => {
        closed = true;
      });

      await render(hbs`
        <Search @onClose={{this.close}} @space={{this.space}} />
      `);

      await fillIn('.text-search', 'my search terms');
      await click('.btn-submit-search');
      await click('.place-name');

      expect(closed).to.equal(true);
      expect(path).to.equal('/page/test');
      expect(query).to.deep.equal({
        queryParams: { s: 'gec@609e48af7f7737bb2db1a6bc' },
      });
    });
  });
});
