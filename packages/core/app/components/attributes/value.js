/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';
import { guidFor } from '@ember/object/internals';
import { service } from '@ember/service';

export default class AttributesValueComponent extends Component {
  @service store;

  @tracked hasFocus = false;
  @tracked _disabled = null;
  @tracked _tmpValue = null;
  @tracked _isLoading = false;
  @tracked icons;

  get inputId() {
    return `value-${guidFor(this)}`;
  }

  click() {
    if (!this.disabled) {
      const elm = this.element.querySelector('input');

      if (!elm) {
        return;
      }

      elm.focus();
    }
  }

  get isLoading() {
    return this.args.isLoading || this._isLoading;
  }

  get disabled() {
    if (this._disabled === null) {
      return this.args.disabled;
    }

    return this._disabled;
  }

  set disabled(value) {
    this._disabled = value;

    if (this._disabled) {
      this._tmpValue = null;
    }
  }

  get tmpValue() {
    if (this._tmpValue === null) {
      return this.args.value;
    }

    return this._tmpValue;
  }

  set tmpValue(value) {
    this._tmpValue = value;
  }

  @action
  resetValue() {
    this._tmpValue = null;
  }

  @action
  focusIn() {
    later(() => {
      if (this.isDestroyed) return;
      this.hasFocus = true;
    });
  }

  @action
  focusOut() {
    later(() => {
      if (this.isDestroyed) return;
      this.hasFocus = false;
    });
  }

  @action
  longTextFocusOut() {
    later(() => {
      if (this.isDestroyed) return;
      this.hasFocus = false;
    });
  }

  @action
  async change(value, extraValue) {
    const name = extraValue ? value : this.args.key || this.args.name;

    switch (this.args.format) {
      case 'integer':
        this.tmpValue = parseInt(value);
        break;
      case 'decimal':
        this.tmpValue = parseFloat(value);
        break;
      default:
        this.tmpValue = extraValue ? extraValue : value;
    }

    this._isLoading = true;
    try {
      await this.args.onChange?.(name, this.tmpValue);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
    this._isLoading = false;
  }

  @action
  async loadIcons() {
    if (!Array.isArray(this.args.options)) {
      return [];
    }

    const iconRequests = this.args.options.map(
      (a) => this.store.peekRecord('icon', a) || this.store.findRecord('icon', a),
    );

    this.icons = await Promise.all(iconRequests);
  }

  @action
  changeInput(event) {
    return this.change(event.target.value);
  }
}
