/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { toDateTime } from 'models/transforms/calendar-entry-list';
import { DateTime } from 'luxon';
import { service } from '@ember/service';

export default class TimeIntervalComponent extends Component {
  @service intl;

  get begin() {
    const value = toDateTime(this.args.value?.begin);

    return value;
  }

  get end() {
    const value = toDateTime(this.args.value?.end);

    return value;
  }

  get beginStr() {
    return this.begin?.toLocaleString() ?? '';
  }

  get endStr() {
    return this.end?.toLocaleString() ?? '';
  }

  get beginStrFull() {
    return this.begin?.toLocaleString() + ' ' + this.begin.toLocaleString(DateTime.TIME_SIMPLE);
  }

  get endStrFull() {
    return this.end?.toLocaleString() + ' ' + this.end.toLocaleString(DateTime.TIME_SIMPLE);
  }

  get timezone() {
    const here = DateTime.now().zoneName;

    if (here == this.begin?.zoneName) {
      return '';
    }

    return this.begin?.zoneName;
  }

  get dateOn() {
    return this.intl.t('date-on', { date: this.date });
  }

  get date() {
    if (!this.begin) {
      return '';
    }

    return this.beginStr;
  }

  get time() {
    if (!this.begin) {
      return '';
    }

    return this.begin.toLocaleString(DateTime.TIME_SIMPLE) + ' - ' + this.end.toLocaleString(DateTime.TIME_SIMPLE);
  }

  get sameDay() {
    if (!this.begin) {
      return false;
    }

    return this.beginStr == this.endStr;
  }

  get otherDay() {
    if (!this.begin) {
      return false;
    }

    return this.beginStr != this.endStr;
  }

  get dateBetween() {
    return this.intl.t('date-between', {
      begin: this.beginStrFull,
      end: this.endStrFull,
    });
  }
}
