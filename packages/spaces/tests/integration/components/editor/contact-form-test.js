/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/contact-form", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let changedValue;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders the selected value", async function () {
    this.set("value", {
      floatingLabels: true,
      sendTo: "test@test.test",
      emailLabel: "Your email address:",
      nameLabel: "Your name:",
      messageLabel: "Your message:",
      labelSuccess: "Thank you for your message. We will reply soon.",
      submitButtonLabel: "sign up",
      subject: "subject",
    });

    await render(hbs`<Editor::ContactForm @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector(".floating-labels").value).to.equal("yes");
  });

  it("can change the floating labels", async function () {
    this.set("value", {
      floatingLabels: true,
      emailLabel: "Your email address:",
      nameLabel: "Your name:",
      messageLabel: "Your message:",
      labelSuccess: "Thank you for your message. We will reply soon.",
    });

    await render(hbs`<Editor::ContactForm @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".floating-labels").value = "no";
    await triggerEvent(".floating-labels", "change");

    expect(changedValue).to.deep.equal({
      floatingLabels: false,
      emailLabel: "Your email address:",
      nameLabel: "Your name:",
      messageLabel: "Your message:",
      labelSuccess: "Thank you for your message. We will reply soon.",
    });
  });

  it("can change the subject values", async function (a) {
    this.set("value", {
      floatingLabels: true,
      emailLabel: "Your email address:",
      nameLabel: "Your name:",
      messageLabel: "Your message:",
      labelSuccess: "Thank you for your message. We will reply soon.",
    });

    await render(hbs`<Editor::ContactForm @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".form-control.subject", "some subject");

    expect(changedValue).to.deep.equal({
      floatingLabels: true,
      emailLabel: "Your email address:",
      nameLabel: "Your name:",
      messageLabel: "Your message:",
      labelSuccess: "Thank you for your message. We will reply soon.",
      subject: "some subject",
    });
  });
});
