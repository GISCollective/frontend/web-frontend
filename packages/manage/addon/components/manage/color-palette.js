/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { standardColors, ColorPalette, CustomColor } from 'models/transforms/color-palette';
import { htmlSafe } from '@ember/template';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { A } from 'core/lib/array';

class ColorItem {
  @tracked name;
  @tracked value;

  constructor(name, value) {
    this.name = name;
    this.value = value;
  }

  get style() {
    return htmlSafe(`background-color: ${this.value}`);
  }
}

class CustomColorItem {
  @tracked name;
  @tracked lightValue;
  @tracked darkValue;

  constructor(name, lightValue, darkValue) {
    this.name = name;
    this.lightValue = lightValue;
    this.darkValue = darkValue;
  }

  get lightStyle() {
    return htmlSafe(`background-color: ${this.lightValue}`);
  }

  get darkStyle() {
    return htmlSafe(`background-color: ${this.darkValue}`);
  }
}
export default class ManageColorPaletteComponent extends Component {
  elementId = 'color-palette-' + guidFor(this);
  @tracked _value;
  @tracked colors;
  @tracked customColors = A();

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? new ColorPalette();
  }

  @action
  setup() {
    const result = [];

    for (const color of standardColors) {
      result.push(new ColorItem(color, this.value[color]));
    }

    this.colors = result;
    this.customColors = A(this.value.customColors.map(a => new CustomColorItem(a.name, a.lightValue, a.darkValue)));
  }

  @action
  change(name, value) {
    const color = this.colors.find((a) => a.name == name);
    color.value = value;

    if (!this._value) {
      this._value = new ColorPalette(this.value);
    }

    this._value[name] = value;
  }

  @action
  changeField(name, color, value) {
    color[name] = value;
  }

  @action
  colorInput(name, event) {
    this.change(name, event?.target?.value);
  }

  @action
  colorFieldInput(name, color, event) {
    this.changeField(name, color, event?.target?.value);
  }

  @action
  addCustomColor() {
    this.customColors.push(new CustomColorItem("new-color", "#000000", "#000000"))
  }

  @action
  removeCustom(index) {
    console.log("remove", index)
    this.customColors.splice(index, 1);
  }

  @action
  save() {
    let newValue = this.value;
    newValue.customColors = this.customColors.map(a => new CustomColor(a));

    return this.args.onSave?.(this.value);
  }

  @action
  cancel() {
    this._value = null;

    return this.args.onCancel?.();
  }
}
