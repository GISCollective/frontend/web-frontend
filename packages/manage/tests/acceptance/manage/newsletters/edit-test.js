/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitUntil, fillIn } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/newsletters/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let newsletter;
  let receivedNewsletter;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultSpace();

    newsletter = server.testData.storage.addDefaultNewsletter();

    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultPreferences();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();

    server.get(`/mock-server/articles/_/categories`, () => {
      return [200, { 'Content-Type': 'application/json' }, '{ "categories": ["category 1", "category 2"] }'];
    });

    server.put(`/mock-server/newsletters/${newsletter._id}`, (request) => {
      receivedNewsletter = JSON.parse(request.requestBody);
      receivedNewsletter.newsletter._id = newsletter._id;

      server.testData.storage.addNewsletter(receivedNewsletter.newsletter);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedNewsletter)];
    });

    receivedNewsletter = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('contains all the breadcrumbs links', async function () {
    authenticateSession();
    await visit(`/manage/newsletters/edit/${newsletter._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'my first newsletter', 'Edit']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
      '/manage/dashboards/newsletter/5ca78e2160780601008f69ff',
    ]);
  });

  it('redirects to login when the user is not authenticated', async function () {
    invalidateSession();

    await visit(`/manage/newsletters/edit/${newsletter._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fnewsletters%2Fedit%2F5ca78e2160780601008f69ff');
  });

  it('can rename the newsletter', async function () {
    authenticateSession();
    await visit(`/manage/newsletters/edit/${newsletter._id}`);

    await click('.row-name .btn-edit');
    await fillIn('.row-name input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedNewsletter);

    expect(receivedNewsletter).to.deep.equal({
      newsletter: {
        _id: '5ca78e2160780601008f69ff',
        description: 'Newsletter description',
        name: 'new name',
        info: {
          createdOn: '2015-01-01T00:00:00.000Z',
          lastChangeOn: '2015-01-01T00:00:00.000Z',
          author: '5b8a59caef739394031a3f67',
          originalAuthor: '5b8a59caef739394031a3f67',
        },
        visibility: {
          isPublic: true,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
        welcomeMessage: '5ca78e2160780601008f69e6',
      },
    });
  });

  it('can update the newsletter description', async function () {
    authenticateSession();
    await visit(`/manage/newsletters/edit/${newsletter._id}`);

    await click('.row-description .btn-edit');
    await fillIn('.row-description textarea', 'new description');
    await click('.btn-submit');

    await waitUntil(() => receivedNewsletter);

    expect(receivedNewsletter).to.deep.equal({
      newsletter: {
        _id: '5ca78e2160780601008f69ff',
        description: 'new description',
        name: 'my first newsletter',
        welcomeMessage: '5ca78e2160780601008f69e6',
        info: {
          createdOn: '2015-01-01T00:00:00.000Z',
          lastChangeOn: '2015-01-01T00:00:00.000Z',
          author: '5b8a59caef739394031a3f67',
          originalAuthor: '5b8a59caef739394031a3f67',
        },
        visibility: {
          isPublic: true,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        },
      },
    });
  });

  it('renders the newsletter welcome message', async function () {
    authenticateSession();
    await visit(`/manage/newsletters/edit/${newsletter._id}`);

    expect(this.element.querySelector('.row-welcome-message h1').textContent.trim()).to.equal('title');
    expect(this.element.querySelector('.row-welcome-message p').textContent.trim()).to.equal('some content');
  });

  it('can edit the the welcome message', async function () {
    authenticateSession();
    await visit(`/manage/newsletters/edit/${newsletter._id}`);

    await click('.row-welcome-message .btn-edit');

    expect(currentURL()).to.equal('/manage/articles/5ca78e2160780601008f69e6');
  });
});
