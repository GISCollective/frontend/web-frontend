/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from '../../base/app-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';
import * as Sentry from '@sentry/browser';
import { toPageId } from 'core/lib/slug';
import { fetch } from 'models/lib/fetch-source';

export default class BrowseSiteRoute extends AppRoute {
  @service headData;
  @service space;

  queryParams = {
    parentMap: {
      refreshModel: true,
      replace: true,
    },
  };

  async model(params) {
    const feature = await this.store.findRecord('feature', params.id);

    let map;
    let profile;
    let page;
    let modelData = {};

    const pageId = toPageId('feature--:feature-id', this.space);

    if (pageId) {
      try {
        page = await this.store.findRecord('page', pageId);

        for (let col of page.cols) {
          modelData[col.modelKey] = await fetch(modelData, col, this.space.currentSpace, this.store);
        }
      } catch (err) {
        console.error(err);
      }
    }

    if (feature.originalAuthor.indexOf('@') == -1) {
      try {
        profile = await this.store.findRecord('user-profile', feature.originalAuthor);
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
      }
    }

    const maps = await feature.maps;

    if (params.parentMap) {
      map = this.store.findRecord('map', params.parentMap);
    } else {
      map = maps[0];
    }

    const icons = await feature.icons;
    const pictures = await feature.pictures;
    const sounds = await feature.sounds;

    let contributors = [];

    try {
      contributors = await feature.contributors;
    } catch (err) {
      Sentry.captureException(err);
    }

    return hash({
      ...modelData,
      page,
      baseMaps: this.store.query('baseMap', { default: true }),
      feature,
      profile,
      map,
      icons,
      pictures,
      "selectedModel_pictures": pictures,
      "selectedModel_maps": maps,
      sounds,
      maps,
      contributors,
      selectedModel: feature,
      selectedType: 'feature',
      selectedId: params.id,
    });
  }
}
