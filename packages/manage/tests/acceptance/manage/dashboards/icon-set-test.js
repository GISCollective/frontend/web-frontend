/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitFor, waitUntil } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/icon set', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let space;
  let team;
  let iconSet;
  let icon;

  hooks.beforeEach(function () {
    server = new TestServer();
    team = server.testData.create.team();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    space = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    icon = server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultBaseMap();

    iconSet = server.testData.storage.addDefaultIconSet();
    iconSet.canEdit = true;

    const notifications = this.owner.lookup('service:notifications');
    notifications.ask = () => 'yes';

    const service = this.owner.lookup('service:modal');
    service.confirm = () => 'yes';
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('redirects to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2Ficon-set%2F5ca7b702ecd8490100cab96f');
  });

  it('shows the dashboard page when the user is authenticated', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('header').textContent.trim()).to.contain(iconSet.name);
  });

  it('displays the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Green Map® Icons Version 3']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    ]);
  });

  it('can edit the set', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

    await click('.btn-icon-set-edit');

    expect(currentURL()).to.equal('/manage/icons/edit/5ca7b702ecd8490100cab96f');
  });

  it('can access an icon without category and subcategory', async function () {
    icon.category = '';
    icon.subcategory = '';

    authenticateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

    await click('.card-dashboard-icon .title .btn');

    expect(currentURL()).to.equal('/manage/icons/edit/5ca7b702ecd8490100cab96f/5ca7bfc0ecd8490100cab980');
  });

  it('can access an icon with category and no subcategory', async function () {
    icon.category = 'category';
    icon.subcategory = '';

    authenticateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

    await click('.card-dashboard-icon .title .btn');

    expect(currentURL()).to.equal('/manage/icons/edit/5ca7b702ecd8490100cab96f/5ca7bfc0ecd8490100cab980');
  });

  it('can access an icon without category and with subcategory', async function () {
    icon.category = '';
    icon.subcategory = 'subcategory';

    authenticateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

    await click('.card-dashboard-icon .title .btn');

    expect(currentURL()).to.equal('/manage/icons/edit/5ca7b702ecd8490100cab96f/5ca7bfc0ecd8490100cab980');
  });

  it('can access an icon with a category and a subcategory', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

    await click('.card-dashboard-icon .title .btn');

    expect(currentURL()).to.equal('/manage/icons/edit/5ca7b702ecd8490100cab96f/5ca7bfc0ecd8490100cab980');
  });

  describe('when there are 2 icons with different categories and no subcategories', function (hooks) {
    let otherIcon;

    hooks.beforeEach(function () {
      otherIcon = server.testData.storage.addDefaultIcon('2');
      otherIcon.category = 'other';
      otherIcon.subcategory = '';
      otherIcon.canEdit = true;

      icon = server.testData.storage.addDefaultIcon();
      icon.category = 'category';
      icon.subcategory = '';
    });

    it('can switch the category', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      expect(this.element.querySelector('.btn-category.btn-secondary').textContent.trim()).to.equal('other');

      await click('.btn-category.btn-outline-secondary');

      expect(this.element.querySelector('.btn-category.btn-secondary').textContent.trim()).to.equal('category');
    });

    it('selects the first category when a missing one is selected', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/icon-set/${iconSet._id}?c=missing`);

      expect(this.element.querySelector('.btn-category.btn-secondary').textContent.trim()).to.equal('other');

      await click('.btn-category.btn-outline-secondary');

      expect(this.element.querySelector('.btn-category.btn-secondary').textContent.trim()).to.equal('category');
    });
  });

  describe('when there are 2 icons with different subcategories', function (hooks) {
    let otherIcon;

    hooks.beforeEach(function () {
      otherIcon = server.testData.storage.addDefaultIcon('2');
      otherIcon.category = 'category';
      otherIcon.subcategory = 'other';
      otherIcon.canEdit = true;

      icon = server.testData.storage.addDefaultIcon();
      icon.category = 'category';
      icon.subcategory = 'subcategory';
    });

    it('can switch the category', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      expect(this.element.querySelector('.btn-category.btn-secondary').textContent.trim()).to.equal('category');
      expect(this.element.querySelector('.btn-subcategory.btn-secondary').textContent.trim()).to.equal('other');

      await click('.btn-subcategory.btn-outline-secondary');

      expect(this.element.querySelector('.btn-subcategory.btn-secondary').textContent.trim()).to.equal('subcategory');
    });

    it('selects the first subcategory when a missing one is selected', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/icon-set/${iconSet._id}?c=category&sc=missing`);

      expect(this.element.querySelector('.btn-subcategory.btn-secondary').textContent.trim()).to.equal('other');
    });

    it('can add a new icon to a subcategory', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/icon-set/${iconSet._id}?c=category`);

      await click('.btn-add-icon');

      expect(currentURL()).to.equal('/manage/icons/add/5ca7b702ecd8490100cab96f?c=category&sc=other');
    });
  });

  describe('when there are 2 icons with same category but only one has subcategory', function (hooks) {
    let otherIcon;

    hooks.beforeEach(function () {
      otherIcon = server.testData.storage.addDefaultIcon('2');
      otherIcon.category = 'category';
      otherIcon.subcategory = '';
      otherIcon.canEdit = true;

      icon = server.testData.storage.addDefaultIcon();
      icon.category = 'category';
      icon.subcategory = 'subcategory';
    });

    it('shows both icons', async function (a) {
      authenticateSession();

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      expect(this.element.querySelector('.btn-subcategory.btn-secondary').textContent.trim()).to.equal('subcategory');
      expect(this.element.querySelectorAll('.card-dashboard-icon')).to.have.length(2);
    });
  });

  describe('when there are 2 icons and only one has category and subcategory', function (hooks) {
    let otherIcon;

    hooks.beforeEach(function () {
      otherIcon = server.testData.storage.addDefaultIcon('2');
      otherIcon.category = '';
      otherIcon.subcategory = '';
      otherIcon.canEdit = true;

      icon = server.testData.storage.addDefaultIcon();
      icon.category = 'category';
      icon.subcategory = 'subcategory';
    });

    it('shows both icons', async function (a) {
      authenticateSession();

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      expect(this.element.querySelector('.btn-subcategory.btn-secondary').textContent.trim()).to.equal('subcategory');
      expect(this.element.querySelectorAll('.card-dashboard-icon')).to.have.length(2);
    });
  });

  describe('deleting icons', function (hooks) {
    let otherIcon;

    hooks.beforeEach(function () {
      otherIcon = server.testData.storage.addDefaultIcon('2');
      otherIcon.category = 'category';
      otherIcon.subcategory = 'subcategory';
      otherIcon.canEdit = true;

      icon = server.testData.storage.addDefaultIcon();
      icon.category = 'category';
      icon.subcategory = 'subcategory';
    });

    it('can delete an icon using the context menu', async function (a) {
      const service = this.owner.lookup('service:notifications');
      service.ask = () => 'yes';

      authenticateSession();

      let deleted;

      server.server.delete(`/mock-server/icons/2`, () => {
        deleted = true;
        return [204, { 'Content-Type': 'application/json' }, JSON.stringify({})];
      });

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      await click('.btn-open-options');
      await click('.btn-delete');

      expect(deleted).to.equal(true);
    });

    it('can delete an icon by selecting it', async function (a) {
      authenticateSession();

      let deleted;

      server.server.delete(`/mock-server/icons/2`, () => {
        deleted = true;
        return [204, { 'Content-Type': 'application/json' }, JSON.stringify({})];
      });

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      const checkboxes = this.element.querySelectorAll('.card-dashboard-icon input');
      await click(checkboxes[0]);

      await click('.navbar .btn-delete');

      expect(deleted).to.equal(true);
    });

    it('can delete all icons from the selected subcategory', async function (a) {
      authenticateSession();

      let deleteAll = null;
      server.delete_('/mock-server/icons', (req) => {
        deleteAll = req.queryParams;
        return [204];
      });

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      await click('.chk-select-all');
      await click('.navbar .btn-delete');

      await waitUntil(() => deleteAll);

      expect(deleteAll).to.deep.equal({
        iconSet: '5ca7b702ecd8490100cab96f',
        category: 'category',
        subcategory: 'subcategory',
      });
    });

    it('can delete all icons from the selected category', async function (a) {
      otherIcon.subcategory = '';
      icon.subcategory = '';

      authenticateSession();

      let deleteAll = null;
      server.delete_('/mock-server/icons', (req) => {
        deleteAll = req.queryParams;
        return [204];
      });

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      await click('.chk-select-all');
      await click('.navbar .btn-delete');

      await waitUntil(() => deleteAll);

      expect(deleteAll).to.deep.equal({ iconSet: '5ca7b702ecd8490100cab96f', category: 'category' });
    });

    it('can delete all icons when there is no category or subcategory', async function (a) {
      otherIcon.category = '';
      otherIcon.subcategory = '';

      icon.category = '';
      icon.subcategory = '';

      authenticateSession();

      let deleteAll = null;
      server.delete_('/mock-server/icons', (req) => {
        deleteAll = req.queryParams;
        return [204];
      });

      await visit(`/manage/dashboards/icon-set/${iconSet._id}`);

      await click('.chk-select-all');
      await click('.navbar .btn-delete');

      await waitUntil(() => deleteAll);

      expect(deleteAll).to.deep.equal({ iconSet: '5ca7b702ecd8490100cab96f' });
    });
  });
});
