/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class InputContainerGroupComponent extends Component {
  @service fastboot;

  get isDisabled() {
    return this.fastboot.isFastBoot;
  }

  get isEditMode() {
    return this.args.title == this.args.editablePanel;
  }

  get isSaving() {
    return this.args.title == this.args.savingPanel;
  }

  get canEdit() {
    if (!this.args.onSave) {
      return false;
    }

    return this.args.onEdit && !this.isEditMode && this.args.canEdit !== false && !this.args.isDisabled;
  }

  get className() {
    if (!this.args.title) {
      return 'unknown';
    }

    return this.args.title.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }

  @action
  edit() {
    if (!this.args.onEdit) {
      return;
    }

    this.args.onEdit(this.args.title);
  }

  @action
  save() {
    if (!this.args.onSave) {
      return;
    }

    return this.args.onSave(this.args.title);
  }

  @action
  cancel() {
    if (!this.args.onCancel) {
      return;
    }

    return this.args.onCancel(this.args.title);
  }
}
