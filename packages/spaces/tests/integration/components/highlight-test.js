/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from "qunit";
import { setupRenderingTest, it, describe, wait } from "dummy/tests/helpers";
import { click, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

module("Integration | Component | highlight", function (hooks) {
  setupRenderingTest(hooks);

  it("renders nothing when there is no value", async function (assert) {
    await render(hbs`<Highlight />`);

    assert.dom(this.element).hasText("");
  });

  it("renders the content when there is one", async function (assert) {
    await render(hbs`
      <Highlight>
        template block text
      </Highlight>
    `);

    assert.dom(this.element).hasText("template block text");
  });

  describe("the frame", function () {
    it("wraps with a frame the hovered element when it has a name", async function (a) {
      await render(hbs`
      <Highlight @factor={{2}}>
        <div data-name="some name" class="some-element">
          template block text
        </div>
      </Highlight>
    `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      const frame = this.element.querySelector(".highlight-frame");
      expect(frame).to.exist;

      expect(frame.getBoundingClientRect().toJSON()).to.deep.equal(element.getBoundingClientRect().toJSON());
    });

    it("updates the frame size when the selection updates", async function (a) {
      this.set("height", 50);

      await render(hbs`
        <Highlight @factor={{2}}>
          <div data-name="some name" class="some-element" style="height: {{this.height}}px">
            template block text
          </div>
        </Highlight>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      this.set("height", 200);

      await wait(100);

      const frame = this.element.querySelector(".highlight-frame");
      expect(frame).to.exist;

      const frameRect = frame.getBoundingClientRect().toJSON();

      a.deepEqual(frameRect, element.getBoundingClientRect().toJSON());
      a.deepEqual(frameRect.height, 100);
    });

    it("does nothing with a frame the hovered element when it has a name and alt is pressed", async function (a) {
      await render(hbs`
        <Highlight @factor={{2}}>
          <div data-name="some name" class="some-element">
            template block text
          </div>
        </Highlight>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover", { altKey: true });

      const frame = this.element.querySelector(".highlight-frame");
      expect(frame).not.to.exist;
    });

    it("removes the frame on mouse out", async function (a) {
      await render(hbs`
      <Highlight @factor={{2}}>
        <div data-name="some name" class="some-element">
          template block text
        </div>
      </Highlight>
    `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");
      await triggerEvent(element, "mouseout");

      const frame = this.element.querySelector(".highlight-frame");
      expect(frame).not.to.exist;
    });

    it("does not remove the frame on mouse out when the alt key is pressed", async function (a) {
      await render(hbs`
        <Highlight @factor={{2}}>
          <div data-name="some name" class="some-element">
            template block text
          </div>
        </Highlight>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");
      await triggerEvent(element, "mouseout", { altKey: true });

      const frame = this.element.querySelector(".highlight-frame");
      expect(frame).to.exist;
    });

    it("does not wrap with a frame the hovered element when it has no name", async function (a) {
      await render(hbs`
      <Highlight @factor={{2}}>
        <div class="some-element">
          template block text
        </div>
      </Highlight>
    `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      const frame = this.element.querySelector(".highlight-frame");
      expect(frame).not.to.exist;
    });

    it("wraps with a frame the parent of a hovered element when it has a name", async function (a) {
      await render(hbs`
      <Highlight @factor={{2}}>
        <div class="parent-element" data-name="some name">
          parent
          <div class="some-element">
            template block text
          </div>
        </div>
      </Highlight>
    `);

      const element = this.element.querySelector(".some-element");
      const parentElement = this.element.querySelector(".parent-element");
      await triggerEvent(element, "mouseover");

      const frame = this.element.querySelector(".highlight-frame");
      expect(frame).to.exist;

      expect(frame.getBoundingClientRect().toJSON()).to.deep.equal(parentElement.getBoundingClientRect().toJSON());
    });

    it("sets the bottom most element as primary", async function (a) {
      await render(hbs`
        <Highlight @factor={{2}}>
          <div class="parent-element" data-name="some name">
            parent
            <div class="some-element" data-name="other name">
              template block text
            </div>
          </div>
        </Highlight>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      const frame = this.element.querySelector(".primary-frame");
      expect(frame).to.exist;

      expect(frame.getBoundingClientRect().toJSON()).to.deep.equal(element.getBoundingClientRect().toJSON());
    });
  });

  describe("the frame title", function () {
    it("should show the name and type in the frame title", async function (a) {
      await render(hbs`
        <Highlight @factor={{2}}>
          <div data-name="some name" data-type="type" class="some-element">
            template block text
          </div>
        </Highlight>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      expect(this.element.querySelector(".name").textContent.trim()).to.equal("some name");
      expect(this.element.querySelector(".type").textContent.trim()).to.equal("type");
      expect(this.element.querySelector(".global")).not.to.exist;
    });

    it("should show the global icon when there is a data-gid", async function (a) {
      await render(hbs`
        <Highlight @factor={{2}}>
          <div data-name="some name" data-type="type" data-gid="gid" class="some-element">
            template block text
          </div>
        </Highlight>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      expect(this.element.querySelector(".global")).to.exist;
    });

    it("shows an icon instead of the type name for a slot", async function (a) {
      await render(hbs`<div style="margin: 10%">
        <Highlight @factor={{2}}>
          <div data-name="some name" data-type="slot" data-gid="gid" class="some-element">
            template block text
          </div>
        </Highlight>
      </div>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      expect(this.element.querySelector(".highlight-title .type")).not.to.exist;
      expect(this.element.querySelector(".highlight-title .icon-type")).to.exist;
      expect(this.element.querySelector(".highlight-title .icon-type .fa-plug")).to.exist;
    });

    it("should not overlap two titles", async function (a) {
      await render(hbs`
      <div style="margin: 50px">
        <Highlight @factor={{2}}>
          <div data-name="some name" data-type="type">
            <div data-name="some name" data-type="type" class="some-element">
              template block text
            </div>
          </div>
        </Highlight>
      </div>
    `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      const names = this.element.querySelectorAll(".highlight-title");

      expect(names[0].getBoundingClientRect().x).to.be.greaterThan(
        names[1].getBoundingClientRect().x + names[1].getBoundingClientRect().width,
      );
    });

    it("properly sorts the x titles", async function (a) {
      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}}>
            <div data-name="title1" data-type="type">
              <div data-name="title2" data-type="type">
                <div data-name="title3" data-type="type" class="some-element" style="width: 100px; margin: auto;">
                  template block text
                </div>
              </div>
            </div>
          </Highlight>
        </div>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      let titles = [...this.element.querySelectorAll(".highlight-title .name")]
        .map((a) => ({ name: a.textContent.trim(), x: a.getBoundingClientRect().left }))
        .sort((a, b) => a.x - b.x)
        .map((a) => a.name);

      expect(titles).to.deep.equal(["title2", "title1", "title3"]);
    });

    it("should not add x padding to the title when it does not overlap", async function (a) {
      await render(hbs`
      <div style="margin: 50px">
        <Highlight @factor={{2}}>
          <div data-name="some name" data-type="type">
            <div style="padding: 50px;"></div>

            <div data-name="some name" data-type="type" class="some-element">
              template block text
            </div>
          </div>
        </Highlight>
      </div>
    `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      const names = this.element.querySelectorAll(".highlight-title");

      const diff = Math.abs(names[0].getBoundingClientRect().x - names[1].getBoundingClientRect().x);

      expect(diff).not.to.be.greaterThan(1);
    });

    it("highlights an element when it's title is hovered", async function (a) {
      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}}>
            <div data-name="some name" data-type="type">
              <div data-name="some name" data-type="type" class="some-element">
                template block text
              </div>
            </div>
          </Highlight>
        </div>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      let titles = this.element.querySelectorAll(".highlight-title");
      await triggerEvent(titles[0], "mouseover");

      titles = this.element.querySelectorAll(".highlight-title");
      expect(titles[0]).to.have.class("primary-highlight-title");
      expect(titles[1]).not.to.have.class("primary-highlight-title");
    });

    it("triggers an event when a title is clicked", async function (a) {
      let value;

      this.set("select", (v) => {
        value = v;
      });

      await render(hbs`
      <div style="margin: 50px">
        <Highlight @factor={{2}} @onSelect={{this.select}}>
          <div data-name="some name" data-type="type">
            <div data-name="some name" data-type="type" class="some-element">
              template block text
            </div>
          </div>
        </Highlight>
      </div>
    `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      let titles = this.element.querySelectorAll(".highlight-title");
      await click(titles[0]);

      expect(value.toJSON()).to.deep.equal({ name: "some name", type: "type" });
    });

    it("renders the selection when it is provided", async function (a) {
      this.set("selection", { name: "other name", type: "some type", gid: "" });

      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}} @selection={{this.selection}} @onSelect={{this.select}}>
            <div data-name="some name" data-type="type">
              <div data-name="other name" data-type="some type" class="some-element">
                template block text
              </div>
            </div>
          </Highlight>
        </div>
      `);

      let titles = this.element.querySelectorAll(".highlight-title");
      expect(titles).to.have.length(1);
      expect(titles[0]).to.have.class("selection-highlight-title");
    });

    it("renders the selection when it is provided", async function (a) {
      this.set("selection", { name: "other name", type: "some type", gid: "" });

      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}} @selection={{this.selection}} @onSelect={{this.select}}>
            <div data-name="some name" data-type="type">
              <div data-name="other name" data-type="some type" class="some-element">
                template block text
              </div>
            </div>
          </Highlight>
        </div>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      let titles = this.element.querySelectorAll(".highlight-title");
      expect(titles).to.have.length(1);
      expect(titles[0]).to.have.class("selection-highlight-title");
    });

    it("can set a selection as primary when hovered", async function (a) {
      this.set("selection", { name: "other name", type: "some type", gid: "" });

      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}} @selection={{this.selection}} @onSelect={{this.select}}>
            <div data-name="some name" data-type="type">
              <div data-name="other name" data-type="some type" class="some-element">
                template block text
              </div>
            </div>
          </Highlight>
        </div>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");
      await triggerEvent(".highlight-title", "mouseover");

      expect(this.element.querySelector(".highlight-frame.primary-frame")).not.to.exist;
    });

    it("shows the titles of hidden children divs", async function (a) {
      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}}>
            <div data-name="some name" class="some-element">
              template block text

              <div data-name="hidden name" class="hidden-element" style="display: none">
                template block text
              </div>
            </div>
          </Highlight>
        </div>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      let titles = [...this.element.querySelectorAll(".highlight-title .name")].map((a) => a.textContent.trim());

      let yList = [...this.element.querySelectorAll(".highlight-title .name")].map(
        (a) => a.getBoundingClientRect().bottom,
      );

      expect(titles).to.deep.equal(["some name", "hidden name"]);
      expect(yList[0]).to.deep.equal(yList[1]);
    });

    it("shows only unique names", async function (a) {
      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}}>
            <div data-name="some name" data-type="type1" class="some-element">
              template block text

              <div data-name="some name" data-type="type2" class="hidden-element" style="display: none">
                template block text
              </div>
            </div>
          </Highlight>
        </div>
      `);

      const element = this.element.querySelector(".some-element");
      await triggerEvent(element, "mouseover");

      let titles = [...this.element.querySelectorAll(".highlight-title .name")].map((a) => a.textContent.trim());
      let types = [...this.element.querySelectorAll(".highlight-title .type")].map((a) => a.textContent.trim());

      let yList = [...this.element.querySelectorAll(".highlight-title .type")].map(
        (a) => a.getBoundingClientRect().bottom,
      );

      expect(titles).to.deep.equal(["some name"]);
      expect(types).to.deep.equal(["type1", "type2"]);
      expect(yList[0]).to.deep.equal(yList[1]);
    });

    it("shows the titles of hidden children divs between highlight elements", async function (a) {
      await render(hbs`
        <div style="margin: 50px">
          <Highlight @factor={{2}}>
            <div data-name="some name" class="some-element">
              template block text

              <div data-name="hidden name" class="hidden-element" style="display: none">
                template block text
              </div>

              <div data-name="inner name" class="inner-element">
                template block text
              </div>
            </div>
          </Highlight>
        </div>
      `);

      const element = this.element.querySelector(".inner-element");
      await triggerEvent(element, "mouseover");

      let titles = [...this.element.querySelectorAll(".highlight-title .name")].map((a) => a.textContent.trim());

      let yList = [...this.element.querySelectorAll(".highlight-title .name")].map(
        (a) => a.getBoundingClientRect().bottom,
      );

      expect(titles).to.deep.equal(["some name", "hidden name", "inner name"]);
      expect(yList[0]).to.equal(yList[1]);
    });
  });
});
