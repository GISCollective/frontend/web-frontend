#!/bin/bash

echo "Deploy app: $2"
echo "with docker image: $1"
echo "========================================"

sudo docker pull $1
sudo docker stop $2 || true
sudo docker rm $2 || true

sudo docker run -p 9090:80 \
  --network gis-collective \
  -d --restart=always \
  --name=$2 \
  $1
