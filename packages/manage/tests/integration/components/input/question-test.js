/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { expect } from 'chai';

describe('Integration | Component | input/question component', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let iconSetData;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.beforeEach(function() {
    iconSetData = server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIcon('1');
    server.testData.storage.addDefaultIcon('2');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<Input::Question />`);

    expect(this.element.querySelector('.text-question').value).to.equal('');
    expect(this.element.querySelector('.text-help').value).to.equal('');
    expect(this.element.querySelector('.select-type').value).to.equal('');
    expect(this.element.querySelector('.chk-is-required').checked).to.equal(false);
    expect(this.element.querySelector('.text-options')).not.to.exist;
  });

  it('renders a text question when set', async function (assert) {
    this.set('value', {
      question: 'some question',
      help: 'help message',
      type: 'short text',
      isRequired: true,
    });

    await render(hbs`<Input::Question @value={{this.value}} />`);

    expect(this.element.querySelector('.text-question').value).to.equal('some question');
    expect(this.element.querySelector('.text-help').value).to.equal('help message');
    expect(this.element.querySelector('.select-type').value).to.equal('short text');
    expect(this.element.querySelector('.chk-is-required').checked).to.equal(true);
    expect(this.element.querySelector('.text-options')).not.to.exist;
  });

  it('renders validation message when name, question and type are unset', async function (assert) {
    this.set('value', {
      help: 'help message',
      isRequired: true,
    });

    await render(hbs`<Input::Question @value={{this.value}} />`);

    expect(this.element.querySelector('.alert-no-question')).to.have.textContent('The question is not set.');
    expect(this.element.querySelector('.alert-no-name')).to.have.textContent(
      'The name is not set. We recommend to use one or two words separated by a dash.',
    );
    expect(this.element.querySelector('.alert-no-type')).to.have.textContent('The type is not set.');
  });

  it('can change the values', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      question: 'some question',
      help: 'help message',
      type: 'short text',
      isRequired: true,
    });

    await render(hbs`<Input::Question @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('.text-question', 'new question');
    await fillIn('.text-help', 'new help message');
    await fillIn('.text-name', 'new message');

    this.element.querySelector('.select-type').value = 'long text';
    await triggerEvent('.select-type', 'change');

    await click('.chk-is-required');

    expect(value).to.deep.equal({
      question: 'new question',
      help: 'new help message',
      name: 'new message',
      type: 'long text',
      isRequired: false,
      options: undefined,
    });
    expect(this.element.querySelector('.text-options')).not.to.exist;
  });

  it('can set options for options fields', async function (a) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      question: 'some question',
      help: 'help message',
      name: 'name',
      type: 'short text',
      isRequired: true,
    });

    await render(hbs`<Input::Question @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('.select-type').value = 'options';
    await triggerEvent('.select-type', 'change');

    await fillIn('.text-question', 'new question');
    await fillIn('.text-options', 'a,b,c,d');

    a.deepEqual(value, {
      question: 'new question',
      help: 'help message',
      type: 'options',
      isRequired: true,
      options: 'a,b,c,d',
      name: 'name',
    });
  });

  it('can set options for options with other fields', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      question: 'some question',
      help: 'help message',
      type: 'short text',
      isRequired: true,
      name: 'name',
    });

    await render(hbs`<Input::Question @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('.select-type').value = 'options with other';
    await triggerEvent('.select-type', 'change');

    await fillIn('.text-question', 'new question');
    await fillIn('.text-options', 'a,b,c,d');

    expect(value).to.deep.equal({
      question: 'new question',
      help: 'help message',
      type: 'options with other',
      isRequired: true,
      options: 'a,b,c,d',
      name: 'name',
    });
  });

  it('can set geo json question', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      question: 'some question',
      help: 'help message',
      type: 'short text',
      isRequired: true,
      name: 'name',
    });

    await render(hbs`<Input::Question @value={{this.value}} @onChange={{this.change}} @extendedTypes={{true}}/>`);

    this.element.querySelector('.select-type').value = 'geo json';
    await triggerEvent('.select-type', 'change');

    await click('.select-allowAddress');

    expect(value).to.deep.equal({
      question: 'some question',
      help: 'help message',
      type: 'geo json',
      isRequired: true,
      name: 'name',
      options: { allowGps: true, allowManual: true, allowAddress: false, allowExistingFeature: false },
    });
  });

  it('can set an icons question', async function (a) {
    let value;
    let iconSet = await this.store.findRecord('icon-set', iconSetData._id);

    this.set('iconSets', [iconSet]);
    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      question: 'some question',
      help: 'help message',
      type: 'icons',
      isRequired: true,
      name: 'name',
    });

    await render(
      hbs`<Input::Question @iconSets={{this.iconSets}} @value={{this.value}} @onChange={{this.change}} @extendedTypes={{true}}/>`,
    );


    await click('.input-icons-selector .icon-container');
    await click('.input-icons-selector .icon-container');

    a.deepEqual(value, {
      question: 'some question',
      help: 'help message',
      type: 'icons',
      isRequired: true,
      name: 'name',
      options: ['1', '2'],
    });
  });

  it('shows the icon selection', async function () {
    let value;
    let iconSet = await this.store.findRecord('icon-set', iconSetData._id);

    this.set('iconSets', [iconSet]);
    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      question: 'some question',
      help: 'help message',
      type: 'icons',
      isRequired: true,
      name: 'name',
      options: ['1', '2'],
    });

    await render(
      hbs`<Input::Question @iconSets={{this.iconSets}} @value={{this.value}} @onChange={{this.change}} @extendedTypes={{true}}/>`,
    );

    const icons = this.element.querySelectorAll('.icons-order .icon-container');

    expect(icons).to.have.length(2);
  });

  it('can set a decimal question with measurement', async function () {
    let value;
    let iconSet = await this.store.findRecord('icon-set', iconSetData._id);

    this.set('iconSets', [iconSet]);
    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      question: 'some question',
      help: 'help message',
      type: 'decimal',
      isRequired: true,
      name: 'name',
    });

    await render(
      hbs`<Input::Question @iconSets={{this.iconSets}} @value={{this.value}} @onChange={{this.change}} @extendedTypes={{true}}/>`,
    );

    this.element.querySelector('.select-measurement').value = 'Temperature';
    await triggerEvent('.select-measurement', 'change');

    expect(value).to.deep.equal({
      question: 'some question',
      help: 'help message',
      type: 'decimal',
      isRequired: true,
      name: 'name',
      options: { measurement: 'Temperature' }
    });
  });
});
