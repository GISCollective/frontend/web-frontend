/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class ManageEditorsNewsletterComponent extends Component {
  @tracked _form;

  elementId = `newsletter-editor-${guidFor(this)}`;

  formConfig = [
    {
      name: "floatingLabels",
      type: "bool",
    },
    {
      name: "inputGroup",
      type: "bool",
    },
    {
      name: "labelSuccess",
      type: "text",
    },
  ];

  get form() {
    if (this._form) {
      return this._form;
    }

    return this.args.value ?? {};
  }

  triggerChange() {
    return this.args.onChange?.(this.form);
  }

  @action
  formChanged(value) {
    this._form = value;

    this.triggerChange();
  }
}
