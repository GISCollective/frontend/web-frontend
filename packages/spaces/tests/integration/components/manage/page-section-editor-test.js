/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

describe("Integration | Component | manage/page-section-editor", function (hooks) {
  setupRenderingTest(hooks);

  it("renders nothing when there is no value", async function (assert) {
    await render(hbs`<Manage::PageSectionEditor />`);
    ///  <Manage::PageSectionEditor @value={{@selection.data}} @editor={{@selection.editor}} @property={{@selection.property}} @model={{@model}} @deviceSize={{this.deviceSize}} @onChange={{this.changeCol}} />

    assert.dom(this.element).hasText("");
  });
});
