/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | browse/license',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the name and url when they are set', async function () {
    this.set('value', {
      name: 'some license',
      url: 'some url',
    });

    await render(hbs`<Browse::License @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain('some license');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'some url'
    );
  });

  it('renders the name and url when they are set as a list', async function () {
    this.set('value', [
      {
        name: 'some license',
        url: 'some url',
      },
    ]);

    await render(hbs`<Browse::License @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain('some license');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'some url'
    );
  });

  it('renders the name when url is not set', async function () {
    this.set('value', {
      name: 'some license',
    });

    await render(hbs`<Browse::License @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain('some license');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders the url and "unknown" when the name is not set', async function () {
    this.set('value', {
      url: 'some url',
    });

    await render(hbs`<Browse::License @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain('License');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'some url'
    );
  });
});
