/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | gradient-title', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let mapData;
  let map;
  let teamData;

  hooks.beforeEach(async function () {
    server = new TestServer();
    mapData = server.testData.storage.addDefaultMap();
    teamData = server.testData.storage.addDefaultTeam();

    mapData.license = {
      name: 'some license',
      url: 'some url',
    };

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicFeatures: 220,
        totalContributors: 33,
      }),
    ]);

    map = await this.store.findRecord('map', mapData._id);
  });

  it('renders nothing when there is no record', async function (assert) {
    await render(hbs`<GradientTitle />`);
    assert.dom(this.element).hasText('');
  });

  it('renders the title and the team for a map with public dependencies', async function (a) {
    this.set('map', map);
    this.set('model', { map });
    this.set('value', { modelKey: 'map', data: {} });

    await render(
      hbs`<GradientTitle @record={{this.map}} @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Harta Verde București',
    );
    expect(this.element.querySelector('.team').textContent.trim()).to.contain(
      'Open Green Map',
    );
    expect(this.element.querySelector('.license').textContent.trim()).to.equal(
      'some license',
    );
    expect(
      this.element.querySelector('.meta-container').textContent.trim(),
    ).to.contain('220 published sites');
  });

  it('does not render the team when the map team does not exist', async function (a) {
    map.visibility.teamId = 'missing';
    map.visibility.team = undefined;

    this.set('map', map);

    await render(hbs`<GradientTitle @record={{this.map}} />`);

    expect(this.element.querySelector('.team')).not.to.exist;
  });
});
