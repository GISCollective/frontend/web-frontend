/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { service } from "@ember/service";

export default class InputBsColorPickerComponent extends Component {
  @service clickOutside;
  @tracked isVisible;
  @tracked _value = null;

  get value() {
    if (this._value !== null) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  setup(element) {
    this.element = element;
  }

  @action
  hide() {
    this.isVisible = false;
  }

  @action
  change(value) {
    this._value = value;
    this.hide();
    return this.args.onChange?.(value);
  }

  @action
  handleClick() {
    if (this.isVisible) {
      return;
    }

    this.isVisible = true;

    this.clickOutside.subscribe(this.element, () => {
      this.hide();
    });
  }
}
