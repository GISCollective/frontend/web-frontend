/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import MapRoute from '../../base/map-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class Route extends MapRoute {
  @service fullscreen;
  @service embed;
  @service mapStyles;
  @service preferences;
  @service headData;
  @service fastboot;
  @service tracking;
  @service icons;

  baseMapCache = {};

  async beforeModel() {
    try {
      await super.beforeModel();
      await this.preferences.getPreference('appearance.mapExtent');
    } catch (err) {
      // eslint-disable-next-line no-console
      //console.warn('The `appearance.mapExtent` preference is not set.', err);
    }
  }

  async queryBaseMaps(params, map) {
    const key = JSON.stringify(params);

    if (!this.baseMapCache[key]) {
      if (!map?.baseMaps?.useCustomList) {
        this.baseMapCache[key] = await this.store.query('base-map', { default: true });
      } else {
        this.baseMapCache[key] = await Promise.all(map.baseMaps.promiseList);
      }
    }

    return this.baseMapCache[key];
  }

  async model(params) {
    params.map = params.id;
    let map = await this.map(params);

    let baseMaps = this.queryBaseMaps(params, map);
    let icons = this.icons.query(params, 5);

    return hash({
      id: params?.id,
      icons,
      selectedIconList: this.selectedIconList(params),
      map,
      baseMaps,
    });
  }

  async afterModel(model, transition) {
    if (model.map && this.fastboot.isFastBoot) {
      this.tracking.addMapView(this.router.currentURL, model.id);

      await model.map.visibility.fetchTeam();
      let pictureId = model.map.get('squareCover.id');

      if (!pictureId) {
        pictureId = model.map.get('cover.id');
      }

      if (pictureId) {
        await this.store.findRecord('picture', pictureId);
      }

      await model.map.fillMetaInfo(this.headData);
    }

    const from = transition.from?.name ?? '';
    const to = transition.to?.name ?? '';

    if (!this.fastboot.isFastBoot && from != to && from.indexOf('browse.maps.map-view.') == -1) {
      this.tracking.addMapView(this.router.currentURL, model.id);
    }
  }

  async selectedIconList(params) {
    const iconIds = params.icons?.split(',').filter((a) => a.length > 0) ?? [];

    const result = [];

    for (const id of iconIds) {
      try {
        const icon = await this.store.findRecord('icon', id);

        result.push(icon);
      } catch (err) { }
    }

    return result;
  }

  resetController(controller) {
    controller.viewMode = '';
    controller.extentChangeCounter = -1;
    controller._extentM = null;
    controller.set('viewbox', null);
  }

  activate() {
    let controller = this.controllerFor('browse.maps.map-view');
    this.mapStyles.onChange = () => {
      controller.changeView();
    };
  }

  deactivate() {
    this.fullscreen.isEnabled = false;
    this.mapStyles.onChange = null;
  }
}
