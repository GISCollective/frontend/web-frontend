/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class PageAdapter extends Adapter {
  queryRecord(store, schema, query, options) {
    if (typeof query == 'string') {
      const url = this.buildURL('layout', query);
      return this.ajax(url, 'GET');
    }

    return super.queryRecord(...arguments);
  }

  getRevisions(id) {
    const url = this.buildURL('layout', id) + '/revisions';
    return this.ajax(url, 'GET');
  }

  async loadRevision(id, revisionId) {
    let url = this.buildURL('layout', id);

    if (revisionId) {
      url += `/revisions/${revisionId}`;
    }

    const data = await this.ajax(url, 'GET');

    this.store.pushPayload(data);

    return this.store.peekRecord('layout', id);
  }
}
