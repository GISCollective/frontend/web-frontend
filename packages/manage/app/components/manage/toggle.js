/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { debounce } from '@ember/runloop';
import { guidFor } from '@ember/object/internals';

export default class ManageToggleComponent extends Component {
  elementId = `manage-toggle-${guidFor(this)}`;

  @service intl;

  checkbox = null;

  get id() {
    return this.args.id ?? this.elementId;
  }

  get isSaving() {
    return this.args.title && this.args.title == this.args.savingField;
  }

  get className() {
    if (!this.args.title || !this.args.title) {
      return 'unknown';
    }

    return this.args.title.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }

  @action
  setupCheckbox(element) {
    if (this.args.value) {
      element.setAttribute('checked', 'true');
    }

    this.checkbox = element;
  }

  saveState() {
    this.args.onSave(this.checkbox.checked);
  }

  @action
  switchChanged() {
    debounce(this, this.saveState, 200);
  }
}
