/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, settled, triggerEvent, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";
import { waitUntil, click, typeIn } from '@ember/test-helpers';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';
import PageElements from 'core/test-support/page-elements';

describe ('Integration | Component | manage/page-col',  function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultArticle('2');
    server.testData.storage.addDefaultPicture('1');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.put(`/mock-server/pictures/:id`, (request) => {
      let picture = JSON.parse(request.requestBody).picture;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: {
            ...picture,
            _id: request.params.id,
          },
        }),
      ];
    });

    const store = this.owner.lookup('service:store');

    const article_1 = await store.findRecord('article', '1');
    const article_2 = await store.findRecord('article', '2');
    const picture_1 = await store.findRecord('picture', '1');

    this.set('model', {
      article_1,
      article_2,
      picture_1,
    });
  });

  it('can change the editor type', async function () {
    this.set('value', {
      type: 'article',
      data: {
        id: '1',
      },
    });

    let value;
    this.set('change', (v) => {
      value = v.toJSON();
    });

    await render(hbs`<Manage::PageCol @value={{this.value}} @onChange={{this.change}} />`);

    await PageElements.waitEditorJs(this.element);
    await waitFor('.page-col-type');
    const select = this.element.querySelector('.page-col-type');
    select.value = 'picture';
    await triggerEvent(select, 'change');

    expect(this.element.querySelector('.page-col-type').value).to.equal('picture');
    expect(this.element.querySelector('.page-col-id')).not.to.exist;

    expect(value).to.deep.contain({
      container: +0,
      col: +0,
      row: +0,
      name: '0.0.0',
      type: 'picture',
      data: {},
    });
  });
});
