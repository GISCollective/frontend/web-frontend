/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | map-attributions', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<MapAttributions />`);

    assert.dom(this.element).hasText('');
  });

  it('renders the map attributions when set', async function (assert) {
    this.set('map', {
      license: {
        name: 'some license',
        url: 'some url',
      },
    });

    await render(hbs`<MapAttributions @map={{this.map}}/>`);

    expect(this.element.querySelectorAll('a')).to.have.length(1);
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'some url',
    );
    expect(this.element.querySelector('a').textContent.trim()).to.equal(
      'some license',
    );
  });

  it('renders the base map attributions when set', async function (assert) {
    this.set('map', {
      license: {
        name: 'some license',
        url: 'some url',
      },
    });

    this.set('baseMap', {
      attributions: [
        {
          name: 'license 1',
          url: 'url 1',
        },
        {
          name: 'license 2',
          url: 'url 2',
        },
      ],
    });

    await render(
      hbs`<MapAttributions @map={{this.map}} @baseMap={{this.baseMap}} />`,
    );

    expect(this.element.querySelectorAll('a')).to.have.length(3);
    expect(this.element.querySelectorAll('a')[1]).to.have.attribute(
      'href',
      'url 1',
    );
    expect(this.element.querySelectorAll('a')[1].textContent.trim()).to.equal(
      'license 1',
    );
    expect(this.element.querySelectorAll('a')[2]).to.have.attribute(
      'href',
      'url 2',
    );
    expect(this.element.querySelectorAll('a')[2].textContent.trim()).to.equal(
      'license 2',
    );
  });
});
