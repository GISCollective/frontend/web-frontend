/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Model | user profile', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('user-profile', {});
    expect(model).to.be.ok;
  });

  describe('the fullName', function () {
    it('returns --- for the name when no name field is set', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user-profile', {});
      expect(model.fullName).to.equal('---');
    });

    it('returns the first name when is set', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user-profile', {
        firstName: 'first',
      });
      expect(model.fullName).to.equal('first');
    });

    it('returns the last name when is set', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user-profile', {
        lastName: 'last',
      });
      expect(model.fullName).to.equal('last');
    });

    it('returns --- when only th title is set', function () {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user-profile', {
        title: 'title',
      });
      expect(model.fullName).to.equal('---');
    });
  });
});
