/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, wait } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, triggerEvent, waitUntil, click, fillIn, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';
import { move } from 'core/lib/trigger';

describe('Integration | Component | input/photo-gallery', function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders the add button when there is no list', async function () {
    await render(hbs`<Input::PhotoGallery />`);

    expect(this.element.querySelector('.btn-image-add')).to.exist;
  });

  it('triggers on change action when a file is selected', async function () {
    this.set('value', []);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}}/>`);

    const blob = server.testData.create.pngBlob();

    await waitFor('.input-photo-gallery-input');

    await triggerEvent('.input-photo-gallery-input', 'change', {
      files: [blob],
    });

    await waitUntil(() => value);

    expect(value).to.have.length(1);
    expect(value[0].picture).to.startWith(
      'data:image/png;base64,wolQTkcNChoKAAAADUlIRFIAAABkAAAAZAgGAAAAcMOiwpVUAAAACXBIWXMAAAsSAAALE',
    );
  });

  it('triggers on change action when two files are selected', async function () {
    this.set('value', []);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}}/>`);

    const blob = server.testData.create.pngBlob();

    await triggerEvent("[type='file']", 'change', {
      files: [blob, blob],
    });

    await waitUntil(() => value);

    expect(value).to.have.length(2);
    expect(value[0].picture).to.startWith(
      'data:image/png;base64,wolQTkcNChoKAAAADUlIRFIAAABkAAAAZAgGAAAAcMOiwpVUAAAACXBIWXMAAAsSAAALE',
    );
    expect(value[1].picture).to.startWith(
      'data:image/png;base64,wolQTkcNChoKAAAADUlIRFIAAABkAAAAZAgGAAAAcMOiwpVUAAAACXBIWXMAAAsSAAALE',
    );
  });

  it('can change the order of two pictures', async function () {
    this.set('value', [{ picture: 'data:image/png;base64,picture1' }, { picture: 'data:image/png;base64,picture2' }]);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}}/>`);

    await waitFor('.dragSortList img');
    const list = document.querySelector('.dragSortList');
    await move(list, 0, list, 1, false, 'img');

    expect(value).to.have.length(2);
    expect(value[0].picture).to.startWith('data:image/png;base64,picture2');
    expect(value[1].picture).to.startWith('data:image/png;base64,picture1');
  });

  it('can delete a picture', async function () {
    this.set('value', [{ picture: 'data:image/png;base64,picture1' }, { picture: 'data:image/png;base64,picture2' }]);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}}/>`);

    const deleteBtns = this.element.querySelectorAll('.btn-delete');
    await click(deleteBtns[1]);

    expect(value).to.have.length(1);
    expect(value[0].picture).to.startWith('data:image/png;base64,picture1');
  });

  it('can rotate an image', async function () {
    this.set('value', [
      {
        picture: 'data:image/png;base64,picture1',
        rotate() {
          this.isRotated = true;
        },
      },
    ]);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}}/>`);

    await click('.btn-details');

    await waitFor('.btn-rotate');
    await click('.btn-rotate');

    expect(value[0].isRotated).to.equal(true);

    await click('.btn-close');
  });

  it('can set the image as 360', async function () {
    this.set('value', [
      {
        isSaved: false,
        picture: 'data:image/png;base64,picture1',
        save() {
          this.isSaved = true;
        },
      },
    ]);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}} />`);

    await click('.btn-details');
    await waitFor('.chk-enable-360');

    await click('.chk-enable-360');

    await waitUntil(() => value);

    expect(value[0].is360).to.equal(true);
    expect(value[0].isSaved).to.equal(false);

    await click('.btn-close');
  });

  it('can change the picture name', async function () {
    this.set('value', [
      {
        picture: 'data:image/png;base64,picture1',
        name: 'some name',
      },
    ]);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}} />`);

    await click('.btn-details');
    await waitFor('.picture-name');

    await fillIn('.picture-name', 'new name');

    await waitUntil(() => value);

    expect(value[0].name).to.equal('new name');
    await click('.btn-close');
  });

  it('can change the picture attributions', async function () {
    this.set('value', [
      {
        picture: 'data:image/png;base64,picture1',
        name: 'some name',
        meta: {
          attributions: 'some attributions',
        },
      },
    ]);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::PhotoGallery @onChange={{this.change}} @value={{this.value}} />`);

    await click('.btn-details');
    await waitFor('.picture-attributions');
    await fillIn('.picture-attributions', 'new attributions');

    await waitUntil(() => value);

    expect(value[0].meta.attributions).to.equal('new attributions');

    await click('.btn-close');
  });
});
