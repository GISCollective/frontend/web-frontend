/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import PageElements from 'core/test-support/page-elements';

describe('Integration | Component | new-record-form/newsletter', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let server;
  let storage;

  hooks.beforeEach(async function () {
    server = new TestServer();
    storage = server.testData.storage;
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('for a normal user', function (hooks) {
    it('renders an alert when there is no team', async function () {
      await render(hbs`<NewRecordForm::Newsletter />`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.alert-danger .btn-add-team')).to.exist;
      expect(this.element.querySelector('.row-team')).not.to.exist;
    });

    it('does not render an alert when there is a team and it is added by default', async function () {
      const teamData = storage.addDefaultTeam();
      const team = await this.store.findRecord('team', teamData._id);

      this.set('teams', [team]);
      this.set('newsletter', this.store.createRecord('newsletter', {}));

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<NewRecordForm::Newsletter @teams={{this.teams}} @newsletter={{this.newsletter}} @all={{false}} @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await PageElements.waitEditorJs(this.element);

      await fillIn('.input-name', 'new newsletter name');

      await click('.btn-submit');

      expect(value.name).to.equal('new newsletter name');
      expect(value.visibility).to.deep.equal({
        team,
        isPublic: false,
        isDefault: false,
      });
    });

    it('renders the team section when there are 2 teams that allow newsletter', async function () {
      const teamData1 = storage.addDefaultTeam();
      const teamData2 = storage.addDefaultTeam('2');
      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      team1.allowNewsletters = true;
      team2.allowNewsletters = true;

      this.set('teams', [team1, team2]);
      this.set('newsletter', this.store.createRecord('newsletter', {}));

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<NewRecordForm::Newsletter @teams={{this.teams}} @newsletter={{this.newsletter}} @all={{false}}
  @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await PageElements.waitEditorJs(this.element);
      await fillIn('.input-name', 'space name');

      this.element.querySelector('.row-team select').value = '2';
      await triggerEvent('.row-team select', 'change');

      await click('.btn-submit');

      expect(value.name).to.equal('space name');
      expect(value.visibility.team.id).to.equal('2');
    });

    it(`does not render the teams when there are 2 teams that don't allow newsletters`, async function () {
      const teamData1 = storage.addDefaultTeam();
      const teamData2 = storage.addDefaultTeam('2');
      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      team1.allowNewsletters = false;
      team2.allowNewsletters = false;

      this.set('teams', [team1, team2]);
      this.set('newsletter', this.store.createRecord('newsletter', {}));

      this.set('save', () => {});

      await render(
        hbs`<NewRecordForm::Newsletter @teams={{this.teams}} @newsletter={{this.newsletter}} @all={{false}}
  @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await PageElements.waitEditorJs(this.element);
      await fillIn('.input-name', 'space name');

      expect(this.element.querySelector('.row-team select')).not.to.exist;
    });
  });

  describe('for admin user', function (hooks) {
    hooks.beforeEach(function () {
      let service = this.owner.lookup('service:user');
      service.userData = { isAdmin: true };
    });

    it('renders the btn-show-all-teams', async function () {
      const teamData1 = storage.addDefaultTeam();
      const teamData2 = storage.addDefaultTeam('2');
      const team1 = await this.store.findRecord('team', teamData1._id);
      const team2 = await this.store.findRecord('team', teamData2._id);

      team1.allowNewsletters = true;
      team2.allowNewsletters = true;

      this.set('teams', [team1, team2]);
      this.set('newsletter', this.store.createRecord('newsletter', {}));

      await render(
        hbs`<NewRecordForm::Newsletter @teams={{this.teams}} @newsletter={{this.newsletter}} @all={{false}}
  @onToggleAll={{this.toggleAll}} @onSubmit={{this.save}} />`,
      );

      await PageElements.waitEditorJs(this.element);
      expect(this.element.querySelector('.btn-show-all-teams')).to.exist;
    });
  });
});
