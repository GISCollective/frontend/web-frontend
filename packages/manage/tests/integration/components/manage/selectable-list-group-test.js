/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/selectable-list-group', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<Manage::SelectableListGroup />`);

    assert.dom(this.element).hasText('');
  });

  it('renders an item when it is set', async function () {
    this.set('value', { category: [{ id: 1 }] });

    await render(
      hbs`<Manage::SelectableListGroup @value={{this.value}} as |item| ><div class="item">{{item.id}}</div></Manage::SelectableListGroup>`,
    );

    expect(this.element.querySelectorAll('h4')).to.have.length(1);
    expect(this.element.querySelectorAll('.item')).to.have.length(1);

    expect(this.element.querySelector('h4').textContent.trim()).to.equal('category');
    expect(this.element.querySelector('.item').textContent.trim()).to.equal('1');
  });

  it('renders 2 items when they are set', async function () {
    this.set('value', { category: [{ id: 1 }, { id: 2 }] });

    await render(
      hbs`<Manage::SelectableListGroup @value={{this.value}} as |item| ><span>{{item.id}}</span></Manage::SelectableListGroup>`,
    );

    expect([...this.element.querySelectorAll('span')].map((a) => a.textContent).join('')).to.equal('12');
  });

  it('passes the on select function', async function () {
    this.set('value', { category: [{ id: 1 }, { id: 2 }] });

    await render(hbs`<Manage::SelectableListGroup @value={{this.value}} as |item selection onSelect| >
      <button type="button" {{on "click" (fn onSelect "all")}}>btn {{item.id}}</button>
    </Manage::SelectableListGroup>`);

    expect(this.element.querySelector('.count-message')).not.to.exist;

    await click('button');

    expect(this.element.querySelector('.count-message').textContent.trim()).to.equal('all items are selected');
  });

  it('renders the select all chackbox when there are 2 items', async function (a) {
    this.set('value', { category: [{ id: 1 }, { id: 2 }] });

    await render(hbs`<Manage::SelectableListGroup @value={{this.value}} as |item selection onSelect| >
      <button type="button" {{on "click" (fn onSelect "all")}}>btn {{item.id}}</button>
    </Manage::SelectableListGroup>`);

    expect(this.element.querySelector('.count-message')).not.to.exist;

    await click('.chk-select-all');

    expect(this.element.querySelector('.count-message').textContent.trim()).to.equal('all items are selected');
  });
});
