/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";
import { sizedBsProperty, setSizedBsProperty } from "core/lib/bs-cols";
import { tracked } from "@glimmer/tracking";

export default class InputOptionsWidthComponent extends Component {
  elementId = `input-width-${guidFor(this)}`;
  availableColWidths = ["default", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
  availableWidthTypes = ["cols", "px", "%"];

  @tracked _pxWidth;
  @tracked _widthType;

  get widthType() {
    if (this._widthType) {
      return this._widthType;
    }

    const width = this.style.width;
    let unit;

    if (typeof width == "string") {
      unit = this.availableWidthTypes.find((a) => width.indexOf(a) > 0);
    }

    return unit ?? "cols";
  }

  get style() {
    return this.args.value?.style?.[this.size] ?? {};
  }

  get classes() {
    return this.args.value?.classes ?? [];
  }

  get width() {
    if (typeof this._pxWidth == "string") {
      return this._pxWidth;
    }

    return this.args.value?.style?.[this.size]?.width;
  }

  get value() {
    const result = this.args.value ?? {};
    result.classes = this._classes;

    if (!result.style) {
      result.style = {};
    }

    if (!result.style[this.size]) {
      result.style[this.size] = {};
    }

    if (this.width || result.style[this.size].width) {
      result.style[this.size].width = this.width ?? "";
    }

    return result;
  }

  get size() {
    return this.args.deviceSize ?? "sm";
  }

  get colWidth() {
    return sizedBsProperty("col", this.size, this.classes);
  }

  @action
  widthChange(value) {
    this._pxWidth = "";
    this._classes = setSizedBsProperty("col", this.size, value, this.classes);

    return this.args.onChangeOptions?.(this.value);
  }

  @action
  change(value) {
    this._pxWidth = value;
    this._classes = setSizedBsProperty("col", this.size, "default", this.classes);

    return this.args.onChangeOptions?.(this.value);
  }

  @action
  typeChange(value) {
    this._widthType = value;
    setSizedBsProperty("col", this.size, "default");

    this.change("");
  }
}
