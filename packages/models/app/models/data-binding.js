/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { hasMany, attr } from '@ember-data/model';

export default class DataBindingModel extends Model {
  @attr('string') name;
  @attr('data-binding-destination') destination;

  @attr descriptionTemplate;

  @attr('field-mapping') fields;

  @hasMany('icon', { async: true, inverse: null })
  extraIcons;

  @attr('string') updateBy;
  @attr('string') crs;

  @attr('connection') connection;

  @attr('visibility') visibility;
  @attr('model-info') info;
  @attr('boolean') canEdit;

  get title() {
    return this.name;
  }

  async handleAnalyze() {
    return await this.store.adapterFor(this.constructor.modelName).handleAnalyze(this.id);
  }

  async handleTrigger() {
    return await this.store.adapterFor(this.constructor.modelName).handleTrigger(this.id);
  }
}
