/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Select from 'ol/interaction/Select';

export default class FeatureSelect extends Select {
  restorePreviousStyle_() {}

  applySelectedStyle_() {}

  handleEvent(mapBrowserEvent) {
    if (!this.condition_(mapBrowserEvent)) {
      return true;
    }

    const map = mapBrowserEvent.map;
    const selected = [];

    map.forEachFeatureAtPixel(
      mapBrowserEvent.pixel,

      function (feature, layer) {
        if (!this.filter_(feature, layer)) {
          return;
        }
        this.addFeatureLayerAssociation_(feature, layer);
        selected.push(feature);
        return !this.multi_;
      }.bind(this),
      {
        layerFilter: this.layerFilter_,
        hitTolerance: this.hitTolerance_,
      },
    );

    this.notify?.(selected.length > 0 ? selected[0] : null);

    return true;
  }
}
