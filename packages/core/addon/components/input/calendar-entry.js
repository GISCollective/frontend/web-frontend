/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { CalendarEntry } from 'models/transforms/calendar-entry-list';
import { DateTime } from 'luxon';

export default class InputCalendarEntryComponent extends Component {
  @service intl;
  @tracked _value;

  get repetitionTypes() {
    const values = ['norepeat', 'daily', 'weekly', 'monthly', 'yearly'];

    return values.map((id) => ({
      id,
      name: this.intl.t(`repetition.${id}`),
    }));
  }

  get value() {
    if (!this._value) {
      return this.defaultValue;
    }

    return this._value;
  }

  get defaultValue() {
    const begin = DateTime.now().plus({ day: 1 }).set({ seconds: 0, milliseconds: 0 });
    const end = begin.plus({ hour: 1 });

    let timezone = begin.zoneName;

    return new CalendarEntry({ begin, end, repetition: 'norepeat', timezone });
  }

  get hasIntervalEnd() {
    return this.value.repetition && this.value.repetition != 'norepeat';
  }

  get hasInvalidRepetition() {
    if (!this.hasIntervalEnd) {
      return false;
    }

    return this.value.intervalEnd.toMillis() < this.value.end.toMillis();
  }

  get hasInvalidEndDate() {
    return this.value.end.toMillis() < this.value.begin.toMillis();
  }

  @action
  setup() {
    if (!this.args.value) {
      this._value = this.defaultValue;
      this.args.onChange?.(this.value);
      return;
    }

    this._value = new CalendarEntry(this.args.value);
  }

  @action
  changeTimeZone(timezone) {
    const value = this.value;

    value.begin = value.begin.setZone(timezone, { keepLocalTime: true });
    value.end = value.end.setZone(timezone, { keepLocalTime: true });
    value.intervalEnd = value.intervalEnd.setZone(timezone, { keepLocalTime: true });
    value.timezone = timezone;

    this._value = value;

    this.args.onChange?.(this.value);
  }

  @action
  changeRepetition(value) {
    if (!this.value.intervalEnd || this.value.intervalEnd.year < this.value.end.year) {
      this.value.intervalEnd = this.value.end;
    }

    return this.change('repetition', value.id);
  }

  @action
  change(field, value) {
    this._value[field] = value;

    if (field == 'end') {
      this._value.intervalEnd = this._value.intervalEnd.set({
        hour: value.hour,
        minute: value.minute,
        second: value.second,
      });
    }

    this.args.onChange?.(this.value);
  }
}
