/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class ManageEditorsBaseContainerComponent extends Component {
  flexConfig = [
    {
      type: "bs",
      property: "justify-content",
    },
    {
      type: "bs",
      property: "align-items",
    },
    {
      type: "bs",
      property: "flex-direction",
    },
    {
      type: "bs",
      property: "flex-resize",
    },
  ];

  sizeConfig = [
    {
      type: "padding",
    },
    {
      type: "margin",
    },
    {
      type: "px",
      name: "height",
    },
    {
      type: "width",
    },
  ];

  containerConfig = [
    {
      name: "color",
      type: "color",
    },
    {
      type: "border",
    },
  ];

  get showSize() {
    return this.args.element?.attributes?.getNamedItem?.("data-size")?.value != "false";
  }

  get showFlexConfig() {
    return this.args.element?.attributes?.getNamedItem?.("data-flex-config")?.value != "false";
  }
}
