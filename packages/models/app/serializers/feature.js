/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import RESTSerializer from '@ember-data/serializer/rest';

export default class FeatureSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    issueCount: { serialize: false },
    canEdit: { serialize: false },
    isMasked: { serialize: false },
    contributors: { serialize: false },
  };
}
