/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, click, triggerEvent, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/map-layer-list', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders an alert message when a value is not set', async function () {
      await render(hbs`<Manage::MapLayerList @title="test" @isMainValue={{true}} />`);
      expect(this.element.querySelector('.alert-danger').textContent.trim()).to.contain('There are no layers set.');
    });

    it('renders a list of layers', async function () {
      this.set('value', [
        { type: 'Open Street Map', options: {} },
        { type: 'WMS', options: {} },
      ]);

      await render(hbs`<Manage::MapLayerList @values={{this.value}} @title="test" @isMainValue={{true}} />`);
      expect(this.element.querySelector('.alert-danger')).not.to.exist;

      const values = this.element.querySelectorAll('li');

      expect(values[0].textContent.trim()).to.equal('WMS (topmost)');
      expect(values[1].textContent.trim()).to.equal('Open Street Map');
    });
  });

  describe('in edit mode', function (hooks) {
    it('should show and add layer button', async function () {
      let value;

      this.set('onSave', (newValue) => {
        value = newValue;
      });

      await render(
        hbs`<Manage::MapLayerList @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`,
      );

      expect(this.element.querySelector('.input-map-layer')).not.to.exist;

      await click('.btn-add-layer');

      expect(this.element.querySelector('.input-map-layer')).to.exist;

      this.element.querySelector('.select-layer-type').value = 'Open Street Map';
      await triggerEvent('.select-layer-type', 'change');

      await click('.btn-submit');

      expect(value).to.deep.equal([
        {
          type: 'Open Street Map',
          options: {},
        },
      ]);
    });

    it('should delete a layer from the list', async function () {
      let value;

      this.set('onSave', (newValue) => {
        value = newValue;
      });

      this.set('values', [
        {
          type: 'Open Street Maps',
          options: {},
        },
      ]);

      await render(
        hbs`<Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`,
      );
      await click('.btn-delete');

      expect(this.element.querySelector('.input-map-layer')).not.to.exist;

      await click('.btn-submit');

      expect(value).to.deep.equal([]);
    });

    it('should move the layer up in the list', async function () {
      let value;

      this.set('onSave', (newValue) => {
        value = newValue;
      });

      this.set('values', [
        { type: 'Open Street Maps', options: {} },
        { type: 'WMS', options: {} },
      ]);

      await render(
        hbs`<Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`,
      );

      await click(this.element.querySelectorAll('.btn-move-up')[1]);

      await click('.btn-submit');

      expect(value.map((a) => a.toJSON())).to.deep.equal([
        { type: 'WMS', options: { 'server type': 'carmentaserver', tiled: false } },
        { type: 'Open Street Maps', options: {} },
      ]);
    });

    it('should move the layer down in the list', async function () {
      let value;

      this.set('onSave', (newValue) => {
        value = newValue;
      });

      this.set('values', [
        { type: 'Open Street Maps', options: {} },
        { type: 'WMS', options: {} },
      ]);

      await render(
        hbs`<Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`,
      );

      await click(this.element.querySelectorAll('.btn-move-down')[0]);

      await click('.btn-submit');

      expect(value.map((a) => a.toJSON())).to.deep.equal([
        { type: 'WMS', options: { 'server type': 'carmentaserver', tiled: false } },
        { type: 'Open Street Maps', options: {} },
      ]);
    });

    it('should update the layer options', async function () {
      let value;

      this.set('onSave', (newValue) => {
        value = newValue;
      });

      this.set('values', [{ type: 'WMS', options: {} }]);

      await render(
        hbs`<Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`,
      );

      await fillIn('.url', 'https://giscolective.com');

      await click('.btn-submit');

      expect(value.map((a) => a.toJSON())).to.deep.equal([
        { type: 'WMS', options: { 'server type': 'carmentaserver', tiled: false, url: 'https://giscolective.com' } },
      ]);
    });
  });
});
