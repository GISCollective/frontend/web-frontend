/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { debounce } from '@ember/runloop';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageModelInfoComponent extends Component {
  @service store;

  performUserSearch(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject('Term is too short');
    }

    this.store
      .query('user', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  get lastChangeOnSeconds() {
    if (!this.args.model || !this.args.model.info || !this.args.model.info.lastChangeOn) {
      return false;
    }

    return parseInt((new Date() - this.args.model.info.lastChangeOn) / 1000);
  }

  get lastChangeOn() {
    if (this.lastChangeOnSeconds < 60) {
      return parseInt(this.lastChangeOnSeconds) * -1;
    }

    if (this.lastChangeOnSeconds < 3600) {
      return parseInt(this.lastChangeOnSeconds / 60) * -1;
    }

    if (this.lastChangeOnSeconds < 3600 * 24) {
      return parseInt(this.lastChangeOnSeconds / 3600) * -1;
    }

    if (this.lastChangeOnSeconds < 3600 * 24 * 365) {
      return parseInt(this.lastChangeOnSeconds / (3600 * 24)) * -1;
    }

    return parseInt(this.lastChangeOnSeconds / (3600 * 24 * 365)) * -1;
  }

  get lastChangeOnUnit() {
    if (this.lastChangeOnSeconds < 60) {
      return 'second';
    }

    if (this.lastChangeOnSeconds < 3600) {
      return 'minute';
    }

    if (this.lastChangeOnSeconds < 3600 * 24) {
      return 'hour';
    }

    if (this.lastChangeOnSeconds < 3600 * 24 * 365) {
      return 'day';
    }

    return 'year';
  }

  @action
  searchUsers(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performUserSearch, term, resolve, reject, 600);
    });
  }

  @action
  originalAuthorChanged(value) {
    if (!this.args.model.info) {
      this.args.model.set('info', Object.create({}));
    }

    this.args.model.set('info.originalAuthor', value.id);
    return this.args.onSave();
  }
}
