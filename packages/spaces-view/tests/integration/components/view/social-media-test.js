import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import { SocialMediaLinks } from 'models/transforms/social-media-links';

describe('Integration | Component | view/social-media', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing where there is no value', async function (assert) {
    await render(hbs`<View::SocialMedia />`);

    expect(this.element).to.have.textContent('');
  });

  it('renders the facebook icon when the team has one set', async function (a) {
    this.set('value', {
      data: {
        source: {
          useSelectedModel: false,
          useDefaultModel: true,
          model: 'team',
        },
      },
      modelKey: 'team',
    });

    this.set('model', {
      team: {
        id: '1',
        socialMediaLinks: {
          niceLinks: {
            facebook: 'http://facebook.com',
          },
        },
      },
    });

    await render(
      hbs`<View::SocialMedia @value={{this.value}} @model={{this.model}} />`,
    );

    const links = [...this.element.querySelectorAll('a')];

    expect(links).to.have.length(1);
    expect(links[0]).to.have.attribute('href', 'http://facebook.com');
    expect(links[0].querySelector('.fa-facebook')).to.exist;
  });

  it('renders the x twitter icon when the team has one set', async function (a) {
    this.set('value', {
      data: {
        source: {
          useSelectedModel: false,
          useDefaultModel: true,
          model: 'team',
        },
      },
      modelKey: 'team',
    });

    this.set('model', {
      team: {
        id: '1',
        socialMediaLinks: new SocialMediaLinks({
          xTwitter: 'http://x.com',
        }),
      },
    });

    await render(
      hbs`<View::SocialMedia @value={{this.value}} @model={{this.model}} />`,
    );

    const links = [...this.element.querySelectorAll('a')];

    expect(links).to.have.length(1);
    expect(links[0]).to.have.attribute('href', 'http://x.com');
    expect(links[0].querySelector('.fa-x-twitter')).to.exist;
  });
});
