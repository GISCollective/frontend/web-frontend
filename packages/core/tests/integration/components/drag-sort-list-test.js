import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { find, findAll, render, settled, triggerEvent } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import trigger from 'core/lib/trigger';
import { A } from 'models/lib/array';

module('Integration | Component | DragSortList', function (hooks) {
  setupRenderingTest(hooks);

  test('it works', async function (assert) {
    let value;
    const items = A([{ name: 'foo' }, { name: 'bar' }, { name: 'baz' }]);

    this.set('items', items);
    this.set('dragEndCallback', (v) => {
      value = v;
    });
    this.set('additionalArgs', { parent: 'test' });

    await render(hbs`
      <DragSortList
        @additionalArgs={{this.additionalArgs}}
        @items={{this.items}}
        @dragEndAction={{this.dragEndCallback}}
        as |item|
      >
        <div>
          {{item.name}}
        </div>
      </DragSortList>
    `);

    const itemElements = this.element.querySelectorAll('.dragSortItem');
    const [item0, item1] = itemElements;

    trigger(item0, 'dragstart');
    trigger(item1, 'dragover', false);
    trigger(item0, 'dragend');

    await settled();

    assert.deepEqual(value, {
      group: undefined,
      draggedItem: items[0],
      sourceArgs: { parent: 'test' },
      sourceList: items,
      targetArgs: { parent: 'test' },
      targetList: items,
      sourceIndex: 0,
      targetIndex: 1,
    });
  });

  test('sorting with neither dragover nor dragenter', async function (a) {
    let called = false;
    const items = A([{ name: 'foo' }, { name: 'bar' }, { name: 'baz' }]);
    this.set('items', items);
    this.set('dragEndCallback', () => {
      called = true;
    });

    await render(hbs`
      <DragSortList
        @items={{this.items}}
        @dragEndAction={{this.dragEndCallback}}
        as |item|
      >
        <div>
          {{item.name}}
        </div>
      </DragSortList>
    `);

    const item0 = find('.dragSortItem');

    trigger(item0, 'dragstart');
    trigger(item0, 'dragend');

    await settled();

    a.false(called);
  });

  test('drag handle', async function (assert) {
    const items = A([{ name: 'foo' }, { name: 'bar' }, { name: 'baz' }]);
    this.set('items', items);

    let value;
    this.set('dragEndCallback', (v) => {
      value = v;
    });

    await render(hbs`
      <DragSortList
        @items={{this.items}}
        @dragEndAction={{this.dragEndCallback}}
        @handle=".handle"
        as |item|
      >
        <div class="handle">handle</div>
        <div>
          {{item.name}}
        </div>
      </DragSortList>
    `);

    const itemElements = findAll('.dragSortItem');
    const [item0, item1] = itemElements;

    trigger(item0, 'dragstart');
    trigger(item1, 'dragover', false);
    trigger(item0, 'dragend');

    await settled();

    assert.notOk(value);

    trigger(item0.querySelector('.handle'), 'dragstart');
    trigger(item1, 'dragover', false);
    trigger(item0, 'dragend');

    await settled();

    assert.deepEqual(value, {
      group: undefined,
      draggedItem: items[0],
      sourceArgs: undefined,
      sourceList: items,
      targetArgs: undefined,
      targetList: items,
      sourceIndex: 0,
      targetIndex: 1,
    });
  });

  test('nested drag handle', async function (assert) {
    const items = A([{ name: 'foo' }, { name: 'bar' }, { name: 'baz' }]);
    this.set('items', items);

    let value;
    this.set('dragEndCallback', (v) => {
      value = v;
    });

    await render(hbs`
      <DragSortList
        @items={{this.items}}
        @dragEndAction={{this.dragEndCallback}}
        @handle= ".handle"
        as |item|
      >
        <div class="handle">
          <div class="handle2">handle</div>
        </div>
        <div>
          {{item.name}}
        </div>
      </DragSortList>
    `);

    const itemElements = findAll('.dragSortItem');
    const [item0, item1] = itemElements;

    trigger(item0, 'dragstart');
    trigger(item1, 'dragover', false);
    trigger(item0, 'dragend');

    await settled();

    assert.notOk(value);

    trigger(item0.querySelector('.handle2'), 'dragstart');
    trigger(item1, 'dragover', false);
    trigger(item0, 'dragend');

    await settled();

    assert.deepEqual(value, {
      group: undefined,
      draggedItem: items[0],
      sourceArgs: undefined,
      sourceList: items,
      targetArgs: undefined,
      targetList: items,
      sourceIndex: 0,
      targetIndex: 1,
    });
  });

  test('drag start action', async function (assert) {
    const items = A([{ name: 'foo' }, { name: 'bar' }, { name: 'baz' }]);
    this.set('items', items);

    let value;
    this.set('dragStartCallback', (v) => {
      value = v;
    });

    await render(hbs`
      <DragSortList
        @items={{this.items}}
        @dragStartAction={{this.dragStartCallback}}
        as |item|
      >
        <div class="item-wrapper">
          {{item.name}}
        </div>
      </DragSortList>
    `);

    const itemElements = findAll('.dragSortItem');
    const [item0] = itemElements;

    const dataTransfer = new DataTransfer();
    await triggerEvent(item0, 'dragstart', { dataTransfer });

    assert.ok(value);
    assert.deepEqual(value, {
      draggedItem: {
        name: 'foo',
      },
      group: undefined,
      sourceArgs: undefined,
      sourceIndex: 0,
      sourceList: items,
      targetArgs: undefined,
      targetIndex: 0,
      targetList: items,
    });
  });
});
