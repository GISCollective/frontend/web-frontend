/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { buildWaiter } from "@ember/test-waiters";
const waiter = buildWaiter("background-image");

export default class ManageEditorsBackgroundImageComponent extends Component {
  @service store;
  @tracked _picture = null;
  @tracked _options = null;
  @tracked _style = null;

  get picture() {
    if (this._picture) {
      return this._picture;
    }

    return this.value?.record;
  }

  get style() {
    if (this._style !== null) {
      return this._style;
    }

    return this.args.value?.data?.style ?? {};
  }

  get pictureId() {
    if (!this.picture) {
      return this.args.value?.data?.source?.id;
    }

    if (this.picture.get) {
      return this.picture.get("id");
    }

    return this.picture.id;
  }

  get options() {
    if (this._options) {
      return this._options;
    }

    return this.args.value?.data?.options ?? [];
  }

  get hasFixedSize() {
    return this.options.includes("bg-size-fixed");
  }

  get hasFixedX() {
    return this.options.includes("bg-position-x-fixed");
  }

  get hasFixedY() {
    return this.options.includes("bg-position-y-fixed");
  }

  fixStyle() {
    const newStyle = this.style;

    for (const key of Object.keys(newStyle)) {
      if (!this.hasFixedX) {
        delete newStyle[key]["background-position-x"];
      }

      if (!this.hasFixedY) {
        delete newStyle[key]["background-position-y"];
      }

      if (!this.hasFixedSize) {
        delete newStyle[key]["background-size"];
      }
    }

    this._style = newStyle;
  }

  triggerChange() {
    this.args.onChange({
      options: this.options,
      style: this.style,
      source: {
        id: this.pictureId,
        model: "picture",
      },
    });
  }

  @action
  optionsChanged(newOptions) {
    this._options = newOptions;

    this.fixStyle();

    this.triggerChange();
  }

  @action
  changeDimension(options, value, unit, size, property, index) {
    const newStyle = this.style;

    if (!newStyle[size]) {
      newStyle[size] = {};
    }

    let newSize = newStyle[size]["background-size"] ?? [{}, {}];

    newSize[index] = { value, unit };

    newStyle[size]["background-size"] = newSize;

    this._style = newStyle;

    this.triggerChange();
  }

  @action
  changeProperty(options, value, unit, size, property) {
    const newStyle = this.style;

    if (!newStyle[size]) {
      newStyle[size] = {};
    }

    newStyle[size][property] = { value, unit };

    this._style = newStyle;

    this.triggerChange();
  }

  @action
  pictureChanged(_, value) {
    this._picture = value;

    this.triggerChange();
  }

  @action
  async setupPicture() {
    if (!this.args.value?.data?.source?.id) {
      return;
    }

    let token = waiter.beginAsync();

    try {
      this._picture = await this.store.findRecord("picture", this.args.value.data.source.id);
    } catch (err) {
      console.log(err);
    }

    waiter.endAsync(token);
  }
}
