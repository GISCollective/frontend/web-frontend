/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/menu-item', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the menu item values', async function () {
    this.set('value', {
      name: 'name',
      link: { url: 'https://giscollective.com' },
    });

    await render(hbs`<Input::MenuItem @value={{this.value}} />`);

    expect(this.element.querySelector('.text-name').value).to.equal('name');
    expect(this.element.querySelector('input.text-link').value).to.equal('https://giscollective.com');
  });

  it('triggers onChange when the name is changed', async function () {
    this.set('value', {
      name: 'name',
      link: { url: 'https://giscollective.com' },
    });

    let value;
    this.set('change', function (v) {
      value = v;
    });

    await render(hbs`<Input::MenuItem @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('.text-name', 'new name');

    expect(value).to.deep.equal({
      name: 'new name',
      link: { url: 'https://giscollective.com' },
    });
  });

  it('triggers onChange when the link is changed', async function () {
    this.set('value', {
      name: 'name',
      link: { url: 'https://giscollective.com' },
    });

    let value;
    this.set('change', function (v) {
      value = v;
    });

    await render(hbs`<Input::MenuItem @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('input.text-link', 'https://test.com');

    expect(value).to.deep.equal({
      name: 'name',
      link: {
        url: 'https://test.com',
      },
    });
  });
});
