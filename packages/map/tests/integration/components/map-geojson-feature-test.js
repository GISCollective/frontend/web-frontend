import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { transform } from 'ol/proj';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | map-geojson-feature', function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultBaseMap('1');
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders a point in the map center when set', async function (assert) {
    this.set('value', {
      type: 'Point',
      coordinates: [23.369218111038208, 45.43939480420836],
    });
    const center = transform(
      [23.369218111038208, 45.43939480420836],
      'EPSG:4326',
      'EPSG:3857',
    );
    await render(hbs`<MapGeojsonFeature @value={{this.value}} />`);

    const map = this.element.querySelector('.map').olMap;
    const view = map.getView();

    expect(view.getZoom()).to.equal(18);
    expect(view.getCenter()).to.deep.equal(center);

    expect(this.element.querySelector('.map-overlay')).to.have.attribute(
      'data-lon',
      `${center[0]}`,
    );
    expect(this.element.querySelector('.map-overlay')).to.have.attribute(
      'data-lat',
      `${center[1]}`,
    );
    expect(this.element.querySelector('.map-overlay .point-icon')).to.exist;
  });
});
