#!/bin/bash -v

fpm -s dir -t rpm \
  -p ogm-frontend-$1-fedora-36.rpm \
  --verbose \
  --log info \
  -n ogm-frontend \
  -v $1 \
  -m "hello@giscollective.com" \
  --description "GISCollective frontend" \
  ./dist/=/var/www/ogm/

