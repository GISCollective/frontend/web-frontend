/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, fillIn, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/domain', function (hooks) {
  let server;

  setupRenderingTest(hooks);

  hooks.beforeEach(async function () {
    server = new TestServer();
    server.testData.storage.addDefaultSpace();

    server.get(`/mock-server/spaces/:id/categories`, () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    const service = this.owner.lookup('service:space');
    await service.setup();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('when there is one domain set up in the spaces.domains preference', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addPreference('spaces.domains', 'giscollective.com');
    });

    describe('in view mode', function (hooks) {
      it('renders the space subdomain when changing the subdomain is not allowed', async function () {
        this.set('value', {
          domain: 'test.giscollective.com',
          domainValidationKey: 'some-key',
          slug: 'test',

          allowDomainChange: false,
          hasDomainValidated: false,
        });

        await render(hbs`<Manage::Domain @value={{this.value}} @title="domain name"/>`);

        expect(this.element.querySelector('a.domain-url')).to.have.attribute('href', 'https://test.giscollective.com');

        expect(this.element.querySelector('.btn-validate')).not.to.exist;
        expect(this.element.querySelector('.domain-dns')).not.to.exist;
        expect(this.element.querySelector('.domain-validation-key')).not.to.exist;

        expect(this.element.querySelector('.domain-validation-success')).not.to.exist;

        expect(this.element.querySelector('.alert-domain-status')).not.to.exist;
      });

      it('renders verified space domain when changing the subdomain is not allowed', async function () {
        this.set('value', {
          domain: 'example.com',
          domainValidationKey: 'some-key',
          slug: 'test',

          allowDomainChange: true,
          hasDomainValidated: true,
        });

        await render(hbs`<Manage::Domain @value={{this.value}} @title="domain name"/>`);

        expect(this.element.querySelector('a.domain-url')).to.have.attribute('href', 'https://example.com');

        expect(this.element.querySelector('.domain-validation-success').textContent.trim()).to.equal('verified');

        expect(this.element.querySelector('.domain-dns').value).to.equal('example.com ALIAS test.giscollective.com.');

        expect(this.element.querySelector('.domain-validation-key').value).to.equal(
          '_gis-collective-verification-code.example.com TXT gis-collective-verification-code=some-key',
        );
      });

      it('renders unverified space domain when changing the subdomain is not allowed', async function () {
        this.set('value', {
          domain: 'example.com',
          domainValidationKey: 'some-key',
          slug: 'test',

          allowDomainChange: true,
          hasDomainValidated: false,
        });

        await render(hbs`<Manage::Domain @value={{this.value}} @title="domain name"/>`);

        expect(this.element.querySelector('a.domain-url')).to.have.attribute('href', 'https://example.com');

        expect(this.element.querySelector('.domain-validation-danger').textContent.trim()).to.equal('unverified');

        expect(this.element.querySelector('.domain-dns').value).to.equal('example.com ALIAS test.giscollective.com.');

        expect(this.element.querySelector('.domain-validation-key').value).to.equal(
          '_gis-collective-verification-code.example.com TXT gis-collective-verification-code=some-key',
        );
      });

      it('triggers the onVerify event when the verify button is pressed', async function () {
        this.set('value', {
          domain: 'example.com',
          domainValidationKey: 'some-key',
          slug: 'test',

          allowDomainChange: true,
          hasDomainValidated: false,
        });

        let called;
        this.set('verify', function () {
          called = true;
        });

        await render(hbs`<Manage::Domain @value={{this.value}} @onVerify={{this.verify}} @title="domain name"/>`);

        await click('.btn-verify');

        expect(called).to.equal(true);
      });

      it('shows a message with the status', async function () {
        this.set('value', {
          domain: 'example.com',
          domainValidationKey: 'some-key',
          slug: 'test',
          domainStatus: 'ready',
          allowDomainChange: true,
          hasDomainValidated: false,
        });

        await render(hbs`<Manage::Domain @value={{this.value}} @title="domain name"/>`);

        expect(this.element.querySelector('.alert-domain-status')).to.have.textContent(
          'Your space is ready at the reserved domain.',
        );
      });
    });

    describe('in edit mode', function (hooks) {
      it('allows to only change the subdomain', async function () {
        this.set('value', {
          domain: 'test.giscollective.com',
          domainValidationKey: 'some-key',
          slug: 'test',

          allowDomainChange: false,
          hasDomainValidated: false,
        });

        let savedValue;

        this.set('save', (val) => {
          savedValue = val;
        });

        await render(hbs`<Manage::Domain @onSave={{this.save}} @value={{this.value}} @editablePanel="domain name"/>`);

        expect(this.element.querySelector('.select-domain')).not.to.exist;

        await fillIn('.input-space-subdomain', 'newvalue');
        await triggerEvent('.input-space-subdomain', 'change');

        await click('.btn-submit');

        expect(savedValue).to.equal('newvalue.giscollective.com');
      });

      it('allows to change the domain name', async function () {
        this.set('value', {
          domain: 'example.com',
          domainValidationKey: 'some-key',
          slug: 'test',

          allowDomainChange: true,
          hasDomainValidated: true,
        });

        let savedValue;

        this.set('save', (val) => {
          savedValue = val;
        });

        await render(hbs`<Manage::Domain @onSave={{this.save}} @value={{this.value}} @editablePanel="domain name"/>`);

        expect(this.element.querySelector('.message-domain-change-forbidden')).not.to.exist;

        expect(this.element.querySelector('.input-domain').value).to.equal('example.com');

        await fillIn('.input-domain', 'new-domain.com');

        await click('.btn-submit');

        expect(savedValue).to.equal('new-domain.com');
      });
    });
  });

  describe('when there are two domains set in the spaces.domains preference', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addPreference('spaces.domains', 'giscollective.com,other.com');
    });

    it('allows to pick a domain from the list', async function () {
      this.set('value', {
        domain: 'test.giscollective.com',
        domainValidationKey: 'some-key',
        slug: 'test',

        allowDomainChange: false,
        hasDomainValidated: false,
      });
      let savedValue;

      this.set('save', (val) => {
        savedValue = val;
      });

      await render(hbs`<Manage::Domain @value={{this.value}} @editablePanel="domain name" @onSave={{this.save}} />`);

      const e = this.element.querySelector('.select-domain');
      expect(e.options[e.selectedIndex].text).to.equal('giscollective.com');

      e.value = 'other.com';
      await triggerEvent(e, 'change');

      await click('.btn-submit');
      expect(savedValue).to.equal('test.other.com');
    });
  });
});
