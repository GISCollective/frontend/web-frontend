import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | manage/event-list', function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultEvent('1');
    server.testData.storage.addDefaultEvent('2');
  });

  it('renders the value in edit mode', async function () {
    const event = await this.store.findRecord('event', '1');
    this.set('value', [event]);

    await render(hbs`<Manage::EventList @title="test" @value={{this.value}} />`);

    const items = [...this.element.querySelectorAll('li')].map((a) => a.textContent.trim());

    expect(items).to.deep.equal(['test event']);
  });

  it('can change a value in edit mode', async function () {
    const event = await this.store.findRecord('event', '1');
    this.set('value', [event]);

    let value;
    this.set('save', (v) => {
      value = v;
    });

    this.set('list', await this.store.findAll('event'));

    await render(
      hbs`<Manage::EventList @title="test" @editablePanel="test" @value={{this.value}} @list={{this.list}} @onSave={{this.save}} />`,
    );

    await click('.btn-add');

    const selects = [...this.element.querySelectorAll('select')];
    selects[1].value = '2';
    await triggerEvent(selects[1], 'change');

    await click('.btn-submit');

    expect(value.map((a) => a?.id)).to.deep.equal(['1', '2']);
  });
});
