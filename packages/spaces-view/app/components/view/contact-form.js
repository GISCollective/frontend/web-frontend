/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class PageColContactFormComponent extends Component {
  elementId = `contact-${guidFor(this)}`;

  @service intl;

  @tracked showInput = true;

  @tracked team = {};
  @tracked name = '';
  @tracked email = '';
  @tracked subject = '';
  @tracked message = '';

  @tracked _form;
  @tracked _label;

  get label() {
    return this._label ?? this.args.value?.data?.label ?? {};
  }

  get floatingLabels() {
    return this.form.floatingLabels;
  }

  get nameLabel() {
    return this.form.nameLabel || 'Your name:';
  }

  get emailLabel() {
    return this.form.emailLabel || 'Your email:';
  }

  get subjectLabel() {
    return this.form.subjectLabel || 'Subject:';
  }

  get messageLabel() {
    return this.form.messageLabel || 'Message:';
  }

  get submitLabel() {
    return (
      this.form.submitButtonLabel ||
      this.args.value?.data?.button?.label ||
      'Send'
    );
  }

  get disabled() {
    const email =
      this.args.value?.data?.form?.sendTo?.trim?.() || this.team?.contactEmail;

    if (!email) {
      return true;
    }

    return (
      !this.name.trim() ||
      !this.email.trim() ||
      !this.subject.trim() ||
      !this.message.trim()
    );
  }

  get formLabelSuccess() {
    return this.form.labelSuccess || this.intl.t('contact-form-success');
  }

  get form() {
    return this._form ?? this.args?.value?.data?.form ?? {};
  }

  @action
  updateFormProps(event) {
    this._form = event.detail.value;
  }

  @action
  updateLabelProps(event) {
    this._label = event.detail.value;
  }

  @action
  async setup() {
    let subject = this.form.subject ?? '';
    subject = subject.replace(
      '{title}',
      this.args.model?.selectedModel?.title ?? '',
    );

    this.subject = subject;

    this.team = (await this.args.space?.visibility?.fetchTeam?.()) ?? {};
  }

  @action
  async submit() {
    const page = this.args?.model?.page;

    if (!page) {
      return;
    }

    await page.contact({
      colName: this.args?.value?.name,
      name: this.name,
      email: this.email,
      subject: this.subject,
      message: this.message,
    });

    this.showInput = false;
  }
}
