/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { guidFor } from "@ember/object/internals";

export default class InputManageToGlobalPageColComponent extends Component {
  elementId = `input-manage-to-global-page-col-${guidFor(this)}`;
  @tracked isFormVisible;
  @tracked value;
  @tracked error;

  get exists() {
    return this.args.space?.hasPageCol(this.value);
  }

  @action
  showForm() {
    this.isFormVisible = true;
    this.value = "";
  }

  @action
  async submit() {
    try {
      await this.args.onSubmit?.(this.value);
      this.isFormVisible = false;
    } catch (err) {
      this.error = err.message;
    }
  }

  @action
  focus(element) {
    element.focus();
  }
}
