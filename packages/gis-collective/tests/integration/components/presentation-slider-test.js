/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | presentation-slider',  function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let presentation;
  let article;
  let picture;
  let store;

  hooks.before(function () {
    server = new TestServer();

    const article = server.testData.storage.addDefaultArticle('1');
    article.title = 'Article title';
    article.contentBlocks = {
      blocks: [
        {
          type: 'header',
          data: {
            level: 1,
            text: "Green Map's title",
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'some content',
          },
        },
      ],
    };

    server.testData.storage.addDefaultPicture('2');
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    presentation = server.testData.storage.addDefaultPresentation();
    store = this.owner.lookup('service:store');

    article = await store.findRecord('article', '1');
    picture = await store.findRecord('picture', '2');
  });

  it('renders nothing when there is no argument', async function () {
    await render(hbs`<PresentationSlider />`);
    expect(this.element.textContent.trim()).to.equal('');
    const slides = this.element.querySelectorAll('.swiper-slide');

    expect(this.element.textContent.trim()).to.equal('');
    expect(slides).to.have.length(0);
  });

  it('renders nothing when there is an empty col list', async function () {
    presentation.cols = [];
    this.set('presentation', presentation);

    await render(
      hbs`<PresentationSlider @presentation={{this.presentation}} />`
    );
    const slides = this.element.querySelectorAll('.swiper-slide');

    expect(this.element.textContent.trim()).to.equal('');
    expect(slides).to.have.length(0);
  });

  it('renders two columns when they are set', async function () {
    presentation.cols = [
      {
        container: 0,
        col: 0,
        row: 0,
        name: '0.0.0',
        type: 'article',
        data: {
          source: { id: '1', model: 'article' },
        },
        modelKey: 'article_1',
      },
      {
        container: 0,
        col: 1,
        row: 0,
        name: '0.0.1',
        type: 'picture',
        data: {
          source: { id: '2', model: 'picture' },
          sizing: 'auto',
        },
        modelKey: 'picture_2',
      },
    ];

    this.set('model', {
      article_1: article,
      picture_2: picture,
    });
    this.set('presentation', presentation);

    await render(
      hbs`<PresentationSlider @presentation={{this.presentation}} @model={{this.model}} />`
    );

    expect(this.element.querySelector('h1').textContent.trim()).to.contain(
      'title'
    );
    expect(
      this.element.querySelector('.article p').textContent.trim()
    ).to.equal('some content');

    expect(
      this.element.querySelector('img').attributes.getNamedItem('src').value
    ).to.contain(
      '/test/5d5aa72acac72c010043fb59.jpg.lg.jpg'
    );
  });

  describe ('when there are two pages',  function (hooks) {
    hooks.beforeEach(async function () {
      presentation.cols = [
        {
          container: 0,
          col: 0,
          row: 0,
          name: '0.0.0',
          type: 'article',
          data: {
            source: { id: '1', model: 'article' },
          },
          modelKey: 'article_1',
        },
        {
          container: 0,
          col: 1,
          row: 0,
          name: '0.0.1',
          type: 'picture',
          data: {
            sizing: 'auto',
          },
          modelKey: 'picture_2',
        },
        {
          container: 0,
          col: 0,
          row: 1,
          name: '0.1.0',
          type: 'article',
          data: {
            source: { id: '1', model: 'article' },
          },
          modelKey: 'article_1',
        },
        {
          container: 0,
          col: 1,
          row: 1,
          name: '0.1.1',
          type: 'picture',
          data: {
            sizing: 'auto',
          },
          modelKey: 'picture_2',
        },
      ];
    });

    describe ('when there is an intro and an end',  function (hooks) {
      it('shows the first slide when the intro is done', async function () {
        presentation.hasIntro = true;
        presentation.hasEnd = true;
        presentation.introTimeout = 1000;

        this.set('presentation', presentation);

        render(hbs`<PresentationSlider @presentation={{this.presentation}} />`);

        await waitUntil(() =>
          this.element?.querySelector('.slides-welcome-intro.current')
        );

        const currentPage = this.element.querySelectorAll('.current');
        expect(currentPage).to.have.length(1);
        expect(currentPage[0]).to.have.class('slides-welcome-intro');

        await waitUntil(() =>
          this.element?.querySelector('.content-page-0.current')
        );
      });

      it('hides the controls on the intro page', async function () {
        presentation.hasIntro = true;
        presentation.hasEnd = true;
        presentation.introTimeout = 1000;

        this.set('presentation', presentation);

        render(hbs`<PresentationSlider @presentation={{this.presentation}} />`);

        await waitUntil(() =>
          this.element?.querySelector('.slides-welcome-intro.current')
        );

        const currentPage = this.element.querySelectorAll('.current');
        expect(currentPage).to.have.length(1);
        expect(currentPage[0]).to.have.class('slides-welcome-intro');
        expect(this.element.querySelector('.controls')).to.have.class('d-none');

        await waitUntil(() =>
          this.element?.querySelector('.content-page-0.current')
        );
        expect(this.element.querySelector('.controls')).not.to.have.class(
          'd-none'
        );
      });

      it('shows the end slide after the last one', async function () {
        presentation.hasIntro = true;
        presentation.hasEnd = true;
        presentation.introTimeout = 1000;
        this.set('presentation', presentation);

        await render(
          hbs`<PresentationSlider @presentation={{this.presentation}} />`
        );

        await click('.btn-next');
        await click('.btn-next');

        const currentPage = this.element.querySelectorAll('.current');
        expect(currentPage).to.have.length(1);
        expect(currentPage[0]).to.have.class('slides-welcome-end');
        expect(this.element.querySelector('.controls')).to.have.class('d-none');
      });
    });

    describe ('when there is no intro or end',  function (hooks) {
      hooks.beforeEach(async function () {
        presentation.hasIntro = false;
        presentation.hasEnd = false;
        presentation.introTimeout = 1000;
        this.set('presentation', presentation);

        this.set('model', {
          article_1: article,
          picture_2: picture,
        });

        await render(
          hbs`<PresentationSlider @presentation={{this.presentation}} @model={{this.model}} />`
        );
      });

      it('renders all columns', async function () {
        const titles = this.element.querySelectorAll('h1');
        const images = this.element.querySelectorAll('img');

        expect(titles).to.have.length(2);
        expect(images).to.have.length(2);

        expect(titles[0].textContent.trim()).to.contain('title');
        expect(titles[1].textContent.trim()).to.contain('title');

        expect(images[0].attributes.getNamedItem('src').value).to.contain(
          '/test/5d5aa72acac72c010043fb59.jpg.lg.jpg'
        );
        expect(images[1].attributes.getNamedItem('src').value).to.contain(
          '/test/5d5aa72acac72c010043fb59.jpg.lg.jpg'
        );
      });

      it('shows the first page', async function () {
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(1);
        expect(pages[0]).to.have.class('content-page-0');
      });

      it('shows the second page when the next button is pressed', async function () {
        await click('.btn-next');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(1);
        expect(pages[0]).to.have.class('content-page-1');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('2 of 2');
      });

      it('shows the second page when the next button is pressed many times', async function () {
        await click('.btn-next');
        await click('.btn-next');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(1);
        expect(pages[0]).to.have.class('content-page-1');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('2 of 2');
      });

      it('shows the first page when the prev button is pressed', async function () {
        await click('.btn-next');
        await click('.btn-prev');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(1);
        expect(pages[0]).to.have.class('content-page-0');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('1 of 2');
      });

      it('shows the first page when the prev button is pressed many times', async function () {
        await click('.btn-next');
        await click('.btn-prev');
        await click('.btn-prev');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(1);
        expect(pages[0]).to.have.class('content-page-0');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('1 of 2');
      });
    });
  });
});
