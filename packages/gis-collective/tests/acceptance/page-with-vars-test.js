/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitUntil } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';
import { setupApplicationTest } from '../helpers';
import { PageModel } from 'ogm/lib/page-model';

describe('Acceptance | page-view with variable', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedMetrics;

  hooks.beforeEach(async function () {
    PageModel.clearCache();
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam('000000000000000000000001');

    await this.store.findAll('picture');

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.get(`/mock-server/layouts/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });

    server.post(`/mock-server/metrics`, (request) => {
      const receivedMetric = JSON.parse(request.requestBody);
      receivedMetric.metric._id = '1';

      receivedMetrics.push(receivedMetric.metric);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedMetric),
      ];
    });
  });

  hooks.beforeEach(function () {
    receivedMetrics = [];
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('when there is a page that matches the slug', function (hooks) {
    let page;
    let campaign;

    hooks.beforeEach(function () {
      authenticateSession();

      PageModel.clearCache();
      page = server.testData.create.page();
      page.slug = 'section--page--:campaign-id';
      page.canEdit = true;

      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultArticle();
      campaign = server.testData.storage.addDefaultCampaign();
      server.testData.create.pagesMap['section--page--:campaign-id'] = page._id;
      server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');
      server.testData.storage.addDefaultTeam('5ca78e2160780601008f69e6');
    });

    it('can be rendered', async function () {
      await visit(`/section/page/${campaign._id}`);

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
    });

    it('renders an edit button', async function () {
      await visit(`/section/page/${campaign._id}`);
      expect(this.element.querySelector('.btn-edit-page')).to.exist;
      await click('.btn-edit-page');

      expect(currentURL()).to.equal(
        '/manage/pages/000000000000000000000001/content?selectedId=5ca78aa160780601008f6aaa',
      );
    });

    it('triggers a page view metric', async function() {
      await visit(`/section/page/${campaign._id}`);

      await waitUntil(() => receivedMetrics.length);

      expect(receivedMetrics).to.have.length(2);
      expect(receivedMetrics[1]).to.deep.contain({
        name: '000000000000000000000001',
        type: 'pageView',
        value: 1,
        labels: {},
        _id: '1'
      });
    });

    it('triggers a data view metric', async function() {
      await visit(`/section/page/${campaign._id}`);

      await waitUntil(() => receivedMetrics.length);

      expect(receivedMetrics).to.have.length(2);
      expect(receivedMetrics[0]).to.deep.contain({
        name: '5ca78aa160780601008f6aaa',
        type: 'campaignLoad',
        value: 1,
        labels: {},
        _id: '1'
      });
    });
  });

  describe('when there is a page that matches the slug, but the variable is invalid', function (hooks) {
    let page;
    let campaign;

    hooks.beforeEach(function () {
      authenticateSession();
      page = server.testData.create.page();
      page.slug = 'section--page--:campaign-id';
      page.canEdit = true;

      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultArticle();
      campaign = server.testData.storage.addDefaultCampaign();
      server.testData.create.pagesMap['section--page--:campaign-id'] = page._id;
      server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');
      server.testData.storage.addDefaultTeam('5ca78e2160780601008f69e6');
    });

    it('can be rendered', async function () {
      await visit(`/section/page/${campaign._id}`);

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
    });
  });

  describe('when there is a page that contains the variable in the path', function (hooks) {
    let page;
    let page404;

    hooks.beforeEach(function () {
      authenticateSession();
      page = server.testData.create.page();
      page.slug = 'section--page--:campaign-id';
      page.canEdit = true;

      page404 = server.testData.create.page('404');
      page404.slug = '404';
      page404.canEdit = true;

      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultArticle();
      server.testData.storage.addDefaultCampaign();
      server.testData.create.pagesMap['section--page--:campaign-id'] = page._id;
    });

    it('redirects to a new page with a valid id when the variable is valid', async function () {
      await visit(`/section/page/:campaign-id`);

      expect(currentURL()).to.equal('/section/page/5ca78aa160780601008f6aaa');

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
    });

    it('shows a generic 404 page when the variable is invalid', async function () {
      await visit(`/section/page/:other-id`);

      expect(currentURL()).to.equal('/section/page/:other-id');
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('Page Not Found');
    });

    it('shows a the 404 page when it exists', async function () {
      server.testData.create.pagesMap['404'] = page404._id;
      server.testData.storage.addPage(page404);

      await visit(`/section/page/:other-id`);

      expect(currentURL()).to.equal('/404');
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
    });
  });
});
