/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { expect } from 'chai';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/date', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Input::Date @allowUnset={{true}} />`);
    expect(this.element.textContent.trim()).to.equal('Enable');
  });
});
