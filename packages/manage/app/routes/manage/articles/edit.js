/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageArticlesEditRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const article = await this.store.findRecord('article', params['id']);
    let type = article.type;

    let newsletter;

    if (['newsletter-welcome-message', 'newsletter-article'].includes(type)) {
      newsletter = await this.store.findRecord('newsletter', article.relatedId);
    }

    const existingCategories = (await this.store.adapterFor('article').categories(article.visibility.teamId))
      .categories;

    const team = await article.visibility.fetchTeam();

    let events;
    if (['newsletter-article'].includes(type)) {
      events = await this.store.query('event', { team: team.id });
    }

    return hash({
      article,
      newsletter,
      team,
      existingCategories,
      events,
    });
  }
}
