/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class EditorLinkComponent extends Component {
  config = [
    {
      name: "link",
      type: "link",
      label: "destination",
    },
    {
      name: "newTab",
      type: "bool",
      label: "open in new tab",
    },
  ];
}
