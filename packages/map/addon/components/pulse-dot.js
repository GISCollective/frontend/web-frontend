import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class PulseDotComponent extends Component {
  @action
  handleClick() {
    return this.args.onClick?.();
  }
}
