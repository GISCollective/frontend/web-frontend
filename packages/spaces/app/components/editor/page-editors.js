/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { htmlSafe } from "@ember/template";

export default class ManageEditorsPageEditorsComponent extends Component {
  @tracked x = 0;
  @tracked y = 0;
  @tracked isMoving = false;
  @tracked width = 0;
  @tracked newLeftWidth = false;

  get style() {
    if (!this.newLeftWidth) {
      return htmlSafe("");
    }

    return htmlSafe(`width: ${this.newLeftWidth}%`);
  }

  @action
  setupContainer(element) {
    this.container = element;
  }

  @action
  setupProperties(element) {
    this.properties = element;
  }

  @action
  setupResizer(element) {
    this.resizer = element;
  }

  @action
  mouseDownHandler(e) {
    this.x = e.clientX;
    this.y = e.clientY;
    this.width = this.container.getBoundingClientRect().width;
    this.isMoving = true;
  }

  @action
  mouseUpHandler() {
    this.isMoving = false;
  }

  @action
  mouseMoveHandler(e) {
    if (!this.isMoving) {
      return;
    }

    const dx = e.clientX - this.x;

    this.newLeftWidth = ((this.width + dx) * 100) / this.resizer.parentNode.getBoundingClientRect().width;
  }

  @action
  deselect(e) {
    if (e.target != this.container) {
      return;
    }

    this.args.onDeselect?.();
  }
}
