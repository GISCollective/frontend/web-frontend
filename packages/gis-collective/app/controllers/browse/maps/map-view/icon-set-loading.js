/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class BrowseMapsMapViewSpotlightController extends Controller {
  @tracked loadingIconSet;
}
