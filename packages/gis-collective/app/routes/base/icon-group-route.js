/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from './app-route';

export default class IconGroupRoute extends AppRoute {
  addIcons(group) {
    if (Array.isArray(group['icons']) && group['icons'].length > 0) {
      group['icons'] = group['icons'].map((icon) => {
        return this.store.push(this.store.normalize('icon', icon));
      });
    }

    Object.keys(group['categories']).forEach((a) => this.addIcons(group['categories'][a]));
  }
}
