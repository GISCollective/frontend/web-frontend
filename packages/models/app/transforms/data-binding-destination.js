/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class DataBindingDestinationTransform extends Transform {
  deserialize(serialized) {
    return new DataBindingDestination(serialized);
  }

  serialize(deserialized) {
    if (deserialized?.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class DataBindingDestination {
  @tracked type = '';
  @tracked modelId = '';
  @tracked deleteNonSyncedRecords = false;

  constructor(value) {
    if (value?.modelId) {
      this.modelId = value.modelId;
    }

    if (value?.type) {
      this.type = value.type;
    }

    if (value?.deleteNonSyncedRecords) {
      this.deleteNonSyncedRecords = value.deleteNonSyncedRecords;
    }
  }

  toJSON() {
    return {
      modelId: this.modelId ?? '',
      type: this.type ?? '',
      deleteNonSyncedRecords: this.deleteNonSyncedRecords ?? false,
    };
  }
}
