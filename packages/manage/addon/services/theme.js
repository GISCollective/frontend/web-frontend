/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class ThemeService extends Service {
  @service fastboot;
  @tracked _preferred;

  get name() {
    if (['light', 'dark'].includes(this.preferred)) {
      return this.preferred;
    }

    return window?.matchMedia?.('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
  }

  get preferred() {
    if (this.fastboot.isFastBoot) {
      return 'auto';
    }
    const value = this._preferred ?? localStorage.getItem('theme');

    return ['light', 'dark'].includes(value) ? value : 'auto';
  }

  set preferred(value) {
    localStorage.setItem('theme', value);
    this._preferred = value;
  }
}
