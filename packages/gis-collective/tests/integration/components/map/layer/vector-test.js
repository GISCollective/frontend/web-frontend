/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import Map from 'ol/Map';

describe ('Integration | Component | map/layer/vector',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the map is not set', async function () {
    await render(hbs`<Map::Layer::Vector />`);
    expect(this.element.textContent.trim()).to.equal('');

    await render(hbs`<Map::Layer::Vector>test</Map::Layer::Vector>`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders', async function () {
    this.set('map', new Map());
    await render(
      hbs`<Map::Layer::Vector @map={{this.map}} as |layer| >-{{layer}}-</Map::Layer::Vector>`
    );
    expect(this.element.textContent.trim()).to.equal('-[object Object]-');
  });

  it('creates a new layer in the map', async function () {
    this.set('map', new Map());

    await render(
      hbs`<Map::Layer::Vector @map={{this.map}}></Map::Layer::Vector>`
    );
    expect(this.map.getLayers().getLength()).to.equal(1);
  });

  it('removes the layer when the component is removed', async function () {
    const map = new Map();
    this.set('map', map);
    this.set('show', true);

    await render(
      hbs`{{#if this.show}}<Map::Layer::Vector @map={{this.map}} />{{/if}}`
    );
    expect(map.getLayers().getLength()).to.equal(1);

    this.set('show', false);
    expect(map.getLayers().getLength()).to.equal(0);
  });
});
