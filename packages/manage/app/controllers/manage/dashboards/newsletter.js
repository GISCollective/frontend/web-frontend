/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from './dashboard';
import { ManageListStore } from 'manage/lib/manage-list-store';
import { getOwner } from '@ember/application';
import { action } from '@ember/object';

export default class ManageDashboardsNewsletterController extends Controller {
  manageListStore = new ManageListStore();

  get breadcrumbs() {
    if (!this.model.newsletter) {
      return [];
    }

    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        text: this.model.newsletter.name,
      },
    ];
  }

  get apiUrl() {
    const config = getOwner(this).resolveRegistration('config:environment');

    return config.apiUrl;
  }

  @action
  async downloadEmails() {
    const blob = await this.model.newsletter.downloadEmails();

    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');

    a.href = url;
    a.download = 'emails.csv';

    document.body.appendChild(a);

    a.click();
    a.remove();
  }
}
