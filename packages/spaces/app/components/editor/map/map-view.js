/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from "@glimmer/tracking";
import Component from "../base/modelItem";
import { action } from "@ember/object";

export default class ManageEditorsMapMapViewComponent extends Component {
  @tracked _destination;
  @tracked _map;

  mapConfig = [
    {
      name: "mode",
      options: ["show all features", "use a map"],
    },
    {
      name: "flyOverAnimation",
      type: "bool",
    },
    {
      name: "featureHover",
      options: ["do nothing", "mouse pointer"],
    },
    {
      name: "featureSelection",
      options: ["do nothing", "open details in panel", "go to page"],
    },
    {
      name: "enableMapInteraction",
      options: ["no", "always", "automatic"],
    },
    {
      name: "enablePinning",
      type: "bool",
    },
    {
      name: "restrictView",
      type: "bool",
    },
    {
      name: "showUserLocation",
      type: "bool",
    },
    {
      name: "showZoomButtons",
      type: "bool",
    },
    {
      name: "showBaseMapSelection",
      type: "bool",
    },
    {
      name: "showTitle",
      type: "bool",
    },
  ];

  get map() {
    if (this._map) {
      return this._map;
    }

    return this.args.value?.data?.map ?? {};
  }

  @action
  changeMap(value) {
    this._map = value;

    return this.triggerChange();
  }

  get destination() {
    if (this._destination) {
      return this._destination;
    }

    return this.args.value?.data?.map?.destination;
  }

  @action
  destinationChanged(value) {
    this._destination = value;
    this.triggerChange();
  }

  triggerChange() {
    const newData = {
      map: this.map,
      sizing: "fill",
    };

    if (this.map.mode == "use a map") {
      newData.source = this.source;
    }

    if (this.map.featureSelection == "go to page") {
      newData.map.destination = this.destination;
    }

    this.args.onChange?.(newData);
  }
}
