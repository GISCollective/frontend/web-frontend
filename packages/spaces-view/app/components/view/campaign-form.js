/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import RequiredAttributes from '../../lib/required-attributes';
import AttributesContainer from '../../attributes/containers/attributes';
import {
  addIconSection,
  handleAttributeChange,
  handleGroupRemove,
} from '../../attributes/actions';
import { fromPageState } from 'core/lib/page-state';
import GeocodingSearch from '../../lib/geocoding-search';

export default class PageColCampaignFormComponent extends Component {
  @service fastboot;
  @service position;
  @service store;
  @service intl;
  @service router;
  @service session;
  @service notifications;

  @tracked geocodingSearch;
  @tracked answer;
  @tracked baseMaps;
  @tracked optionalIcons = [];
  @tracked requiredAttributes;
  @tracked iconAttributes;
  @tracked _isDisabled;
  @tracked map;
  @tracked positionDetails;
  @tracked hasNoResults;
  @tracked campaign;

  @tracked isValidContributor;
  @tracked hasValidQuestions;
  @tracked isLoading;

  get isDisabled() {
    if (this.isLoading) {
      return true;
    }

    if (this.args.isEditor) {
      return false;
    }

    if (this.picturesRequired && !this.answer.pictures?.length) {
      return true;
    }

    if (this.showAboutYouSection && !this.isValidContributor) {
      return true;
    }

    if (!this.hasValidQuestions) {
      return true;
    }

    return this._isDisabled;
  }

  get state() {
    return fromPageState(this.args.state);
  }

  get descriptionLabel() {
    return (
      this.campaign?.options?.descriptionLabel ?? this.intl.t('description')
    );
  }

  get locationLabel() {
    let label = '';

    if (this.campaign?.options?.location?.customQuestion) {
      label = this.campaign?.options?.location?.label.trim?.();
    }

    return label || this.intl.t('Location');
  }

  get optionalIconsQuestion() {
    if (
      !this.campaign?.options?.icons.customQuestion ||
      !this.campaign?.options?.icons.label
    ) {
      return 'Please select an icon from the list';
    }

    return this.campaign.options.icons.label;
  }

  get canContribute() {
    const registrationMandatory =
      this.campaign?.options?.registrationMandatory || false;
    return registrationMandatory ? this.session.isAuthenticated : true;
  }

  get showAboutYouSection() {
    return (
      this.campaign?.contributorQuestions?.filter((a) => a.isVisible)?.length >
      0
    );
  }

  get submitBtnValue() {
    const value = JSON.parse(
      JSON.stringify(this.args.value?.data?.submit ?? {}),
    );

    if (!Array.isArray(value.classes)) {
      value.classes = [];
    }

    if (this.isDisabled) {
      value.classes = value.classes.filter((a) => a.indexOf('btn-') != 0);
      value.classes.push('btn-secondary');
    }

    return value;
  }

  @action
  validateContributor(value) {
    this.isValidContributor = value;
  }

  @action
  validateQuestions(value) {
    this.hasValidQuestions = value;
  }

  @action
  changeIcons(icons) {
    this.answer.icons = icons;

    const names = icons.map((a) => a.name);
    names.push('position details');
    names.push('about');

    icons.forEach((icon) => {
      if (!this.answer.attributes[icon.name]) {
        addIconSection(this.answer, icon);
      }
    });

    Object.keys(this.answer.attributes)
      .filter((key) => names.indexOf(key) == -1)
      .forEach((key) => {
        delete this.answer.attributes[key];
      });

    this.iconAttributes.feature = this.answer;
  }

  @action
  handleRemove(key, index) {
    handleGroupRemove(this.answer, key, index);
    this.iconAttributes.feature = this.answer;
    this.checkRequiredAttributes();
  }

  @action
  addSection(icon) {
    addIconSection(this.answer, icon);
    this.iconAttributes.feature = this.answer;
    this.checkRequiredAttributes();
  }

  @action
  changeAttributes(values) {
    const specialKeys = [
      'position',
      'name',
      'description',
      'positionDetails',
      'icons',
      'pictures',
      'sounds',
    ];

    if (values.position) {
      this.answer.position = values.position;
    }

    if (values.name) {
      this.answer.title = values.name;
    }

    if (values.description) {
      this.answer.content = values.description;
    }

    if (values.positionDetails) {
      this.answer.attributes['position details'] = values.positionDetails;
    }

    if (values.icons) {
      this.answer.icons = [...this.defaultIcons, ...values.icons];
      this.iconAttributes.feature = this.answer;
    }

    if (values.pictures) {
      this.answer.pictures = values.pictures;
    }

    if (values.sounds) {
      this.answer.sounds = values.sounds;
    }

    const otherKeys = Object.keys(values).filter(
      (a) => !specialKeys.includes(a),
    );

    if (
      otherKeys.length > 0 &&
      typeof this.answer.attributes['other'] != 'object'
    ) {
      this.answer.attributes['other'] = {};
    }

    if (otherKeys.length > 0) {
      for (const key of otherKeys) {
        this.answer.attributes['other'][key] = values[key];
      }
    }
  }

  @action
  handleAnswer(group, key, value) {
    handleAttributeChange(this.answer, group.key, group.index, key, value);
  }

  @action
  checkRequiredAttributes() {
    this._isDisabled = this.requiredAttributes.validate(this.answer);
  }

  resizePictures() {
    if (!this.answer.pictures.pictureOptimization) {
      return [];
    }

    return Promise.all(
      this.answer.pictures
        .filter((a) => a.isNew)
        .map((a) => a.localResize(1920, 1920)),
    );
  }

  @action
  async submit() {
    if (this.args.isEditor) {
      return false;
    }

    this.isLoading = true;

    try {
      await this.resizePictures();

      const pictures = (await this.answer.pictures) ?? [];
      const sounds = (await this.answer.sounds) ?? [];

      await Promise.all(pictures.filter((a) => a.isNew).map((a) => a.save()));
      await Promise.all(sounds.filter((a) => a.isNew).map((a) => a.save()));

      await this.answer.save();

      await this.router.transitionTo('campaigns.success', this.campaign.id);
    } catch (err) {
      console.error(err);
      this.notifications.handleError(err);
    }

    this.isLoading = false;
  }

  @action
  async setup(campaign) {
    this.campaign = campaign;
    this.geocodingSearch = new GeocodingSearch(this.store);
    this.defaultIcons = [];

    try {
      this.defaultIcons = (await this.campaign.icons).slice();
    } catch (err) {
      console.log(err);
    }

    try {
      this.optionalIcons = (await this.campaign.optionalIcons).slice();
    } catch (err) {
      console.log(err);
    }

    this.iconAttributes = new AttributesContainer(this.intl);

    this.requiredAttributes = new RequiredAttributes();

    this.answer = this.store.createRecord('campaign-answer', {
      campaign: this.campaign,
      icons: this.defaultIcons,
      attributes: {},
    });

    this.defaultIcons.forEach((icon) => {
      addIconSection(this.answer, icon);
    });

    this.iconAttributes.feature = this.answer;
  }

  @action
  async searchGeocoding(term) {
    this.hasNoResults = false;

    if (!term) {
      return this.geocodingSearch.clear();
    }

    return await this.geocodingSearch.search(term);
  }

  @action
  change(fieldName, value) {
    this.answer[fieldName] = value;
  }
}
