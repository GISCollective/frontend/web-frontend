/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class FiltersIssuesComponent extends Component {
  @service intl;
  @service clickOutside;
  @tracked showSuggestions = false;

  get isWithIssues() {
    return this.args.value === 'true';
  }

  get niceValue() {
    if (this.isWithIssues) {
      return this.intl.t('with issues');
    }

    return null;
  }

  @action
  toggle() {
    if (this.isWithIssues) {
      return;
    }

    this.args.onChange?.('true');
  }

  @action
  reset() {
    this.args.onChange?.(null);
  }
}
