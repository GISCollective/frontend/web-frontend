/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { hasMany, attr } from '@ember-data/model';

export default class GeocodingModel extends Model {
  @attr('string') name;
  @attr('number') score;
  @attr('geo') geometry;

  @hasMany('icon', { async: true, inverse: null })
  icons;

  @attr license;
}
