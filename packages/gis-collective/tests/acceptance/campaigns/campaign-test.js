/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click, waitUntil, fillIn, triggerEvent, blur, waitFor } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import { transform } from 'ol/proj';

describe('Acceptance | campaigns/campaign', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let campaign;
  let picture;
  let geolocation;
  let geocoding;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultTeam();
    geocoding = server.testData.storage.addDefaultGeocoding();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultMap('5ca89e37ef1f7e010007f54c');
    picture = server.testData.storage.addDefaultPicture();

    const positionService = this.owner.lookup('service:position');
    geolocation = {
      watchPosition(callback) {
        callback({
          coords: {
            longitude: 1,
            latitude: 2,
            accuracy: 14,
            altitude: 15,
            altitudeAccuracy: 16,
          },
        });

        return 1;
      },
      clearWatch() {},
    };

    positionService.geolocation = geolocation;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('for a public campaign', function (hooks) {
    hooks.beforeEach(async function () {
      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));

      server.testData.storage.addDefaultBaseMap();
      await visit(`/campaigns/${campaign._id}`);
    });

    it('can visit /campaigns/:id', async function () {
      expect(currentURL()).to.equal(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('h1')).not.to.equal(`Page Not Found`);
    });

    it('should display the campaign article', async function () {
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('Campaign 1');
      expect(this.element.querySelector('.article p').textContent.trim()).to.equal(
        "We'd like to collect urban nature sounds that characterize Berlin's natural life. Please help us find interesting places for sound recording! Here are the places and sounds we collected so far on the Berlin Sound Map."
      );
    });

    it('should display the campaign cover', async function () {
      expect(this.element.querySelector('.cover')).to.have.attribute(
        'style',
        `background-image: url('${picture.picture}/md')`
      );
    });

    it('should not display the unpublished icon', async function () {
      expect(this.element.querySelector('.cover')).to.have.attribute(
        'style',
        `background-image: url('${picture.picture}/md')`
      );
    });

    it('should contain all the browse links with the first site map', async function () {
      expect(PageElements.breadcrumbs()).to.deep.equal(['Campaigns', campaign.name]);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/campaigns']);
    });
  });

  describe('for a public campaign with an invalid map', function (hooks) {
    hooks.beforeEach(async function () {
      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));
      campaign.map.map = 'missing';

      server.testData.storage.addDefaultBaseMap();
    });

    it('should display the campaign article', async function () {
      await visit(`/campaigns/${campaign._id}`);

      expect(this.element.querySelector('.group-position-details')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('Campaign 1');
      expect(this.element.querySelector('.article p').textContent.trim()).to.equal(
        "We'd like to collect urban nature sounds that characterize Berlin's natural life. Please help us find interesting places for sound recording! Here are the places and sounds we collected so far on the Berlin Sound Map."
      );
    });
  });

  describe('for a public campaign that is associated to a map', function (hooks) {
    let receivedAnswer;

    hooks.beforeEach(async function () {
      server.post(`/mock-server/campaignanswers`, (request) => {
        receivedAnswer = JSON.parse(request.requestBody);
        receivedAnswer.campaignAnswer._id = 'some-answer';

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedAnswer)];
      });
    });

    it('can set a custom position', async function () {
      const map = server.testData.storage.addDefaultMap();
      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));
      campaign.map = {
        map: map._id,
        isEnabled: true,
      };
      campaign.area = {
        type: 'Polygon',
        coordinates: [
          [
            [-4.420499850423413, 55.80883615353929],
            [-4.102271005870181, 55.80883615353929],
            [-4.102271005870181, 55.90650283086345],
            [-4.420499850423413, 55.90650283086345],
            [-4.420499850423413, 55.80883615353929],
          ],
        ],
      };
      server.testData.storage.addDefaultBaseMap();
      campaign.baseMaps = map.baseMaps;

      await visit(`/campaigns/${campaign._id}`);

      await click('.btn-manual');

      expect(this.element.querySelector('.map-marker.ol-point')).not.to.exist;

      const olMap = this.element.querySelector('.map').olMap;
      const view = olMap.getView();
      view.setZoom(16);
      view.setCenter(transform([30, 40], 'EPSG:4326', 'EPSG:3857'));

      await click('.btn-submit-location');

      await click('.btn-submit-campaign');

      await waitUntil(() => receivedAnswer);

      expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);

      expect(receivedAnswer).to.deep.equal({
        campaignAnswer: {
          position: { type: 'Point', coordinates: [30, 40] },
          attributes: {
            'position details': {
              altitude: +0,
              accuracy: 1000,
              altitudeAccuracy: 1000,
              type: 'manual',
            },
          },
          campaign: '5ca78aa160780601008f6aaa',
          icons: [],
          featureId: null,
          _id: 'some-answer',
        },
      });
    });

    it('does not select a position type by default', async function () {
      const map = server.testData.storage.addDefaultMap();
      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));

      campaign.map = {
        map: map._id,
        isEnabled: true,
      };
      campaign.area = {
        type: 'Polygon',
        coordinates: [
          [
            [-4.420499850423413, 55.80883615353929],
            [-4.102271005870181, 55.80883615353929],
            [-4.102271005870181, 55.90650283086345],
            [-4.420499850423413, 55.90650283086345],
            [-4.420499850423413, 55.80883615353929],
          ],
        ],
      };
      server.testData.storage.addDefaultBaseMap();
      campaign.baseMaps = map.baseMaps;

      await visit(`/campaigns/${campaign._id}`);

      expect(this.element.querySelector('.btn-location-option.btn-success')).not.to.exist;
      expect(this.element.querySelector('.group-map')).not.to.exist;
    });

    it('sets the user location extent when the gps button is pressed', async function () {
      const map = server.testData.storage.addDefaultMap();
      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));

      campaign.map = {
        map: map._id,
        isEnabled: true,
      };
      campaign.area = {
        type: 'Polygon',
        coordinates: [
          [
            [7, 48],
            [7, 32],
            [27, 32],
            [27, 48],
            [7, 48],
          ],
        ],
      };
      server.testData.storage.addDefaultBaseMap();
      campaign.baseMaps = map.baseMaps;

      let position = this.owner.lookup('service:position');
      position.longitude = 17;
      position.latitude = 38;
      position.status = 'WATCHING';

      await visit(`/campaigns/${campaign._id}`);

      await click('.btn-gps');

      const olMap = this.element.querySelector('.map').olMap;
      const view = olMap.getView();
      const z = view.getZoom();
      const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

      expect(parseInt(z)).to.equal(15);
      expect(center.map((a) => parseInt(a))).to.deep.equal([17, 38]);
    });

    describe('using the geocoding feature', (hooks) => {
      let map;

      hooks.beforeEach(async function () {
        map = server.testData.storage.addDefaultMap();
        campaign = server.testData.storage.addDefaultCampaign();
        campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));

        campaign.map = {
          map: map._id,
          isEnabled: true,
        };
        campaign.area = {
          type: 'Polygon',
          coordinates: [
            [
              [7, 48],
              [7, 32],
              [27, 32],
              [27, 48],
              [7, 48],
            ],
          ],
        };
        server.testData.storage.addDefaultBaseMap();
        campaign.baseMaps = map.baseMaps;

        let position = this.owner.lookup('service:position');
        position.longitude = 17;
        position.latitude = 38;
      });

      it('can use the search field with manual position', async function () {
        await visit(`/campaigns/${campaign._id}`);

        await click('.btn-manual');

        await fillIn('.input-search-container .input-search', 'some value');
        await click('.input-search-container .btn-search');
        await click('.input-search-container .list-group-item-action');

        let olMap = this.element.querySelector('.map').olMap;
        let view = olMap.getView();
        let z = view.getZoom();
        let center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

        expect(parseInt(z)).to.equal(17);
        expect(center.map((a) => Math.ceil(a * 100) / 100)).to.deep.equal([13.44, 52.5]);

        await click('.btn-submit-location');

        olMap = this.element.querySelector('.map').olMap;
        view = olMap.getView();
        z = view.getZoom();
        center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

        expect(parseInt(z)).to.equal(15);
        expect(center.map((a) => Math.ceil(a * 100) / 100)).to.deep.equal([13.44, 52.5]);

        await click('.btn-submit-campaign');
        await waitUntil(() => receivedAnswer);

        const recievedCoordinates = receivedAnswer.campaignAnswer.position.coordinates.map(
          (a) => Math.ceil(a * 100) / 100
        );

        expect(recievedCoordinates).to.deep.equal([13.44, 52.5]);
      });

      it('keeps the map extent for outside points after an address was selected', async function () {
        geocoding.geometry.coordinates = [1, 2];
        server.testData.storage.addGeocoding(geocoding);

        await visit(`/campaigns/${campaign._id}`);

        await click('.btn-address');

        await fillIn('.card-address input', 'some value');
        await click('.btn-search-address');

        let olMap = this.element.querySelector('.map').olMap;
        let view = olMap.getView();
        let z = view.getZoom();
        let center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

        expect(parseInt(z)).to.equal(15);
        expect(center.map((a) => Math.ceil(parseInt(a * 100) / 100))).to.deep.equal([1, 2]);

        await click('.picker-options');
        await click('.btn-submit-location');

        olMap = this.element.querySelector('.map').olMap;
        view = olMap.getView();
        z = view.getZoom();
        center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

        expect(parseInt(z)).to.equal(15);
        expect(center.map((a) => Math.ceil(parseInt(a * 100) / 100))).to.deep.equal([1, 2]);

        await click('.btn-submit-campaign');
        await waitUntil(() => receivedAnswer);

        const recievedCoordinates = receivedAnswer.campaignAnswer.position.coordinates.map((a) =>
          Math.ceil(parseInt(a * 100) / 100)
        );

        expect(recievedCoordinates).to.deep.equal([1, 2]);
      });
    });

    it('sets the map point center when the selected point is outside the extent', async function () {
      const map = server.testData.storage.addDefaultMap();
      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));

      campaign.map = {
        map: map._id,
        isEnabled: true,
      };
      campaign.area = {
        type: 'Polygon',
        coordinates: [
          [
            [7, 48],
            [7, 32],
            [27, 32],
            [27, 48],
            [7, 48],
          ],
        ],
      };
      server.testData.storage.addDefaultBaseMap();
      campaign.baseMaps = map.baseMaps;

      await visit(`/campaigns/${campaign._id}`);

      await click('.btn-manual');

      expect(this.element.querySelector('.map-marker.ol-point')).not.to.exist;

      const olMap = this.element.querySelector('.map').olMap;
      const view = olMap.getView();
      view.setZoom(16);
      view.setCenter(transform([1, 2], 'EPSG:4326', 'EPSG:3857'));

      const z = view.getZoom();
      const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

      await click('.btn-submit-location');

      expect(parseInt(z)).to.equal(16);
      expect(center.map((a) => Math.round(a))).to.deep.equal([1, 2]);
    });

    it('ignores missing basemaps', async function () {
      const map = server.testData.storage.addDefaultMap();
      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));

      campaign.map = {
        map: map._id,
        isEnabled: true,
      };
      campaign.area = {
        type: 'Polygon',
        coordinates: [
          [
            [-4.420499850423413, 55.80883615353929],
            [-4.102271005870181, 55.80883615353929],
            [-4.102271005870181, 55.90650283086345],
            [-4.420499850423413, 55.90650283086345],
            [-4.420499850423413, 55.80883615353929],
          ],
        ],
      };
      server.testData.storage.addDefaultBaseMap();
      campaign.baseMaps = map.baseMaps;
      campaign.baseMaps.list.push('missing');

      await visit(`/campaigns/${campaign._id}`);

      await click('.btn-manual');

      expect(this.element.querySelector('.map-layer-base')).to.exist;
      expect(this.element.querySelector('.map-layer-base').textContent.trim()).to.equal('000000111111222222333333');
    });
  });

  describe('for a public campaign that needs registration', function (hooks) {
    hooks.beforeEach(async function () {
      campaign = server.testData.create.campaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));

      campaign.options = {
        registrationMandatory: true,
      };
      server.testData.storage.addCampaign(campaign);

      await visit(`/campaigns/${campaign._id}`);
    });

    it('can visit /campaigns/:id', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(currentURL()).to.equal(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('h1')).not.to.equal(`Page Not Found`);
    });

    it('should display the campaign article', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('h1').textContent.trim()).to.equal('Campaign 1');
      expect(this.element.querySelector('.article p').textContent.trim()).to.equal(
        "We'd like to collect urban nature sounds that characterize Berlin's natural life. Please help us find interesting places for sound recording! Here are the places and sounds we collected so far on the Berlin Sound Map."
      );
      expect(this.element.querySelector('.cover')).to.have.attribute(
        'style',
        `background-image: url('${picture.picture}/md')`
      );
    });

    it('shows a message that the user needs to be registered', async function () {
      await visit(`/campaigns/${campaign._id}`);

      expect(this.element.querySelector('.alert-warning')).to.exist;
      expect(this.element.querySelector('.alert-link')).to.exist;
      expect(this.element.querySelector('.alert-link')).to.have.attribute('href', '/login');
    });
  });

  describe('for a private editable campaign', function (hooks) {
    let receivedCampaign;

    hooks.beforeEach(async () => {
      campaign = server.testData.create.campaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));
      campaign.canEdit = true;
      campaign.visibility.isPublic = false;

      server.testData.storage.addCampaign(campaign);
      authenticateSession();
      await visit(`/campaigns/${campaign._id}`);

      server.put(`/mock-server/campaigns/${campaign._id}`, (request) => {
        receivedCampaign = JSON.parse(request.requestBody);
        receivedCampaign.campaign._id = campaign._id;

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedCampaign)];
      });
    });

    it('should display the unpublished icon', async function () {
      expect(this.element.querySelector('.icon-not-published')).to.exist;
    });

    it('should allow editing the campaign', async function () {
      await click('.subtitle-container .dropdown-item-edit');
      expect(currentURL()).to.equal(`/manage/surveys/edit/${campaign._id}`);
    });

    it('should allow publishing the campaign', async function () {
      await click('.subtitle-container .btn-manage-publish');

      await waitUntil(() => receivedCampaign != null, { timeout: 3000 });
      expect(receivedCampaign.campaign.visibility.isPublic).to.equal(true);
    });
  });

  describe('for a public editable campaign', function (hooks) {
    let receivedCampaign;

    hooks.beforeEach(async () => {
      campaign = server.testData.create.campaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));
      campaign.canEdit = true;
      campaign.visibility.isPublic = true;

      server.testData.storage.addCampaign(campaign);
      authenticateSession();
      await visit(`/campaigns/${campaign._id}`);

      server.put(`/mock-server/campaigns/${campaign._id}`, (request) => {
        receivedCampaign = JSON.parse(request.requestBody);
        receivedCampaign.campaign._id = campaign._id;

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedCampaign)];
      });
    });

    it('should allow unpublishing the campaign', async function () {
      await click('.subtitle-container .btn-manage-unpublish');

      await waitUntil(() => receivedCampaign != null, { timeout: 3000 });
      expect(receivedCampaign.campaign.visibility.isPublic).to.equal(false);
    });
  });

  describe('the campaign questions', function (hooks) {
    let icon1;
    let icon2;
    let icon3;
    let receivedAnswer;
    let receivedPictures;

    hooks.beforeEach(async function () {
      icon1 = server.testData.create.icon('000000000000000000000001');
      icon1.name = 'icon 1';
      icon1.localName = 'local icon 1';
      icon1.attributes = [
        {
          name: 'attribute1',
          help: 'some help message',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon1);

      icon2 = server.testData.create.icon('000000000000000000000002');
      icon2.name = 'icon 2';
      icon2.localName = 'local icon 2';
      icon2.allowMany = true;
      icon2.attributes = [
        {
          name: 'attribute1',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon2);

      icon3 = server.testData.create.icon('000000000000000000000003');
      icon3.name = 'icon 3';
      icon3.localName = 'local icon 3';
      icon3.allowMany = true;
      icon3.attributes = [
        {
          name: 'attribute1',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon3);

      campaign = server.testData.storage.addDefaultCampaign();
      campaign.questions = campaign.questions.map((a) => ({ ...a, isRequired: false }));
      campaign.icons = [icon1._id, icon2._id];
      campaign.optionalIcons = [icon3._id];

      server.post(`/mock-server/campaignanswers`, (request) => {
        receivedAnswer = JSON.parse(request.requestBody);
        receivedAnswer.campaignAnswer._id = 'some-answer';

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedAnswer)];
      });

      receivedPictures = [];
      server.post(
        '/mock-server/pictures',
        (request) => {
          const pictureRequest = JSON.parse(request.requestBody);
          pictureRequest.picture['_id'] = receivedPictures.length + 1;
          receivedPictures.push(pictureRequest);

          return [200, { 'Content-Type': 'application/json' }, JSON.stringify(pictureRequest)];
        },
        false
      );
    });

    describe('a campaign with no custom labels', (hooks) => {
      hooks.beforeEach(async function () {
        await visit(`/campaigns/${campaign._id}`);
        await PageElements.wait(300);
      });

      it('renders the about section', function () {
        expect(this.element.querySelector('.group-about .card-header')).not.to.exist;
      });

      it('should show all the icon questions', async function (a) {
        let currentUrl = currentURL();

        await click('.btn-gps');

        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn('.card-icon-2 input', 'value 2');

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        a.deepEqual(receivedAnswer.campaignAnswer, {
          position: { type: 'Point', coordinates: [1, 2] },
          attributes: {
            'icon 1': { attribute1: 'value 1' },
            'icon 2': [{ attribute1: 'value 2' }],
            'position details': { accuracy: 14, altitude: 15, altitudeAccuracy: 16, type: 'gps' },
          },
          campaign: '5ca78aa160780601008f6aaa',
          icons: ['000000000000000000000001', '000000000000000000000002'],
          _id: 'some-answer',
          featureId: null,
        });

        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });

      it('should allow adding a second icon instance', async function (a) {
        let currentUrl = currentURL();

        await click('.btn-gps');
        await click('.btn-add-icon-2');

        const inputs = this.element.querySelectorAll('.card-icon-2 input');
        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn(inputs[0], 'value 2');
        await fillIn(inputs[1], 'value 3');

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        a.deepEqual(receivedAnswer, {
          campaignAnswer: {
            position: { type: 'Point', coordinates: [1, 2] },
            attributes: {
              'icon 1': { attribute1: 'value 1' },
              'icon 2': [{ attribute1: 'value 2' }, { attribute1: 'value 3' }],
              'position details': { accuracy: 14, altitude: 15, altitudeAccuracy: 16, type: 'gps' },
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            featureId: null,
            _id: 'some-answer',
          },
        });

        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });
    });
  });
});
