/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from "qunit";
import { setupRenderingTest, it } from "dummy/tests/helpers";
import { render, waitUntil } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";
import FastBootMock from "core/test-support/fast-boot-mock";

module("Integration | Component | components/view/redirect", function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.owner.register("service:fastboot", FastBootMock);
  });

  it("renders renders a message with the redirect", async function (a) {
    this.set("value", {
      data: {
        redirect: {
          destination: {
            url: "https://giscollective.com",
          },
        },
      },
    });

    await render(hbs`<View::Redirect @value={{this.value}} />`);

    expect(
      this.element
        .querySelector(".message")
        .textContent.trim()
        .split("\n")
        .map((a) => a.trim())
        .join(""),
    ).to.equal("This page has been moved to:https://giscollective.com");
  });

  it("sets the redirect header to the destination url when it is not in edit mode and rendered with fastboot", async function (a) {
    let headService = this.owner.lookup("service:head-data");
    let fastboot = this.owner.lookup("service:fastboot");
    fastboot.isFastBoot = true;

    this.set("value", {
      data: {
        redirect: {
          destination: {
            url: "https://giscollective.com",
          },
        },
      },
    });

    await render(hbs`<View::Redirect @value={{this.value}} @isEditor={{false}} />`);

    await waitUntil(() => headService.redirect);

    expect(headService.redirect).to.equal("https://giscollective.com");
    expect(fastboot.response.statusCode).to.equal(302);
    expect(fastboot.response.headers.location).to.deep.equal(["https://giscollective.com"]);
  });

  it("adds the current space domain when the link starts with /", async function (a) {
    let headService = this.owner.lookup("service:head-data");
    let fastboot = this.owner.lookup("service:fastboot");
    let space = this.owner.lookup("service:space");
    space.currentSpace = { domain: "giscollective.com" };
    fastboot.isFastBoot = true;

    this.set("value", {
      data: {
        redirect: {
          destination: {
            url: "/test",
          },
        },
      },
    });

    await render(hbs`<View::Redirect @value={{this.value}} @isEditor={{false}} />`);

    await waitUntil(() => headService.redirect);

    expect(headService.redirect).to.equal("https://giscollective.com/test");
    expect(fastboot.response.statusCode).to.equal(302);
    expect(fastboot.response.headers.location).to.deep.equal(["https://giscollective.com/test"]);
  });

  it("does not set the redirect header to the destination url when it is not in edit mode and not rendered with fastboot", async function (a) {
    let headService = this.owner.lookup("service:head-data");
    let fastboot = this.owner.lookup("service:fastboot");
    fastboot.isFastBoot = false;

    this.set("value", {
      data: {
        redirect: {
          destination: {
            url: "https://giscollective.com",
          },
        },
      },
    });

    await render(hbs`<View::Redirect @value={{this.value}} @isEditor={{false}} />`);

    expect(headService.redirect).not.to.exist;
  });

  it("does not set the redirect header to the destination url when it is in edit", async function (a) {
    let headService = this.owner.lookup("service:head-data");
    let fastboot = this.owner.lookup("service:fastboot");
    fastboot.isFastBoot = true;

    this.set("value", {
      data: {
        redirect: {
          destination: {
            url: "https://giscollective.com",
          },
        },
      },
    });

    await render(hbs`<View::Redirect @value={{this.value}} @isEditor={{true}} />`);

    expect(headService.redirect).not.to.exist;
  });
});
