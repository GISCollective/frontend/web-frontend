/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";
import { expect } from "chai";

describe("Integration | Component | editor/properties/link-named", function (hooks) {
  setupRenderingTest(hooks);
  let changedValue;
  let server;
  let space;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage("000000000000000000000002");

    space = server.testData.storage.addDefaultSpace();
    space.getPagesMap = () => {
      return {
        page1: "000000000000000000000001",
        page2: "000000000000000000000002",
        page3: "000000000000000000000003",
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("space", space);
    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders nothing when the value is not set", async function () {
    await render(hbs`<Editor::Properties::LinkNamed />`);

    expect(this.element.querySelector(".link-name").value).to.equal("");
    expect(this.element.querySelector(".ember-power-select-selected-item")).not.to.exist;
  });

  it("renders the value when set", async function () {
    this.set("value", {
      name: "some name",
      link: {
        pageId: "000000000000000000000001",
      },
    });

    await render(hbs`<Editor::Properties::LinkNamed @value={{this.value}} @space={{this.space}} />`);

    expect(this.element.querySelector(".link-name").value).to.equal("some name");
    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("page1");
  });

  it("can change the name", async function () {
    this.set("value", {
      name: "some name",
      link: {
        pageId: "000000000000000000000001",
      },
      newTab: false,
    });

    await render(
      hbs`<Editor::Properties::LinkNamed @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`,
    );

    await fillIn(".link-name", "new name");

    expect(this.element.querySelector(".link-name").value).to.equal("new name");
    expect(changedValue).to.deep.equal({
      name: "new name",
      link: { pageId: "000000000000000000000001" },
      newTab: false,
    });
  });

  it("can change the link", async function () {
    this.set("value", {
      name: "some name",
      link: {
        pageId: "000000000000000000000001",
      },
      newTab: false,
    });

    await render(
      hbs`<Editor::Properties::LinkNamed @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`,
    );

    await selectSearch(".ember-power-select-trigger", "https://giscollective.com");

    await selectChoose(".ember-power-select-trigger", "https://giscollective.com");

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "https://giscollective.com",
    );

    expect(changedValue).to.deep.equal({
      name: "some name",
      link: { url: "https://giscollective.com" },
      newTab: false,
    });
  });

  it("can change the open in new tab option", async function () {
    this.set("value", {
      name: "some name",
      link: {
        pageId: "000000000000000000000001",
      },
    });

    await render(
      hbs`<Editor::Properties::LinkNamed @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`,
    );

    this.element.querySelector(".open-in-new-tab").value = "yes";
    await triggerEvent(".open-in-new-tab", "change");

    expect(changedValue).to.deep.equal({
      name: "some name",
      link: { pageId: "000000000000000000000001" },
      newTab: true,
    });
  });
});
