/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../../../base/app-route';
import { hash } from 'rsvp';
import { service } from '@ember/service';

export default class BrowseMapsMapViewSpotlightRoute extends Route {
  @service fullscreen;
  @service fastboot;
  @service icons;
  @service embed;

  async model(params) {
    const iconsQuery = {};

    if (!this.embed.allIcons) {
      iconsQuery.limit = 9;
    }

    const parentModel = this.modelFor('browse.maps.map-view');
    await parentModel.map?.iconSets?.fetch?.();

    let iconSets = [];

    if (parentModel.map?.iconSets?.list?.length) {
      iconSets = await Promise.all(parentModel.map.iconSets.list);
      iconsQuery.map = parentModel.map.id;
    } else {
      iconSets = await this.store.query('icon-set', { default: true });
    }

    if (iconSets.length == 1) {
      iconsQuery.limit = undefined;
    }

    const icons = {};

    const iconPromises = [];

    iconSets.forEach((iconSet) => {
      const id = iconSet.get('id');
      const name = iconSet.get('name');

      const list = this.icons.query({
        ...iconsQuery,
        iconSet: id,
      });
      icons[name] = list;
      iconPromises.push(list);
    });

    await Promise.all(iconPromises);

    return hash({
      iconSets,
      icons,
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    // eslint-disable-next-line ember/no-controller-access-in-routes
    controller.parent = this.controllerFor('browse.maps.map-view');
  }

  activate() {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = true;
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = false;
    controller.allFeatures = null;
  }
}
