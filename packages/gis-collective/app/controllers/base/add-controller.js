/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddController extends Controller {
  @service notifications;
  @service router;
  @service user;
  @service store;
  @service intl;

  queryParams = ['all', 'next'];

  @tracked all = false;
  @tracked hasTeam;
  @tracked team;

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  change(field, value) {
    this.editableModel.set(field, value);
    this.hasTeam = !!this.editableModel.visibility.team;

    if (field == 'visibility.team') {
      this.team = value;
    }
  }

  @action
  async save() {
    let result;

    try {
      result = await this.editableModel.save();
    } catch (err) {
      return this.notifications.handleError(err);
    }

    this.router.transitionTo(this.destination, result.id);

    return result;
  }
}
