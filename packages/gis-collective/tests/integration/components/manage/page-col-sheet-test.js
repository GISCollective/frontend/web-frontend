/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, waitFor, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import click from '@ember/test-helpers/dom/click';

describe ('Integration | Component | manage/page-col-sheet',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders a hidden sheet', async function () {
    await render(hbs`<Manage::PageColSheet />`);
    expect(
      this.element.querySelector('.offcanvas-body').textContent.trim()
    ).to.equal('');

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });

  it('renders a visible sheet when @show is true', async function () {
    await render(hbs`<Manage::PageColSheet @show={{true}}/>`);

    await waitUntil(() => !this.element.querySelector('.showing'));
    await waitUntil(() => !this.element.querySelector('.showing'));

    expect(this.element.querySelector('.offcanvas')).to.have.class('show');
  });

  it('closes the sheet when the close button is pressed', async function () {
    await render(hbs`<Manage::PageColSheet @show={{true}}/>`);

    await click('.btn-close');
    await waitUntil(() => !this.element.querySelector('.offcanvas.hiding'));

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });

  it('allows editing the blocks content', async function () {
    let value;
    this.set('onChange', (v) => {
      value = v.blocks;
    });

    await render(
      hbs`<Manage::PageColSheet @show={{true}} @type="blocks" @onChange={{this.onChange}}/>`
    );
    await waitFor('.editor-is-ready', { timeout: 5000 });

    await click('.add-paragraph-link');

    await waitUntil(() => !this.element.querySelector(".btn-set").attributes.getNamedItem("disabled"));

    await click('.btn-set');

    expect(value).deep.equal([
      { type: 'paragraph', data: { text: 'New paragraph' } },
    ]);
    await waitUntil(() => !this.element.querySelector('.offcanvas.hiding'));
    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });
});
