/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | card/title-description', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<Card::TitleDescription />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the campaign title', async function () {
    this.set('campaign', {
      id: 'some-id',
      name: 'some name',
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    expect(this.element.querySelector('.name').textContent.trim()).to.equal('some name');
  });

  it('does not render the campaign picture', async function () {
    this.set('campaign', {
      id: 'some-id',
      cover: {
        picture: 'some-picture',
      },
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    expect(this.element.querySelector('.picture-bg')).not.to.exist;
  });

  it('renders the campaign first paragraph', async function () {
    this.set('campaign', {
      id: 'some-id',
      cover: {
        picture: 'some-picture',
      },
      firstParagraph: 'description',
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    expect(this.element.textContent).to.contain('description');
    expect(this.element.querySelector('.card-description')).to.exist;
  });

  it('does not render the campaign options', async function () {
    this.set('campaign', {
      id: 'some-id',
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    expect(this.element.querySelector('.campaign-options')).not.to.exist;
  });

  it('renders the campaign options when the campaign is editable', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
      constructor: {
        modelName: 'campaign',
      },
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    expect(this.element.querySelector('.record-options')).to.exist;
  });

  it('allows to delete an editable campaign', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
      constructor: {
        modelName: 'campaign',
      },
    });

    let deletedCampaign;
    this.set('onDelete', (val) => {
      deletedCampaign = val;
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} @onDelete={{this.onDelete}} />`);

    await click('.record-options');
    await click('.btn-delete');

    expect(deletedCampaign).deep.equal({
      id: 'some-id',
      canEdit: true,
      constructor: {
        modelName: 'campaign',
      },
    });
  });

  it('allows to edit an editable campaign', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
      constructor: {
        modelName: 'campaign',
      },
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    await click('.record-options');
    expect(this.element.querySelector('.dropdown-item-edit')).to.exist;
  });

  it('shows an unpublished icon when the campaign is not public', async function () {
    this.set('campaign', {
      id: 'some-id',
      visibility: {
        isPublic: false,
      },
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    expect(this.element.querySelector('.icon-not-published')).to.exist;
  });

  it('does not show an unpublished icon when the campaign is public', async function () {
    this.set('campaign', {
      id: 'some-id',
      visibility: {
        isPublic: true,
      },
    });

    await render(hbs`<Card::TitleDescription @record={{this.campaign}} />`);

    expect(this.element.querySelector('.icon-not-published')).not.to.exist;
  });
});
