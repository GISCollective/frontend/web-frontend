/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/newsletter', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let spaceData;
  let receivedEmail;

  hooks.before(function () {
    server = new TestServer();
    spaceData = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultNewsletter('1');

    server.post('/mock-server/newsletters/1/subscribe', (request) => {
      receivedEmail = JSON.parse(request.requestBody);
      return [201, {}, ''];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    receivedEmail = null;

    const newsletter_1 = await this.store.findRecord('newsletter', '1');

    this.set('space', await this.store.findRecord('space', spaceData._id));
    this.set('model', { newsletter_1 });
  });

  it('can render the grouped form with floating labels', async function () {
    this.set('value', {
      data: {
        source: { model: 'newsletter', id: '1' },
        form: {
          floatingLabels: true,
          inputGroup: true,
          label: 'Your email address:',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
        },
        button: {
          label: 'sign up',
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
        buttonContainer: {
          classes: ['text-center'],
        },
      },
      modelKey: 'newsletter_1',
    });

    await render(
      hbs`<View::Newsletter @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.input-group')).to.exist;
    expect(
      this.element.querySelector(
        '.input-group .form-floating .newsletter-email',
      ),
    ).to.exist;
    expect(this.element.querySelector('.input-group .btn-newsletter-submit')).to
      .exist;
    expect(this.element.querySelector('.btn-newsletter-submit')).to.have.class(
      'btn-primary',
    );
  });

  it('can render the ungrouped form with floating labels', async function () {
    this.set('value', {
      data: {
        source: { model: 'newsletter', id: '1' },
        form: {
          floatingLabels: true,
          inputGroup: false,
          label: 'Your email address:',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
        },
        button: {
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
      },
      modelKey: 'newsletter_1',
    });

    await render(
      hbs`<View::Newsletter @value={{this.value}} @model={{this.model}} />`,
    );
    expect(this.element.querySelector('.input-group')).not.to.exist;
    expect(this.element.querySelector('.form-floating .newsletter-email')).to
      .exist;
    expect(this.element.querySelector('.btn-newsletter-submit')).to.exist;
    expect(this.element.querySelector('.btn-newsletter-submit')).to.have.class(
      'btn-primary',
    );
  });

  it('can render the grouped form without floating labels', async function () {
    this.set('value', {
      data: {
        source: { model: 'newsletter', id: '1' },
        form: {
          floatingLabels: false,
          inputGroup: true,
          label: 'Your email address:',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
        },
        button: {
          label: 'sign up',
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
        buttonContainer: {
          classes: ['text-center'],
        },
      },
      modelKey: 'newsletter_1',
    });

    await render(
      hbs`<View::Newsletter @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.input-group')).to.exist;
    expect(this.element.querySelector('.form-floating')).not.to.exist;
    expect(this.element.querySelector('.input-group .newsletter-email')).to
      .exist;
    expect(this.element.querySelector('.input-group .btn-newsletter-submit')).to
      .exist;
    expect(this.element.querySelector('.btn-newsletter-submit')).to.have.class(
      'btn-primary',
    );
  });

  it('can render the ungrouped form without floating labels', async function () {
    this.set('value', {
      data: {
        source: { model: 'newsletter', id: '1' },
        form: {
          floatingLabels: false,
          inputGroup: false,
          label: 'Your email address:',
        },
        labelStyle: {
          color: 'green',
          classes: ['fw-light'],
        },
        button: {
          label: 'sign up',
          classes: ['btn-primary', 'size-btn-lg', 'w-50'],
        },
        buttonContainer: {
          classes: ['text-center'],
        },
      },
      modelKey: 'newsletter_1',
    });

    await render(
      hbs`<View::Newsletter @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.input-group')).not.to.exist;
    expect(this.element.querySelector('.form-floating')).not.to.exist;
    expect(this.element.querySelector('.newsletter-email')).to.exist;
    expect(this.element.querySelector('.btn-newsletter-submit')).to.exist;
    expect(this.element.querySelector('.btn-newsletter-submit')).to.have.class(
      'btn-primary',
    );
  });

  it('can submit the ungrouped form', async function () {
    this.set('value', {
      data: {
        source: { model: 'newsletter', id: '1' },
        form: {
          floatingLabels: false,
          inputGroup: false,
          label: 'Your email address:',
        },
      },
      modelKey: 'newsletter_1',
    });

    await render(
      hbs`<View::Newsletter @value={{this.value}} @model={{this.model}} />`,
    );

    await fillIn('.newsletter-email', 'test@giscollective.com');
    await click('.btn-newsletter-submit');

    await waitUntil((a) => receivedEmail);

    expect(receivedEmail).to.deep.equal({ email: 'test@giscollective.com' });
  });

  it('can submit the grouped form', async function () {
    this.set('value', {
      data: {
        source: { model: 'newsletter', id: '1' },
        form: {
          floatingLabels: false,
          inputGroup: true,
          label: 'Your email address:',
          labelSuccess: 'You are now subscribed',
        },
      },
      modelKey: 'newsletter_1',
    });

    await render(
      hbs`<View::Newsletter @value={{this.value}} @model={{this.model}} />`,
    );

    await fillIn('.newsletter-email', 'test@giscollective.com');
    await click('.btn-newsletter-submit');

    await waitUntil(() => receivedEmail);

    expect(receivedEmail).to.deep.equal({ email: 'test@giscollective.com' });

    expect(this.element.querySelector('.newsletter-email')).not.to.exist;
    expect(
      this.element.querySelector('.thank-you-message').textContent.trim(),
    ).to.equal('You are now subscribed');
  });
});
