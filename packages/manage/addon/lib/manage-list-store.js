import { tracked } from '@glimmer/tracking';

export class ManageListStore {
  @tracked viewMode = 'list';
}
