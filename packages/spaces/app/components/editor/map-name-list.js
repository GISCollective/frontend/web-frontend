/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { service } from "@ember/service";
import { action } from "@ember/object";
import { A } from "models/lib/array";

export default class ManageEditorsMapNameListComponent extends Component {
  @service store;
  _value = null;
  @tracked list;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  async loadMaps() {
    const result = await this.store.query("map", { edit: true });
    this.list = A(result.filter((a) => this.args.value.map((b) => b.id).indexOf(a.id) == -1));

    for (let map of this.args.value) {
      this.list.push(map);
    }
  }

  @action
  change(isEnabled, value) {
    this._value = value;
    return this.args.onChange?.(value);
  }
}
