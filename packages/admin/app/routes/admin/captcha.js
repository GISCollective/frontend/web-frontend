/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/authenticated-admin-route';
import { hash } from 'rsvp';

export default class AdminCaptchaRoute extends Route {
  model() {
    return hash({
      enabled: this.preferences.requestPreference('captcha.enabled'),
      recaptchaSiteKey: this.preferences.requestPreference('secret.recaptcha.siteKey'),
      recaptchaSecretKey: this.preferences.requestPreference('secret.recaptcha.secretKey'),
      mtcaptchaSiteKey: this.preferences.requestPreference('secret.mtcaptcha.siteKey'),
      mtcaptchaPrivateKey: this.preferences.requestPreference('secret.mtcaptcha.privateKey'),
    });
  }
}
