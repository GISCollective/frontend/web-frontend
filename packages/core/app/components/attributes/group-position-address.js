/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import GeocodingSearch from '../../lib/geocoding-search';
import { tracked } from '@glimmer/tracking';
import { GeoJson } from '../../lib/geoJson';
import { guidFor } from '@ember/object/internals';

export default class AttributesGroupPositionAddressComponent extends Component {
  elementId = `address-${guidFor(this)}`;

  @service intl;
  @service store;

  @tracked _searchTerm = null;
  @tracked _address = null;
  @tracked geometry = null;
  @tracked geocodingSearch;
  @tracked isSearching;
  @tracked hasNoResults;

  get searchTerm() {
    if (this._searchTerm !== null) {
      return this._searchTerm;
    }

    return this.args.details?.searchTerm ?? '';
  }

  set searchTerm(value) {
    this._searchTerm = value;
  }

  get address() {
    if (this._address !== null) {
      return this._address;
    }

    return this.args.details?.address ?? '';
  }

  get group() {
    return {
      key: 'address',
      name: this.intl.t('address'),
    };
  }

  async triggerChange() {
    const details = this.args.details ?? {};
    details.searchTerm = this.searchTerm;
    details.address = this.address;
    details.type = 'address';

    const position = this.geometry?.center ?? null;

    await this.args.onChange?.(position, details);
  }

  @action
  showSearchResults() {
    return this.changeSearchTerm('', this.searchTerm);
  }

  @action
  async searchGeocoding(term) {
    this.hasNoResults = false;

    if (!term) {
      return this.geocodingSearch.clear();
    }

    return await this.geocodingSearch.search(term);
  }

  @action
  async keyPress(event) {
    if (event.key == 'Enter' || event.keyCode == 13) {
      this.isSearching = true;
      await this.changeSearchTerm('', this.searchTerm);
      this.isSearching = false;
    }
  }

  @action
  async changeSearchTerm(key, value) {
    this._searchTerm = value;

    await this.searchGeocoding(value);

    if (this.geocodingSearch?.results?.length) {
      const result = this.geocodingSearch.results[0];
      this._address = result.name;
      this.geometry = new GeoJson(result['geometry']);
    }

    if (!this.geocodingSearch?.results?.length) {
      this.hasNoResults = true;
    }

    return this.triggerChange();
  }

  @action
  changeAddress(key, value) {
    this._address = value.name;
    this.geometry = new GeoJson(value['geometry']);

    return this.triggerChange();
  }

  @action
  async setup() {
    this._searchTerm = null;
    this._address = null;
    this.geocodingSearch = new GeocodingSearch(this.store);

    if (this.args.details?.searchTerm) {
      await this.geocodingSearch.search(this.args.details.searchTerm);
    }
  }
}
