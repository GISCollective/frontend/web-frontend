/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "./base-options";
import { action } from "@ember/object";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { setDefaultBsProperty, setSizedBsProperty } from "core/lib/bs-cols";
import { LayoutContainer } from "models/transforms/layout-container-list";
export default class InputOptionsContainerOptionsComponent extends Component {
  @tracked _visibility;
  @tracked _options;
  @tracked _layers;
  @tracked _anchor;
  @tracked _height;

  availableWidths = ["default", "fluid"];
  availableHeights = ["default", "fill"];
  availableVisibility = ["always", "user:authenticated", "user:unauthenticated"];

  elementId = `input-container-options-section-${guidFor(this)}`;

  layersConfig = [
    {
      name: "count",
      type: "number",
    },
    {
      name: "showContentAfter",
      type: "number",
    },
    {
      name: "effect",
      options: ["none", "scroll-parallax"],
    },
  ];

  get anchor() {
    if (typeof this._anchor == "string") {
      return this._anchor;
    }

    return this.args.container?.anchor;
  }

  set anchor(value) {
    this._anchor = value;

    this.triggerChange();
  }

  get options() {
    return this._options || this.args.container?.options || [];
  }

  get layers() {
    const currentValue = this._layers || this.args.container?.layers || {};

    const effect = currentValue.effect;
    const count = isNaN(currentValue.count) || currentValue.count < 1 ? 1 : currentValue.count;
    const showContentAfter =
      isNaN(currentValue.showContentAfter) || currentValue.showContentAfter > count
        ? count
        : currentValue.showContentAfter;

    return { count, showContentAfter, effect };
  }

  @action
  onChangeOptions(options) {
    this._options = options;

    return this.triggerChange();
  }

  get width() {
    return this.getProperty("container", this.options);
  }

  get height() {
    if (this._height) {
      return this._height;
    }

    return this.args.container?.height ?? "default";
  }

  get visibility() {
    if (this._visibility) {
      return this._visibility;
    }

    return this.args.container?.visibility ?? this.availableVisibility[0];
  }

  @action
  visibilityChange(value) {
    this._visibility = value;

    return this.triggerChange();
  }

  triggerChange() {
    const newValue = {
      gid: this.args.container?.gid,
      rows: this.args.container?.rows,
      options: this.options,
      visibility: this.visibility,
      layers: this.layers,
      anchor: this.anchor,
      height: this.height,
    };

    return this.args.onChange?.(new LayoutContainer(newValue));
  }

  @action
  widthChange(value) {
    this._options = this.args.size
      ? setSizedBsProperty("container", this.args.size, value, this.options)
      : setDefaultBsProperty("container", value, this.options);

    return this.triggerChange();
  }

  @action
  heightChange(value) {
    this._height = value;

    return this.triggerChange();
  }

  @action
  changeLayers(value) {
    this._layers = value;
    this.triggerChange();
  }
}
