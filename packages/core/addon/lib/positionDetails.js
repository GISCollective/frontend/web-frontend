/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';

export default class PositionDetails {
  @tracked capturedUsingGPS = false;
  @tracked type = null;
  @tracked altitude = 0;
  @tracked accuracy = 1000;
  @tracked altitudeAccuracy = 1000;
  @tracked searchTerm = null;
  @tracked address = null;
  @tracked id = null;

  constructor(deserialized) {
    if (!deserialized || typeof deserialized != 'object') {
      deserialized = {};
    }

    if (deserialized.capturedUsingGPS) {
      this.capturedUsingGPS = deserialized.capturedUsingGPS;
    }

    if (deserialized.type) {
      this.type = deserialized.type;
    }

    if (deserialized.id) {
      this.id = deserialized.id;
    }

    if (deserialized.searchTerm) {
      this.searchTerm = deserialized.searchTerm;
    }

    if (deserialized.address) {
      this.address = deserialized.address;
    }

    if (deserialized.altitude) {
      this.altitude = deserialized.altitude;
    }

    if (deserialized.accuracy) {
      this.accuracy = deserialized.accuracy;
    }

    if (deserialized.altitudeAccuracy) {
      this.altitudeAccuracy = deserialized.altitudeAccuracy;
    }

    if (deserialized['altitude accuracy']) {
      this.altitudeAccuracy = deserialized['altitude accuracy'];
    }
  }

  toJSON() {
    const result = {
      altitude: this.altitude,
      accuracy: this.accuracy,
      altitudeAccuracy: this.altitudeAccuracy,
    };

    if (this.type) {
      result.type = this.type;
    } else {
      result.capturedUsingGPS = this.capturedUsingGPS ?? false;
    }

    if (this.type) {
      result.type = this.type;
    }

    if (this.searchTerm) {
      result.searchTerm = this.searchTerm;
    }

    if (this.address) {
      result.address = this.address;
    }

    if (this.id) {
      result.id = this.id;
    }

    return result;
  }
}

export class PositionDetailsGps extends PositionDetails {
  constructor(positionService) {
    super({});

    this.positionService = positionService;
  }

  get capturedUsingGPS() {
    return true;
  }

  get type() {
    return 'gps';
  }

  get altitude() {
    return this.positionService.altitude;
  }

  get accuracy() {
    return this.positionService.accuracy;
  }

  get altitudeAccuracy() {
    return this.positionService.altitudeAccuracy;
  }

  get searchTerm() {
    return null;
  }

  get address() {
    return null;
  }

  get id() {
    return null;
  }
}
