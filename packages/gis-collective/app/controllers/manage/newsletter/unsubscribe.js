/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageNewsletterUnsubscribeController extends Controller {
  @tracked isUnsubscribed;

  @action
  async unsubscribe() {
    await this.model.newsletter.unsubscribe(this.model.email);

    this.isUnsubscribed = true;
  }
}
