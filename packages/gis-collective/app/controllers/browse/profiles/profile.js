/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class BrowseProfilesProfileController extends Controller {
  @service user;
  @service router;

  get isAdmin() {
    return this.user.isAdmin;
  }

  get hasJoinedTime() {
    return this.model.profile.joinedTime && !isNaN(this.model.profile.joinedTime.getTime());
  }

  get hasUsername() {
    return this.model.profile.userName && this.model.profile.userName != '';
  }

  get isCalendarVisible() {
    return this.model.profile.showCalendarContributions || this.user.isAdmin || this.user.id == this.model.profile.id;
  }

  get twitterHandle() {
    return this.model.profile.twitter.replace('@', '');
  }

  @action
  navigateToTeam(id) {
    return this.router.transitionTo('browse.teams.team', id);
  }
}
