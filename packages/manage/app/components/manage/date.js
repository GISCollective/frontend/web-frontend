/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputManageDateComponent extends Component {
  @tracked _value = null;

  get value() {
    if (!this._value && (!this.args.value || this.args.value?.getYear() < 1990)) {
      return new Date();
    }

    if (this._value === null) {
      return new Date(this.args.value?.getTime());
    }

    return this._value;
  }

  @action
  onChange(value) {
    this._value = value;
  }

  @action
  save() {
    return this.args.onSave?.(this.value);
  }

  @action
  cancel() {
    this._value = null;

    return this.args.onCancel?.();
  }
}
