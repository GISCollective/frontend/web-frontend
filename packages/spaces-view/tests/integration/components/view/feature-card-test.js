/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/feature-card', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let feature;

  hooks.before(function () {
    server = new TestServer();
    feature = server.testData.storage.addDefaultFeature('1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders a feature when the value is set', async function () {
    this.set('value', feature);

    await render(hbs`<View::FeatureCard @value={{this.value}} />`);

    expect(this.element.querySelector('.site-card')).to.exist;
    expect(
      this.element.querySelector('.card-title').textContent.trim(),
    ).to.equal('Nomadisch Grün - Local Urban Food');
    expect(this.element.querySelector('.card-title')).to.have.attribute(
      'data-route',
      'browse.sites.site',
    );
  });

  it('can use a custom route', async function () {
    this.set('value', feature);
    this.set('options', {
      route: 'browse.sites.other-site',
    });

    await render(
      hbs`<View::FeatureCard @value={{this.value}} @options={{this.options}} />`,
    );

    expect(this.element.querySelector('.card-title')).to.have.attribute(
      'data-route',
      'browse.sites.other-site',
    );
  });
});
