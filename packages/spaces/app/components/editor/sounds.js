/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "./base/modelList";
import { action } from "@ember/object";

export default class ManageEditorsSoundsComponent extends Component {
  @action
  createSound() {
    return this.store.createRecord("sound", {
      name: "",
      feature: this.args.options?.parent,
      sound: "",
    });
  }
}
