/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/newsletter", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let changedValue;
  let spaceData;

  hooks.before(function () {
    server = new TestServer();
    spaceData = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultTeam();

    server.testData.storage.addDefaultNewsletter("1");
    server.testData.storage.addDefaultNewsletter("2");
    server.testData.storage.addDefaultNewsletter("3");
    server.testData.storage.addDefaultNewsletter("4");
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    this.set("space", await this.store.findRecord("space", spaceData._id));
  });

  it("renders the selected value", async function () {
    this.set("value", {
      floatingLabels: true,
      labelSuccess: "You are now subscribed.",
    });

    await render(hbs`<Editor::Newsletter @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector(".floating-labels").value).to.equal("yes");
    expect(this.element.querySelector("input.label-success").value).to.equal("You are now subscribed.");
  });

  it("can change the form data", async function () {
    this.set("value", {
      floatingLabels: true,
      label: "Your email address:",
    });

    await render(hbs`<Editor::Newsletter @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}}/>`);

    this.element.querySelector(".floating-labels").value = "no";
    await triggerEvent("select.floating-labels", "change");

    expect(changedValue).to.deep.equal({
      floatingLabels: false,
      label: "Your email address:",
      inputGroup: false,
    });
  });
});
