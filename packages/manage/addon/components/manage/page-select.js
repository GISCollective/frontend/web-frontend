/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class InputManagePageSelectComponent extends Component {
  @tracked value = null;
  @service store;

  get list() {
    return ['', ...(this.args.list ?? [])];
  }

  @action
  async setup() {
    if (!this.args.value) {
      return;
    }

    if (typeof this.args.value == 'object') {
      this.value = this.args.value;
    }

    if (this.args.isString) {
      this.value = this.store.peekRecord('page', this.args.value);
    }

    if (this.args.isString && !this.value) {
      this.value = await this.store.findRecord('page', this.args.value);
    }
  }

  @action
  change(value) {
    this.value = value;
  }

  @action
  save() {
    if (this.args.isString) {
      return this.args.onSave(this.value?.id);
    }

    return this.args.onSave(this.value);
  }

  @action
  cancel() {
    this.setup();
    return this.args.onCancel();
  }
}
