/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';

export default class AddPageRoute extends AuthenticatedRoute {
  modelsToClear = ['page'];

  queryParams = {
    all: { refreshModel: true },
    spaceId: { refreshModel: true },
  };

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const spaceParams = {};

    if (params.all) {
      spaceParams.all = params.all;
    } else {
      spaceParams.edit = true;
    }

    let space;
    let spaces;

    if (params.spaceId) {
      space = await this.store.findRecord('space', params.spaceId);
      spaces = [space];
    } else {
      spaces = this.store.query('space', spaceParams);
    }

    return hash({
      page: this.store.createRecord('page', {
        name: '',
        description: '',
        info: new ModelInfo(),
        visibility: {
          isPublic: false,
          team: null,
        },
        space,
      }),
      spaces,
    });
  }
}
