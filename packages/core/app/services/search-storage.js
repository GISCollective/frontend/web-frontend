/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';

export default class SearchStorageService extends Service {
  @tracked history = A();
  @tracked term = '';

  add(term) {
    if (!term) {
      return;
    }

    this.history.push(term);
    this.history = [...new Set(this.history)];

    if (this.history.length > 5) {
      this.history = this.history.slice(1, 6);
    }
  }
}
