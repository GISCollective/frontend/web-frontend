/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class MapAdapter extends Adapter {
  subscribe(id, email) {
    const url = this.buildURL('newsletter', id) + '/subscribe';

    return this.ajax(url, 'POST', { data: { email } });
  }

  unsubscribe(id, email) {
    const url = this.buildURL('newsletter', id) + '/unsubscribe';

    return this.ajax(url, 'POST', { data: { email } });
  }

  importEmails(id, emails, replace) {
    const url = this.buildURL('newsletter', id) + '/import-emails';

    return this.ajax(url, 'POST', { data: { emails, replace } });
  }

  async downloadEmails(id) {
    const url = this.buildURL('newsletter', id) + '/export-emails';

    const response = await fetch(url, {
      method: 'GET',
      headers: new Headers({
        Authorization: `Bearer ${this.session.data.authenticated.access_token}`,
      }),
    });

    return response.blob();
  }
}
