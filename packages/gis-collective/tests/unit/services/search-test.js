/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe ('Unit | Service | search',  function (hooks) {
  setupTest(hooks);

  // TODO: Replace this with your real tests.
  it('exists', function () {
    let service = this.owner.lookup('service:search');
    expect(service).to.be.ok;
  });
});
