/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";

export default class ManageEditorsPropertiesFilterComponent extends Component {
  elementId = `input-filters-${guidFor(this)}`;
  options = [
    "icons",
    "location",
    "feature attribute",
    "map feature",
    "event repetition",
    "day of week",
    "date",
    "calendar attribute",
  ];
}
