/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsBackgroundColorComponent extends Component {
  @tracked _color = null;

  get color() {
    if (this._color !== null) {
      return this._color;
    }

    return this.args.value?.data?.background?.color;
  }

  @action
  changeBgColor(color) {
    this._color = color;

    this.args.onChange?.({
      background: {
        color,
      },
    });
  }
}
