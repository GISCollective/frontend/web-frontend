/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import LazyList from 'models/lib/lazy-list';

export default class ManageCampaignsAnswersRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const campaign = await this.store.findRecord('campaign', params.id);
    await campaign.loadRelations();

    const team = campaign.visibility.team;

    const answers = new LazyList('campaign-answer', { campaign: params.id }, this.store);
    await answers.loadNext();

    return hash({
      team,
      campaign,
      answers,
    });
  }
}
