/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { LayoutContainer } from 'models/transforms/layout-container-list';
import { LayoutRow, LayoutCol } from 'models/transforms/layout-row-list';

export function getDefaultPresentationContainer(useDefaultRowOptions = true, container = 0, row = 0) {
  const rowOptions = useDefaultRowOptions ? ['align-items-center'] : [];

  return [
    new LayoutContainer({
      options: ['page-container'],
      rows: [
        new LayoutRow({
          options: rowOptions,
          cols: [
            new LayoutCol({
              name: `${container}.${row}.0`,
              options: ['col-12', 'col-md-6', 'order-1', 'order-md-0'],
            }),
            new LayoutCol({
              name: `${container}.${row}.1`,
              options: ['col-12', 'col-md-6', 'order-0', 'order-md-0'],
            }),
          ],
        }),
      ],
    }),
  ];
}
