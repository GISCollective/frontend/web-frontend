import Component from '@glimmer/component';

export default class DragSortItemComponent extends Component {
  get draggingEnabled() {
    return this.args.draggingEnabled ?? true;
  }

  get listId() {
    return Math.random().toString(36).slice(2, 7);
  }
}
