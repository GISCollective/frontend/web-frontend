/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
module.exports = {
  useBabelInstrumenter: true,
  reporters: ['lcov', 'html', 'text-summary'],
};
