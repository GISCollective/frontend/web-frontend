/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageTeamsSubscriptionRoute extends AuthenticatedRoute {
  model(params) {
    return hash({
      team: this.store.findRecord('team', params.id),
    });
  }
}
