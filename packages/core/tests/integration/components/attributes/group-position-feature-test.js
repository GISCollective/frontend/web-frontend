/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/group-position-feature', function (hooks) {
  setupRenderingTest(hooks, { server: true });

  let featureData;

  hooks.beforeEach(function () {
    this.server.testData.storage.addDefaultPicture();
    this.server.testData.storage.addDefaultUser();
    featureData = this.server.testData.storage.addDefaultFeature();
  });

  it('renders a warning when there is no feature state', async function () {
    await render(hbs`<Attributes::GroupPositionFeature />`);

    expect(this.element.querySelector('.alert-no-feature-selected')).to.exist;
  });

  it('renders the feature name, description and cover when the state has a feature id', async function () {
    this.set('state', `fid@${featureData._id}`);

    await render(hbs`<Attributes::GroupPositionFeature @state={{this.state}} />`);

    expect(this.element.querySelector('.alert-no-feature-selected')).not.to.exist;

    expect(this.element.querySelector('.card-cover-title-description')).to.exist;
    expect(this.element.querySelector('.card-title').textContent.trim()).to.equal('Nomadisch Grün - Local Urban Food');
  });

  it('triggers the onChange event when there is no position details set', async function () {
    let position;
    let details;

    this.set('change', (p, d) => {
      position = p;
      details = d;
    });

    this.set('state', `fid@${featureData._id}`);

    await render(hbs`<Attributes::GroupPositionFeature @state={{this.state}} @onChange={{this.change}} />`);

    await waitUntil(() => details, { options: { timeout: 2000 } });

    expect(details.toJSON()).to.deep.equal({
      altitude: +0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
      type: 'feature',
      id: '5ca78e2160780601008f69e6',
    });
    expect(position.toJSON()).to.deep.equal(featureData.position);
  });

  it('does not trigger the onChange event when the position details are set', async function () {
    let position;
    let details;

    this.set('change', (p, d) => {
      position = p;
      details = d;
    });

    this.set('state', `fid@${featureData._id}`);
    this.set('details', { type: 'feature', id: featureData._id });
    this.set('position', featureData.position);

    await render(
      hbs`<Attributes::GroupPositionFeature @details={{this.details}} @state={{this.state}} @onChange={{this.change}} />`,
    );

    expect(position).not.to.exist;
    expect(details).not.to.exist;
  });

  it('triggers the onChange event when the position details id is different from the selection', async function () {
    let position;
    let details;

    this.set('change', (p, d) => {
      position = p;
      details = d;
    });

    this.set('state', `fid@${featureData._id}`);
    this.set('details', { type: 'feature', id: 'other' });
    this.set('position', featureData.position);

    await render(
      hbs`<Attributes::GroupPositionFeature @details={{this.details}} @state={{this.state}} @onChange={{this.change}} />`,
    );

    await waitUntil(() => details, { options: { timeout: 2000 } });

    expect(details.toJSON()).to.deep.equal({
      altitude: +0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
      type: 'feature',
      id: '5ca78e2160780601008f69e6',
    });
    expect(position.toJSON()).to.deep.equal(featureData.position);
  });
});
