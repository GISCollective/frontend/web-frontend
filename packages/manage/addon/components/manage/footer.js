import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageFooterComponent extends Component {
  @service space;
  @service theme;

  @tracked pagesMap = {};
  @tracked defaultPagesMap = {};
  @tracked defaultDomain = '';

  get faqHref() {
    if (this.pagesMap['resources--faq']) {
      return '/resources/faq';
    }

    if (this.defaultPagesMap['resources--faq']) {
      return `https://${this.defaultDomain}/resources/faq`;
    }

    return '';
  }

  get picture() {
    return this.theme.name == 'light' ? 'giscollective-black.svg' : 'giscollective-white.svg';
  }

  get supportHref() {
    if (this.pagesMap['services--support-training']) {
      return '/services/support-training';
    }

    if (this.defaultPagesMap['services--support-training']) {
      return `https://${this.defaultDomain}/services/support-training`;
    }

    return '';
  }

  get contactHref() {
    if (this.pagesMap['contact']) {
      return '/contact';
    }

    if (this.defaultPagesMap['contact']) {
      return `https://${this.defaultDomain}/contact`;
    }

    return '';
  }

  get reportIssueHref() {
    if (this.pagesMap['support--report-issue']) {
      return '/support/report-issue';
    }

    if (this.defaultPagesMap['support--report-issue']) {
      return `https://${this.defaultDomain}/support/report-issue`;
    }

    return '';
  }

  @action
  async setup() {
    this.pagesMap = (await this.space?.currentSpace?.getPagesMap?.()) ?? {};
    this.defaultPagesMap = (await this.space?.defaultSpace?.getPagesMap?.()) ?? {};
    this.defaultDomain = (await this.space?.defaultSpace?.domain) ?? '';
  }
}
