/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupTest } from "../../helpers";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Unit | Service | space", function (hooks) {
  let service;
  let server;
  let space;
  let page;

  setupTest(hooks);

  hooks.beforeEach(function () {
    server = new TestServer();
    service = this.owner.lookup("service:space");
    space = server.testData.storage.addDefaultSpace();
    page = server.testData.storage.addDefaultPage();
  });

  it("exists", function () {
    expect(service).to.be.ok;
    expect(service.currentSpace).not.to.exist;
  });

  it("loads the default space on setup", async function () {
    await service.setup();
    expect(service.currentSpace?.id).to.equal(space._id);
    expect(service.defaultSpace?.id).to.equal(space._id);
  });

  describe("get hasLegacyMenu", function () {
    it("returns true for a default space", function () {
      space.visibility.isDefault = true;

      service.currentSpace = space;

      expect(service.hasLegacyMenu).to.equal(true);
    });

    it("returns false for a non default space", function () {
      space.visibility.isDefault = false;

      service.currentSpace = space;

      expect(service.hasLegacyMenu).to.equal(false);
    });

    it("returns false for a default space when it has a landing page", async function () {
      space.visibility.isDefault = true;

      service.currentSpace = space;
      service.landingPage = page;

      expect(service.hasLegacyMenu).to.equal(false);
    });
  });

  describe("get landingPagePath", function () {
    it("returns the world map view for a default space", function () {
      space.visibility.isDefault = true;

      service.currentSpace = space;

      expect(service.landingPagePath).to.equal("/browse/maps/_/map-view");
    });

    it("returns the login page when the current space is not set", function () {
      service.currentSpace = null;

      expect(service.landingPagePath).to.equal("/login");
    });

    it("returns the landing page path when the space is not default and it is set", async function () {
      space.visibility.isDefault = false;

      service.currentSpace = space;
      service.landingPage = page;

      expect(service.landingPagePath).to.equal("/page/test");
    });

    it("returns the landing page path when the space is the default and and it has one", async function () {
      space.visibility.isDefault = true;

      service.currentSpace = space;
      service.landingPage = page;

      expect(service.landingPagePath).to.equal("/page/test");
    });
  });

  describe("getLinkFor", function () {
    it("returns the icon path when there is an icon page", async function () {
      await service.setup();

      service.currentSpace._getPagesMap = {
        "browse--icon--:icon-id": "000000000000000000000001",
      };

      expect(service.getLinkFor("Icon", "0002")).to.equal("/browse/icon/0002");
    });

    it("returns undefined when there is no match", function () {
      service.pagesMap = {};

      expect(service.getLinkFor("Icon", "0002")).not.to.exist;
    });
  });
});
