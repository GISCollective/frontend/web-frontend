'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function (defaults) {
  const app = new EmberApp(defaults, {
    'ember-cli-terser': {
      enabled: false,
    },
    minifyCSS: {
      enabled: false,
    },
    minifyJS: {
      options: {
        exclude: ['**/vendor.js'],
      },
    },
    fingerprint: {
      enabled: false,
    },
    sassOptions: {
      includePaths: [
        '../../node_modules/bootstrap/scss',
        '../map/app',
        '../core/app',
        '.',
      ],
    },
    babel: {
      "plugins": [["@babel/plugin-transform-nullish-coalescing-operator", { loose: true}]],
    }
  });

  return app.toTree();
};
