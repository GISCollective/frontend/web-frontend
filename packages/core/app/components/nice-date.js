/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class NiceDateComponent extends Component {
  get isSet() {
    if (!this.args.value?.toDateString) {
      return false;
    }

    if (this.args.value.getFullYear() <= 1) {
      return false;
    }

    return true;
  }

  get text() {
    if (!this.isSet) {
      return '';
    }

    return this.args.value.toDateString();
  }
}
