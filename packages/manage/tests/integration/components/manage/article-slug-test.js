/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/article-slug', function (hooks) {
  setupRenderingTest(hooks);

  it('renders an info message when there is no value in view mode', async function (a) {
    await render(hbs`<Manage::ArticleSlug @title="slug" />`);

    expect(this.element.querySelector('.alert.alert-info')).to.have.textContent(
      'This article has no slug. The id will be used to identify this article.',
    );
  });

  it('renders a value when is set', async function (a) {
    await render(hbs`<Manage::ArticleSlug @title="slug" @value="some-slug" />`);

    expect(this.element.querySelector('.alert.alert-info')).not.to.exist;
    expect(this.element.querySelector('.view-slug')).to.have.textContent('some-slug');
  });

  it('can edit the value in edit mode', async function (a) {
    let value;
    this.set('save', (v) => {
      value = v;
    });

    this.set('edit', () => {});

    await render(
      hbs`<Manage::ArticleSlug @title="slug" @value="some-slug" @editablePanel="slug" @onSave={{this.save}} @onEdit={{this.edit}} />`,
    );

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');

    await fillIn('.input-article-slug', 'new-value');
    await click('.btn-submit');

    expect(this.element.querySelector('.view-slug')).not.to.exist;
    expect(value).to.equal('new-value');
  });

  it('can edit an empty value', async function (a) {
    let value;
    this.set('save', (v) => {
      value = v;
    });

    this.set('edit', () => {});

    await render(
      hbs`<Manage::ArticleSlug @title="slug" @value="" @editablePanel="slug" @onSave={{this.save}} @onEdit={{this.edit}} />`,
    );

    await fillIn('.input-article-slug', 'new-value');
    await click('.btn-submit');

    expect(this.element.querySelector('.alert')).not.to.exist;
    expect(this.element.querySelector('.view-slug')).not.to.exist;
    expect(value).to.equal('new-value');
  });

  it('renders a disabled save button on invalid slug', async function (a) {
    let value;
    this.set('save', (v) => {
      value = v;
    });

    this.set('edit', () => {});

    await render(
      hbs`<Manage::ArticleSlug @title="slug" @value="some-slug" @editablePanel="slug" @onSave={{this.save}} @onEdit={{this.edit}} />`,
    );

    await fillIn('.input-article-slug', 'new value');
    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');
  });
});
