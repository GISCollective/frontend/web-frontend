/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from "@glimmer/tracking";
import Component from "../base/modelItem";
import { action } from "@ember/object";

export default class ManageEditorsMapFeatureComponent extends Component {
  @tracked _borderClasses;
  @tracked _elementsFullscreen;
  @tracked _elementsZoom;
  @tracked _elementsDownload;
  @tracked _elementsMapLicense;
  @tracked _elementsBaseMapLicense;

  get elementsFullscreen() {
    if (this._elementsFullscreen) {
      return this._elementsFullscreen;
    }

    return this.args.value?.data?.elements?.fullscreen ?? false;
  }

  set elementsFullscreen(value) {
    this._elementsFullscreen = value;
    this.triggerChange();
  }

  get elementsZoom() {
    if (this._elementsZoom) {
      return this._elementsZoom;
    }

    return this.args.value?.data?.elements?.zoom ?? false;
  }

  set elementsZoom(value) {
    this._elementsZoom = value;
    this.triggerChange();
  }

  get elementsDownload() {
    if (this._elementsDownload) {
      return this._elementsDownload;
    }

    return this.args.value?.data?.elements?.download ?? false;
  }

  set elementsDownload(value) {
    this._elementsDownload = value;
    this.triggerChange();
  }

  get elementsMapLicense() {
    if (this._elementsMapLicense) {
      return this._elementsMapLicense;
    }

    return this.args.value?.data?.elements?.mapLicense ?? false;
  }

  set elementsMapLicense(value) {
    this._elementsMapLicense = value;
    this.triggerChange();
  }

  get elementsBaseMapLicense() {
    if (this._elementsBaseMapLicense) {
      return this._elementsBaseMapLicense;
    }

    return this.args.value?.data?.elements?.baseMapLicense ?? false;
  }

  set elementsBaseMapLicense(value) {
    this._elementsBaseMapLicense = value;
    this.triggerChange();
  }

  get borderClasses() {
    if (this._borderClasses) {
      return this._borderClasses;
    }

    return this.args.value?.data?.border?.classes ?? [];
  }

  @action
  borderClassesChanged(value) {
    this._borderClasses = value;

    return this.triggerChange();
  }

  triggerChange() {
    this.args.onChange?.({
      source: this.source,
      border: { classes: this.borderClasses },
      elements: {
        fullscreen: this.elementsFullscreen,
        zoom: this.elementsZoom,
        download: this.elementsDownload,
        mapLicense: this.elementsMapLicense,
        baseMapLicense: this.elementsBaseMapLicense,
      },
    });
  }
}
