/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, click, waitUntil, currentURL } from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';

describe ('Acceptance | admin/access', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedPreference;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultSpace();

    server.put('/mock-server/preferences/:id', (request) => {
      receivedPreference = JSON.parse(request.requestBody);
      receivedPreference.preference._id = receivedPreference.preference.name;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPreference),
      ];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login when the user is not authenticated', async function () {
    await visit('/admin/access');
    expect(currentURL()).to.equal('/login');
  });

  describe ('an authenticated user', function (hooks) {
    hooks.beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser(false);
    });

    it('should be redirect to index', async function () {
      await visit('/admin/access');

      expect(currentURL()).to.equal('/');
    });
  });

  describe ('an authenticated admin', function (hooks) {
    hooks.beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser(true);
    });

    it('is able to disable the Manage section', async function () {
      authenticateSession();

      await visit('/admin/access');

      await click('.allow-manage-without-teams');
      await click('.btn-submit');

      await waitUntil(() => receivedPreference);

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'access.allowManageWithoutTeams', _id: 'access.allowManageWithoutTeams', value: 'false' },
      });
    });

    it('is able to disable the multi project mode', async function () {
      authenticateSession();

      await visit('/admin/access');

      await click('.is-multi-project-mode');
      await click('.btn-submit');

      await waitUntil(() => receivedPreference);

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'access.isMultiProjectMode', _id: 'access.isMultiProjectMode', value: 'false' },
      });
    });

    it('renders a disable submit button by default', async function () {
      authenticateSession();

      await visit('/admin/access');
      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
    });
  });
});
