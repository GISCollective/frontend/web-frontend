/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ModelActionsDataBindingComponent extends Component {
  @action
  onDelete() {
    return this.args.onDelete?.(this.args.record);
  }

  @action
  handleTrigger() {
    this.args.record?.handleTrigger?.();
  }

  @action
  handleAnalyze() {
    this.args.record?.handleAnalyze?.();
  }
}
