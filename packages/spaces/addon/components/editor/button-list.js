/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { A } from "models/lib/array";
import { tracked } from "@glimmer/tracking";
import { later } from "@ember/runloop";

export default class EditorButtonListComponent extends Component {
  @tracked _buttons;

  get buttons() {
    if (this._buttons) {
      return this._buttons;
    }

    return A((this.args.value ?? []).map((a) => new ButtonOptions(a)));
  }

  triggerChange(buttons) {
    later(() => {
      this._buttons = buttons;
    });
    this.args.onChange?.(buttons.map((a) => a.toJSON()));
  }

  @action
  buttonOptionsChanged(index, value) {
    const buttons = this.buttons;
    buttons[index]._classes = value;

    this.triggerChange(buttons);
  }

  @action
  buttonChanged(index, value) {
    const buttons = this.buttons;
    const classes = buttons[index]?.classes ?? [];
    buttons[index] = new ButtonOptions({ ...value, classes });

    later(() => {
      this.triggerChange(buttons);
    });
  }

  @action
  deleteButton(index) {
    const buttons = this.buttons;
    buttons.splice(index, 1);

    this.args.triggerChange(buttons);
  }

  @action
  addButton() {
    const buttons = this.buttons;

    buttons.push(
      new ButtonOptions({ name: "", link: "", classes: ["btn-primary"] }, () => {
        this.triggerChange(buttons);
      }),
    );

    this.triggerChange(buttons);
  }
}

class ButtonOptions {
  @tracked _name;
  @tracked _link;
  @tracked _classes;
  @tracked _newTab;

  constructor(button, change) {
    this.change = change;
    this._name = button.name;
    this._link = button.link;
    this._type = button.type ?? "default";
    this._storeType = button.storeType ?? "apple";
    this._classes = button.classes;
    this._newTab = button.newTab;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
    this.change();
  }

  get newTab() {
    return this._newTab ?? false;
  }

  set newTab(value) {
    this._newTab = value;
    this.change();
  }

  get link() {
    return this._link;
  }

  set link(value) {
    this._link = value;
    this.change();
  }

  get type() {
    return this._type;
  }

  set type(value) {
    this._type = value;
    this.change();
  }

  get storeType() {
    return this._storeType;
  }

  set storeType(value) {
    this._storeType = value;
    this.change();
  }

  get classes() {
    return this._classes;
  }

  set classes(value) {
    this._classes = value;
    this.change();
  }

  toJSON() {
    return {
      name: this.name,
      link: this.link,
      newTab: this.newTab,
      type: this.type,
      storeType: this.storeType,
      classes: this._classes,
    };
  }
}
