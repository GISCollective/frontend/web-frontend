/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | manage/container-background-options',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the current value', async function () {
    this.set('value', {
      type: 'background/color',
      data: {
        background: {
          color: 'primary',
        },
      },
    });
    await render(
      hbs`<Manage::ContainerBackgroundOptions @value={{this.value}} />`
    );

    expect(
      this.element.querySelector('.selected-color').textContent.trim()
    ).to.equal('primary');
  });
});
