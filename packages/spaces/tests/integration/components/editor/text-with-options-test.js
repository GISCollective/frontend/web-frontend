/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, click, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/text-with-options", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the values", async function () {
    this.set("value", {
      classes: ["fw-bold", "text-center", "text-line-clamp-3", "text-size-12", "lh-sm"],
      color: "green",
    });

    await render(hbs`<Editor::TextWithOptions @value={{this.value}} />`);

    expect(this.element.querySelector(".input-sm-text-line-clamp").value).to.equal("3");
    expect(this.element.querySelector(".input-sm-lh").value).to.equal("sm");
    expect(this.element.querySelector(".input-sm-text-size").value).to.equal("12");
    expect(this.element.querySelector(".input-sm-fw").value).to.equal("bold");
    expect(this.element.querySelector(".input-sm-text").value).to.equal("center");
    expect(this.element.querySelector(".bs-color-picker .selected-value")).to.have.class("bg-green");
  });

  it("renders a heading level editor when the editor is a heading", async function (a) {
    this.set("value", {
      classes: ["fw-bold", "text-center", "text-line-clamp-3", "text-size-12", "lh-sm"],
      color: "green",
      heading: 3,
    });
    this.set("heading_element", null);

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<h3></h3><Editor::TextWithOptions @element={{this.heading_element}} @value={{this.value}} @onChange={{this.onChange}}/>`,
    );

    const heading = this.element.querySelector("h3");

    this.set("heading_element", heading);

    expect(this.element.querySelector(".input-heading").value).to.equal("3");

    this.element.querySelector(".input-heading").value = "2";
    await triggerEvent(".input-heading", "change");

    expect(value).to.deep.equal({
      color: "green",
      classes: ["fw-bold", "text-center", "text-line-clamp-3", "text-size-12", "lh-sm"],
      minLines: { sm: +0, md: +0, lg: +0 },
      heading: 2,
    });
  });

  it("can change the values", async function () {
    this.set("value", {
      classes: ["fw-bold", "text-center"],
      color: "green",
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::TextWithOptions @value={{this.value}} @onChange={{this.onChange}}/>`);

    this.element.querySelector(".input-sm-fw").value = "light";
    await triggerEvent(".input-sm-fw", "change");

    this.element.querySelector(".input-sm-text").value = "end";
    await triggerEvent(".input-sm-text", "change");

    this.element.querySelector(".input-sm-lh").value = "lg";
    await triggerEvent(".input-sm-lh", "change");

    this.element.querySelector(".input-sm-text-line-clamp").value = "4";
    await triggerEvent(".input-sm-text-line-clamp", "change");

    this.element.querySelector(".input-sm-text-size").value = "11";
    await triggerEvent(".input-sm-text-size", "change");

    await click(".bs-color-picker");
    await click(".btn-color-warning");

    expect(value).to.deep.equal({
      classes: ["fw-light", "text-end", "lh-lg", "text-line-clamp-4", "text-size-11"],
      color: "warning",
      minLines: { lg: 0, md: 0, sm: 0 },
    });
  });

  it("can change the minimum number of default rows", async function () {
    this.set("value", {
      classes: ["fw-bold", "text-center"],
      color: "green",
      text: "some text",
      minLines: {
        sm: 1,
        md: 2,
        lg: 3,
      },
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::TextWithOptions @enableTextValue={{true}} @value={{this.value}} @onChange={{this.onChange}}/>`,
    );

    expect(this.element.querySelector(".min-lines-sm").value).to.equal("1");
    expect(this.element.querySelector(".min-lines-md").value).to.equal("2");
    expect(this.element.querySelector(".min-lines-lg").value).to.equal("3");

    await fillIn(".min-lines-sm", "11");
    await fillIn(".min-lines-md", "22");
    await fillIn(".min-lines-lg", "33");

    this.set("value", {
      classes: ["fw-bold", "text-center"],
      color: "green",
      text: "some text",
      minLines: {
        sm: 11,
        md: 22,
        lg: 33,
      },
    });
  });
});
