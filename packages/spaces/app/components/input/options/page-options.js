/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class InputOptionsPageOptionsComponent extends Component {
  elementId = `layout-options-${guidFor(this)}`;

  @tracked _modeId = null;
  @tracked pageRevisions = null;

  get modeId() {
    if (this._modeId !== null) {
      return this._modeId;
    }

    return this.args.modelId;
  }

  set modeId(value) {
    this._modeId = value;
    this.args.onModelIdChange?.(value);
  }

  get hasSlugWithVariable() {
    return this.args.page?.slug?.includes?.(":");
  }

  @action
  changeSlug(value) {
    this.args.page.slug = value;
  }

  @action
  changeCategories(value) {
    this.args.page.categories = value;
  }

  @action
  saveIsPublic(value) {
    this.args.page.visibility.isPublic = value;
  }

  @action
  coverChanged(_, value) {
    this.args.page.cover = value;
  }

  @action
  async setup() {
    const result = await this.args.page?.getRevisions?.();

    if (result?.revisions) {
      this.pageRevisions = result.revisions;
    }
  }
}
