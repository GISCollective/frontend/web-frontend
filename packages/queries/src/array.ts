export function addObject<T>(list: T[], item: T) {
  if ("addObject" in list && typeof list.addObject == "function") {
    return list.push(item);
  }

  list.push(item);

  return list;
}

export function addObjects<T>(list: T[], items: T[]) {
  if ("addObjects" in list && typeof list.addObjects == "function") {
    return list.addObjects(items);
  }

  for (const item of items) {
    list.push(item);
  }

  return list;
}

export function toArray<T>(list: T[]) {
  if ("slice" in list && typeof list.slice == "function") {
    return list.slice();
  }

  if ("toArray" in list && typeof list.toArray == "function") {
    return list.toArray();
  }

  return [...list];
}

export function lastObject<T>(list: T[]) {
  if (!list) {
    return;
  }

  if ("lastObject" in list && typeof list.lastObject == "function") {
    return list.lastObject();
  }

  if (!list.length) {
    return;
  }

  return list[list.length - 1];
}
