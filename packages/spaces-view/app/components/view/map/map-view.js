/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import config from '../../../config/environment';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { FeatureSelection } from '../../../lib/map/feature-selection';
import { fromPageState, toPageState } from 'core/lib/page-state';
import { transform } from 'ol/proj';
import { cancel, later, debounce } from '@ember/runloop';
import Polygon from 'ol/geom/Polygon';
import { htmlSafe } from '@ember/template';

export default class PageColMapMapViewComponent extends Component {
  elementId = `map-view-${guidFor(this)}`;

  @service mapStyles;
  @service position;
  @service router;
  @service tracking;
  @service fastboot;
  @service store;
  @service preferences;
  @service search;
  @service fullscreen;
  @service session;

  @tracked _selectedBaseMap;
  @tracked _hasInteractions;
  @tracked hasFocus;
  @tracked isHover;
  @tracked hoveredFeature;
  @tracked pendingHoveredFeature;
  @tracked olMap;

  @tracked featureSelection;
  @tracked map;
  @tracked baseMapList;
  @tracked element;
  @tracked isPinned;
  @tracked minHeight;

  featureTilesUrl = config.siteTilesUrl;

  constructor() {
    super(...arguments);

    this.featureSelection = new FeatureSelection();
  }

  get mapViewLink() {
    return this.args.space.getLinkFor('MapView', this.map);
  }

  get style() {
    const result = this.minHeight
      ? `min-height: calc(${this.minHeight}px + 10%);`
      : ``;

    return htmlSafe(result);
  }

  get bearer() {
    let access_token = this.session?.get('data.authenticated.access_token');

    if (!access_token) {
      return null;
    }

    return `Bearer ${access_token}`;
  }

  get selectedFeature() {
    return this.state.fid;
  }

  get hasInteractions() {
    if (this.args.value?.data?.map?.enableMapInteraction == 'always') {
      return true;
    }

    return this._hasInteractions;
  }

  get worldExtent() {
    try {
      if (
        this.position.longitude &&
        this.position.latitude &&
        this.args.value?.data?.map?.showUserLocation
      ) {
        const radius = 0.005;
        const topRight = [
          this.position.longitude - radius,
          this.position.latitude - radius,
        ];
        const bottomLeft = [
          this.position.longitude + radius,
          this.position.latitude + radius,
        ];

        return topRight.concat(bottomLeft);
      }
      // eslint-disable-next-line no-empty
    } catch (err) {}

    return this.preferences.appearanceMapExtent;
  }

  get defaultExtent() {
    if (!this.map?.area?.coordinates?.length) {
      return this.worldExtent;
    }

    const polygon = new Polygon([this.map.area.coordinates[0]]);
    return polygon.getExtent();
  }

  get extent() {
    if (this.state.z && this.state.lon && this.state.lat) {
      return null;
    }

    if (this.mapId == '_') {
      return this.worldExtent;
    }

    return this.defaultExtent;
  }

  get constraintExtent() {
    if (!this.args.value?.data?.map?.restrictView) {
      return null;
    }

    if (this.mapId == '_') {
      return this.worldExtent;
    }

    return this.defaultExtent;
  }

  get mapId() {
    return this.map?.id || this.map?._id || '_';
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    return this.baseMapList?.[0] ?? null;
  }

  get baseMaps() {
    return this.map?.baseMaps?.list;
  }

  get state() {
    return fromPageState(this.args.state);
  }

  get isReady() {
    if (this.args.value?.data?.map?.mode == 'use a map' && !this.map) {
      return false;
    }

    return !this.fastboot.isFastBoot && this.tilesUrl;
  }

  get showZoomButtons() {
    return this.args.value?.data?.map?.showZoomButtons && this.hasInteractions;
  }

  get showFeaturePopUp() {
    const featureSelection = this.args?.value?.data?.map?.featureSelection;

    if (featureSelection != 'show popup') {
      return false;
    }

    return !!this.state.fid;
  }

  @action
  hoverFeature(feature) {
    const featureHover = this.args?.value?.data?.map?.featureHover;

    if (featureHover != 'mouse pointer') {
      return;
    }

    this.isHover = !!feature;

    if (feature) {
      const properties = feature.getProperties();
      if (properties['canView'] !== 'true' && properties['visibility'] != '1') {
        this.isHover = false;
        feature = null;
      }
    }

    if (!this.isContextVisible) {
      this.hoveredFeature = feature;
      this.pendingHoveredFeature = false;
    } else {
      this.pendingHoveredFeature = feature;
    }
  }

  @action
  async setupRecord(record) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    if (!record) {
      return;
    }
    this.map = record;

    try {
      this.baseMapList = await record.baseMaps?.fetch?.();
    } catch (err) {
      console.log(err);
    }
  }

  get geocoding() {
    if (!this.state['gec'] || this.state['gec'] == this.selectedGec) {
      return null;
    }

    return this.store.peekRecord('geocoding', this.state['gec']);
  }

  focusGeocoding() {
    const geocoding = this.geocoding;
    this.selectedGec = geocoding?.id;

    this.fit(geocoding);
  }

  @action
  updateState() {
    if (this.selectedFeature) {
      this.pin();
      this._hasInteractions = true;
    }

    if (this.state['gec']) {
      this.focusGeocoding();
    }
  }

  @action
  async setup(element) {
    this.element = element;
    this.createdAt = new Date();

    if (this.args?.value?.data?.map?.mode == 'show all features') {
      this.baseMapList = await this.store.query('base-map', { default: true });
    }

    later(() => {
      this.updateState();
    });

    this.element.selectFeature = (a) => this.selectFeature(a);
  }

  @action
  setupMap(map) {
    this.olMap = map;

    const view = map.getView();
    const projection = view.getProjection();

    if (this.state.z) {
      view.setZoom(parseInt(this.state.z));
    }

    if (this.state.lon && this.state.lat) {
      const center = transform(
        [parseFloat(this.state.lon), parseFloat(this.state.lat)],
        'EPSG:4326',
        projection,
      );
      view.setCenter(center);
    }

    this.tracking.addMapView(this.router.currentURL, this.mapId);

    later(() => {
      this.updateState();
    });
  }

  get stateUpdateTimeout() {
    return this.args.stateUpdateTimeout ?? 1000;
  }

  get timeoutHover() {
    return this.args.timeoutHover ?? 2000;
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.mapStyles.unwatchChange(this.elementId);
  }

  @action
  localize() {
    return this.position.watchPosition();
  }

  @action
  selectBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  async selectFeature(feature) {
    if (this.args.isEditor) {
      return;
    }

    this.featureSelection.update(feature);

    const featureId = this.featureSelection.featureId ?? '';

    if (featureId == this.state.fid) {
      return;
    }

    const newState = toPageState({
      ...this.state,
      fid: featureId,
    });

    const featureSelection = this.args?.value?.data?.map?.featureSelection;

    if (['open details in panel', 'show popup'].includes(featureSelection)) {
      later(() => {
        this.args.onChangeState?.(newState);
      }, 5);
    }

    if (featureSelection == 'go to page' && this.featureSelection.featureId) {
      let path = await this.args.space?.getPagePath(
        this.args?.value?.data?.map?.destination?.pageId,
      );

      path = path.replace(':map_id', this.mapId);

      this.router.transitionTo(`${path}?s=${newState}`);
    }
  }

  get hasAutoInteraction() {
    return this.args.value?.data?.map?.enableMapInteraction == 'automatic';
  }

  @action
  handleMouseOver() {
    if (this.args.isEditor) {
      return;
    }

    if (this.hasInteractions) {
      cancel(this.cancelTimer);
      return;
    }

    if (!this.hasAutoInteraction) {
      return;
    }

    this.hasFocus = true;
    this.hasInteractionsTimer = later(() => {
      this._hasInteractions = true;
    }, this.timeoutHover);
  }

  @action
  deselectFeature() {
    const newState = toPageState({
      ...this.state,
      fid: undefined,
    });

    later(() => {
      this.args.onChangeState?.(newState);
    }, 5);
  }

  @action
  handleMouseOut() {
    if (this.state.fid || this.featureSelection?.type) {
      return;
    }

    this.cancelTimer = later(() => {
      this._hasInteractions = false;
      this.hasFocus = false;
      cancel(this.hasInteractionsTimer);
    }, 1000);
  }

  @action
  viewChanged(view) {
    const now = new Date();

    if (now.getTime() - this.createdAt.getTime() < this.stateUpdateTimeout) {
      return;
    }

    debounce(this, this.updateViewState, view, 100);
  }

  updateViewState(view) {
    const currentState = this.state;
    const projection = view.getProjection();

    const center = transform(view.getCenter(), projection, 'EPSG:4326');
    const z = parseInt(view.getZoom());

    const state = toPageState({
      ...currentState,
      lon: center[0],
      lat: center[1],
      z,
    });

    this.args.onChangeState?.(state);
  }

  get tilesUrl() {
    if (this.args.value?.data?.map?.mode == 'use a map' && !this.map) {
      return null;
    }

    let url = `${this.featureTilesUrl}/{z}/{x}/{y}?format=vt`;

    if (this.map) {
      url = `${url}&map=${this.map.id}`;
    }

    if (this.state.at) {
      url += `&attributes=${this.state.at}`;
    }

    if (this.state.if) {
      url += `&icons=${this.state.if}`;
    }

    return url;
  }

  get transitionSteps() {
    if (!this.element) {
      return {};
    }

    const rect = this.element.parentElement.getBoundingClientRect();

    return {
      0: {
        position: 'relative',
      },
      1: {
        position: 'fixed',
        top: `${rect.y}px`,
        left: `${rect.x}px`,
        width: `${rect.width}px`,
        height: `${rect.height}px`,
      },
      100: {
        position: 'fixed',
        top: `0px`,
        left: `0px`,
        width: `100%`,
        height: `100%`,
      },
    };
  }

  @action
  onTransitionEnd() {
    cancel(this.centerTimer);

    later(() => {
      this.olMap?.updateSize();
    });
  }

  @action
  onTransitionStart() {
    this.watchMapCenter();
  }

  @action
  resizeContainer(height) {
    this.minHeight = height;
  }

  watchMapCenter() {
    this.olMap?.updateSize();

    this.centerTimer = later(() => {
      this.watchMapCenter();
    });
  }

  get hasPinning() {
    return this.args.value?.data?.map?.enablePinning;
  }

  pin() {
    if (this.lastPin == this.selectedFeature) {
      return;
    }

    this.lastPin = this.selectedFeature;
    const featureSelection = this.args?.value?.data?.map?.featureSelection;

    if (
      featureSelection == 'open details in panel' &&
      !this.isPinned &&
      this.hasPinning
    ) {
      this.isPinned = true;
      this.fullscreen.freezeBody();
    }
  }

  @action
  unpin() {
    this.isPinned = false;
    this.fullscreen.unfreezeBody();

    const newState = toPageState({
      ...this.state,
      fid: undefined,
    });

    later(() => {
      this.args.onChangeState?.(newState);
    }, 5);
  }

  @action
  openSearch() {
    this.search.isEnabled = true;
    this.search.map = this.mapId;
  }

  fit(feature) {
    if (!feature) {
      return;
    }

    const geometry = feature.position || feature.geometry;

    return this.fitOlFeature(geometry.toOlFeature('EPSG:3857'));
  }

  fitOlFeature(feature) {
    if (!this.olMap || !feature) {
      return;
    }

    const geometry = feature.getGeometry?.();
    const view = this.olMap.getView();

    view.cancelAnimations();

    if (geometry.getType?.() == 'Point') {
      let center = geometry.getFlatCoordinates?.();

      if (!center) {
        return;
      }

      return view.animate({
        center,
        zoom: 16,
        duration: this.mapViewAnimationDuration,
      });
    }

    view.fit(geometry.getExtent(), {
      duration: this.mapViewAnimationDuration,
    });
  }

  get loaderStyle() {
    return htmlSafe(`animation-duration: ${this.timeoutHover}ms;`);
  }
}
