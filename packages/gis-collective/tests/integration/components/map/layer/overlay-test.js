/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | map/layer/overlay',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    let overlay;
    this.set('map', {
      addOverlay(v) {
        overlay = v;
      },
    });

    this.set('position', [1, 2]);

    await render(hbs`
      <Map::Layer::Overlay @map={{this.map}} @positioning="bottom-right" @position={{this.position}}>
        template block text
      </Map::Layer::Overlay>
    `);

    expect(overlay).to.exist;
    expect(overlay.getElement().textContent.trim()).to.equal(
      'template block text'
    );
    expect(overlay.getPositioning()).to.equal('bottom-right');

    const position = overlay.getPosition().map((a) => Math.ceil(a));
    expect(`${position}`).to.equal('111320,222685');
  });
});
