/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';

export default class BaseMap extends Model {
  @attr('string') name;
  @attr attributions;
  @attr('string') icon;
  @attr('layer') layers;
  @attr('boolean') canEdit;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr('visibility') visibility;
  @attr('number') defaultOrder;

  get needsWebGl() {
    return this.layers.map((a) => a.type).includes('MapBox');
  }

  get title() {
    return this.name;
  }

  async getCover() {
    return await this.cover;
  }
}
