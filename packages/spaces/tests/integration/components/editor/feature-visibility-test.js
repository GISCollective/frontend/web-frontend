/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/feature-visibility", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the value", async function () {
    await render(hbs`<Editor::FeatureVisibility @value={{1}} />`);
    expect(this.element.querySelector(".form-select").value).to.equal("1");
  });

  it("can change the value", async function () {
    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::FeatureVisibility @value={{0}} @onChange={{this.change}} />`);

    const select = this.element.querySelector(".manage-editors-feature-visibility select");

    select.value = "-1";
    await triggerEvent(select, "change");

    expect(value).to.equal(-1);
  });
});
