/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class GradientTitleComponent extends Component {
  @service store;
  @tracked team;
  @tracked _meta;

  get teamLink() {
    if (!this.team?.id || !this.args.space) {
      return '';
    }

    return this.args.space.getLinkFor('Team', this.team.id);
  }

  get meta() {
    if (!this.args.record?.meta && !this._meta) {
      return [];
    }

    const meta = this.args.record?.meta || this._meta;

    return Object.keys(meta).map((key) => {
      return { title: key, value: this.args.record.meta[key] };
    });
  }

  @action
  async setupRecord() {
    if (!this.args.record) {
      return;
    }

    this._meta = await this.args.record.reloadMeta();

    if (this.args.record.visibility?.team?.name) {
      this.team = this.args.record.visibility.team;
      return;
    }

    try {
      if (this.args.record.visibility?.teamId) {
        this.team = await this.store.findRecord(
          'team',
          this.args.record.visibility?.teamId,
        );
      }
    } catch (err) {
      console.log('The team might be private', err);
    }
  }

  @action
  watchSize(element) {
    this.rootElement = element;

    this.onResize();
    this.spyResize();
  }

  spyResize() {
    try {
      this.sizeObserver?.disconnect?.();

      this.sizeObserver = new ResizeObserver(() => {
        window.requestAnimationFrame(() => {
          if (this.isDestroyed || this.isDestroying) {
            return;
          }

          this.onResize();
        });
      });
      this.sizeObserver.observe(this.rootElement);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  }

  onResize() {
    const rect = this.rootElement?.getBoundingClientRect?.();

    this.height = rect?.height;

    this.args.onResize?.(this.height);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.sizeObserver?.disconnect?.();
  }
}
