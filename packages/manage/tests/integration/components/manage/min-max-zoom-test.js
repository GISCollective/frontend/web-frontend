/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/min-max-zoom', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the values in the read mode', async function () {
    this.set('value', {
      minZoom: 1,
      maxZoom: 12,
    });

    await render(hbs`<Manage::MinMaxZoom @title="zoom" @value={{this.value}}/>`);
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'The icon is visible between 1 and 12 zoom levels.',
    );
  });

  it('can change the values in the edit mode', async function () {
    this.set('value', {
      minZoom: 1,
      maxZoom: 12,
    });

    let minZoom, maxZoom;
    this.set('save', (v1, v2) => {
      minZoom = v1;
      maxZoom = v2;
    });

    await render(
      hbs`<Manage::MinMaxZoom @onSave={{this.save}} @editablePanel="zoom" @title="zoom" @value={{this.value}}/>`,
    );

    expect(this.element.querySelector('.input-min-value').value).to.equal('1');
    expect(this.element.querySelector('.input-max-value').value).to.equal('12');

    await fillIn('.input-min-value', '2');
    await fillIn('.input-max-value', '15');

    await click('.btn-submit');

    expect(minZoom).to.equal(2);
    expect(maxZoom).to.equal(15);
  });

  it('resets the values when cancel is pressed', async function () {
    this.set('value', {
      minZoom: 1,
      maxZoom: 12,
    });

    await render(hbs`<Manage::MinMaxZoom @editablePanel="zoom" @title="zoom" @value={{this.value}}/>`);

    await fillIn('.input-min-value', '2');
    await fillIn('.input-max-value', '15');

    await click('.btn-cancel');

    expect(this.element.querySelector('.input-min-value').value).to.equal('1');
    expect(this.element.querySelector('.input-max-value').value).to.equal('12');
  });
});
