/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, waitUntil, click, fillIn, blur, triggerEvent, waitFor, typeIn } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';
import { selectSearch } from 'ember-power-select/test-support';

describe('Acceptance | manage/icons/edit', function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let receivedIcon;
  let iconSet;
  let icon;
  let isDeleted;

  hooks.beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultArticle('login');

    icon = server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIcon('5cc8dc1038e882010061545a');

    server.put(`/mock-server/icons/${icon._id}`, (request) => {
      server.testData.storage.addDefaultIconSet();

      receivedIcon = JSON.parse(request.requestBody);
      receivedIcon.icon._id = icon._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedIcon)];
    });

    isDeleted = false;
    server.delete(`/mock-server/icons/${icon._id}`, () => {
      isDeleted = true;

      return [204, {}, ''];
    });

    receivedIcon = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('for an icon of a private icon set', function (hooks) {
    hooks.beforeEach(function () {
      iconSet = server.testData.create.iconSet('5ca7b702ecd8490100cab96f');
      iconSet.visibility.isPublic = false;

      server.testData.storage.addIconSet(iconSet);
    });

    it('should show an alert', async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.alert-icon-set-private')).to.exist;
    });
  });

  describe('for an icon with a private parent icon', function (hooks) {
    hooks.beforeEach(function () {
      iconSet = server.testData.storage.addDefaultIconSet();

      const parentSet = server.testData.create.iconSet('000000000000000000000001');
      parentSet.visibility.isPublic = false;
      server.testData.storage.addIconSet(parentSet);

      const parentIcon = server.testData.create.icon('000000000000000000000001');
      parentIcon.name = 'parent icon';
      parentIcon.iconSet = '000000000000000000000001';
      parentIcon.image.value = 'some image';
      server.testData.storage.addIcon(parentIcon);

      icon.parent = '000000000000000000000001';
      server.testData.storage.addIcon(icon);
    });

    it('should show an alert', async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);
      expect(this.element.querySelector('.alert-parent-icon-private')).to.exist;
    });

    it('should be able to use the parent image', async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.btn-use-parent-icon');

      expect(receivedIcon.icon.image.useParent).to.equal(true);
      expect(this.element.querySelector('.attribute-name .icon-inherited')).not.to.exist;
    });

    describe('when the icon has an inherited attribute', function (hooks) {
      hooks.beforeEach(function () {
        iconSet = server.testData.storage.addDefaultIconSet();

        icon.attributes = [
          {
            help: '',
            isPrivate: true,
            name: 'provides food',
            displayName: 'provides food',
            type: 'boolean',
            options: '',
            isInherited: true,
          },
        ];

        server.testData.storage.addIcon(icon);
      });

      it('should show the private attribute with the inherit symbol', async function () {
        authenticateSession();
        await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
        await PageElements.waitEditorJs(this.element);

        expect(this.element.querySelector('.attribute-name .icon-inherited')).to.exist;
        expect(this.element.querySelector('.attribute-name .icon-not-published')).to.exist;
      });

      it('should show disabled input fields for the inherited attribute', async function () {
        authenticateSession();
        await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
        await PageElements.waitEditorJs(this.element);

        await click('.container-group-attributes .btn-edit');

        expect(this.element.querySelector('.container-group-attributes .input-attribute-name')).to.have.attribute(
          'disabled',
          '',
        );
        expect(this.element.querySelector('.container-group-attributes .btn-delete')).to.have.attribute('disabled', '');
      });
    });
  });

  describe('for an icon of a public icon set', function (hooks) {
    hooks.beforeEach(function () {
      iconSet = server.testData.storage.addDefaultIconSet();
      server.testData.storage.addDefaultIconSet('000000000000000000000001');
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);
      expect(currentURL()).to.equal(
        '/login?redirect=%2Fmanage%2Ficons%2Fedit%2F5ca7b702ecd8490100cab96f%2F5ca7bfc0ecd8490100cab980',
      );
    });

    it('can visit /manage/icons/edit', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.main-container > .error')).not.to.exist;
      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      expect(this.element.querySelector('.alert-icon-set-private')).not.to.exist;
    });

    it('can change `allow many instances` value', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);

      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      await click(this.element.querySelector('.side-bar .container-group-allow-many-instances .btn-switch'));

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.allowMany).to.equal(true);
    });

    it('can change the style values', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.container-group-styles .btn-edit');
      await click('.check-has-custom-style');

      let colorInput = this.element.querySelector('.col-site .icon-style-background-color');
      colorInput.value = '#cc0011';
      await triggerEvent(colorInput, 'change');
      await blur('.col-site .icon-style-background-color');

      colorInput = this.element.querySelector('.col-line .icon-style-background-color');
      colorInput.value = '#cc0022';
      await triggerEvent(colorInput, 'change');
      await blur('.col-line .icon-style-background-color');

      colorInput = this.element.querySelector('.col-polygon .icon-style-background-color');
      colorInput.value = '#cc0033';
      await triggerEvent(colorInput, 'change');
      await blur('.col-polygon .icon-style-background-color');

      await click('.container-group-styles .btn-submit');

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.styles.hasCustomStyle).to.equal(true);
      expect(receivedIcon.icon.styles.types.site.backgroundColor).to.equal('#cc0011');
      expect(receivedIcon.icon.styles.types.line.backgroundColor).to.equal('#cc0022');
      expect(receivedIcon.icon.styles.types.polygon.backgroundColor).to.equal('#cc0033');
    });

    it('can change the article', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);

      await click('.row-article .btn-edit');

      await waitFor('.editor-js.editor-is-ready');

      await click('.ce-header');
      await fillIn('.ce-header', '');
      await typeIn('.ce-header', ' --- new title');

      await wait(200);
      await click('.ce-paragraph');
      await fillIn('.ce-paragraph', '');
      await typeIn('.ce-paragraph', ' --- new paragraph');

      await wait(200);
      await click('.ce-header');

      await waitUntil(() => !this.element.querySelector('.btn-submit[disabled]'), { timeout: 3000 });

      await click('.btn-submit');
      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.name).to.equal('--- new title');
      expect(receivedIcon.icon.description).to.deep.equal({
        blocks: [
          { type: 'header', data: { text: ' --- new title', level: 1 } },
          { type: 'paragraph', data: { text: ' --- new paragraph' } },
        ],
      });
    });

    it('can change `icon set` value', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      await click('.title-icon-set .btn-edit');

      this.element.querySelector('.value-icon-set select').value = '000000000000000000000001';
      await triggerEvent('.value-icon-set select', 'change');

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.iconSet).to.equal('000000000000000000000001');
    });

    it('can change `category` value', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      await click(this.element.querySelector('.container-group-category .btn-edit'));
      await fillIn('.input-textbox-suggestions input', 'new category');

      await click(this.element.querySelector('.container-group-category .btn-submit'));

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.category).to.equal('new category');
    });

    it('can change `subcategory` value', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      await click(this.element.querySelector('.container-group-subcategory .btn-edit'));
      await fillIn('.input-textbox-suggestions input', 'new subcategory');

      await click(this.element.querySelector('.container-group-subcategory .btn-submit'));

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.subcategory).to.equal('new subcategory');
    });

    it('can change `minZoom` and `maxZoom` values', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.container-group-visibility .btn-edit');

      await fillIn('.input-min-value', '2');
      await fillIn('.input-max-value', '15');

      await click('.btn-submit');

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.minZoom).to.equal(2);
      expect(receivedIcon.icon.maxZoom).to.equal(15);
    });

    it('can change the `otherNames` values', async function () {
      authenticateSession();

      icon.otherNames = ['name1', 'name2'];

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.row-other-names .btn-edit');

      expect(this.element.querySelector('.row-other-names input').value).to.equal('name1, name2');

      await fillIn('.row-other-names input', 'name1, name2,,,,name3');

      await click('.btn-submit');

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.otherNames).to.deep.equal(['name1', 'name2', 'name3']);
    });

    it('can change the `keepWhenSmall` value', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      expect(this.element.querySelector('#toggle-keep-when-small')).to.have.attribute('checked', 'true');
      await click('#toggle-keep-when-small');

      await waitUntil(() => receivedIcon, { timeout: 3000 });

      expect(receivedIcon.icon.keepWhenSmall).to.equal(false);
    });

    it('can set a parent icon', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.openSideBar(this.element);

      await click(this.element.querySelector('.container-group-parent-icon .btn-edit'));
      await selectSearch('.container-group-parent-icon .ember-power-select-trigger', 'something');

      await waitUntil(() => server.history.indexOf('GET /mock-server/icons?term=something') != -1);
      expect(server.history).to.contain('GET /mock-server/icons?term=something');

      await click(this.element.querySelectorAll('.ember-power-select-option')[1]);
      await click(this.element.querySelector('.container-group-parent-icon .btn-submit'));

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.parent).to.equal('5cc8dc1038e882010061545a');
    });

    it('does not disable the category and subcategory edit for icons with parents', async function () {
      icon.parent = '5cc8dc1038e882010061545a';
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      expect(this.element.querySelector('.container-group-category .btn-edit')).to.exist;
      expect(this.element.querySelector('.container-group-subcategory .btn-edit')).to.exist;
    });

    it('allows removing the parent icon when it is set', async function () {
      icon.parent = '5cc8dc1038e882010061545a';
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);
      await click(this.element.querySelector('.container-group-parent-icon .btn-edit'));
      await click(this.element.querySelector('.container-group-parent-icon .btn-remove-parent'));

      await waitUntil(() => !this.element.querySelector('.container-group-parent-icon .btn-remove-parent'));

      expect(
        this.element.querySelector('.container-group-parent-icon .ember-power-select-trigger').textContent.trim(),
      ).to.equal('');

      await click(this.element.querySelector('.container-group-parent-icon .btn-submit'));

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(this.element.querySelector('.container-group-parent-icon .value').textContent.trim()).to.equal('not set');

      expect(receivedIcon.icon.parent).not.to.equal('5cc8dc1038e882010061545a');
    });

    it('renders the parent icon when it is set', async function () {
      icon.parent = '5cc8dc1038e882010061545a';
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      expect(this.element.querySelector('.container-group-parent-icon img')).to.have.attribute('src', icon.image.value);
      expect(this.element.querySelector('.container-group-parent-icon .value').textContent.trim()).to.equal(icon.name);
    });

    it('renders a message when the parent is not set', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await PageElements.openSideBar(this.element);

      expect(this.element.querySelector('.container-group-parent-icon .value').textContent.trim()).to.equal('not set');
    });

    it('can add a new options attribute', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.row-attributes .btn-edit');
      await click('.row-attributes .btn-add-attribute');

      await fillIn('.input-attribute-name', 'name');

      this.element.querySelector('.input-icon-attribute select').value = 'options';
      await triggerEvent('.input-icon-attribute select', 'change');

      await fillIn('.input-attribute-name', 'name');
      await fillIn('.input-attribute-options', 'option1,option2');
      await click('.btn-submit');

      await waitUntil(() => receivedIcon != null, { timeout: 3000 });

      expect(receivedIcon.icon.attributes).to.deep.equal([
        {
          name: 'name',
          displayName: '',
          help: '',
          options: 'option1,option2',
          type: 'options',
          isPrivate: false,
          from: {},
          isInherited: false,
          isRequired: false,
        },
      ]);
    });

    it('can update the order', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      await click('.row-order .btn-edit');
      await fillIn('.row-order input', '3');
      await click('.btn-submit');

      await waitUntil(() => receivedIcon);

      expect(receivedIcon.icon.order).to.equal(3);
    });
  });

  describe('for an icon with a private attribute', function (hooks) {
    hooks.beforeEach(function () {
      iconSet = server.testData.storage.addDefaultIconSet();
      icon = server.testData.create.icon('000000000000000000000001');
      icon.image.value = 'some image';

      icon.attributes = [
        {
          help: '',
          isPrivate: true,
          name: 'provides food',
          displayName: 'provides food',
          type: 'boolean',
          options: '',
        },
      ];

      server.testData.storage.addIcon(icon);
    });

    it('should show the private attribute', async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.attribute-name .icon-not-published')).to.exist;
    });
  });
});
