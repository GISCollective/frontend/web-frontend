/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';

export default class NotificationsService extends Service {
  lastCall = [];

  ask(options) {
    this.lastCall = ['ask', ...arguments];
  }

  handleError(err) {
    this.lastCall = ['handleError', ...arguments];
  }

  showError(error) {
    this.lastCall = ['showError', ...arguments];
  }

  showMessage(title, message) {
    this.lastCall = ['showMessage', ...arguments];
  }
}
