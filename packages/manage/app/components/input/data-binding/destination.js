/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { DataBindingDestination } from '../../../transforms/data-binding-destination';

export default class InputDataBindingDestinationComponent extends Component {
  @service store;

  @tracked _value;
  @tracked _deleteNonSyncedRecords = null;
  @tracked maps;

  get value() {
    if (!this._value) {
      return this.args.value?.modelId;
    }

    return this._value;
  }

  get deleteNonSyncedRecords() {
    if (this._deleteNonSyncedRecords === null) {
      return this.args.value?.deleteNonSyncedRecords;
    }

    return this._deleteNonSyncedRecords;
  }

  set deleteNonSyncedRecords(value) {
    this._deleteNonSyncedRecords = value;
    this.triggerChange();
  }

  triggerChange() {
    const value = new DataBindingDestination({
      type: 'Map',
      modelId: this.value?.id ?? '',
      deleteNonSyncedRecords: this.deleteNonSyncedRecords ?? false,
    });

    return this.args.onChange?.(value);
  }

  @action
  changeValue(value) {
    this._value = value;
    this.triggerChange();
  }

  @action
  async setup() {
    if (this.args.team?.id == this.teamId) {
      return;
    }

    if (!this.args.team) {
      this.maps = [];
      this._value = null;
      this.triggerChange();
      return;
    }

    this.teamId = this.args.team.id;
    this.maps = await this.store.query('map', { team: this.teamId });

    if (this.maps.length && !this.value?.id) {
      this._value = this.maps[0];
      this.triggerChange();
    }
  }
}
