/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { transform } from 'ol/proj';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class MapInteractiveComponent extends Component {
  @service store;
  @tracked _baseMaps;
  @tracked isDragged = false;

  get baseMaps() {
    return this._baseMaps;
  }

  get selectedBaseMap() {
    return this.baseMaps?.[0];
  }

  get userLocation() {
    if (!this.args.userLocation) {
      return null;
    }

    return transform(this.args.userLocation, 'EPSG:4326', 'EPSG:3857');
  }

  updateUserLocation() {
    if (!this.args.userLocation || !this.args.followUserLocation) {
      return;
    }

    const view = this.map.getView();
    view.setZoom(18);
    view.setCenter(this.userLocation);
  }

  changeCenter(e) {
    this.isDragged = true;
  }

  @action
  resetDrag() {
    this.isDragged = false;
    this.focusOnUser();
  }

  @action
  mapReady(map) {
    this.map = map;
    this.view = this.map.getView();
    this.updateUserLocation();

    this._changeCenter = (e) => this.changeCenter(e);

    this.view.on('change:center', this._changeCenter);
  }

  focusOnUser() {
    this.view.un('change:center', this._changeCenter);
    this.view.setCenter(this.userLocation);
    this.view.on('change:center', this._changeCenter);
  }

  @action
  focusOnCenter() {
    this.view.un('change:center', this._changeCenter);
    this.view.setCenter(this.args.center);
    this.view.setZoom(16);
    this.view.on('change:center', this._changeCenter);
  }

  @action
  updateViewCenter() {
    if (this.isDragged) {
      return;
    }

    if(this.args?.center?.length == 2) {
      return this.focusOnCenter();
    }

    if(!this.args.followUserLocation) {
      return;
    }

    this.focusOnUser();
  }

  @action
  async setup() {
    this._baseMaps = await this.store.query('base-map', { default: true });

    if(this.args?.center?.length == 2) {
      this.focusOnCenter();
    }
  }
}
