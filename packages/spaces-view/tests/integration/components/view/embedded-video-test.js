import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | view/embedded-video', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::EmbeddedVideo />`);

    expect(this.element.querySelector('source')).not.to.exist;
  });

  it('renders a video when there is a source', async function (assert) {
    this.set('value', {
      data: {
        source: {
          url: 'https://docs.giscollective.com/img/manage/pages/edit-page-info.mp4',
        },
      },
    });

    await render(hbs`<View::EmbeddedVideo @value={{this.value}}/>`);

    expect(this.element.querySelector('video')).to.exist;
    expect(this.element.querySelector('source')).to.have.attribute(
      'src',
      'https://docs.giscollective.com/img/manage/pages/edit-page-info.mp4',
    );
  });
});
