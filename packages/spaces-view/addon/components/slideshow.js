import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class SlideshowComponent extends Component {
  @service fullscreen;

  @tracked isSlideshowVisible = false;
  @tracked isSlideshowPresentable = false;
  @tracked isSlideshowPresenting = false;
  @tracked slideshowTarget;

  get largePictureValues() {
    return {
      sizing: 'auto',
      classes: [],
      attributions: 'below',
    };
  }

  @action
  setup() {
    this.slideshowTarget = document.querySelector('#slideshow-target');

    later(() => {
      this.fullscreen.freezeBody();
      this.isSlideshowPresentable = true;
    }, 1);
  }

  @action
  willDestroy() {
    super.willDestroy(...arguments);
    this.fullscreen.unfreezeBody();
  }

  @action
  prepareHideSlideshow() {
    this.isSlideshowPresentable = false;

    later(() => {
      const modal = this.slideshowTarget.querySelector(
        '.page-col-gallery-slideshow',
      );

      if (!modal) {
        return;
      }

      const style = getComputedStyle?.(modal);

      if (style.opacity == 0) {
        this.checkHideSlideshow();
      }
    }, 10);
  }

  @action
  checkHideSlideshow() {
    if (!this.isSlideshowPresentable) {
      return this.hideSlideshow();
    }

    this.isSlideshowPresenting = true;
  }

  @action
  hideSlideshow() {
    this.isSlideshowVisible = false;
    this.isSlideshowPresentable = false;
    this.isSlideshowPresenting = false;

    this.args?.onHide();
  }
}
