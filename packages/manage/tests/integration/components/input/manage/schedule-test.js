/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | input/manage/schedule', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders the value', async function (assert) {
      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'weekly',
          timezone: 'CET',
        },
      ]);

      await render(hbs`<Manage::Schedule @value={{this.value}} @title="events" />`);

      expect(
        this.element
          .querySelector('.date-interval')
          .textContent.split('\n')
          .map((a) => a.trim())
          .filter((a) => a)
          .join(' '),
      ).to.equal(`01/05/2023 - 01/05/2024`);
    });
  });

  describe('in edit mode', function () {
    it('renders the value', async function (assert) {
      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'weekly',
          timezone: 'CET',
        },
      ]);

      await render(hbs`<Manage::Schedule @value={{this.value}} @title="events" @editablePanel="events" />`);

      expect(this.element.querySelector('.schedule-begin .datepicker-input').value).to.equal('2023-05-01');
    });

    it('can edit the entry', async function (assert) {
      let value;

      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'weekly',
          timezone: 'CET',
        },
      ]);

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Schedule @onSave={{this.save}} @value={{this.value}} @title="events" @editablePanel="events" />`,
      );

      await fillIn('.schedule-begin .datepicker-input', '2023-05-14');

      await click('.btn-submit');

      expect(value.map((a) => a.toJSON())).to.deep.equal([
        {
          begin: '2023-05-14T00:00:00.000Z',
          end: '2023-05-14T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'weekly',
          timezone: 'CET',
        },
      ]);
    });

    it('can cancel the changes', async function (assert) {
      let value;

      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'weekly',
          timezone: 'CET',
        },
      ]);

      let cancel;
      this.set('cancel', () => {
        cancel = true;
      });

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Schedule @onCancel={{this.cancel}} @onSave={{this.save}} @value={{this.value}} @title="events" @editablePanel="events" />`,
      );

      this.element.querySelector('.schedule-begin .day').value = '14';
      await triggerEvent(this.element.querySelector('.schedule-begin .day'), 'change');

      await click('.btn-cancel');

      expect(value).not.to.exist;
      expect(cancel).to.equal(true);
      expect(this.value).to.deep.equal([
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'weekly',
          timezone: 'CET',
        },
      ]);
    });
  });
});
