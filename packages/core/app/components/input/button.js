/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { later } from '@ember/runloop';
import { buildWaiter } from '@ember/test-waiters';

let waiter = buildWaiter('input-button-waiter');

export default class InputButtonComponent extends Component {
  @service notifications;
  @tracked isWaiting;
  @tracked isError;

  @action
  async handleAction() {
    if (!this.args.action || this.args.isEditor) {
      return;
    }

    let token = waiter.beginAsync();

    this.isWaiting = true;
    this.isError = false;

    let result;

    try {
      result = await this.args.action();
    } catch (err) {
      this.notifications.handleError(err);
      this.isError = true;
    }

    this.isWaiting = false;

    later(() => {
      waiter.endAsync(token);
    });

    return result;
  }
}
