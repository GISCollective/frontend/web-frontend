/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | view/attribute', function (hooks) {
  let featureData;
  let server;

  setupRenderingTest(hooks);

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    featureData = server.testData.storage.addDefaultFeature();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::Attribute />`);
    assert.dom(this.element).hasText('');
  });

  it("renders nothing when the model can't be loaded", async function (assert) {
    this.set('value', {
      data: {
        source: { model: 'feature', id: '1' },
        sourceAttribute: {
          field: 'icon.property',
        },
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        attributeStyle: {
          color: 'red',
          classes: ['fw-light'],
        },
      },
    });

    await render(hbs`<View::Attribute @value={{this.value}}/>`);
    assert.dom(this.element).hasText('');
  });

  it('renders the title and attribute when the model is found and it has the attribute', async function (a) {
    featureData.attributes = {
      icon: {
        property: 'value',
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      modelKey: 'feature',
      data: {
        source: { model: 'feature', id: '1' },
        sourceAttribute: {
          field: 'icon.property',
        },
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        attributeStyle: {
          color: 'red',
          classes: ['fw-light'],
        },
      },
    });

    this.set('model', {
      feature,
    });

    await render(
      hbs`<View::Attribute @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(
      this.element.querySelector('.attribute-title').textContent.trim(),
    ).to.equal('some title');
    expect(this.element.querySelector('.attribute-title')).to.have.classes(
      'text-with-options display-3 fw-bold text-green',
    );

    expect(
      this.element.querySelector('.attribute-value').textContent.trim(),
    ).to.equal('value');
    expect(this.element.querySelector('.attribute-value')).to.have.classes(
      'text-with-options fw-light text-red',
    );
  });

  it('renders the title and attributes when the value is an object article', async function (a) {
    featureData.attributes = {
      icon: {
        property: {
          blocks: [{ type: 'paragraph', data: { text: 'description' } }],
        },
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      modelKey: 'feature',
      data: {
        source: { model: 'feature', id: '1' },
        sourceAttribute: {
          field: 'icon.property',
        },
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        attributeStyle: {
          color: 'red',
          classes: ['fw-light'],
        },
      },
    });

    this.set('model', {
      feature,
    });

    await render(
      hbs`<View::Attribute @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(
      this.element.querySelector('.attribute-title').textContent.trim(),
    ).to.equal('some title');

    expect(
      this.element.querySelector('.attribute-value').textContent.trim(),
    ).to.equal('description');
  });

  it('renders nothing when the feature does not have the attribute', async function (a) {
    featureData.attributes = {};

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      modelKey: 'feature',
      data: {
        source: { model: 'feature', id: '1' },
        sourceAttribute: {
          field: 'icon.property',
        },
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        attributeStyle: {
          color: 'red',
          classes: ['fw-light'],
        },
      },
    });

    this.set('model', {
      feature,
    });

    await render(
      hbs`<View::Attribute @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelector('.attribute-title')).not.to.exist;

    expect(this.element.querySelector('.attribute-value')).not.to.exist;
  });
});
