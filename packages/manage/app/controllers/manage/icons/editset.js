/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { IconStyle } from '../../../transforms/icon-style';

export default class ManageIconsEditSetController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service router;

  @tracked editableField = '';
  @tracked savingField = '';
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        text: this.model.iconSet.name,
      },
      {
        text: this.intl.t('edit'),
        capitalize: true,
      },
    ];
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.iconSet.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      },
    );
  }

  @action
  saveIsPublic(newValue) {
    this.model.iconSet.visibility.isPublic = newValue;
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.iconSet.visibility.isDefault = newValue;
    this.editableField = this.intl.t('is default');

    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.iconSet.visibility.team = newValue;
    this.editableField = this.intl.t('team');

    return this.save();
  }

  @action
  saveStyle(newValue) {
    this.model.iconSet.styles = new IconStyle(newValue);
    return this.save();
  }

  @action
  saveArticle(name, description) {
    this.model.iconSet.set('name', name);
    this.model.iconSet.set('description', description);

    return this.save();
  }

  @action
  saveCover(index, cover) {
    this.model.iconSet.set('cover', cover);

    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.iconSet.rollbackAttributes();
  }

  @action
  async delete() {
    await this.model.iconSet.destroyRecord();

    await this.router.transitionTo('manage.dashboards.dashboard', this.model.team.id);
  }
}
