/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
// eslint-disable-next-line no-undef
module.exports = {
  rules: {
    'no-bare-strings': false,
    'no-inline-styles': false,
    'no-invalid-interactive': false,
    'no-action': false,
    'no-implicit-this': { allow: ['target.value'] },
  },
};
