/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, click, waitUntil, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | state-filters/icons',  function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let store;
  let iconData;

  hooks.before(function () {
    server = new TestServer();
    iconData = server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultPicture();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    server.history = [];
  });

  it('renders the label when there is no state', async function () {
    await render(hbs`<StateFilters::Icons />`);

    expect(
      this.element.querySelector('.btn-select').textContent.trim()
    ).to.equal('icon');
  });

  it('renders the icon name when the state is set to an icon id and the icon is preloaded', async function () {
    await store.findRecord('icon', iconData._id);
    server.history = [];

    this.set('state', `if@${iconData._id}`);
    this.set('options', { iconSet: "1", source: 'icon-set' });

    await render(hbs`<StateFilters::Icons @options={{this.options}} @state={{this.state}} />`);
    await server.waitAllRequests();

    expect(
      this.element.querySelector('.btn-select').textContent.trim()
    ).to.equal('Healthy Dining');
    expect(server.history).to.deep.equal([
      'GET /mock-server/icons?grupset=true&group=true',
    ]);
  });

  it('renders the icon name when the state is set to an icon id and the icon is not preloaded', async function () {
    this.set('state', `if@${iconData._id}`);
    this.set('options', { iconSet: "1", source: 'icon-set' });

    await render(hbs`<StateFilters::Icons @state={{this.state}} @options={{this.options}} />`);
    await server.waitAllRequests();

    await waitUntil(() => this.element.querySelector(".btn-pill-icon").textContent.trim() != "icon");

    expect(
      this.element.querySelector('.btn-select').textContent.trim()
    ).to.equal('Healthy Dining');
  });

  it('shows the state icon in the selector', async function () {
    this.set('state', `if@${iconData._id}`);

    this.set('options', { iconSet: "1", source: 'icon-set' });
    await render(hbs`<StateFilters::Icons @state={{this.state}} @options={{this.options}} />`);

    await click(".btn-pill-icon");

    expect(this.element.querySelector('.selected.icon-element')).to.exist;
  });

  it('shows the icon picker on click', async function () {
    await render(hbs`<StateFilters::Icons @state={{this.state}} />`);

    await click('.btn-select');
    await server.waitAllRequests();

    expect(this.element.querySelector('.icon-picker')).to.have.class(
      'is-visible'
    );
  });

  it('triggers the onChangeState when an icon is selected', async function () {
    let value;

    this.set('state', '');
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });
    this.set('options', { iconSet: "1", source: 'icon-set' });

    await render(
      hbs`<StateFilters::Icons @state={{this.state}} @options={{this.options}} @onChangeState={{this.onChangeState}} />`
    );

    await click('.btn-pill-icon');
    await click('.btn-icon');

    expect(value).to.equal('if@5ca7bfc0ecd8490100cab980');
    expect(
      this.element.querySelector('.btn-select').textContent.trim()
    ).to.equal('Healthy Dining');
  });

  it('hides the icon picker on icon select', async function () {
    this.set('options', { iconSet: "1", source: 'icon-set' });

    await render(hbs`<StateFilters::Icons @options={{this.options}} @state={{this.state}} />`);

    await click('.btn-select');
    await click('.btn-icon');

    expect(this.element.querySelector('.icon-picker')).not.to.have.class(
      'is-visible'
    );
  });

  it('triggers the onChangeState with no if value when the filter is cleared', async function () {
    let value;

    this.set('state', 'if@5ca7bfc0ecd8490100cab980');
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });
    this.set('options', { iconSet: "1", source: 'icon-set' });

    await render(
      hbs`<StateFilters::Icons @state={{this.state}} @options={{this.options}} @onChangeState={{this.onChangeState}} />`
    );

    await waitFor(".btn-reset");

    await click('.btn-reset');

    expect(value).to.equal('');
    expect(
      this.element.querySelector('.btn-select').textContent.trim()
    ).to.equal('icon');
  });

  it('hides the icon picker when the close button is pressed', async function () {
    await render(hbs`<StateFilters::Icons @state={{this.state}} />`);

    await click('.btn-select');

    await click('.btn-close-picked');

    expect(this.element.querySelector('.icon-picker')).not.to.have.class(
      'is-visible'
    );
  });
});
