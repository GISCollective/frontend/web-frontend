/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class BrowseMapArticleComponent extends Component {

  @tracked _team;

  get meta() {
    if (!this.args.map?.meta) {
      return [];
    }

    return Object.keys(this.args.map.meta).map((key) => {
      return { title: key, value: this.args.map.meta[key] };
    });
  }

  get contentBlocks() {
    if (!this.args.map) {
      return { blocks: [] };
    }

    const blocks = this.args.map.contentBlocks?.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1)) ?? [];

    return { blocks };
  }

  get hasTeam() {
    return !!this._team;
  }

  @action
  async setup() {
    try {
      this._team = await this.args.map?.visibility?.fetchTeam();
    } catch(err) {
      this._team = null;
      console.error(err);
    }
  }
}
