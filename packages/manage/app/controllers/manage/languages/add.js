/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class ManageLanguagesAddController extends Controller {
  @tracked fileSelectorLabel;
  @service notifications;
  @service intl;
  @service router;

  get isInvalid() {
    return (
      !this.model.hasDirtyAttributes ||
      this.model.name.trim() == '' ||
      this.model.locale.trim() == '' ||
      this.model.file.trim() == ''
    );
  }

  @action
  save() {
    this.model.save().then(
      () => {
        return this.router.transitionTo('manage.languages.index');
      },
      (err) => {
        this.notifications.handleError(err);
      },
    );
  }

  @action
  parseFile(fileName, content) {
    this.model.file = content;
  }
}
