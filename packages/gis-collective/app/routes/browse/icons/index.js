/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BrowseRoute from '../../base/browse';
import { hash } from 'rsvp';
import LazyList from 'models/lib/lazy-list';

export default class BrowseIconsIndexRoute extends BrowseRoute {
  async model(params) {
    const iconsParams = {};

    if (params['search']) {
      iconsParams['term'] = params['search'];
    }

    const iconSets = new LazyList('icon-set', iconsParams, this.store);
    await iconSets.loadNext();

    return hash({
      iconSets,
    });
  }
}
