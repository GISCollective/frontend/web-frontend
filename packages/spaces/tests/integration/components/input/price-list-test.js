/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | input/price-list", function (hooks) {
  setupRenderingTest(hooks);

  it("can add a price to an empty list", async function () {
    let value;
    this.set("save", (v) => {
      value = v;
    });

    await render(hbs`<Input::PriceList @onChange={{this.save}} />`);

    await click(".btn-add");
    await fillIn(".text-value-0", "22");
    await fillIn(".text-currency-0", "EUR");
    await fillIn(".text-details-0", "details 22");

    expect(value).to.deep.equal([{ value: "22", currency: "EUR", details: "details 22" }]);
  });

  it("can add a price and an empty item to an empty list", async function () {
    let value;
    this.set("save", (v) => {
      value = v;
    });

    await render(hbs`<Input::PriceList @onChange={{this.save}} />`);

    await click(".btn-add");
    await fillIn(".text-value-0", "some value");
    await click(".btn-add");

    expect(this.element.querySelector(".text-value-1").value).to.equal("");
    expect(value).to.deep.equal([{ value: "some value" }, {}]);
  });

  it("renders a list of prices", async function (a) {
    this.set("value", [
      { value: 1, currency: "usd", details: "details 1" },
      { value: 2, currency: "eur", details: "details 2" },
      { value: 3, currency: "gbp", details: "details 3" },
    ]);

    await render(hbs`<Input::PriceList @value={{this.value}} />`);

    expect(this.element.querySelector(".text-value-0").value).to.equal("1");
    expect(this.element.querySelector(".text-currency-0").value).to.equal("usd");
    expect(this.element.querySelector(".text-details-0").value).to.equal("details 1");

    expect(this.element.querySelector(".text-value-1").value).to.equal("2");
    expect(this.element.querySelector(".text-value-2").value).to.equal("3");
  });

  it("can delete a value", async function () {
    this.set("value", ["1", "2", "3"]);

    let value;
    this.set("save", (v) => {
      value = v;
    });

    await render(hbs`<Input::PriceList @value={{this.value}} @onChange={{this.save}} />`);

    const btns = this.element.querySelectorAll(".btn-danger");

    await click(btns[1]);

    expect(value).to.deep.equal(["1", "3"]);
  });
});
