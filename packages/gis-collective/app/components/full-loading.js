/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import config from '../config/environment';

export default class FullLoadingComponent extends Component {
  @tracked isVisible = true;
  apiUrl = config.apiUrl;

  @action
  update() {
    this.isVisible = this.args.isVisible;
  }

  @action
  insert() {
    this.isVisible = this.args.isVisible;
  }
}
