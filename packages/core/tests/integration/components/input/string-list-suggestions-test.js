/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe('Integration | Component | input/string-list-suggestions', function (hooks) {
  setupRenderingTest(hooks);

  it('can add a string to an empty list', async function (a) {
    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(hbs`<Input::StringListSuggestions @onChange={{this.save}} />`);

    await click('.btn-add');
    await selectSearch('.text-value-0', 'some value');
    await click(this.element);

    expect(value).to.deep.equal(['some value']);
  });

  it('can pick an existing option', async function (a) {
    let value;
    this.set('save', (v) => {
      value = v;
    });

    this.set('options', ['option 1', 'option 2', 'option 3', 'other']);

    await render(hbs`<Input::StringListSuggestions @options={{this.options}} @onChange={{this.save}} />`);

    await click('.btn-add');
    await selectSearch('.text-value-0', 'opt');
    await selectChoose('.text-value-0', 'option 2');
    await click(this.element);

    expect(value).to.deep.equal(['option 2']);
  });

  it('can add a string and an empty item to an empty list', async function (a) {
    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(hbs`<Input::StringListSuggestions @onChange={{this.save}} />`);

    await click('.btn-add');
    await selectSearch('.text-value-0', 'some value');
    await click('.btn-add');

    expect(this.element.querySelector('.text-value-1').textContent.trim()).to.equal('');
    expect(value).to.deep.equal(['some value', '']);
  });

  it('renders a list of strings', async function () {
    this.set('value', ['1', '2', '3']);

    await render(hbs`<Input::StringListSuggestions @value={{this.value}} />`);

    expect(this.element.querySelector('.text-value-0').textContent.trim()).to.equal('1');
    expect(this.element.querySelector('.text-value-1').textContent.trim()).to.equal('2');
    expect(this.element.querySelector('.text-value-2').textContent.trim()).to.equal('3');
  });

  it('can delete a value', async function () {
    this.set('value', ['1', '2', '3']);

    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(hbs`<Input::StringListSuggestions @value={{this.value}} @onChange={{this.save}} />`);

    const btns = this.element.querySelectorAll('.btn-danger');

    await click(btns[1]);
    await click(this.element);

    expect(value).to.deep.equal(['1', '3']);
  });

  describe('with primary categories', function () {
    it('shows a select for the first option', async function () {
      this.set('value', ['1', '2', '3']);
      this.set('primaryOptions', ['10', '20', '30']);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Input::StringListSuggestions @primaryOptions={{this.primaryOptions}} @value={{this.value}} @onChange={{this.save}} />`,
      );

      const groups = this.element.querySelectorAll('.input-group');
      expect(groups[0].querySelector('.ember-basic-dropdown-trigger')).not.to.exist;
      expect(groups[0].querySelector('select').value).to.equal('');

      const values = [...this.element.querySelectorAll('.ember-power-select-selected-item')];
      expect(values).to.have.length(3);
      expect(values[0]).to.have.textContent('1');
      expect(values[1]).to.have.textContent('2');
      expect(values[2]).to.have.textContent('3');
    });

    it('shows the primary value when it is set', async function () {
      this.set('value', ['10', '2', '3']);
      this.set('primaryOptions', ['10', '20', '30']);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Input::StringListSuggestions @primaryOptions={{this.primaryOptions}} @value={{this.value}} @onChange={{this.save}} />`,
      );

      const groups = this.element.querySelectorAll('.input-group');
      expect(groups[0].querySelector('.ember-basic-dropdown-trigger')).not.to.exist;
      expect(groups[0].querySelector('select').value).to.equal('10');

      const values = [...this.element.querySelectorAll('.ember-power-select-selected-item')];
      expect(values).to.have.length(2);
      expect(values[0]).to.have.textContent('2');
      expect(values[1]).to.have.textContent('3');
    });

    it('can change a primary value', async function () {
      this.set('value', ['10', '2', '3']);
      this.set('primaryOptions', ['10', '20', '30']);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Input::StringListSuggestions @primaryOptions={{this.primaryOptions}} @value={{this.value}} @onChange={{this.save}} />`,
      );

      this.element.querySelector('select').value = '20';
      await triggerEvent('select', 'change');

      expect(value).to.deep.equal(['20', '2', '3']);
    });

    it('triggers an event when the primary value is changed', async function () {
      this.set('value', ['10', '2', '3']);
      this.set('primaryOptions', ['10', '20', '30']);

      let value;
      this.set('primaryChange', (v) => {
        value = v;
      });
      this.set('save', (v) => {});

      await render(
        hbs`<Input::StringListSuggestions @onPrimaryChange={{this.primaryChange}} @primaryOptions={{this.primaryOptions}} @value={{this.value}} @onChange={{this.save}} />`,
      );

      this.element.querySelector('select').value = '20';
      await triggerEvent('select', 'change');

      expect(value).to.deep.equal('20');
    });

    it('can change a secondary value', async function () {
      this.set('value', ['10', '2', '3']);
      this.set('primaryOptions', ['10', '20', '30']);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Input::StringListSuggestions @primaryOptions={{this.primaryOptions}} @value={{this.value}} @onChange={{this.save}} />`,
      );

      await selectSearch('.text-value-1', 'some value');
      await click('.text-value-2');

      expect(value).to.deep.equal(['10', 'some value', '3']);
    });

    it('shows 2 primary values when set', async function () {
      this.set('value', ['10', '20', '3']);
      this.set('primaryOptions', ['10', '20', '30']);

      let value;
      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Input::StringListSuggestions @primaryOptions={{this.primaryOptions}} @value={{this.value}} @onChange={{this.save}} />`,
      );

      const groups = this.element.querySelectorAll('.input-group');
      expect(groups[0].querySelector('.ember-basic-dropdown-trigger')).not.to.exist;
      expect(groups[0].querySelector('select').value).to.equal('10');

      const values = [...this.element.querySelectorAll('.ember-power-select-selected-item')];
      expect(values).to.have.length(2);
      expect(values[0]).to.have.textContent('20');
      expect(values[1]).to.have.textContent('3');
    });
  });
});
