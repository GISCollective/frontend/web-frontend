/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { SpaceSearchOptions } from 'models/transforms/space-search-options';
import { expect } from 'chai';

module('Integration | Component | manage/search-options', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders the value when it is set', async function (a) {
      this.set('value', new SpaceSearchOptions({ features: true }));

      await render(hbs`<Manage::SearchOptions @value={{this.value}} @title="events" />`);

      const keys = [...this.element.querySelectorAll('.text-model-name')].map((a) => a.textContent.trim());
      const values = [...this.element.querySelectorAll('.text-model-search-value')].map((a) => a.textContent.trim());

      expect(keys).to.deep.equal([
        'features',
        'maps',
        'campaigns',
        'iconSets',
        'teams',
        'icons',
        'geocodings',
        'events',
        'articles',
      ]);
      expect(values).to.deep.equal(['yes', 'no', 'no', 'no', 'no', 'no', 'no', 'no', 'no']);
    });
  });

  describe('in edit mode', function () {
    it('renders the value', async function (assert) {
      this.set('value', new SpaceSearchOptions({ features: true }));

      await render(hbs`<Manage::SearchOptions @value={{this.value}} @title="events" @editablePanel="events" />`);

      const keys = [...this.element.querySelectorAll('.text-model-name')].map((a) => a.textContent.trim());
      const values = [...this.element.querySelectorAll('.text-model-search-value select')].map((a) => a.value);

      expect(keys).to.deep.equal([
        'features',
        'maps',
        'campaigns',
        'iconSets',
        'teams',
        'icons',
        'geocodings',
        'events',
        'articles',
      ]);
      expect(values).to.deep.equal(['true', 'false', 'false', 'false', 'false', 'false', 'false', 'false', 'false']);
    });

    it('renders the value', async function (assert) {
      let value;

      this.set('value', new SpaceSearchOptions({ features: true }));

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::SearchOptions @onSave={{this.save}} @value={{this.value}} @title="events" @editablePanel="events" />`,
      );

      this.element.querySelector('.value-features').value = 'false';
      await triggerEvent(this.element.querySelector('.value-features'), 'change');

      this.element.querySelector('.value-maps').value = 'true';
      await triggerEvent(this.element.querySelector('.value-maps'), 'change');

      await click('.btn-submit');

      expect(value.toJSON()).to.deep.equal({
        features: false,
        maps: true,
        campaigns: false,
        iconSets: false,
        teams: false,
        icons: false,
        geocodings: false,
        events: false,
        articles: false,
      });
    });
  });
});
