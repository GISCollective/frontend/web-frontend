/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, waitUntil, triggerEvent, click, fillIn, waitFor } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/teams/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let team;
  let receivedTeam;
  let defaultPicture;
  let receivedPicture;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(async function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    team = server.testData.create.team();
    defaultPicture = server.testData.create.picture();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPage();

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/teams/${team._id}`, (request) => {
      receivedTeam = JSON.parse(request.requestBody);
      receivedTeam.team['_id'] = team._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedTeam)];
    });

    receivedTeam = null;
  });

  hooks.afterEach(function() {
    return server.waitAllRequests();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fteams%2F5ca78e2160780601008f69e6');
  });

  it('should show the team page', async function (a) {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    a.deepEqual(PageElements.containerGroupTitles(), [
      'name and description',
      'logo',
      'gallery',
      "social media",
      "contact email",
      'categories',
      'members',
      "Invite more users:"
    ]);
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();
    await visit(`/manage/teams/${team._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Edit']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    ]);
  });

  it('should be able to update the article', async function () {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    await click('.row-article .btn-edit');

    await click('.ce-header');
    await wait(200);

    await fillIn('.ce-header', 'new name');

    await click('.ce-paragraph');
    await wait(200);

    await fillIn('.ce-paragraph', 'new about');
    await wait(200);

    await click('.ce-header');

    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'));
    await click('.btn-submit');

    await waitUntil(() => receivedTeam);

    expect(receivedTeam.team.name).to.equal('new name');
    expect(receivedTeam.team.about.blocks).to.deep.equal([
      { type: 'header', data: { text: 'new name', level: 1 } },
      { type: 'paragraph', data: { text: 'new about' } },
    ]);
  });

  it('should be able to update the logo', async function () {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    await click('.row-logo .btn-edit');

    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-logo input[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    await waitUntil(() => receivedPicture);
    await waitUntil(() => receivedTeam);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Team', id: team._id },
      data: {},
      disableOptimization: false,
    });
    expect(receivedTeam.team.logo).to.equal(defaultPicture._id);
  });

  it('should be able to update two gallery pictures', async function () {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    await click('.row-gallery .btn-edit');

    const blob = server.testData.create.pngBlob();
    await triggerEvent(".row-gallery input[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    await waitUntil(() => receivedPicture);
    await waitUntil(() => receivedTeam);

    expect(receivedTeam.team.pictures).to.deep.equal([defaultPicture._id]);
  });

  it('should render the "is public" label', async function () {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain('is public');
    expect(this.element.querySelector('.container-group-is-public label').textContent).to.contain('is public');
  });

  it('should be able to make the team not public', async function () {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-is-public .btn-switch'));

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.isPublic).to.equal(false);
  });

  it('can toggle the allowCustomDataBindings flag when the user is admin', async function () {
    server.testData.storage.addDefaultUser(true);

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-allow-custom-data-bindings .btn-switch'));

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.allowCustomDataBindings).to.equal(true);
  });

  it('can toggle the allowNewsletters flag when the user is admin', async function () {
    server.testData.storage.addDefaultUser(true);

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-can-send-newsletters .btn-switch'));

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.allowNewsletters).to.equal(true);
  });

  it('can toggle the allowEvents flag when the user is admin', async function () {
    server.testData.storage.addDefaultUser(true);

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-can-create-events .btn-switch'));

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.allowEvents).to.equal(true);
  });

  it('can toggle the allowReports flag when the user is admin', async function () {
    server.testData.storage.addDefaultUser(true);

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-can-view-reports .btn-switch'));

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.allowReports).to.equal(true);
  });

  it('only shows a label for allowCustomDataBindings flag when the user is not admin', async function () {
    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-allow-custom-data-bindings .btn-switch')).not.to
      .exist;

    expect(this.element.querySelector('.value-allow-custom-data-bindings').textContent.trim()).to.equal('no');
  });

  it('only shows a label for allowNewsletters flag when the user is not admin', async function () {
    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-can-send-newsletters .btn-switch')).not.to.exist;

    expect(this.element.querySelector('.value-can-send-newsletters').textContent.trim()).to.equal('no');
  });

  it('only shows a label for allowEvents flag when the user is not admin', async function () {
    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-can-crete-events .btn-switch')).not.to.exist;

    expect(this.element.querySelector('.value-can-create-events').textContent.trim()).to.equal('no');
  });

  it('only shows a label for allowReports flag when the user is not admin', async function () {
    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-can-view-reports .btn-switch')).not.to.exist;

    expect(this.element.querySelector('.value-can-view-reports').textContent.trim()).to.equal('no');
  });

  it('can edit the team categories', async function () {
    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    await click('.row-categories .btn-edit');
    await click('.btn-add');

    await fillIn('.text-value-0', 'some category');

    await click('.btn-submit');

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.categories).to.deep.equal(['some category']);
  });

  it('can edit the social media fields', async function () {
    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    await click('.row-social-media .btn-edit');

    await fillIn('.row-social-media input', 'https://example.com');

    await click('.btn-submit');

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.socialMediaLinks).to.deep.equal({ facebook: 'https://example.com', youtube: '', xTwitter: '', instagram: '', whatsApp: '', tikTok: '', linkedin: '' });
  });

  it('can edit the email field', async function () {
    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    await click('.row-contact-email .btn-edit');

    await fillIn('.row-contact-email input', 'test@example.com');

    await click('.btn-submit');

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.contactEmail).to.equal('test@example.com');
  });

  it('can invite a list of emails to the team', async function (a) {
    let receivedInvitation = null;
    server.post(`/mock-server/teams/5ca78e2160780601008f69e6/invite`, (request) => {
      receivedInvitation = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({}),
      ];
    });

    server.testData.storage.addDefaultUser();

    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    await fillIn(".input-email-list", "a@b.c\nd@e.f");

    this.element.querySelector('.input-role').value = 'members';
    await triggerEvent('.input-role', 'change');

    await click(".btn-invite");
    await waitUntil(() => receivedInvitation);

    a.deepEqual(receivedInvitation, {
      "emailList": [
        "a@b.c",
        "d@e.f"
      ],
      "role": "members"
    });
  });

  describe("for a team with pending invitations", function (hooks) {
    hooks.beforeEach(function () {
      team = server.testData.create.team();
      team.invitations = [{
        "email": "user1@asd.com",
        "role": "guests"
      }, {
        "email": "user2@asd.com",
        "role": "members"
      }];

      server.testData.storage.addTeam(team);
    });

    it("shows the list of invited emails", async function (a) {
      authenticateSession();

      await visit(`/manage/teams/${team._id}`);

      const emails = [...this.element.querySelectorAll(".pending-email")].map(a => a.textContent.trim());
      const roles = [...this.element.querySelectorAll(".pending-role")].map(a => a.textContent.trim());

      a.deepEqual(emails, ["user1@asd.com", "user2@asd.com"]);
      a.deepEqual(roles, ["guests", "members"]);
    });

    it("shows the list of invited emails", async function (a) {
      let message;

      const notifications = this.owner.lookup('service:notifications');
      notifications.ask = (m) => {
        message = m;
      };

      authenticateSession();

      await visit(`/manage/teams/${team._id}`);

      await click(".pending-delete");

      a.deepEqual(message, {
        "question": "Are you sure you want to remove `user1@asd.com` from the team?",
        "title": "remove pending member"
      });

      const emails = [...this.element.querySelectorAll(".pending-email")].map(a => a.textContent.trim());
      const roles = [...this.element.querySelectorAll(".pending-role")].map(a => a.textContent.trim());

      a.deepEqual(emails, ["user2@asd.com"]);
      a.deepEqual(roles, ["members"]);

      await waitUntil(() => receivedTeam != null, { timeout: 3000 });

      a.deepEqual(receivedTeam.team.invitations, [{
        "email": "user2@asd.com",
        "role": "members"
      }]);
    });
  });
});
