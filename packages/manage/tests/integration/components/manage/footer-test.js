import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { expect } from 'chai';

describe('Integration | Component | manage/footer', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let spaceData;
  let defaultSpaceData;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function() {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    spaceData = server.testData.storage.addDefaultSpace();
    defaultSpaceData = server.testData.storage.addDefaultSpace('1');

    const service = this.owner.lookup('service:space');
    service.currentSpace = await this.store.findRecord('space', spaceData._id);
    service.defaultSpace = await this.store.findRecord('space', defaultSpaceData._id);
  });

  it('renders the GISCollective logo and link', async function () {
    await render(hbs`<Manage::Footer />`);

    expect(this.element.querySelector('.giscollective-logo a')).to.have.attribute('href', 'https://giscollective.com');
    expect(this.element.querySelector('.giscollective-logo a')).to.have.attribute('target', '_blank');
    expect(this.element.querySelector('.giscollective-logo a img')).to.have.attribute(
      'src',
      '/img/giscollective-black.svg',
    );
  });

  it('renders the version', async function () {
    await render(hbs`<Manage::Footer @version="v1.2" />`);

    expect(this.element.querySelector('.version')).to.have.textContent('v1.2,');
  });

  it('renders only the docs link when the space has no help pages', async function () {
    await render(hbs`<Manage::Footer />`);

    expect(this.element.querySelectorAll('.more-help a')).to.have.length(1);
    expect(this.element.querySelector('.more-help a')).to.have.attribute('href', 'https://docs.giscollective.com/');
  });

  describe('for a space with all help pages', function () {
    it('renders the FAQ and services links', async function () {
      server.testData.storage.addDefaultPage('62854d8cd2f1a9010097ef02');
      server.testData.storage.addDefaultPage('62854cd753d20c01006f53d9');

      const space = await this.store.findRecord('space', spaceData._id);
      space._getPagesMap = {
        'resources--faq': '62854d8cd2f1a9010097ef02',
        'services--support-training': '62854cd753d20c01006f53d9',
      };

      await render(hbs`<Manage::Footer />`);

      const links = [...this.element.querySelectorAll('.more-help a')];

      expect(links).to.have.length(3);

      expect(links[0]).to.have.attribute('href', 'https://docs.giscollective.com/');
      expect(links[1]).to.have.attribute('href', '/resources/faq');
      expect(links[2]).to.have.attribute('href', '/services/support-training');
    });
  });

  describe('for a space with all contact pages', function () {
    it('renders the contact and report issue links', async function () {
      server.testData.storage.addDefaultPage('62854d8cd2f1a9010097ef02');
      server.testData.storage.addDefaultPage('62854cd753d20c01006f53d9');

      const space = await this.store.findRecord('space', spaceData._id);
      space._getPagesMap = {
        contact: '62854d8cd2f1a9010097ef02',
        'support--report-issue': '62854cd753d20c01006f53d9',
      };

      await render(hbs`<Manage::Footer />`);

      const links = [...this.element.querySelectorAll('.contact a')];

      expect(links).to.have.length(2);
      expect(links[0]).to.have.attribute('href', '/contact');
      expect(links[1]).to.have.attribute('href', '/support/report-issue');
    });
  });

  describe('for a space with no pages', function () {
    it('renders the contact and report issue links from the default space', async function () {
      server.testData.storage.addDefaultPage('62854d8cd2f1a9010097ef02');
      server.testData.storage.addDefaultPage('62854cd753d20c01006f53d9');

      const space = await this.store.findRecord('space', defaultSpaceData._id);
      space._getPagesMap = {
        contact: '62854d8cd2f1a9010097ef02',
        'support--report-issue': '62854cd753d20c01006f53d9',
      };

      await render(hbs`<Manage::Footer />`);

      const links = [...this.element.querySelectorAll('.contact a')];

      expect(links).to.have.length(2);
      expect(links[0]).to.have.attribute('href', 'https://test.giscollective.com/contact');
      expect(links[1]).to.have.attribute('href', 'https://test.giscollective.com/support/report-issue');
    });

    it('renders the FAQ and services links from the default space', async function () {
      server.testData.storage.addDefaultPage('62854d8cd2f1a9010097ef02');
      server.testData.storage.addDefaultPage('62854cd753d20c01006f53d9');

      const space = await this.store.findRecord('space', defaultSpaceData._id);
      space._getPagesMap = {
        'resources--faq': '62854d8cd2f1a9010097ef02',
        'services--support-training': '62854cd753d20c01006f53d9',
      };

      await render(hbs`<Manage::Footer />`);

      const links = [...this.element.querySelectorAll('.more-help a')];

      expect(links).to.have.length(3);

      expect(links[0]).to.have.attribute('href', 'https://docs.giscollective.com/');
      expect(links[1]).to.have.attribute('href', 'https://test.giscollective.com/resources/faq');
      expect(links[2]).to.have.attribute('href', 'https://test.giscollective.com/services/support-training');
    });
  });
});
