import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | nice-value', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<NiceValue />`);

    assert.dom().hasText('');
  });

  it('renders yes for true', async function (assert) {
    await render(hbs`<NiceValue @value={{true}} />`);

    assert.dom().hasText('yes');
  });

  it('renders no for false', async function (assert) {
    await render(hbs`<NiceValue @value={{false}} />`);

    assert.dom().hasText('no');
  });

  it('renders 0 for 0', async function (assert) {
    await render(hbs`<NiceValue @value={{0}} />`);

    assert.dom().hasText('0');
  });

  it('renders 1.2 for 1.2', async function (assert) {
    await render(hbs`<NiceValue @value={{1.2}} />`);

    assert.dom().hasText('1.2');
  });

  it('renders a string for a string', async function (assert) {
    await render(hbs`<NiceValue @value="test" />`);

    assert.dom().hasText('test');
  });

  it('renders blocks when the value has blocks', async function (assert) {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title2',
            level: 2,
          },
        },
      ],
    });

    await render(hbs`<NiceValue @value={{this.value}} />`);
    expect(this.element.querySelector('h2').textContent.trim()).to.equal('title2');
  });
});
