/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { toContentBlocks } from 'models/lib/content-blocks';

export default class AttributesValueLongTextComponent extends Component {
  get value() {
    return toContentBlocks(this.args.value);
  }

  @action
  updateValue(title, value) {
    this.args.onChange?.(value);
  }
}
