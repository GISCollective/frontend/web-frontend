/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from '../../helpers';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | icon', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Icon />`);

    expect(this.element.querySelector('.fa-dot-circle')).not.to.exist;
    expect(this.element.querySelector('img')).not.to.exist;
    expect(this.element.querySelector('.loading-icon')).not.to.exist;
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the icon when it is set', async function () {
    this.set('icon', {
      name: 'test icon',
      localName: 'test icon',
      image: { value: 'image', useParent: false },
    });

    await render(hbs`<Icon @src={{this.icon}} />`);

    expect(this.element.querySelector('.fa-dot-circle')).not.to.exist;
    expect(this.element.querySelector('.loading-icon')).not.to.exist;

    expect(this.element.querySelector('img')).to.exist;
    expect(this.element.querySelector('img')).to.have.attribute('src', 'image');
    expect(this.element.querySelector('img')).to.have.attribute('alt', 'test icon');
  });

  it('renders a question circle fa icon when the image is not set', async function () {
    this.set('icon', {
      name: 'test icon',
    });

    await render(hbs`<Icon @src={{this.icon}} />`);

    expect(this.element.querySelector('img')).not.to.exist;
    expect(this.element.querySelector('.loading-icon')).not.to.exist;
    expect(this.element.querySelector('.fa-circle-question')).to.exist;
  });

  it('renders an animated circle if it is a promise', async function () {
    this.set('icon', {
      then() {},
    });

    await render(hbs`<Icon @src={{this.icon}} />`);

    expect(this.element.querySelector('img')).not.to.exist;
    expect(this.element.querySelector('.fa-circle-question')).not.to.exist;
    expect(this.element.querySelector('.loading-icon')).to.exist;
  });

  it('replaces the loading container with the icon image when the promise is resolved', async function () {
    this.set(
      'icon',
      new Promise((resolve) => {
        setTimeout(
          () =>
            resolve({
              name: 'name',
              image: { value: 'image', useParent: false },
            }),
          200,
        );
      }),
    );

    await render(hbs`<Icon @src={{this.icon}} />`);
    await waitFor('.loading-icon');
    await waitFor('img');

    expect(this.element.querySelector('.fa-circle-question')).not.to.exist;
    expect(this.element.querySelector('.loading-icon')).not.to.exist;
  });

  it('replaces the loading container with the missing icon when the promise is rejected', async function () {
    let iconReject;

    this.set('icon', {
      then(resolve, reject) {
        iconReject = reject;
      },
    });

    await render(hbs`<Icon @src={{this.icon}} />`);
    await waitFor('.loading-icon');

    iconReject();
    await waitFor('.fa-circle-question');

    expect(this.element.querySelector('img')).not.to.exist;
    expect(this.element.querySelector('.loading-icon')).not.to.exist;
  });

  it('renders the options when set', async function (a) {
    this.set('icon', {
      id: 'icon-id',
      name: 'test icon',
      localName: 'test icon',
      image: { value: 'image', useParent: false },
    });

    this.set('options', {
      style: { sm: { height: '1px' }, md: { height: '2px' }, lg: { height: '3px' } },
      classes: ['mt-5'],
      destination: { path: '/login' },
    });

    await render(hbs`<Icon @src={{this.icon}} @options={{this.options}} />`);

    expect(this.element.querySelector('.icon-container')).to.have.class('mt-5');
    expect(this.element.querySelector('a')).to.have.attribute('href', '/login?s=if@icon-id');
  });

  it('replaces the icon-id variable in the destination', async function (a) {
    this.set('icon', {
      id: 'icon-id',
      name: 'test icon',
      localName: 'test icon',
      image: { value: 'image', useParent: false },
    });

    this.set('options', {
      style: { sm: { height: '1px' }, md: { height: '2px' }, lg: { height: '3px' } },
      classes: ['mt-5'],
      destination: { path: '/browse/icon/:icon-id' },
    });

    await render(hbs`<Icon @src={{this.icon}} @options={{this.options}} />`);

    expect(this.element.querySelector('a')).to.have.attribute('href', '/browse/icon/icon-id');
  });
});
