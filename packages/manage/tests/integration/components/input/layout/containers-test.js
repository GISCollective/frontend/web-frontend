/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { LayoutContainer } from 'models/transforms/layout-container-list';

describe('Integration | Component | input/layout/containers', function (hooks) {
  let page;

  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    page = {
      layoutContainers: [
        new LayoutContainer({
          rows: [{ cols: [{ type: 'type', options: [] }], options: [] }],
          options: [],
        }),
      ],
    };
    this.set('page', page);
  });

  it('renders a container with a row and col', async function () {
    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onRowEdit={{this.onRowEdit}} />`);

    expect(this.element.querySelector('.container')).to.exist;
    expect(this.element.querySelector('.container .row')).to.exist;
    expect(this.element.querySelector('.container .row .col')).to.exist;
  });

  it('can add a new col', async function () {
    let value;
    page.addLayout = (a, b, c) => {
      value = [a, b, c];
    };
    this.set('page', page);

    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onRowEdit={{this.onRowEdit}} />`);

    await click('.row');
    await click('.btn-add-col');

    expect(value).to.deep.equal([0, 0, 1]);
  });

  it('can select a col', async function () {
    let type;
    let location;
    let row;

    this.set('onSelect', (t, l, c) => {
      type = t;
      location = l;
      row = c;
    });

    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onSelect={{this.onSelect}} @onRowEdit={{this.onRowEdit}} />`);

    await click('.row');
    await click('.btn-col-options');

    expect(type).to.equal('col');
    expect(location).to.deep.equal([0, 0, 0]);
    expect(row).to.equal(page.layoutContainers[0].rows[0].cols[0]);
  });

  it('can select a row', async function () {
    let type;
    let location;
    let row;

    this.set('onSelect', (t, l, c) => {
      type = t;
      location = l;
      row = c;
    });

    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onSelect={{this.onSelect}} @onRowEdit={{this.onRowEdit}} />`);

    await click('.btn-edit-row');

    expect(type).to.equal('row');
    expect(location).to.deep.equal([0, 0]);
    expect(row).to.equal(page.layoutContainers[0].rows[0]);
  });

  it('can add a new container at the end', async function () {
    let value;

    page.addLayout = (a, b, c) => {
      value = [a, b, c];
    };
    this.set('page', page);

    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onRowEdit={{this.onRowEdit}} />`);

    await click('.btn-add-container-1');

    expect(value).to.deep.equal([1, +0, +0]);
  });

  it('can add a new container on top', async function () {
    let newLayoutValue;
    page.addLayout = (a, b, c) => {
      newLayoutValue = [a, b, c];
    };

    let containerValue;
    page.addLayoutContainerBefore = (a) => {
      containerValue = a;
    };

    this.set('page', page);

    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onRowEdit={{this.onRowEdit}} />`);

    await click('.btn-add-container-0');

    expect(containerValue).to.equal(0);
    expect(newLayoutValue).to.deep.equal([0, 0, 0]);
  });

  it('can remove a container', async function () {
    let value;

    page.deleteContainer = (a) => {
      value = a;
    };
    this.set('page', page);

    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onRowEdit={{this.onRowEdit}} />`);

    await click('.btn-remove-container-0');

    expect(value).to.deep.equal(0);
  });

  it('triggers an event when the config button is pressed', async function () {
    let type;
    let location;
    let row;

    this.set('onSelect', (t, l, c) => {
      type = t;
      location = l;
      row = c;
    });

    await render(hbs`
    <Input::Layout::Containers @page={{this.page}} @onSelect={{this.onSelect}} />`);

    await click('.btn-config-container-0');

    expect(type).to.equal('container');
    expect(location).to.deep.equal([0]);
    expect(row).to.equal(page.layoutContainers[0]);
  });
});
