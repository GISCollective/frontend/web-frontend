/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent, waitUntil, click } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/picture-with-options", function (hooks) {
  setupRenderingTest(hooks);

  let picture;
  let server;
  let receivedPicture;
  let pageData;

  hooks.before(function () {
    server = new TestServer();

    pageData = server.testData.storage.addDefaultPage();

    receivedPicture = null;
    server.post("/mock-server/pictures", (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = "1";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedPicture)];
    });

    server.put("/mock-server/pictures/5cc8dc1038e882010061545a", (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = "5cc8dc1038e882010061545a";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedPicture)];
    });

    picture = server.testData.storage.addDefaultPicture();
  });

  hooks.beforeEach(function () {
    receivedPicture = null;
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("allows to set a proportional size", async function () {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::PictureWithOptions @onChange={{this.onChange}}/>`);

    this.element.querySelector(".picture-sizing").value = "proportional";
    await triggerEvent(".picture-sizing", "change");

    await fillIn(".picture-proportion", "16:9");

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      size: {
        sizing: "proportional",
        proportion: "16:9",
      },
      container: {},
    });
  });

  it("allows to set a fixed width size", async function () {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    this.set("value", {});

    await render(hbs`<Editor::PictureWithOptions @value={{this.value}} @onChange={{this.onChange}}/>`);

    this.element.querySelector(".picture-sizing").value = "fixed width";
    await triggerEvent(".picture-sizing", "change");

    await fillIn(".sm-size-px .input-px-size", "100");
    await fillIn(".md-size-px .input-px-size", "200");
    await fillIn(".lg-size-px .input-px-size", "300");

    await waitUntil(() => value);

    expect(value).to.deep.equal({
      size: {
        sizing: "fixed width",
        customStyle: {
          sm: { width: "100px", height: "auto" },
          md: { height: "auto", width: "200px" },
          lg: { height: "auto", width: "300px" },
        },
        containerClasses: [],
      },
      container: {},
    });
  });

  it("allows to set a fixed height size", async function () {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    this.set("value", {});

    await render(hbs`<Editor::PictureWithOptions @value={{this.value}} @onChange={{this.onChange}}/>`);

    this.element.querySelector(".picture-sizing").value = "fixed height";
    await triggerEvent(".picture-sizing", "change");

    await fillIn(".sm-size-px .input-px-size", "100");
    await fillIn(".md-size-px .input-px-size", "200");
    await fillIn(".lg-size-px .input-px-size", "300");

    await waitUntil(() => value);

    expect(value).to.deep.equal({
      size: {
        sizing: "fixed height",
        customStyle: {
          sm: { height: "100px", width: "auto" },
          md: { width: "auto", height: "200px" },
          lg: { width: "auto", height: "300px" },
        },
        containerClasses: [],
      },
      container: {},
    });
  });

  it("shows the fixed height size options", async function () {
    this.set("value", {
      size: {
        sizing: "fixed height",
        customStyle: {
          sm: { height: "100px", width: "auto" },
          md: { width: "auto", height: "200px" },
          lg: { width: "auto", height: "300px" },
        },
        containerClasses: ["text-center"],
      },
    });

    await render(hbs`<Editor::PictureWithOptions @value={{this.value}} />`);

    expect(this.element.querySelector(".picture-sizing").value).to.equal("fixed height");
    expect(this.element.querySelector(".sm-size-px .input-px-size").value).to.equal("100");
    expect(this.element.querySelector(".md-size-px .input-px-size").value).to.equal("200");
    expect(this.element.querySelector(".lg-size-px .input-px-size").value).to.equal("300");
  });

  it("allows to set a fill size", async function () {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::PictureWithOptions @onChange={{this.onChange}}/>`);

    this.element.querySelector(".picture-sizing").value = "fill";
    await triggerEvent(".picture-sizing", "change");

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      size: {
        sizing: "fill",
      },
      container: {},
    });
  });

  it("allows to set the border properties", async function () {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::PictureWithOptions @onChange={{this.onChange}}/>`);

    this.element.querySelector(".input-sm-border-visibility").value = "yes";
    await triggerEvent(".input-sm-border-visibility", "change");

    this.element.querySelector(".input-sm-border-size").value = "3";
    await triggerEvent(".input-sm-border-size", "change");

    this.element.querySelector(".input-sm-border-radius").value = "4";
    await triggerEvent(".input-sm-border-radius", "change");

    await click(".input-border-color .btn-color-yellow");

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      size: {},
      container: {
        classes: ["border-visibility-yes", "border-size-3", "border-radius-4", "border-color-yellow"],
      },
    });
  });

  it("allows to set the container properties", async function () {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::PictureWithOptions @onChange={{this.onChange}}/>`);

    await click(".container-section .bs-color-picker");
    await click(".container-section .btn-color-warning");

    await waitUntil(() => value);

    expect(value.container).to.deep.equal({ color: "warning" });
  });
});
