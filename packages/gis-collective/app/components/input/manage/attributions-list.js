/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageAttributionsListComponent extends Component {
  @tracked _list = null;

  get list() {
    if (this._list === null) {
      return this.args.list;
    }

    return this._list;
  }

  set list(value) {
    this._list = value;
  }

  @action
  save() {
    this.args.onSave?.(this._list);
  }

  @action
  remove(index) {
    const value = this.list.splice(index, 1);

    this.list = value;
  }

  @action
  cancel() {
    this._list = null;
    this.args.onCancel();
  }

  @action
  addItem() {
    const value = this.list;
    value.push({
      name: '',
      url: '',
    });
    this.list = value;
  }
}
