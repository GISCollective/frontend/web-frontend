/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { toValidHash } from 'core/lib/slug';
import { replaceVars } from 'models/lib/path';

export default class CardCoverTitleDescriptionComponent extends Component {
  @service clickOutside;
  @service fastboot;
  @tracked isAdminView;

  deferCoverRendering() {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    let cover = this.args.record?.get?.('cover');

    if (cover?.then) {
      this.fastboot?.deferRendering?.(cover);
    }
  }

  deferParentCoverRendering() {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    let cover = this.args.record?.get?.('cover');

    if (cover?.then) {
      this.fastboot?.deferRendering?.(cover);
    }
  }

  get cover() {
    if (this.args.record?.get) {
      this.deferCoverRendering();
      return this.args.record?.get?.('cover');
    }

    return this.args.record?.cover;
  }

  get parentCover() {
    if (this.args.record?.get) {
      this.deferParentCoverRendering();
      return this.args.record?.get?.('parentCover');
    }

    return this.args.record?.parentCover;
  }

  get picture() {
    let cover = this.cover;

    const name = cover?.get?.('name') ?? cover?.name ?? '';

    const defaultCover = this.args.value?.data?.card?.cover?.defaultCover ?? this.pictureOptions?.defaultCover;

    const showParentCover = defaultCover == 'from parent';

    if (name == 'default' && showParentCover) {
      cover = this.parentCover;
    }

    return cover;
  }

  get destination() {
    const data = this.args.value?.data;
    let destination = data?.card?.destination ?? data?.destination?.slug ?? data?.destination;

    if (typeof destination != 'string') {
      return null;
    }

    let modelName = this.args.record?.get?.('constructor.modelName') ?? '';
    let withHash = destination.indexOf(`:${modelName}-id`) == -1;

    if (destination && withHash && this.args.record?.id) {
      destination += `#${toValidHash(this.args.record.title)}`;
    }

    return replaceVars(modelName, destination, this.args.record);
  }

  get pictureOptions() {
    const result = this.args.value?.data?.picture ?? {};

    if (!result.sizing) {
      result.sizing = 'proportional';
    }

    return result;
  }

  get id() {
    return toValidHash(this.args.record?.title);
  }

  @action
  linkSetup(element) {
    this.link = element;
  }

  @action
  transitionToCampaign() {
    if (this.args.isEditor) {
      return;
    }

    return this.link.click();
  }

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showAdmin() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.hideAdmin();
    });

    this.isAdminView = true;
  }

  @action
  hideAdmin() {
    this.isAdminView = false;
  }
}
