/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { A } from 'core/lib/array';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/card-team', function (hooks) {
  setupRenderingTest(hooks);
  let teamData;
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    teamData = server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPicture();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no record', async function (assert) {
    assert.dom(this.element).hasText('');
  });

  it('renders the team data when there is a record', async function (assert) {
    this.set('team', await this.store.findRecord('team', teamData._id));
    await this.team.updateCurrentUserRole();

    await render(hbs`<Manage::CardTeam @record={{this.team}}/>`);

    expect(this.element.querySelector('.name')).to.have.textContent('Open Green Map');
    expect(this.element.querySelector('.role')).to.have.textContent('unknown');
    expect(this.element.querySelector('.description')).to.have.textContent('test description');
    expect(this.element.querySelector('.action-container a')).to.have.attribute(
      'href',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    );
  });

  it('renders a custom message when there is a record without a description', async function (assert) {
    this.set('team', await this.store.findRecord('team', teamData._id));
    await this.team.updateCurrentUserRole();

    this.team.about = '';

    await render(hbs`<Manage::CardTeam @record={{this.team}}/>`);

    expect(this.element.querySelector('.description')).to.have.textContent(
      'The team has no description outlining its members, activities, and goals.',
    );
  });

  it('renders the dropdown when the team is editable', async function (assert) {
    this.set('team', await this.store.findRecord('team', teamData._id));
    await this.team.updateCurrentUserRole();

    this.team.canEdit = true;

    await render(hbs`<Manage::CardTeam @record={{this.team}}/>`);

    expect(this.element.querySelector('.btn-open-options')).to.exist;
  });

  it('does not render he options when the team is not editable', async function (assert) {
    this.set('team', await this.store.findRecord('team', teamData._id));
    await this.team.updateCurrentUserRole();

    this.team.canEdit = false;

    await render(hbs`<Manage::CardTeam @record={{this.team}}/>`);

    expect(this.element.querySelector('.btn-open-options')).not.to.exist;
  });
});
