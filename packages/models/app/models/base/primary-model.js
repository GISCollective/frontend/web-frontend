/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model from '@ember-data/model';
import { attr } from '@ember-data/model';

export default class PrimaryModel extends Model {
  @attr('visibility') visibility;
  @attr('model-info') info;
  @attr('boolean') canEdit;

  unpublish() {
    this.visibility.isPublic = false;

    return this.save();
  }

  publish() {
    this.visibility.isPublic = true;

    return this.save();
  }

  deleteMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.deleteMany(...arguments);
  }

  publishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.publishMany(...arguments);
  }

  unpublishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.unpublishMany(...arguments);
  }

  get isPublished() {
    return this.visibility.isPublic;
  }

  get availableActions() {
    return [this.isPublished ? 'unpublish' : 'publish'];
  }
}
