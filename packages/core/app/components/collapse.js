/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';

export default class CollapseComponent extends Component {
  @tracked isShowing;

  elementId = 'collapse-' + guidFor(this);

  @action
  toggle() {
    this.isShowing = !this.isShowing;
  }
}
