/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitFor, waitUntil } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/map', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let space;
  let team;
  let map;
  let feature;
  let updatedFeature;

  hooks.beforeEach(function () {
    server = new TestServer();
    team = server.testData.create.team();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    space = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultBaseMap();
    map = server.testData.storage.addDefaultMap();
    map.canEdit = true;

    feature = server.testData.storage.addDefaultFeature();
    feature.canEdit = true;

    updatedFeature = null;
    server.put('/mock-server/features/:id', (request) => {
      updatedFeature = JSON.parse(request.requestBody);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(updatedFeature)];
    });

    const notifications = this.owner.lookup('service:notifications');
    notifications.ask = () => 'yes';

    const service = this.owner.lookup('service:modal');
    service.confirm = () => 'yes';
  });

  hooks.afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('redirects to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/map/${map._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2Fmap%2F5ca89e37ef1f7e010007f54c');
  });

  it('shows the dashboard page when the user is authenticated', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('header').textContent.trim()).to.contain(map.name);
  });

  it('displays a public feature as a card', async function () {
    feature.visibility = 1;
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    expect(this.element.querySelectorAll('.manage-card-feature').length).to.equal(1);
    expect(this.element.querySelector('.manage-card-feature .name').textContent.trim()).to.equal(
      'Nomadisch Grün - Local Urban Food',
    );
    expect(this.element.querySelector('.badge.text-bg-warning')).not.to.exist;
    expect(this.element.querySelector('.badge.text-bg-info')).not.to.exist;
    expect(this.element.querySelector('.badge.text-bg-success')).to.exist;
  });

  it('displays a private feature as a card', async function () {
    feature.visibility = 0;
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    expect(this.element.querySelectorAll('.manage-card-feature').length).to.equal(1);
    expect(this.element.querySelector('.manage-card-feature .name').textContent.trim()).to.equal(
      'Nomadisch Grün - Local Urban Food',
    );
    expect(this.element.querySelector('.badge.text-bg-warning')).to.exist;
    expect(this.element.querySelector('.badge.text-bg-info')).not.to.exist;
    expect(this.element.querySelector('.badge.text-bg-success')).not.to.exist;
  });

  it('displays a pending feature as a card', async function () {
    feature.visibility = -1;
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    expect(this.element.querySelectorAll('.manage-card-feature').length).to.equal(1);
    expect(this.element.querySelector('.manage-card-feature .name').textContent.trim()).to.equal(
      'Nomadisch Grün - Local Urban Food',
    );
    expect(this.element.querySelector('.badge.text-bg-warning')).not.to.exist;
    expect(this.element.querySelector('.badge.text-bg-info')).to.exist;
    expect(this.element.querySelector('.badge.text-bg-success')).not.to.exist;
  });

  it('displays the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Harta Verde București']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    ]);
  });

  it('can edit the map', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    await click('.btn-map-edit');

    expect(currentURL()).to.equal('/manage/maps/5ca89e37ef1f7e010007f54c');
  });

  it('can access a map feature', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    await click('.manage-card-feature .btn-main-action');

    expect(currentURL()).to.equal('/manage/features/edit/5ca78e2160780601008f69e6');
  });

  it('can delete a map feature', async function () {
    const service = this.owner.lookup('service:notifications');
    service.ask = () => 'yes';

    let deleted;

    server.server.delete(`/mock-server/features/5ca78e2160780601008f69e6`, () => {
      deleted = true;
      return [204, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    await click('.btn-open-options');
    await click('.btn-delete');

    expect(deleted).to.equal(true);
  });

  it('can delete a feature by selecting it', async function () {
    let deleted;

    server.server.delete(`/mock-server/features/5ca78e2160780601008f69e6`, () => {
      deleted = true;
      return [204, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    const checkboxes = this.element.querySelectorAll('.manage-card-feature input');
    await click(checkboxes[0]);

    await click('.navbar .btn-delete');

    expect(deleted).to.equal(true);
  });

  it('can unpublish a feature by selecting it', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    const checkboxes = this.element.querySelectorAll('.manage-card-feature input');
    await click(checkboxes[0]);

    await click('.navbar .btn-unpublish');

    await waitUntil(() => updatedFeature);

    expect(updatedFeature.feature.visibility).to.equal(0);
  });

  it('can publish a feature by selecting it', async function () {
    feature.visibility = -1;
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    const checkboxes = this.element.querySelectorAll('.manage-card-feature input');
    await click(checkboxes[0]);

    await click('.navbar .btn-publish');

    await waitUntil(() => updatedFeature);

    expect(updatedFeature.feature.visibility).to.equal(1);
  });

  it('can set to pending a feature by selecting it', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    const checkboxes = this.element.querySelectorAll('.manage-card-feature input');
    await click(checkboxes[0]);

    await click('.navbar .btn-pending');

    await waitUntil(() => updatedFeature);

    expect(updatedFeature.feature.visibility).to.equal(-1);
  });

  it('can add a feature on the map', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/map/${map._id}`);

    await click('.btn-add-feature');

    expect(currentURL()).to.equal(`/manage/features/add?mapId=${map._id}`);
  });

  describe('when there are 2 features', function (hooks) {
    let otherFeature;
    let deleteAll;
    let publishAll;
    let unpublishAll;
    let pendingAll;

    hooks.beforeEach(function () {
      otherFeature = server.testData.storage.addDefaultFeature('2');
      otherFeature.canEdit = true;
      otherFeature.visibility = 1;

      deleteAll = null;
      server.delete_('/mock-server/features', (req) => {
        deleteAll = req.queryParams;
        return [204];
      });

      publishAll = null;
      server.post('/mock-server/features/publish', (req) => {
        publishAll = req.queryParams;
        return [204];
      });

      unpublishAll = null;
      server.post('/mock-server/features/unpublish', (req) => {
        unpublishAll = req.queryParams;
        return [204];
      });

      pendingAll = null;
      server.post('/mock-server/features/pending', (req) => {
        pendingAll = req.queryParams;
        return [204];
      });
    });

    it('can delete all features', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/map/${map._id}`);

      await click('.chk-select-all');
      await click('.navbar .btn-delete');

      await waitUntil(() => deleteAll);

      expect(deleteAll).to.deep.equal({ map: '5ca89e37ef1f7e010007f54c' });
    });

    it('can unpublish all features', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/map/${map._id}`);

      await click('.chk-select-all');
      await click('.btn-unpublish');

      expect(unpublishAll).to.deep.equal({ map: '5ca89e37ef1f7e010007f54c' });
    });

    it('can publish all features', async function () {
      otherFeature.visibility = 0;

      authenticateSession();

      await visit(`/manage/dashboards/map/${map._id}`);

      await click('.chk-select-all');
      await click('.btn-publish');

      expect(publishAll).to.deep.equal({ map: '5ca89e37ef1f7e010007f54c' });
    });

    it('can set to pending all features', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/map/${map._id}`);

      await click('.chk-select-all');
      await click('.btn-pending');

      expect(pendingAll).to.deep.equal({ map: '5ca89e37ef1f7e010007f54c' });
    });
  });

  describe('the reports section', function (hooks) {
    hooks.beforeEach(function () {
      const metrics = {};

      for (let i = 1; i < 10; i++) {
        metrics[`${i}`] = {
          _id: `${i}`,
          name: '1',
          reporter: '',
          labels: {},
          time: `2020-12-0${i}T00:00:00Z`,
          type: 'mapView',
          value: i,
        };
      }

      server.testData.storage['metrics'] = metrics;
    });

    it('is hidden when the team has no access to reports', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/map/${map._id}`);

      expect(this.element.querySelector('.manage-map-reports')).not.to.exist;
    });

    it('is shown when the team has access to reports', async function () {
      team.allowReports = true;

      authenticateSession();

      await visit(`/manage/dashboards/map/${map._id}`);

      expect(this.element.querySelector('.manage-map-reports')).to.exist;
    });

    it('is shown when the user is admin', async function () {
      server.testData.storage.addDefaultUser(true);

      authenticateSession();

      await visit(`/manage/dashboards/map/${map._id}`);

      expect(this.element.querySelector('.manage-map-reports')).to.exist;
    });
  });
});
