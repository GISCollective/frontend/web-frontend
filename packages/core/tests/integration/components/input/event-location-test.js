/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { expect } from 'chai';
import { hbs } from 'ember-cli-htmlbars';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | input/event-location', function (hooks) {
  setupRenderingTest(hooks);
  let value;
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    const feature1 = server.testData.storage.addDefaultFeature('1');
    feature1.name = 'feature 1';

    const feature2 = server.testData.storage.addDefaultFeature('2');
    feature2.name = 'feature 2';

    value = undefined;

    this.set('change', function (v) {
      value = v;
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders an empty dropdown by default', async function (a) {
    await render(hbs`<Input::EventLocation />`);

    expect(this.element.querySelector('.input-event-location')).to.exist;
  });

  it('renders a text value', async function (a) {
    this.set('value', {
      type: 'Text',
      value: 'some place in our town',
    });

    await render(hbs`<Input::EventLocation @value={{this.value}}/>`);

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal(
      'some place in our town (as text)',
    );
  });

  it('sets a new text value while the search term is written', async function (a) {
    this.set('value', {
      type: 'Text',
      value: 'some place in our town',
    });

    await render(hbs`<Input::EventLocation @value={{this.value}} @onChange={{this.change}}/>`);

    await selectSearch('.input-event-location', 'a new text value');

    a.deepEqual(value, {
      type: 'Text',
      value: 'a new text value',
    });
  });

  it('sets a new text value when the search term is selected', async function (a) {
    this.set('value', {
      type: 'Text',
      value: 'some place in our town',
    });

    await render(hbs`<Input::EventLocation @value={{this.value}} @onChange={{this.change}}/>`);

    await selectSearch('.input-event-location', 'a new text value');
    await selectChoose('.input-event-location', 'a new text value');

    a.deepEqual(value, {
      type: 'Text',
      value: 'a new text value',
    });
  });

  it('searches for a feature by name without a map when it is not set', async function (a) {
    this.set('value', {
      type: 'Text',
      value: 'some place in our town',
    });

    await render(hbs`<Input::EventLocation @value={{this.value}} @onChange={{this.change}}/>`);

    await selectSearch('.input-event-location', 'some feature name');
    await selectChoose('.input-event-location', 'feature 1');

    a.deepEqual(server.history, ['GET /mock-server/features?term=some%20feature%20name']);

    a.deepEqual(value, {
      type: 'Feature',
      value: '1',
    });
  });

  it('searches for a feature by name with a map when it is set', async function (a) {
    this.set('value', {
      type: 'Text',
      value: 'some place in our town',
    });

    this.set('map', {
      id: 'map-id',
    });

    await render(hbs`<Input::EventLocation @map={{this.map}} @value={{this.value}} @onChange={{this.change}}/>`);

    await selectSearch('.input-event-location', 'some feature name');
    await selectChoose('.input-event-location', 'feature 1');

    a.deepEqual(server.history, ['GET /mock-server/features?map=map-id&term=some%20feature%20name']);

    a.deepEqual(value, {
      type: 'Feature',
      value: '1',
    });
  });
});
