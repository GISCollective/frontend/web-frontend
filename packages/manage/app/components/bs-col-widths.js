/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { sizedBsProperty } from 'core/lib/bs-cols';

export default class BsColWidthsComponent extends Component {
  get options() {
    return this.args.col?.options ?? [];
  }

  get name() {
    let name = this.args.col?.name ?? '';

    const newLocal = this.args?.containerIndex ?? '?';
    const row = this.args?.rowIndex ?? '?';
    const col = this.args?.colIndex ?? '?';

    const position = `${newLocal}.${row}.${col}`;

    return name ? `${name} (${position})` : position;
  }

  get tabletSize() {
    return this.getValue('md');
  }

  get desktopSize() {
    return this.getValue('lg');
  }

  get mobileSize() {
    return this.getValue();
  }

  getValue(size) {
    const colSize = sizedBsProperty('col', size, this.options);
    if (!isNaN(colSize)) {
      return colSize;
    }

    const pxSize = sizedBsProperty('wpx', size, this.options);
    if (!isNaN(pxSize)) {
      return `${pxSize}px`;
    }

    return `-`;
  }
}
