/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './selectable-list';

export default class ManageSelectableListGroupComponent extends Component {
  get allValues() {
    if (this.hashValue) {
      const value = this.hashValue;
      return Object.keys(value).flatMap((a) => value[a]);
    }

    return Object.values(this.listValue || {})?.flatMap((a) => a.records) ?? [];
  }

  get listValue() {
    if (!Array.isArray(this.args.value)) {
      return null;
    }

    return this.args.value;
  }

  get hashValue() {
    if (Array.isArray(this.args.value)) {
      return null;
    }

    return this.args.value;
  }
}
