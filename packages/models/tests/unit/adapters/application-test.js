/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module, test } from 'qunit';
import { expect } from 'chai';

import { setupTest } from 'dummy/tests/helpers';

module('Unit | Adapter | application', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function () {
    let adapter = this.owner.lookup('adapter:application');
    expect(adapter).to.exist;
  });
});
