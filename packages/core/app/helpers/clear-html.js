/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { helper } from '@ember/component/helper';
import sanitizeHtml from 'sanitize-html';

export function clearHtml(params /*, hash*/) {
  return sanitizeHtml(params[0], {
    allowedTags: [],
    allowedAttributes: {},
  });
}

export default helper(clearHtml);
