/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, click } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/background/color", function (hooks) {
  setupRenderingTest(hooks);
  let changedValue;

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    this.set("value", {
      data: {
        background: {
          color: "green",
        },
      },
    });
  });

  it("renders the current value", async function () {
    await render(hbs`<Editor::Background::Color @value={{this.value}} />`);

    expect(this.element.querySelector(".selected-color").textContent.trim()).to.equal("green");
  });

  it("can change the color", async function () {
    this.set("value", {
      data: {
        background: {
          color: "green",
        },
      },
    });

    await render(hbs`<Editor::Background::Color @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".btn-color-blue");

    expect(changedValue).to.deep.equal({
      background: {
        color: "blue",
      },
    });
  });
});
