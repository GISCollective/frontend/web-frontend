/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import {
  visit,
  currentURL,
  click,
  fillIn,
} from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe ('Acceptance | add/space',  function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedSpace;
  let space;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/spaces/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fspaces%2Fadd');
  });

  describe ('when an user is authenticated and no team is available',  function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/spaces/add`);

      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'New space'
      );

      expect(
        this.element.querySelector('.row-name label').textContent.trim()
      ).to.contain('Name');

      expect(this.element.querySelector('.row-name input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-domain label').textContent.trim()
      ).to.contain('Domain');
      expect(this.element.querySelector('.row-domain input.form-control')).to
        .exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.btn-add-team')).to.exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit(`/manage/spaces/add`);
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.page');
    });
  });

  describe ('when an user is authenticated and a space is available',  function (hooks) {
    hooks.beforeEach(function () {
      space = server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultUser();
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit(`/manage/spaces/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Dashboard',
        'New space',
      ]);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
      ]);
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/spaces/add`);

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');
    });

    describe ('when the server request is successful',  function (hooks) {
      hooks.beforeEach(function () {
        space = server.testData.storage.addDefaultSpace();
        server.testData.storage.addDefaultSpace('000000000000000000000001');
        server.testData.storage.addDefaultTeam('000000000000000000000001');
        server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');

        server.post('/mock-server/spaces', (request) => {
          receivedSpace = JSON.parse(request.requestBody);
          receivedSpace.space._id = 'new-id';
          server.testData.storage.addSpace(receivedSpace.space);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedSpace),
          ];
        });
      });

      it('redirects to the edit page after the request response is received', async function () {
        authenticateSession();

        await visit(`/manage/spaces/add`);

        await fillIn('.input-name', 'new page name');
        await fillIn('.input-space-subdomain', 'newdomain');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedSpace.space.name).to.equal('new page name');
        expect(receivedSpace.space.domain).to.equal(
          'newdomain.giscollective.com'
        );
        expect(receivedSpace.space.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '000000000000000000000001',
        });

        expect(currentURL()).to.equal(`/manage/spaces/edit/new-id`);
      });
    });

    describe ('when the server request fails',  function (hooks) {
      hooks.beforeEach(function () {
        server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');
        server.post('/mock-server/spaces', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description: 'The `space.domain` field is required.',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function (a) {
        const service = this.owner.lookup('service:notifications');

        authenticateSession();

        await visit(`/manage/spaces/add`);

        await fillIn('.input-name', 'new page name');
        await fillIn('.input-space-subdomain', 'newdomain');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/manage/spaces/add`);

        a.equal(service.lastCall[0], 'handleError');
        a.equal(
          service.lastCall[1].message,
          'Ember Data Request POST /mock-server/spaces returned a 400\nPayload (application/json)\n[object Object]',
        );
      });
    });
  });

  describe ('when an admin user is authenticated',  function (hooks) {
    hooks.beforeEach(() => {
      server.testData.storage.addDefaultUser(true);
      server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/spaces/add`);
      expect(currentURL()).to.equal('/manage/spaces/add');
    });

    describe ('when the server request is successfully',  function (hooks) {
      hooks.beforeEach(function () {
        server.testData.storage.addDefaultTeam('1');
        server.testData.storage.addDefaultPage('000000000000000000000001');
        server.testData.storage.addDefaultTeam('2');

        space = server.testData.storage.addDefaultSpace();
        server.post('/mock-server/spaces', (request) => {
          receivedSpace = JSON.parse(request.requestBody);
          receivedSpace.space._id = space._id;

          server.testData.storage.addSpace(receivedSpace.space);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedSpace),
          ];
        });
      });

      it('queries the spaces without the all query param', async function () {
        authenticateSession();

        await visit(`/manage/spaces/add`);

        await fillIn('.input-name', 'new page name');
        await fillIn('.input-space-subdomain', 'newdomain');

        await PageElements.wait(1000);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click(this.element.querySelector('.btn.btn.btn-submit'));

        expect(receivedSpace.space.name).to.deep.equal('new page name');
        expect(currentURL()).to.equal(
          `/manage/spaces/edit/6227c131624b2cf1626dd029`
        );
      });

      it('queries the teams with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/manage/spaces/add`);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain('GET /mock-server/teams?all=true');
      });
    });
  });
});
