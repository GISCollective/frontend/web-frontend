/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { CampaignLocationQuestion } from 'dummy/transforms/campaign-questions';
import Service from '@ember/service';

class MockPosition extends Service {
  hasPosition = true;
  center = [0, 0];
  hasGeolocationSupport = false;
}

describe('Integration | Component | attributes/group-position-type', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Attributes::GroupPositionType />`);

    expect(this.element.querySelector('label').textContent.trim()).to.equal('How do you want to select the location?');
  });

  it('allows to change the position type', async function () {
    let value;

    this.set(
      'options',
      new CampaignLocationQuestion({
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      }),
    );

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Attributes::GroupPositionType @options={{this.options}} @onChange={{this.onChange}}/>`);

    await click('.btn-gps');

    expect(value).to.equal('gps');
  });

  it('renders all options when all allow options are true', async function () {
    this.set(
      'options',
      new CampaignLocationQuestion({
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      }),
    );

    await render(hbs`<Attributes::GroupPositionType @options={{this.options}} />`);

    const options = [...this.element.querySelectorAll('.btn-location-option')].map((a) => a.textContent.trim());

    expect(options).to.deep.equal([
      'use my current location',
      'choose manually on the map',
      'by address',
      'select an existing feature',
    ]);
  });

  it('renders no options when all allow options are false', async function () {
    this.set(
      'options',
      new CampaignLocationQuestion({
        allowGps: false,
        allowManual: false,
        allowAddress: false,
        allowExistingFeature: false,
      }),
    );

    await render(hbs`<Attributes::GroupPositionType @options={{this.options}} />`);

    const options = [...this.element.querySelectorAll('.btn-location-option')].map((a) => a.textContent.trim());

    expect(options).to.deep.equal([]);
  });

  it('renders the selected type value', async function () {
    this.set('details', {
      type: 'gps',
    });

    this.set(
      'options',
      new CampaignLocationQuestion({
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      }),
    );

    await render(hbs`<Attributes::GroupPositionType @options={{this.options}} @details={{this.details}}/>`);

    expect(this.element.querySelector('.btn-success').textContent.trim()).to.equal('use my current location');
  });

  it('renders a disabled gps value when the gps is not available', async function () {
    this.owner.register('service:position', MockPosition);

    this.set('details', {
      type: 'manual',
    });

    this.set(
      'options',
      new CampaignLocationQuestion({
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: true,
      }),
    );

    await render(hbs`<Attributes::GroupPositionType @options={{this.options}} @details={{this.details}}/>`);

    expect(this.element.querySelector('.btn-location-option')).to.have.attribute('disabled', '');
  });
});
