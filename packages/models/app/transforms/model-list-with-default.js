/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { getOwner } from '@ember/application';
import { A } from 'models/lib/array';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class ModelListWithDefaultTransform extends Transform {
  deserialize(serialized, options) {
    const store = getOwner(this).lookup('service:store');

    return new ModelListWithDefault(serialized, store, options.model);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class ModelListWithDefault {
  @tracked useCustomList = false;
  @tracked _list = A([]);
  idList = A([]);
  model = '';

  store;

  constructor(serialized, store, model) {
    this.store = store;
    this.model = model;

    this.useCustomList = false;
    this.idList = A([]);

    if (!serialized) {
      return;
    }

    if (serialized.useCustomList) {
      this.useCustomList = serialized.useCustomList;
    }

    if (serialized.list) {
      this.idList = serialized.list;
    }
  }

  get list() {
    if (this._list.length != this.idList.length) {
      this.fetch(this.idList);
    }

    return this._list;
  }

  set list(value) {
    this._list = A(value);
  }

  get promiseList() {
    const result = [];

    for (let id of this.idList) {
      if (!id) {
        continue;
      }

      let item = this.store.peekRecord(this.model, id);

      if (!item) {
        item = this.store.findRecord(this.model, id, { reload: true });
      }

      result.push(item);
    }

    return result;
  }

  async fetch() {
    const resolved = await Promise.allSettled(this.promiseList);

    return new Promise((resolve) => {
      later(() => {
        this._list = A(resolved.filter((a) => a.status == 'fulfilled').map((a) => a.value));

        resolve(this._list);
      });
    });
  }

  toJSON() {
    let list = this.idList;

    if (this._list) {
      list = this._list.map((a) => a.get('id'));
    }

    return {
      useCustomList: this.useCustomList,
      list,
    };
  }
}
