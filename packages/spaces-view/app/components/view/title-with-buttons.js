/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class TitleWithButtonsComponent extends Component {
  @tracked picture;
  @service store;

  get paragraphAlign() {
    const classes = this.args.value?.data?.paragraph?.classes ?? [];

    return classes.filter((a) => a.indexOf('text-') == 0);
  }

  get hasPicture() {
    return this.picture || this.args.value?.data?.source?.id;
  }

  @action
  async setup() {
    if (this.args.value?.data?.source) {
      return;
    }

    if (this.args.value?.data?.picture?.id) {
      this.args.value.data.source = {
        id: this.args.value.data.picture.id,
        model: 'picture',
      };

      this.picture = this.args.value.data.source;
      this.args.value.data.picture.id = undefined;
    }
  }
}
