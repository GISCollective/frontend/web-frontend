/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import * as olInteraction from 'ol/interaction';

export default class MapInteractionDefaultsComponent extends Component {
  @action
  setupOl() {
    if (!this.args.map) {
      return;
    }

    const config = {
      altShiftDragRotate: this.args.altShiftDragRotate,
      onFocusOnly: this.args.onFocusOnly,
      doubleClickZoom: this.args.doubleClickZoom,
      keyboard: this.args.keyboard,
      mouseWheelZoom: this.args.mouseWheelZoom,
      shiftDragZoom: this.args.shiftDragZoom,
      dragPan: this.args.dragPan,
      pinchRotate: this.args.pinchRotate,
      pinchZoom: this.args.pinchZoom,
      zoomDelta: this.args.zoomDelta,
      zoomDuration: this.args.zoomDuration,
    };

    if (!this.interactions) {
      this.interactions = olInteraction.defaults(config);
    }

    this.interactions.forEach((interaction) => {
      this.args.map.addInteraction(interaction);
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (this.interactions && this.args.map) {
      this.interactions.forEach((item) => {
        this.args.map.removeInteraction(item);
      });
    }
  }
}
