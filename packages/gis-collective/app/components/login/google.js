/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class LoginGoogleComponent extends Component {
  @service preferences;
  @service intl;

  @tracked googleClientId;

  @action
  handleLogin(response) {
    this.args.onLogIn(response.credential);
  }

  @action
  async setup() {
    const googleClientIdPreference = await this.preferences.getPreference('integrations.google.client_id');

    this.googleClientId = googleClientIdPreference.value;

    window.handleGoogleCredentialResponse = (response) => {
      return this.handleLogin(response);
    };
  }
}
