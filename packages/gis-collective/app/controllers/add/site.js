/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '../base/map-controller';
import { service } from '@ember/service';
import { transform } from 'ol/proj';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { debounce } from '@ember/runloop';
import { ModelInfo } from '../../transforms/model-info';
import { GeoJson } from 'core/lib/geoJson';
import PositionDetails from '../../lib/positionDetails';
import GeocodingSearch from '../../lib/geocoding-search';
import RequiredAttributes from '../../lib/required-attributes';
import { addIconSection, handleAttributeChange, handlePicturesChange, handleGroupRemove } from '../../attributes/actions';
import { addIconGroupToStore } from '../../lib/icons';
import { later } from '@ember/runloop';

export default class AddSiteController extends Controller {
  queryParams = [
    {
      lon: { type: 'number' },
      lat: { type: 'number' },
      map: { type: 'string' },
      icons: { type: 'string' },
      ignoreGps: { type: 'boolean' },
      preserveScrollPosition: { type: 'boolean' },
    },
  ];

  @service notifications;
  @service position;
  @service store;
  @service intl;
  @service user;
  @service('loading') loadingService;

  @tracked pictureOptimization = false;
  @tracked currentlyLoading = false;
  @tracked map = '';

  @tracked maps = [];
  @tracked iconGroup = {};
  @tracked areMapsLoading;
  @tracked areIconsLoading;
  @tracked ignoreGps;
  @tracked submitDisabled;

  preserveScrollPosition = true;

  constructor() {
    super(...arguments);
    this.geocodingSearch = new GeocodingSearch(this.store);
    this.requiredAttributes = new RequiredAttributes();
  }

  progress(picture, percentage) {
    picture.set('progress', percentage);
  }

  resizePictures() {
    if (!this.model.feature.pictures.pictureOptimization) {
      return Promise.resolve([]);
    }

    return Promise.all(this.model.feature.pictures.filter((a) => a.isNew).map((a) => a.localResize(1920, 1920)));
  }

  get loadingList() {
    if (!this.loadingService.isLoading) {
      return [];
    }

    return ['about.maps', 'icons'];
  }

  get isEmpty() {
    if (!this.model.feature) {
      return true;
    }

    if (this.hasInvalidPosition) {
      return true;
    }

    if (this.model.feature.name?.trim()) {
      return false;
    }

    if (this.model.feature.description?.blocks?.length) {
      return false;
    }

    if (this.model.feature.pictures?.length) {
      return false;
    }

    if (this.model.feature.icons?.length) {
      return false;
    }

    return true;
  }

  get isDisabled() {
    return this.submitDisabled;
  }

  handlePositionChange(key, pointCoords) {
    if (key == 'position') {
      if (!pointCoords) {
        pointCoords = this.tmpCenter || this.center;
      }

      this.lon = pointCoords[0];
      this.lat = pointCoords[1];

      this.model.feature.set('position.coordinates', pointCoords);

      this.areMapsLoading = true;
      debounce(this, this.updateMaps, 1000);

      return;
    }

    if (key == 'capturedUsingGPS') {
      this.ignoreGps = !value;
    } else {
      value = parseFloat(value);
    }

    handleAttributeChange(this.model.feature, 'position details', null, key, value);
  }

  handleAboutChange(key, value) {
    if (key == 'maps') {
      this.map = '';
      this.model.feature.maps.setObjects(value);
      return this.updateIcons();
    }

    if (key == 'name') {
      this.model.feature.name = value;
    }

    if (key == 'description') {
      this.model.feature.description = value;
    }
  }

  handleIconsChange(key, value) {
    if (key == 'icons') {
      this.model.feature.icons.setObjects(value);
    }
  }

  async createNewFeature(params) {
    let coordinates = [parseFloat(params?.lon) || 0, parseFloat(params?.lat) || 0];
    let attributes = this.defaultAttributes(params);

    if (attributes['position details']['type'] == 'gps' && this.position.hasPosition) {
      coordinates = this.position.center.slice();
    }

    const maps = [];

    if (params?.map) {
      const map = await this.store.findRecord('map', params['map']);
      maps.push(map);
    }

    return this.store.createRecord('feature', {
      id: null,
      name: '',
      maps,
      description: '',
      position: new GeoJson({ coordinates, type: 'Point' }),
      pictures: [],
      icons: [],
      info: new ModelInfo(),
      attributes,
      visibility: 0,
    });
  }

  defaultAttributes(params) {
    let altitude = '';
    let accuracy = '';
    let altitudeAccuracy = '';

    let type = this.position.hasPosition ? 'gps' : 'manual';

    if (params && (params['ignoreGps'] == 'true' || params['ignoreGps'] == true)) {
      type = 'manual';
    }

    if (params && !isNaN(params.lon) && !isNaN(params.lat)) {
      type = 'manual';
    } else if (type == 'gps' && this.position.hasPosition) {
      altitude = this.position.altitude;
      accuracy = this.position.accuracy;
      altitudeAccuracy = this.position.altitudeAccuracy;
    }

    return {
      'position details': new PositionDetails({
        altitude,
        accuracy,
        type,
        altitudeAccuracy,
      }),
    };
  }

  get defaultCenter() {
    return [0, 0];
  }

  async updateMaps() {
    if (!this.model?.feature?.position) {
      return;
    }

    this.areMapsLoading = true;
    const coordinates = this.model.feature.position.coordinates;
    const containing = `${coordinates[0]},${coordinates[1]}`;

    try {
      this.maps = await this.store.query('map', { containing });
    } catch (err) {
      this.maps = [];
    }

    this.areMapsLoading = false;
  }

  async updateIcons() {
    if (!this.model || !this.model.feature) {
      return;
    }

    if (this.model.feature.maps.length == 0) {
      this.iconGroup = this.model.defaultIcons;
      return;
    }

    this.areIconsLoading = true;
    const map = await this.model.feature.firstMap;
    const iconSet = map.iconSets.idList;

    let iconQuery = {
      iconSet: iconSet.join(),
      locale: this.intl.primaryLocale,
    };

    try {
      const icons = await this.store.adapterFor('icon').group(iconQuery);
      addIconGroupToStore(icons, this.store);
      this.iconGroup = icons;
    } catch (err) {
      this.iconGroup = this.model.defaultIcons;
    }

    this.areIconsLoading = false;
  }

  @action
  changeMapView(view) {
    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
    this.tmpCenter = center;
  }

  @action
  async searchGeocoding(term) {
    if (!term) {
      return this.geocodingSearch.clear();
    }

    return await this.geocodingSearch.search(term);
  }

  @action
  async changePosition(position, details) {
    await this.searchGeocoding(details['searchTerm']);

    if (this.geocodingSearch.hasNewResults) {
      details['address'] = this.geocodingSearch.results[0].name;
      position = this.geocodingSearch.results[0].geometry;
    }

    if (JSON.stringify(this.model.feature.position) != JSON.stringify(position.center)) {
      this.areMapsLoading = true;
      debounce(this, this.updateMaps, 1000);
    }

    this.model.feature.position = position.center;
    this.model.feature.attributes['position details'] = details;
    this.model.feature.notifyPropertyChange?.('attributes');
  }

  @action
  remove(key, index) {
    handleGroupRemove(this.model.feature, key, index);
    this.checkRequiredAttributes();
  }

  @action
  addSection(icon) {
    addIconSection(this.model.feature, icon);
    this.checkRequiredAttributes();

    this.model.feature.notifyPropertyChange('attributes');
  }

  @action
  checkRequiredAttributes() {
    this.submitDisabled = this.requiredAttributes.validate(this.model.feature);
  }

  @action
  async submit() {
    this.isSaving = true;
    this.isError = false;

    await this.resizePictures();
    const pictures = this.model.feature.pictures.filter((a) => a.isNew);
    const picturePromises = pictures.map((a) =>
      a.save({
        adapterOptions: {
          progress: (percentage) => {
            this.progress(a, percentage);
          },
        },
      })
    );

    await Promise.all(picturePromises);

    const newFeature = await this.model.feature.save();

    this.maps = null;
    this.icons = null;
    this.lon = null;
    this.lat = null;
    this.ignoreGps = null;

    let map;
    let feature;

    if (newFeature.maps.length > 0) {
      map = newFeature.firstMap.id;
    }

    if (newFeature.canEdit) {
      feature = newFeature.id;
    }

    later(() => {
      this.router.transitionTo('add.site-success', {
        queryParams: { map, feature },
      });
    });
  }

  @action
  async reset() {
    this.model.feature.name = '';
    this.model.feature.description = {};
    this.model.feature.attributes = this.defaultAttributes();
    this.model.feature.pictures = [];
    this.model.feature.icons = [];
    this.model.feature.maps = [];
  }

  @action
  editSection(name) {
    if (name == 'position') {
      this.router.transitionTo('add.site');
    }
  }

  @action
  changePictures(value) {
    this.handlePicturesChange('pictures', value);
  }

  @action
  change(group, key, value) {
    switch (group.key) {
      case 'position':
        this.handlePositionChange(key, value);
        break;

      case 'about':
        this.handleAboutChange(key, value);
        break;

      case 'pictures':
        handlePicturesChange(this.model.feature, key, value);
        break;

      case 'icons':
        this.handleIconsChange(key, value);
        break;

      default:
        handleAttributeChange(this.model.feature, group.key, group.index, key, value);
        this.checkRequiredAttributes();
    }
  }
}
