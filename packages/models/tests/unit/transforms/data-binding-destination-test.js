/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | data binding destination', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:data-binding-destination');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized).to.deep.equal({
      type: '',
      modelId: '',
      deleteNonSyncedRecords: false,
    });
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize({
      type: 'Map',
      modelId: '1',
      deleteNonSyncedRecords: true,
    });

    expect(deserialized.toJSON()).to.deep.equal({
      type: 'Map',
      modelId: '1',
      deleteNonSyncedRecords: true,
    });
  });

  it('can serialize an object', function () {
    const decorators = {
      type: 'Map',
      modelId: '1',
      deleteNonSyncedRecords: true,
    };

    let deserialized = transform.deserialize(decorators);

    expect(transform.serialize(deserialized)).to.deep.equal(decorators);
  });
});
