# GISCollective Web Frontend

Our web frontend is written in using [ember.js](https://emberjs.com/). This repository is a monorepo using [yarn workspaces](https://yarnpkg.com/features/workspaces). This monorepo contains:

  - `gis-collective` which is the main frontend app. You will most probable contribute here.
  - `ember-editor-js` is an ember wrapper over the [editor-js](https://editorjs.io/) library.
  - `ember-ol` __(deprecated)__ is an ember wrapper over the [open layers](https://openlayers.org/) library.
  - `ember-models` __(deprecated)__ is a library that contains several model definitions for GISCollective.

You can read more about the architecture at [https://guide.giscollective.com/en/develop/architecture/](https://guide.giscollective.com/en/develop/architecture/) or find more about our platform at
 [https://giscollective.com](https://giscollective.com/).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with yarn)
* [Ember CLI](https://ember-cli.com/)
* [Firefox](https://www.mozilla.org/en-US/firefox/new/), [Google Chrome](https://google.com/chrome/) or any other modern browser

## Installation

* `git clone <repository-url>` this repository
* `cd web-frontend`
* `yarn`
* `yarn start`

## Running / Development

* `yarn start` in the repo root folder or `ember serve` in a package folder.
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Writing Tests

* Ember guidelines at [https://guides.emberjs.com/release/testing/](https://guides.emberjs.com/release/testing/).
* Ember mocha at [https://github.com/emberjs/ember-mocha](https://github.com/emberjs/ember-mocha).
* Ember chai at [https://github.com/ember-cli/ember-cli-chai](https://github.com/ember-cli/ember-cli-chai).

### Running Tests

* `cd packages/<package>`
* `ember test`
* `ember test --server`

### Linting

* `cd packages/<package>`
* `yarn lint`
* `yarn lint:fix`

### Building

* `cd packages/<package>`
* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

This app can be deployed using any standard methods supported by ember.js. If you want support for `Ember Fastboot`, we recommend you to deploy the app using the provided docker container:

* docker build --no-cache=true -t $IMAGE_TAG .
* docker push $IMAGE_TAG

## Contributing

You can find our contribution guide at [https://guide.giscollective.com/en/develop/CONTRIBUTING/](https://guide.giscollective.com/en/develop/CONTRIBUTING/)

## Code of conduct

You can find our code of conduct at [https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/](https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/)

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
