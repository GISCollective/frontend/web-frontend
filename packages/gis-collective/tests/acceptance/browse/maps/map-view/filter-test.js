/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, waitUntil } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';

describe.skip('Acceptance | browse/maps/map-view/filter', function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPage('000000000000000000000001');
    server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultTeam('5ca78e2160780601008f69e6');

    server.server.get(`/mock-server/maps/:id/meta`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          publicSites: 220,
          totalContributors: 33,
        }),
      ];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('on the map view', function (hooks) {
    let map;

    hooks.beforeEach(function () {
      map = server.testData.storage.addDefaultMap();
    });

    it('loads only icons from that map from the default viewbox on first load', async function () {
      await visit(`/browse/maps/${map._id}/map-view/filter-icons`);

      await waitUntil(() => server.history.filter((a) => a.indexOf('/icons') != -1).length > 0);
      expect(server.history).to.contain(`GET /mock-server/icons?map=${map._id}&group=true`);
    });
  });
});
