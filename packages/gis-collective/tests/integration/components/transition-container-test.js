/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | transition-container',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the keyframe styles', async function () {
    this.set('transitionSteps', {
      0: {
        position: 'relative',
      },
      1: {
        position: 'fixed',
        top: '1px',
        left: '2px',
        width: '3px',
        height: '4px',
      },
      100: {
        position: 'fixed',
        top: '0px',
        left: '0px',
        width: '100%',
        height: '100%',
      },
    });

    await render(hbs`
      <TransitionContainer @id="some-id" @transitionSteps={{this.transitionSteps}}>
        template block text
      </TransitionContainer>
    `);

    const style = this.element.querySelector('style').textContent.trim();

    expect(style).to.contain('0% { position: relative; }');
    expect(style).to.contain(
      '1% { position: fixed; top: 1px; left: 2px; width: 3px; height: 4px; }'
    );
    expect(style).to.contain(
      '100% { position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; }'
    );
  });
});
