import { setupRenderingTest, describe, it } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { expect } from "chai";

describe("Integration | Component | editor/button-picture", function (hooks) {
  setupRenderingTest(hooks);

  let picture1;
  let picture2;
  let picture3;
  let server;
  let receivedPicture;
  let value;

  hooks.before(function () {
    server = new TestServer();

    receivedPicture = null;
    server.post("/mock-server/pictures", (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = "10";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedPicture)];
    });

    picture1 = server.testData.storage.addDefaultPicture("1");
    picture1.picture = "/test/1";

    picture2 = server.testData.storage.addDefaultPicture("2");
    picture2.picture = "/test/2";

    picture3 = server.testData.storage.addDefaultPicture("3");
    picture3.picture = "/test/3";
  });

  hooks.beforeEach(function () {
    receivedPicture = null;

    this.set("onChange", (v) => {
      value = v;
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders a value", async function (assert) {
    this.set("value", {
      picture: picture1._id,
      hover: picture2._id,
      active: picture3._id,
      style: {
        lg: { height: "3px" },
        md: { height: "2px" },
        sm: { height: "1px" },
      },
      position: "top",
    });

    await render(hbs`<Editor::ButtonPicture @value={{this.value}} />`);

    expect(this.element.querySelector(".input-btn-picture img")).to.have.attribute("src", "/test/1/sm");
    expect(this.element.querySelector(".input-btn-hover img")).to.have.attribute("src", "/test/2/sm");
    expect(this.element.querySelector(".input-btn-active img")).to.have.attribute("src", "/test/3/sm");
    expect(this.element.querySelector(".sm-size-px .input-px-size").value).to.equal("1");
    expect(this.element.querySelector(".md-size-px .input-px-size").value).to.equal("2");
    expect(this.element.querySelector(".lg-size-px .input-px-size").value).to.equal("3");
    expect(this.element.querySelector(".input-position").value).to.equal("top");
  });

  it("can change a value", async function (a) {
    this.set("value", {
      picture: picture1._id,
      hover: picture2._id,
      active: picture3._id,
      style: {
        lg: { height: "3px" },
        md: { height: "2px" },
        sm: { height: "1px" },
      },
      position: "top",
    });
    let value;

    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::ButtonPicture @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".input-position").value = "bottom";
    await triggerEvent(".input-position", "change");

    expect(this.element.querySelector(".input-position").value).to.equal("bottom");

    a.deepEqual(value, {
      active: "3",
      hover: "2",
      picture: "1",
      position: "bottom",
      style: {
        lg: {
          height: "3px",
        },
        md: {
          height: "2px",
        },
        sm: {
          height: "1px",
        },
      },
    });
  });
});
