/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class RtlService extends Service {
  @tracked _isRTL = false;

  set isRTL(value) {
    this._isRTL = value;
  }

  get isRTL() {
    if (!document) {
      return this._isRTL;
    }

    const html = document.querySelector('html');

    if (html.classList.contains('translated-rtl')) {
      return true;
    }

    return this._isRTL;
  }

  updateHtml() {
    if (!document) {
      return;
    }

    const html = document.querySelector('html');

    if (this.isRTL && !html.hasAttribute('dir')) {
      html.setAttribute('dir', 'rtl');
    }

    if (!this.isRTL && html.hasAttribute('dir')) {
      html.removeAttribute('dir');
    }
  }

  setup() {
    if (!document) {
      return;
    }

    this.observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.type === 'attributes') {
          this.updateHtml();
        }
      });
    });

    this.observer.observe(document.querySelector('html'), {
      attributes: true,
    });
  }
}
