import Controller from '@ember/controller';
import { transform } from 'ol/proj';
import { service } from '@ember/service';

export default class MapViewController extends Controller {
  @service embeddedClient;

  get center() {
    if (!this.embeddedClient.value?.center) {
      return null;
    }

    return transform(
      this.embeddedClient.value?.center,
      'EPSG:4326',
      'EPSG:3857',
    );
  }
}
