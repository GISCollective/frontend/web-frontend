/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageDataBindingFieldMappingComponent extends Component {
  @tracked _fieldList = null;

  get fieldList() {
    if (this._fieldList !== null) {
      return this._fieldList;
    }

    return this.args.value;
  }

  @action
  updateMapping(key, value) {
    if (this._fieldList === null) {
      this._fieldList = this.args.value;
    }

    this._fieldList.forEach((field, index) => {
      if (field.key != key) return;

      this._fieldList[index].destination = value;
    });
  }

  @action
  async save() {
    await this.args.onSave?.(this.fieldList);

    this._fieldList = null;
  }

  @action
  async cancel() {
    await this.args.onCancel?.();
    this._fieldList = null;
  }
}
