/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ApplicationRoute extends Route {
  @service session;
  @service user;
  @service intl;

  async beforeModel() {
    this.intl.setLocale(["en-us"]);
    await this.session.setup();
    await this.user.ready();
  }

  @action
  error(error) {
    console.error(error);
  }
}
