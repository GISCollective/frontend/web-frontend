/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';

export default class AttributesViewValueComponent extends Component {
  get isBool() {
    if (this.isBlocks) {
      return false;
    }

    return ['boolean', 'boolean-required'].includes(this.args.format);
  }

  get isBlocks() {
    if (this.args.format == 'long text') {
      return true;
    }

    return Array.isArray(this.args.value?.blocks);
  }

  get blocksValue() {
    return toContentBlocks(this.args.value);
  }
}
