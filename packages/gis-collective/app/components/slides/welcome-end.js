/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class SlideWelcomeEndComponent extends Component {
  @service session;

  get options() {
    return {
      data: this.args.options,
    };
  }

  @action
  back() {
    this.args.onPrevPage?.();
  }
}
