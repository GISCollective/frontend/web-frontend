/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class PageColBreadcrumbComponent extends Component {
  @service store;

  @tracked pages;

  get selectedModel() {
    return this.args.model?.selectedModel;
  }

  get currentPage() {
    return this.args.model?.page;
  }

  get currentPageName() {
    return this.currentPage?.recordTitle?.(this.selectedModel);
  }

  get linkOptions() {
    return { color: 'default' };
  }

  get pagesWithDestinations() {
    return this.pages?.map((page) => {
      const relatedModelType = page.relatedModelType;
      const relatedModel =
        this.selectedModel?.relatedModelByType?.(relatedModelType);

      return {
        page,
        title: page.recordTitle?.(relatedModel),
        destination: page.pathForRecord?.(relatedModel),
      };
    });
  }

  async getPage(id) {
    if (!id) {
      return null;
    }

    if (typeof id == 'object') {
      return this.getPage(id.link?.pageId);
    }

    let page = this.store.peekRecord('page', id);

    if (!page) {
      page = await this.store.findRecord('page', id);
    }

    return page;
  }

  @action
  async setup() {
    const items = this.args.value?.data?.items ?? [];

    this.pages = items.map((a) => this.getPage(a));
    this.pages = await Promise.all(this.pages);
    this.pages = this.pages.filter((a) => a);
  }
}
