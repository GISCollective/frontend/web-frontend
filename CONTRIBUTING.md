# Contributing

You can find our contribution guide at [https:/guide.giscollective.com/en/developers/CONTRIBUTING/](https:/guide.giscollective.com/en/developers/CONTRIBUTING/)