/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class InputMapLayerComponent extends Component {
  rasterTypes = ['Open Street Map', 'GISCollective Map', 'WMS', 'XYZ', 'VectorTile', 'MapBox', 'ArcGIS MapServer'];

  layerOptions = {
    XYZ: [
      {
        name: 'url',
        hint: 'eg. https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
      },
      { name: 'tilePixelRatio', hint: 'eg. 2' },
    ],
    VectorTile: [
      {
        name: 'style',
        hint: 'eg. https://api.maptiler.com/maps/xxx/style.json?key=xxx',
      },
    ],
    MapBox: [
      {
        name: 'style',
        hint: 'eg. mapbox://styles/xxx/xxxxxxxxxx',
      },
      {
        name: 'token',
      },
    ],
    'ArcGIS MapServer': [{ name: 'url' }],
  };

  panels = {
    WMS: [
      {
        name: 'server type',
        options: ['carmentaserver', 'geoserver', 'mapserver', 'qgis'],
      },
      { name: 'url', type: 'text' },
      { name: 'layers', type: 'text' },
      { name: 'styles', type: 'text' },
      { name: 'version', type: 'text' },
      { name: 'gutter', type: 'text' },
      { name: 'tiled', type: 'bool' },
    ],
  };

  get panel() {
    return this.panels[this.args.value?.type];
  }

  get index() {
    return parseInt(this.args.index) || 0;
  }

  get len() {
    return parseInt(this.args.len) || 0;
  }

  get incrementedIndex() {
    return this.index + 1;
  }

  get canNotMoveDown() {
    return this.index >= this.len - 1 || this.len == 1;
  }

  get canNotMoveUp() {
    return this.index <= 0 || this.len == 1;
  }

  get hasOptions() {
    if (!this.args.value) {
      return false;
    }

    return this.layerOptions[this.args.value?.type] ? true : false;
  }

  get optionList() {
    return this.layerOptions[this.args.value?.type].map((a) => {
      const item = {
        name: a.name,
        hint: a.hint,
        options: a.options,
      };

      if (this.args.value.options[a['name']]) {
        item.value = this.args.value?.options[a['name']];
      } else {
        item.value = '';
      }

      return item;
    });
  }

  get isVisibleOnAllLevels() {
    if (!this.args.value?.options) {
      return true;
    }

    return !this.args.value.options.visibility;
  }

  get minVisibility() {
    if (!this.args.value?.options?.visibility) {
      return 0;
    }

    return this.args.value?.options?.visibility[0];
  }

  get maxVisibility() {
    if (!this.args.value?.options?.visibility) {
      return 22;
    }

    return this.args.value.options.visibility[1];
  }

  get minVisibilityList() {
    const list = [];

    for (let i = 0; i <= 22; i++) {
      list.push(i);
    }

    return list;
  }

  get maxVisibilityList() {
    const list = [];

    for (let i = 0; i <= 22; i++) {
      list.push(i);
    }

    return list;
  }

  get gisCollectiveMap() {
    if (!this.args.value?.options) {
      return '';
    }

    return this.args.value?.options['GISCollectiveMap'];
  }

  @action
  changeOption(key, val) {
    this.args.value.options[key] = val;

    this.args.onChangeOption?.(this.index, this.args.value?.options);
  }

  @action
  changeOptions(value) {
    this.args.onChangeOption?.(this.index, value);
  }

  @action
  delete() {
    if (this.args.onDelete) {
      this.args.onDelete(this.index);
    }
  }

  @action
  changeText(key, event) {
    const val = event.target.value;
    this.changeOption(key, val);
  }

  @action
  moveUp() {
    if (this.args.onMoveUp) {
      this.args.onMoveUp(this.index);
    }
  }

  @action
  moveDown() {
    if (this.args.onMoveDown) {
      this.args.onMoveDown(this.index);
    }
  }

  @action
  selectType(event) {
    if (this.args.onChangeType) {
      this.args.onChangeType(event.target.value, this.index);
    }
  }

  @action
  selectVisibility(event) {
    const value = event.target.value == 'true';

    if (value) {
      this.changeOption('visibility');
    } else {
      this.changeOption('visibility', [0, 22]);
    }
  }

  @action
  selectMinVisibility(event) {
    const value = parseInt(event.target.value);
    this.changeOption('visibility', [value, this.maxVisibility]);
  }

  @action
  selectMaxVisibility(event) {
    const value = parseInt(event.target.value);
    this.changeOption('visibility', [this.minVisibility, value]);
  }

  @action
  selectGisCollectiveMap(event) {
    this.changeOption('GISCollectiveMap', event.target.value);
  }
}
