/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import Select from 'ol/interaction/Select';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class MapInteractionSelectComponent extends Component {
  @tracked select;
  @tracked map;
  @tracked features;

  @action
  setup() {
    this.removeInteraction();

    if (!this.args.layer || !this.args.map) {
      return;
    }

    later(() => {
      this.map = this.args.map;
      this.select = new Select({
        layers: [this.args.layer],
      });

      this.select.on('select', () => {
        this.features = this.select.getFeatures();
      });

      this.map.addInteraction(this.select);
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.removeInteraction();
  }

  removeInteraction() {
    if (!this.select) {
      return;
    }

    this.map.removeInteraction(this.select);
    this.select = null;
    this.features = null;
  }

  triggerChange(event) {
    this.args.onChange(event);
  }
}
