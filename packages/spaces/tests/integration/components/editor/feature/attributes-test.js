/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/feature/attributes", function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultFeature("1");
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the selected value", async function () {
    this.set("value", {
      data: { source: { model: "feature", id: "1" } },
    });

    await render(hbs`<Editor::Feature::Attributes @value={{this.value}}/>`);

    expect(this.element.querySelector(".use-current-model").value).to.equal("no");
    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "Nomadisch Grün - Local Urban Food",
    );
  });

  it("can change the value", async function () {
    this.set("value", {
      data: { source: { model: "feature", id: "1" } },
    });

    let changedValue;
    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Feature::Attributes @value={{this.value}} @onChange={{this.onChange}}/>`);

    this.element.querySelector(".use-current-model").value = "yes";
    await triggerEvent(".use-current-model", "change");

    expect(changedValue).to.deep.equal({
      source: {
        useSelectedModel: true,
      },
    });
  });
});
