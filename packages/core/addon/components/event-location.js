/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class EventLocationComponent extends Component {
  @service store;
  @service space;
  @tracked _feature;

  get feature() {
    if (!this.args.value?.value) {
      return null;
    }

    if (this.args.value?.type != 'Feature') {
      return null;
    }

    return this.store.peekRecord('feature', this.args.value?.value) ?? this._feature;
  }

  get value() {
    if (this.args.value?.type == 'Text') {
      return this.args.value?.value;
    }

    if (this.feature) {
      return this.feature.name;
    }

    return '';
  }

  get link() {
    if (!this.feature?.id) {
      return '';
    }

    const to = this.space.getLinkFor('Feature', this.feature);

    return to;
  }

  @action
  async setup() {
    if (this.args.value?.type != 'Feature') {
      return;
    }

    this._feature = this.store.peekRecord('feature', this.args.value?.value);

    if (!this._feature) {
      this._feature = await this.store.findRecord('feature', this.args.value?.value);
    }
  }
}
