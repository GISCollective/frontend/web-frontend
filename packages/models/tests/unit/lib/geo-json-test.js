/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupTest, it } from 'dummy/tests/helpers';
import { GeoJsonGps } from 'models/lib/geoJson';

module('Unit | Lib | geo-json-gps', function (hooks) {
  setupTest(hooks);

  it('returns 0, 0 when there is no position service', function (a) {
    let json = new GeoJsonGps();

    a.deepEqual(json.coordinates, [0, 0]);
  });

  it('returns the default coordinates when there is no position service', function (a) {
    let json = new GeoJsonGps(null, [1, 2]);

    a.deepEqual(json.coordinates, [1, 2]);
  });

  it('returns the position center when exists', function (a) {
    let json = new GeoJsonGps(
      {
        center: [2, 3],
        status: 'WATCHING',
      },
      [1, 2],
    );

    a.deepEqual(json.coordinates, [2, 3]);
  });

  it('returns the default value when the status is not WATCHING', function (a) {
    let json = new GeoJsonGps(
      {
        center: [2, 3],
        status: 'OTHER',
      },
      [1, 2],
    );

    a.deepEqual(json.coordinates, [1, 2]);
  });
});
