/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/icon/grid-with-categories", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let changedValue;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIconSet("1");
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders the selected value", async function () {
    this.set("value", {
      data: { source: { model: "feature", id: "1" } },
    });

    await render(hbs`<Editor::Icon::GridWithCategories @value={{this.value}} />`);

    expect(this.element.querySelector(".use-current-model").value).to.equal("no");
    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "Green Map® Icons Version 3",
    );
  });

  it("renders the category options", async function () {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        categoryStyle: {
          color: "green",
          hoverColor: "red",
          activeColor: "blue",
          classes: ["fw-bold", "text-size-09"],
        },
      },
    });

    await render(hbs`<Editor::Icon::GridWithCategories @value={{this.value}} />`);

    expect(this.element.querySelector(".category-style .text-color-selector .selected-value")).to.have.class(
      "bg-green",
    );
    expect(this.element.querySelector(".category-style .text-hover-color-selector .selected-value")).to.have.class(
      "bg-red",
    );
    expect(this.element.querySelector(".category-style .text-active-color-selector .selected-value")).to.have.class(
      "bg-blue",
    );
  });

  it("renders the subcategory options", async function () {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        subcategoryStyle: {
          color: "green",
          hoverColor: "red",
          activeColor: "blue",
          classes: ["fw-bold", "text-size-09"],
        },
      },
    });

    await render(hbs`<Editor::Icon::GridWithCategories @value={{this.value}} />`);

    expect(this.element.querySelector(".subcategory-style .text-color-selector .selected-value")).to.have.class(
      "bg-green",
    );
    expect(this.element.querySelector(".subcategory-style .text-hover-color-selector .selected-value")).to.have.class(
      "bg-red",
    );
    expect(this.element.querySelector(".subcategory-style .text-active-color-selector .selected-value")).to.have.class(
      "bg-blue",
    );
  });

  it("renders the layout options", async function () {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        iconLayout: {
          itemsPerPage: 10,
          classes: ["col-2", "col-md-3", "col-lg-4"],
        },
      },
    });

    await render(hbs`<Editor::Icon::GridWithCategories @value={{this.value}} />`);

    expect(this.element.querySelector(".items-per-page").value).to.equal("10");
    expect(this.element.querySelector(".responsive-value-sm .input-width-col").value).to.equal("2");

    expect(this.element.querySelector(".responsive-value-md .input-width-col").value).to.equal("3");
  });

  it("can change the subcategory colors", async function (a) {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        subcategoryStyle: {
          color: "green",
          hoverColor: "red",
          activeColor: "blue",
          classes: ["fw-bold", "text-size-09"],
        },
      },
    });

    await render(hbs`<Editor::Icon::GridWithCategories @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".subcategory-style .text-color-selector");
    await click(".subcategory-style .text-color-selector .bg-primary");

    await click(".subcategory-style .text-hover-color-selector");
    await click(".subcategory-style .text-hover-color-selector .bg-secondary");

    await click(".subcategory-style .text-active-color-selector");
    await click(".subcategory-style .text-active-color-selector .bg-danger");

    a.deepEqual(changedValue.subcategoryStyle, {
      color: "primary",
      hoverColor: "secondary",
      activeColor: "danger",
      classes: ["fw-bold", "text-size-09"],
      minLines: { lg: 0, md: 0, sm: 0 },
    });
  });

  it("can change the category colors", async function () {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
        categoryStyle: {
          color: "green",
          hoverColor: "red",
          activeColor: "blue",
          classes: ["fw-bold", "text-size-09"],
        },
      },
    });

    await render(hbs`<Editor::Icon::GridWithCategories @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".category-style .text-color-selector");
    await click(".category-style .text-color-selector .bg-primary");

    await click(".category-style .text-hover-color-selector");
    await click(".category-style .text-hover-color-selector .bg-secondary");

    await click(".category-style .text-active-color-selector");
    await click(".category-style .text-active-color-selector .bg-danger");

    expect(changedValue.categoryStyle).to.deep.equal({
      color: "primary",
      hoverColor: "secondary",
      activeColor: "danger",
      classes: ["fw-bold", "text-size-09"],
      minLines: { lg: 0, md: 0, sm: 0 },
    });
  });

  it("can change the layout properties", async function () {
    this.set("value", {
      data: {
        source: { model: "feature", id: "1" },
      },
    });

    await render(hbs`<Editor::Icon::GridWithCategories @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".items-per-page", 20);

    expect(changedValue.iconLayout).to.deep.equal({ itemsPerPage: 20 });
  });
});
