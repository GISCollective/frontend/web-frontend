/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class InputSelectListComponent extends Component {
  elementId = 'select-list-' + guidFor(this);
  @tracked _values;
  @tracked _isEnabled = null;

  get values() {
    return this._values ?? [];
  }

  get haveEnabledSwitch() {
    return this.args.isEnabled !== undefined;
  }

  get isEnabled() {
    if (!this.haveEnabledSwitch) {
      return true;
    }

    if (this._isEnabled === null) {
      return this.args.isEnabled;
    }

    return this._isEnabled;
  }

  set isEnabled(value) {
    this._isEnabled = value;
  }

  @action
  async setup() {
    let values = (await this.args.values) ?? [];

    if (values.slice) {
      values = values.slice();
    }

    this._values = values;
  }

  @action
  change(isEnabled, values) {
    this._isEnabled = isEnabled;
    this._values = values;
  }

  @action
  save() {
    if (!this.args.onSave) {
      return;
    }

    const validValues = this.values.filter((a) => a !== null && a !== undefined);

    if (!this.isEnabled) {
      return this.args.onSave(this.args.title, [], false);
    }

    return this.args.onSave(this.args.title, validValues, true);
  }

  @action
  cancel() {
    this._values = null;
    this._isEnabled = null;

    if (!this.args.onCancel) {
      return;
    }

    this.args.onCancel(...arguments);
  }
}
