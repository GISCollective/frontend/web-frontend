/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsLongTextComponent extends Component {
  elementId = `editor-js-${guidFor(this)}`;
  @tracked _value = null;

  get value() {
    if (this._value !== null) {
      return this._value;
    }
    return this.args.value;
  }

  set value(value) {
    this._value = value;

    this.args.onChange?.(value);
  }
}
