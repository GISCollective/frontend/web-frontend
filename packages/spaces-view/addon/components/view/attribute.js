/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action, get } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ViewAttributeComponent extends Component {
  @tracked record;

  get value() {
    if (!this.record) {
      return null;
    }

    return get(
      this.record,
      'attributes.' + this.args.value?.data?.sourceAttribute?.field,
    );
  }

  get hasBlocks() {
    return Array.isArray(this.value?.blocks);
  }

  get label() {
    if (this.hasMissingTitle && this.args.isEditor) {
      return 'no title';
    }
    return this.args.value.data?.title?.text;
  }

  get hasMissingTitle() {
    const value = this.args.value.data?.title?.text?.trim?.();

    return value ? false : true;
  }

  @action
  loaded(record) {
    this.record = record;
  }
}
