/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from '../../helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | link', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let spaceData;
  let space;

  hooks.before(async function () {
    server = new TestServer();
    server.testData.create.feature('1');

    server.testData.storage.addDefaultPage();
    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    space = await store.findRecord('space', spaceData._id);
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders the content when a path value is set', async function () {
    await render(hbs`<Link @to="/path">template block text</Link>`);

    expect(this.element.textContent.trim()).to.equal('template block text');
  });

  it('renders an external url when set', async function () {
    await render(hbs`
      <Link @to="https://giscollective.com">
      template block text
      </Link>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
    expect(this.element.querySelector('a')).to.have.attribute('href', 'https://giscollective.com');
  });

  it('renders a route link', async function () {
    this.set('to', {
      route: 'manage.features.add',
    });

    await render(hbs`
      <Link @to={{this.to}}>
      template block text
      </Link>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
    expect(this.element.querySelector('a')).to.have.attribute('href', '/manage/features/add');
  });

  it('renders a link when the value is a valid slug', async function () {
    this.set('to', 'page--test');
    this.set('space', space);

    await this.space.getPagesMap();

    await render(hbs`
      <Link @to={{this.to}} @space={{this.space}}>
      template block text
      </Link>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
    expect(this.element.querySelector('a')).to.have.attribute('data-link-path', '/page/test');
  });

  it('renders a link when the value is a valid page id', async function () {
    this.set('to', '000000000000000000000001');
    this.set('space', space);

    await this.space.getPagesMap();

    await render(hbs`
      <Link @to={{this.to}} @space={{this.space}}>
      template block text
      </Link>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
    expect(this.element.querySelector('a')).to.have.attribute('data-link-path', '/page/test');
  });

  it('renders a link when the value an anchor', async function () {
    this.set('to', '#anchor');
    this.set('space', space);

    await this.space.getPagesMap();

    await render(hbs`
      <Link @to={{this.to}} @space={{this.space}}>
      template block text
      </Link>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
    expect(this.element.querySelector('a')).to.have.attribute('href', '#anchor');
  });
});
