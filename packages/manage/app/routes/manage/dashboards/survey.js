/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { action } from '@ember/object';
import LazyList from 'models/lib/lazy-list';
import { tracked } from '@glimmer/tracking';

class CampaignModelData {
  @tracked campaign;
  @tracked team;
  @tracked answers;

  constructor(params, store) {
    this.params = params;
    this.store = store;
  }

  @action
  async reload() {
    this.campaign = await this.store.findRecord('campaign', this.params.id);
    this.team = await this.campaign.visibility.fetchTeam();

    const query = { campaign: this.params.id };
    this.answers = new LazyList('campaign-answer', query, this.store);
    this.answers.itemsPerPage = 100;
    await this.answers.loadNext();

    return this;
  }
}

export default class ManageDashboardsCampaignRoute extends AuthenticatedRoute {
  async model(params) {
    const campaignModel = new CampaignModelData(params, this.store);

    return campaignModel.reload();
  }
}
