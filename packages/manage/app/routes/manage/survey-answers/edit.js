/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { tracked } from '@glimmer/tracking';

class ManageCampaignAnswersEditModel {
  @tracked icons;
  @tracked answerIcons;
  @tracked iconSets;
  @tracked campaign;
  @tracked team;
  @tracked map;
  @tracked baseMaps;
  @tracked campaignAnswer;
  @tracked originalAuthorProfile;
  @tracked attributes;
}

export default class ManageCampaignAnswersEditRoute extends AuthenticatedRoute {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const model = new ManageCampaignAnswersEditModel();

    model.campaignAnswer = await this.store.findRecord('campaign-answer', params.id);
    await model.campaignAnswer.icons;

    model.campaign = await model.campaignAnswer.campaign;
    model.baseMaps = await model.campaign.baseMaps?.fetch?.();
    model.map = await model.campaign.map?.fetch?.();
    model.iconSets = await model.map?.iconSets?.fetch?.();

    const icons = await model.campaign.icons;
    const optionalIcons = await model.campaign.optionalIcons;
    model.icons = [...icons, ...optionalIcons];
    model.answerIcons = icons;

    await model.campaign.loadRelations();

    model.team = await model.campaign.visibility.fetchTeam();

    try {
      await model.campaignAnswer.info.fetch();
    } catch (err) {
      console.log('Invalid user info:', err);
    }

    model.originalAuthorProfile = await model.campaignAnswer.info.originalAuthorProfile;

    return model;
  }
}
