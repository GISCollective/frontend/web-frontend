/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | offcanvas', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a hidden offcanvas', async function () {
    await render(hbs`<Offcanvas />`);
    expect(this.element.querySelector('.offcanvas-body').textContent.trim()).to.equal('');

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });

  it('renders a visible sheet when @show is true', async function () {
    await render(hbs`<Offcanvas @show={{true}}/>`);
    await waitUntil(() => !this.element.querySelector('.offcanvas.showing'));

    expect(this.element.querySelector('.offcanvas')).to.have.class('show');
  });

  it('closes the sheet when the close button is pressed', async function () {
    await render(hbs`<Offcanvas @show={{true}}/>`);

    await click('.btn-close');

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });
});
