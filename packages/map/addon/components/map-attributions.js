/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class MapAttributionsComponent extends Component {
  get attributions() {
    const list = [];

    if (this.args?.map?.license?.name) {
      list.push(this.args.map.license);
    }

    const attributions = this.args.baseMap?.get
      ? this.args.baseMap?.get('attributions')
      : this.args.baseMap?.attributions;

    for (const item of attributions ?? []) {
      list.push(item);
    }

    return list;
  }
}
