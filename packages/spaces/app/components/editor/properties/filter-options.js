/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { service } from "@ember/service";

export default class ManageEditorsPropertiesFilterOptionsComponent extends Component {
  @service store;

  @tracked iconSets = [];
  @tracked maps = [];
  @tracked calendars = [];
  @tracked _options;

  filterTypeConfig = {
    icons: [
      {
        name: "label",
        type: "text",
      },
      {
        name: "source",
        options: ["icon-set", "model"],
      },
      {
        name: "sourceModelKey",
        options: [],
      },
      {
        name: "icon-set",
        options: [],
      },
    ],
    "map feature": [
      {
        name: "label",
        type: "text",
      },
      {
        name: "placeholder-text",
        type: "text",
      },
      {
        name: "map",
        options: [],
      },
      {
        name: "with-search",
        type: "bool",
      },
    ],
    location: [
      {
        name: "label",
        type: "text",
      },
    ],
    "event repetition": [
      {
        name: "label",
        type: "text",
      },
      { name: "norepeat", type: "bool" },
      { name: "daily", type: "bool" },
      { name: "weekly", type: "bool" },
      { name: "monthly", type: "bool" },
      { name: "yearly", type: "bool" },
    ],
    date: [
      {
        name: "label",
        type: "text",
      },
    ],
    "calendar attribute": [
      {
        name: "label",
        type: "text",
      },
      {
        name: "placeholder-text",
        type: "text",
      },
      {
        name: "attribute-name",
        type: "text",
      },
      {
        name: "calendar",
      },
      {
        name: "with-search",
        type: "bool",
      },
    ],
    "feature attribute": [
      {
        name: "label",
        type: "text",
      },
      {
        name: "placeholder-text",
        type: "text",
      },
      {
        name: "attribute-name",
        type: "text",
      },
      {
        name: "map",
      },
      {
        name: "with-search",
        type: "bool",
      },
    ],
    "day of week": [
      {
        name: "label",
        type: "text",
      },
    ],
  };

  get options() {
    if (this._options) {
      return this._options;
    }

    return this.value.options;
  }

  get config() {
    if (this.value.type == "icons" && !this.iconSets.length) {
      return [];
    }

    if (this.value.type == "map feature" && !this.maps.length) {
      return [];
    }

    if (this.value.type == "calendar attribute" && !this.calendars.length) {
      return [];
    }

    if (this.value.type == "feature attribute" && !this.maps.length) {
      return [];
    }

    const result = this.filterTypeConfig[this.value.type] ?? [];

    if (this.value.type == "icons") {
      result[2].options = this.modelKeys;
      result[3].options = this.iconSets.map((a) => ({
        id: a.id,
        name: a.name,
      }));
    }

    if (this.value.type == "map feature") {
      result[2].options = this.maps.map((a) => ({
        id: a.id,
        name: a.name,
      }));
    }

    if (this.value.type == "calendar attribute") {
      result[3].options = this.calendars.map((a) => ({
        id: a.id,
        name: a.name,
      }));
    }

    if (this.value.type == "feature attribute") {
      result[3].options = this.maps.map((a) => ({
        id: a.id,
        name: a.name,
      }));
    }

    return result;
  }

  get value() {
    return this.args.value ?? {};
  }

  get modelKeys() {
    if (this.value.type !== "icons") {
      return [];
    }

    const keys = Object.keys(this.args.model ?? {});

    return keys.filter((key) => this.args.model[key]?.iconSetsFilter !== undefined);
  }

  @action
  changeOptions(value) {
    this._options = value;

    return this.args.onChange?.(value);
  }

  @action
  async setup() {
    let iconSets = (await this.store.query("icon-set", { default: true })).slice();

    if (this.args.model?.team?.id) {
      const teamSets = (await this.store.query("icon-set", { team: this.args.model?.team?.id })).slice();
      iconSets = [...iconSets, ...teamSets];

      this.maps = (await this.store.query("map", { team: this.args.model?.team?.id })).slice();
      this.calendars = (await this.store.query("calendar", { team: this.args.model?.team?.id })).slice();
    }

    this.iconSets = iconSets;
  }
}
