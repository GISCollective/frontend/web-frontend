import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputGeoJsonSourcesComponent extends Component {
  sources = ['allowGps', 'allowManual', 'allowAddress', 'allowExistingFeature'];
  @tracked _value;

  get hasValidValue() {
    return typeof this.args.value?.allowGps == 'boolean';
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    if (this.hasValidValue) {
      return this.args.value;
    }

    return {
      allowGps: true,
      allowManual: true,
      allowAddress: true,
      allowExistingFeature: false,
    };
  }

  @action
  change(source, event) {
    if (!this._value) {
      this._value = this.value;
    }

    this._value[source] = event.target.checked;

    this.args.onChange?.(this._value);
  }

  @action
  setup() {
    if (this.hasValidValue) {
      return;
    }

    this.args.onChange?.(this.value);
  }
}
