/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | input/options/size-unit", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the value when set with the right unit", async function () {
    let called = false;

    this.set("value", "300px");
    this.set("change", () => {
      called = true;
    });
    await render(hbs`<Input::Options::SizeUnit @value={{this.value}} @unit="px" />`);

    expect(this.element.querySelector(".input-px-size").value).to.equal("300");
    expect(called).to.equal(false);
  });

  it("renders the value when set with the wrong unit", async function () {
    this.set("value", "300px");
    await render(hbs`<Input::Options::SizeUnit @value={{this.value}} @unit="%" />`);

    expect(this.element.querySelector(".input-percentage-size").value).to.equal("300");
  });

  it("triggers a change event when the unit is wrong", async function () {
    let value;
    this.set("value", "300px");
    this.set("change", (v) => {
      value = v;
    });
    await render(hbs`<Input::Options::SizeUnit @value={{this.value}} @unit="%" @onChange={{this.change}}/>`);

    expect(value).to.equal("300%");
  });

  it("does nothing when the value is an empty string", async function () {
    let value;
    this.set("value", "");
    this.set("change", (v) => {
      value = v;
    });
    await render(hbs`<Input::Options::SizeUnit @value={{this.value}} @unit="%" @onChange={{this.change}}/>`);

    expect(value).to.be.undefined;
  });

  it("triggers a change event when the value is updated", async function () {
    let value;
    this.set("value", "300px");
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::Options::SizeUnit @value={{this.value}} @unit="%" @onChange={{this.change}}/>`);

    await fillIn(".input-percentage-size", "200");

    expect(value).to.equal("200%");
  });

  it("triggers a change event when the unit is changed", async function () {
    let value;
    this.set("value", "300px");
    this.set("unit", "px");
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::Options::SizeUnit @value={{this.value}} @unit={{this.unit}} @onChange={{this.change}}/>`);

    this.set("unit", "%");
    expect(value).to.equal("300%");
  });
});
