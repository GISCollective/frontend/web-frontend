/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { debounce } from "@ember/runloop";
import { dasherize } from "@ember/string";

export default class InputRecordIdComponent extends Component {
  @service store;

  @tracked record;

  async performSearchRecord(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject("Term is too short");
    }

    if (this.args.model == "page") {
      let pages = {};

      try {
        pages = await this.args.space.getPagesMap();
      } catch (err) {
        return reject(err);
      }

      pages = Object.values(pages).map((a) => this.store.peekRecord("page", a) || this.store.findRecord("page", a));
      try {
        pages = await Promise.all(pages);
      } catch (err) {
        return reject(err);
      }

      pages = pages.filter((a) => a.name.toLowerCase().indexOf(term.toLowerCase()) >= 0);

      return resolve(pages);
    }

    const query = { term };

    if (this.args.team) {
      query.team = this.args.team;
    }

    return this.store
      .query(dasherize(this.args.model), query)
      .then((data) => {
        resolve(data.slice());
      })
      .catch(reject);
  }

  @action
  async setup() {
    if (!this.args.model || !this.args.value) {
      return;
    }

    try {
      this.record = await this.store.findRecord(this.args.model, this.args.value);
    } catch (err) {
      console.error(err);
    }
  }

  @action
  searchRecord(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performSearchRecord, term, resolve, reject, 600);
    });
  }

  @action
  handleChange(record) {
    this.record = record;
    return this.args.onChange?.(record.id, record);
  }
}
