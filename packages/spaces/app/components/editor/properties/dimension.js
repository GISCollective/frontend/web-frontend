/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";
import { sizedBsProperty, setSizedBsProperty } from "core/lib/bs-cols";
import { tracked } from "@glimmer/tracking";

export default class InputDimensionComponent extends Component {
  elementId = `input-dimension-${guidFor(this)}`;
  availableColWidths = ["default", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];

  _units = ["cols", "px", "%", "vh", "vw"];

  @tracked _numericValue = null;
  @tracked _unit = null;
  @tracked _options = null;

  get hasNumericValue() {
    return this.unit != "cols";
  }

  get availableUnits() {
    return this.args.units ?? this._units;
  }

  get unit() {
    if (typeof this._unit == "string") {
      return this._unit;
    }

    if (typeof this.args.index == "number" && Array.isArray(this.args?.value?.[this.size]?.[this.property])) {
      return this.args?.value[this.size][this.property][this.args.index].unit ?? this.availableUnits[0];
    }

    if (this.args?.value && this.args?.value[this.size] && this.args?.value[this.size][this.property]) {
      return this.args?.value[this.size][this.property].unit;
    }

    const property = sizedBsProperty("wpx", this.size, this.options);
    if (!isNaN(property)) {
      return "px";
    }

    if (this.args.col?.options?.length > 0) {
      return "cols";
    }

    if (this.numericValue != "") {
      return "px";
    }

    return this.availableUnits[0];
  }

  get options() {
    if (this._options !== null) {
      return this._options;
    }
    return this.args.col?.options ?? [];
  }

  get size() {
    return this.args.size ?? this.args.deviceSize ?? "sm";
  }

  get colWidth() {
    return sizedBsProperty("col", this.size, this.options);
  }

  get property() {
    return this.args.property;
  }

  get numericValue() {
    if (this._numericValue !== null) {
      return this._numericValue;
    }

    if (typeof this.args.index == "number" && Array.isArray(this.args?.value?.[this.size]?.[this.property])) {
      return this.args?.value[this.size][this.property][this.args.index].value;
    }

    if (this.args?.value && this.args?.value[this.size] && this.args?.value[this.size][this.property]) {
      return this.args?.value[this.size][this.property].value;
    }

    const property = sizedBsProperty("wpx", this.size, this.options);
    if (!isNaN(property)) {
      return property;
    }

    return "";
  }

  set numericValue(value) {
    if (isNaN(value)) {
      this._numericValue = "";
      this._options = ["wpx-"];
      return this.triggerChange();
    }

    this._numericValue = parseFloat(value);
    let newOptions = setSizedBsProperty("wpx", this.size, value, this.options);
    this._options = setSizedBsProperty("col", this.size, "default", newOptions);

    this.triggerChange();
  }

  triggerChange() {
    return this.args.onChangeOptions?.(
      this.options,
      this.numericValue,
      this.unit,
      this.size,
      this.args.property,
      this.args.index,
    );
  }

  @action
  dimensionChange(value) {
    this._numericValue = parseInt(value);
    let newOptions = setSizedBsProperty("col", this.size, value, this.options);
    this._options = setSizedBsProperty("wpx", this.size, "default", newOptions);

    return this.triggerChange();
  }

  @action
  unitChange(value) {
    if (value == "cols") {
      this.dimensionChange("default");
    }

    this._unit = value;

    return this.triggerChange();
  }
}
