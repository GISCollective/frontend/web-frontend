/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { fromPageState, toPageState } from 'core/lib/page-state';
import { DateTime } from 'luxon';

export default class StateFiltersDateComponent extends Component {
  @service intl;
  @service clickOutside;

  get title() {
    return this.args.options?.label ?? this.intl.t('date-label');
  }

  get dateValue() {
    if (!this.state.da) {
      return null;
    }

    return DateTime.fromISO(this.state.da);
  }

  get value() {
    if (!this.state.da) {
      return '';
    }

    return this.dateValue.toLocaleString();
  }

  get state() {
    return fromPageState(this.args.state);
  }

  @action
  change(value) {
    const newState = toPageState({
      ...this.state,
      da: value?.startOf?.('day').plus({ hours: 12 }).toISO?.(),
    });

    this.args.onChangeState?.(newState);
    this.clickOutside.unsubscribe();
  }
}
