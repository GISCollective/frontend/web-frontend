/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PageColPictureComponent extends Component {
  @service store;
  @tracked _picture;
  @tracked _pictureOptions;
  @tracked _destination;
  @tracked linkElement;

  get pictureOptions() {
    if (this._pictureOptions) {
      return this._pictureOptions;
    }

    return this.args.value?.data?.picture ?? this.args.value?.data ?? {};
  }

  get picture() {
    if (this._picture) {
      return this._picture;
    }

    const key = this.args.value?.modelKey;
    return this.args.model?.[key] ?? this.args.value?.record;
  }

  get destination() {
    if (this._destination) {
      return this._destination;
    }

    return (
      this.args.value?.data?.destination?.link ??
      this.args.value?.data?.destination
    );
  }

  get containerClasses() {
    if (this.pictureOptions?.size?.sizing) {
      return 'size-fill';
    }
    return '';
  }

  @action
  setupLink(element) {
    if (this.args.isEditor) {
      return;
    }

    this.linkElement = element;
  }

  @action
  onClick() {
    if (this.args.onClick?.()) {
      return;
    }

    this.linkElement?.click();
  }

  @action
  async setupPicture(record) {
    if (!record) {
      return;
    }

    const type = record.constructor?.modelName;

    if (type == 'picture') {
      this._picture = record;
      return;
    }

    await record.pictures;
    this._picture = await record.cover;
  }
}
