/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import {
  render,
  triggerEvent,
  fillIn,
  waitUntil,
  waitFor,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';
import click from '@ember/test-helpers/dom/click';
import { fromLonLat } from 'ol/proj';
import { transform } from 'ol/proj';

describe('Integration | Component | view/campaign-form', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let campaignData;
  let mapData;

  let map;
  let campaign;
  let geolocation;
  let receivedAnswer;

  hooks.before(async function () {
    server = new TestServer();
    campaignData = server.testData.storage.addDefaultCampaign('1');
    mapData = server.testData.storage.addDefaultMap();

    server.testData.storage.addDefaultBaseMap();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultFeature('5ca8c2ecef1f7e01000886f3');
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    this.owner.lookup('service:router').transitionTo = () => {};
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };

    campaign = await this.store.findRecord('campaign', campaignData._id);
    map = await this.store.findRecord('map', mapData._id);

    let positionService = this.owner.lookup('service:position');

    geolocation = {
      watchPosition(callback) {
        callback({
          coords: {
            longitude: 1,
            latitude: 2,
            accuracy: 14,
            altitude: 15,
            altitudeAccuracy: 16,
          },
        });

        return 1;
      },
      clearWatch() {},
    };

    positionService.geolocation = geolocation;
    await positionService.watchPosition();

    receivedAnswer = null;
    server.post(`/mock-server/campaignanswers`, (request) => {
      receivedAnswer = JSON.parse(request.requestBody);
      receivedAnswer.campaignAnswer._id = 'some-answer';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedAnswer),
      ];
    });
  });

  describe('when a fid is in the state', function (hooks) {
    let simpleCampaign;

    hooks.beforeEach(async function () {
      let iconData = server.testData.storage.addDefaultIcon();

      campaignData = {
        _id: '2',
        map: {
          isEnabled: true,
          map: mapData._id,
        },
        article: 'Description text',
        name: 'Campaign 1',
        icons: [],
        area: mapData.area,
        baseMaps: mapData.baseMaps,
        questions: [
          {
            question: 'Location',
            type: 'geo json',
            name: 'position',
            isRequired: false,
            isVisible: true,
            disableDelete: true,
            hasVisibility: true,
            options: {
              allowGps: true,
              allowManual: true,
              allowAddress: true,
              allowExistingFeature: true,
            },
          },
          {
            question: "What's the name of this place?",
            type: 'short text',
            name: 'name',
            isRequired: false,
            isVisible: true,
            disableDelete: true,
            hasVisibility: true,
          },
          {
            question: 'What do you like about this place?',
            type: 'long text',
            name: 'description',
            isRequired: false,
            isVisible: true,
            disableDelete: true,
            hasVisibility: true,
          },
          {
            question: 'Please select an icon from the list',
            type: 'icons',
            name: 'icons',
            isRequired: false,
            isVisible: true,
            disableDelete: true,
            hasVisibility: true,
            options: [iconData._id],
          },
          {
            question: 'Pictures',
            type: 'pictures',
            name: 'pictures',
            isRequired: false,
            isVisible: true,
            disableDelete: true,
            hasVisibility: true,
          },
        ],
      };

      server.testData.storage.addCampaign(campaignData);
      server.testData.storage.addDefaultFeature('5ca8c2ecef1f7e01000886f3');
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultFeature('5ca8c2ecef1f7e01000886aa');

      simpleCampaign = await this.store.findRecord(
        'campaign',
        campaignData._id,
      );
    });

    it('sets the feature location and details', async function () {
      this.set('model', { campaign: simpleCampaign });
      this.set('value', {
        modelKey: 'campaign',
      });
      this.set('state', 'fid@5ca8c2ecef1f7e01000886f3');

      await render(
        hbs`<View::CampaignForm @state={{this.state}} @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitUntil(() => !this.element.querySelector('.is-loading'));

      expect(this.element.querySelector('.feature-details-card')).to.exist;
      expect(
        this.element
          .querySelector('.feature-details-card .card-title')
          .textContent.trim(),
      ).to.equal('Nomadisch Grün - Local Urban Food');

      await PageElements.wait(200);
    });

    it('does not render the feature alert', async function () {
      this.set('model', { campaign: simpleCampaign });
      this.set('value', {
        modelKey: 'campaign',
      });
      this.set('state', 'fid@5ca8c2ecef1f7e01000886f3');

      await render(
        hbs`<View::CampaignForm @state={{this.state}} @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitUntil(() => !this.element.querySelector('.is-loading'));

      expect(this.element.querySelector('.alert-warning')).not.to.exist;
    });

    it('does not render the pick on the map button', async function () {
      this.set('model', { campaign: simpleCampaign });
      this.set('value', {
        modelKey: 'campaign',
      });
      this.set('state', 'fid@5ca8c2ecef1f7e01000886f3');

      await render(
        hbs`<View::CampaignForm @state={{this.state}} @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitUntil(() => !this.element.querySelector('.is-loading'));

      expect(this.element.querySelector('.btn-edit')).not.to.exist;
    });

    it('renders the hover element for the fid', async function () {
      this.set('model', { campaign: simpleCampaign });
      this.set('value', {
        modelKey: 'campaign',
      });
      this.set('state', 'fid@5ca8c2ecef1f7e01000886f3');

      await render(
        hbs`<View::CampaignForm @state={{this.state}} @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitUntil(() => !this.element.querySelector('.is-loading'));

      expect(this.element.querySelector('.hover-overlay')).to.exist;
      expect(
        this.element.querySelector('.hover-overlay img'),
      ).to.have.attribute('src', '/test/5ca7bfbfecd8490100cab97d.jpg');

      let olMap = this.element.querySelector('.ol-map-plain').olMap;
      const view = olMap.getView();

      const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

      expect(center).to.deep.equal([13.433576, 52.495780999999994]);
      expect(view.getZoom()).to.equal(15);
    });

    it('renders the a layer with the campaign map features', async function () {
      this.set('model', { campaign: simpleCampaign });
      this.set('value', {
        modelKey: 'campaign',
      });
      this.set('state', 'fid@5ca8c2ecef1f7e01000886f3');

      await render(
        hbs`<View::CampaignForm @state={{this.state}} @model={{this.model}} @value={{this.value}}/>`,
      );

      await waitUntil(() => !this.element.querySelector('.is-loading'));

      expect(
        this.element.querySelector('.vector-tiles').textContent.trim(),
      ).to.equal(
        '/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c',
      );
    });

    it('triggers on change when another feature is selected', async function () {
      this.set('model', { campaign: simpleCampaign });
      this.set('value', {
        modelKey: 'campaign',
      });
      this.set('state', 'fid@5ca8c2ecef1f7e01000886f3');

      let point;
      let details;

      this.set('change', (p, d) => {
        point = p;
        details = d;
      });

      await render(
        hbs`<View::CampaignForm @state={{this.state}} @model={{this.model}} @value={{this.value}} @onChange={{this.change}}/>`,
      );

      await waitUntil(() => !this.element.querySelector('.is-loading'));

      expect(this.element.querySelector('.map-container.fullscreen')).not.to
        .exist;

      await click('.picker-options');

      const vtElement = this.element.querySelector('.vector-tiles');
      expect(vtElement.textContent.trim()).to.equal(
        '/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c',
      );

      vtElement.selectFeature({
        getGeometry: () => ({
          getExtent: () => [
            1502287.3822740193, 6886844.872352015, 1502287.3822740193,
            6886844.872352015,
          ],
        }),
        getProperties: () => ({
          _id: '5ca8c2ecef1f7e01000886aa',
          id: '5ca8c2ecef1f7e01000886aa',
        }),
      });

      await PageElements.wait(200);
    });

    it('submits the form with the feature details', async function (a) {
      this.set('model', { campaign: simpleCampaign });
      this.set('value', {
        modelKey: 'campaign',
      });
      this.set('state', 'fid@5ca8c2ecef1f7e01000886f3');

      let point;
      let details;

      this.set('change', (p, d) => {
        point = p;
        details = d;
      });

      await render(
        hbs`<View::CampaignForm @state={{this.state}} @model={{this.model}} @value={{this.value}} @onChange={{this.change}}/>`,
      );

      await waitUntil(() => !this.element.querySelector('.is-loading'));

      expect(this.element.querySelector('.map-container.fullscreen')).not.to
        .exist;

      await click('.picker-options');

      const vtElement = this.element.querySelector('.vector-tiles');

      vtElement.selectFeature({
        getGeometry: () => ({
          getExtent: () => [
            1502287.3822740193, 6886844.872352015, 1502287.3822740193,
            6886844.872352015,
          ],
        }),
        getProperties: () => ({
          _id: '5ca8c2ecef1f7e01000886aa',
          id: '5ca8c2ecef1f7e01000886aa',
        }),
      });

      await click('.btn-submit-location');
      await click('.btn-submit-campaign');

      await waitUntil(() => receivedAnswer?.campaignAnswer);

      a.deepEqual(receivedAnswer.campaignAnswer, {
        position: {
          type: 'Point',
          coordinates: [13.495277166366577, 52.47704311945856],
        },
        attributes: {
          'position details': {
            altitude: 0,
            accuracy: 1000,
            altitudeAccuracy: 1000,
            type: 'feature',
            id: '5ca8c2ecef1f7e01000886aa',
          },
        },
        campaign: '2',
        icons: [],
        featureId: null,
        _id: 'some-answer',
      });
    });
  });

  describe('the campaign questions', function (hooks) {
    let icon1;
    let icon2;
    let icon3;
    let receivedPictures;
    let receivedSounds;

    hooks.beforeEach(async function () {
      let map = server.testData.storage.addDefaultMap();
      server.testData.storage.addDefaultBaseMap();

      icon1 = server.testData.create.icon('000000000000000000000001');
      icon1.name = 'icon 1';
      icon1.localName = 'local icon 1';
      icon1.attributes = [
        {
          name: 'attribute1',
          help: 'some help message',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon1);

      icon2 = server.testData.create.icon('000000000000000000000002');
      icon2.name = 'icon 2';
      icon2.localName = 'local icon 2';
      icon2.allowMany = true;
      icon2.attributes = [
        {
          name: 'attribute1',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon2);

      icon3 = server.testData.create.icon('000000000000000000000003');
      icon3.name = 'icon 3';
      icon3.localName = 'local icon 3';
      icon3.allowMany = true;
      icon3.attributes = [
        {
          name: 'attribute1',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon3);

      campaign = server.testData.create.campaign();
      campaign.icons = [icon1._id, icon2._id];
      campaign.area = map.area;
      campaign.baseMaps = map.baseMaps;

      campaign.questions = campaign.questions.map((a) => ({
        ...a,
        isRequired: false,
        options: a.name == 'icons' ? [icon3._id] : a.options,
      }));

      receivedPictures = [];
      server.post(
        '/mock-server/pictures',
        (request) => {
          const pictureRequest = JSON.parse(request.requestBody);
          pictureRequest.picture['_id'] = receivedPictures.length + 1;
          receivedPictures.push(pictureRequest);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(pictureRequest),
          ];
        },
        false,
      );
      server.put(
        '/mock-server/pictures/:id',
        (request) => {
          const pictureRequest = JSON.parse(request.requestBody);
          pictureRequest.picture['_id'] = request.params.id;
          receivedPictures.push(pictureRequest);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(pictureRequest),
          ];
        },
        false,
      );

      receivedSounds = [];
      server.post(
        '/mock-server/sounds',
        (request) => {
          const soundRequest = JSON.parse(request.requestBody);
          soundRequest.sound['_id'] = receivedSounds.length + 1;
          receivedSounds.push(soundRequest);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(soundRequest),
          ];
        },
        false,
      );
      server.put(
        '/mock-server/sounds/:id',
        (request) => {
          const soundRequest = JSON.parse(request.requestBody);
          soundRequest.sound['_id'] = request.params.id;
          receivedSounds.push(soundRequest);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(soundRequest),
          ];
        },
        false,
      );
    });

    describe('a campaign with required pictures', function (hooks) {
      hooks.beforeEach(async function () {
        campaign.questions = campaign.questions.map((a) => ({
          ...a,
          isRequired: a.name == 'pictures',
        }));

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
          data: {
            submit: {
              classes: ['btn-primary'],
            },
          },
        });

        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);
        await click('.btn-gps');
      });

      it('renders the required star', async function () {
        expect(
          this.element.querySelector('.attribute-value-pictures .required'),
        ).to.exist;
      });

      it('renders a disabled submit button', async function () {
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.classes('btn-secondary');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).not.to.have.classes('btn-primary');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('disabled', '');
      });

      it('can set and submit the about questions', async function () {
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('disabled', '');

        const blob = server.testData.create.pngBlob();

        await triggerEvent(
          ".attribute-value-pictures [type='file']",
          'change',
          { files: [blob] },
        );

        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).not.to.have.attribute('disabled', '');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).not.to.have.classes('btn-secondary');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.classes('btn-primary');
      });
    });

    describe('a campaign with required sounds', function (hooks) {
      hooks.beforeEach(async function () {
        campaign.questions = campaign.questions.map((a) => ({
          ...a,
          isRequired: a.name == 'sounds',
        }));

        campaign.questions.push({
          question: 'Sounds',
          help: '',
          type: 'sounds',
          name: 'sounds',
          isRequired: true,
          isVisible: true,
          options: {},
          value: undefined,
        });

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
        });

        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);
        await click('.btn-gps');
      });

      it('can set and submit the about questions', async function (a) {
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('disabled', '');

        const blob = server.testData.create.mp3Blob();

        await triggerEvent(".attribute-value-sounds [type='file']", 'change', {
          files: [blob],
        });

        await waitUntil(
          (a) => !this.element.querySelector('.btn-submit-campaign[disabled]'),
        );

        await click('.btn-submit-campaign');

        a.deepEqual(receivedAnswer.campaignAnswer.sounds, ['1']);
      });
    });

    describe('a campaign with required name and description', function (hooks) {
      hooks.beforeEach(async function () {
        campaign.questions = campaign.questions.map((a) => ({
          ...a,
          isRequired: a.name == 'name' || a.name == 'description',
        }));

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
        });
      });

      it('can set and submit the about questions', async function (a) {
        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);
        await click('.btn-gps');

        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('disabled', '');

        await fillIn('.attribute-value-name input', 'name');

        await waitFor('.editor-is-ready', { timeout: 2000 });

        await fillIn(
          '.attribute-value-description .ce-paragraph',
          'description',
        );

        await waitUntil(
          (a) => !this.element.querySelector('.btn-submit-campaign[disabled]'),
          { timeout: 3000 },
        );

        await click('.btn-submit-campaign');

        a.deepEqual(receivedAnswer.campaignAnswer.attributes.about, {
          description: {
            blocks: [
              {
                data: {
                  text: 'description',
                },
                type: 'paragraph',
              },
            ],
          },
          name: 'name',
        });
      });

      it('can apply page col properties to the form labels', async function (a) {
        this.set('value', {
          modelKey: 'campaign',
          name: 'test-name',
          data: {
            labels: {
              color: 'primary',
              classes: ['fw-bold'],
            },
          },
        });

        await render(
          hbs`<View::CampaignForm @path="test.path" @model={{this.model}} @value={{this.value}} />`,
        );
        await PageElements.wait(150);

        expect(this.element.querySelector('.form-label')).to.have.classes(
          'fw-bold text-primary',
        );
        expect(this.element.querySelector('.form-label')).to.have.attribute(
          'data-path',
          'test.path.data.labels',
        );
        expect(this.element.querySelector('.form-label')).to.have.attribute(
          'data-type',
          'label',
        );
        expect(this.element.querySelector('.form-label')).to.have.attribute(
          'data-editor',
          'text-with-options',
        );
        expect(this.element.querySelector('.form-label')).to.have.attribute(
          'data-name',
          'test-name',
        );
      });

      it.skip('can apply page col properties to the submit button', async function (a) {
        this.set('value', {
          modelKey: 'campaign',
          name: 'test-name',
          data: {
            labels: {
              color: 'primary',
              classes: ['fw-bold'],
            },
            submit: {
              classes: ['btn-primary', 'fw-bold', 'text-size-09'],
            },
          },
        });

        await render(
          hbs`<View::CampaignForm @path="test.path" @model={{this.model}} @value={{this.value}} />`,
        );

        await fillIn('.attribute-value-name input', 'name');
        await fillIn('.ce-paragraph', 'other');
        await click('.btn-gps');
        await waitUntil(
          () =>
            !this.element.querySelector('.btn-submit').hasAttribute('disabled'),
        );
        await PageElements.wait(150);

        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.classes('btn-primary');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('data-path', 'test.path.data.submit');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('data-type', 'submit button');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('data-editor', 'button-style');
        expect(
          this.element.querySelector('.btn-submit-campaign'),
        ).to.have.attribute('data-name', 'test-name');
      });

      it('can apply page col properties to the location label', async function (a) {
        this.set('value', {
          modelKey: 'campaign',
          name: 'test-name',
          data: {
            locationLabels: {
              color: 'primary',
              classes: ['fw-bold'],
            },
          },
        });

        await render(
          hbs`<View::CampaignForm @path="test.path" @model={{this.model}} @value={{this.value}} />`,
        );

        expect(
          this.element.querySelector('.form-label-position-details'),
        ).to.have.classes('fw-bold text-primary');
        expect(
          this.element.querySelector('.form-label-position-details'),
        ).to.have.attribute('data-path', 'test.path.data.locationLabels');
        expect(
          this.element.querySelector('.form-label-position-details'),
        ).to.have.attribute('data-type', 'location label');
        expect(
          this.element.querySelector('.form-label-position-details'),
        ).to.have.attribute('data-editor', 'text-with-options');
        expect(
          this.element.querySelector('.form-label-position-details'),
        ).to.have.attribute('data-name', 'test-name');
      });
    });

    describe('a campaign with no custom labels', (hooks) => {
      hooks.beforeEach(async function () {
        campaign.area = map.area;
        campaign.baseMaps = map.baseMaps;

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
        });

        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);

        await click('.btn-gps');
      });

      it('does not render the about section', function () {
        expect(this.element.querySelector('.group-about')).not.to.exist;
      });

      it('should show all the icon questions', async function (a) {
        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn('.card-icon-2 input', 'value 2');

        await click('.btn-submit');

        a.deepEqual(receivedAnswer.campaignAnswer, {
          position: { type: 'Point', coordinates: [1, 2] },
          attributes: {
            'icon 1': { attribute1: 'value 1' },
            'icon 2': [{ attribute1: 'value 2' }],
            'position details': {
              altitude: 15,
              accuracy: 14,
              altitudeAccuracy: 16,
              type: 'gps',
            },
          },
          campaign: '5ca78aa160780601008f6aaa',
          icons: ['000000000000000000000001', '000000000000000000000002'],
          featureId: null,
          _id: 'some-answer',
        });
      });

      it('should allow adding a second icon instance', async function () {
        await click('.btn-add-icon-2');

        const inputs = this.element.querySelectorAll('.card-icon-2 input');
        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn(inputs[0], 'value 2');
        await fillIn(inputs[1], 'value 3');

        await click('.btn-submit');

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [1, 2] },
            attributes: {
              'position details': {
                altitude: 15,
                accuracy: 14,
                altitudeAccuracy: 16,
                type: 'gps',
              },
              'icon 1': { attribute1: 'value 1' },
              'icon 2': [{ attribute1: 'value 2' }, { attribute1: 'value 3' }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
            featureId: null,
          },
        });
      });

      it('should allow selecting an optional icon', async function (a) {
        const header = this.element.querySelector(
          '.attribute-value-icons label',
        );
        expect(header.textContent.trim()).to.equal(
          'Please select an icon from the list',
        );

        await click('.input-icons-list-toggle .icon-element');

        await fillIn(
          '.group-icon-3 .attribute-value-local-attribute1 input',
          'test',
        );

        await click('.btn-submit');

        a.deepEqual(receivedAnswer, {
          campaignAnswer: {
            position: { type: 'Point', coordinates: [1, 2] },
            attributes: {
              'position details': {
                altitude: 15,
                accuracy: 14,
                altitudeAccuracy: 16,
                type: 'gps',
              },
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
              'icon 3': [{ attribute1: 'test' }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: [
              '000000000000000000000001',
              '000000000000000000000002',
              '000000000000000000000003',
            ],
            _id: 'some-answer',
            featureId: null,
          },
        });
      });

      it('should not add an icon and its attributes when selecting an optional icon twice', async function () {
        await click('.input-icons-list-toggle .icon-element');
        await click('.input-icons-list-toggle .icon-element');
        await click('.btn-submit');

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [1, 2] },
            attributes: {
              'position details': {
                altitude: 15,
                accuracy: 14,
                altitudeAccuracy: 16,
                type: 'gps',
              },
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
            featureId: null,
          },
        });
      });

      it('deleting a second icon instance should delete the attributes', async function (a) {
        await click('.btn-add-icon-2');

        const inputs = this.element.querySelectorAll('.card-icon-2 input');
        const containers = this.element.querySelectorAll(
          '.group-container.group-icon-2',
        );

        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn(inputs[0], 'value 2');
        await fillIn(inputs[1], 'value 3');
        await click(containers[1].querySelector('.group-icon-2 .btn-delete'));

        await click('.btn-submit');

        a.deepEqual(receivedAnswer, {
          campaignAnswer: {
            position: { type: 'Point', coordinates: [1, 2] },
            attributes: {
              'position details': {
                altitude: 15,
                accuracy: 14,
                altitudeAccuracy: 16,
                type: 'gps',
              },
              'icon 1': { attribute1: 'value 1' },
              'icon 2': [{ attribute1: 'value 2' }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
            featureId: null,
          },
        });
      });

      it('should allow adding 2 pictures', async function (a) {
        const blob = server.testData.create.pngBlob();

        await triggerEvent(
          ".attribute-value-pictures [type='file']",
          'change',
          { files: [blob] },
        );

        await waitUntil(
          () => this.element.querySelectorAll('.image-list img').length > 0,
        );

        await triggerEvent(
          ".attribute-value-pictures [type='file']",
          'change',
          { files: [blob] },
        );

        await waitUntil(
          () => this.element.querySelectorAll('.image-list img').length > 1,
        );

        await click('.btn-submit');

        a.deepEqual(receivedAnswer, {
          campaignAnswer: {
            position: { type: 'Point', coordinates: [1, 2] },
            attributes: {
              'position details': {
                altitude: 15,
                accuracy: 14,
                altitudeAccuracy: 16,
                type: 'gps',
              },
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            pictures: ['1', '2'],
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
            featureId: null,
          },
        });

        expect(receivedPictures.length).to.equal(2);
      });

      it('should allow changing the position', async function () {
        await waitUntil(() =>
          this.element.querySelector('.group-position-details'),
        );

        await click('.picker-options');

        let olMap = this.element.querySelector('.ol-map-plain').olMap;

        olMap.getView().setZoom(16);
        olMap.getView().setCenter(fromLonLat([45, 0]));

        await click('.btn-submit');
        await click('.btn-submit');

        await waitUntil(() => receivedAnswer);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [45, +0] },
            attributes: {
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
              'position details': {
                altitude: +0,
                accuracy: 1000,
                altitudeAccuracy: 1000,
                type: 'manual',
              },
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            featureId: null,
            _id: 'some-answer',
          },
        });

        expect(receivedPictures.length).to.equal(0);
      });
    });

    describe('a campaign with contributor questions', (hooks) => {
      hooks.beforeEach(async function () {
        campaign.area = map.area;
        campaign.baseMaps = map.baseMaps;
        campaign.contributorQuestions = [
          {
            question: 'custom question',
            help: 'custom help',
            type: 'email',
            name: 'email',
            isVisible: true,
            isRequired: true,
          },
          {
            question: 'your name',
            type: 'short text',
            name: 'name',
            isVisible: true,
            isRequired: true,
          },
        ];

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
        });

        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);

        await click('.btn-gps');
      });

      it('renders the contributor questions', async function (a) {
        expect(this.element.querySelector('.about-you-section')).to.exist;
        const inputs = [
          ...this.element.querySelectorAll(
            '.about-you-section .attribute-value',
          ),
        ];

        expect(inputs).to.have.length(2);

        const label = inputs.map((a) => a.querySelector('label'));

        expect(label[0]).to.have.textContent('custom question *');
        expect(label[1]).to.have.textContent('your name *');

        expect(this.element.querySelector('.btn-submit')).to.have.attribute(
          'disabled',
          '',
        );
      });

      it('can submit the contributor questions', async function (a) {
        let inputs = [
          ...this.element.querySelectorAll(
            '.about-you-section .attribute-value input',
          ),
        ];
        await fillIn(inputs[0], 'a@b.c');

        inputs = [
          ...this.element.querySelectorAll(
            '.about-you-section .attribute-value input',
          ),
        ];
        await fillIn(inputs[1], 'some name');

        await click('.btn-submit-campaign');

        await waitUntil(() => receivedAnswer);

        a.deepEqual(receivedAnswer.campaignAnswer.contributor, {
          email: 'a@b.c',
          name: 'some name',
        });
      });
    });

    describe('a campaign without contributor questions', (hooks) => {
      hooks.beforeEach(async function () {
        campaign.area = map.area;
        campaign.baseMaps = map.baseMaps;
        campaign.contributorQuestions = [
          {
            question: 'custom question',
            help: 'custom help',
            type: 'email',
            name: 'email',
            isVisible: false,
            isRequired: true,
          },
        ];

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
        });

        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);

        await click('.btn-gps');
      });

      it('does not render the about you section', async function (a) {
        expect(this.element.querySelector('.about-you-section')).not.to.exist;
      });
    });

    describe('a campaign with extra custom questions', (hooks) => {
      hooks.beforeEach(async function () {
        campaign.area = map.area;
        campaign.baseMaps = map.baseMaps;
        campaign.questions.push({
          question: 'extra info?',
          help: '',
          type: 'short text',
          name: 'xtra',
          isRequired: true,
          isVisible: true,
          options: {},
          value: undefined,
        });

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
        });

        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);

        await click('.btn-gps');
      });

      it('can add the extra answer', async function (a) {
        await fillIn('.attribute-value-xtra input', 'yes');

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer);

        a.deepEqual(receivedAnswer.campaignAnswer.attributes, {
          'icon 1': {
            attribute1: null,
          },
          'icon 2': [
            {
              attribute1: null,
            },
          ],
          other: {
            xtra: 'yes',
          },
          'position details': {
            accuracy: 14,
            altitude: 15,
            altitudeAccuracy: 16,
            type: 'gps',
          },
        });
      });
    });

    describe('a campaign with stream discharge questions', (hooks) => {
      hooks.beforeEach(async function () {
        campaign.area = map.area;
        campaign.baseMaps = map.baseMaps;
        campaign.questions.push({
          question: 'extra info?',
          help: '',
          type: 'stream discharge',
          name: 'xtra',
          isRequired: true,
          isVisible: true,
          options: {},
          value: undefined,
        });

        server.testData.storage.addCampaign(campaign);

        const campaignModel = await this.owner
          .lookup('service:store')
          .findRecord('campaign', campaign._id);

        this.set('model', { campaign: campaignModel });
        this.set('value', {
          modelKey: 'campaign',
        });

        await render(
          hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
        );
        await PageElements.wait(150);

        await click('.btn-gps');
      });

      it('can add the extra answer', async function (a) {
        await fillIn('.input-velocity', '2');
        await fillIn('.input-width', '3');
        await fillIn('.input-depth', '4');

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer);

        a.deepEqual(receivedAnswer.campaignAnswer.attributes, {
          'icon 1': {
            attribute1: null,
          },
          'icon 2': [
            {
              attribute1: null,
            },
          ],
          other: {
            xtra: {
              data: {
                depth: 4,
                result: 24,
                velocity: 2,
                width: 3,
              },
              type: 'stream discharge',
            },
          },
          'position details': {
            accuracy: 14,
            altitude: 15,
            altitudeAccuracy: 16,
            type: 'gps',
          },
        });
      });
    });
  });

  describe('for a public campaign that needs registration', function (hooks) {
    hooks.beforeEach(async function () {
      campaign = server.testData.create.campaign();
      campaign.options = {
        registrationMandatory: true,
      };
      server.testData.storage.addCampaign(campaign);
      const campaignModel = await this.owner
        .lookup('service:store')
        .findRecord('campaign', campaign._id);

      this.set('model', { campaign: campaignModel });
      this.set('value', {
        modelKey: 'campaign',
      });

      await render(
        hbs`<View::CampaignForm @model={{this.model}} @value={{this.value}}/>`,
      );
      await PageElements.wait(150);
    });

    it('shows a message that the user needs to be registered', async function () {
      expect(this.element.querySelector('.alert-warning')).to.exist;
      expect(this.element.querySelector('.alert-link')).to.exist;
      expect(this.element.querySelector('.alert-link')).to.have.attribute(
        'href',
        '/login',
      );
    });
  });
});
