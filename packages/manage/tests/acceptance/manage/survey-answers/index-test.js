/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { visit, currentURL, waitFor, click } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | manage/survey answers/index', function (hooks) {
  setupApplicationTest(hooks, { server: true });
  let server;

  hooks.beforeEach(async function () {
    server = new TestServer();

    let user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    const profile = server.testData.storage.addDefaultProfile();
    server.testData.storage.addDefaultUser(false, profile._id);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultBaseMap();
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultFeature('5ca78e2160780601008f69e6');

    let picture = server.testData.storage.addDefaultPicture();
    let campaign = server.testData.storage.addDefaultCampaign();
    let icon = server.testData.storage.addDefaultIcon();

    let answer = server.testData.storage.addDefaultCampaignAnswer();
    answer.icons = [icon._id];
    answer.info.originalAuthor = profile._id;
    answer.pictures = [picture._id];
    campaign.icons = [icon._id];

    server.testData.storage.addDefaultUser(answer.info.originalAuthor);
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('can visit /manage/survey-answers', async function () {
    authenticateSession();

    await visit(`/manage/survey-answers`);
    expect(currentURL()).to.equal(`/manage/survey-answers`);
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/survey-answers`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fsurvey-answers');
  });

  it('shows a list of answers', async function () {
    authenticateSession();

    await visit(`/manage/survey-answers`);

    expect(this.element.querySelector('.answer-name').textContent.trim()).to.equal('some test');
  });

  it('can navigate the answer details on click', async function () {
    authenticateSession();

    await visit(`/manage/survey-answers`);

    await waitFor('.answer-name');
    await click('.answer-name');

    expect(currentURL()).to.equal('/manage/survey-answers/5ca78aa160780601008f6bbb');
  });
});
