/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/authenticated-admin-route';
import { service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminIntegrationsRoute extends Route {
  @service preferences;

  model() {
    const fields = ['nominatim', 'google.client_id', 'google.client_secret', 'slack.webhook'];
    const items = {};

    fields.forEach((key) => {
      items[key.replace('.', '')] = this.preferences.requestPreference(`integrations.${key}`);
    });

    return hash(items);
  }
}
