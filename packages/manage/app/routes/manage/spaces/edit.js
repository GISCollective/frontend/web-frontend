/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageSpacesEditRoute extends AuthenticatedRoute {
  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params) {
    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    const space = await this.store.findRecord('space', params.id);
    const team = await space.visibility.fetchTeam();

    return hash({
      teams: this.store.query('team', teamsParams),
      team,
      pages: this.store.query('page', { space: params.id }),
      space,
    });
  }
}
