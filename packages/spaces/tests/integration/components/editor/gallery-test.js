/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/gallery", function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultFeature("1");
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the current value", async function () {
    this.set("value", {
      primaryPhoto: "unique",
      showPrimaryPhotoAttributions: true,
      showPhotoList: true,
    });

    await render(hbs`<Editor::Gallery @value={{this.value}} />`);

    expect(this.element.querySelector(".primary-photo").value).to.equal("unique");
    expect(this.element.querySelector(".show-primary-photo-attributions").value).to.equal("yes");
    expect(this.element.querySelector(".show-photo-list").value).to.equal("yes");
  });

  it("can change the values", async function () {
    this.set("value", {});

    let changedValue;
    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Gallery @value={{this.value}} @onChange={{this.onChange}}/>`);

    this.element.querySelector(".primary-photo").value = "unique";
    await triggerEvent(".primary-photo", "change");

    this.element.querySelector(".show-primary-photo-attributions").value = "yes";
    await triggerEvent(".show-primary-photo-attributions", "change");

    this.element.querySelector(".show-photo-list").value = "yes";
    await triggerEvent(".show-photo-list", "change");

    expect(changedValue).to.deep.equal({
      primaryPhoto: "unique",
      showPrimaryPhotoAttributions: true,
      showPhotoList: true,
    });
  });
});
