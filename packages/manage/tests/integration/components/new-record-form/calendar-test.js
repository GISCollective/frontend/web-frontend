/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent, typeIn, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';

describe('Integration | Component | new-record-form/calendar', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let receivedCalendar;
  let calendarData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    let team1 = server.testData.storage.addDefaultTeam();
    team1.allowEvents = true;

    calendarData = server.testData.storage.addDefaultCalendar();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders a disabled add button by default', async function () {
    await render(hbs`<NewRecordForm::Calendar />`);

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');
  });

  it('can submit a new calendar when the fields are filled in', async function () {
    this.set('calendar', await this.store.createRecord('calendar', { visibility: {} }));

    this.set('save', (v) => {
      receivedCalendar = v.serialize();
    });

    await render(hbs`<NewRecordForm::Calendar @model={{this.calendar}} @onSubmit={{this.save}} />`);

    await PageElements.waitEditorJs(this.element);

    await fillIn('.form-control-name', 'new calendar');

    await fillIn('.ce-paragraph', '');
    await typeIn('.ce-paragraph', 'test message');
    await PageElements.wait(510);

    this.element.querySelector('.row-type select').value = 'schedules';
    await triggerEvent('.row-type select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedCalendar);

    expect(receivedCalendar).to.deep.equal({
      name: 'new calendar',
      article: { blocks: [{ type: 'paragraph', data: { text: 'test message' } }] },
      iconSets: undefined,
      map: { isEnabled: false, map: '' },
      type: 'schedules',
      attributes: [],
      visibility: { isPublic: false, isDefault: false, team: '5ca78e2160780601008f69e6' },
      info: undefined,
      cover: null,
      recordName: null,
    });
  });

  it('allows setting a team when there are 2', async function () {
    let team2 = server.testData.storage.addDefaultTeam('2');
    team2.allowEvents = true;

    this.set('calendar', await this.store.createRecord('calendar', { visibility: {} }));

    this.set('save', (v) => {
      receivedCalendar = v.serialize();
    });

    await render(hbs`<NewRecordForm::Calendar @model={{this.calendar}} @onSubmit={{this.save}} />`);

    await PageElements.waitEditorJs(this.element);

    await fillIn('.form-control-name', 'new calendar');

    await fillIn('.ce-paragraph', '');
    await typeIn('.ce-paragraph', 'test message');
    await PageElements.wait(510);

    this.element.querySelector('.row-type select').value = 'schedules';
    await triggerEvent('.row-type select', 'change');

    this.element.querySelector('.row-team select').value = '2';
    await triggerEvent('.row-team select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedCalendar);

    expect(receivedCalendar.visibility).to.deep.equal({ isPublic: false, isDefault: false, team: '2' });
  });
});
