/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { GeoJson } from '../lib/geoJson';

export default class GeoTransform extends Transform {
  deserialize(serialized) {
    return new GeoJson(serialized);
  }

  serialize(deserialized) {
    if (!deserialized || typeof deserialized != 'object') {
      return null;
    }

    if (deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}
