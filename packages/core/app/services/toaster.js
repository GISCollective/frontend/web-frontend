/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service, { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';

class ToastDetails {
  @tracked code;
  @tracked message;
  @tracked serialized;
  @tracked localMessage;
}

export class Toast {
  @tracked title;
  @tracked level;
  @tracked icon;
  @tracked canClose;
  @tracked message;
  @tracked details;
  @tracked index;
}

export default class ToasterService extends Service {
  @service intl;
  @tracked toasts = A([]);
  @tracked index = 0;

  handleLocationError(error) {
    let details = new ToastDetails();

    details.code = error.code;
    details.message = error.message;
    details.serialized = JSON.stringify(error);

    switch (error.code) {
      case error.PERMISSION_DENIED:
        details.localMessage = this.intl.t('geo-location-permission-denied');
        break;

      case error.POSITION_UNAVAILABLE:
        details.localMessage = this.intl.t('geo-location-permission-unavailable');
        break;

      case error.TIMEOUT:
        details.localMessage = this.intl.t('geo-location-permission-timeout');
        break;

      default:
        details.localMessage = this.intl.t('geo-location-unknown-error');
        break;
    }

    const toast = new Toast();
    (toast.title = this.intl.t('Location error')),
      (toast.level = 'danger'),
      (toast.icon = 'location-arrow'),
      (toast.canClose = true),
      (toast.message = details.localMessage),
      (toast.details = details);

    this.addToast(toast);

    return details.localMessage;
  }

  handleError(err) {
    let details = new ToastDetails();
    let message = this.intl.t('An unknown error occurred.');

    if (err && err['errors']) {
      return err.errors.forEach((error) => {
        this.handleError(error);
      });
    }

    if (err.message) {
      message = err.message;
    }

    details.code = err.code;
    details.message = message;

    try {
      details.serialized = JSON.stringify(err);
    } catch (err) {
      /// ignored
    }
    const toast = new Toast();
    toast.title = this.intl.t('error');
    toast.level = 'danger';
    toast.message = message;
    toast.details = details;

    this.addToast(toast);
  }

  addToast(toast) {
    toast.index = this.index;
    this.toasts.push(toast);
    this.index++;
  }

  remove(index) {
    let matchedToasts = this.toasts.filter((a) => a.index == index);
    this.toasts.removeObjects(matchedToasts);
  }
}
