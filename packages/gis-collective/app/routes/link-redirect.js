/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class LinkRedirectRoute extends Route {
  @service store;
  @service fastboot;
  @service embed;

  queryParams = {
    id: {
      refreshModel: true,
    },
  };

  beforeModel() {
    this.embed.isEnabled = true;
  }

  async model(params) {
    const link = await this.store.findRecord('article-link', params.id);

    if (this.fastboot.isFastBoot) {
      this.fastboot.response.headers.set('location', link.url);
      this.fastboot.response.statusCode = 302;
    } else {
      window.location.assign(link.url);
    }

    return link.url;
  }
}
