/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class IconSpriteTransform extends Transform {
  deserialize(serialized) {
    return new IconSprite(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() || deserialized || {};
  }
}

class IconSprite {
  @tracked smallId;
  @tracked largeId;

  constructor(data) {
    if (data?.small) {
      this.smallId = data.small;
    }

    if (data?.large) {
      this.largeId = data.large;
    }
  }

  toJSON() {
    const result = {};

    if (this.smallId) {
      result.small = this.smallId;
    }

    if (this.largeId) {
      result.large = this.largeId;
    }

    return result;
  }
}
