/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AttributesValueOptionsComponent from './value-options';
import { action } from '@ember/object';
import { A } from 'models/lib/array';

export default class AttributesValueOptionsMultipleComponent extends AttributesValueOptionsComponent {
  selectMessage = 'select one or more values from the list';

  get value() {
    if (!this.args.value) {
      return [];
    }

    if (this.args.value.slice) {
      return this.args.value.slice();
    }

    return this.args.value;
  }

  get hasValues() {
    if (!Array.isArray(this.value)) {
      return false;
    }

    return this.value.length > 0;
  }

  isSelected(option, value) {
    let values = [];

    if (value) {
      values = this.value;
    }

    values = values.map((a) => {
      if (a.id) {
        return a.id;
      }

      return a;
    });

    if (option.id) {
      return values.filter((a) => a == option.id).length > 0;
    }

    return values.filter((a) => a == option).length > 0;
  }

  raiseChange(newValue) {
    this.args.onChange?.(newValue);
  }

  removeValue(value) {
    let newValues = [];

    if (this.value) {
      newValues = this.value;
    }

    if (value.id) {
      newValues = newValues.filter((a) => a.id != value.id);
    }

    this.raiseChange(A(newValues));
  }

  addValue(value) {
    let newValues = [];

    if (this.args.value) {
      for (let item of this.value) {
        newValues.push(item);
      }
    }

    newValues.push(value);

    this.raiseChange(A(newValues));
  }

  @action
  select(option) {
    if (option.selected) {
      return this.removeValue(option.value);
    }

    return this.addValue(option.value);
  }
}
