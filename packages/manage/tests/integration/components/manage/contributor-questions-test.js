/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | manage/contributor-questions', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders the email question by default', async function (assert) {
      this.set('value', [
        {
          type: 'email',
          name: 'email',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::ContributorQuestions @title="test" @value={{this.value}} />`);

      const questions = this.element.querySelectorAll('.question-item');

      expect(questions).to.have.length(1);

      expect(this.element.querySelector('.question')).to.have.textContent('What is your email? *');
      expect(this.element.querySelector('.help')).to.have.textContent(
        'We need your email to let you know if we have questions about your contribution and to let you know about the progress.',
      );
      expect(this.element.querySelector('.type')).to.have.textContent('email');
    });

    it('does not render the email question when it is hidden', async function (assert) {
      this.set('value', [
        {
          type: 'email',
          name: 'email',
          isRequired: true,
          isVisible: false,
        },
      ]);

      await render(hbs`<Manage::ContributorQuestions @title="test" @value={{this.value}} />`);

      expect(this.element.querySelector('.question-item')).not.to.exist;
      expect(this.element.querySelector('.question')).not.to.exist;
      expect(this.element.querySelector('.alert.alert-warning')).to.have.textContent(
        "There are no questions about the contributor's identity. The answer will be added anonymously.",
      );
    });
  });

  describe('in edit mode', function () {
    it('renders the email question by default', async function (assert) {
      await render(hbs`<Manage::ContributorQuestions @title="test" @editablePanel="test" />`);

      expect(this.element.querySelector('.alert-info')).to.have.textContent(
        "The user won't be asked to enter an email.",
      );
    });

    it('renders a custom email question', async function (assert) {
      this.set('value', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'email',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Manage::ContributorQuestions @title="test" @value={{this.value}} @editablePanel="test" />`);

      expect(this.element.querySelector('.text-question').value).to.equal('custom question');
      expect(this.element.querySelector('.text-help').value).to.equal('custom help');
    });

    it('adds an email question if the list has none', async function (assert) {
      this.set('value', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'other',
          isRequired: true,
        },
      ]);

      await render(hbs`<Manage::ContributorQuestions @title="test" @value={{this.value}} @editablePanel="test" />`);

      const questions = [...this.element.querySelectorAll('.input-question')];
      expect(questions).to.have.length(1);

      expect(this.element.querySelector('.alert-info')).to.have.textContent(
        "The user won't be asked to enter an email.",
      );
      expect(questions[0].querySelector('.text-question').value).to.equal('custom question');
    });

    it('can add a new question', async function (a) {
      let value;

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::ContributorQuestions @title="test" @value={{this.value}} @editablePanel="test" @onSave={{this.save}} />`,
      );

      await click('.btn-toggle-visibility');
      await click('.btn-add-question');

      const questions = [...this.element.querySelectorAll('.input-question')];

      expect(questions).to.have.length(2);

      await fillIn(questions[1].querySelector('.text-question'), 'new question');
      await fillIn(questions[1].querySelector('.text-name'), 'question');

      await click('.btn-submit');

      a.deepEqual(value, [
        {
          question: 'What is your email?',
          help: 'We need your email to let you know if we have questions about your contribution and to let you know about the progress.',
          type: 'email',
          name: 'email',
          options: undefined,
          isRequired: true,
          isVisible: true,
          disableDelete: true,
          hasVisibility: true,
        },
        {
          question: 'new question',
          name: 'question',
          help: undefined,
          type: undefined,
          isRequired: undefined,
          isVisible: true,
          options: undefined,
        },
      ]);
    });

    it('can not delete the email question', async function (assert) {
      await render(hbs`<Manage::ContributorQuestions @title="test" @editablePanel="test" />`);

      expect(this.element.querySelector('.btn-delete')).not.to.exist;
      expect(this.element.querySelector('.btn-toggle-visibility')).to.exist;
    });
  });
});
