/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class AdminRegistrationController extends Controller {
  @service notifications;
  @service store;
  @tracked isSaving = false;

  get isDirty() {
    return this.model.registerEnabled.hasDirtyAttributes ||
      this.model.registerEmailDomains.hasDirtyAttributes ||
      this.model.registerMandatory.hasDirtyAttributes ||
      this.model.spacesDomains.hasDirtyAttributes ||
      this.model.registerURL.hasDirtyAttributes;
  }

  get isDisabled() {
    return !this.isDirty || this.isSaving;
  }

  @action
  save() {
    this.isSaving = true;
    this.isSavingError = false;

    Promise.all([this.model.registerEnabled.save(), this.model.registerEmailDomains.save(), this.model.registerMandatory.save(), this.model.spacesDomains.save(), this.model.registerURL.save()])
      .then(() => {
        this.isSaving = false;
        this.isSavingError = false;
      })
      .catch((err) => {
        this.isSaving = false;
        this.isSavingError = true;
        this.notifications.handleError(err);
      });
  }
}
