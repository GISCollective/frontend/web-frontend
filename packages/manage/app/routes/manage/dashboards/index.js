/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageDashboardsRoute extends AuthenticatedRoute {
  async model() {
    if (this.isFastBoot) {
      return {};
    }

    const teams = await this.store.query('team', {
      user: this.user.userData.id,
    });

    for (const team of teams) {
      await team.updateCurrentUserRole();
    }

    return hash({ teams });
  }
}
