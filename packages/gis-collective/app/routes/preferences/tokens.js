/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../base/authenticated-route';
import { hash } from 'rsvp';

export default class PreferencesTokensRoute extends AuthenticatedRoute {
  model() {
    return this.store.queryRecord('user', { me: true }).then((user) => {
      return user.updateTokens().then(() => {
        return hash({
          user: user,
          tokens: user.tokens,
        });
      });
    });
  }
}
