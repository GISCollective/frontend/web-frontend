/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | detailed location', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:location');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('deserialize a null to object', function () {
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({
      isDetailed: true,
      simple: '',
      detailedLocation: {
        country: '',
        province: '',
        city: '',
        postalCode: '',
      },
    });
  });

  it('deserialize a value to a object', function () {
    const deserialized = transform
      .deserialize({
        isDetailed: true,
        simple: 'simple',
        detailedLocation: {
          country: 'country',
          province: 'province',
          city: 'city',
          postalCode: 'postalCode',
        },
      })
      .toJSON();

    expect(deserialized).to.deep.equal({
      isDetailed: true,
      simple: 'country, province, city, postalCode',
      detailedLocation: {
        country: 'country',
        province: 'province',
        city: 'city',
        postalCode: 'postalCode',
      },
    });
  });

  it('deserialize a simple value to a object', function () {
    const deserialized = transform
      .deserialize({
        isDetailed: false,
        simple: 'simple',
      })
      .toJSON();

    expect(deserialized).to.deep.equal({
      isDetailed: false,
      simple: 'simple',
      detailedLocation: {
        country: '',
        province: '',
        city: '',
        postalCode: '',
      },
    });
  });

  it('deserialize a string value to a simple object', function () {
    const deserialized = transform.deserialize('simple').toJSON();

    expect(deserialized).to.deep.equal({
      isDetailed: false,
      simple: 'simple',
      detailedLocation: {
        country: '',
        province: '',
        city: '',
        postalCode: '',
      },
    });
  });

  it('transforms a detailed location to simple', function () {
    const deserialized = transform.deserialize({
      isDetailed: true,
      simple: 'simple',
      detailedLocation: {
        country: '1',
        province: '2',
        city: '3',
        postalCode: '4',
      },
    });

    expect(deserialized.simple).to.equal('1, 2, 3, 4');
  });
});
