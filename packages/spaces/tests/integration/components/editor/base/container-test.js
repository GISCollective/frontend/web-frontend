/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { setupRenderingTest, it, describe } from "dummy/tests/helpers";
import { render, fillIn, triggerEvent, click } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/base/container", function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.set("value", {
      verticalSize: "fill",
      verticalAlign: "top",
      color: "blue-500",
      classes: ["ps-3"],
    });
  });

  it("renders the container values", async function () {
    await render(hbs`<Editor::Base::Container @value={{this.value}} />`);

    expect(this.element.querySelector(".container-section .sm-start-padding").value).to.equal("3");

    expect(this.element.querySelector(".container-section .bs-color-picker .selected-value.bg-blue-500")).to.exist;
  });

  it("can change the border value", async function (a) {
    let changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Base::Container @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input-sm-border-size").value = "4";
    await triggerEvent(".input-sm-border-size", "change");

    expect(changedValue).to.deep.equal({
      verticalSize: "fill",
      verticalAlign: "top",
      color: "blue-500",
      classes: ["ps-3", "border-size-4"],
    });
  });

  it("can change the color value", async function () {
    let changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Base::Container @value={{this.value}} @onChange={{this.onChange}} />`);

    await click(".container-section .bs-color-picker");
    await click(".container-section .btn-color-warning");

    expect(changedValue).to.deep.equal({
      verticalSize: "fill",
      verticalAlign: "top",
      color: "warning",
      classes: ["ps-3"],
    });
  });

  it("can change the margin value", async function (a) {
    let changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Base::Container @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".sm-top-margin").value = "4";
    await triggerEvent(".sm-top-margin", "change");

    expect(changedValue).to.deep.equal({
      verticalSize: "fill",
      verticalAlign: "top",
      color: "blue-500",
      classes: ["ps-3", "mt-4"],
    });
  });

  it("can change the height value", async function (a) {
    let changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });

    await render(hbs`<Editor::Base::Container @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".sm-size-px .input-px-size", "22");
    await fillIn(".md-size-px .input-px-size", "33");
    await fillIn(".lg-size-px .input-px-size", "44");

    expect(changedValue).to.deep.equal({
      verticalSize: "fill",
      verticalAlign: "top",
      color: "blue-500",
      classes: ["ps-3"],
      style: { sm: { height: "22px" }, md: { height: "33px" }, lg: { height: "44px" } },
    });
  });

  it("allows changing the flex box properties", async function (a) {
    let changedValue = null;

    this.set("onChange", (v) => {
      this.set("value", v);
      changedValue = v;
    });

    await render(hbs`<Editor::Base::Container @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input-sm-justify-content").value = "center";
    await triggerEvent(".input-sm-justify-content", "change");

    this.element.querySelector(".input-md-justify-content").value = "start";
    await triggerEvent(".input-md-justify-content", "change");

    this.element.querySelector(".input-lg-justify-content").value = "end";
    await triggerEvent(".input-lg-justify-content", "change");

    this.element.querySelector(".input-sm-align-items").value = "end";
    await triggerEvent(".input-sm-align-items", "change");

    a.deepEqual(changedValue, {
      classes: [
        "ps-3",
        "justify-content-center",
        "justify-content-md-start",
        "justify-content-lg-end",
        "align-items-end",
      ],
      color: "blue-500",
      verticalAlign: "top",
      verticalSize: "fill",
    });
  });

  it("hides the size option when the element has data-size=false", async function (a) {
    let changedValue = null;

    this.set("onChange", (v) => {
      this.set("value", v);
      changedValue = v;
    });

    this.set("testElement", null);

    await render(
      hbs`<div class="test-element" data-size="false"></div><Editor::Base::Container @element={{this.testElement}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.set("testElement", this.element.querySelector(".test-element"));

    expect(this.element.querySelector(".container-size")).not.to.exist;
  });

  it("hides the flex option when the element has data-flex-config=false", async function (a) {
    let changedValue = null;

    this.set("onChange", (v) => {
      this.set("value", v);
      changedValue = v;
    });

    this.set("testElement", null);

    await render(
      hbs`<div class="test-element" data-flex-config="false"></div><Editor::Base::Container @element={{this.testElement}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.set("testElement", this.element.querySelector(".test-element"));

    expect(this.element.querySelector(".container-flex-config")).not.to.exist;
  });

  it("can change the flex resize value", async function (a) {
    let changedValue = null;

    this.set("onChange", (v) => {
      this.set("value", v);
      changedValue = v;
    });

    this.set("testElement", null);

    await render(
      hbs`<div class="test-element" data-flex-config="false"></div><Editor::Base::Container @element={{this.testElement}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    this.element.querySelector(".input-sm-flex-resize").value = "grow";
    await triggerEvent(".input-sm-flex-resize", "change");

    this.element.querySelector(".input-md-flex-resize").value = "shrink";
    await triggerEvent(".input-md-flex-resize", "change");

    a.deepEqual(changedValue, {
      classes: ["ps-3", "flex-resize-grow", "flex-resize-md-shrink"],
      color: "blue-500",
      verticalAlign: "top",
      verticalSize: "fill",
    });
  });
});
