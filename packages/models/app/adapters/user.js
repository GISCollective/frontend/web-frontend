/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';
import fetch from 'fetch';
import { isServerErrorResponse, isUnauthorizedResponse } from 'ember-fetch/errors';

export default class UserAdapter extends Adapter {
  changePassword(model, currentPassword, newPassword) {
    const url = this.buildURL('user', model.get('id')) + '/changePassword';

    return this.ajax(url, 'POST', {
      data: {
        currentPassword,
        newPassword,
      },
    });
  }

  revokeApiTokenByName(model, name) {
    const url = this.buildURL('user', model.get('id')) + '/revoke';

    return this.ajax(url, 'POST', { data: { name } });
  }

  removeUser(model, password) {
    const url = this.buildURL('user', model.get('id')) + '/remove';

    return this.ajax(url, 'POST', { data: { password } });
  }

  promote(model, password) {
    const url = this.buildURL('user', model.get('id')) + '/promote';

    return this.ajax(url, 'POST', { data: { password } });
  }

  downgrade(model, password) {
    const url = this.buildURL('user', model.get('id')) + '/downgrade';

    return this.ajax(url, 'POST', { data: { password } });
  }

  createToken(model, name, expire) {
    const url = this.buildURL('user', model.get('id')) + '/token';

    return this.ajax(url, 'POST', { data: { name, expire } });
  }

  getApiTokens(model) {
    const url = this.buildURL('user', model.get('id')) + '/listTokens';

    return this.ajax(url, 'GET');
  }

  forgotPassword(email, token, password) {
    const url = `${this.host}/users/forgotpassword`;

    return this.ajax(url, 'POST', { data: { email, token, password } });
  }

  registerChallenge() {
    const url = `${this.host}/users/registerchallenge`;
    return this.ajax(url, 'POST');
  }

  register(name, username, email, password, challenge, newsletter = false) {
    const url = `${this.host}/users/register`;
    const data = { ...name, username, email, password, newsletter };

    if (challenge) {
      data['challenge'] = challenge;
    }

    return this.ajax(url, 'POST', { data });
  }

  activate(email, token) {
    const url = `${this.host}/users/activate`;
    return this.ajax(url, 'POST', { data: { email, token } });
  }

  testNotification(model) {
    const url = this.buildURL('user', model.get('id')) + '/testNotification';
    return this.ajax(url, 'POST', {});
  }

  async queryRecord(store, schema, query, options) {
    if (query.me) {
      return super.queryRecord(store, schema, { id: 'me' }, options);
    }

    return super.queryRecord(...arguments);
  }
}
