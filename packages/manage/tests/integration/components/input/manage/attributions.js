/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/attributions-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Input::Manage::Attributions />`);
    const labels = this.element.querySelectorAll('.input-group-text');

    expect(labels[0].textContent.trim()).to.equal('Text');
    expect(labels[1].textContent.trim()).to.equal('URL');
  });
});
