/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { defaultBsProperty, setDefaultBsProperty } from "core/lib/bs-cols";
import { action } from "@ember/object";

export default class ManageEditorsPropertiesBorderComponent extends Component {
  elementId = `editor-property-${guidFor(this)}`;

  get color() {
    return defaultBsProperty("border-color", this.args.classes);
  }

  @action
  changeColor(value) {
    const classes = setDefaultBsProperty("border-color", value, this.args.classes);
    this.args.onChange(classes);
  }
}
