/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { service } from "@ember/service";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { defaultBsProperty, setDefaultBsProperty } from "core/lib/bs-cols";
import { toPagePath } from "core/lib/slug";

export default class ManageEditorsCardOptionsComponent extends Component {
  @service pageCols;
  @service store;

  pictureConfig = [
    {
      type: "border",
    },
    {
      name: "defaultCover",
      options: ["default", "from parent"],
    },
  ];

  elementId = `manage-editors-card-${guidFor(this)}`;

  @tracked _title;
  @tracked _description;

  @tracked _pictureProportion;
  @tracked _pictureClasses;
  @tracked _picture;

  @tracked _columns;
  @tracked _cardClasses;
  @tracked _containerClasses;

  @tracked _cardType;
  @tracked _destination;
  @tracked _cover;

  @tracked _extraFields;

  fields = {
    event: ["nextOccurrence", "entries", "schedule", "icons"],
    feature: ["icons"],
  };

  get showTitle() {
    return this.cardType.indexOf("title") != -1;
  }

  get showDescription() {
    return this.cardType.indexOf("description") != -1;
  }

  get showCover() {
    return this.cardType.indexOf("cover") != -1;
  }

  get columns() {
    if (this._columns) {
      return this._columns;
    }

    return this.args.value?.columns ?? {};
  }

  get cardType() {
    if (this._cardType) {
      return this._cardType;
    }

    return this.args?.value?.components?.cardType ?? this.pageCols.cardTypes[0];
  }

  get destination() {
    if (this._destination) {
      return this._destination;
    }

    return this.args.value?.destination?.slug;
  }

  get value() {
    return this.args.value ?? {};
  }

  get pageList() {
    if (!this.args.modelType) {
      return Object.keys(this.args.space.pagesMap).map(toPagePath);
    }

    const model = this.store.createRecord(this.args.modelType);
    const validVars = [...(model.relatedModels ?? []), this.args.modelType].map((a) => `:${a}-id`);

    const isValidSlug = (slug) => {
      return validVars.find((a) => slug.indexOf(a) >= 0);
    };

    const result = [];

    Object.keys(this.args.space.pagesMap)
      .filter(isValidSlug)
      .map(toPagePath)
      .forEach((a) => {
        result.push(a);
      });

    return result;
  }

  get title() {
    if (this._title) {
      return this._title;
    }

    return this.args.value?.title ?? {};
  }

  get description() {
    if (this._description) {
      return this._description;
    }

    return this.args.value?.description ?? {};
  }

  get pictureProportion() {
    if (this._pictureProportion) {
      return this._pictureProportion;
    }

    return this.args.value?.picture?.proportion ?? "1:1";
  }

  get pictureClasses() {
    if (this._pictureClasses) {
      return this._pictureClasses;
    }

    return this.args.value?.picture?.classes ?? [];
  }

  get pictureBorderColor() {
    return defaultBsProperty("border-color", this.pictureClasses);
  }

  get picture() {
    if (this._picture) {
      return this._picture;
    }

    return this.args.value?.picture;
  }

  get cardClasses() {
    return this.columns.classes ?? [];
  }

  get containerClasses() {
    if (this._containerClasses) {
      return this._containerClasses;
    }

    return this.columns.containerClasses ?? [];
  }

  get model() {
    return this.args.model ?? this.value.source?.model;
  }

  get extraFields() {
    if (this._extraFields) {
      return this._extraFields;
    }

    return this.value?.extraFields ?? {};
  }

  get extraFieldsConfig() {
    let result = [
      {
        name: "fullDescription",
        type: "bool",
      },
    ];

    if (this.fields[this.model]) {
      const fields = this.fields[this.model].map((name) => ({
        name,
        type: "bool",
      }));

      result = [...result, ...fields];
    }

    return result;
  }

  @action
  triggerChange() {
    const value = this.value ?? {};
    value.destination = { slug: this.destination };
    value.title = this.title;
    value.description = this.description;
    value.columns = this.columns;
    value.components = {
      cardType: this.cardType,
    };
    value.picture = {
      ...this.picture,
      proportion: this.pictureProportion,
    };

    if (this.extraFieldsConfig?.length) {
      value.extraFields = this.extraFields;
    } else {
      delete value.extraFields;
    }

    return this.args.onChange?.(value);
  }

  @action
  changeExtraFields(value) {
    this._extraFields = value;

    return this.triggerChange();
  }

  @action
  changePicture(value) {
    this._picture = value;

    return this.triggerChange();
  }

  @action
  onChangeColumns(value) {
    this._columns = value;

    return this.triggerChange();
  }

  @action
  changeCardType(value) {
    this._cardType = value;

    return this.triggerChange();
  }

  @action
  changeBorderColor(value) {
    this._pictureClasses = setDefaultBsProperty("border-color", value, this.pictureClasses);
    this.triggerChange();
  }

  @action
  onChangeCardClasses(value) {
    if (!this._columns) {
      this._columns = this.columns;
    }

    this._columns.classes = value;
    this.triggerChange();
  }

  @action
  onChangeContainerClasses(value) {
    this._containerClasses = value;

    if (!this._columns) {
      this._columns = this.columns;
    }

    this._columns.containerClasses = value;
    this.triggerChange();
  }

  @action
  changePictureProperty(key, value) {
    this[`_picture${key}`] = value;

    this.triggerChange();
  }

  @action
  changeTitle(value) {
    this._title = value;

    this.triggerChange();
  }

  @action
  changeDescription(value) {
    this._description = value;

    this.triggerChange();
  }

  @action
  changeDestination(destination) {
    this._destination = destination;

    return this.triggerChange();
  }
}
