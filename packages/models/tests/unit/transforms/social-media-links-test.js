import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | social media links', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:social-media-links');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized.toJSON()).to.deep.equal({
      facebook: '',
      youtube: '',
      xTwitter: '',
      instagram: '',
      whatsApp: '',
      tikTok: '',
      linkedin: '',
    });
  });

  it('can deserialize an object with all fields', function (a) {
    let deserialized = transform.deserialize({
      facebook: '1',
      youtube: '2',
      xTwitter: '3',
      instagram: '4',
      whatsApp: '5',
      tikTok: '6',
      linkedin: '7',
    });

    a.deepEqual(transform.serialize(deserialized), {
      facebook: '1',
      youtube: '2',
      xTwitter: '3',
      instagram: '4',
      whatsApp: '5',
      tikTok: '6',
      linkedin: '7',
    });
  });
});
