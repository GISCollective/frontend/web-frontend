/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { CampaignQuestions } from '../../../../transforms/campaign-questions';

export default class InputManageCampaignsQuestionsComponent extends Component {
  @tracked _name = null;
  @tracked _description = null;
  @tracked _location = null;
  @tracked _icons = null;
  @tracked _pictures = null;
  @tracked _optionalIcons;

  get nameConfig() {
    const result = [
      {
        name: 'customQuestion',
        type: 'bool',
      },
    ];

    const name = this.name;

    if (name.customQuestion) {
      result.push({
        name: 'label',
        type: 'text',
      });
    }

    return result;
  }

  get descriptionConfig() {
    const result = [];

    result.push({
      name: 'customQuestion',
      type: 'bool',
    });

    if (this.description.customQuestion) {
      result.push({
        name: 'label',
        type: 'text',
      });
    }

    return result;
  }

  get picturesConfig() {
    const result = [];

    result.push({
      name: 'customQuestion',
      type: 'bool',
    });

    if (this.pictures.customQuestion) {
      result.push({
        name: 'label',
        type: 'text',
      });
    }

    result.push({
      name: 'isMandatory',
      type: 'bool',
    });

    return result;
  }

  get iconsConfig() {
    const result = [];
    const hasIcons = this.optionalIcons?.data?.records?.length;

    if (hasIcons) {
      result.push({
        name: 'customQuestion',
        type: 'bool',
      });
    }

    if (hasIcons && this.icons.customQuestion) {
      result.push({
        name: 'label',
        type: 'text',
      });
    }

    return result;
  }

  get locationConfig() {
    const result = [];

    result.push({
      name: 'customQuestion',
      type: 'bool',
    });

    if (this.location?.customQuestion) {
      result.push({
        name: 'label',
        type: 'text',
      });
    }

    result.push({
      name: 'allowGps',
      type: 'bool',
    });

    result.push({
      name: 'allowManual',
      type: 'bool',
    });

    result.push({
      name: 'allowAddress',
      type: 'bool',
    });

    result.push({
      name: 'allowExistingFeature',
      type: 'bool',
    });

    return result;
  }

  get name() {
    if (this._name) {
      return this._name;
    }

    return this.args.value?.name ?? {};
  }

  get description() {
    if (this._description) {
      return this._description;
    }

    return this.args.value?.description ?? {};
  }

  get pictures() {
    if (this._pictures) {
      return this._pictures;
    }

    return this.args.value?.pictures ?? {};
  }

  get location() {
    if (this._location) {
      return this._location;
    }

    return (
      this.args.value?.location ?? {
        customQuestion: false,
        allowGps: true,
        allowManual: true,
        allowAddress: true,
        allowExistingFeature: false,
      }
    );
  }

  get icons() {
    if (this._icons) {
      return this._icons;
    }

    return {
      customQuestion: this.args.value?.icons?.customQuestion ?? false,
      label: this.args.value?.icons?.label || 'Please select an icon from the list',
    };
  }

  get iconsLabel() {
    if (!this.args?.optionalIcons?.length) {
      return '';
    }

    if (!this.args.value?.icons?.customQuestion || !this.args.value?.icons?.label) {
      return 'Please select an icon from the list';
    }

    return this.args.value?.icons?.label;
  }

  get showDescriptionQuestion() {
    if (typeof this._showDescriptionQuestion == 'boolean') {
      return this._showDescriptionQuestion;
    }

    return this._showDescriptionQuestion !== null ? this._showDescriptionQuestion : this.args.value?.showDescriptionQuestion || false;
  }

  get descriptionLabel() {
    if (this._descriptionLabel === '') {
      return '';
    }
    return this._descriptionLabel || this.args.value?.descriptionLabel || '';
  }

  get optionalIcons() {
    let records = this._optionalIcons || this.args.optionalIcons?.slice?.() || [];

    return {
      data: {
        ids: records?.map((a) => a.id ?? a._id),
        records,
        flex: 'disabled',
      },
    };
  }

  @action
  changeIconsOptions(value) {
    this._icons = value;
  }

  @action
  iconsChanged(value) {
    this._optionalIcons = value;
  }

  @action
  changeNameOptions(value) {
    this._name = value;
  }

  @action
  changeDescriptionOptions(value) {
    this._description = value;
  }

  @action
  changePicturesOptions(value) {
    this._pictures = value;
  }

  @action
  changeLocationOptions(value) {
    this._location = value;
  }

  @action
  save() {
    return this.args.onSave(
      new CampaignQuestions({
        name: this.name,
        description: this.description,
        icons: this.icons,
        location: this.location,
        pictures: this.pictures
      }),
      this.optionalIcons.data?.records ?? []
    );
  }

  @action
  cancel() {
    this._name = null;
    this._description = null;
    this._location = null;
    this._icons = null;

    return this.args.onCancel();
  }
}
