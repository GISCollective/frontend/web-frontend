/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr } from '@ember-data/model';
import PictureMeta from 'models/lib/picture-meta';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { generateBlur } from '../lib/blur';

export default class PictureModel extends Model {
  @attr('string') name;
  @attr('string') picture;
  @attr('boolean') canEdit;
  @attr('string') owner;
  @attr('string') hash;
  @attr('picture-meta', {
    defaultValue() {
      return new PictureMeta();
    },
  })
  meta;

  @tracked progress;
  @tracked rnd = '';

  get hashBg() {
    return generateBlur(this.hash, 7, 7);
  }

  get canRotate() {
    return this.owner != '@system';
  }

  get md() {
    if (this.isNew) {
      return this.picture;
    }

    return `${this.picture}/md?rnd=${this.rnd}`;
  }

  get styleLg() {
    return htmlSafe(`background-image: url('${this.picture}/lg?rnd=${this.rnd}')`);
  }

  get attributions() {
    let result = `${this.name.trim()}`;

    if (this.name && this.meta.attributions) {
      result += ' - ';
    }

    if (this.meta.attributions) {
      result += `${this.meta.attributions.trim()}`;
    }

    if (result == '' || result == ' - ') {
      return false;
    }

    return result;
  }

  get is360() {
    return this.meta.renderMode == '360';
  }

  set is360(value) {
    this.meta.renderMode = value ? '360' : '';
  }

  async rotate() {
    if (this.isNew) {
      return this.localRotate();
    }

    const adapter = this.store.adapterFor(this.constructor.modelName);

    await adapter.rotate(this);

    this.rnd = Math.random();
  }

  async trySave(remaining = 2) {
    if(remaining <= 0) {
      return this.save();
    }

    try {
      return await this.save();
    } catch(err) {
      return this.trySave(remaining - 1);
    }
  }

  async setLink(model, id) {
    if (this.meta?.link?.model != model || this.meta?.link?.id != id) {
      this.meta.link = {
        model,
        id,
      };

      return this.save();
    }
  }

  async localResize(maxWidth, maxHeight, mime) {
    if (!this.isNew) {
      return;
    }

    if (this.picture.length < 1024 * 10) {
      return;
    }

    let img = document.createElement('img');
    img.crossOrigin = 'Anonymous';
    const p = new Promise((resolve, reject) => {
      img.addEventListener('error', (event, a, b, c) => {
        reject(new Error('The selected image could not be resized. Please try a different format.'));
      });

      img.addEventListener('load', (event) => {
        try {
          const w = img.naturalWidth;
          const h = img.naturalHeight;

          if (maxWidth >= img.width && maxHeight >= img.height) {
            return resolve();
          }

          if (w == 0 || h == 0) {
            return reject();
          }

          var ratio = 1;
          if (img.width > maxWidth) {
            ratio = maxWidth / img.width;
          } else if (img.height > maxHeight) {
            ratio = maxHeight / img.height;
          }

          if (ratio == 1) {
            return resolve();
          }

          var canvas = document.createElement('canvas');
          canvas.width = w * ratio;
          canvas.height = h * ratio;
          var ctx = canvas.getContext('2d');
          ctx.drawImage(img, 0, 0, w, h, 0, 0, w * ratio, h * ratio);

          var data;
          try {
            data = canvas.toDataURL(mime ?? 'image/jpeg', 0.95);
          } catch (err) {
            return reject();
          }

          this.picture = data;
          resolve();
        } catch (err) {
          reject(err);
        }
      });
    });

    img.src = this.picture;

    return p;
  }

  localRotate(mime) {
    var angle = -90;

    var img = document.createElement('img');
    img.src = this.picture;

    return new Promise((resolve, reject) => {
      img.onload = () => {
        try {
          var h = img.naturalHeight;
          var w = img.naturalWidth;

          if (w == 0 || h == 0) {
            return reject();
          }

          var max = Math.max(w, h);

          //rotate image
          var canvas = document.createElement('canvas');
          canvas.width = max;
          canvas.height = max;
          var ctx = canvas.getContext('2d');

          ctx.translate(0, w);
          ctx.rotate(angle * (Math.PI / 180));
          ctx.drawImage(img, 0, 0);

          //crop image
          var canvas2 = document.createElement('canvas');
          canvas2.width = h;
          canvas2.height = w;
          var ctx2 = canvas2.getContext('2d');
          ctx2.drawImage(canvas, 0, 0);

          const data = canvas2.toDataURL(mime ?? 'image/jpeg', 0.95);

          this.picture = data.slice();
          resolve();
        } catch (err) {
          reject(err);
        }
      };
    });
  }
}
