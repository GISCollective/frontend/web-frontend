/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '../pages/base';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { PageCol } from 'models/transforms/page-col-list';
import { service } from '@ember/service';
import { getDefaultPresentationContainer } from '../../../lib/presentation';
import { TrackedMap } from 'tracked-maps-and-sets';
import { Selection } from '../pages/content';

export default class ManagePresentationsContentController extends Controller {
  @service intl;
  @service space;
  @tracked selection;
  @tracked colsMap = undefined;

  createColsMap() {
    this.colsMap = new TrackedMap();

    for (const col of this.model.presentation.cols) {
      const key = col.name;
      this.colsMap.set(key, col);
    }
  }

  @action
  select(selection) {
    if (!this.colsMap) {
      this.createColsMap();
    }

    if (this.selection && this.selection.title == selection?.title) {
      return;
    }

    if (selection?.type == 'col') {
      const key = selection.data?.name;

      if (!this.colsMap.has(key)) {
        this.colsMap.set(key, new PageCol(selection));
      }

      selection.data = this.colsMap.get(key);
    }

    this.selection = new Selection(selection);

    if (selection?.type == 'col') {
      this.selection.data.store = this.store;
    }
  }

  @action
  async changeCol(newData, newType) {
    this.selection.data.data = newData || {};
    this.selection.data.type = newType;

    const key = this.selection.data.modelKey;
    const hasModelKey = !!key;
    const result = await this.selection.data.fetch?.();

    if (hasModelKey && result) {
      this.model[key] = result;
    }
  }

  @action
  async saveContent() {
    this.model.presentation.cols = this.cols;
    return this.model.presentation.save();
  }

  @action
  addSlide(rowIndex) {
    if (!this.colsMap) {
      this.createColsMap();
    }

    const newColsMap = new TrackedMap();

    this.colsMap.forEach((value, key) => {
      const pieces = key.split('.').map((a) => parseInt(a));

      const newKey = pieces[1] < rowIndex ? `${pieces[0]}.${pieces[1]}.${pieces[2]}` : `${pieces[0]}.${pieces[1] + 1}.${pieces[2]}`;

      if (pieces[1] >= rowIndex) {
        value.row += 1;
      }

      value.name = newKey;
      newColsMap.set(newKey, value);
    });

    newColsMap.set(
      `0.${rowIndex}.0`,
      new PageCol({
        name: `0.${rowIndex}.0`,
        container: 0,
        row: rowIndex,
        col: 0,
      })
    );
    newColsMap.set(
      `0.${rowIndex}.1`,
      new PageCol({
        name: `0.${rowIndex}.1`,
        container: 0,
        row: rowIndex,
        col: 1,
      })
    );

    this.colsMap = newColsMap;
  }

  @action
  deleteSlide(rowIndex) {
    if (!this.colsMap) {
      this.createColsMap();
    }

    this.colsMap.delete(`0.${rowIndex}.0`);
    this.colsMap.delete(`0.${rowIndex}.1`);

    const newColsMap = new TrackedMap();

    this.colsMap.forEach((value, key) => {
      const pieces = key.split('.').map((a) => parseInt(a));

      const newKey = pieces[1] < rowIndex ? `${pieces[0]}.${pieces[1]}.${pieces[2]}` : `${pieces[0]}.${pieces[1] - 1}.${pieces[2]}`;

      if (pieces[1] >= rowIndex) {
        value.row -= 1;
      }

      newColsMap.set(newKey, value);
    });

    this.colsMap = newColsMap;
  }

  @action
  selectPresentation() {
    this.selection = new Selection('presentation', 'presentation');
  }

  get endSlideOptions() {
    return {
      data: this.model.presentation.endSlideOptions,
    };
  }

  get cols() {
    if (!this.colsMap) {
      return this.model.presentation.cols;
    }

    return [...this.colsMap.entries()].map((a) => a[1]);
  }

  get rowCount() {
    return this.cols.map((a) => a.row).reduce((a, b) => Math.max(a, b), 0) + 1;
  }

  get containers() {
    const result = [];

    for (let i = 0; i < this.rowCount; i++) {
      result.push(getDefaultPresentationContainer(true, 0, i));
    }

    return result;
  }
}
