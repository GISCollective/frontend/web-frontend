/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/data-binding/connection-info',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders a message when the type is not set', async function () {
    await render(hbs`<Input::DataBinding::ConnectionInfo />`);

    expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
      'Select a type before setting the connection info.'
    );
  });

  it('renders a message when the type is not supported', async function () {
    await render(hbs`<Input::DataBinding::ConnectionInfo @type="other" />`);

    expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
      'The selected connection type is not supported yet.'
    );
  });

  it('renders the airtable fields and values', async function () {
    this.set('value', {
      _apiKey: '1',
      baseId: '2',
      tableName: '3',
      viewName: '4',
    });

    await render(
      hbs`<Input::DataBinding::ConnectionInfo @type="airtable" @value={{this.value}}/>`
    );

    expect(
      this.element.querySelector('.connection-info-_apiKey').value
    ).to.equal('1');

    expect(
      this.element.querySelector('.connection-info-baseId').value
    ).to.equal('2');

    expect(
      this.element.querySelector('.connection-info-tableName').value
    ).to.equal('3');

    expect(
      this.element.querySelector('.connection-info-viewName').value
    ).to.equal('4');
  });

  it('triggers on change when the airtable fields are changed', async function () {
    this.set('value', {
      _apiKey: '1',
      baseId: '2',
      tableName: '3',
      viewName: '4',
    });

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::DataBinding::ConnectionInfo @type="airtable" @value={{this.value}} @onChange={{this.change}}/>`
    );

    await fillIn('.connection-info-_apiKey', 'a');
    await fillIn('.connection-info-baseId', 'b');
    await fillIn('.connection-info-tableName', 'c');
    await fillIn('.connection-info-viewName', 'd');

    expect(value).to.deep.equal({
      _apiKey: 'a',
      baseId: 'b',
      tableName: 'c',
      viewName: 'd',
    });
  });
});
