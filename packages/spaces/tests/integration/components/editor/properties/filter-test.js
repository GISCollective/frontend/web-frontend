/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/properties/filter", function (hooks) {
  setupRenderingTest(hooks);

  it("renders an empty selector when no value is set", async function () {
    await render(hbs`<Editor::Properties::Filter />`);
    expect(this.element.querySelector(".input-filter").value).to.equal("");
  });

  it("renders the value when set", async function () {
    this.set("value", "icons");

    await render(hbs`<Editor::Properties::Filter @value={{this.value}} />`);
    expect(this.element.querySelector(".input-filter").value).to.equal("icons");
  });

  it("can change the value", async function () {
    this.set("value", "icons");

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Properties::Filter @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".input-filter").value = "location";
    await triggerEvent(".input-filter", "change");

    expect(value).to.equal("location");
  });
});
