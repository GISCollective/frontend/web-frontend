/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { attributeToSheetColData } from 'manage/lib/SheetColData';

export default class InputSiteAttributesPropertyComponent extends Component {
  get attribute() {
    if (!this.args.icon) {
      return null;
    }

    return this.args.icon.attributes.find((a) => a.name == this.args.property.name);
  }

  get attributeValue() {
    const result = {};

    result[this.args.icon.name] = {};
    result[this.args.icon.name][this.args.property.name] = this.args.property.value;

    return result;
  }

  get colData() {
    return attributeToSheetColData(this.args.icon, this.attribute);
  }

  get displayName() {
    if (this.args.property?.displayName?.trim()) {
      return this.args.property.displayName;
    }

    return this.args.property?.name;
  }

  @action
  deleteProperty() {
    this.args.property.isDeleted = true;
  }

  @action
  dataChanged(value) {
    if (this.args.property.isBlocks && Array.isArray(value)) {
      this.args.property.value.blocks = value;
      return;
    }

    this.args.property.value = value;
  }
}
