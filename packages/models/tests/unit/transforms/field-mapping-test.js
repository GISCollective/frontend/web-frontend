/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | field mapping', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:field-mapping');
  });

  // Replace this with your real tests.
  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an list with two objects', function () {
    let deserialized = transform.deserialize([
      {
        key: 'a',
        destination: 'b',
        preview: ['1'],
      },
      {
        key: 'c',
        destination: 'd',
        preview: ['2'],
      },
    ]);

    expect(deserialized.map((a) => a.toJSON())).to.deep.equal([
      {
        key: 'a',
        destination: 'b',
        preview: ['1'],
      },
      {
        key: 'c',
        destination: 'd',
        preview: ['2'],
      },
    ]);
  });

  it('can serialize an list with two objects', function () {
    const decorators = [
      {
        key: 'a',
        destination: 'b',
        preview: ['1'],
      },
      {
        key: 'c',
        destination: 'd',
        preview: ['2'],
      },
    ];

    let deserialized = transform.deserialize(decorators);
    expect(transform.serialize(deserialized)).to.deep.equal(decorators);
  });
});
