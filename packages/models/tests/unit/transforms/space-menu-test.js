/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | space menu', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:space-menu');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized.items).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized.items).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize({
      items: [
        {
          link: {
            pageId: '61292c4c7bdf9301008fd7be',
          },
          name: 'About',
          dropDown: [
            {
              name: 'name',
              link: {
                pageId: '61292c4c7bdf9301008fd7be',
              },
            },
          ],
        },
        {
          link: {},
          name: 'Browse',
        },
        {
          link: {},
          name: 'Propose a site',
        },
      ],
    });

    expect(deserialized.items.length).to.deep.equal(3);
  });

  it('can serialize an object', function (a) {
    const menu = {
      items: [
        {
          link: {
            pageId: '61292c4c7bdf9301008fd7be',
          },
          name: 'About',
          dropDown: [
            {
              name: 'name',
              link: {
                pageId: '61292c4c7bdf9301008fd7be',
              },
              newTab: undefined,
            },
          ],
        },
        {
          link: {},
          name: 'Browse',
          dropDown: [],
        },
        {
          link: {},
          name: 'Propose a site',
          dropDown: [],
        },
      ],
    };

    let deserialized = transform.deserialize(menu);

    a.deepEqual(transform.serialize(deserialized), menu);
  });
});
