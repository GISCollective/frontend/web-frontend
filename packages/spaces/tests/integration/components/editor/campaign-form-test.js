/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | editor/campaign-form", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let changedValue;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultCampaign("1");
    server.testData.storage.addDefaultCampaign("2");
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders the value record when is set", async function () {
    this.set("campaign", {
      data: { source: { model: "campaign", id: "1" } },
    });

    await render(hbs`<Editor::CampaignForm @value={{this.campaign}} />`);

    const selector = this.element.querySelector(".model-name");
    expect(selector).not.to.exist;

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("Campaign 1");
  });

  it("can select a custom model and campaign", async function () {
    this.set("campaign", {});

    await render(hbs`<Editor::CampaignForm @value={{this.campaign}} @onChange={{this.onChange}} />`);

    await selectSearch(".model-id", "Campaign");
    await selectChoose(".model-id", "Campaign 1");

    expect(changedValue).to.deep.equal({
      source: {
        useDefaultModel: false,
        useSelectedModel: false,
        model: "campaign",
        id: "1",
      },
    });
  });
});
