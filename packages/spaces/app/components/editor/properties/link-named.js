/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { later } from "@ember/runloop";

export default class ManageEditorsPropertiesLinkNamedComponent extends Component {
  elementId = `link-named-${guidFor(this)}`;
  @tracked _name;
  @tracked _openInNewTab;

  config = [
    {
      name: "name",
      type: "text",
      label: "link name",
    },
    {
      name: "newTab",
      type: "bool",
      label: "open in new tab",
    },
  ];

  get name() {
    if (typeof this._name == "string") {
      return this._name;
    }

    return this.args.value?.name;
  }

  set name(value) {
    this._name = value;
    this.triggerChange();
  }

  get link() {
    if (this._link) {
      return this._link;
    }

    return this.args.value?.link ?? {};
  }

  get openInNewTab() {
    if (this._openInNewTab !== undefined) {
      return this._openInNewTab;
    }

    return this.args.value?.newTab ?? false;
  }

  triggerChange() {
    return this.args.onChange?.({
      name: this.name,
      link: this.link,
      newTab: this.openInNewTab,
    });
  }

  @action
  onChangeLink(value) {
    this._link = value;

    later(() => {
      this.triggerChange();
    });
  }

  @action
  changeOptions(value) {
    this._openInNewTab = value.newTab;
    this._name = value.name;

    return this.triggerChange();
  }
}
