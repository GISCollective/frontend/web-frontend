import { module, test } from 'qunit';
import { setupTest } from 'embedded/tests/helpers';

module('Unit | Service | fastboot', function (hooks) {
  setupTest(hooks);

  // TODO: Replace this with your real tests.
  test('it exists', function (assert) {
    let service = this.owner.lookup('service:fastboot');
    assert.ok(service);
  });
});
