/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class EmbedService extends Service {
  @tracked isEnabled = false;
  @tracked mapInfo = true;
  @tracked showMore = true;
  @tracked allIcons = false;
}
