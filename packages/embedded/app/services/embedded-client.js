import { getOwner } from '@ember/application';
import Service, { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class EmbeddedClientService extends Service {
  @service router;
  @service tiles;

  @tracked isReady;
  @tracked resolveSetup;
  @tracked setupPromise;
  @tracked position;
  @tracked value;
  messageIndex = -1;

  constructor() {
    super(...arguments);

    if (!window) {
      return;
    }

    this.originalFetch = window?.fetch;

    window.setConfig = this.setConfig;
    window.setValue = this.setValue;
    window.resolveRequest = this.resolveRequest;
    window.rejectRequest = this.rejectRequest;
    window.fetch = this.fetch;
  }

  b64toBlob(base64Data, contentType = 'application/octet-stream') {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
        var begin = sliceIndex * sliceSize;
        var end = Math.min(begin + sliceSize, bytesLength);

        var bytes = new Array(end - begin);
        for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
        }
        byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  get config() {
    return getOwner(this).resolveRegistration('config:environment');
  }

  @action
  setValue(value) {
    let newValue = value;

    if (value && typeof value == 'string') {
      try {
        newValue = JSON.parse(value);
      } catch(err) {
        console.error(err);
        newValue = undefined;
      }
    }

    if (JSON.stringify(this.value) == JSON.stringify(newValue)) {
      return;
    }

    this.value = newValue;
  }

  @action
  resolveRequest(messageIndex, value) {
    if (!window.responses?.[messageIndex]) {
      return;
    }

    window.responses[messageIndex]?.resolve(value);
    delete window.responses[messageIndex];
  }

  @action
  rejectRequest(messageIndex, value) {
    if (!window.responses?.[messageIndex]) {
      return;
    }

    window.responses[messageIndex]?.reject(new Error("Error on HOST fetch."));
    delete window.responses[messageIndex];
  }

  @action
  fetch(options) {
    if(typeof options == "string") {
      options= {
        url: options,
        method: "GET",
        headers: [],
      }
    }

    const url = options.url;
    const method = options.method;
    const headers = Object.fromEntries(options.headers);
    const body = options.body;

    console.log("Host Request", method, url);
    return this.sendMessage('fetch', { url, method, headers, body });
  }

  @action
  sendMessage(name, ...args) {
    this.messageIndex++;

    if (!window.responses) {
      window.responses = {};
    }

    const action = () =>{
      const message = JSON.stringify({
        name,
        args,
        messageIndex: this.messageIndex,
      });

      if(typeof window.ReactNativeWebView?.postMessage != "function") {
        console.error("RN is not working");
        return;
      }

      return window.ReactNativeWebView.postMessage(message);
    }

    let promise;
    switch (name) {
      case 'storeFetch':
        promise = new Promise((resolve, reject) => {
          const proxyResolve = (serializedData) => {
            resolve(JSON.parse(serializedData));
          };

          window.responses[this.messageIndex] = {
            resolve: proxyResolve,
            reject,
          };
          action();
        });
        break;

      case 'fetch':
        promise = new Promise(async (resolve, reject) => {
          const proxyResolve = async (serializedData) => {
            try {
              const data = JSON.parse(serializedData);

              const blob = await this.b64toBlob(data.body);
              const response = new Response(blob, data.options);

              resolve(response);
            } catch(err) {
              console.error(err);
              reject(err)
            }
          };

          window.responses[this.messageIndex] = {
            resolve: proxyResolve,
            reject,
          };
          action();
        });
        break;

      default:
        action();
    }

    return promise;
  }

  checkConfig() {
    if (this.isReady || !this.resolveSetup) {
      return;
    }
    console.log('Checking the config...');

    if (!this.config.apiUrl) {
      this.isReady = false;
      console.log('apiUrl not ready');
      return;
    }

    if (!this.config.route) {
      console.log('route not ready');

      this.isReady = false;
      return;
    }

    this.isReady = true;

    console.log('READY.');
    clearInterval(this.setupTimer);
    this.resolveSetup?.();
    this.resolveSetup = null;
  }

  @action
  setConfig(key, value) {
    this.config[key] = value;

    if (key == 'bearer') {
      this.tiles.bearer = `Bearer ${value}`;
    }

    if (key == 'apiUrl') {
      this.tiles.url = `${value}/tiles`;
    }

    if (this.isReady && key == 'route') {
      this.router.transitionTo(value);
    }

    if (this.isReady) {
      return;
    }

    this.checkConfig();
  }

  async setup() {
    if (this.setupPromise) {
      return this.setupPromise;
    }

    this.setupPromise = new Promise((resolve, reject) => {
      window.isReady = true;
      this.resolveSetup = resolve;
      this.rejectSetup = reject;
    });

    this.setupCounter = 0;
    this.setupTimer = setInterval(() => {
      if (this.setupCounter > 5) {
        clearInterval(this.setupTimer);
        this.rejectSetup?.(new Error('Timeout while waiting for the setup.'));
      }
    }, 1000);

    this.checkConfig();

    return this.setupPromise;
  }
}
