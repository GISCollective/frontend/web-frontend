/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
} from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe ('Acceptance | manage/page/add',  function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedPage;
  let space;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();

    receivedPage = null;

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [],
        }),
      ];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('redirects to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/pages/add`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fpages%2Fadd');
  });

  describe ('when an user is authenticated and no space is available',  function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/pages/add`);

      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'New page'
      );

      expect(
        this.element.querySelector('.row-name label').textContent.trim()
      ).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-path label').textContent.trim()
      ).to.contain('Path');
      expect(this.element.querySelector('.row-path input.form-control')).to
        .exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;

      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.btn-add-space')).to.exist;
    });

    it('the button in the alert should redirect the user to the add a space form', async function () {
      authenticateSession();
      await visit(`/manage/pages/add`);
      await click('.alert-danger .btn-add-space');

      expect(currentURL()).to.equal('/manage/spaces/add?next=add.page');
    });

    it('the button in the Space section should also redirect to the add a space form', async function () {
      authenticateSession();
      await visit(`/manage/pages/add`);
      await click('.btn-add-space');

      expect(currentURL()).to.equal('/manage/spaces/add?next=add.page');
    });
  });

  describe ('when an user is authenticated and a space is available',  function (hooks) {
    hooks.beforeEach(function () {
      space = server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');
      server.testData.storage.addDefaultTeam('000000000000000000000001');
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/pages/add`);

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();
      await visit(`/manage/pages/add`);

      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Dashboard',
        'New page',
      ]);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
      ]);
    });

    describe ('when the server request is successful',  function (hooks) {
      hooks.beforeEach(function () {
        space = server.testData.storage.addDefaultSpace();
        server.testData.storage.addDefaultSpace('000000000000000000000001');

        server.post('/mock-server/pages', (request) => {
          receivedPage = JSON.parse(request.requestBody);
          receivedPage.page._id = 'new-id';
          server.testData.storage.addPage(receivedPage.page);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedPage),
          ];
        });
      });

      it('redirects to the edit page after the request response is received', async function () {
        authenticateSession();

        await visit(`/manage/pages/add`);

        await fillIn('.input-name', 'new page name');
        await fillIn('.input-path', '/new-slug');

        this.element.querySelector('.row-space select').value = space._id;
        await triggerEvent('.row-space select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedPage.page.name).to.equal('new page name');
        expect(receivedPage.page.slug).to.equal('new-slug');
        expect(receivedPage.page.space).to.equal(space._id);
        expect(receivedPage.page.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '61292c4c7bdf9301008fd7b6',
        });

        expect(currentURL()).to.equal(
          `/manage/pages/${receivedPage.page._id}/content`
        );
      });
    });

    describe ('when the server request fails',  function (hooks) {
      hooks.beforeEach(function () {
        server.testData.storage.addDefaultSpace('000000000000000000000001');
        server.post('/mock-server/layouts', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description: 'The `page.slug` field is required.',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });

        server.post('/mock-server/pages', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description: 'The `page.slug` field is required.',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function (a) {
        const service = this.owner.lookup('service:notifications');

        authenticateSession();

        await visit(`/manage/pages/add`);

        await fillIn('.input-name', 'new page name');
        await fillIn('.input-path', '/new-slug');

        this.element.querySelector('.row-space select').value = space._id;
        await triggerEvent('.row-space select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/manage/pages/add`);

        a.equal(service.lastCall[0], 'handleError');
        a.equal(
          service.lastCall[1].message,
          'Ember Data Request POST /mock-server/pages returned a 400\nPayload (application/json)\n[object Object]',
        );
      });
    });
  });

  describe ('when an admin user is authenticated',  function (hooks) {
    hooks.beforeEach(() => {
      space = server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultUser(true);
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/manage/pages/add`);
      expect(currentURL()).to.equal('/manage/pages/add');
    });

    describe ('when the server request is successfully',  function (hooks) {
      let page;

      hooks.beforeEach(function () {
        page = server.testData.storage.addDefaultPage();
        server.post('/mock-server/pages', (request) => {
          receivedPage = JSON.parse(request.requestBody);
          receivedPage.page._id = page._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedPage),
          ];
        });
        server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');
        server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');
      });

      it('queries the spaces without the all query param', async function () {
        authenticateSession();

        await visit(`/manage/pages/add`);

        await fillIn('.input-name', 'new page name');
        await fillIn('.input-path', '/new-slug');

        this.element.querySelector('.row-space select').value = space._id;
        await triggerEvent('.row-space select', 'change');

        await PageElements.wait(1000);
        expect(server.history).to.contain('GET /mock-server/spaces?edit=true');

        await click(this.element.querySelector('.btn.btn.btn-submit'));

        expect(receivedPage.page.name).to.deep.equal('new page name');
        expect(currentURL()).to.equal(`/manage/pages/${page._id}/content`);
      });

      it('queries the spaces with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/manage/pages/add`);
        expect(server.history).to.contain('GET /mock-server/spaces?edit=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain('GET /mock-server/spaces?all=true');
      });
    });
  });
});
