/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { belongsTo, attr } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';
import { DetailedAttributeCategory } from '../lib/attribute-category';

export default class IconModel extends Model {
  @attr('string') name;
  @attr('string') localName;
  @attr description;
  @attr('optional-icon-image') image;
  @attr('string') parent;
  @attr('inherited-style') styles;

  @belongsTo('iconSet', { async: true, inverse: null })
  iconSet;

  @attr('string') category;
  @attr('string') subcategory;

  @attr('boolean', { defaultValue: false }) allowMany;
  @attr('icon-attribute-definition-list') attributes;

  @attr('number', { defaultValue: 0 }) minZoom;
  @attr('number', { defaultValue: 20 }) maxZoom;
  @attr('boolean', { defaultValue: true }) keepWhenSmall;

  @attr('boolean') canEdit;
  @attr otherNames;

  @attr('number', { defaultValue: 999 }) order;

  get cover() {
    return {
      picture: this.image.value,
      name: this.localName,
    };
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.localName;
    metaInfo.description = this.firstParagraph;
    metaInfo.imgSrc = this.image.value;
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    return metaInfo;
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  get title() {
    return this.localName;
  }

  get imageStyles() {
    return this.styles?.types.imageStyles(this.image.value);
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.localName);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  get asCategory() {
    const category = new DetailedAttributeCategory();

    const otherNames = Array.isArray(this.otherNames) ? this.otherNames : [];

    category.icon = this;
    category.names = [this.name, ...otherNames];
    category.displayName = this.displayName || this.name;

    return category;
  }

  get isPublished() {
    return this.iconSet.get('visibility.isPublic');
  }

  get isPublic() {
    return this.iconSet.get('visibility.isPublic');
  }

  deleteMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.deleteMany(...arguments);
  }

  async loadRelations() {
    const promises = [];

    promises.push(this.image);
    promises.push(this.iconSet);

    await Promise.all(promises);
  }
}
