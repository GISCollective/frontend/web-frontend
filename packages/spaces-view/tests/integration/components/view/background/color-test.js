/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/background/color', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a div with the bg color class', async function () {
    this.set('value', {
      type: 'background/color',
      data: {
        options: [],
        background: {
          color: 'blue',
        },
      },
    });

    await render(hbs`<View::Background::Color @value={{this.value}}/>`);

    expect(this.element.querySelector('.container-bg-color')).to.have.class(
      'bg-blue',
    );
  });

  it('renders a div with the provided custom class', async function () {
    await render(hbs`<View::Background::Color @class="custom-class"/>`);

    expect(
      this.element.querySelector('.container-bg-color > div'),
    ).to.have.class('custom-class');
  });
});
