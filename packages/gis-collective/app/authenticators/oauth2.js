/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import OAuth2PasswordGrant from 'ember-simple-auth/authenticators/oauth2-password-grant';
import config from '../config/environment';

export default class CustomAuthenticator extends OAuth2PasswordGrant {
  serverTokenEndpoint = `${config.apiUrl}/auth/token`;
  serverTokenRevocationEndpoint = `${config.apiUrl}/auth/revoke`;
}
