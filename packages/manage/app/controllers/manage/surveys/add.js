/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddCampaignController extends Controller {
  @service notifications;
  @service user;
  @service router;
  @service intl;

  queryParams = ['all'];

  @tracked all = false;
  @tracked isInvalid = false;
  @tracked article;
  @tracked hasTeam;
  @tracked team;

  get canSubmit() {
    return this.model.campaign.name && this.hasTeam;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.campaign.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new survey'),
      capitalize: true,
    });

    return pieces;
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  changeArticleValue(title, article) {
    this.article = article;
  }

  @action
  changeTeam(team) {
    this.model.campaign.set('visibility.team', team);
    this.hasTeam = !!this.model.campaign.visibility.team;
    this.team = team;
  }

  @action
  async save() {
    const articleBlocks = this.article?.blocks ?? [];
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.campaign.name,
          level: 1,
        },
      },
      ...articleBlocks,
    ];

    this.model.campaign.article = {
      blocks,
    };

    const result = await this.model.campaign.save();

    this.router.transitionTo('manage.surveys.edit', result.id);
  }
}
