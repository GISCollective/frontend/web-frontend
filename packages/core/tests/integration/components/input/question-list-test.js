/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | input/question-list', function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there are no questions', async function (assert) {
    await render(hbs`<Input::QuestionList />`);

    assert.dom(this.element).hasText('');
  });

  describe('the email questions', function () {
    it('renders an email question', async function (assert) {
      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'other',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onChange={{this.change}} />`);

      expect(this.element.querySelector('label')).to.have.textContent('custom question *');
      expect(this.element.querySelector('.input-group-help')).to.have.textContent('custom help');
      expect(this.element.querySelector('input')).to.have.attribute('type', 'email');

      await fillIn('input', 'a@b.c');

      expect(value).to.deep.equal({
        other: 'a@b.c',
      });
    });

    it('does not render an email question when isVisible=false', async function (assert) {
      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'other',
          isRequired: true,
          isVisible: false,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onChange={{this.change}} />`);

      expect(this.element.querySelector('label')).not.to.have.exist;
      expect(this.element.querySelector('.input-group-help')).not.to.exist;
      expect(this.element.querySelector('input')).not.to.exist;
    });

    it('sets the current user email when useCurrentUser is true', async function () {
      const service = this.owner.lookup('service:user');
      service.userData = {
        email: 'contact@szabobogdan.com',
        username: 'bogdan_szabo',
        name: 'Bogdan Szabo',
        _id: '5b870669796da25424540deb',
      };

      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'email',
          isVisible: true,
          isRequired: true,
        },
      ]);

      await render(
        hbs`<Input::QuestionList @questions={{this.questions}} @useCurrentUser={{true}} @onChange={{this.change}} />`,
      );

      expect(this.element.querySelector('input')).not.to.exist;
      expect(this.element.querySelector('.value-read-only')).to.have.textContent('contact@szabobogdan.com');

      expect(value).to.deep.equal({
        email: 'contact@szabobogdan.com',
      });
    });

    it('ignores the current user email when useCurrentUser is true and the field is not named email', async function () {
      const service = this.owner.lookup('service:user');
      service.userData = {
        email: 'contact@szabobogdan.com',
        username: 'bogdan_szabo',
        name: 'Bogdan Szabo',
        _id: '5b870669796da25424540deb',
      };

      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'other',
          isVisible: true,
          isRequired: true,
        },
      ]);

      await render(
        hbs`<Input::QuestionList @questions={{this.questions}} @useCurrentUser={{true}} @onChange={{this.change}} />`,
      );

      expect(this.element.querySelector('input')).to.exist;
      expect(this.element.querySelector('input').value).to.equal('');

      expect(value).not.to.exist;
    });
  });

  describe('the geo json questions', function (hooks) {
    let position;

    hooks.beforeEach(function () {
      position = this.owner.lookup('service:position');
      position.status = 'WATCHING';
      position.longitude = 1;
      position.latitude = 2;
    });

    it('renders an geo json question', async function (a) {
      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'geo json',
          name: 'other',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onChange={{this.change}} />`);

      await click('.btn-gps');

      a.deepEqual(value.other.toJSON(), { type: 'Point', coordinates: [1, 2] });

      position.longitude = 10;
      position.latitude = 20;

      a.deepEqual(value.other.toJSON(), { type: 'Point', coordinates: [10, 20] });
    });
  });

  describe('the pictures questions', function (hooks) {
    it('renders a pictures question', async function (assert) {
      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'pictures',
          name: 'other',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onChange={{this.change}} />`);

      const blob = server.testData.create.pngBlob();
      await triggerEvent(".image-list-input[type='file']", 'change', {
        files: [blob],
      });

      await waitFor('.image-item-container');

      expect(this.element.querySelectorAll('.image-item-container')).to.have.length(1);

      expect(value.other).to.have.length(1);
    });
  });

  describe('the sounds questions', function (hooks) {
    it('renders a sounds question', async function (assert) {
      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'sounds',
          name: 'other',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onChange={{this.change}} />`);

      const blob = server.testData.create.mp3Blob();
      await triggerEvent(".sound-list-input[type='file']", 'change', {
        files: [blob],
      });

      expect(this.element.querySelectorAll('.sound-list .item')).to.have.length(1);
      expect(value.other).to.have.length(1);
    });
  });

  describe('the icons questions', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultIcon('1');
      server.testData.storage.addDefaultIcon('2');
    });

    it('renders an icons question', async function (assert) {
      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'icons',
          name: 'other',
          isRequired: true,
          isVisible: true,
          options: ['1', '2'],
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onChange={{this.change}} />`);

      await click('.icon-element');

      expect(value.other).to.have.length(1);
      expect(value.other[0].id).to.equal('1');
    });
  });

  describe('questions without name', function (hooks) {
    it('is not rendered', async function (assert) {
      let value;
      this.set('change', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'text',
          name: '',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onChange={{this.change}} />`);

      expect(this.element.querySelector('.attribute-value')).not.to.exist;
    });
  });

  describe('validation', function () {
    it('calls onValidate with false on the first render, on initial render when there are required fields', async function () {
      let value;
      this.set('onValidate', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'other',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onValidate={{this.onValidate}} />`);

      expect(value).to.equal(false);
    });

    it('calls onValidate with true on the first render, on initial render when there are no required fields', async function () {
      let value;
      this.set('onValidate', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'other',
          isRequired: false,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onValidate={{this.onValidate}} />`);

      expect(value).to.equal(true);
    });

    it('calls onValidate with true when a required field is filled in', async function () {
      let value;
      this.set('onValidate', (v) => {
        value = v;
      });

      this.set('questions', [
        {
          question: 'custom question',
          help: 'custom help',
          type: 'email',
          name: 'other',
          isRequired: true,
          isVisible: true,
        },
      ]);

      await render(hbs`<Input::QuestionList @questions={{this.questions}} @onValidate={{this.onValidate}} />`);

      await fillIn('input', 'a@b.c');

      expect(value).to.equal(true);
    });
  });
});
