/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { service } from '@ember/service';
import { fromPageState } from 'core/lib/page-state';

export default class PageColPriceOfferComponent extends Component {
  @service fastboot;

  get currencyCode() {
    const state = fromPageState(this.args.state);

    return state.cu ?? this.args.value?.data?.priceList?.[0]?.currency;
  }

  get price() {
    const currency = this.currencyCode?.toLowerCase();

    return this.args.value?.data?.priceList?.find(
      (a) => a.currency?.toLowerCase() == currency,
    );
  }

  get customPriceRowStyle() {
    if (this.fastboot.isFastBoot) {
      return htmlSafe('');
    }

    const text = this.price?.value ?? '';

    if (text) {
      return htmlSafe('');
    }

    const otherPrice = document
      .querySelector('.price-offer .price-row .offer-price')
      ?.getBoundingClientRect?.();
    if (!otherPrice) {
      return htmlSafe('');
    }

    return htmlSafe(`min-height: ${otherPrice.height}px;`);
  }

  get currency() {
    const currency = this.price?.currency ?? '';

    switch (currency.toLowerCase()) {
      case 'usd':
        return 'currency.usd';
      case 'eur':
        return 'currency.eur';
      case 'gbp':
        return 'currency.gbp';
      default:
        return 'currency.default';
    }
  }

  get customClasses() {
    const priceClasses = this.args?.value?.data?.price?.classes ?? [];

    return priceClasses
      .filter((a) => a.indexOf('text-') == 0)
      .map((a) => a.replace('text-', 'justify-content-'));
  }

  get buttonContainerClasses() {
    const priceClasses = this.args?.value?.data?.price?.classes ?? [];

    return priceClasses.filter(
      (a) => a.indexOf('text-') == 0 && a.indexOf('text-size') != 0,
    );
  }
}
