/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

module('Integration | Component | view/event/dates', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let eventData;

  hooks.before(function () {
    server = new TestServer();
    eventData = server.testData.storage.addDefaultEvent('1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::Event::Dates />`);

    assert.dom(this.element).hasText('');
  });

  it('renders the text style', async function (a) {
    this.set('value', {
      data: {
        source: { model: 'event', id: '1' },
        style: {
          color: 'green',
          classes: ['fw-bold'],
        },
      },
      modelKey: 'event',
    });

    eventData.entries[0].begin = '2018-07-01T12:30:10Z';
    eventData.entries[0].end = '2023-07-01T13:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'yearly';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.entry')).to.have.classes(
      'text-with-options fw-bold text-green entry',
    );
  });

  it('renders a yearly entry that happens in one day', async function (assert) {
    this.set('value', {
      data: {
        source: { model: 'event', id: '1' },
        style: {
          color: 'green',
          classes: ['fw-bold'],
        },
      },
      modelKey: 'event',
    });

    eventData.entries[0].begin = '2018-07-01T12:30:10Z';
    eventData.entries[0].end = '2023-07-01T13:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'yearly';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert
      .dom(this.element)
      .hasText('Yearly, from 2018 to 2023, on July 1, 14:30 to 15:30');
  });

  it('renders a yearly entry that happens in 3 days', async function (assert) {
    this.set('value', {
      data: {
        source: { model: 'event', id: '1' },
        style: {
          color: 'green',
          classes: ['fw-bold'],
        },
      },
      modelKey: 'event',
    });

    eventData.entries[0].begin = '2018-07-01T13:30:10Z';
    eventData.entries[0].end = '2018-07-03T18:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-03T18:30:10Z';
    eventData.entries[0].repetition = 'yearly';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert
      .dom(this.element)
      .hasText('Yearly, from 2018 to 2023, on July 1, 15:30 to July 3, 20:30');
  });

  it('renders a monthly entry', async function (assert) {
    this.set('value', {
      data: {
        source: { model: 'event', id: '1' },
        style: {
          color: 'green',
          classes: ['fw-bold'],
        },
      },
      modelKey: 'event',
    });

    eventData.entries[0].intervalEnd = '2018-07-01T13:30:10Z';
    eventData.entries[0].begin = '2018-07-01T13:30:10Z';
    eventData.entries[0].end = '2023-07-01T18:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'monthly';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert
      .dom(this.element)
      .hasText(
        'Monthly, from July 2018 to July 2023, on July 1, 15:30 to 20:30',
      );
  });

  it('renders a weekly entry', async function (assert) {
    this.set('value', {
      data: {},
      modelKey: 'event',
    });

    eventData.entries[0].intervalEnd = '2018-07-01T13:30:10Z';
    eventData.entries[0].begin = '2018-07-01T13:30:10Z';
    eventData.entries[0].end = '2023-07-01T18:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'weekly';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert
      .dom(this.element)
      .hasText(
        'Weekly, from 1 July 2018 to 1 July 2023, on Sunday, 15:30 to 20:30',
      );
  });

  it('renders a weekly entry in 3 days', async function (assert) {
    this.set('value', {
      data: {},
      modelKey: 'event',
    });

    eventData.entries[0].intervalEnd = '2018-07-01T13:30:10Z';
    eventData.entries[0].begin = '2018-07-01T13:30:10Z';
    eventData.entries[0].end = '2023-07-03T18:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'weekly';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert
      .dom(this.element)
      .hasText(
        'Weekly, from 1 July 2018 to 1 July 2023, on Sunday, 15:30 to Monday, 20:30',
      );
  });

  it('renders a daily entry', async function (assert) {
    this.set('value', {
      data: {},
      modelKey: 'event',
    });

    eventData.entries[0].intervalEnd = '2018-07-01T13:30:10Z';
    eventData.entries[0].begin = '2018-07-01T13:30:10Z';
    eventData.entries[0].end = '2023-07-01T18:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'daily';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert
      .dom(this.element)
      .hasText('Daily, from 1 July 2018 to 1 July 2023, 15:30 to 20:30');
  });

  it('renders a daily entry that happens in 3 days', async function (assert) {
    this.set('value', {
      data: {},
      modelKey: 'event',
    });

    eventData.entries[0].intervalEnd = '2018-07-01T13:30:10Z';
    eventData.entries[0].begin = '2018-07-01T13:30:10Z';
    eventData.entries[0].end = '2023-07-03T18:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'daily';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert
      .dom(this.element)
      .hasText(
        'Daily, from 1 July 2018 to 1 July 2023, 15:30 to July 3, 20:30',
      );
  });

  it('renders an entry with no repetitions', async function (assert) {
    this.set('value', {
      data: {},
      modelKey: 'event',
    });

    eventData.entries[0].intervalEnd = '2018-07-01T13:30:10Z';
    eventData.entries[0].begin = '2018-07-01T13:30:10Z';
    eventData.entries[0].end = '2023-07-01T18:30:10Z';
    eventData.entries[0].intervalEnd = '2023-07-01T13:30:10Z';
    eventData.entries[0].repetition = 'norepeat';

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert.dom(this.element).hasText('1 July 2018, 15:30 to 20:30');
  });

  it('renders an entry with a different timezone', async function (assert) {
    this.set('value', {
      data: {},
      modelKey: 'event',
    });

    eventData.entries[0] = {
      intervalEnd: '0001-01-01T22:26:02Z',
      repetition: 'norepeat',
      end: '2024-11-11T22:30:00Z',
      begin: '2024-11-11T21:00:00Z',
      timezone: 'America/New_York',
    };

    const event = await this.store.findRecord('event', eventData._id);
    this.set('model', {
      event,
    });

    await render(
      hbs`<View::Event::Dates @model={{this.model}} @value={{this.value}} />`,
    );

    assert.dom(this.element).hasText('11 November 2024, 22:00 to 23:30');
  });
});
