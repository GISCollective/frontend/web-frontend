/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/article/date', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  let featureData;

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    server.testData.storage.addDefaultPreferences();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Article::Date />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the feature creation date', async function () {
    featureData = server.testData.storage.addDefaultFeature();

    const feature = await store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {},
      modelKey: 'feature',
    });

    this.set('model', { feature });

    await render(
      hbs`<View::Article::Date @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelector('.label').textContent.trim()).to.equal(
      'added on',
    );
    expect(this.element.querySelector('.date').textContent.trim()).to.equal(
      'October 31, 2009',
    );
  });

  it('renders the feature release date', async function () {
    let articleData = server.testData.storage.addDefaultArticle();

    const article = await store.findRecord('article', articleData._id);
    article.releaseDate = new Date('2022-02-03T12:22:22Z');

    this.set('value', {
      data: {
        field: { name: 'releaseDate' },
      },
      modelKey: 'article',
    });

    this.set('model', { article });

    await render(
      hbs`<View::Article::Date @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelector('.label').textContent.trim()).to.equal(
      'released on',
    );
    expect(this.element.querySelector('.date').textContent.trim()).to.equal(
      'February 3, 2022',
    );
  });

  it('renders the feature last change date', async function () {
    featureData = server.testData.storage.addDefaultFeature();
    featureData.info.lastChangeOn = '2022-12-02T00:22:33Z';

    const feature = await store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {
        field: { name: 'info.lastChangeOn' },
      },
      modelKey: 'feature',
    });

    this.set('model', { feature });

    await render(
      hbs`<View::Article::Date @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelector('.label').textContent.trim()).to.equal(
      'last change on',
    );
    expect(this.element.querySelector('.date').textContent.trim()).to.equal(
      'December 2, 2022',
    );
  });

  it('adds the label classes', async function () {
    featureData = server.testData.storage.addDefaultFeature();

    const feature = await store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {
        date: { type: 'last change' },
        label: { color: 'green', classes: ['fw-bold', 'display-2'] },
      },
      modelKey: 'feature',
    });

    this.set('model', { feature });

    await render(
      hbs`<View::Article::Date @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelector('.label')).to.have.classes(
      'label fw-bold display-2 text-green',
    );
  });

  it('adds the date classes', async function () {
    featureData = server.testData.storage.addDefaultFeature();

    const feature = await store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {
        date: {
          type: 'last change',
          color: 'green',
          classes: ['fw-bold', 'display-2'],
        },
      },
      modelKey: 'feature',
    });

    this.set('model', { feature });

    await render(
      hbs`<View::Article::Date @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(this.element.querySelector('.date')).to.have.classes(
      'date fw-bold display-2 text-green',
    );
  });
});
