/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "../attributes/value-options-other";

export default class ManageEditorsOptionsWithOtherComponent extends Component {
  get optionsList() {
    return this.args.options?.values ?? [];
  }

  get options() {
    return super.options.split(",");
  }
}
