/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectChoose, selectSearch } from "ember-power-select/test-support";
import waitUntil from "@ember/test-helpers/wait-until";

describe("Integration | Component | editor/user", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let user1;
  let user2;

  hooks.before(function () {
    server = new TestServer();
    user1 = server.testData.storage.addDefaultUser(false, "1");
    server.testData.storage.addDefaultProfile("1");

    user2 = server.testData.storage.addDefaultUser(false, "2");
    user2.username = "gedaiu2";
    server.testData.storage.addDefaultProfile("2");
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the values", async function () {
    this.set("value", user1);

    await render(hbs`<Editor::User @value={{this.value._id}} />`);

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("gedaiu");
  });

  it("can change the values", async function () {
    this.set("value", user1);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::User @value={{this.value._id}} @onChange={{this.change}} />`);

    await selectSearch(".ember-power-select-trigger", "gedaiu");
    selectChoose(".ember-power-select-trigger", "gedaiu2");

    await waitUntil(() => value);
    await server.waitAllRequests();

    expect(value).to.equal("2");
  });
});
