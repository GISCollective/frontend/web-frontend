import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | input/geo-json-sources', function (hooks) {
  setupRenderingTest(hooks);

  it('renders and triggers onChange with the default value when the value is not set', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::GeoJsonSources @onChange={{this.change}}/>`);

    const inputs = this.element.querySelectorAll('.form-check-input:checked');

    expect(inputs).to.have.length(3);
    expect(value).to.deep.equal({ allowGps: true, allowManual: true, allowAddress: true, allowExistingFeature: false });
  });

  it('renders the provided value and does not trigger onChange', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', { allowGps: true, allowManual: false, allowAddress: false, allowExistingFeature: false });

    await render(hbs`<Input::GeoJsonSources @value={{this.value}} @onChange={{this.change}}/>`);

    const inputs = this.element.querySelectorAll('.form-check-input:checked');

    expect(inputs).to.have.length(1);
    expect(value).not.to.exist;
  });

  it('triggers onChange when a value is toggled', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', { allowGps: true, allowManual: false, allowAddress: false, allowExistingFeature: false });

    await render(hbs`<Input::GeoJsonSources @value={{this.value}} @onChange={{this.change}}/>`);

    const inputs = this.element.querySelectorAll('.form-check-input');
    await click(inputs[1]);

    expect(value).to.deep.equal({
      allowGps: true,
      allowManual: true,
      allowAddress: false,
      allowExistingFeature: false,
    });
  });
});
