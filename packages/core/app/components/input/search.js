/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputSearchComponent extends Component {
  elementId = `input-search-${guidFor(this)}`;
  @tracked searchTerm = '';
  @tracked isSearching = false;

  @action
  async clear() {
    this.searchTerm = '';
    await this.search();
  }

  @action
  async search() {
    this.isSearching = true;
    try {
      await this.args.onSearch?.(this.searchTerm);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.warn(err);
    }
    this.isSearching = false;
  }

  @action
  async keyUp(event) {
    if (event.key == 'Enter' || event.keyCode == 13) {
      await this.search();
    }
  }
}
