/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | input/manage/calendar-entry-list', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders the value', async function (assert) {
      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'monthly',
          timezone: 'CET',
        },
      ]);

      await render(hbs`<Manage::CalendarEntryList @value={{this.value}} @title="events" />`);

      expect(this.element.querySelector('.calendar-entry')).to.have.textContent(
        `monthly between 01/05/2023 and 01/05/2024 on 01 02:00 - 03:00`,
      );
    });
  });

  describe('in edit mode', function () {
    it('renders the value', async function (assert) {
      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'monthly',
          timezone: 'CET',
        },
      ]);

      await render(hbs`<Manage::CalendarEntryList @value={{this.value}} @title="events" @editablePanel="events" />`);

      const entries = this.element.querySelectorAll('.input-calendar-entry');

      expect(entries).to.have.length(1);

      expect(this.element.querySelector('.value-begin .datepicker-input').value).to.equal(`2023-5-1`);
      expect(this.element.querySelector('.value-end .datepicker-input').value).to.equal(`2023-6-1`);
    });

    it('can edit the entry', async function (assert) {
      let value;

      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'monthly',
          timezone: 'CET',
        },
      ]);

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::CalendarEntryList @onSave={{this.save}} @value={{this.value}} @title="events" @editablePanel="events" />`,
      );

      this.element.querySelector('.value-begin select.hour').value = '15';
      await triggerEvent('.value-begin select.hour', 'change');

      await click('.btn-submit');

      expect(value.map((a) => a.toJSON())).to.deep.equal([
        {
          begin: '2023-05-01T13:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'monthly',
          timezone: 'CET',
        },
      ]);
    });

    it('can cancel the changes', async function (assert) {
      let value;

      this.set('value', [
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'monthly',
          timezone: 'CET',
        },
      ]);

      let cancel;
      this.set('cancel', () => {
        cancel = true;
      });

      this.set('save', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::CalendarEntryList @onCancel={{this.cancel}} @onSave={{this.save}} @value={{this.value}} @title="events" @editablePanel="events" />`,
      );

      this.element.querySelector('.value-begin select.hour').value = '15';
      await triggerEvent('.value-begin select.hour', 'change');

      await click('.btn-cancel');

      expect(value).not.to.exist;
      expect(cancel).to.equal(true);
      expect(this.value).to.deep.equal([
        {
          begin: '2023-05-01T00:00:00.000Z',
          end: '2023-06-01T01:00:00.000Z',
          intervalEnd: '2024-05-01T01:00:00.000Z',
          repetition: 'monthly',
          timezone: 'CET',
        },
      ]);
    });
  });
});
