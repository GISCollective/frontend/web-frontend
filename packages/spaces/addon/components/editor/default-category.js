/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorDefaultCategoryComponent extends Component {
  @tracked _value;

  get value() {
    if (typeof this._value == "string") {
      return this._value;
    }

    return this.args.value?.value;
  }

  @action
  change(newValue) {
    this._value = newValue;

    this.args.onChange?.({ value: newValue });
  }
}
