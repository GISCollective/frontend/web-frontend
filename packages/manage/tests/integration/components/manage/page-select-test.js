/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/page-select', function (hooks) {
  setupRenderingTest(hooks);

  describe('in the view mode', function (hooks) {
    it('renders the title', async function () {
      await render(hbs`<Manage::PageSelect @title="test" @isMainValue={{true}}/>`);

      expect(this.element.querySelector('.panel-title').textContent.trim()).to.equal('test');
    });

    it('renders the value', async function () {
      this.set('value', { name: 'selected page' });

      await render(hbs`<Manage::PageSelect @title="test" @isMainValue={{true}} @value={{this.value}} />`);
      expect(this.element.querySelector('a').textContent.trim()).to.equal(`selected page`);
    });

    it('renders an error message when the value is not set and the emptyError exists', async function () {
      await render(hbs`<Manage::PageSelect @title="test" @isMainValue={{true}} />`);
      expect(this.element.querySelector('.alert').textContent.trim()).to.equal('No page is set.');
    });
  });

  describe('in edit mode', function (hooks) {
    hooks.beforeEach(function () {
      this.set('value', { id: '1', name: 'selected page' });
      this.set('list', [
        { id: '1', name: 'selected page' },
        { id: '2', name: 'other page' },
      ]);
    });

    it('should render an input text with the value', async function () {
      await render(
        hbs`<Manage::PageSelect @title="test" @editablePanel="test" @isMainValue={{true}} @list={{this.list}} @value={{this.value}}/>`,
      );

      var e = this.element.querySelector('select');
      var value = e.options[e.selectedIndex].value;

      expect(value).to.equal('1');
    });

    it('should save a new value', async function () {
      let value;

      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::PageSelect @title="test" @editablePanel="test" @isMainValue={{true}} @list={{this.list}} @value={{this.value}} @onSave={{this.save}}/>`,
      );

      this.element.querySelector('select').value = '2';
      await triggerEvent('select', 'change');

      await click('.btn-submit');

      expect(value).to.deep.equal({ id: '2', name: 'other page' });
    });
  });
});
