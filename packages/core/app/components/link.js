/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { resolveLinkObject } from '../lib/links';
import { later } from '@ember/runloop';
import config from '../config/environment';

export default class LinkComponent extends Component {
  @service space;
  @service router;

  @tracked _value;
  @tracked type;

  get target() {
    return this.args.newTab ? '_blank' : undefined;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    return resolveLinkObject(
      this.args.to,
      this.args.space,
      this.space?.currentSpace,
      this.args.query,
      this.args.variables,
    );
  }

  get isMissing() {
    return !this.value || Object.keys(this.value).length == 0;
  }

  @action
  ignoreAction(event) {
    event.preventDefault();

    return false;
  }

  @action
  async handleClick(page, event) {
    if (this.args.newTab) {
      return;
    }

    event.preventDefault();
    const pieces = page.split('#');
    const info = this.router?.recognize(page) ?? {};

    /// hack to avoid replacing the current url when it has query params
    if (info.name == 'page' && config.locationType != 'none') {
      try {
        window.history.pushState(window.history.state, '', this.router.currentURL);
      } catch (err) {
        console.log('ERR', err);
      }
    }

    try {
      await this.router.transitionTo(pieces[0]);
    } catch (err) {
      console.log('ERR', err);
    }

    if (pieces[1]) {
      promise.then(() => {
        later(() => {
          const location = window.location.toString();

          if (location.indexOf('#') == -1) {
            window.history.replaceState(history.state, '', location + '#' + pieces[1]);
            window.location.hash = pieces[1];
          }
        });
      });
    }

    return false;
  }
}
