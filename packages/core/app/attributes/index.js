/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';

import AttributesContainer from './containers/attributes';

export default class AttributesForm {
  @tracked groups = A([]);
  @tracked _feature;

  constructor(intl) {
    this.attributeFactory = new AttributesContainer(intl);
  }

  createGroups() {
    const result = A(this.attributeFactory.groups);

    this.updateLoadingState(A(result));

    return result;
  }

  get loadingList() {
    return this._loadingList;
  }

  set loadingList(value) {
    this._loadingList = value || [];

    this.updateLoadingState(this.groups);
  }

  updateLoadingState(groups) {
    if (!this._loadingList) return;

    groups.forEach((group) => {
      group.isLoading = this._loadingList.indexOf(group.name) != -1;

      if (group.type == 'pictureList' || group.type == 'iconList') {
        return;
      }

      group.list
        .filter((a) => a.set && !a.isDestroyed)
        .forEach((item) => {
          const key = group.name + '.' + item.name;
          item.set('isLoading', this._loadingList.indexOf(key) != -1);
        });

      group.list
        .filter((a) => !a.set)
        .forEach((item) => {
          const key = group.name + '.' + item.name;
          item.isLoading = this._loadingList.indexOf(key) != -1;
        });
    });
  }

  get feature() {
    return this._feature;
  }

  set feature(value) {
    this._feature = value;

    this.attributeFactory.feature = value;

    this.updateGroups();
  }

  updateGroups() {
    let changed = false;
    const newGroups = this.createGroups();

    newGroups.forEach((group, index) => {
      if (index >= this.groups.length) {
        this.groups.push(group);
        changed = true;
        return;
      }

      const oldGroup = this.groups.objectAt(index);

      if (oldGroup.key == group.key && oldGroup.type == group.type && oldGroup.addButton == group.addButton) {
        return;
      }

      this.groups[index] = group;
      changed = true;
    });

    if (changed) {
      // eslint-disable-next-line no-self-assign
      this.groups = this.groups;
    }
  }
}
