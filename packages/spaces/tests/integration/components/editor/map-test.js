/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from "qunit";
import { setupRenderingTest, it } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

module("Integration | Component | editor/map editor", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the values", async function (a) {
    this.set("value", {
      mode: "show all features",
      featureHover: "do nothing",
      enablePinning: true,
      featureSelection: "do nothing",
      flyOverAnimation: true,
      enableMapInteraction: "no",
      restrictView: true,
      showUserLocation: true,
      showZoomButtons: true,
      showBaseMapSelection: true,
    });

    await render(hbs`<Editor::Map @value={{this.value}} />`);

    expect(this.element.querySelector("select.mode").value).to.equal("show all features");
    expect(this.element.querySelector("select.fly-over-animation").value).to.equal("yes");
    expect(this.element.querySelector("select.enable-map-interaction").value).to.equal("no");
    expect(this.element.querySelector("select.show-user-location").value).to.equal("yes");
  });

  it("can change the values", async function (a) {
    this.set("value", {
      mode: "show all features",
      featureHover: "do nothing",
      enablePinning: true,
      featureSelection: "do nothing",
      flyOverAnimation: true,
      enableMapInteraction: "no",
      restrictView: true,
      showUserLocation: true,
      showZoomButtons: true,
      showBaseMapSelection: true,
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Map @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".fly-over-animation").value = "no";
    await triggerEvent(".fly-over-animation", "change");

    this.element.querySelector(".show-user-location").value = "no";
    await triggerEvent(".show-user-location", "change");

    expect(value).to.deep.equal({
      mode: "show all features",
      featureHover: "do nothing",
      enablePinning: true,
      featureSelection: "do nothing",
      flyOverAnimation: false,
      enableMapInteraction: "no",
      restrictView: true,
      showUserLocation: false,
      showZoomButtons: true,
      showBaseMapSelection: true,
      showTitle: false,
      enableFullMapView: false,
    });
  });

  it("can set to show the popup on feature selection", async function (a) {
    this.set("value", {
      mode: "show all features",
      featureHover: "do nothing",
      enablePinning: true,
      featureSelection: "do nothing",
      flyOverAnimation: true,
      enableMapInteraction: "no",
      restrictView: true,
      showUserLocation: true,
      showZoomButtons: true,
      showBaseMapSelection: true,
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Map @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".feature-selection").value = "show popup";
    await triggerEvent(".feature-selection", "change");

    expect(value.featureSelection).to.deep.equal("show popup");
  });

  it("can set to show the full map view button", async function (a) {
    this.set("value", {
      mode: "show all features",
      featureHover: "do nothing",
      enablePinning: true,
      featureSelection: "do nothing",
      flyOverAnimation: true,
      enableMapInteraction: "no",
      restrictView: true,
      showUserLocation: true,
      showZoomButtons: true,
      showBaseMapSelection: true,
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Map @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".enable-full-map-view").value = "yes";
    await triggerEvent(".enable-full-map-view", "change");

    expect(value.enableFullMapView).to.deep.equal(true);
  });
});
