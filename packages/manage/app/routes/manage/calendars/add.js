/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { ModelInfo } from '../../../transforms/model-info';
import { ModelListWithDefault } from '../../../transforms/model-list-with-default';

export default class ManageCalendarsAddRoute extends AuthenticatedRoute {
  modelsToClear = ['calendar'];

  model(params) {
    if (this.isFastBoot) {
      return;
    }

    return hash({
      calendar: this.store.createRecord('calendar', {
        name: '',
        info: new ModelInfo(),
        iconSets: new ModelListWithDefault({}, this.store, 'icon-set'),
        visibility: {
          isPublic: false,
          team: null,
        },
      }),
    });
  }
}
