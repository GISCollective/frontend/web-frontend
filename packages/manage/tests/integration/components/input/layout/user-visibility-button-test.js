/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/layout/user-visibility-button', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a button with an user slash icon', async function () {
    await render(hbs`<Input::Layout::UserVisibilityButton />`);

    expect(this.element.querySelector('.btn .fa-user-slash')).to.exist;
    expect(this.element.querySelector('.btn .fa-user')).not.to.exist;
  });

  it('renders a button with an user icon when the condition contains "user:authenticated"', async function () {
    this.set('conditions', ['user:authenticated']);

    await render(hbs`<Input::Layout::UserVisibilityButton @conditions={{this.conditions}} />`);

    expect(this.element.querySelector('.btn .fa-user-slash')).not.to.exist;
    expect(this.element.querySelector('.btn .fa-user')).to.exist;
  });

  it('triggers on change event with "user:unauthenticated" when the value is "user:authenticated" and button pressed', async function () {
    this.set('conditions', ['user:authenticated']);

    let value;
    this.set('onChange', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Layout::UserVisibilityButton @conditions={{this.conditions}} @onChange={{this.onChange}} />`,
    );

    await click('.btn');

    expect(value).to.deep.equal(['user:unauthenticated']);
  });

  it('triggers on change event with "user:authenticated" when the value is "user:unauthenticated" and button pressed', async function () {
    this.set('conditions', ['user:unauthenticated']);

    let value;
    this.set('onChange', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Layout::UserVisibilityButton @conditions={{this.conditions}} @onChange={{this.onChange}} />`,
    );

    await click('.btn');

    expect(value).to.deep.equal(['user:authenticated']);
  });
});
