import setupDeprecationWorkflow from 'ember-cli-deprecation-workflow';

setupDeprecationWorkflow({
  workflow: [
    { handler: "log", matchId: "ember-data:no-a-with-array-like" },
    { handler: "log", matchId: "ember-data:deprecate-promise-many-array-behaviors" },
    { handler: "log", matchId: "ember-data:deprecate-array-like" }
  ]
});