/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { CalendarEntry } from 'models/transforms/calendar-entry-list';
import { service } from '@ember/service';
import { DateTime } from 'luxon';

export default class CalendarEntryComponent extends Component {
  @service intl;

  get value() {
    return new CalendarEntry(this.args.value);
  }

  get norepeat() {
    return this.value?.repetition == 'norepeat';
  }

  get repeatDetail() {
    if (this.value?.repetition == 'weekly') {
      return '`' + this.value.begin.toLocaleString({ weekday: 'long' }) + '`';
    }

    if (this.value?.repetition == 'monthly') {
      return this.intl.t('date-on-detail', { date: this.value.begin.toFormat('dd') });
    }

    if (this.value?.repetition == 'yearly') {
      return this.intl.t('date-on-detail', { date: this.value.begin.toFormat('LLL dd') });
    }

    return '';
  }

  get niceRepetitionDate() {
    if (!this.args.value || this.norepeat) {
      return '';
    }

    const begin = this.value.begin.toLocaleString();
    const end = this.value.intervalEnd.toLocaleString();

    return `${this.intl.t(`repeat-${this.value.repetition}-until`, { begin, end })}`;
  }

  get niceRepetitionTime() {
    if (!this.args.value || this.norepeat) {
      return '';
    }

    return (
      '`' +
      this.value.begin.toLocaleString(DateTime.TIME_SIMPLE) +
      '` - `' +
      this.value.end.toLocaleString(DateTime.TIME_SIMPLE) +
      '`'
    );
  }

  get timezone() {
    const here = DateTime.now().zoneName;

    if (here == this.value.begin?.zoneName) {
      return '';
    }

    return this.value.begin?.zoneName;
  }
}
