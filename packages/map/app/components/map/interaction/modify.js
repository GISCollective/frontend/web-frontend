/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import Modify from 'ol/interaction/Modify';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class MapInteractionModifyComponent extends Component {
  @tracked modify;
  @tracked map;

  @action
  setup() {
    this.removeInteraction();
    if (!this.args.features || !this.args.map) {
      return;
    }

    later(() => {
      this.map = this.args.map;
      this.modify = new Modify({
        features: this.args.features,
      });

      this.map.addInteraction(this.modify);

      this.modify.on('modifyend', (ev) => {
        this.triggerChange(ev);
      });
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.removeInteraction();
  }

  removeInteraction() {
    if (this.modify) {
      this.map.removeInteraction(this.modify);
      this.modify = null;
    }
  }

  triggerChange(event) {
    this.args.onChange(event);
  }
}
