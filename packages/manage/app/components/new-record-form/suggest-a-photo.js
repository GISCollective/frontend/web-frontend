/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './issue';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class NewRecordFormSuggestAPhotoComponent extends Component {
  @tracked attributions;
  @tracked picture;

  async createIssue() {
    const issue = super.createIssue();

    issue.attributions = this.attributions;
    issue.file = this.picture;

    return issue;
  }

  get isInvalid() {
    return super.isInvalid || !this.attributions || !this.picture;
  }

  @action
  setPhoto(_, value) {
    this.picture = value.picture;
  }
}
