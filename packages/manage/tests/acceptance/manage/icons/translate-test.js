/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, waitUntil, click, fillIn, waitFor } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/icons/translate', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedIcon;
  let iconSet;
  let icon;

  hooks.beforeEach(function () {
    server = new TestServer();
    iconSet = server.testData.create.iconSet();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPage();
    server.testData.storage.addIconSet(iconSet);
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  describe('with the default icon', function (hooks) {
    hooks.beforeEach(function () {
      icon = server.testData.create.icon();
      server.testData.storage.addIcon(icon);
      server.testData.storage.addDefaultTranslation();

      server.put('/mock-server/icons/' + icon._id, (request) => {
        receivedIcon = JSON.parse(request.requestBody);
        receivedIcon.icon._id = icon._id;

        server.testData.storage.addIcon(receivedIcon.icon);

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedIcon)];
      });

      receivedIcon = null;
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      expect(currentURL()).to.contain('/login');
    });

    it('should go to the icon edit page on edit button click', async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      await PageElements.waitEditorJs(this.element);

      await click('.btn-go-edit');

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });

    it("should go to 'icon edit' page on breadcrumb click", async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      await PageElements.waitEditorJs(this.element);

      const breadcrumbs = this.element.querySelectorAll('.breadcrumb a');

      await click(breadcrumbs[2]);
      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });

    it("should go to the 'your icon sets' page on breadcrumb click", async function () {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      await PageElements.waitEditorJs(this.element);

      const breadcrumbs = this.element.querySelectorAll('.breadcrumb a');
      await click(breadcrumbs[0]);

      expect(currentURL()).to.equal(`/manage/dashboards/5ca78e2160780601008f69e6`);
    });

    it('should show be able to change the article', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      await click('.row-article .btn-edit');

      await click('.ce-header');
      await fillIn('.ce-header', 'new name');
      await wait(400);

      await click('.ce-paragraph');
      await fillIn('.ce-paragraph', 'new description');
      await wait(400);

      await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
      await click('.btn-submit');

      await waitUntil(() => receivedIcon);

      expect(receivedIcon.icon.localName).to.equal('new name');
      expect(receivedIcon.icon.description.blocks).to.deep.equal([
        { type: 'header', data: { text: 'new name', level: 1 } },
        { type: 'paragraph', data: { text: 'new description' } },
      ]);
      expect(server.history).to.contain('PUT /mock-server/icons/5ca7bfc0ecd8490100cab980?locale=ro-ro');
    });

    it('should show be able to change the category', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      await click('.row-category .btn-edit');
      await fillIn('.row-category input', 'new category');
      await click('.btn-submit');

      await waitUntil(() => receivedIcon);

      expect(receivedIcon.icon.category).to.equal('new category');
      expect(receivedIcon.icon.subcategory).to.equal('Green Economy');
      expect(server.history).to.contain('PUT /mock-server/icons/5ca7bfc0ecd8490100cab980?locale=ro-ro');
    });

    it('should show be able to change the subcategory', async function () {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      await click('.row-subcategory .btn-edit');
      await fillIn('.row-subcategory input', 'new subcategory');
      await click('.btn-submit');

      await waitUntil(() => receivedIcon);

      expect(receivedIcon.icon.subcategory).to.equal('new subcategory');
      expect(receivedIcon.icon.category).to.equal('Sustainable Living');
      expect(server.history).to.contain('PUT /mock-server/icons/5ca7bfc0ecd8490100cab980?locale=ro-ro');
    });
  });
});
