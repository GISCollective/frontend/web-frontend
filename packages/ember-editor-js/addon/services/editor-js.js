/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class EditorJsService extends Service {
  @tracked editorCount;
  @tracked tools = {};

  async waitToDestroy() {
    return new Promise((resolve) => {
      if (this.editorCount == 0) {
        return resolve();
      }

      const timer = setInterval(() => {
        if (this.editorCount > 0) {
          return;
        }

        clearInterval(timer);
        return resolve();
      }, 10);
    });
  }
}
