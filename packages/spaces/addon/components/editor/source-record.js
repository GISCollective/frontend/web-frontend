/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";

export default class EditorSourceRecordComponent extends Component {
  get withPicture() {
    return this.args.element?.attributes?.getNamedItem?.("data-with-picture")?.value.toLowerCase?.() == "true";
  }

  get preferredModel() {
    return (
      this.args.element?.attributes?.getNamedItem?.("data-preferred-model")?.value.toLowerCase?.() ||
      this.args.preferredModel
    );
  }
}
