/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, triggerEvent, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/manage/data-binding/field-mapping',  function (hooks) {
  setupRenderingTest(hooks);

  describe ('in view mode',  function (hooks) {
    it('renders a message when the value is not set', async function () {
      await render(
        hbs`<Input::Manage::DataBinding::FieldMapping @title="fields" />`
      );

      expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
        'There are no fields to be mapped.'
      );
    });

    it('renders a map when the fields is a map', async function () {
      this.set('value', [
        { key: 'isPublished', destination: '', preview: [] },
        {
          key: 'some name',
          destination: 'name',
          preview: ['name 1', 'name 2'],
        },
        { key: '_id', destination: '', preview: [] },
        { key: 'maps ', destination: 'attributes', preview: [] },
        { key: 'position.lon', destination: 'position.lon', preview: [] },
        { key: 'description', destination: 'description', preview: [] },
        { key: 'pictures', destination: 'pictures', preview: [] },
        { key: 'icons ', destination: 'icons', preview: [] },
        { key: 'attributes', destination: '', preview: [] },
        { key: 'contributors ', destination: 'contributors', preview: [] },
        { key: 'position.lat', destination: 'position.lat', preview: [] },
      ]);

      await render(
        hbs`<Input::Manage::DataBinding::FieldMapping @title="fields" @value={{this.value}}/>`
      );

      const values = [...this.element.querySelectorAll('.binded-value')].map(
        (a) =>
          a.textContent
            .split(' ')
            .map((b) => b.trim())
            .filter((a) => a)
            .join(' ')
      );

      expect(values).to.deep.equal([
        'isPublished (ignored)',
        'some name name',
        '_id (ignored)',
        'maps attributes',
        'position.lon position.lon',
        'description description',
        'pictures pictures',
        'icons icons',
        'attributes (ignored)',
        'contributors contributors',
        'position.lat position.lat',
      ]);
    });
  });

  describe ('in view mode',  function (hooks) {
    it('renders the values', async function () {
      this.set('value', [
        { key: 'isPublished', destination: '', preview: [] },
        {
          key: 'some name',
          destination: 'name',
          preview: ['name 1', 'name 2'],
        },
        { key: '_id', destination: '', preview: [] },
        { key: 'maps ', destination: 'attributes', preview: [] },
        { key: 'position.lon', destination: 'position.lon', preview: [] },
        { key: 'description', destination: 'description', preview: [] },
        { key: 'pictures', destination: 'pictures', preview: [] },
        { key: 'icons ', destination: 'icons', preview: [] },
        { key: 'attributes', destination: '', preview: [] },
        { key: 'contributors ', destination: 'contributors', preview: [] },
        { key: 'position.lat', destination: 'position.lat', preview: [] },
      ]);

      await render(
        hbs`<Input::Manage::DataBinding::FieldMapping @title="fields" @editablePanel="fields" @value={{this.value}}/>`
      );

      const sourceFields = [
        ...this.element.querySelectorAll('.source-field'),
      ].map((a) => a.textContent.trim());

      expect(sourceFields).to.deep.equal([
        'isPublished',
        'some name',
        '_id',
        'maps',
        'position.lon',
        'description',
        'pictures',
        'icons',
        'attributes',
        'contributors',
        'position.lat',
      ]);

      const destinationFields = [
        ...this.element.querySelectorAll('.feature-field-mapping select'),
      ].map((a) => a.value);

      expect(destinationFields).to.deep.equal([
        '',
        'name',
        '',
        'position.lon',
        'description',
        'pictures',
        'icons',
        '',
        'contributors',
        'position.lat',
      ]);
    });

    it('changes the values on save', async function () {
      let value;

      this.set('save', (v) => {
        value = v;
      });

      this.set('value', [
        { key: 'isPublished', destination: '', preview: [] },
        {
          key: 'some name',
          destination: 'name',
          preview: ['name 1', 'name 2'],
        },
        { key: '_id', destination: '', preview: [] },
        { key: 'maps ', destination: 'attributes', preview: [] },
        { key: 'position.lon', destination: 'position.lon', preview: [] },
        { key: 'description', destination: 'description', preview: [] },
        { key: 'pictures', destination: 'pictures', preview: [] },
        { key: 'icons ', destination: 'icons', preview: [] },
        { key: 'attributes', destination: '', preview: [] },
        { key: 'contributors ', destination: 'contributors', preview: [] },
        { key: 'position.lat', destination: 'position.lat', preview: [] },
      ]);

      await render(
        hbs`<Input::Manage::DataBinding::FieldMapping @title="fields" @editablePanel="fields" @value={{this.value}} @onSave={{this.save}}/>`
      );

      const fieldMappings = this.element.querySelectorAll(
        '.feature-field-mapping'
      );

      const select = fieldMappings[0].querySelector('.form-select');
      select.value = 'description';
      await triggerEvent(select, 'change');

      await click('.btn-submit');

      expect(value[0]).to.deep.equal({
        key: 'isPublished',
        destination: 'description',
        preview: [],
      });
    });
  });
});
