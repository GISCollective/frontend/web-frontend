/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";

export default class InputComponentSelectorComponent extends Component {
  @service pageCols;

  get backgroundComponents() {
    return this.pageCols.backgroundComponents.map((a) => this.toButton(a));
  }

  get groups() {
    return [
      {
        groupName: "Background Components",
        options: this.backgroundComponents,
      },
    ];
  }

  toButton(name) {
    const icons = {
      banner: "newspaper",
      blocks: "paragraph",
      picture: "image",
      pictures: "images",
      sounds: "file-audio",
      article: "stream",
      icons: "chess-pawn",
      "campaign-card-list": "pencil-alt",
      menu: "bars",
    };
    const label = name
      .split("-")
      .flatMap((a) => a.split("/"))
      .join(" ");
    const cls = label.split(" ").join("-");

    return {
      name,
      label,
      cls,
      icon: icons[name],
      isSelected: name == this.args.value,
    };
  }
}
