/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service, { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import * as sphere from 'ol/sphere';

export default class PositionService extends Service {
  @service fastboot;
  @service toaster;

  @tracked errorMessage;
  @tracked status;

  @tracked latitude;
  @tracked longitude;
  @tracked accuracy;
  @tracked altitude;
  @tracked altitudeAccuracy;

  geolocation = null;
  timeout = 10000;

  constructor() {
    super(...arguments);

    if (!this.fastboot.isFastBoot && navigator) {
      this.geolocation = navigator.geolocation;
    }

    this.reset();
  }

  get isError() {
    return !!this.errorMessage;
  }

  get isAccurate() {
    return this.accuracy <= 5;
  }

  get isAlmostAccurate() {
    return this.accuracy > 5 && this.accuracy < 20;
  }

  get isNotAccurate() {
    return this.accuracy > 20;
  }

  get center() {
    if (this.longitude && this.latitude) {
      return [this.longitude, this.latitude];
    }

    return null;
  }

  get hasGeolocationSupport() {
    if (this.fastboot.isFastBoot) {
      return false;
    }

    return !!this.geolocation && !!this.geolocation.watchPosition;
  }

  get hasPosition() {
    if (!this.watchId) {
      return false;
    }

    return this.center !== null;
  }

  get isHeadlessBrowser() {
    let isHeadless = false;

    try {
      isHeadless = /\bHeadlessChrome\//.test(navigator.userAgent);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }

    return isHeadless;
  }

  get isWaiting() {
    return this.status == 'WAITING';
  }

  get hasGeoLocationPosition() {
    return !!this.center;
  }

  reset() {
    this.errorMessage = undefined;
    this.watchId = null;

    this.latitude = NaN;
    this.longitude = NaN;
    this.accuracy = NaN;
    this.altitude = NaN;
    this.altitudeAccuracy = NaN;
    this.status = 'PENDING';
    this._promise = null;
  }

  shouldUpdate(longitude, latitude, accuracy) {
    if (accuracy <= this.accuracy) {
      return true;
    }

    const distance = parseInt(
      sphere.getDistance([longitude, latitude], [parseFloat(this.longitude), parseFloat(this.latitude)]),
    );

    return isNaN(distance) || distance > 5;
  }

  @action
  async watchPosition() {
    if (this.center) {
      return Promise.resolve(this.center);
    }

    if (this.watchId) {
      return this._promise;
    }

    if (!this.hasGeolocationSupport) {
      this.status = 'NOT_AVAILABLE';
      throw new Error('Your device does not support geo location.');
    }

    this.status = 'WAITING';

    this.watchId = this.geolocation.watchPosition(
      (position) => {
        const accuracy = Math.round(position.coords.accuracy * 100) / 100;

        if (this.shouldUpdate(position.coords.longitude, position.coords.latitude, accuracy)) {
          this.longitude = position.coords.longitude;
          this.latitude = position.coords.latitude;
          this.accuracy = accuracy;
          this.altitude = position.coords.altitude;
          this.altitudeAccuracy = position.coords.altitudeAccuracy;
        }

        this.errorMessage = '';
        this.status = 'WATCHING';
      },
      (error) => {
        this.status = 'NOT_AVAILABLE';

        if (!this.center && !this.isError && this.lastCode != error?.code) {
          this.errorMessage = this.toaster.handleLocationError(error);
        }

        this.lastCode = error?.code;
      },
      {
        enableHighAccuracy: true,
        timeout: this.timeout,
        maximumAge: Infinity,
      },
    );
  }

  @action
  cancelWatch() {
    this.status = 'PENDING';

    if (!this.hasGeolocationSupport) {
      return;
    }

    this.geolocation.clearWatch?.(this.watchId);
    this.watchId = null;
    this._promise = null;
  }

  willDestroy() {
    this.cancelWatch();
  }
}
