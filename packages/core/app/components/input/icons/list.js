/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { Modal } from 'bootstrap';

import { buildWaiter } from '@ember/test-waiters';
const waiter = buildWaiter('InputIconsListComponent-waiter');

export default class InputIconsListComponent extends Component {
  @service fastboot;
  @tracked selectedCategory = '';
  @tracked selectedSubcategory = '';
  @tracked modalVisible;
  @tracked selection;

  @action
  setupModal(element) {
    this.modal = new Modal(element, { show: false });
    let token;

    element.addEventListener('hide.bs.modal', () => {
      token = waiter.beginAsync();
    });

    element.addEventListener('hidden.bs.modal', () => {
      this.modalVisible = false;
      waiter.endAsync(token);
    });

    element.addEventListener('show.bs.modal', () => {
      token = waiter.beginAsync();
    });

    element.addEventListener('shown.bs.modal', () => {
      waiter.endAsync(token);
    });
  }

  willDestroy() {
    super.willDestroy();

    try {
      this.modal?.dispose();
    } catch (err) {}
  }

  @action
  openModal() {
    this.selection = this.args.value ?? [];
    this.modalVisible = true;

    if (this.modal?._element) {
      this.modal.show();
    }
  }

  @action
  hide() {
    if (this.modal?._element) {
      return this.modal.hide();
    }
  }

  @action
  selectionChanged(value) {
    this.selection = value;
  }

  @action
  orderChanged(value) {
    this.selection = value;
    this.args.onChange?.(value);
  }

  @action
  save() {
    this.args.onChange?.(this.selection);
    if (this.modal?._element) {
      return this.modal.hide();
    }
  }
}
