import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class EditorContentPickerComponent extends Component {
  availableModes = ["all", "paragraph"];

  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? {};
  }

  get mode() {
    return this.value.mode ?? "all";
  }

  get paragraphIndex() {
    return this.value.paragraphIndex ?? 0;
  }

  @action
  change(field, value) {
    if (!this._value) {
      this._value = this.value;
    }

    this._value[field] = value;

    return this.args.onChange(this.value);
  }
}
