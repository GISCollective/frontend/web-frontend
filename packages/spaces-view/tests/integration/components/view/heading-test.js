/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/heading', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Heading />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the data with the selected style when set', async function () {
    this.set('value', {
      data: {
        style: {
          color: 'green',
          classes: ['fw-bold', 'display-2'],
          text: 'some title',
        },
      },
    });

    await render(hbs`<View::Heading @value={{this.value}} />`);

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'some title',
    );
    expect(this.element.querySelector('h1')).to.have.classes(
      'text-with-options fw-bold display-2 text-green',
    );
  });
});
