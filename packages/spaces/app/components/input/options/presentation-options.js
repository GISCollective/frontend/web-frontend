/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { action } from "@ember/object";

export default class InputOptionsPresentationOptionsComponent extends Component {
  elementId = `presentation-options-${guidFor(this)}`;

  get endSlideOptions() {
    return {
      data: this.args.presentation.endSlideOptions,
    };
  }

  @action
  saveToggle(field, value) {
    this.args.presentation.set(field, value);
  }

  @action
  coverChanged(key, value) {
    this.args.presentation.cover = value;
  }

  @action
  changeEndSlideOptions(value) {
    this.args.presentation.endSlideOptions = value;
  }
}
