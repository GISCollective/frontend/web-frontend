/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputFontStyleComponent extends Component {
  @tracked _classes;
  @tracked previewValue = 'The quick brown fox jumps over the lazy dog';

  get strClasses() {
    return this.classes.join(' ');
  }

  get classes() {
    if (this._classes) {
      return this._classes;
    }

    return this.args.value ?? [];
  }

  @action
  changeClasses(newClasses) {
    this._classes = newClasses;

    return this.args.onChange(newClasses);
  }
}
