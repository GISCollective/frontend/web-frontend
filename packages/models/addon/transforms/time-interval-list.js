/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { TimeInterval } from './time-interval';
import { A } from '../lib/array';

export default class TimeIntervalListTransform extends Transform {
  deserialize(serialized) {
    return A(serialized?.map((a) => new TimeInterval(a)) ?? []);
  }

  serialize(deserialized) {
    return deserialized?.map((a) => a.toJSON()) ?? [];
  }
}
