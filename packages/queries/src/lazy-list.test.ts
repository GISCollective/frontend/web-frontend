/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { LazyList } from "./lazy-list";
import { addObject } from "./array";
import { StoreType } from "./StoreType";

describe("lazy-list", () => {
  it("initializes the object with an empty bucket list", () => {
    let list = new LazyList("", {}, {});

    expect(list.buckets.length).toEqual(0);
  });

  it("initializes the object with loadedAny = false", () => {
    let list = new LazyList("", {}, {});

    expect(list.loadedAny).toEqual(false);
    expect(list.isEmpty).toEqual(true);
  });

  it("initializes the object with canLoadMore = true", () => {
    let list = new LazyList(null, {}, {});

    expect(list.canLoadMore).toEqual(true);
  });

  it("initializes the object with isLoading = false", () => {
    let list = new LazyList("", {}, {});

    expect(list.isLoading).toEqual(false);
  });

  it("initializes the object with lastError = null", () => {
    let list = new LazyList("", {}, {});

    expect(list.lastError).toEqual(null);
  });

  it("sets is loading to true when the first page is requested", async () => {
    let done;
    let mockStore = {
      query: () =>
        new Promise((d) => {
          done = d;
        }),
    };

    let list = new LazyList("a", {}, mockStore);

    list.loadNext();

    expect(list.isLoading).toEqual(true);

    done();
    await list;
  });

  it("sets the error when the data can't be loaded", async () => {
    let mockStore = {
      query: () => new Promise((done, fail) => fail("some message")),
    };

    let list = new LazyList("a", {}, mockStore);

    await list.loadNext();

    expect(list.isLoading).toEqual(false);
    expect(list.lastError).toEqual("some message");
    expect(list.canLoadMore).toEqual(false);
  });

  it("requests the first page from the store with the right params", async () => {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList("campaign", { team: 1 }, mockStore);

    await list.loadNext();

    expect(params).toEqual([
      "campaign",
      {
        limit: 24,
        skip: 0,
        team: 1,
      },
    ]);
  });

  it("does not request the records when there is an error", async () => {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList("campaign", { team: 1 }, mockStore);
    list.lastError = "error";
    await list.loadNext();

    expect(params).toBeFalsy();
  });

  it("adds the first page result to the bucket list", async () => {
    let mockStore = {
      query: function () {
        return [{ id: 1 }, { id: 2 }];
      },
    };

    let list = new LazyList("campaign", { team: 1 }, mockStore);

    await list.loadNext();

    expect(list.buckets).toHaveLength(1);
    expect(list.buckets[0]).toEqual([{ id: 1 }, { id: 2 }]);
    expect(list.items).toEqual([{ id: 1 }, { id: 2 }]);
    expect(list.loadedAny).toEqual(true);
    expect(list.isEmpty).toEqual(false);
  });

  it("adds the first two pages results to the bucket list", async () => {
    let mockStore = {
      query: function () {
        return [{ id: 1 }, { id: 2 }];
      },
    };

    let list = new LazyList("campaign", { team: 1 }, mockStore);
    list.itemsPerPage = 2;

    await list.loadNext();
    await list.loadNext();

    expect(list.buckets).toHaveLength(2);
    expect(list.buckets[0]).toEqual([{ id: 1 }, { id: 2 }]);
    expect(list.buckets[1]).toEqual([{ id: 1 }, { id: 2 }]);
    expect(list.items).toEqual([{ id: 1 }, { id: 2 }, { id: 1 }, { id: 2 }]);
  });

  it("counts the loaded records and use the value as skip param", async () => {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList("campaign", { team: 1 }, mockStore);
    let objects1 = [];
    let objects2 = [];

    for (let i = 0; i < 24; i++) {
      objects1.push({ id: i });
      objects2.push({ id: i });
    }

    addObject(list.buckets, objects1);
    addObject(list.buckets, objects2);

    await list.loadNext();

    expect(params).toEqual([
      "campaign",
      {
        limit: 24,
        skip: 48,
        team: 1,
      },
    ]);
  });

  it("counts the undeleted records and use the value as skip param", async () => {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    } as unknown as StoreType;

    let list = new LazyList("campaign", { team: 1 }, mockStore);
    let objects = [];

    for (let i = 0; i < 24; i++) {
      objects.push({ id: i, isDeleted: i % 2 });
    }

    addObject(list.buckets, objects);

    await list.loadNext();

    expect(params).toEqual([
      "campaign",
      {
        limit: 24,
        skip: 12,
        team: 1,
      },
    ]);
  });

  it("can not load more data when the last bucket has less items than the limit", async () => {
    let params;
    let mockStore = {
      query: function () {
        params = [...arguments];

        return new Promise((done, fail) => done([]));
      },
    };

    let list = new LazyList("campaign", { team: 1 }, mockStore);
    let objects1 = [];

    for (let i = 0; i < 24; i++) {
      objects1.push({ id: i });
    }

    addObject(list.buckets, objects1);
    addObject(list.buckets, [{ id: "last" }]);

    await list.loadNext();

    expect(list.canLoadMore).toEqual(false);
    expect(params).toBeFalsy();
  });

  describe("limitTo", () => {
    it("returns a new LazyList with 4 items when there are 24 items", async () => {
      let list = new LazyList("campaign", { team: 1 }, {});
      let objects1 = [];

      for (let i = 0; i < 24; i++) {
        objects1.push({ id: i });
      }

      addObject(list.buckets, objects1);
      addObject(list.buckets, [{ id: "last" }]);

      const newList = list.limitTo(4);

      expect(newList.canLoadMore).toEqual(false);
      expect(newList.items).toEqual([{ id: 0 }, { id: 1 }, { id: 2 }, { id: 3 }]);
    });
  });

  describe("when the query has a limit", () => {
    it("initializes the object with canLoadMore = true", () => {
      let list = new LazyList("a", { limit: 10 }, {});

      expect(list.canLoadMore).toEqual(true);
    });

    it("requests the first page from the store with the right params", async () => {
      let params;
      let mockStore = {
        query: function () {
          params = [...arguments];

          return new Promise((done, fail) => done([]));
        },
      };

      let list = new LazyList("campaign", { limit: 10, team: 1 }, mockStore);

      await list.loadNext();

      expect(params).toEqual([
        "campaign",
        {
          limit: 10,
          skip: 0,
          team: 1,
        },
      ]);

      expect(list.canLoadMore).toEqual(false);
    });
  });
});
