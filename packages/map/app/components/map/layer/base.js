/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class MapLayerBaseComponent extends Component {
  @service layer;
  @tracked currentLayers = [];
  @tracked mapBack;

  hasChanges(newLayers) {
    if (this.currentLayers.length != newLayers.length) {
      return true;
    }

    let result = false;

    this.currentLayers.forEach((layer, index) => {
      if (layer.__id === undefined || layer.__id != newLayers[index].__id) {
        result = true;
      }
    });

    return result;
  }

  get layers() {
    const olLayers = this.args.value?.get
      ? this.args.value?.get('layers')
      : this.args.value?.layers;

    return (
      olLayers?.filter((a) => a.type != 'VectorTile' && a.type != 'MapBox') ??
      []
    );
  }

  get vectorTileStyle() {
    const olLayers = this.args.value?.get
      ? this.args.value?.get('layers')
      : this.args.value?.layers;

    const layer = olLayers?.filter((a) => a.type == 'VectorTile');

    if (!layer?.length) {
      return null;
    }

    return layer[0].options?.style;
  }

  get mapBoxLayer() {
    const olLayers = this.args.value?.get
      ? this.args.value?.get('layers')
      : this.args.value?.layers;

    const layer = olLayers?.filter((a) => a.type == 'MapBox');

    if (!layer?.length) {
      return null;
    }

    return layer[0];
  }

  removeCurrentLayers() {
    for (let layer of this.currentLayers) {
      this.args.map.removeLayer(layer);
    }

    this.currentLayers = [];
  }

  @action
  setup() {
    later(this, () => {
      this.update();
    });
  }

  @action
  update() {
    const hasBackgroundLayer = this.vectorTileStyle || this.mapBoxLayer;

    if (hasBackgroundLayer || !this.args.map || !this.layers?.length) {
      return later(() => {
        this.removeCurrentLayers();
      });
    }

    const result = this.layers
      .slice()
      .reverse()
      .map((layer) => this.layer.toOl(layer, this.args.cacheId, this.args.map));

    if (!this.hasChanges(result)) {
      return;
    }

    this.removeCurrentLayers();

    const mapLayers = this.args.map.getLayers();

    for (let layer of result) {
      mapLayers.insertAt(0, layer);
    }

    this.currentLayers = result;

    this.args.map.setLayers(mapLayers);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (!this.layers?.length || !this.args.map) {
      return;
    }

    for (let layer of this.currentLayers) {
      this.args.map.removeLayer(layer);
    }
  }
}
