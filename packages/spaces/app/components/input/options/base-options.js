/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { guidFor } from "@ember/object/internals";
import { setDefaultBsProperty, sizedBsProperty, setSizedBsProperty } from "core/lib/bs-cols";

export default class InputBaseOptionsComponent extends Component {
  elementId = `options-section-${guidFor(this)}`;

  get size() {
    return this.args.size ?? this.args.deviceSize;
  }

  get type() {
    return this.size ?? "type";
  }

  get options() {
    return this.args.options ?? [];
  }

  getProperty(property) {
    return sizedBsProperty(property, this.size, this.options);
  }

  @action
  changeProperty(property, value) {
    const newOptions = this.size
      ? setSizedBsProperty(property, this.size, value, this.options)
      : setDefaultBsProperty(property, value, this.options);

    this.args.onChangeOptions?.(newOptions);
  }
}
