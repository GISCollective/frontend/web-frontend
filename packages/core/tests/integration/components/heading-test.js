/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from '../../helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | heading', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the level is not set', async function () {
    await render(hbs`<Heading />`);
    expect(this.element.textContent.trim()).to.equal('');

    await render(hbs`
      <Heading>
        template block text
      </Heading>
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a level1 heading', async function () {
    await render(hbs`
      <Heading @level="1">
        text
      </Heading>
    `);

    expect(this.element.querySelector('h1').textContent.trim()).to.equal('text');
    expect(this.element.querySelector('h2')).not.to.exist;
    expect(this.element.querySelector('h3')).not.to.exist;
    expect(this.element.querySelector('h4')).not.to.exist;
    expect(this.element.querySelector('h5')).not.to.exist;
    expect(this.element.querySelector('h6')).not.to.exist;
  });

  it('renders a level2 heading', async function () {
    await render(hbs`
      <Heading @level="2">
        text
      </Heading>
    `);

    expect(this.element.querySelector('h2').textContent.trim()).to.equal('text');
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h3')).not.to.exist;
    expect(this.element.querySelector('h4')).not.to.exist;
    expect(this.element.querySelector('h5')).not.to.exist;
    expect(this.element.querySelector('h6')).not.to.exist;
  });

  it('renders a level3 heading', async function () {
    await render(hbs`
      <Heading @level="3">
        text
      </Heading>
    `);

    expect(this.element.querySelector('h3').textContent.trim()).to.equal('text');
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;
    expect(this.element.querySelector('h4')).not.to.exist;
    expect(this.element.querySelector('h5')).not.to.exist;
    expect(this.element.querySelector('h6')).not.to.exist;
  });

  it('renders a level4 heading', async function () {
    await render(hbs`
      <Heading @level="4">
        text
      </Heading>
    `);

    expect(this.element.querySelector('h4').textContent.trim()).to.equal('text');
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;
    expect(this.element.querySelector('h3')).not.to.exist;
    expect(this.element.querySelector('h5')).not.to.exist;
    expect(this.element.querySelector('h6')).not.to.exist;
  });

  it('renders a level5 heading', async function () {
    await render(hbs`
      <Heading @level="5">
        text
      </Heading>
    `);

    expect(this.element.querySelector('h5').textContent.trim()).to.equal('text');
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;
    expect(this.element.querySelector('h3')).not.to.exist;
    expect(this.element.querySelector('h4')).not.to.exist;
    expect(this.element.querySelector('h6')).not.to.exist;
  });

  it('renders a level6 heading', async function () {
    await render(hbs`
      <Heading @level="6">
        text
      </Heading>
    `);

    expect(this.element.querySelector('h6').textContent.trim()).to.equal('text');
    expect(this.element.querySelector('h1')).not.to.exist;
    expect(this.element.querySelector('h2')).not.to.exist;
    expect(this.element.querySelector('h3')).not.to.exist;
    expect(this.element.querySelector('h4')).not.to.exist;
    expect(this.element.querySelector('h5')).not.to.exist;
  });
});
