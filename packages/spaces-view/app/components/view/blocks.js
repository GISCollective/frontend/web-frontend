/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { toContentBlocks } from 'models/lib/content-blocks';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { PageCol } from 'models/transforms/page-col-list';
import { service } from '@ember/service';

export default class BlocksComponent extends Component {
  @tracked model;
  @service fastboot;
  @service store;
  @service space;

  get styleOptions() {
    return this.args.value?.data ?? {};
  }

  get isEmpty() {
    if (!this.args.isEditor) {
      return false;
    }

    const blocks = this.content?.blocks ?? [];

    return !blocks.find(
      (a) => typeof a.data?.text != 'string' || a.data.text.trim() != '',
    );
  }

  get cls() {
    let result = this.args.value?.data?.container?.classes ?? [];

    if (this.args.value?.data?.container?.color) {
      result.push(`bg-${this.args.value?.data?.container?.color}`);
    }

    if (this.args.value?.data?.container?.color) {
      result.push(
        `container-align-${this.args.value?.data?.container?.verticalAlign}`,
      );
    }

    return result.join(' ');
  }

  get content() {
    if (Array.isArray(this.args.value?.data?.content?.blocks)) {
      return {
        blocks: this.args.value?.data?.content?.blocks,
      };
    }

    if (Array.isArray(this.args.value?.data?.blocks)) {
      return {
        blocks: this.args.value?.data?.blocks,
      };
    }

    if (Array.isArray(this.args.value?.blocks)) {
      return {
        blocks: this.args.value?.blocks,
      };
    }

    if (typeof this.args.value == 'string') {
      return toContentBlocks(this.args.value);
    }

    return this.args.value?.data?.content;
  }

  get value() {
    let result = {};
    if (this.args.value?.data && typeof this.args.value.data == 'object') {
      result = { ...this.args.value.data };
    }

    result.content = this.content;

    return result;
  }

  get isProportionType() {
    return this.args.value?.data?.container?.verticalSize === 'proportional';
  }

  get proportion() {
    const pieces = (this.args.value?.data?.container?.proportion ?? '')
      .split(':')
      .map((a) => parseInt(a));

    if (pieces.length != 2) {
      return 1;
    }

    return `${pieces[0]}/${pieces[1]}`;
  }

  get aspectRationStyle() {
    if (!this.isProportionType) {
      return null;
    }

    return htmlSafe(`aspect-ratio: ${this.proportion};`);
  }

  @action
  async setup() {
    const model = {};

    const blocks = this.value?.blocks?.filter((a) => a?.data?.source) ?? [];

    for (const block of blocks) {
      const col = new PageCol(block);
      model[col.modelKey] = await fetch(
        model,
        col,
        this.args.space,
        this.store,
      );
      block.modelKey = col.modelKey;
    }

    this.model = model;
  }
}
