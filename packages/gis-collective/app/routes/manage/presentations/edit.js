/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import ManageEditRoute from '../../base/manage-edit-route';
import { fetch } from 'models/lib/fetch-source';

export default class ManagePresentationsEditRoute extends ManageEditRoute {
  async model(params) {
    const presentation = await this.store.findRecord('presentation', params.id);

    const modelData = {
      presentation,
    };

    try {
      for (const col of presentation.cols) {
        const promise = await fetch(modelData, col, {}, this.store);
        const key = col.modelKey;

        if (promise && key) {
          modelData[key] = promise;
        }
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }

    return super.model(params, modelData);
  }
}
