/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | browse/site/cover',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Browse::Site::Cover />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the feature blurhash when there is no picture', async function () {
    this.set('feature', {
      hashBg: 'hashBg',
    });

    await render(hbs`<Browse::Site::Cover @feature={{this.feature}} />`);
    expect(this.element.querySelector('.cover-hash')).to.have.attribute(
      'style',
      'hashBg'
    );
  });

  it('renders the first picture blurhash when there is a picture', async function () {
    this.set('feature', {
      hashBg: 'featureBg',
      cover: {
        picture: 'some-picture',
        hashBg: 'picture-hash',
      },
    });

    await render(hbs`<Browse::Site::Cover @feature={{this.feature}} />`);
    expect(this.element.querySelector('.cover-hash')).to.have.attribute(
      'style',
      'picture-hash'
    );
    expect(this.element.querySelector('.cover-image')).to.have.attribute(
      'style',
      "background-image: url('some-picture/md')"
    );
  });
});
