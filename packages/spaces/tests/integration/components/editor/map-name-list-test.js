/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, click } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/map-list", function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let map1;
  let map2;

  hooks.beforeEach(function () {
    server = new TestServer();

    map1 = server.testData.create.map("1");
    map2 = server.testData.create.map("2");

    server.testData.storage.addMap(map1);
    server.testData.storage.addMap(map2);

    server.testData.storage.addDefaultPicture();
  });

  it("renders the values", async function () {
    this.set("value", [map1, map2]);

    await render(hbs`<Editor::MapNameList @value={{this.value}} @list={{this.value}}/>`);

    expect(this.element.querySelectorAll(".form-select").length).to.equal(2);
  });

  it("can change the values", async function () {
    this.set("value", []);
    this.set("list", [map1, map2]);

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::MapNameList @value={{this.value}} @list={{this.list}} @onChange={{this.change}}/>`);

    await click(".btn-add-item");

    expect(value.length).to.equal(1);
    expect(value[0].id).to.deep.equal(map2._id);
  });
});
