/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | view/selector',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the col does not match', async function () {
    this.set('cols', [
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'title-with-buttons',
        data: {
          title: { text: 'title', options: ['display-md-5'], heading: 1 },
          paragraph: { text: 'paragraph' },
          buttons: [],
        },
      },
    ]);

    this.set('container', 1);
    this.set('col', 1);
    this.set('row', 1);

    await render(hbs`
      <View::Selector
        @container={{this.container}}
        @col={{this.col}}
        @row={{this.row}}
        @cols={{this.cols}}
        as |col|>
        {{col.type}}
      </View::Selector>
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the col when the indexes match', async function () {
    this.set('cols', [
      {
        container: 0,
        col: 0,
        row: 0,
        name: '0.0.0',
        type: 'title-with-buttons',
        data: {
          title: { text: 'title', options: ['display-md-5'], heading: 1 },
          paragraph: { text: 'paragraph' },
          buttons: [],
        },
      },
    ]);

    this.set('layoutCol', { name: '0.0.0' });

    await render(hbs`
      <View::Selector
        @layoutCol={{this.layoutCol}}
        @cols={{this.cols}}
        as |col|>
        {{col.type}}
      </View::Selector>
    `);

    expect(this.element.textContent.trim()).to.equal('title-with-buttons');
  });
});
