/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AdminSmtpController extends Controller {
  @service notifications;
  @service user;

  @tracked isSaving;
  @tracked isError;

  @tracked mailType = ['smtp', 'mailersend.com'];
  @tracked SMTPAuthType = ['plain', 'none', 'login', 'cramMd5'];
  @tracked SMTPConnectionType = ['plain', 'startTLS', 'tls'];

  get isSMTP() {
    return this.model['mail_type'].value == 'smtp';
  }

  get isMailerSend() {
    return this.model['mail_type'].value == 'mailersend.com';
  }

  get isDirty() {
    const keys = Object.keys(this.model);

    return keys.filter((a) => this.model[a].hasDirtyAttributes).length > 0;
  }

  get isDisabled() {
    return !this.isDirty || this.isSaving;
  }

  @action
  sendTestMessage() {
    return this.model.user.testNotification();
  }

  @action
  save() {
    this.isSaving = true;

    return Promise.all(
      Object.keys(this.model)
        .filter((a) => this.model[a].hasDirtyAttributes)
        .map((a) => this.model[a].save())
    )
      .then(() => {
        this.isSaving = false;
      })
      .catch((err) => {
        this.isSaving = false;
        this.isError = true;

        this.notifications.handleError(err);
      });
  }
}
