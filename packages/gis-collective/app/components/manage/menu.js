/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageMenuComponent extends Component {
  @service space;
  @service loading;
  @service user;
  @service session;

  @tracked logo;

  @action
  async setup() {
    this.logo = await this.space.currentSpace?.logo;
  }

  @action
  async signOutClick() {
    this.session.invalidate();
    this.store.unloadAll();
  }

  get size() {
    if(this.logo?.picture?.indexOf(".jpg") > 0) {
      return ".sm.jpg";
    }

    return "/sm";
  }

  get allowAdd() {
    return true;
  }

  get allowManage() {
    if (!this.session.isAuthenticated) {
      return false;
    }

    if (this.user.teams?.length || this.user?.isAdmin) {
      return true;
    }

    return false;
  }
}
