/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class ManageLanguagesIndexController extends Controller {
  @action
  delete(item) {
    return item.destroyRecord();
  }
}
