/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | editor/date', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the value', async function () {
    this.set('value', '2021-03-05');

    await render(hbs`<Editor::Date @value={{this.value}} />`);

    expect(this.element.querySelector('.datepicker-input').value).to.equal('2021-03-05');
  });

  it('can change the current value', async function () {
    this.set('value', '2021-03-05');
    let value;
    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Editor::Date @value={{this.value}} @onChange={{this.onChange}}/>`);

    await click('.datepicker-input');
    await click('.datepicker-cell.day');

    expect(value).to.equal('2021-02-28');
  });
});
