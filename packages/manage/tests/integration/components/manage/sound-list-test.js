/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, triggerEvent, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/sound-list', function (hooks) {
  let server;
  setupRenderingTest(hooks);

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('can add a new sound while in edit mode', async function () {
    this.set('value', []);

    let value;
    this.set('feature', { id: 1 });

    this.set('save', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::SoundList @feature={{this.feature}} @value={{this.value}} @onSave={{this.save}} @title="sounds" @editablePanel="sounds" />`,
    );
    waitFor('source');

    const blob = server.testData.create.mp3Blob();

    await triggerEvent("[type='file']", 'change', {
      files: [blob],
    });

    await click('.btn-submit');

    expect(value).to.exist;
    expect(value).to.have.length(1);
    expect(value[0].sound).to.startWith('data:audio/mpeg;base64,DcOBwrHDlcK0woHCicOJwoXCucKRw6TCgcK');
  });

  it('shows all sounds when not in edit mode', async function () {
    this.set('value', [{ sound: 'link1' }, { sound: 'link2' }]);

    await render(
      hbs`<Manage::SoundList @value={{this.value}} @onSave={{this.save}} @title="other" @editablePanel="sounds" />`,
    );

    waitFor('source');

    const sounds = this.element.querySelectorAll('source');

    expect(sounds).to.have.length(8);
    expect(sounds[0]).to.have.attribute('src', 'link1/weba-192');
    expect(sounds[1]).to.have.attribute('src', 'link1/ogg-192');
    expect(sounds[2]).to.have.attribute('src', 'link1/aac-192');
    expect(sounds[3]).to.have.attribute('src', 'link1/mp3-192');
    expect(sounds[4]).to.have.attribute('src', 'link2/weba-192');
    expect(sounds[5]).to.have.attribute('src', 'link2/ogg-192');
    expect(sounds[6]).to.have.attribute('src', 'link2/aac-192');
    expect(sounds[7]).to.have.attribute('src', 'link2/mp3-192');
  });
});
