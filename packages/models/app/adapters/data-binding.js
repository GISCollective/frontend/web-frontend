/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Adapter from 'models/adapters/application';

export default class DataBindingAdapter extends Adapter {
  handleAnalyze(dataBindingId) {
    const url = this.buildURL('data-binding', dataBindingId) + '/analyze';
    return this.ajax(url, 'GET');
  }

  handleTrigger(dataBindingId) {
    const url = this.buildURL('data-binding', dataBindingId) + '/trigger';
    return this.ajax(url, 'GET');
  }
}
