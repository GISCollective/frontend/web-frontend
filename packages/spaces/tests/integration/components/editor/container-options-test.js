/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from "qunit";
import { setupRenderingTest, it } from "dummy/tests/helpers";
import { fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { expect } from "chai";

module("Integration | Component | editor/container-options", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the anchor", async function () {
    this.set("value", {
      anchor: "some-anchor",
    });

    await render(hbs`<Editor::ContainerOptions @value={{this.value}} />`);

    expect(this.element.querySelector(".text-anchor").value).to.equal("some-anchor");
  });

  it("renders the height", async function () {
    this.set("value", {
      height: "fill",
    });

    await render(hbs`<Editor::ContainerOptions @value={{this.value}} />`);

    expect(this.element.querySelector(".height-select").value).to.equal("fill");
  });

  it("can change the anchor", async function () {
    this.set("value", {
      anchor: "some-anchor",
    });

    let value;
    this.set("change", function (v) {
      value = v;
    });

    await render(hbs`<Editor::ContainerOptions @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn(".text-anchor", "new-anchor");

    expect(this.element.querySelector(".text-anchor").value).to.equal("new-anchor");
    expect(value.toJSON().anchor).to.equal("new-anchor");
  });

  it("can change the height", async function () {
    this.set("value", {
      height: "fill",
    });

    let value;
    this.set("change", function (v) {
      value = v;
    });

    await render(hbs`<Editor::ContainerOptions @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector(".height-select").value = "default";
    await triggerEvent(".height-select", "change");

    expect(value.toJSON().height).to.equal("default");
  });
});
