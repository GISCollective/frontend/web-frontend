/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render, click, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | map/controls',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the location button when onLocalize is set', async function () {

    let localizeCalled = false;

    this.set('localize', () => {
      localizeCalled = true;
    });

    await render(hbs`
    <Map::Plain as |map|>
      <Map::Controls @map={{map}} @onLocalize={{this.localize}} />
    </Map::Plain>`);

    await waitFor('.btn-localize');

    await click(this.element.querySelector('.btn-localize'));

    expect(localizeCalled).to.equal(true);

    this.set('localize', false);
    expect(this.element.querySelector('.btn-localize')).not.to.exist;
  });

  it('renders the error location button when onLocalize throws an error', async function () {

    this.set('localize', () => {
      throw new Error('asd');
    });

    await render(hbs`
    <Map::Plain as |map|>
      <Map::Controls @map={{map}} @onLocalize={{this.localize}} />
    </Map::Plain>`);

    await waitFor('.btn-localize');

    expect(this.element.querySelector('.btn-localize.btn-danger')).not.to.exist;
    await click(this.element.querySelector('.btn-localize'));
    expect(this.element.querySelector('.btn-localize.btn-danger')).to.exist;
  });

  it('removes the error state when the localization is a success', async function () {

    let i = 0;

    this.set('localize', () => {
      if (i == 0) {
        i++;
        throw new Error('asd');
      }
    });

    await render(hbs`
    <Map::Plain as |map|>
      <Map::Controls @map={{map}} @onLocalize={{this.localize}} />
    </Map::Plain>`);

    await waitFor('.btn-localize');

    await click(this.element.querySelector('.btn-localize'));
    expect(this.element.querySelector('.btn-localize.btn-danger')).to.exist;

    await click(this.element.querySelector('.btn-localize'));
    expect(this.element.querySelector('.btn-localize.btn-danger')).not.to.exist;
  });
});
