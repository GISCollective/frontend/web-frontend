/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export { toModelSlug, toPagePath } from 'models/lib/model-slug';

export function matchSlug(variableSlug, slug) {
  const variablePieces = variableSlug.split('--');
  const pieces = slug.split('--');

  if (variablePieces.length != pieces.length) {
    return false;
  }

  const matches = variablePieces.map((a, index) => a.includes(':') || a == pieces[index]).filter((a) => !a);

  return matches.length == 0;
}

export function toPageSlug(path) {
  if (!path?.indexOf) {
    return '';
  }

  const queryIndex = path.indexOf('?');

  if (queryIndex != -1) {
    path = path.substr(0, queryIndex);
  }

  return path
    .split('/')
    .filter((a) => a != '')
    .join('--');
}

export function toPageId(path, space) {
  if (!space?.pagesMap) {
    return;
  }

  const slug = toPageSlug(path);

  if (slug.indexOf(':') != -1) {
    return space.pagesMap[slug];
  }

  if (space.pagesMap[slug]) {
    return space.pagesMap[slug];
  }

  const variableSlugs = Object.keys(space.pagesMap).filter((a) => a.includes(':'));

  for (const variableSlug of variableSlugs) {
    if (matchSlug(variableSlug, slug)) {
      return space.pagesMap[variableSlug];
    }
  }
}

export async function fillVariable(path, variableName, store) {
  let model = variableName.replace('-id', '').replace(':', '');
  let record;

  try {
    const recordList = await store.query(model, {
      limit: 1,
    });
    record = recordList[0];
  } catch (err) {
    return;
  }

  if (record) {
    return path.replace(`:${variableName}-id`, record.id).replace(variableName, record.id);
  }
}

export function getVariablesFromPath(path) {
  return path.match(/(:[a-z-]*-id)/gm) ?? [];
}

export function toValidHash(value) {
  if (typeof value != 'string') {
    return '';
  }

  return value.replace(/[^A-Za-z0-9]/g, '-');
}
