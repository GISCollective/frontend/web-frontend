/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, belongsTo } from '@ember-data/model';
import Inflector from 'ember-inflector';

Inflector.inflector.irregular('meta', 'metas');

export default class SearchMetaModel extends Model {
  @attr('visibility') visibility;
  @attr('string') title;
  @attr('string') description;

  @attr('string') relatedId;
  @attr('string') relatedModel;

  @attr keywords;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr feature;

  @attr('date') lastChangeOn;
}
