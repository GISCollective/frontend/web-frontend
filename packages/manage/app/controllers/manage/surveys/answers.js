/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageCampaignsAnswersController extends Controller {
  @service intl;
  @service router;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.model.team.get('name'),
        capitalize: true,
      },
      {
        text: this.model.campaign.name,
      },
      {
        text: this.intl.t('answers'),
        capitalize: true,
      },
    ];
  }

  @action
  editAnswer(answer) {
    this.router.transitionTo('manage.survey-answers.edit', answer.id);
  }
}
