/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | attributes/value component', function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIcon('1');
    server.testData.storage.addDefaultIcon('2');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value and format', async function () {
    await render(hbs`<Attributes::Value/>`);

    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector('.image-list')).not.to.exist;
    expect(this.element.querySelector('.input-geo-json')).not.to.exist;
  });

  it('renders a geo json input when the format is geo json', async function () {
    await render(hbs`<Attributes::Value @format="geo json"/>`);

    expect(this.element.querySelector('.input-geo-json')).to.exist;
  });

  it('passes the geojson options when set', async function (a) {
    this.set('options', {
      allowAddress: false,
      allowExistingFeature: false,
      allowGps: true,
      allowManual: true,
    });

    await render(hbs`<Attributes::Value @format="geo json" @options={{this.options}}/>`);

    const buttons = [...this.element.querySelectorAll('.btn-outline-secondary')].map((a) => a.textContent.trim());

    a.deepEqual(buttons, ['use my current location', 'choose manually on the map']);
  });

  it('renders a pictures input when the format is pictures', async function () {
    await render(hbs`<Attributes::Value @format="pictures"/>`);

    expect(this.element.querySelector('.image-list')).to.exist;
  });

  it('renders an icons input when the format is icons', async function () {
    this.set('options', ['1', '2']);

    await render(hbs`<Attributes::Value @format="icons" @options={{this.options}}/>`);

    expect(this.element.querySelector('.input-icons-list-toggle')).to.exist;
    expect(this.element.querySelectorAll('.icon-element')).to.have.length(2);
  });

  it('renders a sounds input when the format is sounds', async function () {
    await render(hbs`<Attributes::Value @format="sounds" />`);

    expect(this.element.querySelector('.sound-list')).to.exist;
  });
});
