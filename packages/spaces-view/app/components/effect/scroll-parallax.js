/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class EffectScrollParallaxComponent extends Component {
  @tracked scrollChanges;

  getScrollParent(node) {
    if (node == null || node.tagName == 'BODY') {
      return window;
    }

    if (node.scrollHeight > node.clientHeight) {
      return node;
    } else {
      return this.getScrollParent(node.parentNode);
    }
  }

  @action
  updateParallax() {
    const bottom = this.element.getBoundingClientRect().bottom;
    const h = this.element.getBoundingClientRect().height;

    const diff = Math.max(h - bottom, 0);

    this.layers.forEach((element, index) => {
      element.style.transform =
        'translate3d(0, ' + diff / (index + 1) + 'px, 0)';
    });

    this.content.forEach(function (element) {
      const rect = element.getBoundingClientRect();

      const opacityDiff = Math.max(bottom - rect.top, 0);

      element.style.transform = `translate3d(0, ${diff * 1.2}px, 0)`;
      element.style.opacity = opacityDiff / h;
    });
  }

  @action
  setupLayers(element) {
    this.element = element.querySelector('.layers-container') || element;
    this.layers = [...this.element.childNodes].filter((a) =>
      a.classList?.contains?.('layer'),
    );
    this.content = [...this.element.childNodes].filter(
      (a) => a.tagName == 'DIV' && !a.classList?.contains?.('layer'),
    );

    this.updateParallax();

    this.scrollHandler = () => {
      this.scrollChanges++;
    };

    this.scrollElement = this.getScrollParent(element.parentNode);
    this.scrollElement.addEventListener('scroll', this.scrollHandler);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.scrollElement?.removeEventListener('scroll', this.scrollHandler);
  }
}
