/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { GeoJson, GeoJsonGps } from '../../lib/geoJson';
import { PositionDetailsGps } from '../../lib/positionDetails';

export default class AttributesGroupPositionDetailsComponent extends Component {
  @service intl;

  @tracked _position;
  @tracked _details;
  @tracked showDetails;

  typeOptionsWithValues = {
    gps: 'use my current location',
    manual: 'choose manually',
    address: 'by address',
  };

  get typeOptions() {
    return Object.keys(this.typeOptionsWithValues)
      .map((a) => this.intl.t(this.typeOptionsWithValues[a]))
      .join(',');
  }

  get group() {
    return {
      key: 'position details',
      name: this.intl.t('position-details'),
    };
  }

  get details() {
    if (this._details) {
      return this._details;
    }
    return this.args.details ?? {};
  }

  get position() {
    if (this._position) {
      return this._position;
    }

    return this.args.position ?? { type: 'Point', coordinates: [0, 0] };
  }

  get type() {
    return this.typeOptionsWithValues[this.details['type']];
  }

  get translatedType() {
    if (!this.type) {
      return '';
    }

    return this.intl.t(this.type);
  }

  get hasReadOnlyFields() {
    return this.type != this.typeOptionsWithValues['manual'];
  }

  get longitude() {
    return this.position.coordinates[0] ?? 0;
  }

  get latitude() {
    return this.position.coordinates[1] ?? 0;
  }

  get altitude() {
    return this.details.altitude ?? 0;
  }

  get accuracy() {
    return this.details.accuracy ?? 0;
  }

  get altitudeAccuracy() {
    return this.details.altitudeAccuracy ?? 0;
  }

  get showPositionFields() {
    return [this.typeOptionsWithValues['gps'], this.typeOptionsWithValues['manual']].indexOf(this.type) != -1;
  }

  get showAddressFields() {
    return this.type == this.typeOptionsWithValues['address'];
  }

  get searchTerm() {
    return this.details.searchTerm;
  }

  get selectedAddress() {
    if (!this.args.searchResults.length || !this.details.address) {
      return '';
    }

    return this.args.searchResults.map((a) => a.name).find((a) => a === this.details.address);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    clearTimeout(this.timer);
  }

  @action
  toggleDetails() {
    this.showDetails = !this.showDetails;
  }

  @action
  setup() {
    this._details = null;
    this._position = null;
  }

  @action
  changePosition(key, value) {
    let position = this.position;

    if (key == 'longitude') {
      position.coordinates[0] = parseFloat(value);
    }
    if (key == 'latitude') {
      position.coordinates[1] = parseFloat(value);
    }

    return this.args.onChange?.(position, this.details);
  }

  @action
  changeDetails(key, value) {
    const details = this.details;
    details[key] = parseFloat(value);

    return this.args.onChange?.(this.position, details);
  }

  @action
  changeSearchTerm(key, value) {
    const details = this.details;
    details[key] = value;

    return this.args.onChange?.(this.position, details);
  }

  @action
  changeAddress(key, value) {
    const details = this.details;
    details[key] = value['name'];

    const geometry = new GeoJson(value['geometry']);

    return this.args.onChange?.(geometry.center, details);
  }

  @action
  changeType(_, typeLabel) {
    let type = '';

    Object.keys(this.typeOptionsWithValues).forEach((key) => {
      if (this.intl.t(this.typeOptionsWithValues[key]) == typeLabel) {
        type = key;
      }
    });

    if (type == 'gps') {
      const service = this.args.positionService ?? {};

      return this.args.onChange?.(new GeoJsonGps(service), new PositionDetailsGps(service));
    }

    return this.args.onChange?.(this.position, {
      ...this.details,
      type,
    });
  }
}
