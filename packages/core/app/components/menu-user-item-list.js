/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './menu-item-list';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { SpaceMenuChildItem } from '../transforms/space-menu';
import { later } from '@ember/runloop';

export default class MenuUserItemListComponent extends Component {
  @service session;
  @service user;
  @service fastboot;
  @service intl;
  @service router;
  @service store;

  get userMenu() {
    const result = [];

    result.push(
      new SpaceMenuChildItem({
        name: this.intl.t('dashboard'),
        link: {
          route: 'manage.dashboards.index',
        },
      })
    );

    if (this.user.id) {
      result.push(
        new SpaceMenuChildItem({
          name: this.intl.t('profile'),
          link: {
            route: 'browse.profiles.profile',
            model: this.user.id,
          },
        })
      );
    }

    result.push(
      new SpaceMenuChildItem({
        name: this.intl.t('preferences'),
        link: {
          route: 'preferences.profile',
        },
      })
    );

    if (this.user.isAdmin) {
      result.push(
        new SpaceMenuChildItem({
          name: this.intl.t('settings'),
          link: {
            route: 'admin.appearance',
          },
        })
      );
    }

    return result;
  }

  @action
  async handleSignOut() {
    await this.session.invalidate();
    this.store.unloadAll();

    later(() => {
      this.router.transitionTo('login.index');
    });
  }
}
