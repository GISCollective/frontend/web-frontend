/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | search-result-item', function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let icon;
  let article;
  let spaceService;
  let space;

  hooks.beforeEach(async function () {
    spaceService = this.owner.lookup('service:space');
    icon = this.server.testData.storage.addDefaultIcon();
    article = this.server.testData.storage.addDefaultArticle();
    let spaceData = this.server.testData.storage.addDefaultSpace();

    space = await this.store.findRecord("space", spaceData._id);
    this.set("space", space);
  });

  it('renders the title and the description', async function () {
    this.set('value', {
      title: 'title',
      description: 'description',
    });

    await render(hbs`<SearchResultItem @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector('h3').textContent.trim()).to.equal(
      'title'
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'description'
    );
    expect(this.element.querySelector('.cover')).not.to.exist;
  });

  it('escapes the title and the description', async function () {
    this.set('value', {
      title: '<br>Statement of Purpose&nbsp;',
      description:
        '<a href="https://www.greenmap.org/stories/energy-green-map-nyc/159">evolving process</a>',
    });

    await render(hbs`<SearchResultItem @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector('h3').textContent.trim()).to.equal(
      'Statement of Purpose'
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'evolving process'
    );
    expect(this.element.querySelector('.cover')).not.to.exist;
  });

  it('renders the cover when exists', async function () {
    this.set('value', {
      title: 'title',
      description: 'description',
      cover: {
        picture: 'picture-url',
      },
    });

    await render(hbs`<div class="search-container"><SearchResultItem @space={{this.space}} @value={{this.value}} /></div>`);

    expect(this.element.querySelector('.cover')).to.exist;
    expect(this.element.querySelector('.picture-bg')).to.have.attribute(
      'style',
      'background-image: url(picture-url/sm);aspect-ratio: 1;'
    );
  });

  it('renders the icon when the meta belongs to an icon', async function () {
    this.set('value', {
      title: 'title',
      description: 'description',
      relatedId: icon._id,
      relatedModel: 'Icon',
    });

    await render(hbs`<SearchResultItem @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector('.cover')).not.to.exist;
    expect(this.element.querySelector('.cover-icon')).to.exist;
  });

  it('renders an icon link when the space has an icon page', async function () {
    space.getLinkFor = () => '/link/id';

    this.set('value', {
      title: 'title',
      description: 'description',
      relatedId: icon._id,
      relatedModel: 'Icon',
    });

    await render(hbs`<SearchResultItem @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector('h3 a')).to.have.attribute(
      'href',
      '/link/id'
    );
    expect(this.element.querySelector('.btn-view-feature-article')).not.to
      .exist;
    expect(this.element.querySelector('.btn-view-feature-map')).not.to.exist;
  });

  it('renders an article link with the matching category link', async function () {
    space.getLinkFor = (model, record) => `/link/${model}/${record.id}`;

    this.set('value', {
      title: 'title',
      description: 'description',
      relatedId: article._id,
      relatedModel: 'Article',
    });

    await render(hbs`<SearchResultItem @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector('h3 a')).to.have.attribute(
      'href',
      '/link/Article/5ca78e2160780601008f69e6'
    );
  });

  it('renders a map link when the result is a Feature', async function () {
    const spaceService = this.owner.lookup('service:space');
    spaceService.currentSpace = space;
    space.getLinkFor = (model, record) => `/link/${model}/${record.id}`;

    this.set('value', {
      title: 'title',
      description: 'description',
      relatedId: article._id,
      relatedModel: 'Feature',
      feature: {
        maps: [{id: "map-id"}]
      }
    });

    await render(hbs`<SearchResultItem @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector('.btn-view-feature-map')).to.have.attribute(
      'href',
      '/link/Map/map-id?s=fid@5ca78e2160780601008f69e6@z@15'
    );
    expect(this.element.querySelector('.btn-view-feature-map')).to.have.textContent(
      "View on map"
    );
  });

  it('does not render a view on map button when there is a legacy map link', async function () {
    const spaceService = this.owner.lookup('service:space');
    spaceService.currentSpace = space;
    space.getLinkFor = (model, record) => `/browse/sites?map=${record.id}`;

    this.set('value', {
      title: 'title',
      description: 'description',
      relatedId: article._id,
      relatedModel: 'Feature',
      feature: {
        maps: [{id: "map-id"}]
      }
    });

    await render(hbs`<SearchResultItem @space={{this.space}} @value={{this.value}} />`);

    expect(this.element.querySelector('.btn-view-feature-map')).not.to.exist;
  });
});
