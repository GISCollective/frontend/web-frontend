/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { get, action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ViewSourceAttributeComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value?.data?.sourceAttribute ?? {};
  }

  get defaultAttributeValue() {
    if (this.args.isEditor) {
      return 'no value';
    }

    return null;
  }

  get hasValue() {
    if (!this.args.record || !this.value.field) {
      return false;
    }

    return true;
  }

  get attributeValue() {
    if (!this.hasValue) {
      return this.defaultAttributeValue;
    }

    return get(this.args.record, 'attributes.' + this.value.field);
  }

  get hasBlocks() {
    return Array.isArray(this.attributeValue?.blocks);
  }

  get isBool() {
    return typeof this.attributeValue == 'boolean';
  }

  @action
  updateProps(event) {
    this._value = event.detail.value;
  }
}
