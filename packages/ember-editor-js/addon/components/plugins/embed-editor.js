/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Embed from '@editorjs/embed';

export default class EmbedEditor extends Embed {
  constructor() {
    super(...arguments);
  }

  rendered() {
    if (!this.element?.parentElement?.parentElement) {
      return;
    }

    this.element.parentElement.parentElement.classList.add('is-embed');
  }
}
