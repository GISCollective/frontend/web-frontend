/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class AppRoute extends Route {
  @service session;
  @service fastboot;
  @service router;
  @service preferences;
  @service headData;
  @service user;
  @service store;
  @service space;
  @service lastError;
  @service('loading') loadingService;

  get isFastBoot() {
    return this.fastboot.isFastBoot;
  }

  get isErrorPage() {
    if (this.isFastBoot) {
      return false;
    }

    if (this.router.location.concreteImplementation && this.router.location.concreteImplementation.location.pathname == '/error') {
      return true;
    }

    return false;
  }

  get isPrivate() {
    return this.preferences.isPrivate;
  }

  fixDomain() {
    if(this.fastboot.isFastBoot) {
      const newUrl = `${this.fastboot.request.protocol}//${this.space.currentSpace.domain}${this.fastboot.request.path}`;

      this.fastboot.response.headers.set('location', newUrl);
      this.fastboot.response.statusCode = 302;
      return;
    }

    window.location.hostname = this.space.currentSpace.domain;
  }

  checkServiceRedirect(transition) {
    if(this.space.windowDomain == "localhost") {
      return;
    }

    if(this.router.location?.path?.indexOf("/login") == 0 || transition?.intent?.url == '/error' || this.session.isAuthenticated) {
      return;
    }

    if(!this.space.isValidSpace && !this.isFastBoot && this.space.windowDomain != "localhost") {
      this.lastError.message = 'unknown space';
      return this.router.transitionTo('/error');
    }

    if(!this.space.summary?.isPublic && transition?.intent?.url && !this.session.isAuthenticated) {
      this.router.transitionTo('login.index', {
        queryParams: {
          redirect: transition?.intent?.url,
        },
      });
    }

    if (this.preferences.isPrivate) {
      this.router.transitionTo('login.index');
    }
  }

  @action
  loading(transition) {
    this.loadingService.isLoading = true;
    this.loadingService.isFirst = transition.from == null;

    transition.promise.finally(() => {
      this.loadingService.isLoading = false;
      this.loadingService.isFirst = false;
    });

    return true;
  }

  async beforeModel(transition) {
    return this.isErrorPage || this.checkServiceRedirect(transition);
  }
}
