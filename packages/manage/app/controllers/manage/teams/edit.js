/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';
import { A } from 'core/lib/array';

export default class ManageTeamsAddController extends Controller {
  @service notifications;
  @service intl;
  @service router;
  @service store;
  @service user;
  @service fastboot;

  roleList = ['owners', 'leaders', 'members', 'guests'];
  @tracked newRole = 'guests';
  @tracked editableField = '';
  @tracked savingField = '';
  @tracked selectedRole = 'guests';
  @tracked emailList = [];
  @tracked members = [];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.name,
        model: this.model.id,
        query: {},
        capitalize: true,
      },
      { text: this.intl.t('edit'), capitalize: true },
    ];
  }

  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    this.model.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      },
    );
  }

  removeMemberFrom(listName, item) {
    const list = this.model.get(listName);

    list.setObjects(list.filter((a) => a.email != item.email));
  }

  removeMember(item) {
    this.roleList.forEach((role) => {
      this.removeMemberFrom(role, item);
    });
  }

  performUserSearch(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject('Term is too short');
    }

    this.store
      .query('user', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  async setupMembers() {
    let result = [];

    for(let role of this.roleList) {
      const items = (await this.model.get(role)).map((user) => ({ user, role }));

      result = [...result, ...items];
    }

    this.members = A(result);
  }

  @action
  async removePendingMember(member) {
    await this.notifications
      .ask({
        title: this.intl.t('remove pending member'),
        question: this.intl.t('remove-pending-member-message', { email: member.email }),
      });

    this.model.invitations = A(this.model.invitations.filter(a => a.email != member.email));

    return this.model.save();
  }

  @action
  updateSelectedRole(value) {
    this.selectedRole = value;
  }

  @action
  changeEmailList(newValue) {
    this.emailList = newValue;
  }

  @action
  async inviteUsers() {
    await this.model.invite(this.emailList, this.selectedRole);
    await this.model.reload();
  }

  @action
  searchUsers(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performUserSearch, term, resolve, reject, 600);
    });
  }

  @action
  membersChanged(value) {
    this.model.members = value;
  }

  @action
  async remove(item) {
    await this.notifications
      .ask({
        title: this.intl.t('remove member'),
        question: this.intl.t('remove-member-message', { name: item.name }),
      });

    this.removeMember(item);
    this.save();
  }

  @action
  updateRole(item, role) {
    this.removeMember(item);
    this.model.get(role).push(item);
    return this.save();
  }

  @action
  selectNewRole(event) {
    const value = event.target.value;
    this.newRole = value;
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: {
        link: {
          id: this.model.id,
          model: 'team',
        },
      },
    });
  }

  @action
  togglePublished() {
    this.model.set('isPublic', !this.model.isPublic);
  }

  @action
  saveIsPublic(newValue) {
    this.model.set('isPublic', newValue);
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveAllowCustomDataBindings(newValue) {
    this.model.set('allowCustomDataBindings', newValue);
    this.editableField = this.intl.t('allow custom data bindings');

    return this.save();
  }

  @action
  saveAllowNewsletters(newValue) {
    this.model.set('allowNewsletters', newValue);
    this.editableField = this.intl.t('can send newsletters');

    return this.save();
  }

  @action
  saveAllowEvents(newValue) {
    this.model.set('allowEvents', newValue);
    this.editableField = this.intl.t('can create events');

    return this.save();
  }

  @action
  saveAllowReports(newValue) {
    this.model.set('allowReports', newValue);
    this.editableField = this.intl.t('can view reports');

    return this.save();
  }

  @action
  saveField(field, value) {
    this.model.set(field, value);

    return this.save();
  }

  @action
  requestProfile(id) {
    return this.store.findRecord('user-profile', id);
  }

  @action
  async updatePictures(pictures) {
    const promises = pictures
      .filter((a) => a.isNew)
      .map((picture) =>
        picture.save({
          adapterOptions: {
            progress: (percentage) => {
              this.progress?.(picture, percentage);
            },
          },
        }),
      );

    this.model.pictures = pictures;

    await Promise.all(promises);

    return this.model.save();
  }

  @action
  imageChange(picture) {
    if (picture.isNew) return;

    return picture.save();
  }

  @action
  saveLogo(value) {
    this.model.set('logo', value);
    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.rollbackAttributes();
  }

  @action
  saveArticle(name, about) {
    this.model.set('name', name);
    this.model.set('about', about);

    return this.save();
  }

  @action
  delete() {
    return this.model
      .destroyRecord()
      .then(() => {
        return this.router.transitionTo('manage.teams.index');
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
