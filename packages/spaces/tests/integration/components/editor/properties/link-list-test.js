/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render, waitFor } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { move } from "core/lib/trigger";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | editor/properties/link-list", function (hooks) {
  setupRenderingTest(hooks);
  let changedValue;
  let server;
  let space;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage("000000000000000000000002");

    space = server.testData.storage.addDefaultSpace();
    space.getPagesMap = () => {
      return {
        page1: "000000000000000000000001",
        page2: "000000000000000000000002",
        page3: "000000000000000000000003",
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("space", space);
    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders a link when is set", async function (a) {
    this.set("value", [
      {
        name: "name",
        link: {
          pageId: "000000000000000000000001",
        },
      },
    ]);

    await render(hbs`<Editor::Properties::LinkList @value={{this.value}} @space={{this.space}} />`);

    await waitFor(".link-name");
    expect(this.element.querySelector(".link-name").value).to.equal("name");

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("page1");
  });

  it("can add a new link", async function () {
    this.set("value", [
      {
        name: "name",
        link: {
          pageId: "000000000000000000000001",
        },
      },
    ]);

    await render(
      hbs`<Editor::Properties::LinkList @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`,
    );

    await click(".btn-add-link-item");

    const inputNames = this.element.querySelectorAll(".link-name");
    const selectLinks = this.element.querySelectorAll(".ember-basic-dropdown-trigger");

    await fillIn(inputNames[1], "new name");

    await selectSearch(selectLinks[1], "page2");
    await selectChoose(selectLinks[1], "page2");

    expect(changedValue).to.deep.equal([
      { name: "name", link: { pageId: "000000000000000000000001" }, newTab: false },
      { name: "new name", link: { pageId: "000000000000000000000002" }, newTab: false },
    ]);
  });

  it("can delete a link", async function () {
    this.set("value", [
      { name: "name", link: { pageId: "000000000000000000000001" } },
      { name: "new name", link: { pageId: "000000000000000000000002" } },
    ]);

    await render(
      hbs`<Editor::Properties::LinkList @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`,
    );

    const deleteButtons = this.element.querySelectorAll(".btn-delete");
    await click(deleteButtons[0]);

    expect(changedValue).to.deep.equal([
      { name: "new name", link: { pageId: "000000000000000000000002" }, newTab: false },
    ]);
  });

  it("can change the order of the links", async function () {
    this.set("value", [
      { name: "name", link: { pageId: "000000000000000000000001" }, newTab: false },
      { name: "new name", link: { pageId: "000000000000000000000002" }, newTab: false },
    ]);

    await render(
      hbs`<Editor::Properties::LinkList @value={{this.value}} @space={{this.space}} @onChange={{this.onChange}} />`,
    );

    const list = document.querySelector(".properties-link-list-sortable");
    await move(list, 0, list, 1, false, ".handler-link-item");

    expect(changedValue).to.deep.equal([
      { name: "new name", link: { pageId: "000000000000000000000002" }, newTab: false },
      { name: "name", link: { pageId: "000000000000000000000001" }, newTab: false },
    ]);
  });
});
