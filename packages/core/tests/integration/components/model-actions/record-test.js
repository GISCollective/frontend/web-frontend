/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | model-actions/record', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let spaceData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIconSet();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    const space = this.owner.lookup('service:space');

    spaceData = server.testData.storage.addDefaultSpace();

    const currentSpace = await this.store.findRecord('space', spaceData._id);
    space.currentSpace = currentSpace;
  });

  it('renders nothing when there is no record', async function () {
    await render(hbs`<ModelActions::Record />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the feature actions when the record is a feature', async function () {
    const featureData = server.testData.storage.addDefaultFeature();
    featureData.canEdit = true;

    const feature = await store.findRecord('feature', featureData._id);

    this.set('feature', feature);

    await render(hbs`<ModelActions::Record @record={{this.feature}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Issues', 'Edit']);
  });

  it('renders the map actions when the record is a map', async function () {
    const mapData = server.testData.storage.addDefaultMap();
    mapData.canEdit = true;

    const map = await store.findRecord('map', mapData._id);

    this.set('map', map);

    await render(hbs`<ModelActions::Record @record={{this.map}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['View features', 'Edit the map', 'Map files', 'Map sheet']);
  });

  it('renders the map view page when the space has a dedicated page for it', async function () {
    const space = this.owner.lookup('service:space');
    space.getLinkFor = (a, b) => `https://${a}/${b.id}`;

    const mapData = server.testData.storage.addDefaultMap();
    mapData.canEdit = true;

    const map = await store.findRecord('map', mapData._id);

    this.set('map', map);

    await render(hbs`<ModelActions::Record @record={{this.map}} />`);

    expect(this.element.querySelector('.btn-view')).to.exist;
    expect(this.element.querySelector('.btn-view').textContent.trim()).to.equal('View');
    expect(this.element.querySelector('.btn-view')).to.have.attribute('href', 'https://map/5ca89e37ef1f7e010007f54c');
  });

  it('renders the campaign actions when the record is a campaign', async function () {
    const campaignData = server.testData.storage.addDefaultCampaign();
    campaignData.canEdit = true;

    const campaign = await store.findRecord('campaign', campaignData._id);

    this.set('campaign', campaign);

    await render(hbs`<ModelActions::Record @record={{this.campaign}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });

  it('renders the icon actions when the record is an icon', async function () {
    const iconData = server.testData.storage.addDefaultIcon();
    iconData.canEdit = true;

    const icon = await store.findRecord('icon', iconData._id);

    this.set('icon', icon);

    await render(hbs`<ModelActions::Record @record={{this.icon}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });

  it('renders the iconSet actions when the record is an iconSet', async function () {
    const iconSetData = server.testData.storage.addDefaultIconSet();
    iconSetData.canEdit = true;

    const iconSet = await store.findRecord('iconSet', iconSetData._id);

    this.set('iconSet', iconSet);

    await render(hbs`<ModelActions::Record @record={{this.iconSet}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });

  it('renders the article actions when the record is an article', async function () {
    const articleData = server.testData.storage.addDefaultArticle();
    articleData.canEdit = true;

    const article = await store.findRecord('article', articleData._id);

    this.set('article', article);

    await render(hbs`<ModelActions::Record @record={{this.article}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });

  it('renders the baseMap actions when the record is an baseMap', async function () {
    const baseMapData = server.testData.storage.addDefaultBaseMap();
    baseMapData.canEdit = true;

    const baseMap = await store.findRecord('base-map', baseMapData._id);

    this.set('baseMap', baseMap);

    await render(hbs`<ModelActions::Record @record={{this.baseMap}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });

  it('renders the team actions when the record is an team', async function () {
    const teamData = server.testData.storage.addDefaultTeam();
    teamData.canEdit = true;

    const team = await store.findRecord('team', teamData._id);

    this.set('team', team);

    await render(hbs`<ModelActions::Record @record={{this.team}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });

  it('renders the dataBinding actions when the record is an dataBinding', async function () {
    const dataBindingData = server.testData.storage.addDefaultDataBinding();
    dataBindingData.canEdit = true;

    const dataBinding = await store.findRecord('dataBinding', dataBindingData._id);

    this.set('dataBinding', dataBinding);

    await render(hbs`<ModelActions::Record @record={{this.dataBinding}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit', 'Analyze', 'Trigger']);
  });

  it('renders the page actions when the record is an page', async function () {
    const pageData = server.testData.storage.addDefaultPage();
    pageData.canEdit = true;

    const page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    await render(hbs`<ModelActions::Record @record={{this.page}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });

  it('renders the space actions when the record is an space', async function () {
    const spaceData = server.testData.storage.addDefaultSpace();
    spaceData.canEdit = true;

    const space = await store.findRecord('space', spaceData._id);

    this.set('space', space);

    await render(hbs`<ModelActions::Record @record={{this.space}} />`);

    const labels = [...this.element.querySelectorAll('.dropdown-item')].map((element) => element.textContent.trim());

    expect(labels).to.deep.equal(['Edit']);
  });
});
