/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class SearchService extends Service {
  @tracked _isEnabled;
  @tracked map;

  get isEnabled() {
    return this._isEnabled;
  }

  set isEnabled(value) {
    this._isEnabled = value;
    this.map = undefined;
  }

  @action
  close() {
    this.isEnabled = false;
  }
}
