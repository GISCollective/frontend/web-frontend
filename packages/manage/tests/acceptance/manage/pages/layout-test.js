/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, doubleClick, click, waitUntil, triggerEvent, fillIn } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe('Acceptance | manage/pages/layout', function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let page;
  let receivedPage;
  let restoredPage;
  let space;
  let receivedSpace;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
    server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');
    space = server.testData.storage.addDefaultSpace();

    page = server.testData.storage.addDefaultPage();

    receivedPage = null;
    server.put(`/mock-server/pages/${page._id}`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = page._id;

      server.testData.storage.addPage(receivedPage.page);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPage)];
    });

    server.post(`/mock-server/pages`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = 'new-id';

      server.testData.storage.addPage(receivedPage.page);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPage)];
    });

    receivedSpace = null;
    server.put(`/mock-server/spaces/${space._id}`, (request) => {
      receivedSpace = JSON.parse(request.requestBody);
      receivedSpace.space._id = space._id;

      server.testData.storage.addSpace(receivedSpace.space);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedSpace)];
    });

    restoredPage = {
      page: {
        ...page,
        name: 'restored test',
        layoutContainers: [
          {
            options: [],
            rows: [
              {
                options: [],
                cols: [
                  { type: 'type', data: {}, options: [] },
                  { type: 'type', data: {}, options: [] },
                  { type: 'type', data: {}, options: [] },
                ],
              },
            ],
          },
        ],
        cols: [
          {
            container: 0,
            col: 0,
            row: 0,
            type: 'title-with-buttons',
            data: {
              title: { text: 'restored title', heading: 1 },
              paragraph: { text: 'restored description' },
              buttons: [],
            },
          },
          {
            container: 0,
            col: 1,
            row: 0,
            type: 'picture',
            data: { id: '5cc8dc1038e882010061545a' },
          },
          {
            row: -1,
            type: 'background/color',
            col: -1,
            container: 0,
            data: {
              background: {
                color: 'green-100',
              },
            },
          },
        ],
      },
    };

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [
            {
              time: '2022-05-05T00:53:50Z',
              url: '/mock-server/pages/000000000000000000000001/revisions/627304009d2d9b8820ec2f52',
              id: '627304009d2d9b8820ec2f52',
            },
            {
              time: '2022-05-05T00:53:51Z',
              url: '/mock-server/pages/000000000000000000000001/revisions/627304009d2d9b8820ec2f53',
              id: '627304009d2d9b8820ec2f53',
            },
            {
              time: '2022-05-05T00:53:52Z',
              url: '/mock-server/pages/000000000000000000000001/revisions/627304009d2d9b8820ec2f54',
              id: '627304009d2d9b8820ec2f54',
            },
          ],
        }),
      ];
    });

    server.get(`/mock-server/pages/:id/revisions/:revisionId`, () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(restoredPage)];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/pages/${page._id}/layout`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fpages%2F000000000000000000000001%2Flayout');
  });

  it('can navigate to the page content page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-show-content');

    expect(currentURL()).to.equal('/manage/pages/000000000000000000000001/content');
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/pages/${page._id}/content`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'GISCollective', 'test']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca88c99ecd8490100caba50',
      '/manage/dashboards/space/6227c131624b2cf1626dd029',
    ]);
  });

  it('shows the col editor on col click', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);
    const cols = this.element.querySelectorAll('.btn-col-options');

    await doubleClick(cols[1]);

    expect(this.element.querySelector('.page-editors-properties .input-col-options')).to.exist;
  });

  it('can change and save the selected col name', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);
    const cols = this.element.querySelectorAll('.btn-col-options');

    await click('.row');
    await doubleClick(cols[1]);

    await fillIn('.input-col-name', 'some col name');

    const colNames = [...this.element.querySelectorAll('.bs-col-name')].map((a) => a.textContent.trim());

    expect(colNames).to.deep.equal(['0.0.0 (0.0.0)', 'some col name (0.0.1)']);

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.layoutContainers).to.deep.equal([
      {
        rows: [
          {
            name: '0.0',
            data: {},
            cols: [
              {
                type: 'type',
                data: {},
                name: '0.0.0',
                componentCount: 1,
                options: [],
              },
              {
                type: 'type',
                data: {},
                name: 'some col name',
                componentCount: 1,
                options: [],
              },
            ],
            options: [],
          },
        ],
        options: [],
        visibility: 'always',
        layers: { count: 1, effect: 'none', showContentAfter: 1 },
      },
    ]);
  });

  it('shows the row editor when the row settings button is pressed', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-edit-row');

    expect(this.element.querySelector('.page-editors-properties .input-row-options')).to.exist;
  });

  it('can change the row options', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-edit-row');

    this.element.querySelector('.input-sm-gy').value = '3';
    await triggerEvent('.input-sm-gy', 'change');

    expect(this.element.querySelector('.row')).to.have.attribute('class', 'row gy-3 row-selectable');
  });

  it('shows the container editor when the container settings button is pressed', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    expect(this.element.querySelector('.page-editors-properties .input-container-options')).to.exist;
  });

  it('can change the container options', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    expect(this.element.querySelector('.input-layout .container')).to.exist;
    expect(this.element.querySelector('.input-layout .container-fluid')).not.to.exist;

    this.element.querySelector('.width-select').value = 'fluid';
    await triggerEvent('.width-select', 'change');

    this.element.querySelector('.height-select').value = 'fill';
    await triggerEvent('.height-select', 'change');

    await click('.btn-save');
    await waitUntil(() => receivedPage);

    expect(receivedPage.page.layoutContainers[0].height).to.deep.equal('fill');
  });

  it('can save a layout', async function (a) {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    this.element.querySelector('.width-select').value = 'fluid';
    await triggerEvent('.width-select', 'change');

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    a.deepEqual(receivedPage.page.layoutContainers, [
      {
        rows: [
          {
            name: '0.0',
            data: {},
            cols: [
              {
                type: 'type',
                data: {},
                name: '0.0.0',
                componentCount: 1,
                options: [],
              },
              {
                type: 'type',
                data: {},
                name: '0.0.1',
                componentCount: 1,
                options: [],
              },
            ],
            options: [],
          },
        ],
        height: 'default',
        options: ['container-fluid'],
        visibility: 'always',
        layers: { count: 1, effect: 'none', showContentAfter: 1 },
      },
    ]);
  });

  it('can create a copy of the layout', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    this.element.querySelector('.width-select').value = 'fluid';
    await triggerEvent('.width-select', 'change');

    await click('.btn-copy-layout');

    await waitUntil(() => receivedPage);

    expect(currentURL()).to.equal('/manage/pages/new-id/layout');

    expect(receivedPage.page._id).to.equal('new-id');
    expect(receivedPage.page.name).to.equal('test - copy');
    expect(receivedPage.page.slug).to.equal('page--test-copy');
    expect(receivedPage.page.visibility).to.deep.equal({
      isPublic: false,
      isDefault: false,
      team: '5ca88c99ecd8490100caba50',
    });
  });

  it('can change the layout settings', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.page-editors-container');

    await fillIn('.type-name', 'new name');

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.name).to.equal('new name');
  });

  describe('when there is a page with two rows', function (hooks) {
    hooks.beforeEach(async function () {
      page.cols.push({
        container: 0,
        col: 0,
        row: 1,
        type: 'article',
        data: { id: '2' },
      });

      page.cols.push({
        container: 0,
        col: 1,
        row: 1,
        type: 'picture',
        data: {
          id: '2',
        },
      });

      page.layoutContainers[0].rows.push({
        options: [],
        cols: [
          {
            type: 'type',
            data: {},
            options: [],
          },
          {
            type: 'type',
            data: {},
            options: [],
          },
        ],
      });

      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);
    });

    it('pushes the rows on the page when a row on the layout is added', async function () {
      await click('.btn-add-row-0');
      await click('.row-selectable');
      await click('.btn-save');

      expect(receivedPage.page.layoutContainers[0].rows).to.have.length(3);
      expect(receivedPage.page.cols[0].row).to.equal(1);
      expect(receivedPage.page.cols[1].row).to.equal(1);
    });

    it('removes the cols on the page when a col on the layout is deleted', async function () {
      await click('.row-selectable');
      await click('.btn-delete-col');
      await click('.btn-save');

      expect(receivedPage.page.layoutContainers).to.have.length(1);
      expect(receivedPage.page.cols).to.have.length(3);
      expect(receivedPage.page.cols[0]).to.deep.contain({
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { source: { id: '5ca78e2160780601008f69e6', model: 'article' } },
      });
    });

    it('removes the rows on the page when a row on the layout is deleted', async function () {
      await click('.row-selectable');
      await click('.btn-delete-col');
      await click('.btn-delete-col');
      await click('.btn-save');

      expect(receivedPage.page.layoutContainers).to.have.length(1);
      expect(receivedPage.page.cols).to.have.length(2);
      expect(receivedPage.page.cols[0]).to.deep.contain({
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: '2' },
      });
      expect(receivedPage.page.cols[1]).to.deep.contain({
        container: 0,
        col: 1,
        row: 0,
        type: 'picture',
        data: { id: '2' },
      });
    });

    it('pushes the containers on the page when a container on the layout is added', async function () {
      await click('.btn-add-container-0');
      await click('.btn-save');

      expect(receivedPage.page.layoutContainers).to.have.length(2);
      expect(receivedPage.page.layoutContainers[0].rows).to.have.length(1);
      expect(receivedPage.page.cols[0].container).to.equal(1);
      expect(receivedPage.page.cols[1].container).to.equal(1);
    });

    it('decrements the container index on the page when a container on the layout is deleted', async function () {
      await click('.btn-remove-container-0');
      await click('.btn-save');

      expect(receivedPage.page.layoutContainers).to.have.length(0);
      expect(receivedPage.page.cols).to.have.length(0);
    });

    it('removes the containers on the page when a container on the layout is deleted', async function () {
      await click('.btn-add-container-0');
      await click('.btn-save');
      await click('.btn-remove-container-0');
      await click('.btn-save');

      expect(receivedPage.page.layoutContainers).to.have.length(1);
      expect(receivedPage.page.layoutContainers[0].rows).to.have.length(2);
      expect(receivedPage.page.cols[0].container).to.equal(0);
      expect(receivedPage.page.cols[1].container).to.equal(0);
    });
  });

  describe('a page with 3 empty cols', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      page.cols = [];
      page.layoutContainers = [
        {
          rows: [
            {
              options: [],
              cols: [
                {
                  type: '',
                  options: [],
                },
              ],
            },
            {
              options: [],
              cols: [
                {
                  type: '',
                  options: [],
                },
                {
                  type: '',
                  options: [],
                },
              ],
            },
          ],
          options: [],
        },
      ];

      await visit(`/manage/pages/${page._id}/layout`);
    });

    it('allows switching between all editors', async function () {
      await click('.btn-config-container-0');
      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('container 0');

      await click('.btn-edit-row');

      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('row 0.0');

      await click('.btn-col-options');
      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('col 0.0.0');

      await click('.page-editors-container');
      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('page');
    });

    it('shows only the layout editor when a page is selected after a column', async function () {
      await click('.btn-col-options');
      await click('.page-editors-container');

      expect(this.element.querySelector('.input-container-col-options')).not.to.exist;
    });

    it('shows only the layout editor when a page is selected after a row', async function () {
      await click('.btn-edit-row');
      await click('.page-editors-container');

      expect(this.element.querySelector('.row-options-sheet')).not.to.exist;
    });

    it('shows only the layout editor when a page is selected after a container', async function () {
      await click('.btn-config-container-0');
      await click('.page-editors-container');

      expect(this.element.querySelector('.container-options-sheet')).not.to.exist;
    });
  });

  describe('a page with 2 cols', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      page.cols = [
        {
          name: '0.0.0',
          type: 'title-with-buttons',
          data: {},
        },
        {
          name: '0.0.1',
          type: 'title-with-buttons',
          data: {},
        },
      ];

      await visit(`/manage/pages/${page._id}/layout`);
    });

    it('assigns the name to both layout and page columns', async function () {
      const cols = this.element.querySelectorAll('.layout-col-editable');

      await click(cols[0]);
      await fillIn('.input-col-name', 'column 1');

      await click(cols[1]);
      await fillIn('.input-col-name', 'column 2');

      await click('.btn-save');
      await waitUntil(() => receivedPage);

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'column 1',
          type: 'title-with-buttons',
          data: {},
        },
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'column 2',
          type: 'title-with-buttons',
          data: {},
        },
      ]);

      expect(receivedPage.page.layoutContainers).to.deep.equal([
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: 'type',
                  data: {},
                  name: 'column 1',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: 'type',
                  data: {},
                  name: 'column 2',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
          options: [],
          visibility: 'always',
        },
      ]);
    });

    it('can change the container paddings without changing the cols', async function (a) {
      await click('.btn-config-container-0');

      this.element.querySelector('.sm-top-padding').value = '1';
      await triggerEvent('.sm-top-padding', 'change');

      this.element.querySelector('.sm-bottom-padding').value = '2';
      await triggerEvent('.sm-bottom-padding', 'change');

      this.element.querySelector('.sm-start-padding').value = '3';
      await triggerEvent('.sm-start-padding', 'change');

      this.element.querySelector('.sm-end-padding').value = '4';
      await triggerEvent('.sm-end-padding', 'change');

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      a.deepEqual(receivedPage.page.layoutContainers, [
        {
          height: 'default',
          rows: [
            {
              name: '0.0',
              cols: [
                {
                  type: 'type',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: 'type',
                  data: {},
                  name: '0.0.1',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
              data: {},
            },
          ],
          options: ['pt-1', 'pb-2', 'ps-3', 'pe-4'],
          visibility: 'always',
          layers: { count: 1, showContentAfter: 1, effect: 'none' },
        },
      ]);

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0.0.0',
          type: 'title-with-buttons',
          data: {},
        },
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0.0.1',
          type: 'title-with-buttons',
          data: {},
        },
      ]);
    });
  });

  describe('the page revisions', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      await visit(`/manage/pages/${page._id}/layout`);
    });

    it('are rendered in the page settings', async function () {
      await click('.page-editors-container');

      expect(this.element.querySelector('.page-editors-properties .page-revisions')).to.exist;
    });

    it('restores an older layout version when a button is pressed', async function () {
      await click('.page-editors-container');
      await click('.page-revisions .revision-item');

      await waitUntil(
        () => [...this.element.querySelectorAll('.page-frame-body .row:not(.row-hidden) .col')].length != 2,
      );

      expect(this.element.querySelectorAll('.page-frame-body .row:not(.row-hidden) .col')).to.have.length(3);
    });

    it('can get the latest layout version after another one was restored', async function () {
      await click('.page-editors-container');
      await click('.page-revisions .revision-item');
      await click('.page-revisions .latest-version');

      await waitUntil(() => {
        return [...this.element.querySelectorAll('.page-frame-body .row:not(.row-hidden) .col')].length != 3;
      });

      expect(this.element.querySelectorAll('.page-frame-body .row:not(.row-hidden) .col')).to.have.length(2);
    });

    it('can save the layout after restoration', async function () {
      await click('.page-editors-container');
      await click('.page-revisions .revision-item');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
    });
  });

  describe('global containers', function (hooks) {
    hooks.beforeEach(function () {
      space.layoutContainers = {
        'main-menu-container': {
          gid: 'main-menu-container',
          layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
          rows: [{ cols: [{ data: {} }] }],
        },
      };
    });

    it('can replace a container with a global container', async function () {
      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);

      await click('.btn-config-container-0');

      await selectSearch('.ember-power-select-trigger', 'main-menu-container');
      await selectChoose('.ember-power-select-trigger', 'main-menu-container');

      const cols = this.element.querySelectorAll('.layout-col-editable-content');
      expect(cols).to.have.length(1);

      expect(this.element.querySelector('.btn-to-global-page-col')).not.to.exist;

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedSpace.space.layoutContainers).to.deep.equal({
        'main-menu-container': {
          rows: [
            {
              name: '',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
          gid: 'main-menu-container',
        },
      });
      expect(receivedPage.page.layoutContainers).to.deep.equal([
        {
          rows: [
            {
              name: '',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
          gid: 'main-menu-container',
        },
      ]);
    });

    it('updates the space when a global container structure is updated', async function () {
      page.layoutContainers = [space.layoutContainers['main-menu-container']];
      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);

      await click('.row-selectable');
      await click('.btn-add-row-1');
      await click('.btn-add-col-1');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedPage.page.layoutContainers).to.deep.equal([
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
                { type: '', data: {}, name: '0.0.1', componentCount: 1, options: [] },
              ],
              options: [],
            },
            {
              name: '0.1',
              data: {},
              cols: [{ type: '', data: {}, name: '0.1.0', componentCount: 1, options: [] }],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
          gid: 'main-menu-container',
        },
      ]);

      expect(receivedSpace.space.layoutContainers['main-menu-container']).to.deep.equal(
        receivedPage.page.layoutContainers[0],
      );
    });

    it('updates the space when a row of a global container is updated', async function () {
      page.layoutContainers = [space.layoutContainers['main-menu-container']];
      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);

      await click('.row-selectable');

      this.element.querySelector('.input-sm-justify-content').value = 'center';
      await triggerEvent('.input-sm-justify-content', 'change');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedPage.page.layoutContainers).to.deep.equal([
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: ['justify-content-center'],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
          gid: 'main-menu-container',
        },
      ]);

      expect(receivedSpace.space.layoutContainers['main-menu-container']).to.deep.equal(
        receivedPage.page.layoutContainers[0],
      );
    });

    it('updates the space when a col of a global container is updated', async function () {
      page.layoutContainers = [space.layoutContainers['main-menu-container']];
      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);

      await click('.layout-col-editable');

      this.element.querySelector('.input-md-order').value = '3';
      await triggerEvent('.input-md-order', 'change');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedPage.page.layoutContainers).to.deep.equal([
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: ['order-md-3'],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
          gid: 'main-menu-container',
        },
      ]);

      expect(receivedSpace.space.layoutContainers['main-menu-container']).to.deep.equal(
        receivedPage.page.layoutContainers[0],
      );
    });

    it('updates the space when properties of a global container is updated', async function (a) {
      page.layoutContainers = [space.layoutContainers['main-menu-container']];
      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);

      await click('.btn-config-container');

      this.element.querySelector('.input-sm-min-vh').value = '60';
      await triggerEvent('.input-sm-min-vh', 'change');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      a.deepEqual(receivedPage.page.layoutContainers, [
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          height: 'default',
          options: ['min-vh-60'],
          visibility: 'always',
          layers: { count: 2, showContentAfter: 2, effect: 'some-effect' },
          gid: 'main-menu-container',
        },
      ]);

      a.deepEqual(receivedSpace.space.layoutContainers['main-menu-container'], receivedPage.page.layoutContainers[0]);
    });

    it('updates the view after the visible container was updated', async function () {
      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);

      await click('.row-selectable');
      await click('.btn-add-row-1');
      await click('.btn-add-col-2');

      await click('.btn-config-container-0');

      await selectSearch('.ember-power-select-trigger', 'main-menu-container');
      await selectChoose('.ember-power-select-trigger', 'main-menu-container');

      const cols = this.element.querySelectorAll('.layout-col-editable-content');
      expect(cols).to.have.length(1);

      expect(this.element.querySelector('.btn-to-global-page-col')).not.to.exist;
    });

    it('convert a container to a global container', async function () {
      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);

      await click('.btn-config-container-0');
      await click('.btn-to-global-page-col');
      await fillIn('.input-global-page-col-id', 'new-global-component');
      await click('.btn-success');

      expect(this.element.querySelector('.btn-to-global-page-col')).not.to.exist;

      expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal(
        'new-global-component',
      );

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedPage.page.layoutContainers).to.deep.equal([
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: 'type',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: 'type',
                  data: {},
                  name: '0.0.1',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
          gid: 'new-global-component',
        },
      ]);

      expect(receivedSpace.space.layoutContainers['new-global-component']).to.deep.equal(
        receivedPage.page.layoutContainers[0],
      );
    });
  });
});
