/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { IconStyle } from '../../transforms/icon-style';

export default class InputIconStyleAllComponent extends Component {
  @tracked tmpValue;

  @action
  onUpdate() {
    this.tmpValue = new IconStyle(this.args.value);
  }

  @action
  updateStyle(key, value) {
    this.tmpValue[key] = value;
    this.raiseChange();
  }

  raiseChange() {
    if (!this.args.onChange) {
      return;
    }

    this.args.onChange(new IconStyle(this.tmpValue));
  }
}
