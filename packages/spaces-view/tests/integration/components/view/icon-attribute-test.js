/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/icon-attribute', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when no value is set', async function () {
    await render(hbs`<View::IconAttribute />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the attribute value when is an object', async function () {
    this.set('value', {
      'icon name': {
        key: 'value',
      },
    });

    this.set('options', {
      key: 'key',
      icon: {
        name: 'icon name',
      },
    });

    await render(
      hbs`<View::IconAttribute @value={{this.value}} @options={{this.options}} />`,
    );
    expect(this.element.textContent.trim()).to.equal('value');
  });

  it('renders nothing when the icon is not set', async function () {
    this.set('value', {
      'icon name': {
        key: 'value',
      },
    });

    this.set('options', {
      key: 'key',
    });

    await render(
      hbs`<View::IconAttribute @value={{this.value}} @options={{this.options}} />`,
    );
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when the key is not found', async function () {
    this.set('value', {
      'icon name': {
        key: 'value',
      },
    });

    this.set('options', {
      key: 'other key',
      icon: {
        name: 'icon name',
      },
    });

    await render(
      hbs`<View::IconAttribute @value={{this.value}} @options={{this.options}} />`,
    );
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the attribute value when the otherNames matches an attribute', async function () {
    this.set('value', {
      'other icon name': {
        key: 'value',
      },
    });

    this.set('options', {
      key: 'key',
      icon: {
        name: 'icon name',
        otherNames: ['other icon name'],
      },
    });

    await render(
      hbs`<View::IconAttribute @value={{this.value}} @options={{this.options}} />`,
    );
    expect(this.element.textContent.trim()).to.equal('value');
  });

  it('renders a list when it matches both the name and the other values', async function () {
    this.set('value', {
      'other icon name': {
        key: 'value 1',
      },
      'icon name': {
        key: 'value 2',
      },
    });

    this.set('options', {
      key: 'key',
      icon: {
        allowMany: true,
        name: 'icon name',
        otherNames: ['other icon name'],
      },
    });

    await render(
      hbs`<View::IconAttribute @value={{this.value}} @options={{this.options}} />`,
    );

    const items = this.element.querySelectorAll('li');

    expect(items).to.have.length(2);
    expect(items[0].textContent.trim()).to.equal('value 1');
    expect(items[1].textContent.trim()).to.equal('value 2');
  });

  it('renders the items when the value is part of an icon with many instances', async function () {
    this.set('value', {
      'icon name': [
        {
          key: 'value 1',
        },
        {
          key: 'value 2',
        },
      ],
    });

    this.set('options', {
      key: 'key',
      icon: {
        allowMany: true,
        name: 'icon name',
        otherNames: ['other icon name'],
      },
    });

    await render(
      hbs`<View::IconAttribute @value={{this.value}} @options={{this.options}} />`,
    );

    const items = this.element.querySelectorAll('li');

    expect(items).to.have.length(2);
    expect(items[0].textContent.trim()).to.equal('value 1');
    expect(items[1].textContent.trim()).to.equal('value 2');
  });
});
