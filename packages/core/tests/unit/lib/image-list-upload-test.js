/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { imageListUpload } from 'dummy/lib/image-list-upload';

class FakePicture {
  constructor(id, failures = 0, isNew = true) {
    this._nextId = id;
    this.saves = 0;
    this.failures = failures;
    this.isNew = isNew;
  }

  save() {
    this.saves++;

    if (this.failures) {
      this.failures--;

      throw new Error('TEST Failure');
    }
    return this._nextId;
  }
}

describe('Unit | Lib | image-list-upload', function (hooks) {
  setupTest(hooks);

  describe('imageListUpload', function () {
    it('sets an empty list when there are no pictures', async function () {
      const result = await imageListUpload({ save: () => {} }, [], 'pictures');

      expect(result.pictures).deep.equal([]);
    });

    it('saves a picture and it adds it to the list', async function () {
      const result = await imageListUpload({ save: () => {} }, [new FakePicture('1')], 'pictures');

      expect(result.pictures).deep.equal(['1']);
    });

    it('retries the picture upload on failure', async function () {
      const result = await imageListUpload({ save: () => {} }, [new FakePicture('1', 1)], 'pictures');

      expect(result.pictures).deep.equal(['1']);
    });

    it('retries 3 times when a picture fails to upload', async function () {
      const picture = new FakePicture('1', 10);
      const result = await imageListUpload({ save: () => {} }, [picture], 'pictures');

      expect(result.pictures).deep.equal([]);
      expect(picture.saves).equal(3);
    });

    it('only adds the successful images', async function () {
      const picture = new FakePicture('1', 10);
      const result = await imageListUpload(
        { save: () => {} },
        [new FakePicture('2'), picture, new FakePicture('3')],
        'pictures',
      );

      expect(result.pictures).deep.equal(['2', '3']);
    });

    it('adds a failing image when it is not new', async function () {
      const picture = new FakePicture('1', 10, false);
      const result = await imageListUpload(
        { save: () => {} },
        [new FakePicture('2'), picture, new FakePicture('3')],
        'pictures',
      );

      expect(result.pictures).deep.equal(['2', picture, '3']);
    });
  });
});
