/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

export default class FiltersSearchComponent extends Component {
  @tracked _term;
  @tracked hasFocus = false;
  @service clickOutside;

  get term() {
    if (this._term == '') {
      return '';
    }

    return this._term || this.args.value || '';
  }

  set term(value) {
    this._term = value;

    if (this.args.onType) {
      debounce(this, this.args.onType, value, 50);
    }
  }

  get showSuggestions() {
    return this.args.hasSuggestions && this.hasFocus;
  }

  @action
  setup(element) {
    this.element = element;
  }

  @action
  search() {
    if (!this.args.onSearch) {
      return;
    }

    if (this.term == this.args.value) {
      return;
    }

    return this.args.onSearch(`${this._term}`);
  }

  @action
  focus() {
    this.hasFocus = true;

    this.clickOutside.subscribe(this.element, () => {
      this.hasFocus = false;
    });
  }

  @action
  blur() {
    if (!this.hasFocus) {
      return;
    }

    this._term = null;
    this.hasFocus = false;
  }

  @action
  keyUp(event) {
    if (event.key == 'Enter' || event.keyCode == 13) {
      this.search();
    }

    if (event.key == 'Escape' || event.keyCode == 27) {
      this.blur();
    }
  }
}
