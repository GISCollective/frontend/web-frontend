/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/app-route';
import { service } from '@ember/service';

export default class ActivateRoute extends Route {
  @service preferences;
  @service fastboot;
  @service router;

  queryParams = {
    email: { refreshModel: true },
    token: { refreshModel: true },
  };

  beforeModel() {
    return this.preferences.getPreference('register.enabled').then((registerEnabled) => {
      const value = registerEnabled.value;

      if (value != 'true' && value !== true) {
        this.router.transitionTo('index');
      }
    });
  }

  model(params) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    if (params.email && params.token) {
      return new Promise((resolve) => {
        this.store
          .adapterFor('user')
          .activate(params.email, params.token)
          .then((activation) => {
            resolve({ activation });
          })
          .catch((error) => {
            resolve({ error });
          });
      });
    }

    return {};
  }
}
