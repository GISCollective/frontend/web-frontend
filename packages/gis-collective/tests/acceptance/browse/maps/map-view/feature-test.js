/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';

describe.skip('Acceptance | browse/maps/map-view/feature', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let feature;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPage('000000000000000000000001');
    server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
    feature = server.testData.storage.addDefaultFeature();

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('shows the panel as a feature', async function (a) {
    await visit(`/browse/maps/_/map-view/${feature._id}`);

    expect(this.element.querySelector('.map-search-bis')).to.have.class('has-feature-content');
  });
});
