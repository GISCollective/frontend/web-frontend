/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export class SpaceSearchOptions {
  static fields = ['features', 'maps', 'campaigns', 'iconSets', 'teams', 'icons', 'geocodings', 'events', 'articles'];

  @tracked features = false;
  @tracked maps = false;
  @tracked campaigns = false;
  @tracked iconSets = false;
  @tracked teams = false;
  @tracked icons = false;
  @tracked geocodings = false;
  @tracked events = false;
  @tracked articles = false;

  constructor(object) {
    if (!object || typeof object != 'object') {
      return;
    }

    for (const field of SpaceSearchOptions.fields) {
      if (object[field]) {
        this[field] = object[field];
      }
    }
  }

  toJSON() {
    return {
      features: this.features ?? false,
      maps: this.maps ?? false,
      campaigns: this.campaigns ?? false,
      iconSets: this.iconSets ?? false,
      teams: this.teams ?? false,
      icons: this.icons ?? false,
      geocodings: this.geocodings ?? false,
      events: this.events ?? false,
      articles: this.articles ?? false,
    };
  }
}

export default class SpaceSearchOptionsTransform extends Transform {
  deserialize(serialized) {
    return new SpaceSearchOptions(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() || deserialized;
  }
}
