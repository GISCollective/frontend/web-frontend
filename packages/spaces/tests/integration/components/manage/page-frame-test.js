/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | manage/page-frame", function (hooks) {
  setupRenderingTest(hooks);

  it("renders the header and body", async function () {
    await render(hbs`<Manage::PageFrame><:header>true</:header><:body>false</:body></Manage::PageFrame>`);

    expect(this.element.querySelector(".page-frame-header").textContent.trim()).to.equal("true");
    expect(this.element.querySelector(".page-frame-body").textContent.trim()).to.equal("false");
  });
});
