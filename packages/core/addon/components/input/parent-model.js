/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class InputParentModelComponent extends Component {
  @service store;
  @tracked list;

  get value() {
    if (this.hasValue) {
      return this.args.value;
    }

    return this.list?.[0];
  }

  get hasOneOption() {
    return this.list?.length == 1;
  }

  get hasManyOptions() {
    return this.list?.length > 1;
  }

  get hasValue() {
    if (this.args.value?.isFulfilled) {
      return !!this.args.value.content;
    }

    return !!this.args.value;
  }

  @action
  async setup() {
    if (!this.args.teams?.length) {
      return;
    }

    const team = this.args.teams.map((a) => (a.id ? a.id : a)).join(',');

    this.list = (
      await this.store.query(this.args.model, {
        team,
      })
    ).slice();

    if (!this.hasValue) {
      this.args.onChange?.(this.value);
    }
  }
}
