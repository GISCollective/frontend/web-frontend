/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import { A } from 'core/lib/array';

module('Integration | Component | chk-record-selection', function (hooks) {
  setupRenderingTest(hooks);
  let articleData;
  let server;

  hooks.before(function () {
    server = new TestServer();
    articleData = server.testData.storage.addDefaultArticle();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders an unselected checkbox when the record can be edited', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.canEdit = true;

    this.set('article', article);

    await render(hbs`<ChkRecordSelection @record={{this.article}} />`);

    expect(this.element.querySelector('.chk-selected').checked).to.equal(false);
  });

  it('renders a selected checkbox when the record can be edited and has the id in the selection list', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.canEdit = true;

    this.set('article', article);
    this.set('selection', [article.id]);

    await render(hbs`<ChkRecordSelection @record={{this.article}} @selection={{this.selection}} />`);

    expect(this.element.querySelector('.chk-selected').checked).to.equal(true);
  });

  it('renders a disabled selected checkbox when the record can be edited and all are selected', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.canEdit = true;

    this.set('article', article);
    this.set('selection', 'all');

    await render(hbs`<ChkRecordSelection @record={{this.article}} @selection={{this.selection}} />`);

    expect(this.element.querySelector('.chk-selected').checked).to.equal(true);
    expect(this.element.querySelector('.chk-selected')).to.have.attribute('disabled', '');
  });

  it('does not render a checkbox when the record is not editable', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.canEdit = false;

    this.set('article', article);

    await render(hbs`<ChkRecordSelection @record={{this.article}} @selection={{this.selection}} />`);

    expect(this.element.querySelector('.chk-selected')).not.to.exist;
  });

  it('triggers an event when the checkbox is selected', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.canEdit = true;

    this.set('article', article);

    let value;
    this.set('select', (v) => {
      value = v;
      this.set('selection', v);
    });

    await render(
      hbs`<ChkRecordSelection @record={{this.article}} @selection={{this.selection}} @onSelect={{this.select}} />`,
    );

    await click('.chk-selected');

    expect(value).to.deep.equal([article.id]);
    expect(this.element.querySelector('.chk-selected').checked).to.equal(true);
  });

  it('triggers an event when the checkbox is deselected', async function () {
    const article = await this.store.findRecord('article', articleData._id);
    article.canEdit = true;

    this.set('article', article);
    this.set('selection', A([article.id]));

    let value;
    this.set('select', (v) => {
      value = v;
      this.set('selection', v);
    });

    await render(
      hbs`<ChkRecordSelection @record={{this.article}} @selection={{this.selection}} @onSelect={{this.select}} />`,
    );

    await click('.chk-selected');

    expect(value).to.deep.equal([]);
    expect(this.element.querySelector('.chk-selected').checked).to.equal(false);
  });
});
