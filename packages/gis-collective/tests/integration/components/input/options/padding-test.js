/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/options/paddings',  function (hooks) {
  setupRenderingTest(hooks);
  let value;

  hooks.beforeEach(async function () {
    value = undefined;
    this.set('change', (v) => {
      value = v;
    });

    this.set('options', ['pt-1', 'pb-2', 'ps-3', 'pe-4']);

    await render(
      hbs`<Input::Options::Paddings @options={{this.options}} @deviceSize="sm" @onChangeOptions={{this.change}}/>`
    );
  });

  it('can set the padding top', async function () {
    expect(this.element.querySelector('.sm-top-padding').value).to.equal('1');

    this.element.querySelector('.sm-top-padding').value = '5';
    await triggerEvent('.sm-top-padding', 'change');

    expect(value).to.deep.equal(['pb-2', 'ps-3', 'pe-4', 'pt-5']);
  });

  it('can set the padding bottom', async function () {
    expect(this.element.querySelector('.sm-bottom-padding').value).to.equal(
      '2'
    );

    this.element.querySelector('.sm-bottom-padding').value = '5';
    await triggerEvent('.sm-bottom-padding', 'change');

    expect(value).to.deep.equal(['pt-1', 'ps-3', 'pe-4', 'pb-5']);
  });

  it('can set the padding start', async function () {
    expect(this.element.querySelector('.sm-start-padding').value).to.equal('3');

    this.element.querySelector('.sm-start-padding').value = '5';
    await triggerEvent('.sm-start-padding', 'change');

    expect(value).to.deep.equal(['pt-1', 'pb-2', 'pe-4', 'ps-5']);
  });

  it('can set the padding end', async function () {
    expect(this.element.querySelector('.sm-end-padding').value).to.equal('4');

    this.element.querySelector('.sm-end-padding').value = '5';
    await triggerEvent('.sm-end-padding', 'change');

    expect(value).to.deep.equal(['pt-1', 'pb-2', 'ps-3', 'pe-5']);
  });
});
