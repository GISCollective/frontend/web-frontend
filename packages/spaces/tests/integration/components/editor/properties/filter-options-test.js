/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/properties/filter-options", function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIconSet("2");
    server.testData.storage.addDefaultIconSet("3");
    server.testData.storage.addDefaultIconSet("3");
    server.testData.storage.addDefaultMap("2");
    server.testData.storage.addDefaultMap("3");
  });

  it("renders nothing when there is no value", async function () {
    await render(hbs`<Editor::Properties::FilterOptions />`);

    expect(this.element.textContent.trim()).to.equal("");
  });

  it("shows the model picker when the filter is an icons filter", async function (a) {
    this.set("value", {
      type: "icons",
      options: {},
    });

    this.set("model", {
      selected_model: {
        iconSetsFilter: [],
      },
      other: {
        iconSetsFilter: [],
      },
      noSets: {},
    });

    await render(hbs`<Editor::Properties::FilterOptions @value={{this.value}} @model={{this.model}} />`);

    const options = [...this.element.querySelectorAll(".form-select.source-model-key option")].map((a) =>
      a.textContent.trim(),
    );

    expect(options).to.deep.equal(["selected_model", "other"]);
  });

  it("renders the selected model key value", async function () {
    this.set("value", {
      type: "icons",
      options: {
        sourceModelKey: "other",
      },
    });

    this.set("model", {
      selected_model: {
        iconSetsFilter: [],
      },
      other: {
        iconSetsFilter: [],
      },
      noSets: {},
    });

    await render(hbs`<Editor::Properties::FilterOptions @value={{this.value}} @model={{this.model}} />`);

    expect(this.element.querySelector(".form-select.source-model-key").value).to.equal("other");
  });

  it("can change the the selected model key value", async function () {
    this.set("value", {
      type: "icons",
      options: {
        sourceModelKey: "other",
      },
    });

    this.set("model", {
      selected_model: {
        iconSetsFilter: [],
      },
      other: {
        iconSetsFilter: [],
      },
      noSets: {},
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::Properties::FilterOptions @onChange={{this.onChange}} @value={{this.value}} @model={{this.model}} />`,
    );

    this.element.querySelector(".form-select.source-model-key").value = "selected_model";
    await triggerEvent(".form-select.source-model-key", "change");

    expect(this.element.querySelector(".form-select.source-model-key").value).to.equal("selected_model");
    expect(value).to.deep.equal({
      sourceModelKey: "selected_model",
      source: "icon-set",
      "icon-set": { id: "2", name: "Green Map® Icons Version 3" },
    });
  });
});
