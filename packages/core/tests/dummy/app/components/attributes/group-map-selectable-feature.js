/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { GeoJson } from 'core/lib/geoJson';
import PositionDetails from 'core/lib/positionDetails';

export default class AttributesGroupMapSelectableFeatureComponent extends Component {
  get position() {
    return JSON.stringify(this.args.position);
  }

  @action
  select() {
    this.args.onChange(
      new GeoJson({ type: 'Point', coordinates: [20, 30] }),
      new PositionDetails({ type: 'feature', id: '1' }),
    );
  }
}
