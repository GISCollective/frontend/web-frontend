/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click, fillIn, waitFor } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import waitUntil from '@ember/test-helpers/wait-until';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe ('Acceptance | manage/presentations/content',  function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let presentation;
  let receivedPresentation;
  let picture;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPage();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
    server.testData.storage.addDefaultArticle('000000000000000000000002');

    presentation = server.testData.storage.addDefaultPresentation();
    picture = server.testData.storage.addDefaultPicture();

    presentation.cols.push({
      col: 1,
      row: 1,
      name: '0.1.1',
      type: 'article',
      data: { id: '5ca78e2160780601008f69e6' },
    });

    presentation.cols.push({
      col: 0,
      row: 1,
      name: '0.1.0',
      type: 'picture',
      data: {
        id: '5cc8dc1038e882010061545a',
      },
    });

    receivedPresentation = null;

    server.put(`/mock-server/presentations/${presentation._id}`, (request) => {
      receivedPresentation = JSON.parse(request.requestBody);
      receivedPresentation.presentation._id = presentation._id;

      server.testData.storage.addPresentation(
        receivedPresentation.presentation
      );

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPresentation),
      ];
    });


    server.put(`/mock-server/pictures/:id`, (request) => {
      const picture = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(picture),
      ];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/presentations/${presentation._id}/content`);

    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fpresentations%2F000000000000000000000001%2Fcontent'
    );
  });

  it('can view the presentation cols', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}/content`);

    expect(
      this.element.querySelector('.presentation-name').textContent.trim()
    ).to.equal('test');

    const slides = this.element.querySelectorAll('.presentation-slide');
    expect(slides).to.have.length(2);

    const cols = this.element.querySelectorAll('.page-col');
    expect(cols).to.have.length(4);

    expect(this.element.querySelector('.slides-welcome-intro')).not.to.exist;

    expect(this.element.querySelector('.page-col-0-0-0')).to.have.attribute(
      'data-type',
      'slot'
    );

    expect(this.element.querySelector('.page-col-0-0-1')).to.have.attribute(
      'data-type',
      'slot'
    );

    expect(this.element.querySelector('.page-col-0-1-0')).to.have.attribute(
      'data-type',
      'slot'
    );

    expect(this.element.querySelector('.page-col-0-1-1')).to.have.attribute(
      'data-type',
      'slot'
    );
  });

  it('does not render the container configuration buttons', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}/content`);

    expect(this.element.querySelector('.btn-config-container')).not.to.exist;
  });

  it('can add a slide at the end', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}/content`);

    expect(this.element.querySelector('.presentation-slide-2')).not.to.exist;

    await click('.btn-add-slide-end');

    expect(this.element.querySelector('.presentation-slide-2')).to.exist;
  });

  it('can add a slide at the front', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}/content`);

    expect(this.element.querySelector('.presentation-slide-2')).not.to.exist;

    await click('.btn-add-slide-0');

    expect(this.element.querySelector('.presentation-slide-2')).to.exist;

    expect(this.element.querySelector('.page-col-0-0-0')).to.have.attribute(
      'data-type',
      'slot'
    );
    expect(this.element.querySelector('.page-col-0-0-1')).to.have.attribute(
      'data-type',
      'slot'
    );
  });

  it('can delete the first slide', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}/content`);

    await click('.presentation-slide-0 .btn-delete');

    expect(this.element.querySelector('.presentation-slide-1')).not.to.exist;
  });

  it('can change the presentation properties', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}/content`);

    await click('.page-editors-container');

    await fillIn('.page-name', 'new name');
    await click('.btn-save');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.name).to.equal('new name');
  });

  it('shows the intro slide when the hasIntro is true', async function () {
    authenticateSession();

    presentation.hasIntro = true;
    presentation.picture = picture._id;

    await visit(`/manage/presentations/${presentation._id}/content`);

    expect(this.element.querySelector('.slides-welcome-intro')).to.exist;
    expect(this.element.querySelector('.slides-welcome-intro img')).to.exist;
    expect(
      this.element.querySelector('.slides-welcome-intro h1').textContent.trim()
    ).to.equal('test');
  });

  it('shows the end slide when the hasEnd is true', async function () {
    authenticateSession();

    presentation.hasEnd = true;
    presentation.endSlideOptions = {
      paragraph: {
        text: 'this is a paragraph',
        options: ['text-center', 'fw-bold'],
        color: 'red',
      },
    };

    await visit(`/manage/presentations/${presentation._id}/content`);

    expect(this.element.querySelector('.presentation-slide-end')).to.exist;
    expect(
      this.element.querySelector('.presentation-slide-end p').textContent.trim()
    ).to.equal('this is a paragraph');
  });
});
