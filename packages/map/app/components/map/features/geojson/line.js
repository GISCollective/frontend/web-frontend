/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './base';
import LineString from 'ol/geom/LineString';

export default class MapFeaturesGeojsonLineStringComponent extends Component {
  getGeometry() {
    const coordinates = this.args.value.coordinates?.slice?.() ?? [];
    const geometry = new LineString(coordinates);
    geometry.transform('EPSG:4326', 'EPSG:3857');

    return geometry;
  }
}
