/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, fillIn, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import PageElements from 'core/test-support/page-elements';

describe('Integration | Component | input/site-attributes-property', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<Input::SiteAttributesProperty />`);
    expect(this.element.textContent.trim()).to.equal('---');
  });

  it('renders the blocks when they are set', async function () {
    this.set('property', {
      value: { blocks: [{ type: 'paragraph', data: { text: 'description' } }] },
    });

    await render(hbs`<Input::SiteAttributesProperty @property={{this.property}}/>`);

    expect(this.element.querySelector('.attribute-value').textContent.trim()).to.equal('description');
  });

  it('renders the display name when it is set', async function () {
    this.set('property', {
      displayName: 'display name',
      value: { blocks: [{ type: 'paragraph', data: { text: 'description' } }] },
    });

    await render(hbs`<Input::SiteAttributesProperty @property={{this.property}}/>`);

    expect(this.element.querySelector('.attribute-display-name').textContent.trim()).to.equal('display name');
  });

  it('renders the name when it is set', async function () {
    this.set('property', {
      name: 'name',
      value: { blocks: [{ type: 'paragraph', data: { text: 'description' } }] },
    });

    await render(hbs`<Input::SiteAttributesProperty @property={{this.property}}/>`);

    expect(this.element.querySelector('.attribute-display-name').textContent.trim()).to.equal('name');
  });

  it('renders the name when the display name has a white space', async function () {
    this.set('property', {
      displayName: '  ',
      name: 'name',
      value: { blocks: [{ type: 'paragraph', data: { text: 'description' } }] },
    });

    await render(hbs`<Input::SiteAttributesProperty @property={{this.property}}/>`);

    expect(this.element.querySelector('.attribute-display-name').textContent.trim()).to.equal('name');
  });

  it('does not render the display name when it is not set', async function () {
    this.set('property', {
      value: { blocks: [{ type: 'paragraph', data: { text: 'description' } }] },
    });

    await render(hbs`<Input::SiteAttributesProperty @property={{this.property}}/>`);

    expect(this.element.querySelector('attribute-display-name')).not.to.exist;
  });

  it('renders --- when there are no blocks', async function () {
    this.set('property', {
      value: { blocks: [] },
    });

    await render(hbs`<Input::SiteAttributesProperty @property={{this.property}}/>`);

    expect(this.element.querySelector('.attribute-value').textContent.trim()).to.equal('---');
  });

  describe('edit mode', function (hooks) {
    it('renders and changes an editor js for a long text value with blocks', async function () {
      this.set('property', {
        name: 'key1',
        value: { blocks: [{ type: 'paragraph', data: { text: 'description' } }] },
      });

      this.set('icon', {
        name: 'test',
        attributes: [{ name: 'key1', type: 'long text' }],
      });

      await render(
        hbs`<Input::SiteAttributesProperty @property={{this.property}} @icon={{this.icon}} @isEdit={{true}}/>`,
      );

      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.ce-paragraph').textContent.trim()).to.equal('description');

      await click('.ce-paragraph');
      await fillIn('.ce-paragraph', ' --- new paragraph');
      await click('.ce-paragraph');

      await waitUntil(() => this.property.value.blocks[0].data.text != 'description');

      expect(this.property).to.deep.equal({
        name: 'key1',
        value: {
          blocks: [{ type: 'paragraph', data: { text: ' --- new paragraph' } }],
        },
      });
    });

    it('renders an editor js for a long text value with string', async function () {
      this.set('property', {
        name: 'key1',
        value: 'description',
      });

      this.set('icon', {
        name: 'test',
        attributes: [{ name: 'key1', type: 'long text' }],
      });

      await render(
        hbs`<Input::SiteAttributesProperty @property={{this.property}} @icon={{this.icon}} @isEdit={{true}}/>`,
      );

      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.ce-paragraph').textContent.trim()).to.equal('description');
    });

    it('renders an editor js for a long text value without a type', async function () {
      this.set('property', {
        name: 'key1',
        isBlocks: true,
        value: {
          blocks: [
            {
              "type": "paragraph",
              "data": {
                "text": "test"
              }
            },
          ]
        },
      });

      await render(
        hbs`<Input::SiteAttributesProperty @property={{this.property}} @icon={{this.icon}} @isEdit={{true}}/>`,
      );

      await PageElements.waitEditorJs(this.element);

      expect(this.element.querySelector('.ce-paragraph').textContent.trim()).to.equal('test');
    });
  });
});
