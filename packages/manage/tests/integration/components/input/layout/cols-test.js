/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { LayoutContainer } from 'models/transforms/layout-container-list';
import { LayoutCol } from 'models/transforms/layout-row-list';

describe('Integration | Component | input/layout/cols', function (hooks) {
  let page;
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    page = {
      layoutContainers: [
        new LayoutContainer({
          rows: [
            {
              cols: [
                new LayoutCol({
                  type: '',
                  data: undefined,
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                }),
              ],
              name: '0.0',
              options: [],
            },
          ],
          options: [],
        }),
      ],
    };
  });

  it('renders nothing when there are no columns', async function () {
    await render(hbs`<Input::Layout::Cols />`);
    expect(this.element.querySelector('.col')).not.to.exist;
  });

  it('renders a column when is set', async function () {
    this.set('page', page);

    await render(hbs`<Input::Layout::Cols @page={{this.page}} @containerIndex={{0}} @rowIndex={{0}} />`);
    expect(this.element.querySelectorAll('.col')).to.have.length(1);

    expect(this.element.querySelector('.col')).to.have.attribute('class', 'col layout-col-editable');

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('-');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('-');
  });

  it('renders the column widths when they are set', async function () {
    page.layoutContainers[0].rows[0].cols = [
      new LayoutCol({
        type: '',
        options: ['col-1', 'col-md-2', 'col-lg-3'],
      }),
    ];

    this.set('page', page);

    await render(hbs`<Input::Layout::Cols @page={{this.page}} @containerIndex={{0}} @rowIndex={{0}} />`);

    expect(this.element.querySelector('.col')).to.have.attribute(
      'class',
      'col layout-col-editable col-1 col-md-2 col-lg-3',
    );

    expect(this.element.querySelector('.bs-col-width-desktop').textContent.trim()).to.equal('3');
    expect(this.element.querySelector('.bs-col-width-tablet').textContent.trim()).to.equal('2');
    expect(this.element.querySelector('.bs-col-width-mobile').textContent.trim()).to.equal('1');
  });

  it('renders two columns when they are set', async function () {
    page.layoutContainers[0].rows[0].cols = [
      { type: '', options: [] },
      { type: '', options: [] },
    ];

    this.set('page', page);

    await render(hbs`<Input::Layout::Cols @page={{this.page}} @containerIndex={{0}} @rowIndex={{0}} />`);
    expect(this.element.querySelectorAll('.col')).to.have.length(2);
  });

  it('adds a col at the end when the last plus is pressed', async function () {
    page.layoutContainers[0].rows[0].cols = [
      { type: '', options: [] },
      { type: '', options: [] },
    ];

    let value;
    page.addLayout = (a, b, c) => {
      value = [a, b, c];
    };

    this.set('page', page);

    await render(hbs`<Input::Layout::Cols @page={{this.page}} @containerIndex={{0}} @rowIndex={{0}} />`);

    expect(this.element.querySelectorAll('.btn-add-col-2')).to.have.length(1);

    await click('.btn-add-col-2');

    expect(value).to.deep.equal([0, 0, 2]);
  });

  it('deletes a col at the end when the last delete is pressed', async function () {
    page.layoutContainers[0].rows[0].cols = [
      { type: '', options: [] },
      { type: '', options: [] },
    ];

    let value;
    page.deleteCol = (a, b, c) => {
      value = [a, b, c];
    };

    this.set('page', page);

    await render(hbs`<Input::Layout::Cols @page={{this.page}} @containerIndex={{0}} @rowIndex={{0}} />`);

    expect(this.element.querySelectorAll('.btn-delete-col-2')).to.have.length(1);

    await click('.btn-delete-col-2');

    expect(value).to.deep.equal([0, 0, 1]);
  });

  it('selects a col on click', async function () {
    page.layoutContainers[0].rows[0].cols = [
      { type: '', options: [] },
      { type: '', options: [] },
    ];

    let type;
    let location;
    let value;
    this.set('onSelect', (t, l, v) => {
      type = t;
      location = l;
      value = v;
    });

    this.set('page', page);

    await render(
      hbs`<Input::Layout::Cols @page={{this.page}} @containerIndex={{0}} @rowIndex={{0}} @onSelect={{this.onSelect}} />`,
    );

    const cols = this.element.querySelectorAll('.layout-col-editable');

    await click(cols[1]);

    expect(type).to.equal('col');
    expect(location).to.deep.equal([0, 0, 1]);
    expect(value).to.equal(page.layoutContainers[0].rows[0].cols[1]);
  });
});
