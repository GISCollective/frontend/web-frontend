/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Helper | page-col-class', function (hooks) {
  setupRenderingTest(hooks);

  it('renders all classes by default', async function () {
    this.set('inputValue', ['btn-primary', 'other-prop', 'display-block', 'display-md-block', 'display-lg-block']);

    await render(hbs`{{page-col-class this.inputValue}}`);
    expect(this.element.textContent.trim()).to.equal(
      'btn-primary other-prop display-block display-md-block display-lg-block',
    );
  });

  it('renders classes without size when deviceSize is sm', async function () {
    this.set('inputValue', ['btn-primary', 'other-prop', 'display-block', 'display-md-block', 'display-lg-block']);

    await render(hbs`{{page-col-class this.inputValue deviceSize="sm"}}`);
    expect(this.element.textContent.trim()).to.equal('btn-primary other-prop display-block');
  });

  it('renders classes with the requested size', async function () {
    this.set('inputValue', ['display-block-1', 'display-md-block-2', 'display-lg-block-3']);

    await render(hbs`{{page-col-class this.inputValue deviceSize="md"}}`);
    expect(this.element.textContent.trim()).to.equal('display-block-2');
  });

  it('renders classes without size when one with size does not exist', async function () {
    this.set('inputValue', ['btn-primary', 'display-block-1', 'display-lg-block']);

    await render(hbs`{{page-col-class this.inputValue deviceSize="lg"}}`);
    expect(this.element.textContent.trim()).to.equal('display-block btn-primary');
  });

  it('renders classes without size and the ones with smaller sizes when there is none matching the expected size', async function () {
    this.set('inputValue', ['btn-primary', 'display-block-1', 'display-md-block']);

    await render(hbs`{{page-col-class this.inputValue deviceSize="lg"}}`);
    expect(this.element.textContent.trim()).to.equal('display-block btn-primary');
  });
});
