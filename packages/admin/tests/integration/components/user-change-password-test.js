/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, fillIn, render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupIntl } from 'ember-intl/test-support';

describe ('Integration | Component | user-change-password',  function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, "en-us");

  hooks.beforeEach(function () {
    const modal = this.owner.lookup('service:modal');
    modal.confirmWithPassword = () => 'currentPassword1';
  });

  it('should have the submit button disabled', async function () {
    await render(hbs`<UserChangePassword/>`);

    expect(
      this.element.querySelector('.btn-submit').hasAttribute('disabled')
    ).to.equal(true);
  });

  it('should have the submit button enabled when all passwords are set correctly', async function () {
    this.set('newPassword', 'newPassword');
    this.set('newPassword2', 'newPassword');

    await render(hbs`<UserChangePassword
      @newPassword={{this.newPassword}}
      @newPassword2={{this.newPassword2}} />`);

    expect(
      this.element.querySelector('.btn-submit').hasAttribute('disabled')
    ).to.equal(false);
  });

  it("should show a message when the new passwords don't match", async function () {
    this.set('newPassword1', 'newPassword1');
    this.set('newPassword12', 'newPassword2');

    await render(hbs`<UserChangePassword
      @newPassword={{this.newPassword1}}
      @newPassword2={{this.newPassword12}} />`);

    expect(
      this.element.querySelector('.btn-submit').hasAttribute('disabled')
    ).to.equal(true);
    expect(
      this.element.querySelector('.invalid-feedback').textContent.trim()
    ).to.equal('the passwords do not match');
  });

  it('should show a message when password is < 10 chars', async function () {
    this.set('currentPassword3', '012345678123');
    this.set('newPassword3', '012345678');
    this.set('newPassword32', '012345678');

    await render(hbs`<UserChangePassword
      @currentPassword={{this.currentPassword3}}
      @newPassword={{this.newPassword3}}
      @newPassword2={{this.newPassword32}} />`);

    expect(
      this.element.querySelector('.btn-submit').hasAttribute('disabled')
    ).to.equal(true);
    expect(
      this.element.querySelector('.invalid-feedback').textContent.trim()
    ).to.equal('the new password must have at least 10 chars');
  });

  it('should should trigger the on update event when submit is pressed', async function () {
    this.set('newPassword1', '');
    this.set('newPassword12', '');

    let userPassword;
    let newPassword;
    this.set('changePassword', (a, b) => {
      userPassword = a;
      newPassword = b;
    });

    await render(hbs`<UserChangePassword
      @onUpdate={{this.changePassword}}
      @newPassword={{this.newPassword1}}
      @newPassword2={{this.newPassword12}} />`);

    await fillIn('.input-password', 'newPassword12345');
    await fillIn('.input-password-confirm', 'newPassword12345');

    await click('.btn-submit');

    expect(userPassword).to.equal('currentPassword1');
    expect(newPassword).to.equal('newPassword12345');
  });
});
