/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | visibility-icon',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing', async function () {
    await render(hbs`<VisibilityIcon />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('.icon-not-published')).not.to.exist;
    expect(this.element.querySelector('.icon-pending')).not.to.exist;
  });

  it('renders nothing when value is true', async function () {
    await render(hbs`<VisibilityIcon @value={{true}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('.icon-not-published')).not.to.exist;
    expect(this.element.querySelector('.icon-pending')).not.to.exist;
  });

  it('renders nothing when value is 1', async function () {
    await render(hbs`<VisibilityIcon @value={{1}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('.icon-not-published')).not.to.exist;
    expect(this.element.querySelector('.icon-pending')).not.to.exist;
  });

  it('renders the private icon when the value is false', async function () {
    await render(hbs`<VisibilityIcon @value={{false}}/>`);
    expect(this.element.querySelector('.icon-not-published')).to.exist;
  });

  it('renders the private icon when the value is 0', async function () {
    await render(hbs`<VisibilityIcon @value={{0}}/>`);
    expect(this.element.querySelector('.icon-not-published')).to.exist;
  });

  it('renders the pending icon when the value is -1', async function () {
    await render(hbs`<VisibilityIcon @value={{-1}}/>`);
    expect(this.element.querySelector('.icon-pending')).to.exist;
  });
});
