import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { service } from '@ember/service';

export default class PictureListFullFrameComponent extends Component {
  @service fullscreen;

  @tracked isGalleryPresentable = false;
  @tracked isGalleryPresenting = false;
  @tracked galleryTarget;

  willDestroy() {
    super.willDestroy(...arguments);
    this.fullscreen.unfreezeBody();
  }

  @action
  setup() {
    this.galleryTarget = document.querySelector('#gallery-target');

    later(() => {
      this.fullscreen.freezeBody();
      this.isGalleryPresentable = true;
    }, 1);
  }

  @action
  prepareHideGallery() {
    this.isGalleryPresentable = false;

    later(() => {
      const modal = this.galleryTarget.querySelector('.page-col-gallery-modal');
      if (!modal) {
        return;
      }

      const style = getComputedStyle?.(modal);

      if (style.opacity == 0) {
        this.checkHideGallery();
      }
    }, 10);
  }

  @action
  checkHideGallery() {
    if (!this.isGalleryPresentable) {
      return this.hideGallery();
    }

    this.isGalleryPresenting = true;
  }

  @action
  hideGallery() {
    this.isGalleryVisible = false;
    this.isGalleryPresentable = false;
    this.isGalleryPresenting = false;

    this.args.onHide?.();
  }

  @action
  select(picture) {
    this.args.onSelect?.(picture);
  }
}
