/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AppRoute from './base/app-route';
import { service } from '@ember/service';
import fetch from 'fetch';
import YAML from 'yaml';
import { action } from '@ember/object';
import { hash } from 'rsvp';
import { MetricsTracker } from '../lib/trackers/metrics';
import * as Sentry from '@sentry/browser';
import { later, scheduleOnce } from '@ember/runloop';
import { EmbeddedMapEditor } from 'manage/lib/embedded-map-editor';
import missingMessage from '../utils/intl/missing-message';

export default class ApplicationRoute extends AppRoute {
  @service articles;
  @service editorJs;
  @service embed;
  @service fastboot;
  @service intl;
  @service lastError;
  @service rtl;
  @service space;
  @service store;
  @service tracking;
  @service user;
  @service window;

  queryParams = {
    fastboot: {},
    embed: {},
    mapInfo: {},
    showMore: {},
    locale: {
      refreshModel: true,
    },
  };

  constructor() {
    super(...arguments);

    this.router.on('routeDidChange', () => {
      const page = this.router.currentURL;
      const title = this.router.currentRouteName || 'unknown';

      this.tracking.addPage(page, title);
    });
  }

  get languages() {
    return this.store.findAll('translation').then((result) => {
      result = result.slice().sort((a, b) => (a.locale > b.locale ? 1 : b.locale > a.locale ? -1 : 0));

      if (result.filter((a) => a.locale.indexOf('en-') == 0).length == 0) {
        result.push({ name: 'English', locale: 'en-us' });
      }

      return result;
    });
  }

  async beforeModel(transition) {
    if(!this.intl.primaryLocale) {
      this.intl.setLocale(['en-us']);
    }

    this.intl.setOnMissingTranslation(missingMessage);

    await this.session.setup();
    await this.space.setup();
    await this.preferences.getAllPreferences();

    await this.user.ready();
    await this.articles.checkAbout();

    this.editorJs.tools["map/map-view"] = {
      class: EmbeddedMapEditor
    };

    Sentry.setUser({
      email: this.user?.userData?.email ?? 'unknown',
      isFastboot: this.fastboot.isFastBoot,
    });

    if (this.fastboot.isFastBoot) {
      const page = transition?.intent?.url;
      const title = transition?.targetName || 'unknown';

      this.tracking.addPage(page, title);
    }

    if (!this.fastboot.isFastBoot) {
      this.tracking.registerTracker('metrics', new MetricsTracker(this.store, this.preferences, this.fastboot));

      this.window.setup(window);
      this.rtl.setup();
    }
  }

  async model(params) {
    if (params.embed === 'true' || params.embed === true) {
      this.embed.isEnabled = true;
    }

    if (params.mapInfo === 'false' || params.mapInfo === false) {
      this.embed.mapInfo = false;
    }

    if (params.showMore === 'false' || params.showMore === false) {
      this.embed.showMore = false;
    }

    if (params.allIcons === 'true' || params.allIcons) {
      this.embed.allIcons = true;
    }

    let languages = [];
    let selectedLanguage;

    try {
      languages = await this.languages;
      selectedLanguage = languages.find((a) => a.locale == params.locale);
      // eslint-disable-next-line no-empty, prettier/prettier
    } catch (err) { }

    if (!selectedLanguage && params.locale) {
      return this.router.transitionTo({ queryParams: { locale: undefined } });
    }

    if (params.locale && selectedLanguage?.file) {
      const translations = await fetch(selectedLanguage.file);
      const text = await translations.text();

      this.intl.addTranslations(params.locale, YAML.parse(text));
    }

    if (params.locale && selectedLanguage?.customTranslations) {
      this.intl.addTranslations(params.locale, selectedLanguage?.customTranslations);
    }

    const defaultLocale = this.space.currentSpace?.locale || 'en-us'

    this.intl.setLocale([params.locale || defaultLocale]);

    const models = await this.store.findAll('model');

    return hash({
      languages,
      models: models.map((a) => ({ name: a.name, itemCount: a.itemCount })),
    });
  }

  async afterModel(model) {
    if (this.fastboot.isFastBoot) {
      this.headData.url = `${this.fastboot.request.protocol}//${this.fastboot.request.host}${this.fastboot.request.path}`;
    } else {
      this.headData.url = window.location.toString();
    }

    this.articles.fillMetaInfo(this.headData);

    if (this.fastboot?.isFastBoot) {
      let promise = new Promise((resolve) => {
        let poll = () => {
          later(() => {
            scheduleOnce('afterRender', this, resolve);
          });
        };
        poll();
      });

      this.fastboot.deferRendering(promise);
    }
  }

  @action
  error(error) {
    if (error.name == "AbortError") {
      return;
    }

    const type = Object.prototype.toString.call(error);
    let errorMessage = 'Unknown error';

    if (error && error.message && error.message.toString) {
      errorMessage = error.message.toString().toLowerCase();
    }

    // eslint-disable-next-line no-console
    console.error(type, errorMessage, error);

    const isAdapterError = error.isAdapterError && error.errors && error.errors.length > 0;

    if (isAdapterError && error.errors[0].status == 404) {
      this.intermediateTransitionTo('/page');
      return;
    }

    if (isAdapterError && error.errors[0].status == 0) {
      return;
    }

    if (type === '[object DOMException]' && errorMessage == 'aborted') {
      return;
    }

    if (error.offerLogin) {
      this.lastError.set('offerLogin', true);
    }

    this.lastError.set('message', error.message);
    this.intermediateTransitionTo('error');
  }
}
