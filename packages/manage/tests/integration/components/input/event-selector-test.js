import { module, test } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | input/event-selector', function (hooks) {
  setupRenderingTest(hooks);
  let server;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultEvent('1');
    server.testData.storage.addDefaultEvent('2');
  });

  hooks.after(function() {
    server.shutdown();
  });

  it('renders an empty select by default', async function (assert) {
    await render(hbs`<Input::EventSelector />`);

    const options = [...this.element.querySelectorAll('option')].map((a) => a.textContent.trim());

    expect(options).to.deep.equal(['', 'test event', 'test event']);
  });

  it('renders the selected value', async function (assert) {
    const event = await this.store.findRecord('event', '1');
    this.set('value', event);

    await render(hbs`<Input::EventSelector @value={{this.value}} />`);

    const options = [...this.element.querySelectorAll('option')].map((a) => a.textContent.trim());

    expect(this.element.querySelector('select').value).to.equal('1');
    expect(options).to.deep.equal(['test event', 'test event']);
  });

  it('triggers the change event when the value is changed', async function (assert) {
    const event = await this.store.findRecord('event', '1');
    this.set('value', event);

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::EventSelector @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('select').value = '2';
    await triggerEvent('select', 'change');

    expect(value.id).to.equal('2');
  });
});
