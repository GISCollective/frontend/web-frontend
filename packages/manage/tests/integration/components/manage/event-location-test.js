/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';

describe('Integration | Component | input/manage/event-location', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function () {
    it('renders a text value', async function () {
      this.set('value', {
        type: 'Text',
        value: 'some place in our town',
      });

      await render(hbs`<Manage::EventLocation @value={{this.value}} @title="location" />`);

      expect(this.element.querySelector('.event-location').textContent.trim()).to.equal('some place in our town');
    });
  });

  describe('in edit mode', function (hooks) {
    let value;
    let server;

    hooks.before(function () {
      server = new TestServer();
    });

    hooks.after(function() {
      server.shutdown();
    });

    hooks.beforeEach(function () {
      server.testData.storage.addDefaultFeature('1');

      value = undefined;

      this.set('change', function (v) {
        value = v;
      });
    });

    it('renders the value', async function (a) {
      this.set('value', {
        type: 'Text',
        value: 'some place in our town',
      });

      await render(hbs`<Manage::EventLocation @value={{this.value}} @title="location" @editablePanel="location" />`);

      expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal(
        'some place in our town (as text)',
      );
    });

    it('can change the value', async function (a) {
      this.set('value', {
        type: 'Text',
        value: 'some place in our town',
      });

      await render(
        hbs`<Manage::EventLocation @value={{this.value}} @onSave={{this.change}} @title="location" @editablePanel="location" />`,
      );

      await selectSearch('.input-event-location', 'a new text value');
      await selectChoose('.input-event-location', 'a new text value');
      await click('.btn-submit');
      await waitUntil(() => value);

      expect(value).to.deep.equal({ type: 'Text', value: 'a new text value' });
    });

    it('can reset the changes the value', async function (a) {
      this.set('value', {
        type: 'Text',
        value: 'some place in our town',
      });

      let canceled;
      this.set('cancel', () => {
        canceled = true;
      });

      await render(
        hbs`<Manage::EventLocation @value={{this.value}} @onCancel={{this.cancel}} @title="location" @editablePanel="location" />`,
      );

      await selectSearch('.input-event-location', 'a new text value');
      await selectChoose('.input-event-location', 'a new text value');

      await click('.btn-cancel');

      expect(canceled).to.equal(true);
      expect(value).not.to.exist;
    });
  });
});
