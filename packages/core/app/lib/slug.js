/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export {
  matchSlug,
  toPageSlug,
  toPagePath,
  toPageId,
  toModelSlug,
  fillVariable,
  getVariablesFromPath,
} from 'core/lib/slug';
