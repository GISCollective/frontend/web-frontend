/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class InputLayoutDeviceSizeComponent extends Component {
  @action
  select(value) {
    if (value == this.args.value) {
      return this.args.onChange?.(null);
    }
    this.args.onChange?.(value);
  }
}
