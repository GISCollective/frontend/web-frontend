/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | input/font-style', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function (assert) {
    this.set('value', ['fw-bold', 'text-size-12', 'ff-lato']);

    await render(hbs`<Input::FontStyle @value={{this.value}} />`);

    expect(this.element.querySelector('.input--ff').value).to.equal('lato');
    expect(this.element.querySelector('.input--text-size ').value).to.equal('12');
    expect(this.element.querySelector('.input--fw').value).to.equal('bold');
    expect(this.element.querySelector('.preview')).to.have.classes('fw-bold text-size-12 ff-lato');
  });

  it('can chanage the values', async function (assert) {
    let value;
    this.set('value', ['fw-bold', 'text-size-12', 'ff-lato']);
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::FontStyle @value={{this.value}} @onChange={{this.change}} />`);

    this.element.querySelector('.input--ff').value = 'anton';
    await triggerEvent('.input--ff', 'change');

    this.element.querySelector('.input--fw').value = 'light';
    await triggerEvent('.input--fw', 'change');

    this.element.querySelector('.input--text-size').value = '18';
    await triggerEvent('.input--text-size', 'change');

    assert.deepEqual(value, ['ff-anton', 'fw-light', 'text-size-18']);
  });
});
