/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { toDateTime } from 'models/transforms/calendar-entry-list';
import { DateTime } from 'luxon';

export default class ManageCardComponent extends Component {
  get record() {
    return this.args.record ?? {};
  }

  get bgCover() {
    let picture = this.record.cover?.get?.('picture') ?? this.record.cover?.picture;

    if (!picture) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${picture}/md')`);
  }

  get hasVisibility() {
    if (typeof this.record?.isPublished == 'boolean') {
      return true;
    }

    return !!this.record?.visibility;
  }

  get isPublic() {
    if (typeof this.record?.isPublished == 'boolean') {
      return this.record.isPublished;
    }

    return this.record?.visibility?.isPublic;
  }

  get isPending() {
    if (typeof this.record?.isPending == 'boolean') {
      return this.record.isPending;
    }

    return false;
  }

  get isPrivate() {
    if (typeof this.record?.isPrivate == 'boolean') {
      return this.record.isPrivate;
    }

    return !this.isPublic;
  }

  get author() {
    return this.record?.info?.originalAuthorProfile;
  }

  toNiceDate(value) {
    if (!value) {
      return '';
    }

    return toDateTime(value).toLocaleString(DateTime.DATETIME_FULL);
  }

  get niceCreationDate() {
    return this.toNiceDate(this.record?.info?.createdOn);
  }

  get niceReleaseDate() {
    return this.toNiceDate(this.record?.releaseDate);
  }

  get status() {
    return this.record?.status ?? '';
  }

  get statusColor() {
    if (this.status == 'pending') {
      return 'info';
    }

    if (this.status == 'publishing') {
      return 'info';
    }

    if (this.status == 'sent') {
      return 'success';
    }

    return `secondary`;
  }

  @action
  async setup() {
    await this.record?.info?.fetch?.();
  }

  @action
  onClick() {
    return this.args.onClick?.(...arguments);
  }
}
