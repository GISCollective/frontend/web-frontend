/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/slot', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no argument', async function () {
    await render(hbs`<View::Slot />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a title with buttons page col when type is title-with-buttons', async function () {
    this.set('value', {
      container: 0,
      col: 0,
      row: 0,
      type: 'title-with-buttons',
      data: {
        title: { text: 'title', heading: 1 },
        paragraph: { text: 'paragraph' },
        buttons: [
          {
            name: 'name',
            link: 'https://giscollective.com',
            type: 'default',
            storeType: 'apple',
            options: ['btn-primary'],
          },
        ],
      },
    });

    await render(hbs`<View::Slot @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col')).to.have.attribute('data-type', 'slot');
    expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
  });

  it('renders a picture with fill size when the type is picture', async function () {
    this.set('value', {
      container: 0,
      col: 0,
      row: 0,
      type: 'picture',
      record: {
        picture: 'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
      },
      data: {
        sizing: 'fill',
        proportion: '4:3',
        imageOptions: ['rounded-3'],
      },
    });

    await render(hbs`<View::Slot @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col')).to.have.attribute('data-type', 'slot');
    expect(this.element.querySelector('.page-col')).to.have.class('size-fill');
    expect(this.element.querySelector('.page-col').parentElement).to.have.attribute('class', 'ember-application');
  });

  it('adds size-fill class to the parent col when the page col has a fill sizing', async function () {
    this.set('value', {
      container: 0,
      col: 0,
      row: 0,
      type: 'picture',
      record: {
        picture: 'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
      },
      data: {
        sizing: 'fill',
        proportion: '4:3',
        imageOptions: ['rounded-3'],
      },
    });

    await render(hbs`<div class="col"><View::Slot @value={{this.value}} /></div>`);

    expect(this.element.querySelector('.page-col')).to.have.attribute('data-type', 'slot');
    expect(this.element.querySelector('.page-col')).to.have.class('size-fill');
    expect(this.element.querySelector('.page-col').parentElement).to.have.attribute('class', 'col size-fill');
  });

  it('sets a height style when the col has a height property', async function () {
    this.set('value', {
      container: 0,
      col: 0,
      row: 0,
      type: 'picture',
      record: {
        picture: 'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
      },
      data: {
        sizing: 'fill',
        proportion: '4:3',
        imageOptions: ['rounded-3'],
        height: 150,
      },
    });

    await render(hbs`<View::Slot @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col')).to.have.attribute('style', 'height: 150px;');
  });
});
