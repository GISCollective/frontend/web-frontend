/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { hash } from 'rsvp';
import { toPagePath } from 'core/lib/slug';
import { fromPageState, toPageState } from 'core/lib/page-state';

export default class SearchComponent extends Component {
  @service store;
  @service router;
  @service space;

  @tracked relatedMap;

  @tracked searchTerm;
  @tracked searchedTerm;

  @tracked features;
  @tracked features_expected = 5;

  @tracked maps;
  @tracked maps_expected = 5;

  @tracked campaigns;
  @tracked campaigns_expected = 5;

  @tracked iconSets;
  @tracked iconSets_expected = 5;

  @tracked teams;
  @tracked teams_expected = 5;

  @tracked icons;
  @tracked icons_expected = 5;

  @tracked events;
  @tracked events_expected = 5;

  @tracked articles;
  @tracked articles_expected = 5;

  @tracked geocodings;

  @tracked isDisabled;

  nameMap = {
    features: 'Feature',
    maps: 'Map',
    campaigns: 'Campaign',
    iconSets: 'IconSet',
    teams: 'Team',
    icons: 'Icon',
    geocodings: 'Geocoding',
    events: 'Event',
    articles: 'Article',
  };

  get query() {
    let team;

    if (!this.args.space?.visibility?.isDefault && this.args.space?.visibility?.teamId) {
      team = this.args.space?.visibility?.teamId;
    }

    return {
      q: this.searchTerm,
      map: this.args.mapId,
      team,
    };
  }

  queryModel(model, limit = 6, skip = 0) {
    const query = { ...this.query, limit, skip };

    if (model == 'Geocoding') {
      return this.store.query('geocoding', {
        query: this.searchTerm,
      });
    }

    return this.store.query('search-meta', {
      ...query,
      model,
    });
  }

  get resultCount() {
    const keys = ['features', 'maps', 'campaigns', 'iconSets', 'icons', 'teams', 'icons', 'events', 'articles'];

    return keys.map((a) => this[a]?.length ?? 0).reduce((a, b) => a + b, 0);
  }

  getRequestedModels() {
    let models = ['features'];

    if (!this.args.mapId) {
      models = [...models, 'maps', 'campaigns', 'iconSets', 'teams', 'icons', 'events', 'articles'];
    }

    if (this.args.mapId || this.args.space?.globalMapPageId) {
      models.push('geocodings');
    }

    return models;
  }

  get searchGeoCodings() {
    return !!this.args.mapId;
  }

  get searchOptions() {
    return this.args?.space?.searchOptions;
  }

  @action
  setupSearchButton(element) {
    this.searchButton = element;
  }

  @action
  onClose() {
    return this.args.onClose?.();
  }

  @action
  async onSearch() {
    if (!this.searchOptions) {
      return;
    }

    this.isDisabled = true;

    const models = {};

    const requestedModels = this.getRequestedModels();

    for (const key of requestedModels) {
      if (this.searchOptions[key]) {
        models[key] = this.queryModel(this.nameMap[key]);
      }
    }

    const result = await hash(models);

    this.searchedTerm = this.searchTerm;

    for (const key of Object.keys(this.nameMap)) {
      this[key] = [];
    }

    for (const key of Object.keys(result)) {
      this[key] = result[key].slice();
    }

    this.isDisabled = false;
  }

  @action
  async keyUp(event) {
    if (event.key == 'Enter' || event.keyCode == 13) {
      await this.searchButton.click();
    }
  }

  @action
  async loadMore(key) {
    const result = await this.queryModel(this.nameMap[key], 6, this[key].length);

    this[key] = [...this[key], ...result.slice()];
    this[`${key}_expected`] += 6;
  }

  @action
  async setup() {
    this.relatedMap = undefined;

    if (this.args.mapId) {
      this.relatedMap = await this.store.findRecord('map', this.args.mapId);
    }
  }

  @action
  async showPlace(place) {
    const s = this.router.currentRoute?.queryParams?.s;

    const state = fromPageState(s);
    state['gec'] = place?.id;

    const params = {
      queryParams: { s: toPageState(state) },
    };

    if (this.args.space?.globalMapPageId && !this.args.mapId) {
      const page = await this.args.space.globalMapPageId;

      this.router.transitionTo(toPagePath(page.slug), params);
      return this.onClose();
    }

    this.router.transitionTo(params);
    this.onClose();
  }
}
