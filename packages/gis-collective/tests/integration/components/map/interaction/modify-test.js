/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import Map from 'ol/Map';
import Collection from 'ol/Collection';

describe ('Integration | Component | map/interaction/modify',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing', async function () {
    await render(hbs`<Map::Interaction::Modify />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when map and features are set', async function () {
    const map = new Map();
    this.set('map', map);
    this.set('features', new Collection());

    // Template block usage:
    await render(hbs`
      <Map::Interaction::Modify @map={{this.map}} @features={{this.features}} as | interaction features |>
        {{interaction}}-{{features}}
      </Map::Interaction::Modify>
    `);

    expect(this.element.textContent.trim()).to.equal(
      '[object Object]-[object Object]'
    );
  });

  it('creates a new interaction in the map', async function () {
    const map = new Map();
    this.set('map1', map);
    this.set('features1', new Collection());

    const interactionCount = map.getInteractions().getLength();

    await render(
      hbs`<Map::Interaction::Modify @map={{this.map1}} @features={{this.features1}} />`
    );
    expect(this.map1.getInteractions().getLength()).to.equal(
      interactionCount + 1
    );
  });

  it('removes the interaction when the component is removed', async function () {
    const map = new Map();
    this.set('features2', new Collection());

    this.set('map2', map);
    this.set('show', true);
    const interactionCount = map.getInteractions().getLength();

    await render(
      hbs`{{#if this.show}}<Map::Interaction::Modify @map={{this.map2}} @features={{this.features2}} />{{/if}}`
    );
    expect(map.getInteractions().getLength()).to.equal(interactionCount + 1);

    this.set('show', false);
    expect(map.getInteractions().getLength()).to.equal(interactionCount);
  });
});
