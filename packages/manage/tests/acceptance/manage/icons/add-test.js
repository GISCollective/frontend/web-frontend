/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, fillIn, waitFor, triggerEvent, waitUntil } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import { selectSearch } from 'ember-power-select/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/add/icon', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedIcon;
  let team;
  let iconSet;
  let icon;

  hooks.beforeEach(function () {
    server = new TestServer();
    team = server.testData.create.team();
    iconSet = server.testData.create.iconSet();
    icon = server.testData.create.icon();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);

    server.post('/mock-server/icons', (request) => {
      receivedIcon = JSON.parse(request.requestBody);
      receivedIcon.icon._id = icon._id;

      server.testData.storage.addIcon(receivedIcon.icon);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedIcon)];
    });

    receivedIcon = null;
  });

  hooks.afterEach(function () {
    server.shutdown();
    const elm = document.querySelector('.modal-backdrop');
    if (elm) {
      elm.parentNode.removeChild(elm);
    }

    document.querySelector('body').classList.remove('modal-open');
  });

  describe('when there is no icon set id in the url', function (hooks) {
    describe('when the user does not have access to an icon set', function (hooks) {
      it('can visit /manage/icons/add/_', async function () {
        authenticateSession();
        await visit(`/manage/icons/add/_`);
        await PageElements.waitEditorJs(this.element);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        expect(currentURL()).to.equal(`/manage/icons/add/_`);

        expect(this.element.querySelector('h1').textContent.trim()).to.equal('New icon');

        expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
        expect(this.element.querySelector('.row-name input.form-control')).to.exist;

        expect(this.element.querySelector('.row-description label').textContent.trim()).to.contain('Description');
        expect(this.element.querySelector('.row-description .editor-js')).to.exist;
        expect(this.element.querySelector('.ce-paragraph').textContent).to.contain(
          'This is our icon designed by our team.',
        );

        expect(this.element.querySelector('.row-icon-set label').textContent.trim()).to.contain('Icon set');
        expect(this.element.querySelector('.row-icon-set select')).not.to.exist;

        expect(this.element.querySelector('.row-parent-icon label').textContent.trim()).to.contain('Parent icon');
        expect(this.element.querySelector('.row-parent-icon .ember-power-select-trigger')).to.exist;

        expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
      });

      it('there is a link to add a new icon set', async function () {
        authenticateSession();
        await visit(`/manage/icons/add/_`);
        await PageElements.waitEditorJs(this.element);

        expect(this.element.querySelector('.row-icon-set label').textContent.trim()).to.contain('Icon set');
        expect(this.element.querySelector('.row-icon-set select')).not.to.exist;
        expect(this.element.querySelector('.btn-add-icon-set')).to.exist;
        expect(this.element.querySelector('.btn-add-icon-set')).to.have.attribute(
          'href',
          '/manage/icons/add?next=manage.icons.add',
        );
      });

      it('has the submit button disabled when there is no icon set', async function () {
        authenticateSession();
        await visit(`/manage/icons/add/_`);
        await PageElements.waitEditorJs(this.element);

        await fillIn('.row-name input.form-control', 'test name');

        expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');
      });
    });

    describe('when the user has access to an icon set', function (hooks) {
      hooks.beforeEach(function () {
        server.testData.storage.addIconSet(iconSet);
      });

      it('can visit /manage/icons/add/_', async function () {
        authenticateSession();
        await visit(`/manage/icons/add/_`);
        await PageElements.waitEditorJs(this.element);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        expect(currentURL()).to.equal(`/manage/icons/add/_`);

        expect(this.element.querySelector('h1').textContent.trim()).to.equal('New icon');

        expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
        expect(this.element.querySelector('.row-name input.form-control')).to.exist;

        expect(this.element.querySelector('.row-description label').textContent.trim()).to.contain('Description');
        expect(this.element.querySelector('.row-description .editor-js')).to.exist;
        expect(this.element.querySelector('.ce-paragraph').textContent).to.contain(
          'This is our icon designed by our team.',
        );

        expect(this.element.querySelector('.row-icon-set label').textContent.trim()).to.contain('Icon set');
        expect(this.element.querySelector('.row-icon-set select')).to.exist;

        expect(this.element.querySelector('.row-parent-icon label').textContent.trim()).to.contain('Parent icon');
        expect(this.element.querySelector('.row-parent-icon .ember-power-select-trigger')).to.exist;

        expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
      });

      it('has the submit button disabled when there is no icon set', async function () {
        authenticateSession();
        await visit(`/manage/icons/add/_`);
        await PageElements.waitEditorJs(this.element);

        await fillIn('.row-name input.form-control', 'test name');

        expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');
      });

      it('can create an icon', async function () {
        authenticateSession();
        await visit(`/manage/icons/add/_`);
        await PageElements.waitEditorJs(this.element);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.row-name input.form-control', 'test name');

        this.element.querySelector('.row-icon-set select').value = iconSet._id;
        await triggerEvent('.row-icon-set select', 'change');

        await fillIn('.ce-paragraph', 'test description');

        const blob = server.testData.create.pngBlob();

        await triggerEvent("input[type='file']", 'change', { files: [blob] });

        await PageElements.wait(600);

        await click('.btn-submit');
        await PageElements.waitEditorJs(this.element);

        expect(receivedIcon.icon.description.blocks).to.deep.equal([
          {
            type: 'header',
            data: {
              text: 'test name',
              level: 1,
            },
          },
          {
            type: 'paragraph',
            data: {
              text: 'test description',
            },
          },
        ]);

        expect(receivedIcon).to.deep.equal({
          icon: {
            _id: icon._id,
            name: 'test name',
            localName: null,
            description: {
              blocks: [
                { type: 'header', data: { text: 'test name', level: 1 } },
                { type: 'paragraph', data: { text: 'test description' } },
              ],
            },
            image: {
              useParent: false,
              value: receivedIcon.icon.image.value,
            },
            parent: '',
            category: '',
            subcategory: '',
            allowMany: false,
            minZoom: 0,
            maxZoom: 20,
            keepWhenSmall: true,
            attributes: [],
            iconSet: iconSet._id,
            order: 999,
          },
        });

        expect(currentURL()).to.equal('/manage/icons/edit/' + iconSet._id + '/' + icon._id);
      });
    });
  });

  describe('when there is an icon set id in the url', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addIconSet(iconSet);
      server.testData.storage.addDefaultIcon();
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/icons/add/' + iconSet._id);
      expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Ficons%2Fadd%2F5ca7b702ecd8490100cab96f');
    });

    it('can visit /manage/icons/add/:iconsetid', async function () {
      authenticateSession();
      await visit(`/manage/icons/add/${iconSet._id}`);
      await PageElements.waitEditorJs(this.element);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      expect(currentURL()).to.equal(`/manage/icons/add/${iconSet._id}`);

      expect(this.element.querySelector('h1').textContent.trim()).to.equal('New icon');

      expect(this.element.querySelector('.row-name label').textContent.trim()).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to.exist;

      expect(this.element.querySelector('.row-description label').textContent.trim()).to.contain('Description');
      expect(this.element.querySelector('.row-description .editor-js')).to.exist;
      expect(this.element.querySelector('.ce-paragraph').textContent).to.contain(
        'This is our icon designed by our team.',
      );

      expect(this.element.querySelector('.row-icon-set')).not.to.exist;

      expect(this.element.querySelector('.row-parent-icon label').textContent.trim()).to.contain('Parent icon');
      expect(this.element.querySelector('.row-parent-icon .ember-power-select-trigger')).to.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
    });

    it('can create an icon', async function (a) {
      authenticateSession();
      await visit(`/manage/icons/add/${iconSet._id}`);
      await PageElements.waitEditorJs(this.element);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      expect(currentURL()).to.equal('/manage/icons/add/' + iconSet._id);
      await fillIn('.row-name input.form-control', 'test name');

      await fillIn('.ce-paragraph', 'test description');

      const blob = server.testData.create.pngBlob();

      await triggerEvent("input[type='file']", 'change', { files: [blob] });

      await PageElements.wait(1000);
      await click('.btn-submit');
      await PageElements.waitEditorJs(this.element);

      expect(receivedIcon.icon.description.blocks).to.deep.equal([
        {
          type: 'header',
          data: {
            text: 'test name',
            level: 1,
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'test description',
          },
        },
      ]);

      expect(receivedIcon).to.deep.equal({
        icon: {
          _id: icon._id,
          name: 'test name',
          localName: null,
          description: {
            blocks: [
              {
                type: 'header',
                data: {
                  text: 'test name',
                  level: 1,
                },
              },
              {
                type: 'paragraph',
                data: {
                  text: 'test description',
                },
              },
            ],
          },
          image: {
            useParent: false,
            value: receivedIcon.icon.image.value,
          },
          parent: '',
          category: '',
          subcategory: '',
          minZoom: 0,
          maxZoom: 20,
          keepWhenSmall: true,
          allowMany: false,
          attributes: [],
          iconSet: iconSet._id,
          order: 999,
        },
      });

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });

    it('can create an icon with category and subcategory', async function (a) {
      authenticateSession();
      await visit(`/manage/icons/add/${iconSet._id}?c=category&sc=subcategory`);
      await PageElements.waitEditorJs(this.element);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      await fillIn('.row-name input.form-control', 'test name');
      await fillIn('.ce-paragraph', 'test description');

      const blob = server.testData.create.pngBlob();

      await triggerEvent("input[type='file']", 'change', { files: [blob] });

      await PageElements.wait(1000);
      await click('.btn-submit');
      await PageElements.waitEditorJs(this.element);

      expect(receivedIcon.icon.description.blocks).to.deep.equal([
        {
          type: 'header',
          data: {
            text: 'test name',
            level: 1,
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'test description',
          },
        },
      ]);

      expect(receivedIcon).to.deep.equal({
        icon: {
          _id: icon._id,
          name: 'test name',
          localName: null,
          description: {
            blocks: [
              {
                type: 'header',
                data: {
                  text: 'test name',
                  level: 1,
                },
              },
              {
                type: 'paragraph',
                data: {
                  text: 'test description',
                },
              },
            ],
          },
          image: {
            useParent: false,
            value: receivedIcon.icon.image.value,
          },
          parent: '',
          category: 'category',
          subcategory: 'subcategory',
          minZoom: 0,
          maxZoom: 20,
          keepWhenSmall: true,
          allowMany: false,
          attributes: [],
          iconSet: iconSet._id,
          order: 999,
        },
      });

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });

    it('has the submit button disabled when there is no name', async function () {
      authenticateSession();
      await visit('/manage/icons/add/' + iconSet._id);
      await PageElements.waitEditorJs(this.element);

      expect(currentURL()).to.equal('/manage/icons/add/' + iconSet._id);

      expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');
    });

    it('has the submit button disabled when the image is not filled', async function () {
      authenticateSession();
      await visit('/manage/icons/add/' + iconSet._id);
      await PageElements.waitEditorJs(this.element);

      await waitFor('.row-name');
      await fillIn('.row-name input.form-control', 'icon name');

      expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');
    });

    it('has the submit button enabled when the name and image are filled', async function () {
      authenticateSession();
      await visit('/manage/icons/add/' + iconSet._id);
      await PageElements.waitEditorJs(this.element);

      await waitFor('.row-name');
      await fillIn('.row-name input.form-control', 'icon name');
      const blob = server.testData.create.pngBlob();

      await triggerEvent("input[type='file']", 'change', { files: [blob] });

      expect(this.element.querySelector('.btn.btn-submit')).not.to.have.attribute('disabled', '');
    });

    it('has the submit button enabled when the name is filled and the parent icon image is selected', async function () {
      authenticateSession();
      await visit(`/manage/icons/add/${iconSet._id}`);
      await PageElements.waitEditorJs(this.element);

      await fillIn('.row-name input.form-control', 'icon name');
      await selectSearch('.row-parent-icon .ember-power-select-trigger', 'something');

      await waitUntil(() => server.history.indexOf('GET /mock-server/icons?term=something') != -1);
      await click(this.element.querySelectorAll('.ember-power-select-option')[0]);

      expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn-use-parent-icon'));
      expect(this.element.querySelector('.btn.btn-submit')).not.to.have.attribute('disabled', '');
    });
  });
});
