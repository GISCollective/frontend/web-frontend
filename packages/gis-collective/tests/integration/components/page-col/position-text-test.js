/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | view/position-text',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::PositionText />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('svg')).not.to.exist;
  });

  it('renders the point coordinates', async function () {
    this.set('value', {
      type: 'Point',
      coordinates: [102.0, 0.5],
    });

    await render(hbs`<View::PositionText @value={{this.value}}/>`);

    expect(this.element.querySelector('.fa-location-dot')).to.exist;
    expect(this.element.textContent.trim()).to.equal('102, 0.5');
  });

  it('renders the multi point coordinates', async function () {
    this.set('value', {
      type: 'MultiPoint',
      coordinates: [
        [100.0, 0.0],
        [101.0, 1.0],
      ],
    });

    await render(hbs`<View::PositionText @value={{this.value}}/>`);

    expect(this.element.querySelector('.fa-braille')).to.exist;
    expect(this.element.textContent.trim()).to.contain('100, 0');
    expect(this.element.textContent.trim()).to.contain('+1');
  });

  it('renders the line coordinates', async function () {
    this.set('value', {
      type: 'LineString',
      coordinates: [
        [100.0, 0.0],
        [101.0, 1.0],
      ],
    });

    await render(hbs`<View::PositionText @value={{this.value}}/>`);

    expect(this.element.querySelector('.fa-wave-square')).to.exist;
    expect(this.element.textContent.trim()).to.contain('100, 0');
    expect(this.element.textContent.trim()).to.contain('+1');
  });

  it('renders the multi line coordinates', async function () {
    this.set('value', {
      type: 'MultiLineString',
      coordinates: [
        [
          [100.0, 0.0],
          [101.0, 1.0],
        ],
        [
          [102.0, 2.0],
          [103.0, 3.0],
        ],
      ],
    });

    await render(hbs`<View::PositionText @value={{this.value}}/>`);

    expect(this.element.querySelector('.fa-wave-square')).to.exist;
    expect(this.element.textContent.trim()).to.contain('100, 0');
    expect(this.element.textContent.trim()).to.contain('+3');
  });

  it('renders the polygon coordinates', async function () {
    this.set('value', {
      type: 'Polygon',
      coordinates: [
        [
          [100.0, 0.0],
          [101.0, 0.0],
          [101.0, 1.0],
          [100.0, 1.0],
          [100.0, 0.0],
        ],
      ],
    });

    await render(hbs`<View::PositionText @value={{this.value}}/>`);

    expect(this.element.querySelector('.fa-draw-polygon')).to.exist;
    expect(this.element.textContent.trim()).to.contain('100, 0');
    expect(this.element.textContent.trim()).to.contain('+4');
  });

  it('renders the multi polygon coordinates', async function () {
    this.set('value', {
      type: 'MultiPolygon',
      coordinates: [
        [
          [
            [180.0, 40.0],
            [180.0, 50.0],
            [170.0, 50.0],
            [170.0, 40.0],
            [180.0, 40.0],
          ],
        ],
        [
          [
            [-170.0, 40.0],
            [-170.0, 50.0],
            [-180.0, 50.0],
            [-180.0, 40.0],
            [-170.0, 40.0],
          ],
        ],
      ],
    });

    await render(hbs`<View::PositionText @value={{this.value}}/>`);

    expect(this.element.querySelector('.fa-draw-polygon')).to.exist;
    expect(this.element.textContent.trim()).to.contain('180, 40');
    expect(this.element.textContent.trim()).to.contain('+9');
  });
});
