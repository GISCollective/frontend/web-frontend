/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { fromPageState, toPageState } from 'core/lib/page-state';
import { cancel, later } from '@ember/runloop';

export default class StateFiltersMapFeatureComponent extends Component {
  @service store;
  @service clickOutside;
  @tracked results;
  @tracked _feature;

  @action
  select(value) {
    this._feature = value;
    const newState = toPageState({
      ...this.state,
      ffid: value?.id,
    });

    this.args.onChangeState?.(newState);
    this.clickOutside.unsubscribe();
  }

  get hasSearch() {
    return this.args.options?.['with-search'];
  }

  get limit() {
    return this.hasSearch ? 10 : 0;
  }

  get title() {
    return this.args.options?.label;
  }

  get placeholder() {
    return this.args.options?.['placeholder-text'];
  }

  get state() {
    return fromPageState(this.args.state);
  }

  get feature() {
    if (this._feature) {
      return this._feature;
    }

    if (!this.state.ffid) {
      return null;
    }

    return this.store.peekRecord('feature', this.state.ffid);
  }

  get niceValue() {
    return this.feature?.name;
  }

  @action
  reset() {
    this._feature = null;
    const newState = toPageState({
      ...this.state,
      ffid: undefined,
    });

    this.args.onChangeState?.(newState);
  }

  async loadFeatures(query) {
    query.sortBy = 'name';
    query.sortOrder = 'asc';

    if (this.limit) {
      query.limit = this.limit;
    }

    if (typeof this.args.options?.map == 'string') {
      query.map = this.args.options.map;
    }

    if (typeof this.args.options?.map?.id == 'string') {
      query.map = this.args.options.map.id;
    }

    this.queryTimer = later(async () => {
      try {
        this.results = await this.store.query('feature', query);
      } catch (err) {
        this.results = [];
      }
    }, 500);
  }

  @action
  async search(term) {
    cancel(this.queryTimer);
    if (!term || term.length < 4) {
      this.results = [];
      return;
    }

    const query = {
      term,
    };

    return this.loadFeatures(query);
  }

  @action
  async setup() {
    if (this.state.ffid && !this.feature) {
      this._feature = await this.store.findRecord('feature', this.state.ffid);
    } else {
      this._feature = null;
    }

    if (!this.hasSearch) {
      this.loadFeatures({});
    }
  }
}
