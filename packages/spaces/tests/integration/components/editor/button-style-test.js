/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/button-style", function (hooks) {
  setupRenderingTest(hooks);

  describe("the link buttons", function (hooks) {
    let changedValue;

    hooks.beforeEach(function () {
      changedValue = null;

      this.set("onChange", (v) => {
        changedValue = v;
      });

      this.set("value", {
        classes: ["btn-primary", "size-btn-lg"],
      });
    });

    it("renders the right button type", async function () {
      await render(hbs`<Editor::ButtonStyle @value={{this.value}} @onChange={{this.onChange}} />`);

      expect(this.element.querySelector(".input-sm-btn").value).to.equal("primary");
    });

    it("can change the style", async function () {
      await render(hbs`<Editor::ButtonStyle @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-sm-btn").value = "danger";
      await triggerEvent(".input-sm-btn", "change");

      expect(changedValue).to.deep.equal({
        classes: ["size-btn-lg", "btn-danger"],
      });
    });

    it("can change to a color style", async function (a) {
      await render(hbs`<Editor::ButtonStyle @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-sm-btn").value = "color";
      await triggerEvent(".input-sm-btn", "change");

      const colors = this.element.querySelectorAll(".input-bs-color");
      await click(colors[0]);
      await click(colors[0].querySelector(".btn-color-primary"));

      await click(colors[1]);
      await click(colors[1].querySelector(".btn-color-secondary"));

      expect(changedValue).to.deep.equal({
        classes: ["size-btn-lg", "btn-color", "bg-primary", "text-secondary"],
      });
    });

    it("can change from a color style", async function (a) {
      this.set("value", {
        classes: ["size-btn-lg", "btn-color", "bg-primary", "text-secondary"],
      });

      await render(hbs`<Editor::ButtonStyle @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-sm-btn").value = "color";
      await triggerEvent(".input-sm-btn", "change");

      const colors = this.element.querySelectorAll(".selected-value");

      expect(colors[0]).to.have.class("bg-primary");
      expect(colors[1]).to.have.class("bg-secondary");

      this.element.querySelector(".input-sm-btn").value = "primary";
      await triggerEvent(".input-sm-btn", "change");

      expect(changedValue).to.deep.equal({ classes: ["size-btn-lg", "btn-primary"] });
    });

    it("can change the width", async function () {
      await render(hbs`<Editor::ButtonStyle @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-sm-w").value = "50";
      await triggerEvent(".input-sm-w", "change");

      expect(changedValue).to.deep.equal({
        classes: ["btn-primary", "size-btn-lg", "w-50"],
      });
    });

    it("can change the margin", async function (a) {
      await render(hbs`<Editor::ButtonStyle @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input-sm-w").value = "50";
      await triggerEvent(".input-sm-w", "change");

      expect(changedValue).to.deep.equal({
        classes: ["btn-primary", "size-btn-lg", "w-50"],
      });
    });

    it("can change the font", async function (a) {
      await render(hbs`<Editor::ButtonStyle @value={{this.value}} @onChange={{this.onChange}} />`);

      this.element.querySelector(".input--ff").value = "oswald";
      await triggerEvent(".input--ff", "change");

      expect(changedValue).to.deep.equal({
        classes: ["btn-primary", "size-btn-lg", "ff-oswald"],
      });
    });
  });
});
