/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, wait } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, waitUntil, fillIn, triggerEvent, typeIn } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/spaces/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let space;
  let receivedSpace;
  let defaultPicture;
  let receivedPicture;
  let mapData;

  hooks.before(function() {
    server = new TestServer();
  })

  hooks.after(function () {
    server.shutdown();
  });


  hooks.beforeEach(function () {
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    space = server.testData.storage.addDefaultSpace();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');

    server.testData.storage.addDefaultTeam('61292c4c7bdf9301008fd7b6');
    server.testData.storage.addDefaultTeam('000000000000000000000001');

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultPreferences();

    mapData = server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage('000000000000000000000002');

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/spaces/${space._id}`, (request) => {
      receivedSpace = JSON.parse(request.requestBody);
      receivedSpace.space._id = space._id;

      server.testData.storage.addSpace(receivedSpace.space);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedSpace)];
    });

    receivedSpace = null;
  });


  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fspaces%2Fedit%2F6227c131624b2cf1626dd029');
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/spaces/edit/${space._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'GISCollective', 'Edit']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/61292c4c7bdf9301008fd7b6',
      '/manage/dashboards/space/6227c131624b2cf1626dd029',
    ]);
  });

  it('can change the name', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    expect(this.element.querySelector('.row-name p').textContent.trim()).to.equal('GISCollective');

    await click('.row-name .btn-edit');

    await fillIn('.row-name input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedSpace);
    expect(receivedSpace.space.name).to.equal('new name');
  });

  it('can change the description', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    expect(this.element.querySelector('.row-description .alert').textContent.trim()).to.equal(
      'This space has no description',
    );

    await click('.row-description .btn-edit');

    await fillIn('.row-description input', 'new description');
    await click('.btn-submit');

    await waitUntil(() => receivedSpace);
    expect(receivedSpace.space.description).to.equal('new description');
  });

  it('can change the matomo settings', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-analytics .btn-edit');

    await fillIn('.row-analytics .input-matomo-url', 'http://other.example.com');
    await fillIn('.row-analytics .input-side-id', '2');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace);

    expect(receivedSpace.space.matomoUrl).to.equal('http://other.example.com');
    expect(receivedSpace.space.matomoSiteId).to.equal('2');
  });

  it('does not show the is default flag', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    expect(this.element.querySelector('.side-bar .container-group-is-default .btn-switch')).not.to.exist;
  });

  it('does not show the allow domain change flag', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    expect(this.element.querySelector('.side-bar .container-group-allow-domain-change .btn-switch')).not.to.exist;
  });

  it('can change the is public flag', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(this.element.querySelector('.side-bar .container-group-is-public .btn-switch').checked).to.equal(true);

    await click(this.element.querySelector('.side-bar .container-group-is-public .btn-switch'));

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.visibility.isPublic).to.equal(false);
  });

  it('can change the team', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');
    expect(this.element.querySelector('.btn-show-all-teams')).not.to.exist;

    expect(this.element.querySelector('.value.editing select').value).to.equal('61292c4c7bdf9301008fd7b6');

    this.element.querySelector('.value.editing select').value = '000000000000000000000001';
    await triggerEvent('.value.editing select', 'change');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.visibility.team).to.equal('000000000000000000000001');
  });

  it('can change the logo', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-logo .btn-edit');

    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-logo input[type='file']", 'change', {
      files: [blob],
    });

    await click('.row-logo .btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Space', id: space._id },
      data: {},
      disableOptimization: false,
    });
    expect(receivedSpace.space.logo).to.equal('1');
  });

  it('can change the square logo', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-square-logo .btn-edit');

    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-square-logo input[type='file']", 'change', {
      files: [blob],
    });

    await click('.row-square-logo .btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Space', id: space._id },
      data: {},
      disableOptimization: false,
    });
    expect(receivedSpace.space.logoSquare).to.equal('1');
  });

  it('can change the cover', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-cover .btn-edit');

    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });

    await click('.row-cover .btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Space', id: space._id },
      data: {},
      disableOptimization: false,
    });
    expect(receivedSpace.space.cover).to.equal('1');
  });

  it('can change the default models', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-default-models .btn-edit');

    this.element.querySelector('.model-value select').value = mapData._id;
    await triggerEvent('.model-value select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.defaultModels).to.deep.equal({
      map: '5ca89e37ef1f7e010007f54c',
      campaign: '',
      calendar: '',
      newsletter: '',
    });
  });

  it('can change the attributions', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-attributions .btn-edit');
    await click('.row-attributions .btn-add');

    await typeIn('.row-attributions .input-text', 'some text');
    await typeIn('.row-attributions .input-url', 'some url');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.attributions).to.deep.equal([{ name: 'some text', url: 'some url' }]);
  });

  it('can change the landing page', async function () {
    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-landing-page .btn-edit');

    this.element.querySelector('.row-landing-page select').value = '000000000000000000000002';
    await triggerEvent('.row-landing-page select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.landingPageId).to.deep.equal('000000000000000000000002');
  });

  it('can change the color palette', async function () {
    space.colorPalette = {
      red: '#cc0033',
    };

    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    expect(this.element.querySelector('.color-red').textContent.trim()).to.equal('#cc0033');

    await click('.row-color-palette .btn-edit');

    await fillIn('.input-color-red', '#dd1144');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.colorPalette).to.deep.equal({
      blue: '#3961d0',
      indigo: '#2a1a68',
      purple: '#663eff',
      pink: '#ff3e76',
      red: '#dd1144',
      orange: '#e55b06',
      yellow: '#e5ca06',
      green: '#107353',
      teal: '#5d7388',
      cyan: '#159ea7',
      customColors: []
    });
  });

  it('can change the font styles', async function () {
    space.fontStyles = {
      h1: ['ff-anton', 'fw-light', 'text-size-18'],
    };

    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    expect(this.element.querySelector('.row-font-styles .style-item')).to.exist;
    await click('.row-font-styles .btn-edit');

    const items = [...this.element.querySelectorAll('.row-font-styles .style-item')];

    expect(items[0].querySelector('.input--ff').value).to.equal('anton');

    items[0].querySelector('.input--ff').value = 'oswald';
    await triggerEvent('.input--ff', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.fontStyles).to.deep.equal({
      h1: ['fw-light', 'text-size-18', 'ff-oswald'],
      h2: [],
      h3: [],
      h4: [],
      h5: [],
      h6: [],
      paragraph: [],
    });
  });

  it('can change the search options', async function () {
    space.fontStyles = {
      h1: ['ff-anton', 'fw-light', 'text-size-18'],
    };

    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-search-options .btn-edit');

    this.element.querySelector('.value-features').value = 'false';
    await triggerEvent(this.element.querySelector('.value-features'), 'change');

    this.element.querySelector('.value-maps').value = 'true';
    await triggerEvent(this.element.querySelector('.value-maps'), 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.searchOptions).to.deep.equal({
      features: false,
      maps: true,
      campaigns: false,
      iconSets: false,
      teams: false,
      icons: false,
      geocodings: false,
      events: false,
      articles: false,
    });
  });

  it('can change the domain', async function () {
    space.domain = 'giscollective.com';
    space.allowDomainChange = true;

    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-domain .btn-edit');

    await fillIn('.row-domain .input-domain', 'new-domain.com');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.domain).to.equal('new-domain.com');
  });

  it('can change the redirects', async function () {
    space.redirects = {
      a: {
        url: 'http://giscollective.com',
      },
    };

    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-redirects .btn-edit');

    await fillIn('.redirect-item input', 'b');
    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.redirects).to.deep.equal({ b: { url: 'http://giscollective.com' } });
  });

  it('triggers the verify action when the domain verify button is pressed', async function () {
    let called = '';
    server.post(`/mock-server/spaces/${space._id}/verify`, () => {
      called = true;
      return [201, {}, ''];
    });

    space.domain = 'giscollective.com';
    space.allowDomainChange = true;

    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);
    await server.waitAllRequests();

    await click('.row-domain .btn-verify');

    expect(called).to.equal(true);
  });

  it('can change the default locale', async function () {
    space.locale = 'en-us';

    authenticateSession();
    await visit(`/manage/spaces/edit/${space._id}`);

    const service = this.owner.lookup('service:user');
    service.languages = [
      { name: 'English', locale: 'en-us' },
      { name: 'Romana', locale: 'ro-ro' },
      { name: 'French', locale: 'fr-fr' },
    ];

    await wait(100);

    await click('.row-locale .btn-edit');

    this.element.querySelector('.row-locale select').value = 'ro-ro';
    await triggerEvent('.row-locale select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedSpace != null, { timeout: 3000 });

    expect(receivedSpace.space.locale).to.deep.equal('ro-ro');
  });

  describe('when the authenticated user is an administrator', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('can toggle the is default field', async function () {
      authenticateSession();
      await visit(`/manage/spaces/edit/${space._id}`);
      await server.waitAllRequests();

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(this.element.querySelector('.side-bar .container-group-is-default .btn-switch').checked).to.equal(true);

      await click(this.element.querySelector('.side-bar .container-group-is-default .btn-switch'));

      await waitUntil(() => receivedSpace != null, { timeout: 3000 });

      expect(receivedSpace.space.visibility.isDefault).to.equal(false);
    });

    it('can toggle the allowDomainChange field', async function () {
      authenticateSession();
      await visit(`/manage/spaces/edit/${space._id}`);
      await server.waitAllRequests();

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(this.element.querySelector('.side-bar .container-group-allow-domain-change .btn-switch').checked).to.equal(
        false,
      );

      await click(this.element.querySelector('.side-bar .container-group-allow-domain-change .btn-switch'));

      await waitUntil(() => receivedSpace != null, { timeout: 3000 });

      expect(receivedSpace.space.allowDomainChange).to.equal(true);
    });

    it('can toggle the isTemplate field', async function () {
      authenticateSession();
      await visit(`/manage/spaces/edit/${space._id}`);
      await server.waitAllRequests();

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(this.element.querySelector('.side-bar .container-group-is-template .btn-switch').checked).to.equal(false);

      await click(this.element.querySelector('.side-bar .container-group-is-template .btn-switch'));

      await waitUntil(() => receivedSpace != null, { timeout: 3000 });

      expect(receivedSpace.space.isTemplate).to.equal(true);
    });

    it('can change to any team', async function () {
      authenticateSession();
      await visit(`/manage/spaces/edit/${space._id}`);
      await server.waitAllRequests();

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click('.container-group-team .btn-edit');
      click('.btn-show-all-teams');

      expect(this.element.querySelector('.btn-show-all-teams')).to.exist;

      this.element.querySelector('.value.editing select').value = '61292c4c7bdf9301008fd7b6';
      await triggerEvent('.value.editing select', 'change');

      await waitUntil(() => receivedSpace != null, { timeout: 3000 });

      expect(receivedSpace.space.visibility.team).to.equal('61292c4c7bdf9301008fd7b6');
    });
  });
});
