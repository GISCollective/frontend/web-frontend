/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorButtonStyleComponent extends Component {
  @tracked _style;

  panel = [
    {
      type: "button-style",
    },
    {
      type: "text-style"
    },
    {
      type: "width-percentage",
    },
    {
      type: "margin",
    },
  ];

  get value() {
    return this.args.value ?? {};
  }

  get style() {
    return this._style ?? this.value ?? {};
  }

  triggerChange() {
    this.args.onChange?.(this.style);
  }

  @action
  changeStyleClasses(value) {
    this._style = this.style;
    this._style.classes = value.classes;

    this.triggerChange();
  }
}
