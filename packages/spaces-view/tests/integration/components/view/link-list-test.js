/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/link-list', function (hooks) {
  setupRenderingTest(hooks);
  let spaceData;
  let store;
  let server;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage('000000000000000000000002');

    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    const space = await store.findRecord('space', spaceData._id);
    space.getCachedPagesMap = () => ({
      page1: '000000000000000000000001',
      page2: '000000000000000000000002',
      page3: '000000000000000000000003',
    });

    this.set('space', space);
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::LinkList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the title when it is set', async function () {
    this.set('value', {
      data: {
        title: {
          color: 'green',
          classes: ['display-3', 'fw-bold'],
          text: 'some title',
        },
      },
    });

    await render(hbs`<View::LinkList @value={{this.value}} />`);

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'some title',
    );

    expect(this.element.querySelector('h1')).to.have.class('display-3');
    expect(this.element.querySelector('h1')).to.have.class('fw-bold');
    expect(this.element.querySelector('h1')).to.have.class('text-green');
  });

  it('does not render the title when it is set', async function () {
    this.set('value', {
      data: {
        title: {
          color: 'green',
          classes: ['display-3', 'fw-bold'],
          text: '',
        },
      },
    });

    await render(hbs`<View::LinkList @value={{this.value}} />`);

    expect(this.element.querySelector('h1')).not.to.exist;
  });

  it('render the title in edit mode when it is not set', async function () {
    this.set('value', {
      data: {
        title: {
          color: 'green',
          classes: ['display-3', 'fw-bold'],
          text: '',
        },
      },
    });

    await render(
      hbs`<View::LinkList @isEditor={{true}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('h1')).to.have.textContent('');
  });

  it('renders two links when they are set', async function () {
    this.set('value', {
      data: {
        linkList: [
          { name: 'name', link: { pageId: '000000000000000000000001' } },
          { name: 'new name', link: { pageId: '000000000000000000000002' } },
        ],
      },
    });

    await render(
      hbs`<View::LinkList @value={{this.value}} @space={{this.space}} />`,
    );

    const links = this.element.querySelectorAll('a');

    expect(links[0].textContent.trim()).to.equal('name');
    expect(links[1].textContent.trim()).to.equal('new name');
    expect(links[0]).to.have.attribute('data-link-path', '/page1');
    expect(links[1]).to.have.attribute('data-link-path', '/page2');
    expect(links[1]).not.to.have.attribute('target');
  });

  it('renders a link with the target _blank', async function () {
    this.set('value', {
      data: {
        linkList: [
          {
            name: 'name',
            link: { pageId: '000000000000000000000001' },
            newTab: true,
          },
        ],
      },
    });

    await render(
      hbs`<View::LinkList @value={{this.value}} @space={{this.space}} />`,
    );

    const links = this.element.querySelectorAll('a');

    expect(links[0]).to.have.attribute('target', '_blank');
  });

  it('renders the link style properties', async function () {
    this.set('value', {
      data: {
        link: {
          color: 'green',
          classes: ['fw-bold'],
        },
        linkList: [
          { name: 'name', link: { pageId: '000000000000000000000001' } },
          { name: 'new name', link: { pageId: '000000000000000000000002' } },
        ],
      },
    });

    await render(
      hbs`<View::LinkList @value={{this.value}} @space={{this.space}} />`,
    );

    const link = this.element.querySelector('a');

    expect(link).to.have.class('fw-bold');
    expect(link).to.have.class('text-green');
  });
});
