/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { guidFor } from "@ember/object/internals";

export default class InputOptionsModelItemComponent extends Component {
  elementId = `model-item-${guidFor(this)}`;

  @service store;
  @service intl;

  @tracked _useSelectedModel = null;
  @tracked _useDefaultModel = null;
  @tracked _model = null;
  @tracked _id;
  @tracked picture;

  modelList = ["article", "map", "campaign", "icon", "icon-set", "feature", "team", "event", "calendar", "newsletter"];
  defaultModelList = ["map", "campaign", "team", "calendar", "newsletter"];

  yesNoOptions = ["yes", "no"];

  get canUseDefaultModel() {
    if (this.defaultModelList.includes(this.args.preferredModel)) {
      return true;
    }

    if (this.args.preferredModel) {
      return false;
    }

    return this.defaultModelList.includes(this.model);
  }

  get useDefaultModel() {
    if (this.args.preferredModel && !this.canUseDefaultModel) {
      return "no";
    }

    if (this._useDefaultModel !== null) {
      return this._useDefaultModel ? "yes" : "no";
    }

    return this.args.value?.useDefaultModel ? "yes" : "no";
  }

  get canUseSelectedModel() {
    if (this.args.preferredModel) {
      return false;
    }

    if (typeof this.args.canUseSelectedModel == "boolean") {
      return this.args.canUseSelectedModel;
    }

    return true;
  }

  get canSelectName() {
    if (this.useDefaultModel == "yes") {
      return false;
    }

    if (this.args.preferredModel && !this.canUseDefaultModel) {
      return true;
    }

    if (this.canUseDefaultModel && this.useDefaultModel == "yes") {
      return false;
    }

    return this.modelList.includes(this.model);
  }

  get modelOptions() {
    if (Array.isArray(this.args.models)) {
      return this.args.models;
    }

    const list = [...this.modelList];

    if (this.args?.withPicture) {
      list.push("picture");
    }

    return list.map((id) => ({
      id,
      name: this.intl.t(`option-panel.model-name-list.${id}`),
    }));
  }

  get canSelectModel() {
    return this.useSelectedModel != "yes" && !this.args.model && !this.args.preferredModel;
  }

  get useSelectedModel() {
    if (this._useSelectedModel !== null) {
      return this._useSelectedModel ? "yes" : "no";
    }

    return this.args.value?.useSelectedModel ? "yes" : "no";
  }

  get model() {
    if (this.args.preferredModel) {
      return this.args.preferredModel;
    }

    if (this.args.model) {
      return this.args.model;
    }

    if (this._model !== null) {
      return this._model;
    }

    return this.args?.value?.model;
  }

  get team() {
    const visibility = this.args.space?.visibility ?? {};

    if (visibility.isDefault) {
      return null;
    }

    return visibility.teamId;
  }

  get id() {
    if (this._id) {
      return this._id;
    }

    return this.args?.value?.id;
  }

  async triggerChange() {
    let result = {};

    result.useSelectedModel = this.useSelectedModel == "yes";

    if (result.useSelectedModel) {
      return this.args.onChange?.(result);
    }

    result.useDefaultModel = this.useDefaultModel == "yes";
    result.model = this.model;

    if (result.useDefaultModel) {
      return this.args.onChange?.(result);
    }

    result.id = this.id ?? null;

    this.args.onChange?.(result);
  }

  @action
  async loadPicture() {
    if (this.model != "picture" || !this.id) {
      return;
    }

    this.picture = this.store.peekRecord("picture", this.id);

    if (!this.picture) {
      this.picture = await this.store.findRecord("picture", this.id);
    }
  }

  @action
  useDefaultModelChanged(value) {
    this._useDefaultModel = value == "yes";

    this.triggerChange();
  }

  @action
  modelChanged(value) {
    this._model = value.id;
    this.picture = null;
    this.triggerChange();
  }

  @action
  changeSelectedRecord(id) {
    this._id = id;
    this.triggerChange();
  }

  @action
  useSelectedModelChanged(value) {
    this._useSelectedModel = value == "yes";
    this.triggerChange();
  }

  @action
  pictureChanged(index, value) {
    this.picture = value;

    return this.changeSelectedRecord(value.id);
  }
}
