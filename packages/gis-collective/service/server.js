/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
// eslint-disable-next-line no-undef
const FastBootAppServer = require('fastboot-app-server');
// eslint-disable-next-line no-undef
const promBundle = require('express-prom-bundle');
// eslint-disable-next-line no-undef
const client = require('prom-client');

const userAgentGauge = new client.Gauge({
  name: 'user_agent',
  labelNames: ['name'],
  help: 'The user agents seen by the frontend server',
});

const userIpGauge = new client.Gauge({
  name: 'user_ip',
  labelNames: ['name'],
  help: 'The user ip seen by the frontend server',
});

// eslint-disable-next-line no-undef
const apiHost = process.env.apiHost;
// eslint-disable-next-line no-undef
const authHost = process.env.authHost;
// eslint-disable-next-line no-undef
const port = parseInt(process.env.frontendPort) || 80;

function validateEnvHostVar(value, name) {
  if (value) {
    if (value.indexOf('http://') != 0 && value.indexOf('https://') != 0) {
      // eslint-disable-next-line no-console
      console.log(`The protocol "http://" or "https://" is missing from the ENV var "apiHost"`);

      // eslint-disable-next-line no-undef
      process.exit(1);
    }
    // eslint-disable-next-line no-console
    console.log(`Using "${value}" as backend server`);
  } else {
    // eslint-disable-next-line no-console
    console.log(`The ENV var "${name}" is not set! This might cause some problems`);
  }
}

validateEnvHostVar(apiHost, 'apiHost');

function userAgentStats(req, res, next) {
  if (req.path == '/stats') return next();

  const userAgent = req.get('user-agent');
  userAgentGauge.inc({ name: userAgent });

  next();
}

function userIpStats(req, res, next) {
  if (req.path == '/stats') return next();

  userIpGauge.inc({ name: req.ip });

  next();
}

setInterval(function () {
  client.register.resetMetrics();
}, 15000);

const metricsMiddleware = promBundle({
  includeMethod: true,
  includePath: true,
  metricsPath: '/stats',
});

let server = new FastBootAppServer({
  beforeMiddleware: function (app) {
    app.set('trust proxy');

    app.use(userAgentStats);
    app.use(userIpStats);

    app.use(metricsMiddleware);
  },
  distPath: '.',
  buildSandboxGlobals(defaultGlobals) {
    // Optional - Make values available to the Ember app running in the FastBoot server, e.g. "MY_GLOBAL" will be available as "GLOBAL_VALUE"
    return Object.assign({}, defaultGlobals, { apiHost });
  },
  gzip: false, // Optional - Enables gzip compression.
  port: port, // Optional - Sets the port the server listens on (defaults to the PORT env var or 3000).
});

server.start();
