/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | manage/revision-list", function (hooks) {
  setupRenderingTest(hooks);

  it("renders a message when the list is empty", async function () {
    this.set("value", []);

    await render(hbs`<Manage::RevisionList @value={{this.value}}/>`);

    const revisionElements = this.element.querySelectorAll(".revision-item");
    expect(revisionElements).to.have.length(0);

    expect(this.element.querySelector(".no-revisions-message").textContent.trim()).to.equal("There are no changes");
  });

  it("renders the list of revisions", async function () {
    this.set("value", [
      {
        time: "2022-05-05T00:53:52Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f52",
        id: "627304009d2d9b8820ec2f52",
      },
      {
        time: "2022-05-05T00:53:53Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f53",
        id: "627304009d2d9b8820ec2f53",
      },
      {
        time: "2022-05-05T00:53:54Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f54",
        id: "627304009d2d9b8820ec2f54",
      },
    ]);

    await render(hbs`<Manage::RevisionList @value={{this.value}}/>`);

    const revisionElements = this.element.querySelectorAll(".revision-item");
    expect(revisionElements).to.have.length(3);
  });

  it("triggers an onSelect when a revision is clicked", async function () {
    this.set("value", [
      {
        time: "2022-05-05T00:53:52Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f52",
        id: "627304009d2d9b8820ec2f52",
      },
      {
        time: "2022-05-05T00:53:53Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f53",
        id: "627304009d2d9b8820ec2f53",
      },
      {
        time: "2022-05-05T00:53:54Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f54",
        id: "627304009d2d9b8820ec2f54",
      },
    ]);

    let value;
    this.set("onSelect", (v) => {
      value = v;
    });

    await render(hbs`<Manage::RevisionList @onSelect={{this.onSelect}} @value={{this.value}}/>`);

    const revisionElements = this.element.querySelectorAll(".revision-item");
    await click(revisionElements[1]);

    expect(value).to.equal("627304009d2d9b8820ec2f53");
  });

  it("triggers an onSelect with null when latest version button is pressed", async function () {
    this.set("value", [
      {
        time: "2022-05-05T00:53:52Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f52",
        id: "627304009d2d9b8820ec2f52",
      },
      {
        time: "2022-05-05T00:53:53Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f53",
        id: "627304009d2d9b8820ec2f53",
      },
      {
        time: "2022-05-05T00:53:54Z",
        url: "/mock-server/layouts/000000000000000000000001/revisions/627304009d2d9b8820ec2f54",
        id: "627304009d2d9b8820ec2f54",
      },
    ]);

    let value;
    this.set("onSelect", (v) => {
      value = v;
    });

    await render(hbs`<Manage::RevisionList @onSelect={{this.onSelect}} @value={{this.value}}/>`);

    await click(".latest-version");

    expect(value).to.equal(null);
  });
});
