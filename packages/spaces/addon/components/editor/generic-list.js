/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { A } from "models/lib/array";

export default class EditorGenericListComponent extends Component {
  get listCount() {
    return this.args.value?.length ?? 0;
  }

  @action
  changeListCount(value) {
    if (isNaN(value)) {
      return;
    }

    let result = A(this.args.value);

    if (value < this.args.value?.length ?? 0) {
      result = A(result.slice(0, value));
    }

    while (result.length < parseInt(value)) {
      result.push(null);
    }

    this.args.onChange(result);
  }
}
