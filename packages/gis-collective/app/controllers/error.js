/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
export default class ErrorController extends Controller {
  @service lastError;
  @service session;
  @service router;

  @action
  async login() {
    console.log("auth INVALIDATE SESSION ON ERROR");
    await this.session.invalidate();
    this.transitionTo('login');
  }
}
