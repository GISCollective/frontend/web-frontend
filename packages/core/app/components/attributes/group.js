/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { Popover } from 'bootstrap';

export default class AttributesGroupComponent extends Component {
  get className() {
    if (!this.args.group || !this.args.group.key) {
      return 'invalid';
    }

    return this.args.group.key.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }

  @action
  createValidationPopover(element) {
    this.popover = new Popover(element, {
      trigger: 'hover',
      content: this.args.group.validationMessage,
      delay: { show: 100, hide: 3000 },
    });
  }

  @action
  remove() {
    if (this.args.remove) {
      this.args.remove(...arguments);
    }
  }
}
