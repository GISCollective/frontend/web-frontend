import { module } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import { expect } from 'chai';

module('Integration | Component | input/default-models', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let mapData;
  let campaignData;
  let calendarData;
  let newsletterData;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.beforeEach(function() {
    mapData = server.testData.storage.addDefaultMap();
    campaignData = server.testData.storage.addDefaultCampaign();
    calendarData = server.testData.storage.addDefaultCalendar();
    newsletterData = server.testData.storage.addDefaultNewsletter();

    server.testData.storage.addDefaultMap('1');
    server.testData.storage.addDefaultCampaign('2');
    server.testData.storage.addDefaultCalendar('3');
    server.testData.storage.addDefaultNewsletter('4');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders empty fields by default', async function (assert) {
    await render(hbs`<Input::DefaultModels />`);

    const selects = [...this.element.querySelectorAll('select')].map((a) => a.value);

    expect(selects).to.deep.equal(['', '', '', '']);
  });

  it('renders the values when they are set', async function (assert) {
    this.set('value', {
      map: mapData._id,
      campaign: campaignData._id,
      calendar: calendarData._id,
      newsletter: newsletterData._id,
    });

    this.set('team', { id: '123' });

    server.history = [];

    await render(hbs`<Input::DefaultModels @value={{this.value}} @team={{this.team}} />`);
    const selects = [...this.element.querySelectorAll('select')].map((a) => a.value);

    expect(selects).to.deep.equal([mapData._id, campaignData._id, calendarData._id, newsletterData._id]);
    expect(server.history).to.contain('GET /mock-server/campaigns?team=123');
    expect(server.history).to.contain('GET /mock-server/calendars?team=123');
    expect(server.history).to.contain('GET /mock-server/maps?team=123');
    expect(server.history).to.contain('GET /mock-server/newsletters?team=123');
  });

  it('renders only the provided models when set', async function (assert) {
    this.set('value', {});
    this.set('team', { id: '123' });
    this.set('models', ['map', 'calendar']);

    await render(hbs`<Input::DefaultModels @value={{this.value}} @team={{this.team}} @models={{this.models}}/>`);
    const labels = [...this.element.querySelectorAll('label')].map((a) => a.textContent.trim());
    const selects = [...this.element.querySelectorAll('select')];

    expect(labels).to.deep.equal(['map', 'calendar']);
    expect(selects).to.have.length(2);
  });

  it('can change the values', async function (assert) {
    this.set('value', {
      map: mapData._id,
      campaign: campaignData._id,
      calendar: calendarData._id,
      newsletter: newsletterData._id,
    });

    this.set('team', { id: '123' });
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::DefaultModels @value={{this.value}} @team={{this.team}} @onChange={{this.change}} />`);

    const selects = [...this.element.querySelectorAll('select')];

    selects[0].value = '1';
    selects[1].value = '2';
    selects[2].value = '3';
    selects[3].value = '4';
    await triggerEvent(selects[0], 'change');
    await triggerEvent(selects[1], 'change');
    await triggerEvent(selects[2], 'change');
    await triggerEvent(selects[3], 'change');

    expect(value.toJSON()).to.deep.equal({ map: '1', campaign: '2', calendar: '3', newsletter: '4' });
  });

  it('can remove a value', async function (assert) {
    this.set('value', {
      map: mapData._id,
      campaign: campaignData._id,
      calendar: calendarData._id,
      newsletter: newsletterData._id,
    });

    this.set('team', { id: '123' });
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::DefaultModels @value={{this.value}} @team={{this.team}} @onChange={{this.change}} />`);

    const selects = [...this.element.querySelectorAll('.btn-remove')];

    await click(selects[0]);
    await click(selects[1]);
    await click(selects[2]);
    await click(selects[3]);

    expect(value.toJSON()).to.deep.equal({ map: '', campaign: '', calendar: '', newsletter: '' });
  });
});
