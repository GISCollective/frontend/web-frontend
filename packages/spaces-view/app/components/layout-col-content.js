/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { LayoutCol } from 'models/transforms/layout-row-list';
import { service } from '@ember/service';

export default class LayoutColContentComponent extends Component {
  get col() {
    if (!this.args.col) {
      return new LayoutCol({ name: this.defaultColName });
    }

    const column = this.args.col;

    if (!column.name) {
      return new LayoutCol({ ...column.toJSON(), name: this.defaultColName });
    }

    return column;
  }

  get defaultColName() {
    return this.args.index;
  }

  get hasManyComponents() {
    return this.col.componentCount > 1;
  }

  get cols() {
    const column = this.col;

    const result = [column];

    for (let i = 1; i < column.componentCount; i++) {
      result.push(new LayoutCol({ name: `${column.name}.${i}` }));
    }

    return result;
  }

  @action
  select() {
    if (this.hasManyComponents) {
      return;
    }

    return this.args.onSelect?.({
      type: 'col',
      title: `column ${this.col.name}`,
      data: this.col,
    });
  }

  @action
  selectCol(col) {
    if (!this.args.selectable) {
      return;
    }

    return this.args.onSelect?.({
      type: 'col',
      title: `column ${col.name}`,
      data: col,
    });
  }
}
