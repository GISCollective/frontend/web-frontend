/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, render, waitFor, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import PageElements from 'core/test-support/page-elements';

describe ('Integration | Component | map/layer/selectable-vector-tiles',  function (hooks) {
  setupRenderingTest(hooks, { server: true });
  let map;
  let iconData;

  hooks.beforeEach(function () {
    this.set("ready", (m) => {
      map = m;
    });

    iconData = this.server.testData.storage.addDefaultIcon("5ca7bfc0ecd8490100cab980");
  });

  it('shows a hover element when a feature is hovered', async function () {
    await render(hbs`<Map::Plain @onReady={{this.ready}} as |map|>
      <Map::Layer::SelectableVectorTiles @map={{map}} @url="/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c"/>
    </Map::Plain>`);

    const vtElement = this.element.querySelector('.vector-tiles');
    expect(vtElement.textContent.trim()).to.equal("/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c");

    const view = map.getView();
    view.setZoom(16);
    view.setCenter([1502287.3822740193, 6886844.872352015]);

    expect(this.element.querySelector(".hover-overlay")).not.to.exist;

    vtElement.hoverFeature({
      getGeometry: () => ({
        getExtent: () => [1502287.3822740193, 6886844.872352015, 1502287.3822740193, 6886844.872352015]
      }),
      getProperties: () => ({
        _id: "5ca8c2ecef1f7e01000886aa",
        name: "Walking path in Plänterwald",
        icon: iconData._id,
      }),
    });

    await waitFor(".hover-overlay");
    await waitFor("img");

    expect(this.element.querySelector(".hover-overlay")).to.exist;
  });

  it('shows an active element when a feature is selected', async function () {
    await render(hbs`<Map::Plain @onReady={{this.ready}} as |map|>
      <Map::Layer::SelectableVectorTiles @map={{map}} @url="/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c"/>
    </Map::Plain>`);

    const vtElement = this.element.querySelector('.vector-tiles');
    expect(vtElement.textContent.trim()).to.equal("/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c");

    const view = map.getView();
    view.setZoom(16);
    view.setCenter([1502287.3822740193, 6886844.872352015]);

    expect(this.element.querySelector(".hover-overlay")).not.to.exist;

    vtElement.selectFeature({
      getGeometry: () => ({
        getExtent: () => [1502287.3822740193, 6886844.872352015, 1502287.3822740193, 6886844.872352015]
      }),
      getProperties: () => ({
        _id: "5ca8c2ecef1f7e01000886aa",
        name: "Walking path in Plänterwald",
        icon: iconData._id,
      }),
    });

    await waitFor(".hover-overlay");
    await waitFor("img");

    expect(this.element.querySelector(".hover-icon.is-active")).to.exist;
  });

  it('hides the hover element when a feature is not hovered', async function () {
    await render(hbs`<Map::Plain @onReady={{this.ready}} as |map|>
      <Map::Layer::SelectableVectorTiles @map={{map}} @url="/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c"/>
    </Map::Plain>`);

    const vtElement = this.element.querySelector('.vector-tiles');
    expect(vtElement.textContent.trim()).to.equal("/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c");

    vtElement.hoverFeature({
      getGeometry: () => ({
        getExtent: () => [1502287.3822740193, 6886844.872352015, 1502287.3822740193, 6886844.872352015]
      }),
      getProperties: () => ({
        _id: "5ca8c2ecef1f7e01000886aa",
        name: "Walking path in Plänterwald",
        icon: iconData._id,
      }),
    });

    await waitFor(".hover-overlay");

    vtElement.hoverFeature(null);

    await waitUntil(() => !this.element.querySelector(".hover-overlay"));

    expect(this.element.querySelector(".hover-overlay")).not.to.exist;
  });

  it('triggers onSelect when a feature is selected', async function () {
    let selected;

    this.set("select", (v) => {
      selected = v;
    });

    await render(hbs`<Map::Plain @onReady={{this.ready}} as |map|>
      <Map::Layer::SelectableVectorTiles @map={{map}} @onSelect={{this.select}} @url="/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c"/>
    </Map::Plain>`);

    const vtElement = this.element.querySelector('.vector-tiles');
    expect(vtElement.textContent.trim()).to.equal("/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c");

    const feature = {
      getGeometry: () => ({
        getExtent: () => [1502287.3822740193, 6886844.872352015, 1502287.3822740193, 6886844.872352015]
      }),
      getProperties: () => ({
        _id: "5ca8c2ecef1f7e01000886aa",
        name: "Walking path in Plänterwald",
        icon: iconData._id,
      }),
    };

    vtElement.selectFeature(feature);

    await waitFor("img");

    expect(selected).to.equal(feature);
  });

  it('triggers onSelect when the hover element is clicked', async function () {
    let selected;

    this.set("select", (v) => {
      selected = v;
    });

    await render(hbs`<Map::Plain @onReady={{this.ready}} as |map|>
      <Map::Layer::SelectableVectorTiles @map={{map}} @onSelect={{this.select}} @url="/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c"/>
    </Map::Plain>`);

    const vtElement = this.element.querySelector('.vector-tiles');
    expect(vtElement.textContent.trim()).to.equal("/api-v1/tiles/{z}/{x}/{y}?format=vt&map=5ca89e37ef1f7e010007f54c");

    const feature = {
      getGeometry: () => ({
        getExtent: () => [1502287.3822740193, 6886844.872352015, 1502287.3822740193, 6886844.872352015]
      }),
      getProperties: () => ({
        _id: "5ca8c2ecef1f7e01000886aa",
        name: "Walking path in Plänterwald",
        icon: iconData._id,
      }),
    };

    vtElement.hoverFeature(feature);

    await waitFor(".hover-icon");
    await click(".hover-icon");

    expect(selected).to.equal(feature);
  });
});
