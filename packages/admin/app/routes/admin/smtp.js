/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '../base/authenticated-admin-route';
import { service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminSmtpRoute extends Route {
  @service preferences;

  model() {
    const smtpFields = ['authType', 'connectionType', 'host', 'port', 'password', 'username', 'from'];

    const mailFields = ['type'];
    const mailerSendFields = ['key', 'from'];

    const items = {
      user: this.store.queryRecord('user', { me: true }),
    };

    smtpFields.forEach((key) => {
      items[key] = this.preferences.requestPreference(`secret.smtp.${key}`);
    });

    mailFields.forEach((key) => {
      items[`mail_${key}`] = this.preferences.requestPreference(`secret.mail.${key}`);
    });

    mailerSendFields.forEach((key) => {
      items[`mailersend_${key}`] = this.preferences.requestPreference(`secret.mailersend.${key}`);
    });

    return hash(items);
  }
}
