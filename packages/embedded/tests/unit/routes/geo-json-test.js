import { module, test } from 'qunit';
import { setupTest } from 'embedded/tests/helpers';

module('Unit | Route | geo-json', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:geo-json');
    assert.ok(route);
  });
});
