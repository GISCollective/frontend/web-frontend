import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageImportEmailsComponent extends Component {
  @service intl;
  @service notifications;

  @tracked mode = 'file-upload';
  @tracked value;
  @tracked replace = false;

  get hasNoValue() {
    return typeof this.value != 'string' && !Array.isArray(this.value);
  }

  get isFileUpload() {
    return this.mode == 'file-upload';
  }

  get isCopyPaste() {
    return this.mode == 'copy-paste';
  }

  @action
  change(value) {
    this.value = value;
  }

  @action
  setMode(mode) {
    this.mode = mode;
  }

  @action
  async onSave() {
    if (this.replace) {
      await this.notifications.ask({
        title: this.intl.t(`Replace emails`, { size: 1 }),
        question: this.intl.t('message-replace-emails-confirmation'),
        answers: this.answers,
      });
    }

    await this.args.newsletter.importEmails(this.value, this.replace);

    this.args.onSave?.();
  }
}
