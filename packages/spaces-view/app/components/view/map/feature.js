/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';
import Component from '../base/source-record';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class PageColMapFeatureComponent extends Component {
  @service preferences;

  @tracked maps;
  @tracked map;
  @tracked baseMaps;
  @tracked baseMap;

  get disableFullscreen() {
    return !this.args.value?.data?.elements?.fullscreen;
  }

  get disableZoom() {
    return !this.args.value?.data?.elements?.zoom;
  }

  get disableDownload() {
    return !this.args.value?.data?.elements?.download;
  }

  get disableMapLicense() {
    return !this.args.value?.data?.elements?.mapLicense;
  }

  get disableBaseMapLicense() {
    return !this.args.value?.data?.elements?.baseMapLicense;
  }

  get classes() {
    return this.args.value?.data?.border?.classes ?? [];
  }

  get geometry() {
    if (this.record?.get) {
      return this.record.get('geometry');
    }

    return this.record?.geometry;
  }

  get extent() {
    if (!this.maps?.length) {
      return null;
    }

    return this.maps[0].area;
  }

  get gpxUrl() {
    const id = this.record?.get ? this.record.get('id') : this.record?.id;

    return `${this.preferences.apiUrl}/features/${id}?format=gpx`;
  }

  get isLine() {
    const position = this.record?.get
      ? this.record.get('position')
      : this.record?.position;

    return (
      position?.type == 'LineString' || position?.type == 'MultiLineString'
    );
  }

  @action
  async setup() {
    const record = await this.record;

    await super.setup();

    this.maps = (await record?.maps) ?? [];

    if (this.maps?.length) {
      this.map = await this.maps[0];

      await this.map.baseMaps.fetch();

      this.baseMaps = await this.map.baseMaps.list;
      this.baseMap = this.baseMaps[0];
    }
  }
}
