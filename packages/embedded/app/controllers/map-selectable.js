import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class MapSelectableController extends Controller {
  @service embeddedClient;
  @service position;

  get id() {
    return this.embeddedClient.value?.mapId;
  }

  @action
  handleSelection(idList) {
    this.embeddedClient.sendMessage('selection', idList);
  }
}
