/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import {
  click,
  render,
  triggerEvent,
  waitFor,
  waitUntil,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';

describe('Integration | Component | view/map/map-view', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let map;
  let baseMap1;
  let baseMap2;
  let mapRecord;
  let feature;
  let mapPage;
  let spaceData;
  let receivedMetric;
  let loadingService;

  hooks.before(function () {
    server = new TestServer();
    map = server.testData.storage.addDefaultMap('1');
    feature = server.testData.storage.addDefaultFeature();
    baseMap1 = server.testData.storage.addDefaultBaseMap('1');
    baseMap2 = server.testData.storage.addDefaultBaseMap('2');
    server.testData.storage.addDefaultPicture();
    spaceData = server.testData.storage.addDefaultSpace();
    mapPage = server.testData.storage.addDefaultPage();

    baseMap1.attributions = [
      {
        name: '© OpenStreetMap contributors',
        url: 'https://www.openstreetmap.org/copyright',
      },
    ];

    baseMap1['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    baseMap2['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    map.id = map._id;
    map.baseMaps.list = [baseMap1._id, baseMap2._id];
    map.license = {
      name: 'CC BY-NC 4.0',
      url: 'https://creativecommons.org/licenses/by-nc/4.0/',
    };

    receivedMetric = null;
    server.post(`/mock-server/metrics`, (request) => {
      receivedMetric = JSON.parse(request.requestBody);
      receivedMetric.metric._id = '1';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedMetric),
      ];
    });

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicFeatures: 220,
        totalContributors: 33,
      }),
    ]);
  });

  hooks.beforeEach(async function () {
    loadingService = this.owner.lookup('service:loading');
    mapRecord = await this.store.findRecord('map', '1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders the map layers when it is using a map source', async function (a) {
    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
        },
        source: {
          useSelectedModel: false,
          model: 'map',
          id: '1',
        },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
    );

    const olMap = this.element.querySelector('.map').olMap;
    const layers = olMap.getLayers().getArray();
    expect(layers).to.have.length(2);

    expect(
      this.element.querySelector('.map-layer-base').textContent.trim(),
    ).to.equal(baseMap1._id);
    expect(
      this.element.querySelector('.vector-tiles').textContent.trim(),
    ).to.equal('/api-v1/tiles/{z}/{x}/{y}?format=vt&map=1');
  });

  it('renders the world map layers when it does not have a source', async function () {
    this.set('value', {
      data: {
        map: {
          mode: 'show all features',
        },
        source: {},
      },
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
    );

    const olMap = this.element.querySelector('.map').olMap;
    const layers = olMap.getLayers().getArray();
    expect(layers).to.have.length(2);

    expect(
      this.element.querySelector('.map-layer-base').textContent.trim(),
    ).to.equal(baseMap1._id);
    expect(
      this.element.querySelector('.vector-tiles').textContent.trim(),
    ).to.equal('/api-v1/tiles/{z}/{x}/{y}?format=vt');
  });

  it('shows the map license', async function () {
    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
        },
        source: { useSelectedModel: false, model: 'map', id: '1' },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
    );

    const links = this.element.querySelectorAll('.attributions a');
    expect(links).to.have.length(2);
    expect(links[0]).to.have.attribute(
      'href',
      'https://creativecommons.org/licenses/by-nc/4.0/',
    );
    expect(links[0].textContent.trim()).to.equal('CC BY-NC 4.0');
    expect(links[1]).to.have.attribute(
      'href',
      'https://www.openstreetmap.org/copyright',
    );
    expect(links[1].textContent.trim()).to.equal(
      '© OpenStreetMap contributors',
    );
  });

  it('pushes a metric when the map is loaded', async function (a) {
    let tracking = this.owner.lookup('service:tracking');

    let page;
    let mapId;

    tracking.registerTracker('test', {
      addMapView(a, b) {
        page = a;
        mapId = b;
      },
    });

    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
        },
        source: { useSelectedModel: false, model: 'map', id: '1' },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
    );

    expect(mapId).to.equal('1');
  });

  it('renders the map title when it is set', async function (a) {
    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
          showTitle: true,
        },
        source: { useSelectedModel: false, model: 'map', id: '1' },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.title-container')).to.exist;
  });

  it('does not render the map title when it is not set', async function (a) {
    let mapId;

    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
          showTitle: false,
        },
        source: { useSelectedModel: false, model: 'map', id: '1' },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    await render(
      hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.title-container')).not.to.exist;
  });

  it('renders the "browse-map" button when enableFullMapView is true', async function (a) {
    let mapId;

    this.set('value', {
      data: {
        map: {
          mode: 'use a map',
          enableFullMapView: true,
        },
        source: { useSelectedModel: false, model: 'map', id: '1' },
      },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: mapRecord,
    });

    const space = await this.store.findRecord('space', spaceData._id);
    this.set('space', space);

    await render(
      hbs`<View::Map::MapView @space={{this.space}} @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.btn-browse-map-view')).to.exist;
    expect(
      this.element.querySelector('.btn-browse-map-view'),
    ).to.have.attribute('href', '/browse/maps/1/map-view');
  });

  describe('feature hover', function () {
    it('shows the mouse pointer when a feature is hovered and featureHover is "mouse pointer"', async function () {
      this.set('value', {
        data: {
          map: {
            featureHover: 'mouse pointer',
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
      );

      const vtElement = this.element.querySelector('.vector-tiles');

      vtElement.hoverFeature({
        getProperties: () => ({
          _id: feature._id,
          visibility: '1',
          canView: 'true',
        }),
      });

      await PageElements.wait(100);

      expect(this.element.querySelector('.cursor-pointer')).to.exist;
    });

    it('shows nothing when a feature is hovered and featureHover is "do nothing"', async function () {
      this.set('value', {
        data: {
          map: {
            featureHover: 'do nothing',
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}}/>`,
      );

      const vtElement = this.element.querySelector('.vector-tiles');

      vtElement.hoverFeature({
        getProperties: () => ({
          _id: feature._id,
          visibility: '1',
          canView: 'true',
        }),
      });

      await PageElements.wait(100);

      expect(this.element.querySelector('.cursor-pointer')).not.to.exist;
    });
  });

  describe('viewport', function () {
    it('updates the page state when the viewport is updated', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      const ol = this.element.querySelector('.ol-map-plain').olMap;

      ol.getView().setZoom(5);
      ol.getView().setCenter([100000, 25000]);

      await waitUntil(() => value?.indexOf('@2') == -1);

      expect(value).to.equal(
        'lon@0.8983152841195214@lat@0.2245782459764314@z@5',
      );
    });

    it('sets the view properties from the state, when they are set', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});
      this.set('state', 'lon@1@lat@2@z@6');

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      const ol = this.element.querySelector('.ol-map-plain').olMap;

      const center = ol.getView().getCenter();
      const zoom = ol.getView().getZoom();

      expect(center.map((a) => `${parseInt(a)}`).join(' ')).to.equal(
        '111319 222684',
      );
      expect(zoom).to.equal(6);
      expect(value).to.equal(undefined);
    });
  });

  describe('restrict view', function () {
    it('does not set a view extent when restrictView is not set', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      const ol = this.element.querySelector('.ol-map-plain').olMap;
      const extent = ol.getView().getUpdatedOptions_({});

      expect(extent.showFullExtent).to.be.undefined;
    });

    it('sets a view extent when restrictView is true', async function () {
      this.set('value', {
        data: {
          map: {
            restrictView: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      const ol = this.element.querySelector('.ol-map-plain').olMap;
      const extent = ol.getView().getUpdatedOptions_({});

      expect(extent.showFullExtent).to.equal(true);
    });
  });

  describe('base map selection', function () {
    it('does not show the base map selection by default', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      expect(this.element.querySelector('.base-map-select')).not.to.exist;
    });

    it('allows to switch the base map when showBaseMapSelection is true', async function () {
      this.set('value', {
        data: {
          map: {
            mode: 'use a map',
            showBaseMapSelection: true,
          },
          source: { useSelectedModel: false, model: 'map', id: '1' },
        },
        modelKey: 'map_1',
      });

      this.set('model', {
        map_1: mapRecord,
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} />`,
      );

      await waitFor('.base-map-select .item');
      await click('.base-map-select .item');

      expect(
        this.element.querySelector('.map-layer-base').textContent.trim(),
      ).to.equal(baseMap2._id);
    });
  });

  describe('zoom buttons', function () {
    it('does not show the zoom buttons by default', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      expect(this.element.querySelector('.btn-zoom-in')).not.to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).not.to.exist;
    });

    it('shows the zoom buttons when showZoomButtons is true', async function () {
      this.set('value', {
        data: {
          map: {
            enableMapInteraction: 'always',
            showZoomButtons: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      expect(this.element.querySelector('.btn-zoom-in')).to.exist;
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
    });

    it('zooms in when the zoom in button is pressed', async function () {
      this.set('value', {
        data: {
          map: {
            enableMapInteraction: 'always',
            showZoomButtons: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      const olMap = this.element.querySelector('.map').olMap;
      const view = olMap.getView();

      const initialZoom = view.getZoom();

      await click('.btn-zoom-in');
      await PageElements.wait(300);

      expect(initialZoom).to.be.below(view.getZoom());
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
    });

    it('zooms out when the zoom out button is pressed', async function () {
      this.set('value', {
        data: {
          map: {
            enableMapInteraction: 'always',
            showZoomButtons: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      const olMap = this.element.querySelector('.map').olMap;
      const view = olMap.getView();

      const initialZoom = view.getZoom();

      await click('.btn-zoom-out');
      await PageElements.wait(300);

      expect(initialZoom).to.be.above(view.getZoom());
      expect(this.element.querySelector('.btn-zoom-out')).to.exist;
    });
  });

  describe('user location', function () {
    it('does not show the localize button by default', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      expect(this.element.querySelector('.btn-localize')).not.to.exist;
    });

    it('shows the localize button when showUserLocation is true', async function () {
      this.set('value', {
        data: {
          map: {
            showUserLocation: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      expect(this.element.querySelector('.btn-localize')).to.exist;
    });
  });

  describe('interactions', function () {
    it('does not add interactions by default', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      const olMap = this.element.querySelector('.map').olMap;
      const interactions = olMap.getInteractions();

      expect(interactions.getArray().length).to.equal(1);
    });

    it('adds the default interactions when enableMapInteraction = always', async function () {
      this.set('value', {
        data: {
          map: {
            enableMapInteraction: 'always',
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      const olMap = this.element.querySelector('.map').olMap;
      const interactions = olMap.getInteractions();

      expect(interactions.getArray().length).to.equal(10);
    });

    it('does not add interactions when enableMapInteraction = auto', async function (a) {
      this.set('value', {
        data: {
          map: {
            enableMapInteraction: 'automatic',
            showZoomButtons: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @stateUpdateTimeout={{0}} />`,
      );

      expect(this.element.querySelector('.map-controls')).not.to.exist;
      expect(this.element.querySelector('.unlocked')).not.to.exist;
      expect(this.element.querySelector('.locked')).to.exist;

      const olMap = this.element.querySelector('.map').olMap;
      const interactions = olMap.getInteractions();

      expect(interactions.getArray().length).to.equal(1);
    });

    it('adds the interactions when enableMapInteraction = auto and the map is hovered', async function (a) {
      this.set('value', {
        data: {
          map: {
            enableMapInteraction: 'automatic',
            showZoomButtons: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @timeoutHover={{100}} />`,
      );

      triggerEvent('.map-plain-container', 'mouseover');

      expect(this.element.querySelector('.unlocked')).not.to.exist;
      expect(this.element.querySelector('.locked')).to.exist;

      await PageElements.wait(200);

      expect(this.element.querySelector('.unlocked')).to.exist;
      expect(this.element.querySelector('.locked')).not.to.exist;

      expect(this.element.querySelector('.map-controls')).to.exist;

      const olMap = this.element.querySelector('.map').olMap;
      const interactions = olMap.getInteractions();

      expect(interactions.getArray().length).to.be.greaterThan(1);
    });

    it('does not remove the interactions when a inner button is hovered', async function (a) {
      this.set('value', {
        data: {
          map: {
            enableMapInteraction: 'automatic',
            showZoomButtons: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @timeoutHover={{100}} />`,
      );

      triggerEvent('.map-plain-container', 'mouseover');

      await PageElements.wait(200);

      triggerEvent('.map-plain-container', 'mouseout');
      await PageElements.wait(5);

      expect(this.element.querySelector('.btn-zoom-in')).to.exist;
      triggerEvent('.btn-zoom-in', 'mouseover');

      expect(this.element.querySelector('.unlocked')).to.exist;
      expect(this.element.querySelector('.locked')).not.to.exist;

      expect(this.element.querySelector('.map-controls')).to.exist;

      const olMap = this.element.querySelector('.map').olMap;
      const interactions = olMap.getInteractions();

      expect(interactions.getArray().length).to.be.greaterThan(1);
    });
  });

  describe('the feature selection', function () {
    it('initialize the fullscreen mode when a feature is selected', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: true,
          },
        },
      });

      this.set('model', {});
      this.set('state', 'lon@1@lat@2@z@6@fid@630cc9d615ee680100fb7451');

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      await waitUntil(() => !this.element.querySelector('.to-toggled'));

      expect(this.element.querySelector('.page-col-map-view')).to.have.class(
        'toggled',
      );
    });

    it('updates the page state when a feature is selected and featureSelection is "open details in panel"', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: true,
          },
        },
      });

      this.set('model', {});

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}}/>`,
      );

      const vtElement = this.element.querySelector('.vector-tiles');

      vtElement.selectFeature({
        getProperties: () => ({
          _id: feature._id,
        }),
      });

      await waitUntil(() => value);

      expect(value).to.equal('fid@5ca78e2160780601008f69e6');
    });

    it('is unlocked when a feature is selected', async function (a) {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'show popup',
            showTitle: true,
            enableMapInteraction: 'automatic',
          },
        },
        modelKey: 'map_1',
      });

      this.set('model', {
        map_1: mapRecord,
      });

      this.set('state', 'fid@630cc9d615ee680100fb7451');

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      expect(this.element.querySelector('.unlocked')).to.exist;
      expect(this.element.querySelector('.gradient-title.hide')).to.exist;

      triggerEvent('.map-plain-container', 'mouseout');
      await PageElements.wait(5);

      expect(this.element.querySelector('.unlocked')).to.exist;
      expect(this.element.querySelector('.gradient-title.hide')).to.exist;
    });

    it('allows searching for map features', async function () {
      const searchService = this.owner.lookup('service:search');

      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: true,
          },
        },
      });

      this.set('model', {});
      this.set('state', 'lon@1@lat@2@z@6@fid@630cc9d615ee680100fb7451');

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @state={{this.state}}/>`,
      );

      await click('.btn-search');

      await waitUntil(() => !loadingService.isLoading);

      expect(searchService.isEnabled).to.equal(true);
      expect(searchService.map).to.equal('_');
    });

    it('does not update the state when a feature is selected when featureSelection is "do nothing"', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'do nothing',
            enablePinning: true,
          },
        },
      });

      this.set('model', {});

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}}/>`,
      );

      await waitUntil(() => !loadingService.isLoading);

      const vtElement = this.element.querySelector('.vector-tiles');

      vtElement.selectFeature({
        getProperties: () => ({
          _id: feature._id,
        }),
      });

      expect(value).to.equal(undefined);
    });

    it('does not exit the fullscreen mode when a feature is deselected', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: true,
          },
        },
      });

      this.set('model', {});
      this.set('state', 'lon@1@lat@2@z@6@fid@630cc9d615ee680100fb7451');

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      const vtElement = this.element.querySelector('.vector-tiles');

      vtElement.selectFeature(null);

      await waitUntil(() => !this.element.querySelector('.to-toggled'), {
        timeout: 4000,
      });

      expect(this.element.querySelector('.page-col-map-view.toggled')).to.exist;
    });

    it('does not enter in the fullscreen mode when the pin function is disabled', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: false,
          },
        },
      });

      this.set('model', {});
      this.set('state', 'lon@1@lat@2@z@6@fid@630cc9d615ee680100fb7451');

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @state={{this.state}} />`,
      );

      await PageElements.wait(200);

      expect(
        this.element.querySelector('.page-col-map-view'),
      ).not.to.have.class('toggled');
    });

    it('enters the fullscreen mode when a feature id is set in the state', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: true,
          },
        },
      });

      this.set('model', {});
      this.set('state', '');

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      this.set('state', 'lon@1@lat@2@z@6@fid@630cc9d615ee680100fb7451');

      await waitFor('.page-col-map-view.toggled');

      expect(this.element.querySelector('.page-col-map-view')).to.have.class(
        'toggled',
      );
    });

    it.skip('exit the fullscreen mode when the pin button is pressed', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: true,
          },
        },
      });

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      this.set('model', {});
      this.set('state', 'lon@1@lat@2@z@6@fid@630cc9d615ee680100fb7451');

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      await click('.btn-unpin');

      expect(value).to.equal('lon@1@lat@2@z@6');
      expect(this.element.querySelector('.toggled.page-col-map-view')).not.to
        .exist;
      expect(this.element.querySelector('.btn-unpin')).not.to.exist;
    });

    describe('when featureSelection is "go to page"', function (hooks) {
      let value;
      let path;
      let space;

      hooks.beforeEach(async function () {
        this.router.transitionTo = (p) => {
          path = p;
        };

        this.set('value', {
          data: {
            map: {
              featureSelection: 'go to page',
              destination: mapPage._id,
              enablePinning: true,
            },
          },
        });

        this.set('model', {});

        this.set('onChangeState', (s) => {
          value = s;

          this.set('state', s);
        });

        space = await this.store.findRecord('space', spaceData._id);
        this.set('space', space);
      });

      it('navigates to a new page when a feature is selected', async function () {
        space.getPagePath = () => '/test/page';

        await render(
          hbs`<View::Map::MapView @model={{this.model}} @space={{this.space}} @value={{this.value}} @onChangeState={{this.onChangeState}}/>`,
        );

        const vtElement = this.element.querySelector('.vector-tiles');

        vtElement.selectFeature({
          getProperties: () => ({
            _id: feature._id,
          }),
        });

        await waitUntil(() => path);

        expect(value).to.equal(undefined);
        expect(path).to.equal('/test/page?s=fid@5ca78e2160780601008f69e6');
      });

      it('replaces the map id variable with the selected map id', async function () {
        space.getPagePath = () => '/map/:map_id';

        await render(
          hbs`<View::Map::MapView @model={{this.model}} @space={{this.space}} @value={{this.value}} @onChangeState={{this.onChangeState}}/>`,
        );

        const vtElement = this.element.querySelector('.vector-tiles');

        vtElement.selectFeature({
          getProperties: () => ({
            _id: feature._id,
          }),
        });

        await waitUntil(() => path);

        expect(value).to.equal(undefined);
        expect(path).to.equal('/map/_?s=fid@5ca78e2160780601008f69e6');
      });
    });
  });

  describe('user tracking', function (hooks) {
    let events = [];

    hooks.beforeEach(async function () {
      let tracking = this.owner.lookup('service:tracking');
      tracking.addMapView = (url, id) => {
        events.push({ url, id });
      };
    });

    it('raises a map view event', async function () {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'open details in panel',
            enablePinning: true,
          },
        },
      });

      this.set('model', {});

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}}  />`,
      );

      expect(events.length).to.equal(1);
      expect(events[0]).to.deep.contain({
        url: null,
        id: '_',
      });
    });
  });

  describe('geocoding', function (hooks) {
    let geocoding;

    hooks.beforeEach(async function () {
      geocoding = server.testData.storage.addDefaultGeocoding();
      await this.store.findRecord('geocoding', geocoding._id);
    });

    it('focuses on the geocoding feature when it is set', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});
      this.set('state', `lon@1@lat@2@z@6@gec@${geocoding._id}`);

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      const ol = this.element.querySelector('.ol-map-plain').olMap;

      await PageElements.wait(1200);

      const zoom = parseInt(ol.getView().getZoom());
      const center = ol.getView().getCenter();

      expect(center.map((a) => `${parseInt(a)}`).join(' ')).to.equal(
        '1495418 6890270',
      );

      expect(zoom).to.equal(16);
      expect(value).to.equal(
        `lon@13.433576@lat@52.495780999999994@z@16@gec@609e48af7f7737bb2db1a6bc`,
      );
    });
  });

  describe('the page state', function () {
    it('adds the attribute state to the map tiles url', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});
      this.set('state', `at@Garden.Area|BLACKNESS`);

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      expect(
        this.element.querySelector('.vector-tiles').textContent.trim(),
      ).to.equal(
        '/api-v1/tiles/{z}/{x}/{y}?format=vt&attributes=Garden.Area|BLACKNESS',
      );
    });

    it('adds the icon state to the map tiles url', async function () {
      this.set('value', {
        data: {
          map: {},
        },
      });

      this.set('model', {});
      this.set('state', `if@icon-id`);

      let value;
      this.set('onChangeState', (s) => {
        value = s;

        this.set('state', s);
      });

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @onChangeState={{this.onChangeState}} @state={{this.state}} />`,
      );

      expect(
        this.element.querySelector('.vector-tiles').textContent.trim(),
      ).to.equal('/api-v1/tiles/{z}/{x}/{y}?format=vt&icons=icon-id');
    });

    it('renders a feature popup when there is a selected feature and the map has show popup enabled', async function (a) {
      this.set('value', {
        data: {
          map: {
            featureSelection: 'show popup',
          },
        },
      });

      this.set('model', {});
      this.set('state', `fid@${feature._id}`);

      await render(
        hbs`<View::Map::MapView @model={{this.model}} @value={{this.value}} @state={{this.state}} />`,
      );

      expect(this.element.querySelector('.map-marker-feature')).to.exist;
    });
  });
});
