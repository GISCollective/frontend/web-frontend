/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { fillIn, render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/menu-items", function (hooks) {
  setupRenderingTest(hooks);
  let changedValue;
  let server;
  let space;

  hooks.before(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage("000000000000000000000002");

    space = server.testData.storage.addDefaultSpace();
    space.getPagesMap = () => {
      return {
        page1: "000000000000000000000001",
        page2: "000000000000000000000002",
        page3: "000000000000000000000003",
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    changedValue = null;

    this.set("space", space);
    this.set("onChange", (v) => {
      changedValue = v;
    });
  });

  it("renders a value", async function (assert) {
    this.set("value", {
      items: [{ name: "menu-1", link: { pageId: "/page1" }, dropDown: [] }],
    });

    await render(hbs`<Editor::MenuItems @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".manage-editors-properties-link-named .link-name").value).to.equal("menu-1");

    expect(
      this.element
        .querySelector(".manage-editors-properties-link-named .ember-power-select-selected-item")
        .textContent.trim(),
    ).to.equal("page1");
  });

  it("can change the value", async function (assert) {
    this.set("value", {
      items: [{ name: "menu-1", link: { pageId: "/page1" }, dropDown: [] }],
    });

    await render(hbs`<Editor::MenuItems @space={{this.space}} @value={{this.value}} @onChange={{this.onChange}} />`);

    await fillIn(".link-name", "new name");

    expect(changedValue.toJSON()).to.deep.equal({
      items: [{ name: "new name", link: { pageId: "/page1" }, dropDown: [] }],
    });
  });
});
