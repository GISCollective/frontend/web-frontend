/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';

export default class ContainerComponent extends Component {
  get elementId() {
    if (this.args.id) {
      return this.args.id;
    }

    return `container-${guidFor(this)}`;
  }

  get bgColorClass() {
    return `bg-${this.args.value?.color}`;
  }

  get containerClasses() {
    return this.args.value?.classes;
  }

  get containerAlign() {
    return `container-align-${this.args.value?.verticalAlign}`;
  }
}
