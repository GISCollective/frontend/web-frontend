/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class DropdownComponent extends Component {
  @service clickOutside;
  @tracked showingOptions = false;
  @tracked onRight = false;

  elementId = `dropdown-${guidFor(this)}`;

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showOptions() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.hideOptions();
    });

    this.showingOptions = true;

    const rectangle = this.optionsElement.getBoundingClientRect();
    const diff = window.innerWidth - rectangle.x;

    this.onRight = diff < 200;
  }

  @action
  hideOptions() {
    this.showingOptions = false;
  }
}
