/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, click, triggerEvent, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/date', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders "not set" by default', async function () {
      await render(hbs`<Manage::Date @title="title"/>`);

      expect(this.element.querySelector('.text-muted').textContent.trim()).to.equal('not set');
    });

    it('renders the date when it is set', async function () {
      this.set('value', new Date(Date.parse('2019-01-01T00:00:00')));

      await render(hbs`<Manage::Date @title="title" @value={{this.value}}/>`);
      expect(this.element.querySelector('.view-container').textContent.trim()).to.equal('Tue Jan 01 2019');
    });
  });

  describe('in edit mode', function (hooks) {
    it('can disable the value', async function () {
      this.set('value', new Date(Date.parse('2019-01-01T00:00:00')));

      let value;
      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::Date @allowUnset={{true}} @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}} @editablePanel="title"/>`,
      );
      await click('input[type=checkbox]');
      await click('.btn-submit');

      expect(value.getFullYear()).to.equal(0);
    });

    it('changes the year', async function () {
      this.set('value', new Date(Date.parse('2019-01-01T00:00:00')));

      let value;
      this.set('save', (a) => {
        value = a;
      });

      await render(
        hbs`<Manage::Date @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}} @editablePanel="title"/>`,
      );

      await fillIn('.datepicker-input', '2012-02-01');

      await click('.btn-submit');

      expect(value.getFullYear()).to.equal(2012);
    });

    it('resets the year after cancel is pressed', async function () {
      this.set('value', new Date(Date.parse('2019-01-01T00:00:00')));

      await render(
        hbs`<Manage::Date @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}} @editablePanel="title"/>`,
      );

      await fillIn('.datepicker-input', '2012-02-01');

      await click('.btn-cancel');
      expect(this.element.querySelector('.datepicker-input').value).to.equal('2012-02-01');
    });
  });
});
