import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class EditorMenuItems extends Component {
  @tracked _menu;

  get menu() {
    return this._menu ?? this.args.value ?? { items: [] };
  }

  @action
  change(value) {
    this._menu = value;

    return this.args.onChange(value);
  }
}
