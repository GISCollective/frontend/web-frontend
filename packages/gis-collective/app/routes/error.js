/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class ErrorRoute extends Route {
  @service lastError;

  deactivate() {
    this.lastError.message = 'unknown error';
  }
}
