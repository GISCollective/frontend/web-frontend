/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL, click, fillIn } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import Modal from 'core/test-support/modal';

describe ('Acceptance | login page', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let user;
  let space;

  hooks.beforeEach(function () {
    server = new TestServer();
    let translation1 = server.testData.create.translation('1');
    translation1.name = 'Romana';
    translation1.locale = 'ro-ro';
    translation1.file = null;

    let translation2 = server.testData.create.translation('2');
    translation2.name = 'Francois';
    translation2.locale = 'fr-fr';
    translation2.file = null;

    user = server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addTranslation(translation1);
    server.testData.storage.addTranslation(translation2);

    const intl = this.owner.lookup('service:intl');
    intl.setLocale(['ro-ro','en-us']);
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe ('on a non default space', function (hooks) {
    hooks.beforeEach(function () {
      const space = server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultProfile();
      server.testData.storage.addDefaultPage();

      space.visibility.isDefault = false;
      space.name = "some space name";
      space.landingPageId = "";
    });

    it("does not render the register and activation links", async function() {
      await visit('/login');

      expect(this.element.querySelector(".list-group-register")).not.to.exist;
      expect(this.element.querySelector(".list-group-confirmation")).not.to.exist;
    });

    it("renders the space name", async function() {
      await visit('/login');

      expect(this.element.querySelector(".card-title").textContent.split("\n").map(a => a.trim()).filter(a => a).join(" ")).to.equal("Welcome to some space name!")
    });
  });

  describe ('on a private non default space', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultProfile();
      server.testData.storage.addDefaultPage();

      server.spaceSummary = {
        "isPublic": false,
        "logo": "5cc8dc1038e882010061545a",
        "name": "My private service"
      };
    });

    it("does not render the register and activation links", async function() {
      await visit('/login');

      expect(this.element.querySelector(".list-group-register")).not.to.exist;
      expect(this.element.querySelector(".list-group-confirmation")).not.to.exist;
    });

    it("renders the space name", async function() {
      await visit('/login');

      expect(this.element.querySelector(".card-title").textContent.split("\n").map(a => a.trim()).filter(a => a).join(" ")).to.equal("Welcome to My private service!")
    });
  });

  describe ('on the default space', function (hooks) {
    hooks.beforeEach(function () {
      space = server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultProfile();
      server.testData.storage.addDefaultPage();
      space.domain = "new.opengreenmap.org";

      space.visibility.isDefault = true;
      space.landingPageId = "";
    });

    it("renders the register and activation links", async function() {
      await visit('/login');

      expect(this.element.querySelector(".list-group-register")).to.exist;
      expect(this.element.querySelector(".list-group-register a")).to.have.attribute("href", "/login/register");
      expect(this.element.querySelector(".list-group-confirmation")).to.exist;
      expect(this.element.querySelector(".list-group-confirmation a")).to.have.attribute("href", "/login/activate");
    });

    it("renders a custom url when the register.url is set", async function(a) {
      server.testData.storage.addPreference('register.url', "https://giscollective.com");

      await visit('/login');

      expect(this.element.querySelector(".list-group-register")).to.exist;
      expect(this.element.querySelector(".list-group-register a")).to.have.attribute("href", "https://giscollective.com");
      expect(this.element.querySelector(".list-group-confirmation")).to.exist;
      expect(this.element.querySelector(".list-group-confirmation a")).to.have.attribute("href", "/login/activate");
    });

    describe ('when the user should be redirected to the welcome presentation', function (hooks) {
      hooks.beforeEach(function () {
        let profile = server.testData.create.userProfile(user._id);
        profile.showWelcomePresentation = true;
        server.testData.storage.addUserProfile(profile);
        server.testData.storage.addDefaultPresentation('welcome');

        server.post('/mock-server/auth/token', () => {
          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
              token_type: 'Bearer',
              expires_in: 3600,
              refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
            }),
          ];
        });

        server.put('/mock-server/userprofiles/5b870669796da25424540deb', () => {
          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              profile
            }),
          ];
        });
      });

      it('should redirect to the presentation when is enabled', async function () {
        server.testData.storage.addPreference(
          'appearance.showWelcomePresentation',
          true
        );
        await visit('/login');

        await fillIn('#identification', 'Useremail@mail.com');
        await fillIn('#password', 'password');

        await click('button[type=submit]');

        expect(currentURL()).to.equal('/presentation/welcome?locale=en-us');
      });

      it('should redirect to the world map when is disabled', async function () {
        server.testData.storage.addPreference(
          'appearance.showWelcomePresentation',
          false
        );
        await visit('/login');

        await fillIn('#identification', 'Useremail@mail.com');
        await fillIn('#password', 'password');

        await click('button[type=submit]');

        expect(currentURL()).to.equal('/browse/maps/_/map-view');
      });
    });

    describe ('when the login article is not set', function (hooks) {
      it('can visit /login', async function () {
        await visit('/login');

        expect(this.element.querySelector('#identification')).to.exist;
        expect(this.element.querySelector('#password')).to.exist;
        expect(this.element.querySelector('button[type=submit]')).to.exist;

        expect(currentURL()).to.equal('/login');
      });

      it('should not show the menu on /login for private services', async function () {
        server.testData.storage.addPreference('register.mandatory', true);
        server.testData.storage.addPreference('register.enabled', false);

        await visit('/login');
        expect(this.element.querySelector('.navbar')).not.to.exist;
      });

      it('should use the default locale when it has an invalid value', async function () {
        await visit('/login?locale=ro-rooo');
        expect(currentURL()).to.equal('/login');
      });

      it('should persist the language after login', async function () {
        await visit('/login?locale=ro-ro');
        expect(currentURL()).to.equal('/login?locale=ro-ro');

        await fillIn('#identification', 'Useremail@mail.com');
        await fillIn('#password', 'password');

        let received;

        server.post('/mock-server/auth/token', (request) => {
          received = request.requestBody;
          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
              token_type: 'Bearer',
              expires_in: 3600,
              refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
            }),
          ];
        });

        await click('button[type=submit]');

        expect(received).to.equal(
          'grant_type=password&username=useremail%40mail.com&password=password'
        );
        expect(currentURL()).to.equal('/browse/maps/_/map-view?locale=ro-ro');
      });

      it('should redirect after login if a redirect query param is used', async function () {
        await visit('/login?redirect=/browse/sites');
        expect(currentURL()).to.equal('/login?redirect=/browse/sites');

        await fillIn('#identification', 'useremail@mail.com');
        await fillIn('#password', 'password');
        server.post('/mock-server/auth/token', () => [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
            token_type: 'Bearer',
            expires_in: 3600,
            refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
          }),
        ]);
        await click('button[type=submit]');

        expect(currentURL()).to.equal('/browse/sites?locale=en-us');
      });

      it('should redirect to the redirect query param if the user is already logged in', async function () {
        await visit('/login');
        await fillIn('#identification', 'useremail@mail.com');
        await fillIn('#password', 'password');
        server.post('/mock-server/auth/token', () => [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
            token_type: 'Bearer',
            expires_in: 3600,
            refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
          }),
        ]);
        await click('button[type=submit]');

        await visit('/login?redirect=/browse/sites');
        expect(currentURL()).to.equal('/browse/sites');
      });

      it('displays an error for invalid credentials', async function () {
        server.post('/mock-server/auth/token', () => {
          return [
            401,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              error: 'Invalid password or username',
            }),
          ];
        });

        await visit('/login');

        await fillIn('#identification', 'invalid-email');
        await fillIn('#password', 'invalid-password');
        await click('button[type=submit]');

        expect(
          this.element.querySelector('.alert-danger').textContent.trim()
        ).to.equal('Invalid password or username');
      });

      it('should not display the article', async function () {
        await visit('/login');
        expect(this.element.querySelector('.login-article')).not.to.exist;
        expect(this.element.querySelector('h1')).not.to.exist;
      });

      it('should not display the google login when the google client id is not set', async function () {
        server.testData.storage.addPreference(
          'integrations.google.client_id',
          ''
        );
        await visit('/login');

        expect(this.element.querySelector('.login-google')).not.to.exist;
      });

      it('should display the google login when the google client id is set', async function () {
        server.testData.storage.addPreference(
          'integrations.google.client_id',
          'test_id'
        );
        await visit('/login');

        expect(this.element.querySelector('.login-google')).to.exist;
      });

      it('should display the google login when the google client id is set and the registration is disabled', async function () {
        server.testData.storage.addPreference('register.enabled', false);
        server.testData.storage.addPreference(
          'integrations.google.client_id',
          'test_id'
        );
        await visit('/login');

        expect(this.element.querySelector('.login-google')).to.exist;
      });
    });

    describe ('when the login article is set', function (hooks) {
      let article;

      hooks.beforeEach(function () {
        article = server.testData.create.article('login');
        article.title = 'login title';
        article.content = '# login title\n\nlogin description';

        server.testData.storage.addArticle(article);
      });

      it('should display the article on new.opengreenmap.org', async function () {
        await visit('/login');

        expect(this.element.querySelector('.login-article h1')).to.exist;
        expect(this.element.querySelector('.login-article div p')).to.exist;

        expect(
          this.element.querySelector('.login-article h1').textContent.trim()
        ).to.equal('login title');
        expect(
          this.element.querySelector('.login-article p').textContent.trim()
        ).to.equal('login description');
      });
    });

    describe ('when the login article has an empty content', function (hooks) {
      let article;

      hooks.beforeEach(function () {
        article = server.testData.create.article('login');
        article.title = '';
        article.content = '';

        server.testData.storage.addArticle(article);
      });

      it('should not display the article', async function () {
        await visit('/login');

        expect(this.element.querySelector('.login-article')).not.to.exist;
        expect(this.element.querySelector('h1')).not.to.exist;
      });
    });
  });

  describe ('on a custom space', function (hooks) {
    hooks.beforeEach(function () {
      const space = server.testData.storage.addDefaultSpace();
      server.testData.storage.addDefaultProfile();
      server.testData.storage.addDefaultPage();

      space.visibility.isDefault = false;
    });

    describe ('when the login article is set', function (hooks) {
      let article;

      hooks.beforeEach(function () {
        article = server.testData.create.article('login');
        article.title = 'login title';
        article.content = '# login title\n\nlogin description';

        server.testData.storage.addArticle(article);
      });

      it('does not display the article', async function () {
        await visit('/login');

        expect(this.element.querySelector('.login-article h1')).not.to.exist;
        expect(this.element.querySelector('.login-article div p')).not.to.exist;
      });
    });
  });
});
