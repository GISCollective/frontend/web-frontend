/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import {
  typeIn,
  render,
  fillIn,
  click,
  triggerEvent,
  waitUntil,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/options/page-options',  function (hooks) {
  setupRenderingTest(hooks, { server: true });

  let receivedPicture;

  hooks.beforeEach(function () {
    this.server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      const picture = this.server.testData.storage.addDefaultPicture();

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture,
        }),
      ];
    });
  });

  it('can set a page path', async function () {
    this.set('page', {});

    await render(hbs`<Input::Options::PageOptions @page={{this.page}}/>`);

    await typeIn('.page-path', 'page/with/levels');
    expect(this.element.querySelector('.page-path').value).to.equal(
      '/page/with/levels'
    );
    expect(this.element.querySelector('.btn-fix-slug')).not.to.exist;
    expect(this.element.querySelector('.input-model-id')).not.to.exist;
  });

  it('allows changing the model id when the slug has a variable', async function () {
    this.set('page', {
      slug: '/page/with/:campaign_id',
    });

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::PageOptions @onModelIdChange={{this.change}} @modelId="test-model-id" @page={{this.page}}/>`
    );

    expect(this.element.querySelector('.input-model-id')).to.exist;
    expect(this.element.querySelector('.input-model-id').value).to.equal(
      'test-model-id'
    );

    await fillIn('.input-model-id', '234782634');

    expect(this.element.querySelector('.input-model-id').value).to.equal(
      '234782634'
    );

    expect(value).to.equal('234782634');
  });

  it('renders the slug based on the path', async function () {
    this.set('page', {});

    await render(hbs`<Input::Options::PageOptions @page={{this.page}}/>`);

    await fillIn('.page-path', '/page/with/levels');

    expect(
      this.element.querySelector('.page-slug .slug-value').textContent.trim()
    ).to.equal('page--with--levels');
    expect(this.element.querySelector('.btn-fix-slug')).not.to.exist;
  });

  it('allows to fix an invalid path', async function () {
    this.set('page', {});

    await render(hbs`<Input::Options::PageOptions @page={{this.page}}/>`);

    await fillIn('.page-path', 'page/ w i t h///levels');

    expect(
      this.element.querySelector('.page-slug .slug-value').textContent.trim()
    ).to.equal('page--w i t h--levels');
    expect(this.element.querySelector('.btn-fix-slug')).to.exist;

    await click('.btn-fix-slug');
    expect(this.element.querySelector('.page-path').value).to.equal(
      '/page/w i t h/levels'
    );
  });

  it('shows an error message when an invalid variable is used', async function () {
    this.set('page', {});

    await render(hbs`<Input::Options::PageOptions @page={{this.page}}/>`);

    await fillIn('.page-path', '/browse/:invalid-id');

    expect(
      this.element.querySelector('.page-slug .slug-value').textContent.trim()
    ).to.equal('browse--:invalid-id');
    expect(
      this.element.querySelector('.variable-error').textContent.trim()
    ).to.contain('invalid vars');
    expect(
      this.element.querySelector('.variable-error').textContent.trim()
    ).to.contain(':invalid-id');
  });

  it('shows an error when two valid vars are user', async function () {
    this.set('page', {});

    await render(hbs`<Input::Options::PageOptions @page={{this.page}}/>`);

    await fillIn('.page-path', '/browse/:map-id/:icon-id');

    expect(
      this.element.querySelector('.page-slug .slug-value').textContent.trim()
    ).to.equal('browse--:map-id--:icon-id');
    expect(
      this.element.querySelector('.variable-error').textContent.trim()
    ).to.contain('only one variable is allowed in the path');
  });

  it('can change the description', async function () {
    this.set('page', {});

    await render(hbs`<Input::Options::PageOptions @page={{this.page}}/>`);

    await fillIn('.page-description', 'something about this page');

    expect(this.page.description).to.equal('something about this page');
  });

  it('can change the cover', async function () {
    this.set('page', { id: "some-id" });

    await render(hbs`<Input::Options::PageOptions @page={{this.page}}/>`);

    const blob = this.server.testData.create.pngBlob();

    await triggerEvent("input[type='file']", 'change', {
      files: [blob],
    });

    await waitUntil(() => receivedPicture);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'Page', id: "some-id" },
      data: {},
      disableOptimization: false
    });
  });
});
