/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import Service from '@ember/service';

class MockService extends Service {
  ask() {}
}

describe ('Integration | Component | model-actions/article',  function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function() {
    this.owner.register('service:notifications', MockService);
  })

  it('renders nothing when the campaign is not editable', async function () {
    await render(hbs`<ModelActions::Article />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the edit button when the record is editable', async function () {
    this.set(
      'record',
      this.store.createRecord('article', {
        id: 'id',
        canEdit: true,
      })
    );

    await render(hbs`<ModelActions::Article @record={{this.record}} />`);

    const texts = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

    expect(texts).to.deep.equal(['Edit']);
  });

  describe ('publish options', function (hooks) {
    it('shows the `publish now` option for an newsletter message with the pending state', async function () {
      this.set(
        'record',
        this.store.createRecord('article', {
          id: 'id',
          canEdit: true,
          type: 'newsletter-article',
          status: 'pending',
        })
      );

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      const texts = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

      expect(texts).to.deep.equal(['Edit', 'Send test email', 'Publish now', 'Cancel publishing']);
    });

    it('shows the `send test email` and `send to all recipients` options for an newsletter message with the draft state', async function () {
      this.set(
        'record',
        this.store.createRecord('article', {
          id: 'id',
          canEdit: true,
          type: 'newsletter-article',
          status: 'draft',
        })
      );

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      const texts = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

      expect(texts).to.deep.equal(['Edit', 'Send test email', 'Send to all recipients']);
    });

    it('shows the `cancel publishing` option for an newsletter message with the pending state', async function () {
      this.set(
        'record',
        this.store.createRecord('article', {
          id: 'id',
          canEdit: true,
          type: 'newsletter-article',
          status: 'pending',
        })
      );

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      const texts = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

      expect(texts).to.deep.equal(['Edit', 'Send test email', 'Publish now', 'Cancel publishing']);
    });

    it('shows the `cancel publishing` option for an newsletter message with the publishing state', async function () {
      this.set(
        'record',
        this.store.createRecord('article', {
          id: 'id',
          canEdit: true,
          type: 'newsletter-article',
          status: 'publishing',
        })
      );

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      const texts = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

      expect(texts).to.deep.equal(['Edit', 'Send test email', 'Cancel publishing']);
    });

    it('does not show publish buttons for sent state', async function () {
      this.set(
        'record',
        this.store.createRecord('article', {
          id: 'id',
          canEdit: true,
          type: 'newsletter-article',
          status: 'sent',
        })
      );

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      const texts = [...this.element.querySelectorAll('.dropdown-item')].map((a) => a.textContent.trim());

      expect(texts).to.deep.equal(['Edit', 'Send test email']);
    });

    it('calls the publish method when the button is pressed', async function () {
      let called;

      this.set('record', {
        id: 'id',
        canBePublished: true,
        publish: () => {
          called = true;
        },
      });

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      await click('.dropdown-item-publish');
      expect(called).to.equal(true);
    });

    it('calls the unpublish method when the button is pressed', async function () {
      let called;

      this.set('record', {
        id: 'id',
        canUnpublish: true,
        unpublish: () => {
          called = true;
        },
      });

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      await click('.dropdown-item-unpublish');
      expect(called).to.equal(true);
    });

    it('calls the sendTestEmail method when the button is pressed', async function () {
      let called;

      this.set('record', {
        id: 'id',
        canBeTested: true,
        sendTestEmail: () => {
          called = true;
        },
      });

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      await click('.dropdown-item-send-test-email');
      expect(called).to.equal(true);
    });

    it('changes the status to pending when the button is pressed', async function () {
      let called;

      this.set('record', {
        id: 'id',
        status: 'draft',
        save: () => {
          called = true;
        },
      });

      await render(hbs`<ModelActions::Article @record={{this.record}} />`);

      await click('.dropdown-item-send-to-all');
      expect(called).to.equal(true);
      expect(this.get('record.status')).to.equal('pending');
    });
  });
});
