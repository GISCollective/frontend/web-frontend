/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render, fillIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { tracked } from '@glimmer/tracking';

class MockPage {
  @tracked slug;
  @tracked name;

  set(k, v) {
    this[k] = v;
  }
}

describe('Integration | Component | new-record-form/page', function (hooks) {
  setupRenderingTest(hooks);

  describe('for a normal user', function (hooks) {
    it('renders an alert when there is no space', async function () {
      await render(hbs`<NewRecordForm::Page />`);

      expect(this.element.querySelector('.alert-danger .btn-add-space')).to.exist;
      expect(this.element.querySelector('.row-space')).not.to.exist;
    });

    it('does not render an alert when there is a space and it is added by default', async function () {
      let page = new MockPage();
      this.set('spaces', [{ id: '1', name: 'name' }]);
      this.set('page', page);

      let value;
      this.set('submit', (a) => {
        value = a;
      });

      await render(hbs`<NewRecordForm::Page @spaces={{this.spaces}} @page={{this.page}} @onSubmit={{this.submit}} />`);

      await fillIn('.input-name', 'new page name');
      await fillIn('.input-path', '/new-slug');
      await click(this.element.querySelector('.btn-submit'));

      expect(this.element.querySelector('.alert-danger .btn-add-space')).not.to.exist;
      expect(this.element.querySelector('.row-space')).not.to.exist;
      expect(value).to.deep.contain({
        space: { id: '1', name: 'name' },
        name: 'new page name',
        slug: 'new-slug',
        visibility: { isPublic: false, isDefault: false },
      });
    });

    it('renders the space section when there are 2 spaces', async function () {
      this.set('spaces', [{}, {}]);
      this.set('page', {});

      await render(hbs`<NewRecordForm::Page @spaces={{this.spaces}} @page={{this.page}} />`);

      expect(this.element.querySelector('.alert-danger .btn-add-space')).not.to.exist;
      expect(this.element.querySelector('.row-space')).to.exist;
    });
  });

  describe('for admin user', function (hooks) {
    hooks.beforeEach(function () {
      let service = this.owner.lookup('service:user');
      service.userData = { isAdmin: true };
    });

    it('renders an alert when there is no space', async function () {
      await render(hbs`<NewRecordForm::Page />`);

      expect(this.element.querySelector('.alert-danger .btn-add-space')).to.exist;
      expect(this.element.querySelector('.row-space')).to.exist;
    });
  });
});
