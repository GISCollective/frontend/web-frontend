/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, click, waitFor, settled } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/dashboards/articles', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let space;
  let team;
  let article;
  let categories = '{ "categories": ["category 1", "category 2"] }';

  let deleteIds = [];
  let deleteAll;

  let unpublishedIds = [];
  let unpublishedAll;

  let publishedIds = [];
  let publishedAll;

  hooks.beforeEach(function () {
    server = new TestServer();
    team = server.testData.create.team();

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    space = server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultIcon();

    article = server.testData.create.article('5ca89e27ef1f7e010007f4af');
    article.canEdit = true;
    article.type = 'any';
    article.visibility.team = team._id;

    server.testData.storage.addArticle(article);

    server.get(`/mock-server/articles/_/categories`, () => {
      return [200, { 'Content-Type': 'application/json' }, categories];
    });

    deleteIds = [];
    server.delete_('/mock-server/articles/:id', (req) => {
      deleteIds.push(req.params.id);

      return [204];
    });

    deleteAll = null;
    server.delete_('/mock-server/articles', (req) => {
      deleteAll = req.queryParams;
      return [204];
    });

    unpublishedIds = [];
    server.post('/mock-server/articles/:id/unpublish', (req) => {
      unpublishedIds.push(req.params.id);

      return [204];
    });

    unpublishedAll = null;
    server.post('/mock-server/articles/unpublish', (req) => {
      unpublishedAll = req.queryParams;

      return [204];
    });

    publishedIds = [];
    server.post('/mock-server/articles/:id/publish', (req) => {
      publishedIds.push(req.params.id);

      return [204];
    });

    publishedAll = null;
    server.post('/mock-server/articles/publish', (req) => {
      publishedAll = req.queryParams;

      return [204];
    });
  });

  hooks.afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/articles/${team._id}`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdashboards%2Farticles%2F5ca78e2160780601008f69e6');
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/articles/${team._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Articles']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca78e2160780601008f69e6',
    ]);
  });

  it('should display the article as a card', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/articles/${team._id}`);

    expect(this.element.querySelectorAll('.card-dashboard-article').length).to.equal(1);
    expect(this.element.querySelector('.card-dashboard-article .title').textContent.trim()).to.equal(article.title);
  });

  it('can delete the article', async function () {
    const service = this.owner.lookup('service:notifications');
    service.ask = () => 'yes';

    authenticateSession();
    let deletedArticle = false;
    server.delete(`/mock-server/articles/:id`, () => {
      deletedArticle = true;

      return [204, {}, ''];
    });
    await visit(`/manage/dashboards/articles/${team._id}`);

    await waitFor('.card-dashboard-article .dropdown-toggle');

    await click('.card-dashboard-article .dropdown-toggle');

    await waitFor('.card-dashboard-article .btn-delete:not([disabled])');
    await click('.card-dashboard-article .btn-delete');

    expect(deletedArticle).to.eql(true);
    expect(this.element.querySelectorAll('.card-dashboard-article')).to.have.length(0);
  });

  it('can filter the articles by category', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/articles/${team._id}`);

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];

    await click(buttons[2]);

    expect(this.element.querySelector('.btn-group-name.btn-selected').textContent.trim()).to.equal('category 2');
    expect(currentURL()).to.equal('/manage/dashboards/articles/5ca78e2160780601008f69e6?category=category%202');
  });

  it('can add an article for the current team', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/articles/${team._id}`);

    await click('.btn-add-article');

    expect(currentURL()).to.equal(`/manage/articles/add?teamId=${team._id}`);
  });

  it('can add an article for the current category', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/articles/${team._id}`);

    const buttons = [...this.element.querySelectorAll('.btn-group-name')];
    await click(buttons[2]);

    await click('.btn-add-category-article');

    expect(currentURL()).to.equal(`/manage/articles/add?category=category%202&teamId=${team._id}`);
  });

  it('does not render the categories when there are none', async function () {
    authenticateSession();
    categories = `{ "categories": [] }`;

    await visit(`/manage/dashboards/articles/${team._id}`);

    expect(this.element.querySelector('.group-selector')).not.to.exist;
  });

  describe('bulk delete', function (hooks) {
    it('can delete two articles by selecting them', async function () {
      const service = this.owner.lookup('service:modal');
      service.confirm = () => 'yes';

      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const checkboxes = this.element.querySelectorAll('.newsletter-article-card input');

      await click(checkboxes[0]);
      await click(checkboxes[1]);

      await click('.navbar .btn-delete');

      expect(deleteIds).to.deep.equal(['1', '2']);
    });

    it('can delete one article by selecting it', async function () {
      const service = this.owner.lookup('service:modal');
      service.confirm = () => 'yes';

      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const checkboxes = this.element.querySelectorAll('.newsletter-article-card input');

      await click(checkboxes[0]);

      await click('.navbar .btn-delete');

      expect(deleteIds).to.deep.equal(['1']);
    });

    it('can delete all articles by selecting the select all button', async function () {
      const service = this.owner.lookup('service:modal');
      service.confirm = () => 'yes';

      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      await click('.chk-select-all');
      await click('.navbar .btn-delete');

      expect(deleteAll).to.deep.equal({
        team: team._id,
        type: 'any',
      });
    });

    it('can delete all articles from a category by selecting the select all button', async function () {
      const service = this.owner.lookup('service:modal');
      service.confirm = () => 'yes';

      categories = '{ "categories": ["category 1", "category 2"] }';

      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      await PageElements.wait(100);

      const groups = this.element.querySelectorAll('.btn-group-name');
      await click(groups[1]);

      await click('.chk-select-all');
      await click('.navbar .btn-delete');

      expect(deleteAll).to.deep.equal({
        team: team._id,
        category: 'category 1',
        type: 'any',
      });
    });
  });

  describe('bulk unpublish', function (hooks) {
    it('can unpublish two articles by selecting them', async function () {
      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = true;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = true;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const checkboxes = this.element.querySelectorAll('.newsletter-article-card input');

      await click(checkboxes[0]);
      await click(checkboxes[1]);

      await click('.navbar .btn-unpublish');

      expect(unpublishedIds).to.deep.equal(['1', '2']);
    });

    it('unpublish one article when one is public', async function () {
      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = true;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = false;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const checkboxes = this.element.querySelectorAll('.newsletter-article-card input');

      await click(checkboxes[0]);
      await click(checkboxes[1]);

      await click('.navbar .btn-unpublish');

      expect(unpublishedIds).to.deep.equal(['1']);
    });

    it('unpublish all articles from a category when the select all button is pressed', async function () {
      categories = '{ "categories": ["category 1", "category 2"] }';

      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = true;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = false;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const groups = this.element.querySelectorAll('.btn-group-name');
      await click(groups[1]);

      await click('.chk-select-all');

      await click('.navbar .btn-publish');

      expect(publishedIds).to.deep.equal([]);
      expect(publishedAll).to.deep.equal({ team: '5ca78e2160780601008f69e6', category: 'category 1', type: 'any' });
    });
  });

  describe('bulk publish', function (hooks) {
    it('can publish two articles by selecting them', async function () {
      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = false;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = false;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const checkboxes = this.element.querySelectorAll('.newsletter-article-card input');

      await click(checkboxes[0]);
      await click(checkboxes[1]);

      await click('.navbar .btn-publish');

      expect(publishedIds).to.deep.equal(['1', '2']);
    });

    it('publish one article when one is private', async function () {
      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = true;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = false;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const checkboxes = this.element.querySelectorAll('.newsletter-article-card input');

      await click(checkboxes[0]);
      await click(checkboxes[1]);

      await click('.navbar .btn-publish');

      expect(publishedIds).to.deep.equal(['2']);
    });

    it('publishes all articles when the select all button is pressed', async function () {
      article = server.testData.storage.addDefaultArticle('1');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = true;

      article = server.testData.storage.addDefaultArticle('2');
      article.canEdit = true;
      article.type = 'any';
      article.visibility.team = team._id;
      article.visibility.isPublic = false;

      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      await click('.chk-select-all');

      await click('.navbar .btn-publish');

      expect(publishedIds).to.deep.equal([]);
      expect(publishedAll).to.deep.equal({ team: '5ca78e2160780601008f69e6', type: 'any' });
    });
  });

  describe('for a team with categories', function (hooks) {
    hooks.beforeEach(function () {
      team = server.testData.create.team();
      team.categories = ['news', 'blog', 'other'];
      server.testData.storage.addTeam(team);
    });

    it('shows the team categories', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const buttons = [...this.element.querySelectorAll('.team-categories .btn-group-name')];

      expect(buttons).to.have.length(4);
      expect(buttons[0]).to.have.textContent('all');
      expect(buttons[1]).to.have.textContent('news');
      expect(buttons[2]).to.have.textContent('blog');
      expect(buttons[3]).to.have.textContent('other');

      await click(buttons[1]);

      expect(currentURL()).to.equal('/manage/dashboards/articles/5ca78e2160780601008f69e6?teamCategory=news');
    });

    it('adds new article buttons for the team categories', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      const buttons = [...this.element.querySelectorAll('.btn-add-team-category-article')];

      expect(buttons).to.have.length(3);
      expect(buttons[0]).to.have.textContent('news');
      expect(buttons[1]).to.have.textContent('blog');
      expect(buttons[2]).to.have.textContent('other');

      await click(buttons[1]);

      expect(currentURL()).to.equal('/manage/articles/add?category=blog&teamId=5ca78e2160780601008f69e6');
    });

    it('hides the article categories when there is no team category selected', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}`);

      expect(this.element.querySelector('.article-categories')).not.to.exist;
    });

    it('shows the article categories when a team category is selected', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/articles/${team._id}?teamCategory=news`);

      expect(this.element.querySelector('.article-categories')).to.exist;

      const buttons = [...this.element.querySelectorAll('.article-categories .btn-group-name')];
      await click(buttons[1]);

      expect(currentURL()).to.equal(
        '/manage/dashboards/articles/5ca78e2160780601008f69e6?category=category%201&teamCategory=news',
      );
    });

    it('shows + category button with both categories', async function () {
      authenticateSession();

      await visit('/manage/dashboards/articles/5ca78e2160780601008f69e6?category=category%201&teamCategory=news');

      await click('.btn-add-category-article');

      expect(this.element.querySelector('.select-primary-category').value).to.equal('news');

      const categories = [...this.element.querySelectorAll('.input-categories .ember-power-select-selected-item')];

      expect(categories).to.have.length(1);
      expect(categories[0]).to.have.textContent('category 1');
    });
  });
});
