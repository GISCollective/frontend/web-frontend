/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, click, waitFor, triggerEvent, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/source-buckets", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let spaceData;
  let articleData;
  let article;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam("1");
    server.testData.storage.addDefaultTeam("2");
    spaceData = server.testData.storage.addDefaultSpace();
    articleData = server.testData.storage.addDefaultArticle();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    server.testData.create.pagesMap = {
      campaigns: "1",
      "campaigns--:campaign_id": "2",
      "help--terms": "3",
    };

    this.set("space", await this.store.findRecord("space", spaceData._id));

    article = await this.store.findRecord("article", articleData._id);

    this.space._getPagesMap = {
      campaigns: "1",
      "campaigns--:campaign_id": "2",
      "help--terms": "3",
    };
  });

  it("allows picking the coverAndPictures for articles", async function (a) {
    this.set("value", { ids: [], model: "selected-model", useSelectedModel: true });
    this.set("model", {
      selectedModel: article,
    });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    this.set("testElement", null);

    await render(
      hbs`<div class="test-element" data-preferred-model="picture" /><Editor::SourceBuckets @model={{this.model}} @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} @element={{this.testElement}}/>`,
    );

    this.set("testElement", this.element.querySelector(".test-element"));

    this.element.querySelector(".model-property").value = "coverAndPictures";
    await triggerEvent(".model-property", "change");

    expect(value).to.deep.equal({ useSelectedModel: true, property: "coverAndPictures" });
  });

  it("allows changing to the `all` value", async function (a) {
    this.set("value", { ids: [], model: "campaign" });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::SourceBuckets @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    await waitFor(".btn-check-ids");

    expect(this.element.querySelector(".btn-check-ids").checked).to.equal(true);

    await click(".btn-check-all");

    expect(value).to.deep.equal({ model: "campaign" });
  });

  it("allows filtering features by name", async function (a) {
    this.set("value", { ids: [], model: "feature" });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Editor::SourceBuckets @value={{this.value}} @onChange={{this.onChange}} @space={{this.space}} />`,
    );

    await waitFor(".btn-check-parameters");
    await click(".btn-check-parameters");

    this.element.querySelector("[data-parameter=sortBy] select").value = "fixed";
    await triggerEvent("[data-parameter=sortBy] select", "change");
    await fillIn("[data-parameter=sortBy] .model-text", "name");

    this.element.querySelector("[data-parameter=sortOrder] select").value = "fixed";
    await triggerEvent("[data-parameter=sortOrder] select", "change");
    await fillIn("[data-parameter=sortOrder] .model-text", "asc");

    expect(value).to.deep.equal({
      parameters: { sortBy: { source: "fixed", value: "name" }, sortOrder: { source: "fixed", value: "asc" } },
      model: "feature",
    });
  });
});
