/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, click, waitFor, settled } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import { authenticateSession } from 'ember-simple-auth/test-support';
import PageElements from 'core/test-support/page-elements';

describe ('Acceptance | wizard/import/file preview',  function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let mapFile;
  let meta;

  hooks.afterEach(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultMap('5e2f42c96e290a010057dd95');
    server.testData.storage.addDefaultUser(true);
    meta = {
      preview: [],
    };

    server.get(`/mock-server/mapfiles/:id/preview`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(meta),
      ];
    });

    authenticateSession();
  });

  describe ('when there is a file with no previews',  function (hooks) {
    hooks.beforeEach(function () {
      mapFile = server.testData.storage.addDefaultMapFile();
    });

    it('shows a message that the file is empty', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/preview`);

      await waitFor(".alert-danger");

      expect(this.element.querySelector('.message-alert-file-without-preview'))
        .to.exist;
    });

    it('shows a disabled import button', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/preview`);

      await waitFor('.btn-import');

      expect(this.element.querySelector('.btn-import')).to.have.attribute(
        'disabled',
        ''
      );
    });
  });

  describe ('when there is a file with 2 previews and no errors',  function (hooks) {
    let imported;

    hooks.beforeEach(function () {
      mapFile = server.testData.storage.addDefaultMapFile();
      meta.preview = [
        {
          errors: [],
          result: {
            name: 'first item',
            description: 'first description',
          },
        },
        {
          errors: [],
          result: {
            name: 'second item',
            description: 'second description',
          },
        },
      ];

      imported = false;
      server.post(`/mock-server/mapfiles/${mapFile._id}/import`, () => {
        imported = true;
        return [201, { 'Content-Type': 'application/json' }, ''];
      });
    });

    it('renders the first preview', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/preview`);

      await waitFor('.site-preview-import');

      await settled();
      await waitFor('.badge-new-feature');

      expect(this.element.querySelector('.badge-update-feature')).not.to.exist;
      expect(
        this.element.querySelector('.site-preview-import h1').textContent.trim()
      ).to.equal('first item');
      expect(
        this.element.querySelector('.site-preview-import p').textContent.trim()
      ).to.equal('first description');
    });

    it('can cycle the previews using the next button', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/preview`);

      await waitFor('.btn-next');

      await click('.btn-next');
      expect(
        this.element.querySelector('.site-preview-import h1').textContent.trim()
      ).to.equal('second item');
      expect(
        this.element.querySelector('.site-preview-import p').textContent.trim()
      ).to.equal('second description');

      await click('.btn-next');
      expect(
        this.element.querySelector('.site-preview-import h1').textContent.trim()
      ).to.equal('first item');
      expect(
        this.element.querySelector('.site-preview-import p').textContent.trim()
      ).to.equal('first description');
    });

    it('can cycle the previews using the prev button', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/preview`);

      await waitFor('.btn-prev');

      await click('.btn-prev');
      expect(
        this.element.querySelector('.site-preview-import h1').textContent.trim()
      ).to.equal('second item');
      expect(
        this.element.querySelector('.site-preview-import p').textContent.trim()
      ).to.equal('second description');

      await click('.btn-prev');
      expect(
        this.element.querySelector('.site-preview-import h1').textContent.trim()
      ).to.equal('first item');
      expect(
        this.element.querySelector('.site-preview-import p').textContent.trim()
      ).to.equal('first description');
    });

    it('triggers the import when the button is pressed', async function () {
      server.get(`/mock-server/mapfiles/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            runId: '',
            time: '',
            ping: '',
            ignored: '',
            total: '',
            errors: '',
            sent: '',
            duration: '',
            processed: '',
            status: 'success',
            log: '',
          }),
        ];
      });
      server.get(`/mock-server/mapfiles/:id/log`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({ log: 'message' }),
        ];
      });

      await visit(`/wizard/import/file/${mapFile._id}/preview`);

      await waitFor('.btn-import');

      await click('.btn-import');

      expect(imported).to.equal(true);
      await PageElements.wait(100);
    });
  });
});
