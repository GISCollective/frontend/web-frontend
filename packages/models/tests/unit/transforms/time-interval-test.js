/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { expect } from 'chai';

import { setupTest, it } from 'dummy/tests/helpers';

module('Unit | Transform | time interval', function (hooks) {
  setupTest(hooks);
  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:time-interval');
  });

  it('exists', function (assert) {
    assert.ok(transform);
  });

  it('converts an undefined to a interval in the past', function (a) {
    let deserialized = transform.deserialize(undefined);

    expect(deserialized.begin).to.equalDate(new Date('0001-01-01T00:00:00'));
    expect(deserialized.end).to.equalDate(new Date('0001-01-01T00:00:00'));
  });

  it('can transform an interval with begin and end', function (a) {
    let deserialized = transform.deserialize({
      begin: '2022-01-01T01:00:00',
      end: '2022-01-03T01:00:00',
    });

    expect(deserialized.begin).to.equalDate(new Date('2022-01-01T01:00:00'));
    expect(deserialized.end).to.equalDate(new Date('2022-01-03T01:00:00'));
  });

  it('can serialize an interval with begin and end', function (a) {
    let deserialized = transform.deserialize({
      begin: '2022-01-01T01:00:00Z',
      end: '2022-01-03T01:00:00Z',
    });

    a.deepEqual(deserialized.toJSON(), {
      begin: '2022-01-01T01:00:00.000Z',
      end: '2022-01-03T01:00:00.000Z',
    });
  });
});
