/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe ('Unit | Controller | wizard/import/file-fields',  function (hooks) {
  setupTest(hooks);

  hooks.beforeEach(function() {
    const intl = this.owner.lookup('service:intl');
    intl.setLocale(['en-us']);
  });

  it('is in an error state when the file has no option fields', function () {
    let controller = this.owner.lookup('controller:wizard/import/file-fields');

    const alertList = controller.alertList;

    expect(alertList).to.have.length(1);
    expect(alertList[0]).to.deep.contain({
      level: 'danger',
      id: 'file-not-analyzed-yet',
    });
  });

  it('is in an warning state when the file was already imported', function () {
    let controller = this.owner.lookup('controller:wizard/import/file-fields');
    controller.model = {
      file: {
        map: '5e2f42c96e290a010057dd95',
        name: 'paraexportarrio.csv',
        _id: '602d6c1d4ed8740100236aae',
        size: 13270,
        canEdit: true,
        file: 'http://localhost:9091/mapfiles/602d6c1d4ed8740100236aae/file',
        meta: {
          status: 'success',
        },
        options: {
          uuid: '',
          fields: [
            { key: 'name', destination: 'name' },
            { key: 'position.lat', destination: 'position.lat' },
            { key: 'position.lon', destination: 'position.lon' },
          ],
        },
      },
    };

    const alertList = controller.alertList;

    expect(alertList).to.have.length(1);
    expect(alertList[0]).to.deep.contain({
      level: 'warning',
      id: 'file-already-imported',
    });
  });
});
