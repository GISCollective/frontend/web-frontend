/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class InputOptionsFrameComponent extends Component {
  @tracked isClosed;

  @action
  toggleClose() {
    this.isClosed = !this.isClosed;
  }
}
