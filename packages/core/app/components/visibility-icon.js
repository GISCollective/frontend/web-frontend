/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class VisibilityIconComponent extends Component {
  get isPending() {
    return this.args.value === -1;
  }

  get isPrivate() {
    return this.args.value === false || this.args.value === 0;
  }
}
