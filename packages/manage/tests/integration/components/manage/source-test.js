/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | manage/source', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let campaign;
  let dataBinding;
  let baseMap;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    campaign = server.testData.storage.addDefaultCampaign();
    dataBinding = server.testData.storage.addDefaultDataBinding();
    baseMap = server.testData.storage.addDefaultBaseMap();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<Manage::Source />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when there is a value with no type', async function () {
    this.set('value', {});

    await render(hbs`<Manage::Source @value={{this.value}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the campaign name and answer id when the type is Campaign', async function () {
    this.set('value', {
      type: 'Campaign',
      modelId: campaign._id,
      remoteId: '000000000000000000000001',
    });

    await render(hbs`<Manage::Source @value={{this.value}}/>`);

    expect(this.element.querySelector('.btn-campaign').textContent.trim()).to.equal('Campaign 1');

    expect(
      this.element
        .querySelector('.source-remote-id')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('Answer 000000000000000000000001');
  });

  it('renders the data binding name and remote id when the type is DataBinding', async function () {
    this.set('value', {
      type: 'DataBinding',
      modelId: dataBinding._id,
      remoteId: '000000000000000000000001',
    });

    await render(hbs`<Manage::Source @value={{this.value}}/>`);

    expect(this.element.querySelector('.btn-data-binding').textContent.trim()).to.equal('test');

    expect(
      this.element
        .querySelector('.source-remote-id')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('000000000000000000000001');
  });

  it('renders the base map name when the type is BaseMap', async function () {
    this.set('value', {
      type: 'BaseMap',
      modelId: baseMap._id,
      remoteId: '000000000000000000000001',
    });

    await render(hbs`<Manage::Source @value={{this.value}}/>`);

    expect(this.element.querySelector('.btn-base-map').textContent.trim()).to.equal('name of basemap');

    expect(this.element.querySelector('.source-remote-id')).not.to.exist;
  });

  it('renders the all fields for a generic source', async function () {
    this.set('value', {
      type: 'a',
      modelId: 'b',
      remoteId: '000000000000000000000001',
    });

    await render(hbs`<Manage::Source @value={{this.value}}/>`);

    expect(
      this.element
        .querySelector('.source-type')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('Type a');
    expect(
      this.element
        .querySelector('.source-model-id')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('Model id b');
    expect(
      this.element
        .querySelector('.source-remote-id')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('Remote id 000000000000000000000001');

    expect(this.element.querySelector('.source-sync-at')).not.to.exist;
  });

  it('renders the sync at field when is set', async function () {
    this.set('value', {
      type: 'a',
      modelId: 'b',
      remoteId: '000000000000000000000001',
      syncAt: '2022-04-03T00:22:32Z',
    });

    await render(hbs`<Manage::Source @value={{this.value}}/>`);

    expect(
      this.element
        .querySelector('.source-sync-at')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('sync on  4/3/2022 2:22:32 AM');
  });

  it('does not render the campaign when the id does not exist', async function () {
    this.set('value', {
      type: 'Campaign',
      modelId: 'missing',
      remoteId: 'remote-id',
    });

    await render(hbs`<Manage::Source @value={{this.value}}/>`);

    expect(
      this.element
        .querySelector('.source-type')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('Type Campaign');

    expect(
      this.element
        .querySelector('.source-remote-id')
        .textContent.split('\n')
        .map((a) => a.trim())
        .join(' ')
        .trim(),
    ).to.equal('Remote id remote-id');
  });
});
