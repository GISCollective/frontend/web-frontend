import { fetch, fetchSourceList, PageColType, SpaceType } from "./fetch-source";
import { StoreType } from "./StoreType";

let mockStore: StoreType;

beforeEach(() => {
  mockStore = {
    query: jest.fn(),
    peekRecord: jest.fn(),
    findRecord: jest.fn(),
    queryRecord: jest.fn(),
  };
});

describe("fetchSourceRecord", () => {
  let space: SpaceType;
  let pageCol: PageColType;

  beforeEach(() => {
    mockStore = {
      query: jest.fn(),
      peekRecord: jest.fn(),
      findRecord: jest.fn(),
      queryRecord: jest.fn(),
    };

    space = {
      get: function (key: string) {
        if (key in this) {
          return this[key];
        }

        throw new Error(`not implemented ${key}`);
      },
      teamId: "",
      defaultModels: {
        map: "1",
      },
      visibility: {
        fetchTeam: () => ({ id: "team-id" }),
      },
    };

    pageCol = {
      name: "0.0.0",
      data: {
        source: {
          model: "map",
          useDefaultModel: true,
          useSelectedModel: false,
        },
      },
      type: "",
      query: {},
    };
  });

  describe("with the default flag", () => {
    it("returns a default map when it is a source", async () => {
      mockStore.findRecord.mockReturnValue(Promise.resolve({ id: "1" }));

      const result = await fetch({}, pageCol, space, mockStore);

      expect(result.id).toEqual(space.defaultModels.map);
    });

    it("returns null when there is no source id", async () => {
      space.defaultModels = {};

      pageCol.data = {
        source: {
          model: "map",
          useDefaultModel: true,
          useSelectedModel: false,
        },
      };

      const result = await fetch({}, pageCol, space, mockStore);

      expect(result).toBeFalsy();
    });

    it("returns the space team when the default team is a source", async () => {
      pageCol.data = {
        source: {
          model: "team",
          useDefaultModel: true,
          useSelectedModel: false,
        },
      };

      const result = await fetch({}, pageCol, space, mockStore);

      expect(result.id).toEqual("team-id");
    });
  });

  describe("restoreLegacySource", () => {
    it("fixes the source and returns a bucket list", async () => {
      pageCol.data = {
        model: "map",
        ids: ["1"],
      };

      await fetch({}, pageCol, space, mockStore);

      expect(pageCol.data).toEqual({
        source: {
          ids: ["1"],
          model: "map",
        },
      });
    });
  });

  describe("fetchSourceListPageCol", () => {
    it("returns a bucket list when the ids key is present", async function () {
      pageCol.data = {
        source: {
          model: "map",
          ids: ["1"],
        },
      };

      pageCol.query = { ids: ["1"] };
      mockStore.peekRecord.mockReturnValue({ id: "1" });

      const result = await fetch({}, pageCol, space, mockStore);

      expect(result.items[0].id).toEqual("1");
    });
  });

  describe("fetchSourceRecord", function () {
    it("returns a record when the id key is present", async function () {
      pageCol.data = {
        source: {
          model: "team",
          id: "1",
        },
      };

      mockStore.queryRecord.mockReturnValue({ id: "1" });

      const result = await fetch({}, pageCol, space, mockStore);

      expect(result.id).toEqual("1");
      expect(mockStore.queryRecord).toHaveBeenCalledWith("team", { id: "1" });
    });
  });

  describe("fetchProperty", function () {
    it("returns a record when the id key is present", async function () {
      let map = {
        cover: "cover-1",
      };

      pageCol.data = {
        source: {
          property: "cover",
          useSelectedModel: true,
        },
      };

      const result = await fetch({ selectedModel: map }, pageCol, space, mockStore);

      expect(result).toEqual("cover-1");
    });
  });
});

describe("fetchSourceList", () => {
  it("sets the selected model id when the source is 'selected-model' as object", async () => {
    mockStore.query.mockReturnValue([{ id: "about" }, { id: "5ca78e2160780601008f69e6" }]);

    const modelData = { selectedId: "some-id" };
    const result = await fetchSourceList<{ id: string }>(
      "article",
      { parameters: { key: { source: "selected-model" } } },
      modelData,
      {} as SpaceType,
      mockStore,
    );

    expect(result.model).toEqual("article");

    expect(result.query).toEqual({
      key: "some-id",
    });

    expect(result.items.map((a) => a.id)).toEqual(["about", "5ca78e2160780601008f69e6"]);
  });

  it("sets the query value when the source is not the current model or a state", async () => {
    const modelData = { "state-category": "some-category" };
    const result = await fetchSourceList(
      "article",
      { parameters: { key: "some value" } },
      modelData,
      {} as SpaceType,
      mockStore,
    );

    expect(result.model).toEqual("article");
    expect(result.query).toEqual({
      key: "some value",
    });
  });

  it("queries a list of ids", async () => {
    mockStore.findRecord = (model, id) => Promise.resolve({ model, id } as any);

    const modelData = { "state-category": "some-category" };
    const result = await fetchSourceList("article", { ids: ["1", "2", "3"] }, modelData, {} as SpaceType, mockStore);

    expect(result.model).toEqual("article");
    expect(result.query).toEqual({});
    expect(result.items).toEqual([
      {
        id: "1",
        model: "article",
      },
      {
        id: "2",
        model: "article",
      },
      {
        id: "3",
        model: "article",
      },
    ]);
  });

  it("ignores missing ids", async () => {
    mockStore.findRecord = (model, id): any => {
      if (id == "2") {
        throw new Error("missing");
      }
      return Promise.resolve({ model, id });
    };

    const modelData = { "state-category": "some-category" };
    const result = await fetchSourceList("article", { ids: ["1", "2", "3"] }, modelData, {} as SpaceType, mockStore);

    expect(result.model).toEqual("article");
    expect(result.query).toEqual({});
    expect(result.items).toEqual([
      {
        id: "1",
        model: "article",
      },
      {
        id: "3",
        model: "article",
      },
    ]);
  });
});
