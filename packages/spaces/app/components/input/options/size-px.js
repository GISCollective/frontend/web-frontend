/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class InputSizePxComponent extends Component {
  elementId = `input-size-px-${guidFor(this)}`;

  @tracked _value;

  emptyStyle() {
    return {
      sm: {},
      md: {},
      lg: {},
    };
  }

  get value() {
    if (this._value !== undefined) {
      return this._value;
    }

    if (typeof this.args.value == "object") {
      return this.args.value;
    }

    return this.emptyStyle();
  }

  get numericValues() {
    const currentValue = this.value;
    const result = {};

    for (const size of ["sm", "md", "lg"]) {
      const value = currentValue[size]?.[this.args.property] ?? "";

      result[size] = value.replace("px", "");
    }

    return result;
  }

  @action
  updateValue(size, value) {
    if (!this._value) {
      this._value = this.value;
    }

    if (!this._value[size]) {
      this._value[size] = {};
    }

    this._value[size][this.args.property] = `${value}px`;

    this.args.onChange?.(this._value);
  }
}
