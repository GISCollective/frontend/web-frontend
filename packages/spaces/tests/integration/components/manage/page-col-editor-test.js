/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import { fillIn } from "@ember/test-helpers";
import click from "@ember/test-helpers/dom/click";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | manage/page-col-editor", function (hooks) {
  setupRenderingTest(hooks);
  let pageData;
  let spaceData;
  let page;
  let space;
  let store;
  let server;
  let picture;

  hooks.before(async function () {
    server = new TestServer();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup("service:store");
    pageData = server.testData.storage.addDefaultPage();
    spaceData = server.testData.storage.addDefaultSpace();
    picture = server.testData.storage.addDefaultPicture("5cc8dc1038e882010061545a");
    spaceData.cols = {};

    page = await store.findRecord("page", pageData._id);
    space = await store.findRecord("space", spaceData._id);

    server.put(`/mock-server/pictures/:id`, (request) => {
      const picture = JSON.parse(request.requestBody);
      picture.picture._id = request.params.id;

      return [200, { "Content-Type": "application/json" }, JSON.stringify(picture)];
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("lists the default category components", async function () {
    await render(hbs`<Manage::PageColEditor />`);

    await selectSearch(".ember-power-select-trigger", "");

    const buttons = this.element.querySelectorAll(".label");

    const labels = [...buttons].map((a) => a.textContent);

    expect(labels.length).not.to.equal(0);
  });

  it("triggers on change with the component name when a button is pressed", async function () {
    let value = "";
    this.set("change", (_, v) => {
      value = v;
    });

    await render(hbs`<Manage::PageColEditor @onChange={{this.change}} />`);

    await selectSearch(".ember-power-select-trigger", "title-with-buttons");
    await selectChoose(".ember-power-select-trigger", "title with buttons");

    expect(value).to.equal("title-with-buttons");
  });

  it("renders the button as selected when the value matches the name", async function () {
    this.set("value", {
      type: "title-with-buttons",
    });
    await render(hbs`<Manage::PageColEditor @value={{this.value}} />`);

    const label = this.element.querySelector(".ember-power-select-trigger .label");

    expect(label.textContent.trim()).to.equal("title with buttons");
  });

  it("allows to unselect a component", async function () {
    let called;
    this.set("remove", () => {
      called = true;
    });

    await render(hbs`<Manage::PageColEditor @onRemoveCol={{this.remove}} />`);

    await click(".btn-none");

    expect(called).to.equal(true);
    expect(this.element.querySelector(".btn-to-global-page-col")).not.to.exist;
  });

  it("shows a button with the selected component name", async function () {
    this.set("value", {
      type: "title-with-buttons",
    });

    await render(hbs`<Manage::PageColEditor @value={{this.value}} />`);

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "title with buttons",
    );
  });

  it("does not render the convert to global component when there is no space argument", async function () {
    this.set("value", {
      type: "title-with-buttons",
    });

    await render(hbs`<Manage::PageColEditor @value={{this.value}} />`);
    expect(this.element.querySelector(".btn-to-global-page-col")).not.to.exist;
  });

  it("can be made global, a local page-col", async function () {
    this.set("value", {
      type: "title-with-buttons",
      data: {},
    });

    let value;
    let type;
    let id;

    this.set("change", (v, t, i) => {
      value = v;
      type = t;
      id = i;
    });

    this.set("model", { space: { hasPageCol: () => false } });

    await render(hbs`<Manage::PageColEditor @model={{this.model}} @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelector(".btn-to-global-page-col")).to.exist;
    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "title with buttons",
    );

    await click(".btn-to-global-page-col");
    await fillIn(".input-global-page-col-id", "main-menu");
    await click(".btn-submit");

    expect(value).to.deep.equal({});
    expect(type).to.equal("title-with-buttons");
    expect(id).to.equal("main-menu");
  });

  it("can be made local, a global page-col", async function () {
    space.cols.setPageCol("main-title", "heading", {
      style: {
        classes: [],
        color: "",
        heading: 1,
        text: "some test",
      },
    });

    this.set("value", {
      type: "title-with-buttons",
      gid: "main-title",
      data: {},
    });

    let value;
    let type;
    let id;

    this.set("change", (v, t, i) => {
      value = v;
      type = t;
      id = i;
    });

    this.set("model", {
      page,
      space,
    });

    await render(hbs`<Manage::PageColEditor @value={{this.value}} @onChange={{this.change}} @model={{this.model}} />`);

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("main title");

    expect(this.element.querySelector(".btn-to-global-page-col")).not.to.exist;
    await click(".btn-remove-gid");

    expect(value).to.deep.equal({});
    expect(type).to.equal("title-with-buttons");
    expect(id).to.equal(null);
  });
});
