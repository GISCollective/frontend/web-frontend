/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class PageColFeatureIssueButtonComponent extends Component {
  @service router;
  @tracked record;
  @tracked _button;

  get type() {
    return (
      this._button?.issueType ??
      this.args.value?.data?.button?.issueType ??
      'generic'
    );
  }

  get link() {
    const next = encodeURIComponent(this.router.currentURL);

    return `/add/issue?feature=${this.record?.id}&next=${next}&type=${this.type}`;
  }

  get text() {
    return this.type == 'photo' ? 'suggest a photo' : 'report a problem';
  }

  @action
  updateButtonProps(event) {
    this._button = event.detail.value;
  }

  @action
  setup(record) {
    this.record = record;
  }
}
