import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';
import { action } from '@ember/object';

export default class InputTextEmailListComponent extends Component {
  @tracked _textValue;

  set textValue(value) {
    this._textValue = value;

    debounce(this, this.triggerChange, 500);
  }

  get textValue() {
    if (typeof this._textValue == 'string') {
      return this._textValue;
    }

    if (Array.isArray(this.args.value)) {
      return this.args.value.join('\n');
    }

    return '';
  }

  get value() {
    return this.textValue
      .split(/[ ,\n]+/)
      .map((a) => a.trim().toLowerCase())
      .filter((a) => a);
  }

  get emailCount() {
    return this.value.length;
  }

  @action
  triggerChange() {
    this.args.onChange?.(this.value);
  }
}
