/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageDeleteButtonComponent extends Component {
  @service notifications;
  @service intl;

  get answers() {
    let deleteOptions = this.args.deleteOptions || 'yes:' + this.intl.t('yes');

    return deleteOptions.split(',').map((a) => {
      const pieces = a.split(':');

      return { text: pieces[1], key: pieces[0], class: 'btn-danger' };
    });
  }

  @action
  async click() {
    const response = await this.notifications.ask({
      title: this.intl.t(`delete ${this.args.modelName}`, { size: 1 }),
      question: this.intl.t('delete-confirmation-message', {
        name: this.args.name,
        size: 1,
      }),
      answers: this.answers,
    });

    if (response != 'yes') {
      return;
    }

    await this.args.onDelete(response);
  }
}
