/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/attributes-group', function (hooks) {
  let featureData;
  let server;

  setupRenderingTest(hooks);

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    featureData = server.testData.storage.addDefaultFeature();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<View::AttributesGroup />`);

    assert.dom().hasText('');
  });

  it('renders a group when set', async function (assert) {
    featureData.attributes = {
      icon: {
        property: 'value',
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      modelKey: 'feature',
      data: {
        source: { model: 'feature', id: '1' },
        sourceAttribute: {
          groups: ['icon'],
        },
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        attributeStyle: {
          paragraphStyle: {
            color: 'red',
            classes: ['fw-light'],
          },
        },
      },
    });

    this.set('model', {
      feature,
    });

    await render(
      hbs`<View::AttributesGroup @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(
      this.element.querySelector('.attribute-title').textContent.trim(),
    ).to.equal('property');
    expect(this.element.querySelector('.attribute-title')).to.have.classes(
      'text-with-options display-3 fw-bold text-green',
    );

    expect(
      this.element.querySelector('.attribute-value').textContent.trim(),
    ).to.equal('value');
    expect(this.element.querySelector('.attribute-value')).to.have.classes(
      'text-with-options fw-light text-red',
    );
  });

  it('renders a boolean value', async function (assert) {
    featureData.attributes = {
      icon: {
        property: true,
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      modelKey: 'feature',
      data: {
        source: { model: 'feature', id: '1' },
        sourceAttribute: {
          groups: ['icon'],
        },
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        attributeStyle: {
          color: 'red',
          classes: ['fw-light'],
        },
      },
    });

    this.set('model', {
      feature,
    });

    await render(
      hbs`<View::AttributesGroup @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(
      this.element.querySelector('.attribute-value').textContent.trim(),
    ).to.equal('yes');
  });

  it('renders a text value with an url', async function (assert) {
    featureData.attributes = {
      icon: {
        property: 'some text https://giscollective.com',
      },
    };

    let feature = await this.store.findRecord('feature', featureData._id);

    this.set('value', {
      modelKey: 'feature',
      data: {
        source: { model: 'feature', id: '1' },
        sourceAttribute: {
          groups: ['icon'],
        },
        title: {
          text: 'some title',
          heading: 2,
          color: 'green',
          classes: ['display-3', 'fw-bold'],
        },
        attributeStyle: {
          color: 'red',
          classes: ['fw-light'],
        },
      },
    });

    this.set('model', {
      feature,
    });

    await render(
      hbs`<View::AttributesGroup @value={{this.value}} @model={{this.model}}/>`,
    );

    expect(
      this.element.querySelector('.attribute-value').textContent.trim(),
    ).to.equal('some text https://giscollective.com');

    const link = this.element.querySelector('.attribute-value a');
    expect(link.textContent.trim()).to.equal('https://giscollective.com');
    expect(link).to.have.attribute('href', 'https://giscollective.com');
    expect(link).to.have.attribute('target', '_blank');
  });
});
