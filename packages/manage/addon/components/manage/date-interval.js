/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageDateIntervalComponent extends Component {
  @tracked _startDate;
  @tracked _endDate;

  get startDate() {
    if (this._startDate) {
      return this._startDate;
    }

    return this.args.value?.startDate;
  }

  get endDate() {
    if (this._endDate) {
      return this._endDate;
    }

    return this.args.value?.endDate;
  }

  isValidDate(value) {
    return value?.getFullYear?.() > 1990;
  }

  get hasBothValues() {
    return this.isValidDate(this.args.value?.startDate) && this.isValidDate(this.args.value?.endDate);
  }

  get hasNoValue() {
    return !this.isValidDate(this.args.value?.startDate) && !this.isValidDate(this.args.value?.endDate);
  }

  get hasOnlyStart() {
    return this.isValidDate(this.args.value?.startDate) && !this.isValidDate(this.args.value?.endDate);
  }

  get hasOnlyEnd() {
    return !this.isValidDate(this.args.value?.startDate) && this.isValidDate(this.args.value?.endDate);
  }

  @action
  changeStartDate(value) {
    this._startDate = value;
  }

  @action
  changeEndDate(value) {
    this._endDate = value;
  }

  @action
  save() {
    return this.args?.onSave?.(this.startDate, this.endDate);
  }

  @action
  cancel() {
    this._startDate = null;
    this._endDate = null;

    return this.args?.onCancel?.();
  }
}
