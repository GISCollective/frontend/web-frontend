/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { CalendarEntry } from 'models/transforms/calendar-entry-list';
import { action } from '@ember/object';
import { DateTime } from 'luxon';
export default class InputCalendarEntryListComponent extends Component {
  @action
  factoryValue() {
    const now = new Date();
    now.setSeconds(0, 0);

    const timezone = DateTime.local().zoneName;

    return new CalendarEntry({ begin: now, end: now, repetition: 'norepeat', timezone });
  }
}
