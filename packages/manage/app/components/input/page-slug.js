/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class InputPagePathComponent extends Component {
  elementId = `layout-options-${guidFor(this)}`;
  @tracked _path = null;

  validVars = [
    ':feature-id',
    ':map-id',
    ':campaign-id',
    ':icon-id',
    ':icon-set-id',
    ':article-id',
    ':event-id',
    ':calendar-id',
  ];

  get path() {
    if (this._path !== null) {
      return this._path;
    }

    const slug = this.args.value ?? '';
    const pieces = slug.split('--').filter((a) => a != '');

    return `/${pieces.join('/')}`;
  }

  set path(value) {
    this._path = value;

    this.args.onChange?.(this.slug);
  }

  get slug() {
    return this.path
      .split('/')
      .map((a) => a.trim())
      .filter((a) => a != '')
      .join('--');
  }

  get hasInvalidSlug() {
    if (this.path.substring(0, 1) != '/') {
      return true;
    }

    return this.slug != this.path.substring(1).split('/').join('--');
  }

  get invalidVariables() {
    return this.path
      .split('/')
      .filter((a) => a.trim() != '')
      .filter((a) => a.substring(0, 1) == ':')
      .filter((a) => this.validVars.indexOf(a) == -1);
  }

  get validVariables() {
    return this.path
      .split('/')
      .filter((a) => a.trim() != '')
      .filter((a) => a.substring(0, 1) == ':')
      .filter((a) => this.validVars.indexOf(a) >= 0);
  }

  @action
  fixSlug() {
    this.path = `/` + this.slug.split('--').join('/');
  }
}
