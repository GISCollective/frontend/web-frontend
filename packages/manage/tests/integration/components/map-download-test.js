/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/map/download', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the csv and geojson for a map that can not be edited', async function (a) {
    this.set('map', {
      id: 'id-123',
    });

    await render(hbs`<MapDownload @map={{this.map}} />`);

    let labels = [];
    let links = [];
    this.element.querySelectorAll('.dropdown-item').forEach((element) => {
      labels.push(element.textContent.trim());

      if (element.attributes.getNamedItem('href')) {
        links.push(element.attributes.getNamedItem('href').value.trim());
      }
    });

    a.deepEqual(labels, ['CSV', 'GeoJson']);
    a.deepEqual(links, [
      '/mock-server/features?&map=id-123&format=csv',
      '/mock-server/features?&map=id-123&format=geojson',
    ]);
  });

  it('renders the csv, geojson and geopackage for a map that can be edited', async function () {
    this.set('map', {
      id: 'id-123',
      canEdit: true,
    });

    await render(hbs`<MapDownload @map={{this.map}} />`);

    let labels = [];
    let links = [];
    this.element.querySelectorAll('.dropdown-item').forEach((element) => {
      labels.push(element.textContent.trim());

      if (element.attributes.getNamedItem('href')) {
        links.push(element.attributes.getNamedItem('href').value.trim());
      }
    });

    expect(labels).to.deep.equal(['CSV', 'GeoJson', 'GeoPackage']);
    expect(links).to.deep.equal([
      '/mock-server/features?&map=id-123&format=csv',
      '/mock-server/features?&map=id-123&format=geojson',
      '/mock-server/maps/id-123/geopackage',
    ]);
  });
});
