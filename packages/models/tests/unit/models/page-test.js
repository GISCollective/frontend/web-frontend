/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { LayoutContainer } from 'dummy/transforms/layout-container-list';
import { LayoutContainerMap } from 'dummy/transforms/layout-container-map';
import TestServer from 'models/test-support/gis-collective/test-server';
import { PageCol } from 'models/transforms/page-col-list';

describe('Unit | Model | page', function (hooks) {
  setupTest(hooks, {}, this);

  let store;
  let server;

  hooks.beforeEach(function () {
    store = this.owner.lookup('service:store');
    server = new TestServer();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('exists', function () {
    let model = store.createRecord('page', {});
    expect(model).to.be.ok;
  });

  describe('shouldSave', function (hooks) {
    let model;

    hooks.beforeEach(function () {
      server.post('/mock-server/pages', (request) => {
        const page = JSON.parse(request.requestBody);
        page.page._id = '1';

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(page)];
      });

      server.put('/mock-server/pages/1', (request) => {
        const page = JSON.parse(request.requestBody);
        page.page._id = '1';

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(page)];
      });

      model = store.createRecord('page', {
        cols: [],
        layoutContainers: [],
      });
    });

    it('returns true for a new object', function () {
      expect(model.shouldSave).to.equal(true);
    });

    it('returns false after a model is saved', async function () {
      await model.save();

      expect(model.shouldSave).to.equal(false);
    });

    it('returns false after the name is changed', async function () {
      await model.save();

      model.name = 'test';
      expect(model.shouldSave).to.equal(true);
    });

    it('returns true after a model shouldSave is set to true', async function () {
      await model.save();
      model.shouldSave = true;

      expect(model.shouldSave).to.equal(true);

      await model.save();

      expect(model.shouldSave).to.equal(false);
    });
  });

  describe('exists col', function (hooks) {
    let model;

    hooks.beforeEach(function () {
      model = store.createRecord('page', {
        cols: [],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [{ name: '0.0.0', componentCount: 2 }],
                name: '0.0',
              },
            ],
          }),
        ],
      });
    });

    it('returns true when there is col with the queried name', function () {
      expect(model.existsCol('0.0.0')).to.equal('col');
    });

    it('returns false when there is no col with the queried name', function () {
      expect(model.existsCol('other')).to.equal(false);
    });

    it('returns true when there is a row with the queried name', function () {
      expect(model.existsCol('0.0')).to.equal('row');
    });

    it('returns true when the name is an integer < number of containers', function () {
      expect(model.existsCol('0')).to.equal('container');
    });

    it('returns false when the name is an integer >= number of containers', function () {
      expect(model.existsCol('1')).to.equal(false);
    });

    it('returns true when name matches a sub component', function () {
      expect(model.existsCol('0.0.0.1')).to.equal('col');
    });

    it('returns true when it matches a container layer', function () {
      expect(model.existsCol('0.layer-1')).to.equal('container');
    });
  });

  describe('upsert pageCol', function () {
    it('creates a new page col when it does not exist but the layout exist', function () {
      let model = store.createRecord('page', {
        cols: [],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [{ name: '0.0.0' }],
                name: '0.0',
              },
            ],
          }),
        ],
      });

      const col = model.upsertPageColByName('0.0.0');

      expect(col.name).to.equal('0.0.0');
      expect(model.cols.length).to.equal(1);
      expect(col).to.equal(model.cols[0]);
    });

    it('returns the existing page col when it exists', function () {
      let existingCol = new PageCol({ name: '0.0.0' });

      let model = store.createRecord('page', {
        cols: [existingCol],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [{ name: '0.0.0' }],
                name: '0.0',
              },
            ],
          }),
        ],
      });

      const col = model.upsertPageColByName('0.0.0');

      expect(col).to.equal(existingCol);
      expect(model.cols.length).to.equal(1);
      expect(col).to.equal(model.cols[0]);
    });

    it('throws when there is no layout col with the name', function () {
      let model = store.createRecord('page', {
        cols: [],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [{ name: '0.0.0' }],
                name: '0.0',
              },
            ],
          }),
        ],
      });

      expect(() => model.upsertPageColByName('missing')).to.throw('');
    });
  });

  describe('rename col', function () {
    it('renames the matched columns and layout', function (a) {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [{ name: '0.0.0' }],
                name: '0.0',
              },
            ],
          }),
        ],
      });

      model.renameCol('0.0.0', 'new name');

      const layoutContainers = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(layoutContainers, [
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: 'new name',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);

      const cols = model.cols.map((a) => a.toJSON());
      expect(cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'new name',
          type: undefined,
          data: {},
        },
      ]);
    });

    it('renames the matched rows layouts', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                data: {},
                cols: [{ name: '0.0.0' }],
                name: '0.0',
              },
            ],
          }),
        ],
      });

      model.renameCol('0.0', 'new name');

      const layoutContainers = model.layoutContainers.map((a) => a.toJSON());
      expect(layoutContainers).to.deep.equal([
        {
          rows: [
            {
              data: {},
              name: 'new name',
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);

      const cols = model.cols.map((a) => a.toJSON());
      expect(cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0.0.0',
          type: undefined,
          data: {},
        },
      ]);
    });

    it('throws when the source column name does not exist', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                data: {},
                cols: [{ name: '0.0.0' }],
                name: '0.0',
              },
            ],
          }),
        ],
      });

      expect(() => model.renameCol('missing', 'new name')).to.throw(
        'The source name "missing" does not match any column.',
      );
    });

    it('throws when the destination column name exist', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                data: {},
                cols: [{ name: '0.0.0' }],
                name: '0.0',
              },
            ],
          }),
        ],
      });

      expect(() => model.renameCol('0.0.0', '0.0.0')).to.throw('The destination name "0.0.0" is already used.');
    });

    it('updates the space global containers when the container has a gid', function (a) {
      let space = store.createRecord('space', {
        layoutContainers: new LayoutContainerMap({
          'global-container': {
            gid: 'global-container',
            layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
            rows: [{ name: '0.0', cols: [{ name: '0.0.0' }], options: [] }, { cols: [{ data: {} }] }],
          },
        }),
      });

      let model = store.createRecord('page', {
        space,
        layoutContainers: [new LayoutContainer(space.layoutContainers.map['global-container'])],
      });

      model.renameCol('0.0', 'new name');

      a.deepEqual(space.layoutContainers.map['global-container'].toJSON(), {
        rows: [
          {
            name: '0.0',
            data: {},
            cols: [
              {
                type: '',
                data: {},
                name: '0.0.0',
                componentCount: 1,
                options: [],
              },
            ],
            options: [],
          },
          {
            name: '',
            data: {},
            cols: [{ type: '', data: {}, name: '', componentCount: 1, options: [] }],
            options: [],
          },
        ],
        options: [],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        gid: 'global-container',
      });
    });
  });

  describe('delete container', function () {
    it('throws an error when a missing container is deleted', function () {
      let model = store.createRecord('page', {});

      expect(() => model.deleteContainer(0)).to.throw("The container '0' does not exist.");
    });

    it('removes a container from the list', function () {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteContainer(0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([]);
    });

    it('updates the container indexes', function () {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({}),
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '1.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '1.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteContainer(0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('removes cols matching with the container', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
            visibility: 'always',
            layers: { count: 1, effect: 'none', showContentAfter: 1 },
          }),
        ],
      });

      model.deleteContainer(0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([]);
    });

    it('updates the cols names to match the new layout', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '1.0.0' })],
        layoutContainers: [
          new LayoutContainer({}),
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
            visibility: 'always',
          }),
        ],
      });

      model.deleteContainer(0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0.0.0',
          type: undefined,
          data: {},
        },
      ]);
    });
  });

  describe('delete row', function () {
    it('throws an error when a missing row is deleted', function () {
      let model = store.createRecord('page', {});

      expect(() => model.deleteRow(0, 1)).to.throw("The row '0.1' does not exist.");
    });

    it('removes a row from the list', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteRow(0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('updates the row indexes', function () {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.1.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.1',
                options: [],
              },
            ],
            options: [],
          }),
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '1.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '1.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteRow(0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
        {
          rows: [
            {
              name: '1.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '1.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('removes cols matching with the container and row', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteRow(0, 0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([]);
    });

    it('updates the cols names to match the new layout', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.1.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.1.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.1',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteRow(0, 0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0.0.0',
          type: undefined,
          data: {},
        },
      ]);
    });

    it('updates the space global containers when the container has a gid', function (a) {
      let space = store.createRecord('space', {
        layoutContainers: new LayoutContainerMap({
          'global-container': {
            gid: 'global-container',
            layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
            rows: [{ name: '0.0', cols: [], options: [] }, { cols: [{ data: {} }] }],
          },
        }),
      });

      let model = store.createRecord('page', {
        space,
        layoutContainers: [new LayoutContainer(space.layoutContainers.map['global-container'])],
      });

      model.deleteRow(0, 0);

      a.deepEqual(space.layoutContainers.map['global-container'].toJSON(), {
        rows: [
          {
            name: '',
            data: {},
            cols: [{ type: '', data: {}, name: '', componentCount: 1, options: [] }],
            options: [],
          },
        ],
        options: [],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        gid: 'global-container',
      });
    });
  });

  describe('delete col', function () {
    it('throws an error when a missing col is deleted', function () {
      let model = store.createRecord('page', {});

      expect(() => model.deleteCol(0, 1, 0)).to.throw("The col '0.1.0' does not exist.");
    });

    it('removes a col from the list', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteCol(0, 0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              data: {},
              name: '0.0',
              cols: [],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('updates the col indexes', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                  {
                    type: '',
                    data: {},
                    name: '0.0.1',
                    componentCount: 2,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.1.0',
                    componentCount: 3,
                    options: [],
                  },
                ],
                name: '0.1',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteCol(0, 0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              data: {},
              name: '0.0',
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 2,
                  options: [],
                },
              ],
              options: [],
            },
            {
              name: '0.1',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.1.0',
                  componentCount: 3,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('removes cols matching with the container, row and col indexes', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteCol(0, 0, 0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([]);
    });

    it('updates the cols names to match the new layout', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.1' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                  {
                    type: '',
                    data: {},
                    name: '0.0.1',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.deleteCol(0, 0, 0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0.0.0',
          type: undefined,
          data: {},
        },
      ]);
    });

    it('updates the space global containers when the container has a gid', function (a) {
      let space = store.createRecord('space', {
        layoutContainers: new LayoutContainerMap({
          'global-container': {
            gid: 'global-container',
            layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
            rows: [{ name: '0.0', cols: [{ name: '0.0.0' }], options: [] }, { cols: [{ data: {} }] }],
          },
        }),
      });

      let model = store.createRecord('page', {
        space,
        layoutContainers: [new LayoutContainer(space.layoutContainers.map['global-container'])],
      });

      model.deleteCol(0, 0, 0);

      a.deepEqual(space.layoutContainers.map['global-container'].toJSON(), {
        rows: [
          { data: {}, name: '0.0', cols: [], options: [] },
          {
            name: '',
            data: {},
            cols: [{ type: '', data: {}, name: '', componentCount: 1, options: [] }],
            options: [],
          },
        ],
        options: [],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        gid: 'global-container',
      });
    });
  });

  describe('add layout', function () {
    it('can add a new layout column on an empty page', function (a) {
      let model = store.createRecord('page', {});

      model.addLayout(0, 0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              data: {},
              name: '0.0',
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can add a new layout row on an empty page', function (a) {
      let model = store.createRecord('page', {});

      model.addLayout(0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [{ data: {}, name: '0.0', cols: [], options: [] }],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can add a new layout container on an empty page', function () {
      let model = store.createRecord('page', {});

      model.addLayout(0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          rows: [],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('fills in the missing columns on insert', function (a) {
      let model = store.createRecord('page', {});

      model.addLayout(0, 0, 3);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: '',
                  data: {},
                  name: '0.0.1',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: '',
                  data: {},
                  name: '0.0.2',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: '',
                  data: {},
                  name: '0.0.3',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('fills in the missing rows on insert', function (a) {
      let model = store.createRecord('page', {});

      model.addLayout(0, 2, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            { name: '0.0', data: {}, cols: [], options: [] },
            { name: '0.1', data: {}, cols: [], options: [] },
            {
              name: '0.2',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.2.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('fills in the missing containers on insert', function (a) {
      let model = store.createRecord('page', {});

      model.addLayout(2, 0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
        {
          rows: [],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
        {
          rows: [
            {
              name: '2.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '2.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('updates the space global containers when the container has a gid', function (a) {
      let space = store.createRecord('space', {
        layoutContainers: new LayoutContainerMap({
          'global-container': {
            gid: 'global-container',
            layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
            rows: [{ cols: [{ data: {} }] }],
          },
        }),
      });
      let model = store.createRecord('page', {
        space,
        layoutContainers: [new LayoutContainer(space.layoutContainers.map['global-container'])],
      });

      model.addLayout(0, 2, 0);

      a.deepEqual(space.layoutContainers.map['global-container'].toJSON(), {
        rows: [
          {
            name: '',
            data: {},
            cols: [{ type: '', data: {}, name: '', componentCount: 1, options: [] }],
            options: [],
          },
          { name: '0.1', data: {}, cols: [], options: [] },
          {
            name: '0.2',
            data: {},
            cols: [
              {
                type: '',
                data: {},
                name: '0.2.0',
                componentCount: 1,
                options: [],
              },
            ],
            options: [],
          },
        ],
        options: [],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        gid: 'global-container',
      });
    });
  });

  describe('add layout container before', function () {
    it('can add a container on the first index', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutContainerBefore(0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
        {
          rows: [
            {
              name: '1.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '1.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can add a new container between two existing ones', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                options: [],
                name: '0.0',
              },
            ],
            options: [],
          }),
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '1.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '1.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutContainerBefore(1);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
        {
          rows: [],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
        {
          rows: [
            {
              name: '2.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '2.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can not add a new container before a missing one', function () {
      let model = store.createRecord('page', {
        layoutContainers: [],
      });

      expect(() => model.addLayoutContainerBefore(1)).to.throw(
        "Can't add a container because the index '1' does not exist.",
      );
    });

    it('updates a page col name', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutContainerBefore(0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          container: 1,
          row: 0,
          col: 0,
          name: '1.0.0',
          type: undefined,
          data: {},
        },
      ]);
    });

    it('updates a page col name for sub components', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0.1' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 2,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutContainerBefore(0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          container: 1,
          row: 0,
          col: 0,
          name: '1.0.0.1',
          type: undefined,
          data: {},
        },
      ]);
    });
  });

  describe('add layout row before', function () {
    it('can add a row on the first index', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
            visibility: 'always',
            layers: { count: 1, effect: 'none', showContentAfter: 1 },
          }),
        ],
      });

      model.addLayoutRowBefore(0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            { name: '0.0', cols: [], options: [], data: {} },
            {
              name: '0.1',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.1.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can add a new row between two existing ones', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.1.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.1',
                options: [],
              },
            ],
            options: [],
            visibility: 'always',
            layers: { count: 1, effect: 'none', showContentAfter: 1 },
          }),
        ],
      });

      model.addLayoutRowBefore(0, 1);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
            { name: '0.1', data: {}, cols: [], options: [] },
            {
              name: '0.2',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.2.0',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can not add a new row before a missing one', function () {
      let model = store.createRecord('page', {
        layoutContainers: [new LayoutContainer()],
      });

      expect(() => model.addLayoutRowBefore(0, 4)).to.throw("Can't add a row because the index '4' does not exist.");
    });

    it('can not add a new row in a missing container', function () {
      let model = store.createRecord('page', {
        layoutContainers: [],
      });

      expect(() => model.addLayoutRowBefore(1, 4)).to.throw(
        "Can't add a row because the container '1' does not exist.",
      );
    });

    it('updates a page col name', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutRowBefore(0, 0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          container: 0,
          row: 1,
          col: 0,
          name: '0.1.0',
          type: undefined,
          data: {},
        },
      ]);
    });

    it('updates the space global containers when the container has a gid', function (a) {
      let space = store.createRecord('space', {
        layoutContainers: new LayoutContainerMap({
          'global-container': {
            gid: 'global-container',
            layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
            rows: [{ cols: [{ data: {} }] }],
          },
        }),
      });

      let model = store.createRecord('page', {
        space,
        layoutContainers: [new LayoutContainer(space.layoutContainers.map['global-container'])],
      });

      model.addLayoutRowBefore(0, 0);

      a.deepEqual(space.layoutContainers.map['global-container'].toJSON(), {
        rows: [
          { data: {}, name: '0.0', cols: [], options: [] },
          {
            name: '',
            data: {},
            cols: [{ type: '', data: {}, name: '', componentCount: 1, options: [] }],
            options: [],
          },
        ],
        options: [],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        gid: 'global-container',
      });
    });
  });

  describe('add layout col before', function () {
    it('can add a col on the first index', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutColBefore(0, 0, 0);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              data: {},
              name: '0.0',
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.0',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: '',
                  data: {},
                  name: '0.0.1',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can add new col between 2 existing ones', function (a) {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                  {
                    type: '',
                    data: {},
                    name: '0.0.1',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutColBefore(0, 0, 1);

      const jsonList = model.layoutContainers.map((a) => a.toJSON());

      a.deepEqual(jsonList, [
        {
          rows: [
            {
              name: '0.0',
              data: {},
              cols: [
                {
                  type: '',
                  data: {},
                  name: '0.0.1',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: '',
                  data: {},
                  name: '0.0.1',
                  componentCount: 1,
                  options: [],
                },
                {
                  type: '',
                  data: {},
                  name: '0.0.2',
                  componentCount: 1,
                  options: [],
                },
              ],
              options: [],
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
        },
      ]);
    });

    it('can not add new col before a missing one', function () {
      let model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });
      expect(() => model.addLayoutColBefore(0, 0, 0)).to.throw("Can't add a col because the index '0' does not exist.");
    });

    it('updates a page col name', function () {
      let model = store.createRecord('page', {
        cols: [new PageCol({ name: '0.0.0' })],
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });

      model.addLayoutColBefore(0, 0, 0);

      const jsonList = model.cols.map((a) => a.toJSON());

      expect(jsonList).to.deep.equal([
        {
          container: 0,
          row: 0,
          col: 1,
          name: '0.0.1',
          type: undefined,
          data: {},
        },
      ]);
    });

    it('updates the space global containers when the container has a gid', function (a) {
      let space = store.createRecord('space', {
        layoutContainers: new LayoutContainerMap({
          'global-container': {
            gid: 'global-container',
            layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
            rows: [{ cols: [{ data: {} }] }],
          },
        }),
      });

      let model = store.createRecord('page', {
        space,
        layoutContainers: [new LayoutContainer(space.layoutContainers.map['global-container'])],
      });

      model.addLayoutColBefore(0, 0, 0);

      a.deepEqual(space.layoutContainers.map['global-container'].toJSON(), {
        rows: [
          {
            name: '',
            data: {},
            cols: [
              {
                type: '',
                data: {},
                name: '0.0.0',
                componentCount: 1,
                options: [],
              },
              { type: '', data: {}, name: '', componentCount: 1, options: [] },
            ],
            options: [],
          },
        ],
        options: [],
        visibility: 'always',
        layers: { count: 2, effect: 'some-effect', showContentAfter: 3 },
        gid: 'global-container',
      });
    });
  });

  describe('get container for', function (hooks) {
    let model;

    hooks.beforeEach(function () {
      model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 1,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });
    });

    it('returns the container of a column', function () {
      const container = model.getContainerFor('0.0.0');

      expect(container).to.equal(model.layoutContainers[0]);
    });

    it('returns the container of a row', function () {
      const container = model.getContainerFor('0.0');

      expect(container).to.equal(model.layoutContainers[0]);
    });

    it('returns undefined when the name is not found', function () {
      const container = model.getContainerFor('missing');

      expect(container).not.to.exist;
    });
  });

  describe('getLayoutByName', function (hooks) {
    let model;

    hooks.beforeEach(function () {
      model = store.createRecord('page', {
        layoutContainers: [
          new LayoutContainer({
            rows: [
              {
                cols: [
                  {
                    type: '',
                    data: {},
                    name: '0.0.0',
                    componentCount: 10,
                    options: [],
                  },
                ],
                name: '0.0',
                options: [],
              },
            ],
            options: [],
          }),
        ],
      });
    });

    it('returns the col layout', function () {
      const row = model.getLayoutByName('0.0.0');

      expect(row).to.equal(model.layoutContainers[0].rows[0].cols[0]);
    });

    it('returns a col layout when a sub-component is searched', function () {
      const row = model.getLayoutByName('0.0.0.2');

      expect(row).to.equal(model.layoutContainers[0].rows[0].cols[0]);
    });

    it('returns the row layout', function () {
      const row = model.getLayoutByName('0.0');

      expect(row).to.equal(model.layoutContainers[0].rows[0]);
    });

    it('returns undefined when the name is not found', function () {
      const row = model.getLayoutByName('missing');

      expect(row).not.to.exist;
    });
  });

  describe('relatedModelType', function () {
    it('returns undefined when the slug is an empty string', async function () {
      let model = store.createRecord('page', {});

      expect(model.relatedModelType).to.be.undefined;
    });

    it('returns undefined when the slug does not have a variable', async function () {
      let model = store.createRecord('page', {
        slug: 'a--b--c',
      });

      expect(model.relatedModelType).to.be.undefined;
    });

    it('returns team when the slug has a team id variable', async function () {
      let model = store.createRecord('page', {
        slug: 'a--:team_id--c',
      });

      expect(model.relatedModelType).to.equal('team');
    });
  });

  describe('canShowFeaturePanel', function () {
    it('returns true when there is no col', async function () {
      let model = store.createRecord('page', {});

      expect(model.canShowFeaturePanel).to.equal(true);
    });

    it('returns false when there is a col with a map with popup', async function () {
      let mapWithPopup = new PageCol({ name: '0.0.0' });
      mapWithPopup.type = 'map/map-view';
      mapWithPopup.data = {
        map: {
          featureSelection: 'show popup',
        },
      };

      let model = store.createRecord('page', {
        cols: [mapWithPopup],
      });

      expect(model.canShowFeaturePanel).to.equal(false);
    });
  });

  describe('category property', function () {
    it('returns "other" when there is no slug', async function () {
      let model = store.createRecord('page', {});

      expect(model.category).to.equal('other');
    });

    it('returns the first slug component when set', async function () {
      let model = store.createRecord('page', { slug: 'level1--level2--level3' });

      expect(model.category).to.equal('level1');
    });

    it('returns "top-level" when the slug has one component', async function () {
      let model = store.createRecord('page', { slug: 'level1' });

      expect(model.category).to.equal('top-level');
    });

    it('returns "special" when the slug starts with _', async function () {
      let model = store.createRecord('page', { slug: '_level1' });

      expect(model.category).to.equal('special');
    });

    it('returns "landing page" when the space has the page as landing page id', async function () {
      let space = store.createRecord('space', { landingPageId: '1' });
      let model = store.createRecord('page', { slug: '_level1', id: '1', space });

      expect(model.category).to.equal('landing page');
    });
  });
});
