/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "./base/modelItem";

export default class ManageEditorsCampaignFormComponent extends Component {
  triggerChange() {
    this.args.onChange?.({
      source: this.source,
    });
  }
}
