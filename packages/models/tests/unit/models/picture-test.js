/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Model | picture', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('picture', {});
    expect(model).to.be.ok;
  });
});
