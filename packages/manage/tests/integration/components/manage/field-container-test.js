/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import { wait } from 'dummy/tests/helpers';

module('Integration | Component | manage/field-container', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.set('editablePanel', '');

    this.set('save', () => {
      this.set('editablePanel', '');
    });

    this.set('cancel', () => {
      this.set('editablePanel', '');
    });

    this.set('edit', (value) => {
      this.set('editablePanel', value);
    });
  });

  it('renders the title and view mode by default', async function () {
    await render(hbs`
      <Manage::FieldContainer @title="some title">
        <:view>
          view
        </:view>

        <:edit>
          edit
        </:edit>
      </Manage::FieldContainer>
    `);

    expect(this.element.querySelector('h3').textContent.trim()).to.equal('some title');

    expect(this.element.querySelector('.btn-edit')).to.exist;
    expect(this.element.querySelector('.btn-edit .fa-pencil')).to.exist;

    expect(this.element.querySelector('.view-container').textContent.trim()).to.equal('view');
    expect(this.element.querySelector('.edit-container')).not.to.exist;
    expect(this.element.querySelector('.btn-edit')).not.to.have.attribute('disabled', '');
    expect(this.element.querySelector('.row-save-cancel')).not.to.exist;
  });

  it('renders the title and edit mode when @editablePanel equals to the title panel', async function () {
    await render(hbs`
      <Manage::FieldContainer @title="some title" @editablePanel="some title">
        <:view>
          view
        </:view>

        <:edit>
          edit
        </:edit>
      </Manage::FieldContainer>
    `);

    expect(this.element.querySelector('h3').textContent.trim()).to.equal('some title');

    expect(this.element.querySelector('.btn-edit')).to.exist;
    expect(this.element.querySelector('.btn-edit .fa-pencil')).to.exist;

    expect(this.element.querySelector('.edit-container').textContent.trim()).to.contain('edit');
    expect(this.element.querySelector('.view-container')).not.to.exist;
    expect(this.element.querySelector('.row-save-cancel')).to.exist;
  });

  it('can switch to the edit mode', async function () {
    await render(hbs`
      <Manage::FieldContainer @title="some title" @editablePanel={{this.editablePanel}} @onEdit={{this.edit}} @onSave={{this.save}} @onCancel={{this.cancel}}>
        <:view>
          view
        </:view>

        <:edit>
          edit
        </:edit>
      </Manage::FieldContainer>
    `);

    await click('.btn-edit');

    expect(this.element.querySelector('.edit-container').textContent.trim()).to.contains('edit');
    expect(this.element.querySelector('.view-container')).not.to.exist;
    expect(this.element.querySelector('.row-save-cancel')).to.exist;
  });

  it('can switch to the view mode', async function () {
    await render(hbs`
      <Manage::FieldContainer @title="some title" @editablePanel={{this.editablePanel}} @onEdit={{this.edit}} @onSave={{this.save}} @onCancel={{this.cancel}}>
        <:view>
          view
        </:view>

        <:edit>
          edit
        </:edit>
      </Manage::FieldContainer>
    `);

    await click('.btn-edit');
    await click('.btn-edit');

    expect(this.element.querySelector('.view-container').textContent.trim()).to.contains('view');
    expect(this.element.querySelector('.edit-container')).not.to.exist;
    expect(this.element.querySelector('.row-save-cancel')).not.to.exist;
  });

  it('shows a spinner while saving', async function () {
    let resolve;

    this.set('save', () => {
      return new Promise((r) => {
        resolve = r;
      });
    });

    await render(hbs`
      <Manage::FieldContainer @title="some title" @editablePanel="some title" @onEdit={{this.edit}} @onSave={{this.save}} @onCancel={{this.cancel}}>
        <:view>
          view
        </:view>

        <:edit>
          edit
        </:edit>
      </Manage::FieldContainer>
    `);

    click('.btn-submit');
    await waitFor('.fa-spinner');
    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');

    expect(this.element.querySelector('.btn-cancel')).to.have.attribute('disabled', '');
    expect(this.element.querySelector('.btn-edit')).to.have.attribute('disabled', '');

    resolve();
    await wait(50);

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', null);
    expect(this.element.querySelector('.btn-cancel')).to.have.attribute('disabled', null);
    expect(this.element.querySelector('.btn-edit')).to.have.attribute('disabled', null);
  });
});
