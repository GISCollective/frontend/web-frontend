/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";
import waitUntil from '@ember/test-helpers/wait-until';

describe ('Integration | Component | view/pictures',  function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let picture1;
  let picture2;

  hooks.before(function () {
    server = new TestServer();

    picture1 = server.testData.create.picture('1');
    picture2 = server.testData.create.picture('2');

    server.testData.storage.addPicture(picture1);
    server.testData.storage.addPicture(picture2);

    server.post('/mock-server/pictures', (request) => {
      const pictureRequest = JSON.parse(request.requestBody);
      pictureRequest.picture['_id'] = '5cc8dc1038e882010061545a';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(pictureRequest),
      ];
    });
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::Pictures />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  describe ('when the value is a page col',  function (hooks) {
    it('renders a list of pictures', async function () {
      this.set('value', { data: { records: [picture1, picture2] } });
      await render(hbs`<View::Pictures @value={{this.value}} />`);

      const images = [...this.element.querySelectorAll('img')].map(a => a.attributes.getNamedItem("src").value);

      expect(images).to.have.length(2);
      expect(images[0]).to.contain(
        '/test/5d5aa72acac72c010043fb59.jpg.lg.jpg'
      );
    });
  });
});
