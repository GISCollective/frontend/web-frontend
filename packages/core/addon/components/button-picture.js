import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { guidFor } from '@ember/object/internals';

export default class ButtonPicture extends Component {
  elementId = `button-picture-${guidFor(this)}`;

  @service store;
  @tracked picture;
  @tracked hover;
  @tracked active;

  get pictureUrl() {
    return this.picture?.picture;
  }

  get hoverUrl() {
    return this.hover?.picture ?? this.pictureUrl;
  }

  get activeUrl() {
    return this.active?.picture ?? this.hoverUrl;
  }

  get hasPicture() {
    const options = this.args.value?.pictureOptions ?? {};

    return options.picture || options.hover || options.active;
  }

  get positionClass() {
    return `position-${this.args.value?.pictureOptions?.position ?? "start"}`;
  }

  get text() {
    let text = `${this.args.text ?? ""}`;
    const hasText = text.trim() != "";

    if(!hasText && this.hasPicture) {
      return "";
    }

    if(!hasText && this.args.isEditor) {
      return "(...)";
    }

    return text;
  }

  async getPicture(id) {
    let result = this.store.peekRecord("picture", id);

    if(!result) {
      result = await this.store.findRecord("picture", id);
    }

    return result;
  }

  @action
  async setup() {
    if(this.args.value?.pictureOptions?.picture) {
      this.picture = await this.getPicture(this.args.value.pictureOptions.picture);
    }

    if(this.args.value?.pictureOptions?.hover) {
      this.hover = await this.getPicture(this.args.value.pictureOptions.hover);
    }

    if(this.args.value?.pictureOptions?.active) {
      this.active = await this.getPicture(this.args.value.pictureOptions.active);
    }
  }
}
