import { module, test } from "qunit";
import { setupRenderingTest } from "dummy/tests/helpers";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

module("Integration | Component | page-editor", function (hooks) {
  setupRenderingTest(hooks);

  test("it renders", async function (assert) {
    await render(hbs`<PageEditor />`);

    assert
      .dom()
      .hasText(
        "edit layout save save as a new copy open private The layout has no containers. You need to edit the layout before editing the content. Edit layout page name Path slug: description space Click to select an image is public categories Add revisions There are no changes",
      );
  });
});
