/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { fromPageState, toPageState } from 'core/lib/page-state';

export default class StateFiltersEventRepetitionComponent extends Component {
  @service intl;
  @service clickOutside;

  get title() {
    return this.args.options?.label ?? this.intl.t('repetition');
  }

  get options() {
    return ['norepeat', 'daily', 'weekly', 'monthly', 'yearly'].filter(
      (a) => this.args.options[a],
    );
  }

  get value() {
    return this.state.er;
  }

  get state() {
    return fromPageState(this.args.state);
  }

  @action
  change(value) {
    const newState = toPageState({
      ...this.state,
      er: value,
    });

    this.args.onChangeState?.(newState);
    this.clickOutside.unsubscribe();
  }
}
