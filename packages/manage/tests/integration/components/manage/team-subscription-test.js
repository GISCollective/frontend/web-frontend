import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/team-subscription', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the subscription details', async function (assert) {
    this.set('subscription', {
      name: 'test',
      expire: '2030-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      monthlySupportHours: 1,
      details: 'Details',
      support: [
        {
          hours: 1,
          date: '2022-03-22T00:22:11.000Z',
          details: 'details 1',
        },
      ],
      invoices: [],
    });

    await render(hbs`<Manage::TeamSubscription @value={{this.subscription}} />`);

    expect(this.element.querySelector('.manage-team-subscription h4')).to.have.textContent('Subscription');
    expect(this.element.querySelector('.manage-team-subscription h6')).to.have.textContent('Name');
    expect(this.element.querySelector('.manage-team-subscription .stat-value-name')).to.have.textContent('test');
    expect(this.element.querySelector('.manage-team-subscription .stat-value-domains')).to.have.textContent('a.b.c');
    expect(this.element.querySelector('.manage-team-subscription .stat-value-expire')).to.have.textContent(
      '1 January 2030',
    );
    expect(this.element.querySelector('.manage-team-subscription .stat-value-free-support')).to.have.textContent(
      'one hour',
    );
  });

  it('renders none free support hours when it is consumed', async function (assert) {
    this.set('subscription', {
      name: 'test',
      expire: '2030-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      monthlySupportHours: 1,
      details: 'Details',
      support: [
        {
          hours: 1,
          date: new Date(),
          details: 'details 1',
        },
      ],
      invoices: [],
    });

    await render(hbs`<Manage::TeamSubscription @value={{this.subscription}} />`);

    expect(this.element.querySelector('.manage-team-subscription .stat-value-free-support')).to.have.textContent(
      'none',
    );
  });

  it('renders none free support hours when there is billable support', async function (assert) {
    this.set('subscription', {
      name: 'test',
      expire: '2030-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      monthlySupportHours: 1,
      details: 'Details',
      support: [
        {
          hours: 2,
          date: new Date(),
          details: 'details 1',
        },
      ],
      invoices: [],
    });

    await render(hbs`<Manage::TeamSubscription @value={{this.subscription}} />`);

    expect(this.element.querySelector('.manage-team-subscription .stat-value-free-support')).to.have.textContent(
      'none',
    );
    expect(this.element.querySelector('.manage-team-subscription .stat-value-billable-support')).to.have.textContent(
      'one hour',
    );
  });

  it('renders no billable support hours when it is paid', async function (assert) {
    this.set('subscription', {
      name: 'test',
      expire: '2030-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      monthlySupportHours: 1,
      details: 'Details',
      support: [
        {
          hours: 2,
          date: new Date(),
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 1,
          date: new Date(),
          details: 'details 1',
        },
      ],
    });

    await render(hbs`<Manage::TeamSubscription @value={{this.subscription}} />`);

    expect(this.element.querySelector('.manage-team-subscription .stat-value-free-support')).to.have.textContent(
      'none',
    );
    expect(this.element.querySelector('.manage-team-subscription .stat-value-billable-support')).not.to.exist;
  });

  it('renders available support hours when it is paid in advance', async function (assert) {
    this.set('subscription', {
      name: 'test',
      expire: '2030-01-01T00:00:00.000Z',
      domains: ['a.b.c'],
      comment: 'other',
      monthlySupportHours: 1,
      details: 'Details',
      support: [
        {
          hours: 2,
          date: new Date(),
          details: 'details 1',
        },
      ],
      invoices: [
        {
          hours: 10,
          date: new Date(),
          details: 'details 1',
        },
      ],
    });

    await render(hbs`<Manage::TeamSubscription @value={{this.subscription}} />`);

    expect(this.element.querySelector('.manage-team-subscription .stat-value-free-support')).to.have.textContent(
      '9 hours',
    );
    expect(this.element.querySelector('.manage-team-subscription .stat-value-billable-support')).not.to.exist;
  });
});
