/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { generateBlur } from '../lib/blur';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';
import { guidFor } from '@ember/object/internals';

export default class PictureWithOptionsComponent extends Component {
  elementId = `picture-with-options-${guidFor(this)}`;

  @tracked isLoaded;
  @tracked isReadyOnInit = true;
  @tracked size = 'lg';
  @tracked _value;
  sizeObserverList = [];

  sizeOptions = {
    xs: 100,
    sm: 400,
    md: 800,
    lg: 1600,
  };

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.options;
  }

  get url() {
    if (this.args.src?.startsWith?.('data:image/')) {
      return this.args.src;
    }

    const size = this.args.src?.indexOf('.jpg') > 0 ? `.${this.size}.jpg` : `/${this.size}`;

    return `${this.args.src}${size}`;
  }

  get isAutoSize() {
    return ['auto', 'fixed height', 'fixed width'].includes(this.sizing);
  }

  get isProportionType() {
    return this.sizing === 'proportional';
  }

  get sizing() {
    return this.value?.size?.sizing ?? this.value?.sizing ?? 'auto';
  }

  get containerClasses() {
    let result = [];

    if (this.value?.size?.containerClasses) {
      result = [...this.value.size.containerClasses];
    } else if (this.value?.containerClasses) {
      result = [...this.value.containerClasses];
    }

    return result;
  }

  get containerOptions() {
    return [`size-${this.sizing}`, ...this.containerClasses];
  }

  get proportion() {
    const value = this.value?.proportion ?? this.value?.size?.proportion ?? '';
    const pieces = value.split(':').map((a) => parseInt(a));

    if (pieces.length != 2) {
      return 1;
    }

    return `${pieces[0]}/${pieces[1]}`;
  }

  get style() {
    let allStyles = `background-image: url(${this.url});`;

    if (this.isProportionType) {
      allStyles += `aspect-ratio: ${this.proportion};`;
    }

    return htmlSafe(allStyles);
  }

  get aspectRationStyle() {
    if (!this.isProportionType) {
      return null;
    }

    return htmlSafe(`aspect-ratio: ${this.proportion};`);
  }

  get hashStyle() {
    let hashBg = generateBlur(this.args.hash, 5, 5);

    if (this.isProportionType) {
      hashBg += `aspect-ratio: ${this.proportion};`;
    }

    return htmlSafe(hashBg);
  }

  get pictureClasses() {
    const items = this.value?.classes ?? [];

    return items?.join?.(' ') ?? '';
  }

  get showAttributions() {
    if (!this.args.name && !this.args.meta?.attributions) {
      return false;
    }

    return this.value?.attributions == 'below';
  }

  get customStyle() {
    if (this.sizing == 'auto') {
      return null;
    }

    if (this.sizing == 'proportional') {
      return null;
    }

    return this.value?.customStyle ?? this.value?.size?.customStyle;
  }

  onResize() {
    const parent = this.container.parentElement;

    if (this.container.dataset.autoresize == 'false' || !parent) {
      this.size = 'lg';
      return;
    }

    const maxSize = Math.max(parent.offsetWidth, parent.offsetHeight);

    for (const key of Object.keys(this.sizeOptions)) {
      if (maxSize <= this.sizeOptions[key] / 2) {
        this.isLoaded = this.isLoaded && this.size == key;
        this.size = key;
        break;
      }
    }
  }

  @action
  updateProps(event) {
    this._value = event.detail.value;
  }

  @action
  setupLoading() {
    later(() => {
      if (!this.isLoaded) {
        this.isReadyOnInit = false;
      }
    }, 50);
  }

  @action
  handleClick() {
    return this.args.onClick?.(...arguments);
  }

  @action
  onImageLoad(event) {
    if (event.target?.naturalWidth == 0) {
      return;
    }

    this.isLoaded = true;
    this.args.onLoad?.();
  }

  @action
  watchSize(element) {
    this.container = element;

    try {
      const sizeObserver = new ResizeObserver((entries) => {
        if (!entries?.length) {
          return;
        }

        window?.requestAnimationFrame(() => {
          this.onResize();
        });
      });

      sizeObserver.observe(this.container);

      this.sizeObserverList.push(sizeObserver);
    } catch (err) {
      console.error(err);
    }
  }

  willDestroy() {
    super.willDestroy(...arguments);

    for (const observer of this.sizeObserverList) {
      observer.disconnect();
    }
  }
}
