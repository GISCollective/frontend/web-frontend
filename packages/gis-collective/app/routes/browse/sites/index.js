/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BrowseRoute from '../../base/browse';
import { service } from '@ember/service';
import { hash } from 'rsvp';
import LazyList from 'models/lib/lazy-list';

export default class BrowseSitesRoute extends BrowseRoute {
  @service headData;

  model(params) {
    const featuresParams = {};

    this.featuresSetup(featuresParams, params);

    const getNewBuckets = async () => {
      const features = new LazyList('feature', featuresParams, this.store);
      await features.loadNext();
      return features;
    }

    return hash({
      getNewBuckets,
      features: getNewBuckets(),
      author: this.getAuthor(params),
      icon: this.getIcon(params),
      map: this.getMap(params),
      area: params.area,
    });
  }

  async afterModel(model) {
    if (model.icon) {
      await model.icon.fillMetaInfo(this.headData);
    }

    if (model.map) {
      try {
        await model.map.reloadMeta();
      } catch (err) {
        console.log(`Error on reloading the meta ${model.map.id}.`, err);
      }

      try {
        await model.map.visibility.fetchTeam();
      } catch(err) {
        console.log(`Error on loading the team for ${model.map.id}.`, err);
      }

      await model.map.get('cover');
      await model.map.get('squareCover');
      await model.map.fillMetaInfo(this.headData);
    }

    if (!this.fastboot.isFastBoot) {
      return;
    }

    for (const feature of model.features.items) {
      await feature.loadRelations();
    }

    await model.map?.visibility?.team?.get('logo');
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.setupFilters();
    controller.authorFilter.author = model.author;
    controller.areaFilter.area = model.area;
    controller.mapFilter.map = model.map;
  }
}
