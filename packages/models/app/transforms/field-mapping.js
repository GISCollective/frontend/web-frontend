/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class FieldMappingTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map((a) => new FieldMapping(a));
  }

  serialize(deserialized) {
    if (!Array.isArray(deserialized)) {
      return [];
    }

    return deserialized.map((a) => (a.toJSON ? a.toJSON() : a));
  }
}

export class FieldMapping {
  @tracked key = '';
  @tracked destination = '';
  @tracked preview = [];

  constructor(value) {
    if (value?.key) {
      this.key = value.key;
    }

    if (value?.destination) {
      this.destination = value.destination;
    }

    if (Array.isArray(value?.preview)) {
      this.preview = value.preview;
    }
  }

  toJSON() {
    return {
      key: this.key ?? '',
      destination: this.destination ?? '',
      preview: this.preview ?? [],
    };
  }
}
