/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class LoadingContainerComponent extends Component {
  get animate() {
    if (this._value !== undefined) {
      return this._value;
    }

    if (this.args.animate === undefined) {
      this._value = true;
    } else {
      this._value = !!this.args.animate;
    }

    return this._value;
  }
}
