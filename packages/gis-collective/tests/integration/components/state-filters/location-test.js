/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | state-filters/location',  function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultGeocoding();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    this.owner.lookup('service:store');
    server.history = [];
  });

  it('renders the label when there is no state', async function () {
    await render(hbs`<StateFilters::Location />`);

    expect(
      this.element.querySelector('.btn-select').textContent.trim()
    ).to.equal('location');
  });

  it('can select a location', async function () {
    let value;

    this.set('state', '');
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    await render(
      hbs`<StateFilters::Location @state={{this.state}} @onChangeState={{this.onChangeState}}/>`
    );

    await click('.btn-select');

    await fillIn('.input-filter', 'some place');
    await triggerEvent('.input-filter', 'change');

    await click('.list-group-item');

    expect(value).to.equal(
      'l@Berlin, Coos County, New Hampshire, 03570, United States'
    );
  });

  it('renders the location from the current state', async function () {
    this.set(
      'state',
      'l@Berlin, Coos County, New Hampshire, 03570, United States'
    );

    await render(
      hbs`<StateFilters::Location @state={{this.state}} @onChangeState={{this.onChangeState}}/>`
    );

    expect(
      this.element.querySelector('.btn-pill-location').textContent.trim()
    ).to.equal('Berlin, Coos County, New Hampshire, 03570, United States');
  });

  it('clears the location when the x button is pressed', async function () {
    let value;

    this.set(
      'state',
      'l@Berlin, Coos County, New Hampshire, 03570, United States'
    );
    this.set('onChangeState', (s) => {
      value = s;

      this.set('state', s);
    });

    await render(
      hbs`<StateFilters::Location @state={{this.state}} @onChangeState={{this.onChangeState}}/>`
    );

    await click('.btn-reset');

    expect(value).to.equal('');
    expect(
      this.element.querySelector('.btn-pill-location').textContent.trim()
    ).to.equal('location');
  });
});
