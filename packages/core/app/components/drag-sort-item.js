import { assert } from '@ember/debug';
import { service } from '@ember/service';
import { next } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';

function getComputedStyleInt(element, cssProp) {
  const computedStyle = window.getComputedStyle(element, null);
  const valueStr = computedStyle.getPropertyValue(cssProp);

  return parseInt(valueStr, 10);
}

export default class DragSortItemComponent extends Component {
  @tracked containerElement = null;

  @service dragSort;

  get isActive() {
    return this.args.index == this.dragSort.sourceIndex && this.dragSort.listId == this.args.listId;
  }

  get insertPointStyle() {
    if (!this.containerElement) {
      return htmlSafe(``);
    }
    const height = getComputedStyleInt(this.containerElement, 'height');
    return htmlSafe(`heigth: ${height}px`);
  }

  get isOver() {
    return this.args.index == this.dragSort.targetIndex;
  }

  @action
  dragStart(event) {
    if (!this.args.draggingEnabled) return;

    if (!this.isHandleUsed(event.target)) {
      event.preventDefault();
      return;
    }

    event.stopPropagation();

    // Required for Firefox. http://stackoverflow.com/a/32592759/901944
    if (event.dataTransfer?.setData) event.dataTransfer.setData('text', '');
    if (event.dataTransfer?.setDragImage) event.dataTransfer.setDragImage(this.containerElement, 0, 0);

    this.startDragging(event);
  }

  @action
  dragEnd(event) {
    if (!this.dragSort.isDragging || this.dragSort.listId != this.args.listId) {
      return;
    }

    event.stopPropagation();
    event.preventDefault();

    this.restore();

    const dragEvent = this.dragSort.currentEvent;

    if (dragEvent.sourceIndex == dragEvent.targetIndex) {
      return;
    }

    this.args.dragEndAction(this.dragSort.currentEvent);
  }

  @action
  drop(event) {
    event.preventDefault();
  }

  @action
  dragOver(event) {
    if (!this.dragSort.isDragging || this.dragSort.listId != this.args.listId) {
      return;
    }

    const group = this.group;
    const activeGroup = this.dragSort.group;

    if (group !== activeGroup) return;

    event.stopPropagation();
    event.preventDefault();

    this.draggingOver(event);
  }

  @action
  dragEnter(event) {
    if (!this.dragSort.isDragging) {
      return;
    }

    // Without this, dragOver would not fire in IE11. http://mereskin.github.io/dnd/
    event.preventDefault();
  }

  @action
  startDragging() {
    this.collapse();

    this.dragSort.startDragging({
      additionalArgs: this.args.additionalArgs,
      item: this.args.item,
      index: this.args.index,
      items: this.args.items,
      group: this.args.group,
      isHorizontal: this.args.isHorizontal,
      listId: this.args.listId,
    });

    this.args.dragStartAction?.(this.dragSort.currentEvent);
  }

  @action
  draggingOver(event) {
    const sourceOnly = this.sourceOnly;

    if (sourceOnly) {
      event.preventDefault();
      return;
    }

    const element = this.containerElement;
    const isHorizontal = this.dragSort.isHorizontal;
    const isRtl = this.isRtl && isHorizontal;
    const isPlaceholderBefore = this.shouldShowPlaceholderBefore2;
    const isPlaceholderAfter = this.shouldShowPlaceholderAfter2;
    const placeholderModifier = isRtl ? -1 : 1;

    let beforeAttribute = 'padding-top';
    let afterAttribute = 'padding-bottom';
    if (isHorizontal) {
      beforeAttribute = isRtl ? 'padding-right' : 'padding-left';
      afterAttribute = isRtl ? 'padding-left' : 'padding-right';
    }

    const placeholderCorrection = isPlaceholderBefore
      ? getComputedStyleInt(element, beforeAttribute) * placeholderModifier
      : isPlaceholderAfter
        ? -getComputedStyleInt(element, afterAttribute) * placeholderModifier
        : 0; // eslint-disable-line indent

    const offset = isHorizontal ? element.getBoundingClientRect().left : element.getBoundingClientRect().top;

    const itemSize = isHorizontal ? element.offsetWidth : element.offsetHeight;

    const mousePosition = isHorizontal ? event.clientX : event.clientY;

    const isDraggingUp = isRtl
      ? mousePosition - offset > (itemSize + placeholderCorrection) / 2
      : mousePosition - offset < (itemSize + placeholderCorrection) / 2;

    this.dragSort.draggingOver({
      group: this.args.group,
      index: this.args.index,
      items: this.args.items,
      isDraggingUp,
      listId: this.args.listId,
    });
  }

  @action
  collapse() {
    // The delay is necessary for HTML classes to update with a delay.
    // Otherwise, dragging is finished immediately.
    next(() => {
      if (this.isDestroying || this.isDestroyed) return;
      this.isDragged2 = true;
    });
  }

  @action
  restore() {
    next(() => {
      if (this.isDestroying || this.isDestroyed) {
        return;
      }
      this.isDragged2 = false;
      this.dragSort.sourceIndex = null;
      this.dragSort.targetIndex = null;
      this.dragSort.listId = null;
    });
  }

  @action
  isHandleUsed(target) {
    const handle = this.args.handle;
    const element = this.containerElement;

    if (!handle) return true;

    const handleElement = element.querySelector(handle);

    assert('Handle not found', !!handleElement);

    return handleElement === target || handleElement.contains(target);
  }

  @action
  setup(element) {
    this.containerElement = element;
  }
}
