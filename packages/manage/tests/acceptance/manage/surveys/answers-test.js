/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL } from '@ember/test-helpers';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';
import Modal from 'core/test-support/modal';

describe('Acceptance | manage/surveys/answers', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let campaign;
  let defaultPicture;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    const user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    server.testData.storage.addDefaultSpace();
    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultCampaignAnswer('1');
    server.testData.storage.addDefaultCampaignAnswer('2');
    server.testData.storage.addDefaultPreferences();
    campaign = server.testData.storage.addDefaultCampaign();
    server.testData.storage.addDefaultMap('5ca89e37ef1f7e010007f54c');
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/surveys/answers/${campaign._id}`);
    await PageElements.waitEditorJs(this.element);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fsurveys%2Fanswers%2F5ca78aa160780601008f6aaa');
  });

  it('should contain all the browse links', async function () {
    authenticateSession();
    await visit(`/manage/surveys/answers/${campaign._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'Campaign 1', 'Answers']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/000000000000000000000001',
    ]);
  });

  it('should show the campaign answers related to the campaign', async function () {
    authenticateSession();
    await visit(`/manage/surveys/answers/${campaign._id}`);

    const card = this.element.querySelector('.manage-card');
    expect(card).to.exist;

    expect(card.querySelector('.title').textContent.trim()).to.equal('some test');
    expect(card.querySelector('.badge').textContent.trim()).to.equal('pending');
  });
});
