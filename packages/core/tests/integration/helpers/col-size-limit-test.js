/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Helper | col-size-limit', function (hooks) {
  setupRenderingTest(hooks);

  it('leaves only the mobile classes when the size is mobile', async function () {
    this.set('size', 'mobile');
    this.set('class', 'col-12 col-md-3 col-lg-6');

    await render(hbs`{{col-size-limit this.size this.class}}`);

    expect(this.element.textContent.trim()).to.equal('col-12');
  });

  it('leaves only the mobile classes when the size is tablet and it has no tablet col', async function () {
    this.set('size', 'tablet');
    this.set('class', 'col-12 col-lg-6');

    await render(hbs`{{col-size-limit this.size this.class}}`);

    expect(this.element.textContent.trim()).to.equal('col-12');
  });

  it('leaves only the tablet class when the size is tablet and it has a tablet col', async function () {
    this.set('size', 'tablet');
    this.set('class', 'col-12 col-md-6');

    await render(hbs`{{col-size-limit this.size this.class}}`);

    expect(this.element.textContent.trim()).to.equal('col-6');
  });

  it('leaves only the mobile classes when the size is desktop and it has no desktop col', async function () {
    this.set('size', 'desktop');
    this.set('class', 'col-12');

    await render(hbs`{{col-size-limit this.size this.class}}`);

    expect(this.element.textContent.trim()).to.equal('col-12');
  });

  it('leaves only the tablet classes when the size is desktop and it has no desktop col', async function () {
    this.set('size', 'desktop');
    this.set('class', 'col-12 col-md-6');

    await render(hbs`{{col-size-limit this.size this.class}}`);

    expect(this.element.textContent.trim()).to.equal('col-6');
  });

  it('leaves only the desktop classes when the size is desktop and it has a desktop col', async function () {
    this.set('size', 'desktop');
    this.set('class', 'col-12 col-md-6 col-lg-7');

    await render(hbs`{{col-size-limit this.size this.class}}`);

    expect(this.element.textContent.trim()).to.equal('col-7');
  });
});
