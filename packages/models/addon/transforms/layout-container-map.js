/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { LayoutContainer } from './layout-container-list';
import { service } from '@ember/service';

export default class LayoutContainerMapTransform extends Transform {
  @service store;
  @service fastboot;

  deserialize(serialized) {
    return new LayoutContainerMap(serialized ?? {});
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() ?? {};
  }
}

export class LayoutContainerMap {
  @tracked idList;
  @tracked map = {};

  constructor(data) {
    this.idList = Object.keys(data).sort();

    for (const gid of this.idList) {
      this.map[gid] = new LayoutContainer({
        ...data[gid],
        gid,
      });
    }
  }

  exists(gid) {
    return this.idList.includes(gid) ? true : false;
  }

  setContainer(gid, container) {
    this.isDirty = true;

    if (!this.exists(gid)) {
      this.idList.push(gid);
    }

    this.map[gid] = container;
    container.gid = gid;
  }

  toJSON() {
    const result = {};

    for (const id of this.idList) {
      result[id] = this.map[id].toJSON?.() ?? {};
    }

    return result;
  }
}
