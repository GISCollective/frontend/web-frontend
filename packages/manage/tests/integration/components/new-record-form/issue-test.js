/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, fillIn, click, waitUntil, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | new-record-form/issue', function (hooks) {
  setupRenderingTest(hooks);

  let receivedIssue;

  let server;
  let store;
  let featureData;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultMap();
    featureData = server.testData.storage.addDefaultFeature();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    receivedIssue = null;

    store = this.owner.lookup('service:store');

    server.post('/mock-server/issues', (request) => {
      receivedIssue = JSON.parse(request.requestBody);
      receivedIssue.issue._id = '1';

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedIssue)];
    });
  });

  it('renders a disabled submit button when nothing is filled in', async function () {
    await render(hbs`<NewRecordForm::Issue />`);

    await waitFor('.btn-submit');

    expect(this.element.querySelector('.btn-submit')).to.have.attribute('disabled', '');
  });

  it('creates a new issue when the fields are filled in and the submit button is pressed', async function () {
    let isCalled;

    this.set('submit', () => {
      isCalled = true;
    });

    const feature = store.findRecord('feature', featureData._id);
    this.set('feature', feature);

    await render(hbs`<NewRecordForm::Issue @onSubmit={{this.submit}} @feature={{this.feature}}/>`);

    await waitFor('.input-issue-title');

    await fillIn('.input-issue-title', 'title');
    await fillIn('.input-issue-description', 'description');

    expect(this.element.querySelector('.btn-submit')).not.to.have.attribute('disabled', '');

    await click('.btn-submit');

    expect(isCalled).to.equal(true);

    await waitUntil(() => receivedIssue);

    expect(receivedIssue).to.deep.equal({
      issue: {
        _id: '1',
        title: 'title',
        description: 'description',
        type: 'none',
        author: '@anonymous',
        feature: '5ca78e2160780601008f69e6',
        attributions: null,
        file: null,
        status: null,
        creationDate: null,
      },
    });
  });
});
