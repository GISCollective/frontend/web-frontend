/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | menu-user-item-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    await render(hbs`<MenuUserItemList />`);

    const menuItems = this.element.querySelectorAll('.menu-item');

    expect(menuItems).to.have.length(4);
    expect(menuItems[0].textContent.trim()).to.equal('');
    expect(menuItems[1].textContent.trim()).to.equal('dashboard');
    expect(menuItems[2].textContent.trim()).to.equal('preferences');
    expect(menuItems[3].textContent.trim()).to.equal('sign out');
  });
});
