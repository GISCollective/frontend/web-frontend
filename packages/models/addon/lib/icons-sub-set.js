/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from '@glimmer/tracking';

export default class IconsSubSet {
  @tracked site = null;
  @tracked icons = [];

  async update() {
    this.icons = [];

    const iconSets = await this.feature.iconSets;

    this.icons = await this.store.query('icon', { iconSet: iconSets.join() });

    return this.icons;
  }
}
