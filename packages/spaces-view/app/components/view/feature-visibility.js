/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class PageColFeatureVisibilityComponent extends Component {
  get hasValue() {
    return [1, 0, -1].indexOf(this.args.value) != -1;
  }

  get text() {
    if (this.args.value == 1) {
      return 'visibility-public';
    }

    if (this.args.value == -1) {
      return 'visibility-pending';
    }

    return 'visibility-private';
  }
}
