/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class ResponsiveStyleComponent extends Component {
  styles(values) {
    let result = [];

    for (const key in values) {
      result.push(`${key}: ${this.getValue(values[key])}`);
    }

    if (this.args.isCol && values.width) {
      result.push('flex: 0 0 auto');
    }

    if (result.length == 0) {
      return '';
    }

    return htmlSafe(`${this.args.selector} {${result.join(';')}}`);
  }

  getValue(property) {
    if (Array.isArray(property)) {
      return property.map((a) => this.getValue(a)).join(' ');
    }

    if (typeof property == 'object') {
      return `${property.value}${property.unit}`;
    }

    return property;
  }

  get hasDeviceSize() {
    return ['sm', 'md', 'lg'].includes(this.args.deviceSize);
  }

  get deviceSizeStyle() {
    if (!this.hasDeviceSize) {
      return '';
    }

    const result = {};

    for (const key of Object.keys(this.args.style?.sm ?? {})) {
      result[key] = this.getValue(this.args.style.sm[key]);
    }

    if (['md', 'lg'].includes(this.args.deviceSize)) {
      for (const key of Object.keys(this.args.style?.md ?? {})) {
        result[key] = this.getValue(this.args.style.md[key]);
      }
    }

    if (this.args.deviceSize == 'lg') {
      for (const key of Object.keys(this.args.style?.lg ?? {})) {
        result[key] = this.getValue(this.args.style.lg[key]);
      }
    }

    return this.styles(result);
  }

  get sm() {
    if (this.hasDeviceSize) {
      return '';
    }

    return this.styles(this.args.style?.sm ?? {});
  }

  get md() {
    if (this.hasDeviceSize) {
      return '';
    }

    return this.styles(this.args.style?.md ?? {});
  }

  get lg() {
    if (this.hasDeviceSize) {
      return '';
    }

    return this.styles(this.args.style?.lg ?? {});
  }
}
