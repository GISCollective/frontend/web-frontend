/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class AddPageController extends Controller {
  @service notifications;
  @service router;
  @service user;
  @service store;
  @service intl;

  @tracked all = false;
  @tracked hasTeam;
  @tracked team;

  destination = 'manage.pages.content';
  queryParams = ['all', 'next'];

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  change(field, value) {
    this.editableModel.set(field, value);
    this.hasTeam = !!this.editableModel.visibility.team;

    if (field == 'visibility.team') {
      this.team = value;
    }
  }

  @action
  async save() {
    let result;

    try {
      result = await this.editableModel.save();
    } catch (err) {
      return this.notifications.handleError(err);
    }

    this.router.transitionTo(this.destination, result.id);

    return result;
  }

  get editableModel() {
    return this.model.page;
  }

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.page.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new page'),
      capitalize: true,
    });

    return pieces;
  }
}
