/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { expect } from 'chai';
import { hbs } from 'ember-cli-htmlbars';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';
import { DateTime } from 'luxon';

describe('Integration | Component | input/timezone', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a "not set" message when there is no value', async function (a) {
    await render(hbs`<Input::Timezone />`);

    expect(this.element.querySelector('.ember-power-select-selected-item').textContent.trim()).to.equal('not set');
  });

  it('renders a "CET" message when CET is selected', async function (a) {
    await render(hbs`<Input::Timezone @value="CET"/>`);

    const now = DateTime.now();

    expect(this.element.querySelector('.ember-power-select-selected-item .name').textContent.trim()).to.equal('CET');
    expect(this.element.querySelector('.ember-power-select-selected-item .offset').textContent.trim()).to.equal(
      `+${now.offset / 60}h`,
    );
  });

  it('can search and select a timezone', async function (a) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::Timezone  @value="CET" @onChange={{this.change}} />`);
    await selectSearch('.input-timezone', 'europe');
    await selectChoose('.input-timezone', 'Europe/Bucharest');

    expect(value).to.equal('Europe/Bucharest');
  });
});
