/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "../base/modelItem";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsIconMatrixViewComponent extends Component {
  @tracked _categoryStyle;
  @tracked _subcategoryStyle;

  iconLayoutConfig = [
    {
      name: "itemsPerPage",
      type: "number",
    },
    {
      type: "width",
    },
    {
      type: "bs",
      property: "justify-content",
    },
  ];

  triggerChange() {
    this.args.onChange?.({
      subcategoryStyle: this.subcategoryStyle,
      categoryStyle: this.categoryStyle,
      iconLayout: this.iconLayout,
      source: this.source,
    });
  }

  get iconLayout() {
    if (this._iconLayout) {
      return this._iconLayout;
    }

    return this.args.value?.data?.iconLayout;
  }

  get categoryStyle() {
    if (this._categoryStyle) {
      return this._categoryStyle;
    }

    return this.args.value?.data?.categoryStyle;
  }

  get categoryHoverColor() {
    if (this._categoryHoverColor) {
      return this._categoryHoverColor;
    }

    return this.args.value?.data?.categoryStyle?.hoverColor;
  }

  get categoryActiveColor() {
    if (this._categoryActiveColor) {
      return this._categoryActiveColor;
    }

    return this.args.value?.data?.categoryStyle?.activeColor;
  }

  get subcategoryStyle() {
    if (this._subcategoryStyle) {
      return this._subcategoryStyle;
    }

    return this.args.value?.data?.subcategoryStyle ?? {};
  }

  get subcategoryHoverColor() {
    if (this._subcategoryHoverColor) {
      return this._subcategoryHoverColor;
    }

    return this.args.value?.data?.subcategoryStyle?.hoverColor;
  }

  get subcategoryActiveColor() {
    if (this._subcategoryActiveColor) {
      return this._subcategoryActiveColor;
    }

    return this.args.value?.data?.subcategoryStyle?.activeColor;
  }

  @action
  changeCategoryStyle(value) {
    this._categoryStyle = value;
    this.triggerChange();
  }

  @action
  changeSubcategoryStyle(value) {
    this._subcategoryStyle = value;
    this.triggerChange();
  }

  @action
  change(key, value) {
    this[key] = value;
    this.triggerChange();
  }
}
