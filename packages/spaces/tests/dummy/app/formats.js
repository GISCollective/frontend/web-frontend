/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export default {
  time: {
    hhmmss: {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    },
  },
  date: {
    hhmmss: {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    },
  },
  number: {
    compact: { notation: "compact" },
    EUR: {
      style: "currency",
      currency: "EUR",
    },
    USD: {
      style: "currency",
      currency: "USD",
    },
  },
};
