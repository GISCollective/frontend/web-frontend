/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | editor/background/map", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let space;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultMap("1");
    server.testData.storage.addDefaultMap("2");

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPage("000000000000000000000002");

    space = server.testData.storage.addDefaultSpace();
    space.getPagesMap = () => {
      return {
        page1: "000000000000000000000001",
        page2: "000000000000000000000002",
        page3: "000000000000000000000003",
      };
    };
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("allows selecting a new source value", async function (a) {
    this.set("value", { data: { query: { id: "" } } });

    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Background::Map @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".mode").value = "use a map";
    await triggerEvent(".mode", "change");

    await selectSearch(".model-id", "Harta Verde");
    await selectChoose(".model-id", "Harta Verde București");

    a.deepEqual(value, {
      map: {
        mode: "use a map",
        flyOverAnimation: false,
        featureHover: "do nothing",
        featureSelection: "do nothing",
        enableMapInteraction: "no",
        restrictView: false,
        showUserLocation: false,
        showZoomButtons: false,
        showBaseMapSelection: false,
      },
      sizing: "fill",
      source: { useSelectedModel: false, model: "map", id: "1", useDefaultModel: false, useSelectedModel: false },
    });
  });
});
