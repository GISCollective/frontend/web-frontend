/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | optional-map', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let transform = this.owner.lookup('transform:optional-map');
    expect(transform).to.be.ok;
  });

  it('deserialize a null to a optional-map object', function () {
    let transform = this.owner.lookup('transform:optional-map');
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({
      isEnabled: false,
      map: '',
    });
  });

  it('deserialize a null map to a optional-map object', function () {
    let transform = this.owner.lookup('transform:optional-map');
    const deserialized = transform.deserialize({ map: null }).toJSON();

    expect(deserialized).to.deep.equal({
      isEnabled: false,
      map: '',
    });
  });

  it('deserialize a value to a optional-map object', function () {
    let transform = this.owner.lookup('transform:optional-map');
    const deserialized = transform.deserialize({ isEnabled: true, map: 'some map' }).toJSON();

    expect(deserialized).to.deep.equal({
      isEnabled: true,
      map: 'some map',
    });
  });
});
