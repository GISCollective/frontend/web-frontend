/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, click, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/icons/open-selector',  function (hooks) {
  setupRenderingTest(hooks);

  describe ('When there is a long icon list',  function (hooks) {
    let icons;
    let allIcons;

    hooks.beforeEach(function () {
      allIcons = [];
      for (let i = 0; i < 40; i++) {
        allIcons.push({
          id: `${i}`,
          image: 'image',
          localName: `Monument istoric ${i}`,
          subcategory: 'Monumente',
          name: 'historical monument',
          category: 'Cultură',
        });
      }

      icons = {
        categories: {
          key: {
            icons: allIcons,
          },
        },
      };
    });

    it('renders the list', async function () {
      this.set('icons', icons);

      await render(hbs`<Input::Icons::OpenSelector @icons={{this.icons}}/>`);

      expect(this.element.querySelectorAll('.icon-element')).to.have.length(40);
    });

    it('selects an icon', async function () {
      this.set('icons', icons);

      let selection;
      this.set('selectionChanged', function (value) {
        selection = value;
      });

      await render(
        hbs`<Input::Icons::OpenSelector @icons={{this.icons}} @onSelect={{this.selectionChanged}}/>`
      );

      await click('.btn-icon');

      expect(selection).to.have.length(1);
    });

    it('deselects an icon', async function () {
      this.set('icons', icons);
      this.set('value', allIcons);

      let selection;
      this.set('selectionChanged', function (value) {
        selection = value;
      });

      await render(
        hbs`<Input::Icons::OpenSelector @icons={{this.icons}} @value={{this.value}} @onSelect={{this.selectionChanged}}/>`
      );

      await click('.btn-icon');

      expect(selection).to.have.length(39);
    });

    it('should filter by icon name', async function () {
      this.set('icons', icons);
      this.set('value', allIcons);

      await render(
        hbs`<Input::Icons::OpenSelector @icons={{this.icons}} @value={{this.value}} />`
      );
      await fillIn('input', 'monument istoric 5');

      const filteredIcons = this.element.querySelectorAll('.icon-element');

      expect(filteredIcons.length).to.equal(1);
      expect(filteredIcons[0].textContent).to.contain('Monument istoric 5');
    });

    it('should show a message when no icon is matched', async function () {
      this.set('icons', icons);
      this.set('value', allIcons);

      await render(
        hbs`<Input::Icons::OpenSelector @icons={{this.icons}} @value={{this.value}} />`
      );
      await fillIn('input', 'something');

      const filteredIcons = this.element.querySelectorAll('.icon-element');

      expect(filteredIcons.length).to.equal(0);
      expect(this.element.querySelector('.not-matched-message')).to.exist;
    });
  });
});
