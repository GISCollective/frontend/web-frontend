/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export default class EmbedVideo {
  constructor({ data }) {
    this.data = data;
  }

  static get isReadOnlySupported() {
    return false;
  }

  rendered() {
    if (!this.element?.parentElement?.parentElement) {
      return;
    }

    this.element.parentElement.parentElement.classList.add('is-embed');
  }

  save(blockContent) {
    return {
      container: {
        style: {
          sm: {
            width: `${blockContent.querySelector('.embedded-video-width').value}px`,
          },
        },
      },
      source: {
        url: blockContent.querySelector('.embedded-video-url').value,
      },
    };
  }


  render() {
    const container = document.createElement('div');

    container.innerHTML = `<div class="page-col-embedded-video placeholder">
      ${EmbedVideo.getEditorIcon('30px')}
    </div>

    <div class="embedded-embedded-video-url mt-2 mb-3">
      <div class="row">
        <div class="col col-12 col-md-8">
          <div class="input-group mb-3">
            <span class="input-group-text">Mp4 video url</span>
            <input
              type="text"
              class="form-control embedded-video-url"
              aria-label="video url"
            />
          </div>
        </div>

        <div class="col">
          <div class="col input-group mb-3">
            <span class="input-group-text">${EmbedVideo.widthIcon}</span>
            <input
              type="number"
              class="form-control cdx-search-field__input embedded-video-width"
              placeholder="Width (px)"
            />
          </div>
        </div>
      </div>
    </div>`;

    container.querySelector('.embedded-video-url').value = this.data?.source?.url ?? '';
    container.querySelector('.embedded-video-width').value = (this.data?.container?.style?.sm?.width ?? '').replace("px", "");

    return container;
  }

  static get widthIcon() {
    return `<svg style="height: 14px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
        <!-- Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
        <path fill="currentColor" d="M406.6 374.6l96-96c12.5-12.5 12.5-32.8 0-45.3l-96-96c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L402.7 224l-293.5 0 41.4-41.4c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-96 96c-12.5 12.5-12.5 32.8 0 45.3l96 96c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.3 288l293.5 0-41.4 41.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0z"/>
      </svg>`;
  }

  static getEditorIcon(size) {
    return `<div class="video-placeholder" style = "height: ${size}" > <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
      <!-- Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
      <path fill="currentColor" d="M0 128C0 92.7 28.7 64 64 64l256 0c35.3 0 64 28.7 64 64l0 256c0 35.3-28.7 64-64 64L64 448c-35.3 0-64-28.7-64-64L0 128zM559.1 99.8c10.4 5.6 16.9 16.4 16.9 28.2l0 256c0 11.8-6.5 22.6-16.9 28.2s-23 5-32.9-1.6l-96-64L416 337.1l0-17.1 0-128 0-17.1 14.2-9.5 96-64c9.8-6.5 22.4-7.2 32.9-1.6z"/>
    </svg></div> `;
  }

  static get toolbox() {
    return {
      title: 'Embedded Mp4 video',
      icon: EmbedVideo.getEditorIcon('12px'),
    };
  }
}
