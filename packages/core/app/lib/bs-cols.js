/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/

export {
  validBsSizes,
  isValidBsSize,
  hasBsSize,
  isPropertyWithoutSize,
  setDefaultBsProperty,
  withoutBsSize,
  isAnyOfBsProperty,
  extractProperty,
  hasBsSizedVersion,
  setSizedBsProperty,
  defaultBsProperty,
  sizedBsProperty,
} from 'core/lib/bs-cols';
