/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/icon/grid-with-categories', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let iconSetData;

  hooks.beforeEach(async function () {
    server = new TestServer();

    iconSetData = server.testData.storage.addDefaultIconSet();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Icon::GridWithCategories />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  describe('when there are 2 icons with 2 different categories and subcategories', function (hooks) {
    let icon1;
    let icon2;
    let icon3;

    hooks.beforeEach(async function () {
      icon1 = server.testData.storage.addDefaultIcon('1');
      icon1.category = 'category1';
      icon1.subcategory = 'subcategory1';

      icon2 = server.testData.storage.addDefaultIcon('2');
      icon2.category = 'category2';
      icon2.subcategory = 'subcategory2';

      icon3 = server.testData.storage.addDefaultIcon('3');
      icon3.category = 'category1';
      icon3.subcategory = 'subcategory3';

      const iconSet = await this.store.findRecord('icon-set', iconSetData._id);

      this.set('value', {
        data: {
          categories: {
            button: {
              classes: ['fw-bold', 'text-size-09'],
            },
          },
        },
        modelKey: 'iconSet',
      });

      this.set('model', {
        iconSet,
      });

      this.set('changeState', (s) => this.set('state', s));
      this.set('state', '');
    });

    it('renders the two categories', async function (a) {
      await render(
        hbs`<View::Icon::GridWithCategories @state={{this.state}} @onChangeState={{this.changeState}} @value={{this.value}} @model={{this.model}} />`,
      );

      await waitUntil(
        () => this.element.querySelectorAll('.categories-list .btn').length,
      );

      const categories = [
        ...this.element.querySelectorAll('.categories-list .btn'),
      ];

      expect(categories.length).to.equal(2);
      expect(categories[0].textContent.trim()).to.equal('category1');
      expect(categories[1].textContent.trim()).to.equal('category2');
    });

    it('renders the subcategories of the first category', async function () {
      await render(
        hbs`<View::Icon::GridWithCategories @state={{this.state}} @onChangeState={{this.changeState}} @value={{this.value}} @model={{this.model}} />`,
      );

      await waitUntil(
        () => this.element.querySelectorAll('.subcategories-list .btn').length,
      );

      const subcategories = [
        ...this.element.querySelectorAll('.subcategories-list .btn'),
      ];

      expect(subcategories.length).to.equal(2);
      expect(subcategories[0].textContent.trim()).to.equal('subcategory1');
      expect(subcategories[1].textContent.trim()).to.equal('subcategory3');
    });

    it('renders the first category by default', async function (a) {
      await render(
        hbs`<View::Icon::GridWithCategories @state={{this.state}} @onChangeState={{this.changeState}} @value={{this.value}} @model={{this.model}} />`,
      );

      await waitUntil(
        () => this.element.querySelectorAll('.subcategories-list .btn').length,
      );

      const categories = [
        ...this.element.querySelectorAll('.categories-list .btn'),
      ];

      expect(categories.length).to.equal(2);
      expect(categories[0]).to.have.classes('btn btn-group-name btn-selected');
      expect(categories[1]).to.have.classes('btn btn-group-name');
    });

    it('selects the subcategory when the category is updated', async function (a) {
      await render(
        hbs`<View::Icon::GridWithCategories @state={{this.state}} @onChangeState={{this.changeState}} @value={{this.value}} @model={{this.model}} />`,
      );

      const categories = [
        ...this.element.querySelectorAll('.categories-list .btn'),
      ];
      await click(categories[1]);

      await waitUntil(() => this.element.querySelector('.icon'));
    });

    it('renders the first subcategory by default', async function () {
      await render(
        hbs`<View::Icon::GridWithCategories @state={{this.state}} @onChangeState={{this.changeState}} @value={{this.value}} @model={{this.model}} />`,
      );
      await waitUntil(
        () => this.element.querySelectorAll('.categories-list .btn').length,
      );

      const categories = [
        ...this.element.querySelectorAll('.subcategories-list .btn'),
      ];

      expect(categories.length).to.equal(2);
      expect(categories[0]).to.have.classes('btn btn-group-name btn-selected');
      expect(categories[1]).to.have.attribute('class', 'btn btn-group-name');
    });

    it('renders an active category on click', async function () {
      await render(
        hbs`<View::Icon::GridWithCategories @state={{this.state}} @onChangeState={{this.changeState}} @value={{this.value}} @model={{this.model}} />`,
      );
      await waitUntil(
        () => this.element.querySelectorAll('.categories-list .btn').length,
      );

      let categories = [
        ...this.element.querySelectorAll('.categories-list .btn'),
      ];

      await click(categories[1]);

      expect(
        this.element.querySelector('.btn-selected').textContent.trim(),
      ).to.equal('category2');
    });
  });
});
