/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | browse/primary-picture',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when a picture is not set', async function () {
    await render(hbs`<Browse::PrimaryPicture />`);
    expect(this.element.querySelector('.browse-primary-picture')).not.to.exist;
  });

  it('renders a picture', async function () {
    this.set('pictures', [{ picture: 'picture1' }]);

    await render(hbs`<Browse::PrimaryPicture @pictures={{this.pictures}} />`);

    let images = this.element.querySelectorAll('img');

    expect(images.length).to.equal(1);
    expect(images[0]).to.have.attribute('src', 'picture1/md');
  });
});
