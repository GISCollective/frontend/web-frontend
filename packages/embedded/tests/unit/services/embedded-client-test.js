import { module } from 'qunit';
import { setupTest, it, describe } from 'embedded/tests/helpers';
import { expect } from 'chai';

module('Unit | Service | embedded-client', function (hooks) {
  setupTest(hooks);
  let service;

  hooks.beforeEach(function () {
    service = this.owner.lookup('service:embedded-client');
    service.isReady = false;
    service.setConfig('apiUrl', '');
    service.setConfig('route', '');
  });

  it('exists', function (assert) {
    assert.ok(service);
  });

  describe('setup', async function () {
    it('is not ready by default', function () {
      service.setup();

      expect(service.isReady).not.to.equal(true);
    });

    it('is ready after the config is set up', async function () {
      const promise = service.setup();

      service.setConfig('apiUrl', 'some value');
      expect(service.isReady).to.equal(false);

      service.setConfig('route', 'other value');

      await promise;
      expect(service.isReady).to.equal(true);
    });
  });

  describe('window', async function () {
    it('sets the setConfig global', function () {
      expect(window.setConfig).to.exist;
    });

    it('can set a config using the global function', function () {
      window.setConfig('key', 'value');

      expect(service.config.key).to.equal('value');
    });
  });

  describe('sendMessage', async function (hooks) {
    let lastMessage;

    hooks.beforeEach(function () {
      lastMessage = undefined;

      window.ReactNativeWebView = {
        postMessage(message) {
          lastMessage = message;

          const parsedMessage = JSON.parse(message);
          window.responses?.[parsedMessage.messageIndex]?.resolve('hello');
        },
      };
    });

    it('returns the host answer for a storeFetch message', async function (a) {
      const response = await service.sendMessage('storeFetch', {
        info: 1,
        type: 2,
        options: 3,
      });

      a.deepEqual(JSON.parse(lastMessage), {
        name: 'storeFetch',
        args: [{ info: 1, type: 2, options: 3 }],
        messageIndex: 0,
      });
      expect(response).to.equal('hello');
    });
  });
});
