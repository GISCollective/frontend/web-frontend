/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ManageCampaignAnswerReviewComponent extends Component {
  get canReview() {
    return this.args.value?.status != 2 && this.args.value?.status != 3 && this.args.value?.status != 4;
  }

  get canRevert() {
    return this.args.value?.status >= 2;
  }

  @action
  approve() {
    return this.args.value?.approve();
  }

  @action
  reject() {
    return this.args.value?.reject();
  }

  @action
  revert() {
    return this.args.value?.revert();
  }

  @action
  async setup() {
    await this.args.value?.review?.fetch();
  }
}
