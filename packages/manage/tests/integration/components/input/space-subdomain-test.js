/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | input/space-subdomain', function (hooks) {
  setupRenderingTest(hooks);

  let server;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultPreferences();
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders an empty empty input when the value', async function () {
    await render(hbs`<Input::SpaceSubdomain />`);

    expect(this.element.querySelector('.input-space-subdomain').value).to.equal('');
  });

  it('renders the domain as an addon when there is just one in the preference', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');

    await render(hbs`<Input::SpaceSubdomain />`);

    expect(this.element.querySelector('.input-group-text').textContent.trim()).to.equal('.giscollective.com');
  });

  it('renders a select when there are 2 available domains', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com,other.com');

    await render(hbs`<Input::SpaceSubdomain />`);
    expect(this.element.querySelector('.input-group-domain')).not.to.exist;
    expect(this.element.querySelector('.select-domain').value).to.equal('giscollective.com');
  });

  it('sets the subdomain as the text input value', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');

    await render(hbs`<Input::SpaceSubdomain @value="subdomain.giscollective.com"/>`);

    expect(this.element.querySelector('.input-space-subdomain').value).to.equal('subdomain');
  });

  it('can set a domain with dashes', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');

    let value;
    this.set('change', function (v) {
      value = v;
    });

    await render(hbs`<Input::SpaceSubdomain @value="subdomain.giscollective.com" @onChange={{this.change}}/>`);

    await fillIn('.input-space-subdomain', 'ot-her');

    expect(this.element.querySelector(".invalid-feedback")).not.to.exist;

    expect(value).to.equal('ot-her.giscollective.com');
  });

  it('can change the subdomain when the domain can not be changed', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');

    let value;
    this.set('change', function (v) {
      value = v;
    });

    await render(hbs`<Input::SpaceSubdomain @value="subdomain.giscollective.com" @onChange={{this.change}}/>`);

    await fillIn('.input-space-subdomain', 'other');

    expect(value).to.equal('other.giscollective.com');
  });

  it('can change the domain when there are more options', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com,other.com');

    let value;
    this.set('change', function (v) {
      value = v;
    });

    await render(hbs`<Input::SpaceSubdomain @value="subdomain.giscollective.com" @onChange={{this.change}}/>`);

    this.element.querySelector('.select-domain').value = 'other.com';
    await triggerEvent('.select-domain', 'change');

    expect(value).to.equal('subdomain.other.com');
  });

  it('shows an error message when the value has an invalid domain', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');

    await render(hbs`<Input::SpaceSubdomain @value="subdomain.other.com"/>`);

    expect(this.element.querySelector('.input-group-domain').textContent.trim()).to.equal('.other.com');

    expect(this.element.querySelector('.invalid-feedback').textContent.trim()).to.equal(
      '.other.com will be replaced with .giscollective.com',
    );
  });

  it('shows an error message when the value has invalid chars', async function () {
    server.testData.storage.addPreference('spaces.domains', 'giscollective.com');

    await render(hbs`<Input::SpaceSubdomain @value="subd**@omain.giscollective.com"/>`);

    expect(this.element.querySelector('.input-space-subdomain')).to.have.class('is-invalid');

    expect(this.element.querySelector('.invalid-feedback').textContent.trim()).to.equal(
      'The domain name must contain only letters and number from the english alphabet.',
    );
  });
});
