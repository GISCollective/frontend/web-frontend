/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { run } from '@ember/runloop';

export default function destroyApp(application) {
  run(application, 'destroy');
}
