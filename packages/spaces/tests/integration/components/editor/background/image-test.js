/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, fillIn, waitUntil } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";

describe("Integration | Component | editor/background/image", function (hooks) {
  setupRenderingTest(hooks);

  let picture;
  let server;
  let receivedPicture;
  let value;
  let pageData;

  hooks.before(function () {
    server = new TestServer();

    receivedPicture = null;
    server.post("/mock-server/pictures", (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = "1";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedPicture)];
    });

    picture = server.testData.storage.addDefaultPicture();
    pageData = server.testData.storage.addDefaultPage();
  });

  hooks.beforeEach(function () {
    receivedPicture = null;

    this.set("value", {
      data: {
        source: {
          id: picture._id,
          model: "picture",
        },
        options: ["bg-size-fixed"],
        style: {
          sm: {
            "background-size": [
              { value: 30, unit: "%" },
              { value: 50, unit: "%" },
            ],
          },
        },
      },
    });
    this.set("onChange", (v) => {
      value = v;
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("renders the provided values", async function () {
    this.set("value", {
      data: {
        options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-x-left", "bg-position-y-top"],
        style: {
          sm: {
            "background-size": [
              { value: 30, unit: "%" },
              { value: 50, unit: "%" },
            ],
          },
        },
      },
    });

    await render(hbs`<Editor::Background::Image @value={{this.value}} @onChange={{this.onChange}} />`);

    expect(this.element.querySelector(".input-sm-bg-repeat").value).to.equal("no-repeat");
    expect(this.element.querySelector(".input-sm-bg-size").value).to.equal("cover");
    expect(this.element.querySelector(".input-sm-bg-position-x").value).to.equal("left");
    expect(this.element.querySelector(".input-sm-bg-position-y").value).to.equal("top");

    expect(this.element.querySelector(".input-width")).not.to.exist;
  });

  it("can set a fixed bg position x", async function () {
    this.set("value", {
      data: {
        options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-x-left", "bg-position-y-top"],
        style: {
          sm: {
            "background-size": [
              { value: 30, unit: "%" },
              { value: 50, unit: "%" },
            ],
          },
        },
      },
    });

    await render(hbs`<Editor::Background::Image @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input-sm-bg-position-x").value = "fixed";
    await triggerEvent(".input-sm-bg-position-x", "change");

    await fillIn(".input-dimension-numeric", "20");

    expect(value).to.deep.equal({
      options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-y-top", "bg-position-x-fixed"],
      style: { sm: { "background-position-x": { value: 20, unit: "px" } } },
      source: { id: undefined, model: "picture" },
    });
  });

  it("can set a fixed bg position y", async function () {
    this.set("value", {
      data: {
        options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-x-left", "bg-position-y-top"],
        style: {
          sm: {
            "background-size": [
              { value: 30, unit: "%" },
              { value: 50, unit: "%" },
            ],
          },
        },
      },
    });

    await render(hbs`<Editor::Background::Image @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input-sm-bg-position-y").value = "fixed";
    await triggerEvent(".input-sm-bg-position-y", "change");

    await fillIn(".input-dimension-numeric", "20");

    expect(value).to.deep.equal({
      options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-x-left", "bg-position-y-fixed"],
      style: { sm: { "background-position-y": { value: 20, unit: "px" } } },
      source: { id: undefined, model: "picture" },
    });
  });

  it("can set a centered bg position x", async function () {
    this.set("value", {
      data: {
        options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-x-fixed", "bg-position-y-top"],
        style: {
          sm: {
            "background-position-x": { value: 20, unit: "px" },
          },
        },
      },
    });

    await render(hbs`<Editor::Background::Image @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input-sm-bg-position-x").value = "center";
    await triggerEvent(".input-sm-bg-position-x", "change");

    expect(value).to.deep.equal({
      options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-y-top", "bg-position-x-center"],
      style: { sm: {} },
      source: { id: undefined, model: "picture" },
    });
  });

  it("can set a centered bg position y", async function () {
    this.set("value", {
      data: {
        options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-x-left", "bg-position-y-fixed"],
        style: {
          sm: {
            "background-position-y": { value: 20, unit: "px" },
          },
        },
      },
    });

    await render(hbs`<Editor::Background::Image @value={{this.value}} @onChange={{this.onChange}} />`);

    this.element.querySelector(".input-sm-bg-position-y").value = "center";
    await triggerEvent(".input-sm-bg-position-y", "change");

    expect(value).to.deep.equal({
      options: ["bg-repeat-no-repeat", "bg-size-cover", "bg-position-x-left", "bg-position-y-center"],
      style: { sm: {} },
      source: { id: undefined, model: "picture" },
    });
  });

  it("can upload a picture", async function () {
    this.set("value", {
      data: {},
    });

    this.set("page", await this.store.findRecord("page", pageData._id));

    await render(
      hbs`<Editor::Background::Image @page={{this.page}} @value={{this.value}} @onChange={{this.onChange}} />`,
    );

    const blob = server.testData.create.pngBlob();

    await triggerEvent("input[type='file']", "change", {
      files: [blob],
    });

    await waitUntil(() => receivedPicture);

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: "",
      attributions: "",
      link: {
        model: "Page",
        id: "000000000000000000000001",
      },
      data: {},
      disableOptimization: false,
    });
  });
});

describe("with the sm size", function (hooks) {
  setupRenderingTest(hooks);
  let picture;
  let server;
  let receivedPicture;
  let value;
  let pageData;

  hooks.before(function () {
    server = new TestServer();

    receivedPicture = null;
    server.post("/mock-server/pictures", (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = "1";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedPicture)];
    });

    picture = server.testData.storage.addDefaultPicture();
    pageData = server.testData.storage.addDefaultPage();
  });

  hooks.beforeEach(function () {
    receivedPicture = null;

    this.set("value", {
      data: {
        source: {
          id: picture._id,
          model: "picture",
        },
        options: ["bg-size-fixed"],
        style: {
          sm: {
            "background-size": [
              { value: 30, unit: "%" },
              { value: 50, unit: "%" },
            ],
          },
        },
      },
    });
    this.set("onChange", (v) => {
      value = v;
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    await render(hbs`<Editor::Background::Image @value={{this.value}} @onChange={{this.onChange}} />`);

    await waitUntil(() => this.element.querySelector(".input-picture .image").attributes.getNamedItem("style").value);
  });

  it("renders the picture editor", async function () {
    expect(this.element.querySelector(".input-picture .image")).to.have.attribute(
      "style",
      `background-image: url('/test/5d5aa72acac72c010043fb59.jpg.md.jpg?rnd=undefined')`,
    );
  });

  it("renders the dimensions", async function () {
    expect(
      this.element.querySelector(".responsive-value-sm .input-width-group .input-dimension-numeric").value,
    ).to.equal("30");
    expect(this.element.querySelector(".responsive-value-sm .input-width-group .input-dimension-unit").value).to.equal(
      "%",
    );

    expect(
      this.element.querySelector(".responsive-value-sm .input-height-group .input-dimension-numeric").value,
    ).to.equal("50");
    expect(this.element.querySelector(".responsive-value-sm .input-height-group .input-dimension-unit").value).to.equal(
      "%",
    );
  });

  it("allows to set a picture", async function () {
    const blob = server.testData.create.pngBlob();
    await triggerEvent("input[type='file']", "change", { files: [blob] });

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      options: ["bg-size-fixed"],
      style: {
        sm: {
          "background-size": [
            { value: 30, unit: "%" },
            { value: 50, unit: "%" },
          ],
        },
      },
      source: { id: "1", model: "picture" },
    });
  });

  it("allows to set the background size to contain", async function () {
    this.element.querySelector(".input-sm-bg-size").value = "contain";
    await triggerEvent(".input-sm-bg-size", "change");

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      options: ["bg-size-contain"],
      style: { sm: {} },
      source: { id: "5cc8dc1038e882010061545a", model: "picture" },
    });
  });

  it("allows setting custom dimensions", async function () {
    expect(this.element.querySelector(".input-sm-width")).not.to.exist;
    expect(this.element.querySelector(".input-sm-height")).not.to.exist;

    this.element.querySelector(".input-sm-bg-size").value = "fixed";
    await triggerEvent(".input-sm-bg-size", "change");

    expect(this.element.querySelector(".input-width-group")).to.exist;
    expect(this.element.querySelector(".input-height-group")).to.exist;

    await fillIn(".input-width-group .input-dimension-numeric", "10");
    await fillIn(".input-height-group .input-dimension-numeric", "20");

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      source: {
        id: "5cc8dc1038e882010061545a",
        model: "picture",
      },
      options: ["bg-size-fixed"],
      style: {
        sm: {
          "background-size": [
            { value: 10, unit: "%" },
            { value: 20, unit: "%" },
          ],
        },
      },
    });
  });

  it("allows to set the background repeat to no-repeat", async function () {
    this.element.querySelector(".input-sm-bg-repeat").value = "no-repeat";
    await triggerEvent(".input-sm-bg-repeat", "change");

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      source: {
        id: "5cc8dc1038e882010061545a",
        model: "picture",
      },
      options: ["bg-size-fixed", "bg-repeat-no-repeat"],
      style: {
        sm: {
          "background-size": [
            { value: 30, unit: "%" },
            { value: 50, unit: "%" },
          ],
        },
      },
    });
  });
});

describe("with the md size", function (hooks) {
  setupRenderingTest(hooks);
  let picture;
  let server;
  let receivedPicture;
  let value;
  let pageData;

  hooks.before(function () {
    server = new TestServer();

    receivedPicture = null;
    server.post("/mock-server/pictures", (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = "1";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedPicture)];
    });

    picture = server.testData.storage.addDefaultPicture();
    pageData = server.testData.storage.addDefaultPage();
  });

  hooks.beforeEach(function () {
    receivedPicture = null;

    this.set("value", {
      data: {
        source: {
          id: picture._id,
          model: "picture",
        },
        options: ["bg-size-fixed"],
        style: {
          sm: {
            "background-size": [
              { value: 30, unit: "%" },
              { value: 50, unit: "%" },
            ],
          },
        },
      },
    });
    this.set("onChange", (v) => {
      value = v;
    });
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    await render(hbs`<Editor::Background::Image @deviceSize="md" @value={{this.value}} @onChange={{this.onChange}} />`);

    await waitUntil(() => this.element.querySelector(".input-picture .image").attributes.getNamedItem("style").value);
  });

  it("allows to set custom dimensions", async function () {
    this.element.querySelector(".input-sm-bg-size").value = "fixed";
    await triggerEvent(".input-sm-bg-size", "change");

    expect(this.element.querySelector(".input-width-group")).to.exist;
    expect(this.element.querySelector(".input-height-group")).to.exist;

    await fillIn(".responsive-value-md .input-width-group .input-dimension-numeric", "10");
    await fillIn(".responsive-value-md .input-height-group .input-dimension-numeric", "20");

    await waitUntil(() => value);

    expect(value).to.deep.equal({
      source: {
        id: "5cc8dc1038e882010061545a",
        model: "picture",
      },
      options: ["bg-size-fixed"],
      style: {
        sm: {
          "background-size": [
            { value: 30, unit: "%" },
            { value: 50, unit: "%" },
          ],
        },
        md: {
          "background-size": [
            { value: 10, unit: "px" },
            { value: 20, unit: "px" },
          ],
        },
      },
    });
  });
});
