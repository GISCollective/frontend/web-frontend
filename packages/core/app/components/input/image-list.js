/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { A } from 'models/lib/array';
import PictureMeta from 'models/lib/picture-meta';
import { buildWaiter } from '@ember/test-waiters';
import { service } from '@ember/service';
import { later } from '@ember/runloop';

const waiter = buildWaiter('image-list-waiter');

export default class InputImageListComponent extends Component {
  @service store;

  @tracked isLoading = false;
  @tracked rnd = [];

  get value() {
    if (!this.args.value || this.args.value.isDestroying) {
      return [{ isAdd: true }];
    }

    return A(this.args.value.slice().concat([{ isAdd: true }]));
  }

  @action
  remove(index) {
    const values = this.value;
    values.splice(index, 1);

    this.triggerOnChange(values);
  }

  @action
  async rotate(index) {
    const values = this.value;
    await values[index].rotate();

    return this.triggerOnChange(values);
  }

  @action
  toggle360(index) {
    const values = this.value;
    values[index].is360 = !values[index].is360;

    return values[index].save();
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    this.triggerOnChange(targetList);
  }

  async triggerOnChange(newValue) {
    const onChange = this.args.onChange || this.args.onchange;

    if (!onChange) {
      return;
    }

    let token = waiter.beginAsync();

    this.isLoading = true;

    newValue = newValue.filter((a) => a.picture);

    let result;

    try {
      result = await onChange(newValue);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
    }

    this.isLoading = false;

    later(() => {
      waiter.endAsync(token);
    }, 100);

    return result;
  }

  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });
  }

  async cameraSuccess(imageURI) {
    const picture = this.createImage();
    picture.set('picture', imageURI);

    if (!picture.meta) {
      picture.meta = new PictureMeta();
    }

    picture.meta.link = {
      model: this.args.model,
      id: this.args.modelId,
    };

    const newValue = this.value.concat([picture]);
    await this.triggerOnChange(newValue);
  }

  @action
  imageRemoved(index) {
    this.args.value.splice(index, 1);
    this.triggerOnChange();
  }

  @action
  setupFileInput(element) {
    this.fileElement = element;
  }

  @action
  selectFile() {
    this.fileElement.click();
  }

  @action
  fileSelected(event) {
    var file = event.target.files[0];

    if (!file) {
      return;
    }

    const token = waiter.beginAsync();
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = async (event) => {
      try {
        await this.cameraSuccess(event.target.result);
      } catch (err) {
        console.error(err);
      }

      waiter.endAsync(token);
    };
  }
}
