/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { service } from "@ember/service";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";

export default class ManageEditorsMenuComponent extends Component {
  elementId = `menu-editor-${guidFor(this)}`;
  availablePositions = ["fixed", "stick", "auto"];
  availableAlignments = ["left", "center", "right"];
  availableShadows = ["none", "small", "medium", "large"];
  availableWidths = ["default", "full"];
  availableSizes = ["mobile", "tablet", "desktop"];

  @tracked _value;
  @tracked _logoStyle;

  @service space;

  get showLogo() {
    return this.value.showLogo;
  }

  set showLogo(value) {
    this.change("showLogo", value);
  }

  get showLogin() {
    return this.value.showLogin;
  }

  set showLogin(value) {
    this.change("showLogin", value);
  }

  get showSearch() {
    return this.value.showSearch;
  }

  set showSearch(value) {
    this.change("showSearch", value);
  }

  get position() {
    return this.value.position;
  }

  get align() {
    return this.value.align;
  }

  get shadow() {
    return this.value.shadow;
  }

  get width() {
    return this.value.width;
  }

  get primaryBackground() {
    return this.value.primaryBackground;
  }

  get primaryText() {
    return this.value.primaryText;
  }

  get primaryBorder() {
    return this.value.primaryBorder;
  }

  get secondaryBackground() {
    return this.value.secondaryBackground;
  }

  get secondaryText() {
    return this.value.secondaryText;
  }

  get secondaryBorder() {
    return this.value.secondaryBorder;
  }

  get selectedBackground() {
    return this.value.selectedBackground;
  }

  get selectedText() {
    return this.value.selectedText;
  }

  get hamburgerFrom() {
    return this.value.hamburgerFrom;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    if (typeof this.args.value?.data == "object") {
      return this.args.value?.data;
    }

    return this.args.value || {};
  }

  get menu() {
    return this.value.menu;
  }

  get logoStyle() {
    if (this._logoStyle) {
      return this._logoStyle;
    }

    return this.value.logo?.style ?? {};
  }

  triggerChange() {
    const newValue = this.value;

    if (!newValue.logo) {
      newValue.logo = {};
    }

    newValue.logo.style = this.logoStyle;

    return this.args.onChange?.(newValue);
  }

  @action
  changeLogoStyle(value) {
    this._logoStyle = value;

    return this.triggerChange();
  }

  @action
  change(key, value) {
    const newValue = JSON.parse(JSON.stringify(this.value));
    newValue[key] = value;

    this._value = newValue;

    return this.triggerChange();
  }
}
