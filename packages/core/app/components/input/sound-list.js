/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce, cancel } from '@ember/runloop';
import { service } from '@ember/service';
import { guidFor } from '@ember/object/internals';
import { A } from 'models/lib/array';

export default class InputSoundListComponent extends Component {
  elementId = `input-photo-gallery-${guidFor(this)}`;
  @service store;
  @tracked isLoading;
  @tracked isError;
  @tracked value = [];
  nameUpdates = {};

  @action
  async setup() {
    const value = (await this.args.value?.slice?.()) ?? [];
    value.push({ isAdd: true });

    this.value = A(value);
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    this.triggerOnChange(targetList);
  }

  @action
  changeName(index) {
    const item = this.value.objectAt(index);

    if (item && item.save) {
      if (this.nameUpdates[item.id]) {
        cancel(this.nameUpdates[item.id]);
      }
      this.nameUpdates[item.id] = debounce(item, item.save, 500);
    }
  }

  @action
  remove(index) {
    const newValue = this.value;
    newValue.splice(index, 1);

    this.triggerOnChange(newValue);
  }

  async triggerOnChange(newValue) {
    this.isLoading = true;

    try {
      await this.args.onChange?.(newValue.filter((a) => !a.isAdd));
    } catch (err) {
      this.isError = true;
    }

    this.isLoading = false;
  }

  @action
  selectFile() {
    this.fileElement.click();
  }

  @action
  setupFileInput(element) {
    this.fileElement = element;
  }

  @action
  fileSelected(event) {
    const file = event.target.files[0];

    if (!file) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = (event) => {
      const sound = this.store.createRecord('sound', {
        name: '',
        feature: this.args.feature?.id,
        campaignAnswer: this.args.campaignAnswer?.id,
        sound: '',
        attributions: '',
      });
      sound.sound = event.target.result;
      sound.name = file.name || '';

      const newValue = this.value.filter((a) => !a.isAdd);
      newValue.push(sound);
      newValue.push({ isAdd: true });

      this.value = A(newValue);
      this.triggerOnChange(newValue);
    };
  }
}
