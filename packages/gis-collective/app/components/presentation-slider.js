/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { getDefaultPresentationContainer } from '../lib/presentation';

export default class PresentationComponent extends Component {
  @tracked selectedPage = -1;

  get cols() {
    return this.args.presentation?.cols ?? [];
  }

  get currentLabel() {
    return this.selectedPage + 1;
  }

  get rowCount() {
    if (!this.cols) {
      return 0;
    }

    return this.cols.map((a) => a.row).reduce((a, b) => Math.max(a, b), 0) + 1;
  }

  get containers() {
    const result = [];

    for (let i = 0; i < this.rowCount; i++) {
      result.push(getDefaultPresentationContainer(false, 0, i));
    }

    return result;
  }

  @action
  setupFirstPage() {
    if (!this.args.presentation?.hasIntro) {
      this.selectedPage = 0;
    }
  }

  @action
  nextPage() {
    if (!this.args.presentation?.hasEnd && this.selectedPage == this.rowCount - 1) {
      return;
    }

    this.selectedPage = this.selectedPage + 1;
  }

  @action
  prevPage() {
    if (!this.args.presentation?.hasIntro && this.selectedPage == 0) {
      return;
    }

    this.selectedPage = this.selectedPage - 1;
  }
}
