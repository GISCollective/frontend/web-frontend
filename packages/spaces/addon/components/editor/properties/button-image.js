import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class EditorAttributesGroup extends Component {
  positionOptions = ["start", "end", "top", "bottom"];

  get value() {
    return this.args.value ?? {};
  }

  @action
  changePosition(position) {
    const newValue = { ...this.value, position };

    this.args.onChange?.(newValue);
  }

  @action
  changeStyle(style) {
    const newValue = { ...this.value, style };

    this.args.onChange?.(newValue);
  }

  @action
  async changePicture(key, value) {
    await value?.save?.();

    const newValue = { ...this.value, [key]: value?.id };

    return this.args.onChange?.(newValue);
  }
}
