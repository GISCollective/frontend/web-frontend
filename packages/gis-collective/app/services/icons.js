/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { service } from '@ember/service';
import { addIconGroupToStore } from '../lib/icons';

export default class iconsService extends Service {
  @service store;

  iconsCache = {};
  groupCache = {};

  toQuery(params, limit) {
    let iconsQuery = {};

    if (limit) {
      iconsQuery.limit = limit;
    }

    if (params.viewbox) {
      iconsQuery.viewbox = params.viewbox;
    }

    if (params.limit) {
      iconsQuery.limit = params.limit;
    }

    if (params.iconSet) {
      iconsQuery.iconSet = params.iconSet;
    }

    if (params.map != '_') {
      iconsQuery.map = params.map;
    }

    return iconsQuery;
  }

  group(params) {
    let iconsQuery = this.toQuery(params);
    const key = JSON.stringify(iconsQuery);

    if (!this.groupCache[key]) {
      this.groupCache[key] = this.store
        .adapterFor('icon')
        .group(iconsQuery)
        .then((iconGroup) => {
          addIconGroupToStore(iconGroup, this.store);
          return iconGroup;
        });
    }

    return this.groupCache[key];
  }

  query(params, limit) {
    let iconsQuery = this.toQuery(params, limit);

    const key = JSON.stringify(iconsQuery);

    if (!this.iconsCache[key]) {
      try {
        this.iconsCache[key] = this.store.query('icon', iconsQuery);
      } catch (err) {
        return [];
      }
    }

    return this.iconsCache[key];
  }
}
