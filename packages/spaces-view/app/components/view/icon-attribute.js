/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { A } from '@ember/array';

export default class PageColIconAttributeComponent extends Component {
  get value() {
    if (this.attributes.length == 1) {
      return this.attributes[0];
    }

    return '';
  }

  get isList() {
    return this.args.options?.icon?.allowMany;
  }

  get listValue() {
    if (!this.isList) {
      return undefined;
    }

    return A(this.attributes);
  }

  get key() {
    return this.args.options?.key ?? '';
  }

  get attributes() {
    if (!this.args.value) {
      return [];
    }

    const key = this.key;
    if (!key) {
      return [];
    }

    const iconNames = this.args.options?.icon?.otherNames?.slice() ?? [];

    if (this.args.options?.icon?.name) {
      iconNames.push(this.args.options?.icon?.name);
    }

    const values = [];

    function extractValues(obj) {
      if (typeof obj[key] != 'undefined') {
        values.push(obj[key]);
      }
    }

    for (let iconName of iconNames) {
      if (!this.args.value[iconName]) {
        continue;
      }

      if (Array.isArray(this.args.value[iconName])) {
        this.args.value[iconName].forEach(extractValues);
      }

      if (typeof this.args.value[iconName] == 'object') {
        extractValues(this.args.value[iconName]);
      }
    }

    return values;
  }
}
