/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { setModifierManager, capabilities } from '@ember/modifier';

export default setModifierManager(
  () => ({
    capabilities: capabilities('3.22', { disableAutoTracking: false }),

    createModifier() {},

    installModifier(_state, element, { positional: [value, ...args], named }) {
      if (!value) {
        return;
      }

      element.dataset.name = value.name;
      element.dataset.gid = value.gid;

      if (named.basePath) {
        element.dataset.path = [
          ...named.basePath.split('.'),
          'data',
          ...named.property.split('.'),
        ]
          .filter((a) => a)
          .join('.');
      }
    },

    updateModifier() {},
    destroyModifier() {},
  }),

  class DidInsertModifier {},
);
