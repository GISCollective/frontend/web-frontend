/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { resolveLinkObject } from '../../lib/links';

export default class PageColButtonComponent extends Component {
  resolveVars(path) {
    if (path.indexOf(':') == -1) {
      return path;
    }

    if (this.args.model?.selectedType) {
      path = path.replace(
        `:${this.args.model.selectedType}-id`,
        this.args.model?.selectedId,
      );
    }

    return path;
  }

  get text() {
    if (this.args.value?.data?.content?.name) {
      return this.args.value.data.content.name;
    }

    return '(...)';
  }

  get link() {
    let destination = Object.values(
      resolveLinkObject(
        this.args.value?.data?.content?.link ?? {},
        this.args.space,
        this.args.space,
      ),
    ).find((a) => a);

    if (typeof destination == 'string') {
      destination = this.resolveVars(destination);
    }

    return destination;
  }
}
