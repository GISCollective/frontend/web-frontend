import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';
import TestServer from "models/test-support/gis-collective/test-server";

describe('Integration | Component | button-picture', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let picture1;
  let picture2;
  let picture3;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIconSet();
    picture1 = server.testData.storage.addDefaultPicture("1");
    picture2 = server.testData.storage.addDefaultPicture("2");
    picture3 = server.testData.storage.addDefaultPicture("3");

    picture2.picture = "/test/2.jpg";
    picture3.picture = "/test/3.jpg";
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<ButtonPicture />`);

    assert.dom().hasText('');
    expect(this.element.querySelector("img")).not.to.exist;
  });

  it('renders the content when set', async function (assert) {
    await render(hbs`<ButtonPicture @text="test" />`);

    assert.dom().hasText('test');
    expect(this.element.querySelector("img")).not.to.exist;
  });

  it('renders the pictures when they are set', async function (assert) {
    this.set("value", {
      content: {
        enablePicture: true,
        link: "https://giscollective.com/page1",
        name: "first button",
        newTab: true,
        storeType: undefined,
        type: "default",
      },
      pictureOptions: {
        "active": "3",
        "hover": "2",
        "picture": "1",
        "position": "top",
        "style": {
          "lg": {
            "height": "3px"
          },
          "md": {
            "height": "2px"
          },
          "sm": {
            "height": "1px"
          }
        }
      },
      style: {},
      containerStyle: {},
    });
    await render(hbs`<ButtonPicture @value={{this.value}} />`);

    expect(this.element.querySelector("img.main-picture")).to.have.attribute("src", "/test/5d5aa72acac72c010043fb59.jpg");
    expect(this.element.querySelector("img.main-picture").height).to.equal(3);
    expect(this.element.querySelector("img.hover")).to.have.attribute("src", "/test/2.jpg");
    expect(this.element.querySelector("img.active")).to.have.attribute("src", "/test/3.jpg");
  });

  it('renders the text set with pictures', async function (assert) {
    this.set("value", {
      content: {
        enablePicture: true,
        link: "https://giscollective.com/page1",
        name: "first button",
        newTab: true,
        storeType: undefined,
        type: "default",
      },
      pictureOptions: {
        "active": "3",
        "hover": "2",
        "picture": "1",
        "position": "top",
        "style": {
          "lg": {
            "height": "3px"
          },
          "md": {
            "height": "2px"
          },
          "sm": {
            "height": "1px"
          }
        }
      },
      style: {},
      containerStyle: {},
    });
    await render(hbs`<ButtonPicture @value={{this.value}} @text="test" />`);

    expect(this.element.querySelector(".text").textContent.trim()).to.equal('test');
  });
});
