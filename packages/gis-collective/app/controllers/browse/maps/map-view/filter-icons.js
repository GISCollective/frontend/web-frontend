/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';

export default class BrowseMapsMapViewFilterIconsController extends Controller {
  @tracked parent;
  @service router;

  @action
  onSelectIcon(icon) {
    this.router.transitionTo('browse.maps.map-view', this.router.currentRoute.parent.params.id, {
      queryParams: {
        icons: icon.id,
      },
    });
  }

  @action
  onDeselectIcon() {
    this.router.transitionTo('browse.maps.map-view', this.router.currentRoute.parent.params.id, {
      queryParams: {
        icons: '',
      },
    });
  }
}
