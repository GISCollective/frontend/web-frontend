/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | manage/menu',  function (hooks) {
  let space;
  let spaceService;

  setupRenderingTest(hooks, { server: true });

  hooks.beforeEach(async function () {
    space = this.server.testData.storage.addDefaultSpace();
    this.server.testData.storage.addDefaultPage();
    this.server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');

    spaceService = this.owner.lookup('service:space');
    this.server.get(`/mock-server/spaces/:id/categories`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({}),
      ];
    });

    await spaceService.setup();
  });

  it('renders the logo of the current space', async function () {
    await render(hbs`<Manage::Menu />`);

    expect(this.element.querySelector(".logo")).to.have.attribute("src", "/test/5d5aa72acac72c010043fb59.jpg.sm.jpg");
  });
});
