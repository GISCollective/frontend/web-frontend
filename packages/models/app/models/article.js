/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Model, { attr, hasMany, belongsTo } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';

export default class ArticleModel extends Model {
  @attr('string') title;
  @attr content;
  @attr('string') slug;

  @attr('string') subject;
  @attr('string') type;
  @attr('string') relatedId;
  @attr('string') status;
  @attr('date') releaseDate;
  @attr('string-list') categories;
  @attr('number', { defaultValue: 999 }) order;

  @attr('model-info') info;

  @attr('boolean') canEdit;
  @attr('visibility') visibility;

  @hasMany('picture', { async: true, inverse: null })
  pictures;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @hasMany('event', { async: true, inverse: null })
  events;

  get name() {
    return this.title;
  }

  get contentBlocks() {
    return toContentBlocks(this.content, this.title);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  get canBePublished() {
    return this.status == 'pending';
  }

  get canBeTested() {
    if (!this.type) {
      return false;
    }

    return this.type.indexOf('newsletter') != -1;
  }

  get canUnpublish() {
    return this.status == 'pending' || this.status == 'publishing';
  }

  get isPublished() {
    return this.visibility.isPublic;
  }

  async coverAndPictures() {
    const cover = await this.cover;

    const pictures = await this.pictures;

    return [cover, ...pictures];
  }

  async publish() {
    if (this.isPublished) {
      return false;
    }

    await this.store.adapterFor(this.constructor.modelName).publish(this.id);
    await this.reload();
  }

  async publishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.publishMany(...arguments);
  }

  async unpublish() {
    if (!this.isPublished) {
      return false;
    }

    await this.store.adapterFor(this.constructor.modelName).unpublish(this.id);
    await this.reload();
  }

  async unpublishMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.unpublishMany(...arguments);
  }

  async sendTestEmail() {
    await this.store.adapterFor(this.constructor.modelName).sendTestEmail(this.id);
    await this.reload();
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.title;
    metaInfo.description = this.firstParagraph;
    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    const cover = await this.cover;

    metaInfo.imgSrc = cover?.picture;

    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    return metaInfo;
  }

  get availableActions() {
    return [this.isPublished ? 'unpublish' : 'publish'];
  }

  deleteMany() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.deleteMany(...arguments);
  }

  extraFieldsFor(model) {
    if (model == 'picture') {
      return ['coverAndPictures'];
    }

    return [];
  }
}
