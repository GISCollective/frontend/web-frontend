/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

export default class QuestionsValidationComponent extends Component {
  get selectedItem() {
    const index = this.args.index ?? 0;
    return this.args.value?.[index];
  }

  get hasReusedName() {
    const index = this.args.index ?? 0;
    const name = this.selectedItem?.name;

    if (!name) {
      return;
    }

    return this.args.value?.slice(0, index).find((a) => a.name == name);
  }
}
