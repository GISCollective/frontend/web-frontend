/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
export default class StringListTransform {
  convert(value) {
    if (!Array.isArray(value)) {
      return [];
    }

    return value.map((a) => `${a}`);
  }

  deserialize(serialized) {
    return this.convert(serialized);
  }

  serialize(deserialized) {
    return this.convert(deserialized);
  }

  static create() {
    return new this();
  }
}
