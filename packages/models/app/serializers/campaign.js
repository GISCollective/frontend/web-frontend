/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import RESTSerializer from '@ember-data/serializer/rest';

export default class CampaignSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    canEdit: { serialize: false },
    notifications: { serialize: false },
    baseMaps: { serialize: false },
    area: { serialize: false },
  };
}
