/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'ogm/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'ogm/tests/helpers';
import { visit, currentURL } from '@ember/test-helpers';

import TestServer from "models/test-support/gis-collective/test-server";
import FastBootMock from 'core/test-support/fast-boot-mock';

describe ('Acceptance | login/activate',  function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('can visit /login/activate', async function () {
    await visit('/login/activate');
    expect(currentURL()).to.equal('/login/activate');

    expect(this.element.querySelector('h2').textContent.trim()).to.equal(
      'Account activation'
    );
    expect(this.element.querySelector('.btn-secondary')).to.exist;
    expect(this.element.querySelector('#identification')).to.exist;
  });

  describe ('when the page is rendered using fastboot',  function (hooks) {
    let hasActivateRequest;

    hooks.beforeEach(function () {
      hasActivateRequest = false;
      server.testData.storage.addDefaultPreferences();
      server.post('/mock-server/users/activate', () => {
        hasActivateRequest = true;
        return [400, { 'Content-Type': 'application/json' }];
      });

      this.owner.register('service:fastboot', FastBootMock);
    });

    it('shows the loading logo', async function () {
      await visit('/login/activate?email=test@yahoo.com&token=13526e27');

      expect(hasActivateRequest).to.equal(false);
      expect(this.element.querySelector('.container.loading')).to.exist;
    });
  });

  describe ('when the activation request fails',  function (hooks) {
    hooks.beforeEach(function () {
      server.post('/mock-server/users/activate', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                description: 'Invalid request.',
                status: 400,
                title: 'Validation error',
              },
            ],
          }),
        ];
      });
    });

    it('should show the error message', async function () {
      await visit('/login/activate?email=test@yahoo.com&token=13526e27');
      expect(currentURL()).to.equal(
        '/login/activate?email=test@yahoo.com&token=13526e27'
      );

      expect(this.element.querySelector('h2').textContent.trim()).to.equal(
        'Account activation'
      );
      expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
        'The activation token has expired. Please try again.'
      );
      expect(this.element.querySelector('.btn-secondary')).to.exist;
      expect(this.element.querySelector('#identification')).to.exist;
    });
  });

  describe ('when the activation is successful',  function (hooks) {
    hooks.beforeEach(function () {
      server.post('/mock-server/users/activate', () => {
        return [204, { 'Content-Type': 'application/json' }];
      });
    });

    it('should confirm activation and not show an error message', async function () {
      await visit('/login/activate?email=test@yahoo.com&token=13526e27');
      expect(currentURL()).to.equal(
        '/login/activate?email=test@yahoo.com&token=13526e27'
      );

      expect(this.element.querySelector('h2').textContent.trim()).to.equal(
        'Account activation'
      );
      expect(this.element.querySelector('.alert')).to.not.exist;
      expect(this.element.querySelector('p').textContent.trim()).to.contain(
        'Your account was activated.'
      );
    });
  });
});
