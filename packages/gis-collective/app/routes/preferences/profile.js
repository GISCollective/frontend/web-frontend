/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../base/authenticated-route';
import { service } from '@ember/service';
import { hash } from 'rsvp';

export default class PreferencesProfile extends AuthenticatedRoute {
  @service user;
  @service preferences;
  @service store;

  async model() {
    if(this.fastboot.isFastBoot) {
      return;
    }

    const profile = await this.store.findRecord('user-profile', this.user.id);
    const detailedLocation = await this.preferences.getPreference('profile.detailedLocation');

    return hash({
      profile,
      detailedLocation
    });
  }
}
