/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | view/article/content', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Article::Content />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the data with the selected style when set', async function () {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        style: {
          color: 'green',
          classes: ['fw-bold'],
          container: { color: 'info', classes: ['border-visibility-yes'] },
        },
      },
      modelKey: 'article',
    });

    this.set('model', {
      article: {
        descriptionContentBlocks: {
          blocks: [
            {
              type: 'paragraph',
              data: {
                text: 'some content',
              },
            },
          ],
        },
      },
    });

    await render(
      hbs`<View::Article::Content @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'some content',
    );
    expect(this.element.querySelector('.article p')).to.have.classes(
      'text-with-options fw-bold text-green',
    );

    expect(
      this.element.querySelector('[data-type="container"]'),
    ).to.have.classes(
      'page-col-base-container bg-info border-visibility-yes container-align-undefined',
    );
  });

  it('uses the provided path when it is inside the data object', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        content: { color: 'green', classes: ['fw-bold'] },
      },
      modelKey: 'article',
    });

    this.set('model', {
      article: {
        descriptionContentBlocks: {
          blocks: [
            {
              type: 'paragraph',
              data: {
                text: 'some content',
              },
            },
          ],
        },
      },
    });

    await render(
      hbs`<View::Article::Content @model={{this.model}} @value={{this.value}} @path="col.0.data.content" />`,
    );

    expect(this.element.querySelector('.text-with-options')).to.have.attribute(
      'class',
      'text-with-options fw-bold text-green',
    );

    expect(this.element.querySelector('.text-with-options')).to.have.attribute(
      'data-path',
      'col.0.data.content.paragraphStyle',
    );
  });

  it('does not add the source path when disableSource is true', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        title: { color: 'green', classes: ['fw-bold', 'display-2'] },
      },
      modelKey: 'article',
    });

    this.set('model', {
      article: { name: 'some title' },
    });

    await render(
      hbs`<View::Article::Content @model={{this.model}} @value={{this.value}} @path="col.0.data.content" @disableSource={{true}} />`,
    );

    expect(this.element.querySelector('.source-record')).to.have.attribute(
      'data-path',
      null,
    );
  });

  it('sets the editor attributes to an article with a paragraph', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        style: { paragraphStyle: { color: 'green', classes: ['fw-bold'] } },
        field: { name: 'description' },
      },
      modelKey: 'map',
      name: 'some name',
    });

    this.set('model', {
      map: {
        constructor: {
          modelName: 'map',
        },
        descriptionContentBlocks: {
          blocks: [
            {
              type: 'paragraph',
              data: {
                text: 'some content',
              },
            },
          ],
        },
        tagLineContentBlocks: 'some tagline',
      },
    });

    await render(
      hbs`<View::Article::Content @path="some.path" @model={{this.model}} @value={{this.value}} />`,
    );

    expect(
      this.element.querySelector('[data-type="paragraph"]'),
    ).to.have.attribute('data-path', 'some.path.data.style.paragraphStyle');
    expect(
      this.element.querySelector('[data-type="paragraph"]'),
    ).to.have.attribute('data-editor', 'text-with-options');
    expect(
      this.element.querySelector('[data-type="paragraph"]'),
    ).to.have.attribute('data-name', 'some name');

    expect(
      this.element.querySelector('[data-type="container"]'),
    ).to.have.attribute('data-path', 'some.path.data.style.container');
    expect(
      this.element.querySelector('[data-type="container"]'),
    ).to.have.attribute('data-editor', 'base/container');
    expect(
      this.element.querySelector('[data-type="container"]'),
    ).to.have.attribute('data-name', 'some name');

    expect(
      this.element.querySelector('[data-type="data source"]'),
    ).to.have.attribute('data-path', 'some.path.data.source');
    expect(
      this.element.querySelector('[data-type="data source"]'),
    ).to.have.attribute('data-editor', 'source-record');
    expect(
      this.element.querySelector('[data-type="data source"]'),
    ).to.have.attribute('data-name', 'some name');
  });

  it('sets the editor attributes to an article with a headings', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        style: { h1Style: { color: 'green', classes: ['fw-bold'] } },
        field: { name: 'description' },
      },
      modelKey: 'map',
      name: 'some name',
    });

    this.set('model', {
      map: {
        constructor: {
          modelName: 'map',
        },
        descriptionContentBlocks: {
          blocks: [
            {
              type: 'header',
              data: {
                level: 1,
                text: 'some content',
              },
            },
          ],
        },
        tagLineContentBlocks: 'some tagline',
      },
    });

    await render(
      hbs`<View::Article::Content @path="some.path" @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('[data-type="h1"]')).to.have.classes(
      'fw-bold text-green',
    );

    expect(this.element.querySelector('[data-type="h1"]')).to.have.attribute(
      'data-path',
      'some.path.data.style.h1Style',
    );
    expect(this.element.querySelector('[data-type="h1"]')).to.have.attribute(
      'data-editor',
      'text-with-options',
    );
    expect(this.element.querySelector('[data-type="h1"]')).to.have.attribute(
      'data-name',
      'some name',
    );
  });

  it('can render only one paragraph', async function (a) {
    this.set('value', {
      data: {
        source: { id: 'published-campaigns', model: 'stat' },
        style: { h1Style: { color: 'green', classes: ['fw-bold'] } },
        field: { name: 'description' },
        contentPicker: { mode: 'paragraph', paragraphIndex: '1' },
      },
      modelKey: 'map',
      name: 'some name',
    });

    this.set('model', {
      map: {
        constructor: {
          modelName: 'map',
        },
        descriptionContentBlocks: {
          blocks: [
            {
              type: 'paragraph',
              data: {
                text: 'text 1',
              },
            },
            {
              type: 'paragraph',
              data: {
                text: 'text 2',
              },
            },
          ],
        },
        tagLineContentBlocks: 'some tagline',
      },
    });

    await render(
      hbs`<View::Article::Content @path="some.path" @model={{this.model}} @value={{this.value}} />`,
    );

    expect(this.element.querySelector('.article-viewer')).to.have.textContent(
      'text 2',
    );
  });
});
