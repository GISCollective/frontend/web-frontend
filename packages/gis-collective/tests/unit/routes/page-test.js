/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Unit | Route | page',  function (hooks) {
  setupTest(hooks);
  let server;
  let route;
  let campaign;

  hooks.beforeEach(async function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultSpace();
    campaign = server.testData.storage.addDefaultCampaign();

    server.get(`/mock-server/spaces/:id/pages`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          'campaigns--:campaign_id': '1',
          help: '2',
          'a--b--c': '3',
        }),
      ];
    });

    server.get(`/mock-server/spaces/:id/categories`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({}),
      ];
    });

    let spaceService = this.owner.lookup('service:space');
    await spaceService.setup();

    route = this.owner.lookup('route:page');
  });

  it('exists', function () {
    expect(route).to.be.ok;
  });

  describe ('toPageSlug',  function (hooks) {
    it('ignores the query params from page query', function () {
      expect(route.toPageSlug('help?lang=2')).to.equal('help');
    });

    it('converts the sashes to double dashes', async function () {
      expect(route.toPageSlug('a/b/c')).to.equal('a--b--c');
    });
  });

  describe ('toPageId',  function (hooks) {
    it('matches a path with a query param', function () {
      expect(route.toPageId('help?lang=2')).to.equal('2');
    });

    it('matches a path with 3 components', async function () {
      expect(route.toPageId('a/b/c')).to.equal('3');
    });

    it('matches a path with variables', async function () {
      expect(route.toPageId('campaigns/5')).to.equal('1');
    });
  });
});
