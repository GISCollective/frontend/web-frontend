/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { tracked } from '@glimmer/tracking';

class EditDataBindingModel {
  @tracked dataBinding;
  @tracked teams;
  @tracked team;
  @tracked icons;
}

export default class ManageDataBindingsEditRoute extends AuthenticatedRoute {
  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const teamsParams = {};

    teamsParams.all = !!params.allTeams;
    teamsParams.edit = !params.allTeams;

    const model = new EditDataBindingModel();

    model.dataBinding = await this.store.findRecord('data-binding', params.id);
    model.team = await model.dataBinding.visibility.fetchTeam();
    model.teams = await this.store.query('team', teamsParams);
    model.icons = await model.dataBinding.extraIcons;

    return model;
  }
}
