/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe ('Integration | Helper | nice-distance',  function (hooks) {
  setupRenderingTest(hooks);

  // Replace this with your real tests.
  it('renders', async function () {
    this.set('location1', { coordinates: [0, 0] });
    this.set('location2', [1, 1]);

    await render(hbs`{{nice-distance this.location1 this.location2}}`);

    expect(this.element.textContent.trim()).to.equal('157.25km');
  });
});
