import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | input/text-email-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders an empty text area by default', async function (assert) {
    await render(hbs`<Input::TextEmailList />`);

    expect(this.element.querySelector('.form-text')).to.have.textContent('no emails');
    expect(this.element.querySelector('textarea').value).to.equal('');
  });

  it('renders a list of emails when the value is an array', async function (assert) {
    this.set('value', ['a@b.com', 'b@c.com']);

    await render(hbs`<Input::TextEmailList @value={{this.value}} />`);

    expect(this.element.querySelector('textarea').value).to.equal(`a@b.com\nb@c.com`);
    expect(this.element.querySelector('.form-text')).to.have.textContent('2 emails');
  });

  it('triggers onChange when an email is added', async function (assert) {
    this.set('value', '');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::TextEmailList @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('textarea', 'test@a.com');

    expect(value).to.deep.equal(['test@a.com']);
    expect(this.element.querySelector('.form-text')).to.have.textContent('one email');
  });

  it('triggers onChange when two emails are added on separate lines', async function (assert) {
    this.set('value', '');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::TextEmailList @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('textarea', 'test1@a.com\ntest2@a.com\n');

    expect(value).to.deep.equal(['test1@a.com', 'test2@a.com']);
    expect(this.element.querySelector('.form-text')).to.have.textContent('2 emails');
  });

  it('triggers onChange when two emails are separated with spaces', async function (assert) {
    this.set('value', '');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::TextEmailList @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('textarea', 'test1@a.com test2@a.com');

    expect(value).to.deep.equal(['test1@a.com', 'test2@a.com']);
    expect(this.element.querySelector('.form-text')).to.have.textContent('2 emails');
  });

  it('triggers onChange when two emails are separated with ,', async function (assert) {
    this.set('value', '');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::TextEmailList @value={{this.value}} @onChange={{this.change}} />`);

    await fillIn('textarea', 'test1@a.com, test2@a.com');

    expect(value).to.deep.equal(['test1@a.com', 'test2@a.com']);
    expect(this.element.querySelector('.form-text')).to.have.textContent('2 emails');
  });
});
