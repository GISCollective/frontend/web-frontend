/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import sanitizeHtml from 'sanitize-html';

export default class ArticleFirstParagraphComponent extends Component {
  get max() {
    if (this.args.max) {
      return parseInt(this.args.max);
    }

    return 200;
  }

  get firstParagraph() {
    if (!this.args.contentBlocks?.blocks) {
      return '';
    }

    const paragraph = this.args.contentBlocks.blocks
      .filter((a) => a.type == 'paragraph')
      .map((a) => a?.data?.text ?? '')
      .join(' ');

    if (!paragraph) {
      return '';
    }

    if (paragraph.length < this.max - 5) {
      const clean = sanitizeHtml(paragraph);
      return htmlSafe(clean);
    }

    const clean = paragraph.replace(/(<([^>]+)>)/gi, '');
    return htmlSafe(clean.substr(0, this.max) + '...');
  }
}
