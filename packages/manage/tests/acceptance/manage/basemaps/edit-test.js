/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, waitUntil, triggerEvent } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import PageElements from 'core/test-support/page-elements';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | manage/basemaps/edit', function (hooks) {
  setupApplicationTest(hooks);
  let server;
  let receivedBaseMap;
  let baseMap;
  let receivedPicture;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(async function () {
    server.shutdown();
    await new Promise((resolve) => setTimeout(resolve, 100));
  });

  hooks.beforeEach(function () {
    server.testData.storage.addDefaultSpace();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    const defaultPicture = server.testData.storage.addDefaultPicture();
    baseMap = server.testData.create.baseMap();
    server.testData.storage.addDefaultBaseMap(baseMap._id);

    server.history = [];
    server.get('/mock-server/basemaps/1', () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedBaseMap)];
    });

    receivedBaseMap = null;
    server.put(`/mock-server/basemaps/${baseMap._id}`, (request) => {
      receivedBaseMap = JSON.parse(request.requestBody);
      receivedBaseMap.baseMap._id = baseMap._id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedBaseMap)];
    });

    receivedPicture = null;
    server.post(`/mock-server/pictures`, (request) => {
      receivedPicture = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });
  });

  hooks.afterEach(async function () {
    await server.waitAllRequests();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/basemaps/edit/${baseMap._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fbasemaps%2Fedit%2F000000111111222222333333');
  });

  it('can visit the edit page of an existing basemap', async function () {
    authenticateSession();
    await visit(`/manage/basemaps/edit/${baseMap._id}`);
    expect(currentURL()).to.equal(`/manage/basemaps/edit/${baseMap._id}`);

    expect(this.element.querySelector('.error')).not.to.exist;
    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'name of basemap', 'Edit']);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal('Edit base map');
    expect(PageElements.rowTitles()).to.deep.equal(['name', 'attributions', 'layers', 'cover photo']);
  });

  it('should be able to change the team', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/edit/${baseMap._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    this.element.querySelector('.value.editing select').value = '000000000000000000000001';
    await triggerEvent('.value.editing select', 'change');

    await waitUntil(() => receivedBaseMap != null, { timeout: 3000 });

    expect(receivedBaseMap.baseMap.visibility.team).to.equal('000000000000000000000001');
  });

  it('should render the "is public" label', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/edit/${baseMap._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain('is public');
    expect(this.element.querySelector('.container-group-is-public label').textContent).to.contain('is public');
  });

  it('should render the "is default" label', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/edit/${baseMap._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain('is default');
    expect(this.element.querySelector('.container-group-is-default label').textContent).to.contain('is default');
  });

  it('should be able to unpublish the baseMap', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/edit/${baseMap._id}`);
    await server.waitAllRequests();
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-is-public .btn-switch'));

    await waitUntil(() => receivedBaseMap != null, { timeout: 3000 });

    expect(receivedBaseMap.baseMap.visibility.isPublic).to.equal(false);
  });

  it('should be able to set the baseMap as default', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/edit/${baseMap._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector('.side-bar .container-group-is-default .btn-switch'));

    await waitUntil(() => receivedBaseMap != null, { timeout: 3000 });

    expect(receivedBaseMap.baseMap.visibility.isDefault).to.equal(true);
  });

  it('should not show the show all teams button', async function () {
    authenticateSession();
    await visit(`/manage/basemaps/edit/${baseMap._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).not.to.exist;
  });

  it('can change the cover', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/edit/${baseMap._id}`);

    await click('.row-cover .btn-edit');

    const blob = server.testData.create.pngBlob();
    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });
    await waitUntil(() => !this.element.querySelector('.btn-submit').hasAttribute('disabled'), { timeout: 3000 });
    await click('.btn-submit');

    await waitUntil(() => receivedBaseMap != null, { timeout: 3000 });

    expect(receivedPicture.picture.meta).to.deep.equal({
      renderMode: '',
      attributions: '',
      link: { model: 'baseMap', id: '000000111111222222333333' },
      data: {},
      disableOptimization: false,
    });

    expect(receivedBaseMap.baseMap.cover).to.equal('5cc8dc1038e882010061545a');

    await server.waitAllRequests();
    await PageElements.wait(1000);
  });

  describe('when the authenticated user is an administrator', function (hooks) {
    hooks.beforeEach(function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('should show all teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/basemaps/edit/${baseMap._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?all=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/basemaps/edit/${baseMap._id}?allTeams=true`);
      expect(server.history).to.contain(`GET /mock-server/teams?all=true`);

      expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).to.exist;
    });

    it('should show owned teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/basemaps/edit/${baseMap._id}?allTeams=true`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?edit=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/basemaps/edit/${baseMap._id}`);
      expect(server.history).to.contain(`GET /mock-server/teams?edit=true`);

      expect(this.element.querySelector('.container-group-team .btn-show-all-teams')).to.exist;
    });
  });
});
