/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { addIconGroupToStore } from '../../../lib/icons';
import { service } from '@ember/service';

export default class ManageSitesEditRoute extends AuthenticatedRoute {
  @service user;

  queryParams = {
    parentMap: {
      refreshModel: true,
    },
  };

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return hash({
        feature: this.store.findRecord('feature', params.id),
      });
    }

    const baseMaps = await this.store.query('base-map', { default: true });
    const feature = await this.store.findRecord('feature', params.id);

    let maps = await feature.maps;
    let parentMap = maps[0];

    if (params.parentMap) {
      parentMap = this.store.findRecord('map', params.parentMap);
    }

    let team = await parentMap?.get('visibility')?.fetchTeam();
    let iconSets = await feature.iconSets;

    if (iconSets.length == 0) {
      iconSets = (await this.store.query('icon-set', { default: true })).map((a) => a.id);
    }

    const icons = await this.store.adapterFor('icon').group({ iconSet: iconSets.join() });

    addIconGroupToStore(icons, this.store);

    const teams = await this.store.query('team', { user: this.user.id });
    const mapQueries = await Promise.all(
      teams.map((a) =>
        this.store.query('map', {
          team: a.id,
        }),
      ),
    );

    const pictures = feature.pictures;
    const sounds = feature.sounds;

    let contributors = [];

    try {
      contributors = await feature.contributors;
    } catch (err) {
      console.error(err);
    }

    const area = maps.slice()?.[0]?.area;

    return hash({
      maps: mapQueries.map((a) => a.slice()).flatMap((a) => a),
      area,
      team,
      icons,
      feature,
      baseMaps,
      parentMap,
      featureIcons: feature.icons,
      pictures,
      sounds,
      contributors,
      iconSets: Promise.all(iconSets.map((a) => this.store.findRecord('icon-set', a))),
    });
  }

  getContaining(feature) {
    if (feature.position.type == 'Point') {
      return feature.position.coordinates.join(',');
    }

    if (feature.position.type == 'LineString') {
      return feature.position.coordinates[0].join(',');
    }

    if (feature.position.type == 'MultiLineString') {
      return feature.position.coordinates[0][0].join(',');
    }

    if (feature.position.type == 'Polygon') {
      return feature.position.coordinates[0][0].join(',');
    }

    if (feature.position.type == 'MultiPolygon') {
      return feature.position.coordinates[0][0][0].join(',');
    }

    return '';
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.setupIconsSubsets();
  }
}
