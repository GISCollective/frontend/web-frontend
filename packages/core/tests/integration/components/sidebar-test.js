/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, waitFor, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | sidebar', function (hooks) {
  setupRenderingTest(hooks);

  it('renders the content', async function () {
    await render(hbs`
      <Sidebar>
        template block text
      </Sidebar>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
  });

  it('triggers onClose when the close button is pressed', async function () {
    let called;

    this.set('close', () => {
      called = true;
    });

    await render(hbs`
      <Sidebar @onClose={{this.close}}>
        template block text
      </Sidebar>
    `);

    await waitUntil(() => !this.element.querySelector('.is-moving'));
    await waitFor('.is-transition-ready');

    await click('.btn-close-panel');

    await waitUntil(() => called);

    expect(called).to.equal(true);
  });
});
