/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ManageDashboardsSpaceController extends Controller {
  @service router;
  @service intl;
  @service store;

  @action
  browsePage(page) {
    this.router.transitionTo('manage.pages.content', page.id);
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }

  @action
  async reload() {
    this.model.pages = await this.store.query('page', { space: this.model.space.id });
  }

  get breadcrumbs() {
    if (!this.model.space) {
      return [];
    }

    return [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'manage.dashboards.dashboard',
        text: this.model.team?.name,
        model: this.model.team?.id,
      },
      {
        text: this.model.space.name,
      },
    ];
  }

  get query() {
    return {
      team: this.model.team.id,
      space: this.model.space.id,
    };
  }

  get pages() {
    let groups = {};

    this.model.pages.forEach((page) => {
      let key = page.category;

      if (!groups[key]) {
        groups[key] = [];
      }

      groups[key].push(page);
    });

    let result = [];

    const knownCategories = ['landing page', 'top-level', 'special', 'other'];

    if (groups['landing page']) {
      result.push({ category: 'landing page', records: groups['landing page'] });
    }

    if (groups['top-level']) {
      result.push({ category: 'top-level', records: groups['top-level'] });
    }

    result = [
      ...result,
      ...Object.entries(groups)
        .filter(([category]) => !knownCategories.includes(category))
        .map(([category, records]) => ({ category, records }))
        .sort((a, b) => `${a.category}`.localeCompare(b.category)),
    ];

    if (groups['special']) {
      result.push({ category: 'special', records: groups['special'] });
    }

    if (groups['other']) {
      result.push({ category: 'other', records: groups['other'] });
    }

    return result;
  }
}
