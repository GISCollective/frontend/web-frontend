/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service, { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { buildWaiter } from '@ember/test-waiters';
const waiter = buildWaiter('loading-service');

export default class LoadingService extends Service {
  @service fastboot;
  @tracked _isLoading;
  @tracked isFirst;
  @tracked count = 0;

  get isLoading() {
    if (this.fastboot.isFastBoot) {
      return true;
    }

    return this.count > 0;
  }

  set isLoading(value) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    let count = this.count + (value ? 1 : -1);

    if (!this.token && count > 0) {
      this.token = waiter.beginAsync();
    }

    if (count <= 0) {
      count = 0;

      if (this.token) {
        waiter.endAsync(this.token);
        this.token = null;
      }
    }

    this.count = count;
  }
}
