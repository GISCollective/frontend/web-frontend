import { tracked } from '@glimmer/tracking';

export default class SocialMediaLinksTransform {
  @tracked facebook = '';
  @tracked youtube = '';
  @tracked xTwitter = '';
  @tracked instagram = '';
  @tracked whatsApp = '';
  @tracked tikTok = '';
  @tracked linkedin = '';

  fields = ['facebook', 'youtube', 'xTwitter', 'instagram', 'whatsApp', 'tikTok', 'linkedin'];

  deserialize(serialized) {
    return new SocialMediaLinks(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.() ?? {};
  }

  static create() {
    return new this();
  }
}

export const SocialMediaList = ['facebook', 'youtube', 'xTwitter', 'instagram', 'whatsApp', 'tikTok', 'linkedin'];

export class SocialMediaLinks {
  @tracked facebook = '';
  @tracked youtube = '';
  @tracked xTwitter = '';
  @tracked instagram = '';
  @tracked whatsApp = '';
  @tracked tikTok = '';
  @tracked linkedin = '';

  constructor(serialized) {
    if (!serialized) {
      return this;
    }

    for (const field of SocialMediaList) {
      if (typeof serialized[field] == 'string') {
        this[field] = serialized[field];
      }
    }

    return this;
  }

  get niceLinks() {
    const value = this.toJSON() ?? {};

    if (value.xTwitter) {
      value['x-twitter'] = value.xTwitter;
      value.xTwitter = '';
    }

    return value;
  }

  toJSON() {
    const result = {};

    for (const field of SocialMediaList) {
      result[field] = this[field];
    }

    return result;
  }
}
