/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputTimezoneComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? 'not set';
  }

  offsetForTimezone(timeZone) {
    try {
      const utcDate = new Date(new Date().toLocaleString('en-US', { timeZone: 'UTC' }));
      const tzDate = new Date(new Date().toLocaleString('en-US', { timeZone }));

      const diff = (tzDate.getTime() - utcDate.getTime()) / 6e4;
      let offset;

      if (diff) {
        const sign = diff > 0 ? '+' : '-';
        offset = `${sign}${parseInt((diff / 60) * 10) / 10}h`;
      }

      return offset;
    } catch (err) {
      return null;
    }
  }

  get selected() {
    const name = this.value;
    const offset = this.offsetForTimezone(name);

    return {
      name,
      offset,
    };
  }

  @action
  search(term) {
    let list = Intl?.supportedValuesOf?.('timeZone') ?? [];

    const lowTerm = term.toLowerCase();

    return list
      .filter((a) => a.toLowerCase().indexOf(lowTerm) != -1)
      .map((name) => ({
        name,
        offset: this.offsetForTimezone(name),
      }));
  }

  @action
  change(value) {
    this._value = value.name;
    return this.args.onChange(value.name);
  }
}
