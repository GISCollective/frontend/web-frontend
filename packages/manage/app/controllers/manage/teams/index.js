/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ManageTeams extends Controller {
  @service intl;
  @service searchStorage;
  @service store;

  @tracked search;
  @tracked _search;
  @tracked isSearching = false;

  queryParams = ['search'];

  setupFilters() {
    this._search = this.search;
  }

  @action
  performSearch(term) {
    this.preserveScrollPosition = true;
    if (term == this.search) {
      return;
    }

    this.searchStorage.add(term);
    this.isSearching = true;
    this.search = term;
  }

  @action
  applySearch(term) {
    console.log('term', term);
    this.search = term;
  }

  @action
  delete(team) {
    return team.destroyRecord();
  }

  @action
  resetBuckets() {
    this.model?.buckets?.reset?.();
  }

  didTransition() {
    this.isSearching = false;
    this.resetBuckets();
  }
}
