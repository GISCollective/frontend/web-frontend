/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Application from 'dummy/app';
import config from 'dummy/config/environment';
import QUnit from 'qunit';
import Pretender from 'pretender';
import { marked } from 'marked';
import sanitizeHtml from 'sanitize-html';
import blurhash from 'blurhash';
import md5 from 'blueimp-md5';
import set from 'lodash/set';
import { Chart } from 'chart.js';
import { setApplication } from '@ember/test-helpers';
import { setup } from 'qunit-dom';
import { start } from 'ember-qunit';

import chai from 'chai';
import chaiString from 'chai-string';
import chaiDom from 'chai-dom';

chai.use(chaiDom);
chai.use(chaiString);
chai.config.truncateThreshold = 0;

import { Settings } from 'luxon';
Settings.defaultLocale = 'en-gb';
Settings.defaultZone = 'CET';

const elementFrom = (target) => {
  if (typeof target === 'object') return target;
  return find(target);
};

chai.Assertion.addMethod('attribute', function (attribute, expectedAttribute) {
  let actualAttribute = elementFrom(this._obj).getAttribute(attribute);

  if (typeof actualAttribute === 'string') {
    actualAttribute = actualAttribute
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a != '')
      .join(' ');
  }

  this.assert(
    actualAttribute === expectedAttribute,
    `expected #{this} to have ${attribute} to #{exp}, but got #{act}`,
    `expected #{this} to not have ${attribute} to #{act}`,
    expectedAttribute,
    actualAttribute,
  );
});

chai.Assertion.addMethod('textContent', function (expectedText) {
  let actualText = elementFrom(this._obj)
    .textContent.split('\n')
    .map((a) => a.trim())
    .filter((a) => a)
    .join(' ');

  this.assert(
    actualText === expectedText,
    `expected #{this} to have text to #{exp}, but got #{act}`,
    `expected #{this} to not have text to #{act}`,
    expectedText,
    actualText,
  );
});

chai.Assertion.addMethod('class', function (expectedAttribute) {
  const classList = elementFrom(this._obj)
    .getAttribute('class')
    .split(' ')
    .map((a) => a.trim())
    .filter((a) => a);

  this.assert(
    classList.find((a) => a == expectedAttribute),
    `expected #{this} to have class to #{exp}, but got #{act}`,
    `expected #{this} to not have class to #{act}`,
    expectedAttribute,
    classList.join(', '),
  );
});

chai.Assertion.addMethod('classes', function (expectedClasses) {
  const classList = elementFrom(this._obj)
    .getAttribute('class')
    .split(' ')
    .map((a) => a.trim())
    .filter((a) => a);

  const expectedClassList = Array.isArray(expectedClasses)
    ? expectedClasses
    : expectedClasses
        .split(' ')
        .map((a) => a.trim())
        .filter((a) => a);

  const unmatched = expectedClassList.filter((a) => !classList.includes(a));

  this.assert(
    unmatched.length === 0,
    `expected #{this} to have classes to #{exp}, but got #{act}`,
    `expected #{this} to not have classes to #{act}`,
    classList,
    expectedClasses,
  );
});

setApplication(Application.create(config.APP));

setup(QUnit.assert);

QUnit.config.failOnZeroTests = false;

start({
  failOnZeroTests: false,
});
