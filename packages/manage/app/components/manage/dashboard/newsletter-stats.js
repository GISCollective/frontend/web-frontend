/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class ManageDashboardNewsletterStatsComponent extends Component {
  @service store;

  @tracked newsletterSubscribers;
  @tracked newsletterEmails;
  @tracked newsletterOpen;
  @tracked newsletterInteractions;

  @action
  async setup() {
    this.newsletterSubscribers = this.store.queryRecord('stat', {
      id: 'newsletter-subscribers',
      newsletter: this.args.newsletter?.id,
    });

    this.newsletterEmails = this.store.queryRecord('stat', {
      id: 'newsletter-emails',
      newsletter: this.args.newsletter?.id,
    });

    this.newsletterOpen = this.store.queryRecord('stat', {
      id: 'newsletter-open',
      newsletter: this.args.newsletter?.id,
    });

    this.newsletterInteractions = this.store.queryRecord('stat', {
      id: 'newsletter-interactions',
      newsletter: this.args.newsletter?.id,
    });
  }
}
