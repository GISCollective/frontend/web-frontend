import Component from '@glimmer/component';
import { service } from '@ember/service';

export default class NiceValue extends Component {
  @service intl;

  get niceValue() {
    if (typeof this.args.value == 'boolean') {
      return this.args.value ? this.intl.t('yes') : this.intl.t('no');
    }

    if (typeof this.args.value == 'string') {
      return this.args.value;
    }

    return '';
  }

  get numericValue() {
    if (typeof this.args.value == 'number') {
      return this.args.value;
    }

    return null;
  }

  get blocks() {
    return Array.isArray(this.args.value?.blocks);
  }
}
