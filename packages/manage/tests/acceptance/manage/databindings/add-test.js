/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, beforeEach, afterEach } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { setupApplicationTest } from 'dummy/tests/helpers';
import { visit, currentURL, click, fillIn, triggerEvent, waitUntil } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import Modal from 'core/test-support/modal';
import PageElements from 'core/test-support/page-elements';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | manage/databindings/add', function (hooks) {
  setupApplicationTest(hooks);
  let server;

  hooks.beforeEach(function () {
    server = new TestServer();
    let space = server.testData.storage.addDefaultSpace();
    space.landingPageId = '';

    server.testData.storage.addDefaultPage();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture('5cc8dc1038e882010061545a');
  });

  hooks.afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit('/manage/databindings/add');
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fdatabindings%2Fadd');
  });

  describe('when an user is authenticated and no team is available', function (hooks) {
    it('can visit the page with an authenticated user as admin', async function () {
      server.testData.storage.addDefaultUser(true);
      authenticateSession();

      await visit('/manage/databindings/add');
      expect(currentURL()).to.equal('/manage/databindings/add');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal('New data binding');
    });

    it('redirects to the first page when there is no team as regular user', async function () {
      server.testData.storage.addDefaultUser(false);
      authenticateSession();

      await visit('/manage/databindings/add');

      expect(currentURL()).to.equal('/manage/databindings/add');

      expect(this.element.querySelector('.alert.alert-no-teams')).to.have.textContent(
        'This feature is available only to a limited number of teams. Contact us if you need access to Data Bindings.',
      );
    });
  });

  describe('when an user is authenticated and a team is available', function (hooks) {
    let team;
    let receivedDataBinding;
    let dataBinding;

    hooks.beforeEach(function () {
      team = server.testData.storage.addDefaultTeam();
      team.allowCustomDataBindings = true;

      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultMap();
      dataBinding = server.testData.storage.addDefaultDataBinding();
    });

    it('redirects to the edit page after the request response is received', async function () {
      server.post('/mock-server/databindings', (request) => {
        receivedDataBinding = JSON.parse(request.requestBody);
        receivedDataBinding.dataBinding._id = dataBinding._id;

        return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedDataBinding)];
      });

      authenticateSession();

      await visit('/manage/databindings/add');

      await fillIn('.row-name input.form-control', 'new data binding name');

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      this.element.querySelector('.select-type').value = 'airtable';
      await triggerEvent('.select-type', 'change');

      await fillIn('.connection-info-_apiKey', 'a');
      await fillIn('.connection-info-baseId', 'b');
      await fillIn('.connection-info-tableName', 'c');
      await fillIn('.connection-info-viewName', 'd');

      await PageElements.wait(1000);
      await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

      await waitUntil(() => receivedDataBinding);

      expect(receivedDataBinding.dataBinding.name).to.equal('new data binding name');
      expect(receivedDataBinding.dataBinding.visibility).to.deep.equal({
        isPublic: false,
        isDefault: false,
        team: '5ca78e2160780601008f69e6',
      });
      expect(receivedDataBinding.dataBinding.connection).to.deep.equal({
        type: 'airtable',
        config: {
          _apiKey: 'a',
          baseId: 'b',
          tableName: 'c',
          viewName: 'd',
        },
      });

      expect(currentURL()).to.equal(`/manage/databindings/edit/${receivedDataBinding.dataBinding._id}`);
    });

    it('renders the breadcrumbs', async function () {
      authenticateSession();

      await visit('/manage/databindings/add');

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'New data binding']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/manage/dashboards']);

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'New data binding']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal([
        '/manage/dashboards',
        '/manage/dashboards/5ca78e2160780601008f69e6',
      ]);
    });

    it('does not show the teams selector when there is not team that allows custom bindings', async function () {
      team.allowCustomDataBindings = false;
      authenticateSession();

      await visit('/manage/databindings/add');

      await fillIn('.row-name input.form-control', 'new data binding name');

      expect(this.element.querySelector('.row-team select')).not.to.exist;
    });

    it('shows an error when the server responds with an error', async function (a) {
      server.post('/mock-server/databindings', (request) => {
        receivedDataBinding = JSON.parse(request.requestBody);
        receivedDataBinding.dataBinding._id = dataBinding._id;

        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                description: 'some message',
                status: 400,
                title: 'Validation error',
              },
            ],
          }),
        ];
      });

      authenticateSession();

      let error;

      const notifications = this.owner.lookup('service:notifications');
      notifications.handleError = (e) => {
        error = e['errors'][0];
      };

      await visit('/manage/databindings/add');

      await fillIn('.row-name input.form-control', 'new data binding name');

      this.element.querySelector('.row-team select').value = team._id;
      await triggerEvent('.row-team select', 'change');

      this.element.querySelector('.select-type').value = 'airtable';
      await triggerEvent('.select-type', 'change');

      await fillIn('.connection-info-_apiKey', 'a');
      await fillIn('.connection-info-baseId', 'b');
      await fillIn('.connection-info-tableName', 'c');
      await fillIn('.connection-info-viewName', 'd');

      await click('.btn-submit');

      a.deepEqual(error, {
        description: 'some message',
        status: 400,
        title: 'Validation error',
      });
    });

    it('queries the teams without the all query param for a regular user', async function () {
      authenticateSession();

      await visit('/manage/databindings/add');

      await PageElements.wait(1000);
      expect(server.history).to.contain('GET /mock-server/teams?allowCustomDataBindings=true&edit=true');
    });

    it('queries the teams with the all query param for an admin user', async function () {
      server.testData.storage.addDefaultUser(true);
      authenticateSession();

      await visit('/manage/databindings/add');
      await click('.btn-show-all-teams');

      await PageElements.wait(1000);
      expect(server.history).to.contain('GET /mock-server/teams?all=true');
    });
  });
});
