/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | questions-validation', function (hooks) {
  setupRenderingTest(hooks);

  it('renders nothing when there is no value', async function (assert) {
    await render(hbs`<QuestionsValidation />`);
    assert.dom(this.element).hasText('');
  });

  it('renders an alert for an item that has a reused name', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        name: 'email',
      },
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        name: 'email',
      },
    ]);

    await render(hbs`<QuestionsValidation @value={{this.value}} @index={{1}} />`);

    expect(this.element.querySelector('.alert-danger')).to.have.textContent(
      'There is already a question with the same name.',
    );
  });

  it('renders nothing for a valid question', async function (assert) {
    this.set('value', [
      {
        question: 'some question',
        help: 'help message',
        type: 'short text',
        name: 'email',
      },
    ]);

    await render(hbs`<QuestionsValidation @value={{this.value}} @index={{0}} />`);

    expect(this.element.querySelector('.alert')).not.to.exist;
  });
});
