/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | layout component',  function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let store;
  let pageData;

  hooks.before(function () {
    server = new TestServer();
    pageData = server.testData.storage.addDefaultPage();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    store = this.owner.lookup('service:store');
  });

  it('renders the cols in the right container', async function () {
    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('.container')).to.exist;
    expect(
      this.element.querySelectorAll('.container .col')[0].textContent.trim()
    ).to.equal('0.0.0');
    expect(
      this.element.querySelectorAll('.container .col')[1].textContent.trim()
    ).to.equal('0.0.1');
  });

  it('applies the layout classes', async function () {
    pageData.layoutContainers[0].options = ['container-fluid', 'pb-5'];
    pageData.layoutContainers[0].rows[0].options = ['cls1'];
    pageData.layoutContainers[0].rows[0].cols[0].options = ['cls2'];

    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('.container-fluid.pb-5')).to.exist;
    expect(this.element.querySelector('.row.cls1')).to.exist;
    expect(this.element.querySelector('.col.cls2')).to.exist;
  });

  it('applies the layout classes', async function () {
    pageData.layoutContainers[0].options = ['container-fluid', 'pb-5'];
    pageData.layoutContainers[0].rows[0].options = ['cls1'];
    pageData.layoutContainers[0].rows[0].cols[0].options = ['cls2'];

    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('.container-fluid.pb-5')).to.exist;
    expect(this.element.querySelector('.row.cls1')).to.exist;
    expect(this.element.querySelector('.col.cls2')).to.exist;
  });

  it('sets the col width from the class', async function () {
    pageData.layoutContainers[0].options = ['container-fluid', 'wpx-30', 'p-0'];
    pageData.layoutContainers[0].rows[0].options = ['wpx-20', 'p-0'];
    pageData.layoutContainers[0].rows[0].cols[0].options = ['wpx-10', 'p-0'];

    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('.col.wpx-10').clientWidth).to.equal(10);
    expect(this.element.querySelector('.row.wpx-20').clientWidth).to.equal(20);
    expect(
      this.element.querySelector('.container-fluid.wpx-30').clientWidth
    ).to.equal(30);
  });

  it('does not show the input-layout section when selectable is not set', async function () {
    let page = await store.findRecord('page', pageData._id);
    this.set('page', page);

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('.input-layout')).not.to.exist;
  });

  it('does shows the input-layout section when selectable is true', async function () {
    let page = await store.findRecord('page', pageData._id);
    this.set('page', page);

    this.set('onConfigContainer', () => { });

    await render(
      hbs`<Layout @page={{this.page}} @onConfigContainer={{this.onConfigContainer}} @selectable={{true}} as |layoutCol|>
  {{layoutCol.name}}
</Layout>`
    );

    expect(this.element.querySelector('.input-layout')).to.exist;
  });

  it('uses the anchor property as id when set', async function () {
    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    page.layoutContainers[0].anchor = "some-anchor";

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('#some-anchor')).to.exist;
  });

  it('uses the dasherized anchor property as id when set to an invalid value', async function () {
    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    page.layoutContainers[0].anchor = "some anchor";

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('#some-anchor')).to.exist;
  });

  it('uses the dasherized anchor property as id when set to an invalid value (has whitespaces)', async function () {
    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    page.layoutContainers[0].anchor = "  some anchor ";

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('#some-anchor')).to.exist;
  });

  it('uses the container number as id when set to a white space', async function () {
    let page = await store.findRecord('page', pageData._id);

    this.set('page', page);

    page.layoutContainers[0].anchor = "  ";

    await render(
      hbs`<Layout @page={{this.page}} as |layoutCol|>{{layoutCol.name}}</Layout>`
    );

    expect(this.element.querySelector('#container-0')).to.exist;
  });

  it('triggers the onConfigContainer event when the container config button is clicked', async function () {
    let page = await store.findRecord('page', pageData._id);
    this.set('page', page);

    let index;
    let container;

    this.set('onConfigContainer', (a, b) => {
      index = a;
      container = b;
    });

    await render(
      hbs`<Layout @page={{this.page}} @onConfigContainer={{this.onConfigContainer}} @selectable={{true}} as |layoutCol|>
  {{layoutCol.name}}
</Layout>`
    );

    await click('.btn-config-container');

    expect(index).to.equal(0);
    expect(container).to.equal(this.page.layoutContainers[0]);
  });
});
