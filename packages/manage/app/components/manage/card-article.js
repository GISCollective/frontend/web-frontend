/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './card';
import { action } from '@ember/object';

export default class ManageCardArticleComponent extends Component {
  get record() {
    return this.args.article;
  }

  get title() {
    return this.args.article?.subject || this.args.article?.title;
  }

  get descriptionTitle() {
    if (!this.args.article?.subject) {
      return null;
    }

    return this.args.article?.title;
  }

  get type() {
    return this.args.article?.type ?? '';
  }

  get slug() {
    if (this.type.includes('newsletter')) {
      return null;
    }

    return this.args.article?.slug;
  }

  get status() {
    return this.args.article?.status ?? '';
  }

  get releaseDate() {
    if (!this.args.article?.releaseDate?.getFullYear?.()) {
      return false;
    }

    if (this.args.article.releaseDate.getFullYear() < 2000) {
      return false;
    }

    return this.args.article.releaseDate;
  }

  get statusColor() {
    if (this.status == 'pending') {
      return 'info';
    }

    if (this.status == 'publishing') {
      return 'info';
    }

    if (this.status == 'sent') {
      return 'success';
    }

    return `secondary`;
  }

  @action
  onClick() {
    return this.args.onClick?.(...arguments);
  }
}
