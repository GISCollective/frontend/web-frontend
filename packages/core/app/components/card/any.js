/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from './cover-title-description';
import { service } from '@ember/service';

export default class CardTitleDescriptionComponent extends Component {
  @service pageCols;

  get cardType() {
    return (
      this.args?.value?.data?.card?.type ?? this.args?.value?.data?.components?.cardType ?? this.pageCols.cardTypes[0]
    );
  }
}
