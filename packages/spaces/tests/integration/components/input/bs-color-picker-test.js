/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { click, render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | input/bs-color-picker',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders the label and color', async function () {
    await render(hbs`<Input::BsColorPicker @label="test" @value="red"/>`);

    expect(this.element.querySelector('.label').textContent.trim()).to.equal(
      'test'
    );

    expect(this.element.querySelector('.bg-red')).to.exist;
    expect(this.element.querySelector('.selector.d-none')).to.exist;
  });

  it('shows the colors on click', async function () {
    await render(hbs`<Input::BsColorPicker @label="test" @value="red"/>`);

    await click('.bs-color-picker');

    expect(this.element.querySelector('.selector.d-none')).not.to.exist;
    expect(this.element.querySelector('.selector')).to.exist;
  });

  it('hides the colors on click outside', async function () {
    await render(hbs`<Input::BsColorPicker @label="test" @value="red"/>`);

    await click('.bs-color-picker');
    await click(this.element.querySelector('.bs-color-picker').parentElement);

    await waitFor('.selector.d-none');
  });

  it('triggers on change when a color is picked', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::BsColorPicker @label="test" @value="red" @onChange={{this.change}}/>`
    );

    await click('.bs-color-picker');
    await click('.btn-color-pink');

    expect(value).to.equal('pink');
  });
});
