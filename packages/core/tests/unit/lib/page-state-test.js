/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';
import { fromPageState, toPageState } from 'dummy/lib/page-state';

describe('Unit | Lib | page-state', function (hooks) {
  setupTest(hooks);

  describe('fromPageState', function () {
    it('returns an empty object for an empty string', function () {
      const state = fromPageState('');

      expect(state).deep.equal({});
    });

    it('returns an empty object when the value is null', function () {
      const state = fromPageState(null);

      expect(state).deep.equal({});
    });

    it('returns an object with a property when the state has a value', function () {
      const state = fromPageState('a@3');

      expect(state).deep.equal({ a: '3' });
    });
  });

  describe('toPageState', function () {
    it('returns an empty string when the value is null', function () {
      const strState = toPageState(null);

      expect(strState).to.equal('');
    });

    it('returns a@b for {a: "b"}', function () {
      const strState = toPageState({ a: 'b' });

      expect(strState).to.equal('a@b');
    });

    it('returns a@b@c@d for {a: "b": c: "d"}', function () {
      const strState = toPageState({ a: 'b', c: 'd' });

      expect(strState).to.equal('a@b@c@d');
    });
  });
});
