/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "./base-options";
import { action } from "@ember/object";
import { guidFor } from "@ember/object/internals";

export default class InputOptionsMatrixValueComponent extends Component {
  elementId = `input-matrix-value-options-${guidFor(this)}`;

  availableValues = ["default", "0", "1", "2", "3", "4", "5"];

  get topValue() {
    return this.getProperty(`${this.args.prefix}t`);
  }

  get bottomValue() {
    return this.getProperty(`${this.args.prefix}b`);
  }

  get startValue() {
    return this.getProperty(`${this.args.prefix}s`);
  }

  get endValue() {
    return this.getProperty(`${this.args.prefix}e`);
  }

  @action
  topValueChange(value) {
    this.changeProperty(`${this.args.prefix}t`, value);
  }

  @action
  bottomValueChange(value) {
    this.changeProperty(`${this.args.prefix}b`, value);
  }

  @action
  startValueChange(value) {
    this.changeProperty(`${this.args.prefix}s`, value);
  }

  @action
  endValueChange(value) {
    this.changeProperty(`${this.args.prefix}e`, value);
  }
}
