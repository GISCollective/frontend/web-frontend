/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | review', function (hooks) {
  setupTest(hooks);

  let transform;

  hooks.beforeEach(function () {
    transform = this.owner.lookup('transform:review');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);

    expect(deserialized.toJSON()).to.deep.equal({});
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized.toJSON()).to.deep.equal({});
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize({
      user: 'some-user',
      reviewedOn: '2020-01-01T00:00:00.000Z',
    });

    expect(deserialized.toJSON()).to.deep.equal({
      user: 'some-user',
      reviewedOn: '2020-01-01T00:00:00.000Z',
    });
  });
});
