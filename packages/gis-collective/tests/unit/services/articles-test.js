/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';

describe ('Unit | Service | articles',  function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let service = this.owner.lookup('service:articles');
    expect(service).to.be.ok;
  });

  it('returns false when is not admin and the article is private', async function () {
    let service = this.owner.lookup('service:articles');
    service.user = {
      isAdmin: false,
    };
    service.space = { currentSpace: { visibility: {} } };
    service.store = {
      queryRecord: async () => {
        return { visibility: { isPublic: false } };
      },
    };

    const result = await service.checkAbout();

    expect(result).to.equal(false);
    expect(service.hasAbout).to.equal(false);
  });

  it('returns true when is admin', async function () {
    let service = this.owner.lookup('service:articles');
    service.fastboot = { isFastBoot: false };
    service.space = { currentSpace: { visibility: {} } };
    service.user = {
      isAdmin: true,
    };
    service.store = {
      queryRecord: async () => {
        return { visibility: { isPublic: false } };
      },
    };

    const result = await service.checkAbout();

    expect(result).to.equal(true);
    expect(service.hasAbout).to.equal(true);
  });

  it('returns true when is admin the article is not found', async function () {
    let service = this.owner.lookup('service:articles');

    service.fastboot = { isFastBoot: false };
    service.space = { currentSpace: { visibility: {} } };
    service.user = {
      isAdmin: true,
    };
    service.store = {
      queryRecord: async () => {
        throw Error();
      },
    };

    const result = await service.checkAbout();

    expect(result).to.equal(false);
    expect(service.hasAbout).to.equal(false);
  });

  it('returns true when the user is not admin and the article is public', async function () {
    let service = this.owner.lookup('service:articles');
    service.user = {
      isAdmin: false,
    };
    service.store = {
      queryRecord: async () => {
        return { visibility: { isPublic: true } };
      },
    };
    service.fastboot = { isFastBoot: false };
    service.space = { currentSpace: { visibility: {} } };

    const result = await service.checkAbout();

    expect(result).to.equal(true);
    expect(service.hasAbout).to.equal(true);
  });
});
