/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { describe, it, setupApplicationTest } from 'dummy/tests/helpers';
import { expect } from 'chai';
import { visit, currentURL, triggerEvent, click, fillIn, waitFor, settled } from '@ember/test-helpers';

import TestServer from 'models/test-support/gis-collective/test-server';
import { authenticateSession, invalidateSession } from 'ember-simple-auth/test-support';
import waitUntil from '@ember/test-helpers/wait-until';
import typeIn from '@ember/test-helpers/dom/type-in';
import { selectSearch, selectChoose } from 'ember-power-select/test-support';
import PageElements from 'core/test-support/page-elements';

describe('Acceptance | manage/pages/content', function (hooks) {
  setupApplicationTest(hooks);

  let server;
  let page;
  let otherPage;
  let space;
  let receivedPage;
  let receivedPages;
  let receivedSpace;
  let restoredPage;
  let receivedPicture;
  let picture;
  let article1;
  let article2;

  hooks.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    picture = server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam('5ca88c99ecd8490100caba50');
    space = server.testData.storage.addDefaultSpace('6227c131624b2cf1626dd029');

    article1 = server.testData.storage.addDefaultArticle();
    article2 = server.testData.storage.addDefaultArticle('000000000000000000000002');
    page = server.testData.storage.addDefaultPage();
    page.space = space._id;

    page.cols[0].name = 'col-1';
    page.cols[1].name = 'col-2';
    page.layoutContainers = [
      {
        options: [],
        name: 'container-1',
        rows: [
          {
            options: [],
            name: 'row-1',
            cols: [
              { type: 'type', data: {}, options: [], name: 'col-1' },
              { type: 'type', data: {}, options: [], name: 'col-2' },
              { type: 'type', data: {}, options: [], name: 'col-3' },
            ],
          },
        ],
      },
    ];

    otherPage = server.testData.storage.addDefaultPage('other');
    otherPage.space = space._id;
    otherPage.name = 'other';
    otherPage.cols = [];
    otherPage.layoutContainers = [
      {
        options: [],
        name: 'other-container-1',
        rows: [
          {
            options: [],
            name: 'other-row-1',
            cols: [
              { type: 'type', data: {}, options: [], name: 'other-1' },
              { type: 'type', data: {}, options: [], name: 'other-2' },
            ],
          },
        ],
      },
    ];

    receivedPage = null;
    receivedPages = [];
    restoredPage = {
      page: {
        ...page,
        name: 'restored test',
        cols: [
          {
            container: 0,
            col: 0,
            row: 0,
            name: 'col-1',
            type: 'title-with-buttons',
            data: {
              title: { text: 'restored title', heading: 1 },
              paragraph: { text: 'restored description' },
              buttons: [],
            },
          },
          {
            container: 0,
            col: 1,
            row: 0,
            name: 'col-2',
            type: 'picture',
            data: { id: '5cc8dc1038e882010061545a' },
          },
          {
            row: -1,
            type: 'background/color',
            col: -1,
            name: '0',
            container: 0,
            data: {
              background: {
                color: 'green-100',
              },
            },
          },
        ],
      },
    };

    server.put(`/mock-server/pages/:id`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = request.params.id;
      receivedPages.push(receivedPage);

      server.testData.storage.addPage(receivedPage.page);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPage)];
    });

    server.put(`/mock-server/spaces/:id`, (request) => {
      receivedSpace = JSON.parse(request.requestBody);
      receivedSpace.space._id = request.params.id ?? space._id;

      server.testData.storage.addPage(receivedSpace.space);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedSpace)];
    });

    server.post(`/mock-server/pages`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = 'new-id';

      server.testData.storage.addPage(receivedPage.page);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPage)];
    });

    server.get(`/mock-server/pages/:id/revisions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          revisions: [
            {
              time: '2022-05-05T00:53:50Z',
              url: '/mock-server/pages/000000000000000000000001/revisions/627304009d2d9b8820ec2f52',
              id: '627304009d2d9b8820ec2f52',
            },
            {
              time: '2022-05-05T00:53:51Z',
              url: '/mock-server/pages/000000000000000000000001/revisions/627304009d2d9b8820ec2f53',
              id: '627304009d2d9b8820ec2f53',
            },
            {
              time: '2022-05-05T00:53:52Z',
              url: '/mock-server/pages/000000000000000000000001/revisions/627304009d2d9b8820ec2f54',
              id: '627304009d2d9b8820ec2f54',
            },
          ],
        }),
      ];
    });

    server.get(`/mock-server/pages/:id/revisions/:revisionId`, () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(restoredPage)];
    });

    receivedPicture = null;
    server.post('/mock-server/pictures', (request) => {
      receivedPicture = JSON.parse(request.requestBody);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({ picture })];
    });

    server.put('/mock-server/pictures/:id', (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = request.params.id;

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify(receivedPicture)];
    });
  });

  hooks.afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/pages/${page._id}/content`);

    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fpages%2F000000000000000000000001%2Fcontent');
  });

  it('can navigate to the page layout page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-show-layout');

    expect(currentURL()).to.equal('/manage/pages/000000000000000000000001/layout');
  });

  it('renders the breadcrumbs', async function () {
    authenticateSession();

    await visit(`/manage/pages/${page._id}/content`);

    expect(PageElements.breadcrumbs()).to.deep.equal(['Dashboard', 'Open Green Map', 'GISCollective', 'test']);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/manage/dashboards',
      '/manage/dashboards/5ca88c99ecd8490100caba50',
      '/manage/dashboards/space/6227c131624b2cf1626dd029',
    ]);
  });

  it('renders the open button', async function () {
    authenticateSession();

    await visit(`/manage/pages/${page._id}/content`);

    expect(this.element.querySelector('.btn-open-page')).to.have.attribute(
      'href',
      'https://test.giscollective.com/page/test',
    );
  });

  it('shows the published visibility for a public page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    expect(this.element.querySelector('.page-visibility').textContent.trim()).to.equal('public');
  });

  it('shows a link to the layout editor when there are no containers', async function () {
    authenticateSession();

    page.layoutContainers = [];

    await visit(`/manage/pages/${page._id}/content`);

    expect(this.element.querySelector('.empty-layout p').textContent.trim()).to.equal(
      'The layout has no containers. You need to edit the layout before editing the content.',
    );

    await click('.empty-layout .btn-show-layout');

    expect(currentURL()).to.equal('/manage/pages/000000000000000000000001/layout');
  });

  it('shows the private visibility for a private page', async function () {
    page.visibility.isPublic = false;
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    expect(this.element.querySelector('.page-visibility').textContent.trim()).to.equal('private');
  });

  it('clears the selection when the page is reopened', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.col');
    await click('.btn-show-layout');
    await click('.btn-show-content');

    expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('page');
  });

  it('shows a message inside the empty cols', async function () {
    authenticateSession();
    page.cols = [];
    await visit(`/manage/pages/${page._id}/content`);

    expect(this.element.querySelector('.page-col.empty').textContent.trim()).to.equal('Empty column');
  });

  it('allows changing the column type using a highlight selection', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    const element = this.element.querySelector('[data-editor=slot]');
    await triggerEvent(element, 'mouseover');

    let titles = this.element.querySelectorAll('.highlight-title');
    await click(titles[3]);

    await selectSearch('.ember-power-select-trigger', 'title with buttons');
    await selectChoose('.ember-power-select-trigger', 'title with buttons');

    expect(this.element.querySelector('.title-with-buttons')).to.exist;

    await click('.page-editors-container');
    await triggerEvent('.page-col .title-with-buttons', 'mouseover');
    await click('.title-col-1-title');
    await typeIn('.input-text-value', 'title');

    await click('.page-editors-container');
    await triggerEvent('.page-col .title-with-buttons', 'mouseover');
    await click('.title-col-1-text');
    await typeIn('.input-text-value', 'description');

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.cols).to.deep.equal([
      {
        container: +0,
        col: +0,
        row: +0,
        name: 'col-1',
        type: 'title-with-buttons',
        data: {
          source: { id: '5ca78e2160780601008f69e6', model: 'article' },
          title: { color: '', classes: [], minLines: { sm: +0, md: +0, lg: +0 }, text: 'title' },
          paragraph: { color: '', classes: [], minLines: { sm: +0, md: +0, lg: +0 }, text: 'description' },
        },
      },
      {
        container: +0,
        col: 1,
        row: +0,
        name: 'col-2',
        type: 'picture',
        data: {
          source: {
            model: 'picture',
            id: '5cc8dc1038e882010061545a',
          },
        },
      },
    ]);
  });

  it('allows changing the page name', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.page-editors-container');
    expect(this.element.querySelector('.page-editors-properties .page-name').value).to.equal('test');

    await fillIn('.page-editors-properties .page-name', 'new name');

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.name).to.equal('new name');
  });

  it('allows changing the page slug', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.page-editors-container');
    expect(this.element.querySelector('.page-path').value).to.equal('/page/test');
    await fillIn('.page-path', 'a//b//c/d');

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.slug).to.equal('a--b--c--d');
  });

  it('allows changing the visibility', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.page-editors-container');

    expect(this.element.querySelector('.chk-visibility input').checked).to.equal(true);

    await click('.chk-visibility');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.visibility).to.deep.equal({
      isPublic: true,
      isDefault: false,
      team: '5ca88c99ecd8490100caba50',
    });
  });

  it('can create a copy of the page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-copy-page');

    await waitUntil(() => receivedPage);

    expect(currentURL()).to.equal('/manage/pages/new-id/content');

    expect(receivedPage.page._id).to.equal('new-id');
    expect(receivedPage.page.name).to.equal('test - copy');
    expect(receivedPage.page.slug).to.equal('page--test-copy');
    expect(receivedPage.page.visibility).to.deep.equal({
      isPublic: false,
      isDefault: false,
      team: '5ca88c99ecd8490100caba50',
    });
  });

  it('can be moved to another space', async function () {
    const space1 = server.testData.storage.addDefaultSpace('000000000000000000000005');
    const space2 = server.testData.storage.addDefaultSpace('000000000000000000000006');

    space1.name = 'space 1';
    space2.name = 'space 2';

    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    expect(this.element.querySelector('.input-space-id').value).to.equal('6227c131624b2cf1626dd029');

    this.element.querySelector('.input-space-id').value = '000000000000000000000005';
    await triggerEvent('.input-space-id', 'change');

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.space).to.equal('000000000000000000000005');
  });

  it('can update the page categories', async function () {
    page.categories = ['category 1', 'category 2'];

    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    const inputs = [...this.element.querySelectorAll('.page-categories .form-control')];

    expect(inputs).to.have.length(2);
    expect(inputs[0].value).to.equal('category 1');
    expect(inputs[1].value).to.equal('category 2');

    await fillIn(inputs[0], 'new category 1');
    await fillIn(inputs[1], 'new category 2');

    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.categories).to.deep.equal(['new category 1', 'new category 2']);
  });

  describe('the container', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();
      await visit(`/manage/pages/${page._id}/content`);
    });

    it('can set the background color component', async function () {
      await click('.btn-config-container-0');

      await selectSearch('.ember-power-select-trigger', 'background color');
      await selectChoose('.ember-power-select-trigger', 'background color');

      await click('.btn-color-primary');
      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'article',
          data: {
            source: { id: '5ca78e2160780601008f69e6', model: 'article' },
          },
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'picture',
          data: {
            source: {
              model: 'picture',
              id: '5cc8dc1038e882010061545a',
            },
          },
        },
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0',
          type: 'background/color',
          data: { background: { color: 'primary' } },
        },
      ]);
    });

    it('can set the background image component with custom md sizes', async function () {
      await click('.btn-config-container-0');

      await selectSearch('.ember-power-select-trigger', 'background image');
      await selectChoose('.ember-power-select-trigger', 'background image');

      this.element.querySelector('.input-sm-bg-size').value = 'fixed';
      await triggerEvent('.input-sm-bg-size', 'change');

      await fillIn('.responsive-value-md .input-width-group .input-dimension-numeric', '100');
      await fillIn('.responsive-value-md .input-height-group .input-dimension-numeric', '200');

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'article',
          data: {
            source: { id: '5ca78e2160780601008f69e6', model: 'article' },
          },
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'picture',
          data: {
            source: {
              model: 'picture',
              id: '5cc8dc1038e882010061545a',
            },
          },
        },
        {
          container: +0,
          col: +0,
          row: +0,
          name: '0',
          type: 'background/image',
          data: {
            options: ['bg-size-fixed'],
            style: {
              md: {
                'background-size': [
                  { value: 100, unit: 'px' },
                  { value: 200, unit: 'px' },
                ],
              },
            },
            source: { model: 'picture' },
          },
        },
      ]);
    });

    it('changes the paddings in realtime using the container layout editor', async function () {
      const element = this.element.querySelector('.page-col');
      await triggerEvent(element, 'mouseover');

      let titles = this.element.querySelectorAll('.highlight-title');
      await click(titles[0]);

      this.element.querySelector('.sm-top-padding').value = '1';
      await triggerEvent('.sm-top-padding', 'change');

      this.element.querySelector('.sm-bottom-padding').value = '2';
      await triggerEvent('.sm-bottom-padding', 'change');

      this.element.querySelector('.sm-start-padding').value = '3';
      await triggerEvent('.sm-start-padding', 'change');

      this.element.querySelector('.sm-end-padding').value = '4';
      await triggerEvent('.sm-end-padding', 'change');

      expect(this.element.querySelector('[data-path="page.layoutContainers.0"]')).to.have.classes(
        'pt-1 pb-2 ps-3 pe-4',
      );

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.layoutContainers[0].options).to.deep.equal(['pt-1', 'pb-2', 'ps-3', 'pe-4']);
    });

    it('changes the properties in realtime using the row layout editor', async function () {
      const element = this.element.querySelector('.page-col');
      await triggerEvent(element, 'mouseover');

      let titles = this.element.querySelectorAll('.highlight-title');
      await click(titles[1]);

      this.element.querySelector('.input-sm-border-visibility').value = 'yes';
      await triggerEvent('.input-sm-border-visibility', 'change');

      expect(this.element.querySelector('[data-path="page.layoutContainers.0.rows.0.data.container"]')).to.have.classes(
        'border-visibility-yes',
      );

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.layoutContainers[0].rows[0].data.container).to.deep.equal({
        classes: ['border-visibility-yes'],
      });
    });

    it('changes the properties in realtime using the col layout editor', async function () {
      const element = this.element.querySelector('.page-col');
      await triggerEvent(element, 'mouseover');

      let titles = this.element.querySelectorAll('.highlight-title');
      await click(titles[2]);

      this.element.querySelector('.input-sm-border-visibility').value = 'yes';
      await triggerEvent('.input-sm-border-visibility', 'change');

      expect(
        this.element.querySelector('[data-path="page.layoutContainers.0.rows.0.cols.0.data.container"]'),
      ).to.have.classes('border-visibility-yes');

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.layoutContainers[0].rows[0].cols[0].data.container).to.deep.equal({
        classes: ['border-visibility-yes'],
      });
    });
  });

  describe('with a :feature-id variable slug', function (hooks) {
    hooks.beforeEach(function () {
      authenticateSession();

      server.testData.storage.addDefaultFeature();
      server.testData.storage.addDefaultMap();

      page.slug = 'features--:feature-id';
      page.cols = [
        {
          container: 0,
          col: 0,
          row: 0,
          name: 'col-1',
          type: 'article',
          data: { useSelectedModel: true },
        },
      ];
    });

    it('shows the selected id on the page properties', async function () {
      await visit(`/manage/pages/${page._id}/content`);

      expect(this.element.querySelector('.input-model-id').value).to.equal('5ca78e2160780601008f69e6');
    });

    it('can show the map list associated with the feature', async function () {
      await visit(`/manage/pages/${page._id}/content`);

      await triggerEvent('.source-record', 'mouseover');
      await click('.title-col-1-slot');

      await selectSearch('.ember-power-select-trigger', 'card list');
      await selectChoose('.ember-power-select-trigger', 'card list');

      await click('.page-editors-container');

      await triggerEvent('.card-list', 'mouseover');
      await click('.title-col-1-data-source');

      this.element.querySelector('.model-name').value = 'selected-model';
      await triggerEvent('.model-name', 'change');

      this.element.querySelector('.model-property').value = 'maps';
      await triggerEvent('.model-property', 'change');

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.cols[0]).to.deep.equal({
        container: +0,
        col: +0,
        row: +0,
        name: 'col-1',
        type: 'card-list',
        data: { source: { useSelectedModel: true, property: 'maps' } },
      });
    });
  });

  describe('with a :campaign-id variable slug', function (hooks) {
    let otherCampaign;
    let otherCampaign2;

    hooks.beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultCampaign();
      otherCampaign = server.testData.storage.addDefaultCampaign('other');
      otherCampaign.name = 'Mapping Berlin Soundscapes';
      otherCampaign.article = {
        blocks: [
          {
            type: 'header',
            data: { level: 1, text: 'Mapping Berlin Soundscapes' },
          },
          {
            type: 'paragraph',
            data: {
              text: "We'd like to collect urban nature sounds that characterize Berlin's natural life.",
            },
          },
        ],
      };

      otherCampaign2 = JSON.parse(JSON.stringify(otherCampaign));
      otherCampaign2._id = 'campaign3';
      otherCampaign2.name = 'Campaign 3';
      otherCampaign2.article = {
        blocks: [
          {
            type: 'header',
            data: { level: 1, text: 'Campaign 3' },
          },
          {
            type: 'paragraph',
            data: {
              text: "We'd like to collect urban nature sounds that characterize Berlin's natural life.",
            },
          },
        ],
      };

      server.testData.storage.addCampaign(otherCampaign2);

      page.slug = 'campaigns--:campaign-id';
      page.cols = [
        {
          container: 0,
          col: 0,
          row: 0,
          name: 'col-1',
          type: 'article',
          data: { useSelectedModel: true },
        },
      ];
    });

    it('shows the selected id on the page properties', async function () {
      await visit(`/manage/pages/${page._id}/content`);

      expect(this.element.querySelector('.input-model-id').value).to.equal('5ca78aa160780601008f6aaa');
    });

    it('renders a model when the id is not set', async function () {
      await visit(`/manage/pages/${page._id}/content`);

      expect(this.element.querySelector('.article h1').textContent.trim()).to.equal('Campaign 1');
      expect(this.element.querySelector('.article p').textContent.trim()).to.equal(
        "We'd like to collect urban nature sounds that characterize Berlin's natural life. Please help us find interesting places for sound recording! Here are the places and sounds we collected so far on the Berlin Sound Map.",
      );
    });

    it('renders the model with the selected id', async function () {
      await visit(`/manage/pages/${page._id}/content?selectedId=${otherCampaign._id}`);

      expect(this.element.querySelector('.input-model-id').value).to.equal('other');
      expect(this.element.querySelector('.article h1').textContent.trim()).to.equal('Mapping Berlin Soundscapes');
      expect(this.element.querySelector('.article p').textContent.trim()).to.equal(
        "We'd like to collect urban nature sounds that characterize Berlin's natural life.",
      );
    });

    it('renders a new model when the model id is changed', async function () {
      await visit(`/manage/pages/${page._id}/content?selectedId=${otherCampaign._id}`);

      await fillIn('.input-model-id', '');
      await fillIn('.input-model-id', 'campaign3');

      expect(this.element.querySelector('.input-model-id').value).to.equal('campaign3');

      expect(this.element.querySelector('.article h1').textContent.trim()).to.equal('Campaign 3');
      expect(this.element.querySelector('.article p').textContent.trim()).to.equal(
        "We'd like to collect urban nature sounds that characterize Berlin's natural life.",
      );
    });
  });

  describe('the title with buttons component', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();
      await visit(`/manage/pages/${page._id}/content`);

      const columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await selectSearch('.ember-power-select-trigger', 'title with buttons');
      await selectChoose('.ember-power-select-trigger', 'title with buttons');

      await click('.page-editors-container');
      await triggerEvent('.page-col .title-with-buttons', 'mouseover');
      await click('.title-col-1-title');
    });

    it('can be added on the page', async function (a) {
      await typeIn('.input-text-value', 'title');

      await click('.page-editors-container');
      await triggerEvent('.page-col .title-with-buttons', 'mouseover');
      await click('.title-col-1-text');

      await typeIn('.input-text-value', 'paragraph');

      await click('.page-editors-container');
      await triggerEvent('.page-col .title-with-buttons', 'mouseover');

      await click('.title-col-1-button-list-text');

      await click('.btn-add-button');
      await fillIn('input.input-button-name', 'name');

      await selectSearch('.ember-power-select-trigger', 'https://giscollective.com');
      await click('.btn-save');

      await waitUntil(() => receivedPage);

      a.deepEqual(receivedPage.page.cols[0], {
        container: +0,
        col: +0,
        row: +0,
        name: 'col-1',
        type: 'title-with-buttons',
        data: {
          source: { id: '5ca78e2160780601008f69e6', model: 'article' },
          title: { color: '', classes: [], minLines: { sm: +0, md: +0, lg: +0 }, text: 'title' },
          paragraph: { color: '', classes: [], minLines: { sm: +0, md: +0, lg: +0 }, text: 'paragraph' },
          buttons: [
            {
              name: 'name',
              link: { url: 'https://giscollective.com' },
              newTab: false,
              type: 'default',
              storeType: 'apple',
              classes: ['btn-primary'],
            },
          ],
        },
      });
    });

    it('can set properties for the tablet mode', async function () {
      await typeIn('.input-text-value', 'title');

      await click('.btn-tablet');

      this.element.querySelector('.input-md-text-size').value = '15';
      await triggerEvent('.input-md-text-size', 'change');

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.cols[0]).to.deep.equal({
        container: +0,
        col: +0,
        row: +0,
        name: 'col-1',
        type: 'title-with-buttons',
        data: {
          source: { id: '5ca78e2160780601008f69e6', model: 'article' },
          title: {
            color: '',
            classes: ['text-size-md-15'],
            minLines: { sm: +0, md: +0, lg: +0 },
            text: 'title',
          },
        },
      });
    });
  });

  describe('a page with two articles', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      page.cols = [
        {
          name: 'col-1',
          type: 'article',
          data: { id: '5ca78e2160780601008f69e6' },
        },
        {
          name: 'col-2',
          type: 'article',
          data: { id: '000000000000000000000002' },
        },
      ];

      await visit(`/manage/pages/${page._id}/content`);
    });

    it('selects the page by default', async function () {
      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('page');
    });

    it('allows switching between all editors', async function () {
      await click('.btn-config-container-0');
      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('container 0');

      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-col');

      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('col-1 (col)');

      await click('.page-editors-container');
      columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[1], 'mouseover');

      await click('.title-col-2-col');

      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('col-2 (col)');

      await click('.page-editors-container');
      expect(this.element.querySelector('.page-editors-properties .title').textContent.trim()).to.equal('page');
    });

    it('allows removing a component', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await click('.component-selector .btn-none');

      await click('.btn-save');

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-2',
          type: 'article',
          data: { id: '000000000000000000000002' },
        },
      ]);
    });

    it('can set a title', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');

      await click('.title-col-1-slot');

      await selectSearch('.ember-power-select-trigger', 'article title');
      await selectChoose('.ember-power-select-trigger', 'article title');

      await click('.page-editors-container');

      const element = this.element.querySelector('.source-record');
      await triggerEvent(element, 'mouseover');
      await click('.title-col-1-data-source');

      this.element.querySelector('.model-name').value = 'article';
      await triggerEvent('.model-name', 'change');

      await selectSearch('.model-id', 'test');
      await selectChoose('.model-id', 'title');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal('title');
    });
  });

  describe('a page with two blocks', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      page.cols = [
        {
          name: 'col-1',
          data: {
            typography: {
              lineHeight: 'base',
              alignment: 'start',
              font: 'default',
            },
            container: {
              color: 'green-100',
            },
          },
          type: 'blocks',
          container: 0,
        },
        {
          name: 'col-2',
          data: {
            typography: {
              lineHeight: 'base',
              alignment: 'start',
              font: 'default',
            },
            container: {},
          },
          type: 'blocks',
          container: 0,
        },
      ];

      await visit(`/manage/pages/${page._id}/content`);
    });

    it('allows changing the container properties', async function (a) {
      await click('.btn-config-container-0');

      let element = this.element.querySelector('[data-editor=blocks]');
      await triggerEvent(element, 'mouseover');

      await click('.title-col-1-container');

      await waitFor('.input-sm-border-visibility');
      this.element.querySelector('.input-sm-border-visibility').value = 'yes';
      await triggerEvent('.input-sm-border-visibility', 'change');

      await click('.page-editors-container');
      element = this.element.querySelector('[data-editor=blocks]');
      await triggerEvent(element, 'mouseover');

      await click('.title-col-1-blocks');

      await PageElements.waitEditorJs(this.element);
      await click('.add-paragraph-link');

      await PageElements.wait(1000);

      await click('.page-editors-container');

      element = this.element.querySelector('[data-type=paragraph]');
      await triggerEvent(element, 'mouseover');

      await click('.title-col-1-paragraph');

      await click('.bs-color-picker');
      await click('.btn-color-indigo');
      await click('.btn-save');

      await waitUntil(() => receivedPage);

      a.deepEqual(receivedPage.page.cols, [
        {
          container: 0,
          col: 0,
          row: 0,
          name: 'col-1',
          type: 'blocks',
          data: {
            blocks: [{ type: 'paragraph', data: { text: 'New paragraph' } }],
            paragraphStyle: {
              color: 'indigo',
              classes: [],
              minLines: { sm: 0, md: 0, lg: 0 },
            },
            typography: {
              alignment: 'start',
              font: 'default',
              lineHeight: 'base',
            },
            container: {
              classes: ['border-visibility-yes'],
              color: 'green-100',
            },
          },
        },
        {
          container: 0,
          col: 0,
          row: 0,
          name: 'col-2',
          type: 'blocks',
          data: {
            typography: { lineHeight: 'base', alignment: 'start', font: 'default' },
            container: {},
          },
        },
      ]);
    });

    it('can set a fragment and edit the content', async function (a) {
      let store = this.owner.lookup('service:store');
      const spaceModel = await store.findRecord('space', space._id);
      spaceModel._getPagesMap = {
        page1: page._id,
        page2: otherPage._id,
      };

      // hover the first slot
      let element = this.element.querySelector('.page-col');
      await triggerEvent(element, 'mouseover');

      // select a fragment
      await click('.title-col-1-slot');
      await selectSearch('.ember-power-select-trigger', 'fragment');
      await selectChoose('.ember-power-select-trigger', 'fragment');

      // select the fragment data source
      await click('.page-editors-container');
      element = this.element.querySelector('.view-fragment');
      await triggerEvent(element, 'mouseover');
      await click('.title-col-1-data-source');

      // select the other page in the fragment
      await selectSearch('.ember-power-select-trigger', 'other');
      await selectChoose('.ember-power-select-trigger', 'other');

      // select the second slot in the fragment
      await click('.page-editors-container');
      element = this.element.querySelectorAll('.view-fragment [data-editor=slot]');
      await triggerEvent(element[1], 'mouseover');

      // select a heading in the second slot
      await click('.title-other-2-slot');
      await selectSearch('.ember-power-select-trigger', 'heading');
      await selectChoose('.ember-power-select-trigger', 'heading');

      await click('.page-editors-container');

      // select the heading
      element = this.element.querySelectorAll('.view-fragment h1');
      await triggerEvent(element[0], 'mouseover');
      await click('.title-other-2-text');

      // set a new header value
      fillIn('.manage-editors-properties-text .input-text', 'some value');
      await click('.page-editors-container');

      await click('.btn-save');

      expect(receivedPages).to.have.length(2);
      a.deepEqual(receivedPages[0].page._id, 'other');
      a.deepEqual(receivedPages[1].page._id, '000000000000000000000001');

      a.deepEqual(receivedPages[0].page.cols, [
        {
          col: 0,
          container: 0,
          data: {
            style: {
              classes: [],
              color: '',
              heading: '1',
              minLines: {
                lg: 0,
                md: 0,
                sm: 0,
              },
              text: 'some value',
            },
          },
          name: 'other-2',
          row: 0,
          type: 'heading',
        },
      ]);

      a.deepEqual(receivedPages[1].page.cols, [
        {
          col: 0,
          container: 0,
          data: {
            container: {
              color: 'green-100',
            },
            source: {
              id: 'other',
              model: 'page',
              useDefaultModel: false,
              useSelectedModel: false,
            },
            typography: {
              alignment: 'start',
              font: 'default',
              lineHeight: 'base',
            },
          },
          name: 'col-1',
          row: 0,
          type: 'fragment',
        },
        {
          col: 0,
          container: 0,
          data: {
            container: {},
            typography: {
              alignment: 'start',
              font: 'default',
              lineHeight: 'base',
            },
          },
          name: 'col-2',
          row: 0,
          type: 'blocks',
        },
      ]);
    });
  });

  describe('a page with two title with buttons components and a background color component', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      page.cols = [
        {
          container: 0,
          col: 0,
          row: 0,
          name: 'col-1',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: 0,
          col: 1,
          row: 0,
          name: 'col-2',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: 0,
          col: -1,
          row: -1,
          name: '0',
          type: 'background/color',
          data: {
            background: {
              color: 'primary',
            },
          },
        },
      ];

      await visit(`/manage/pages/${page._id}/content`);
    });

    it('does not reset partial changes while switching to a new col', async function () {
      await click('.page-editors-container');
      await triggerEvent('.page-col-col-1 .title-with-buttons h1', 'mouseover');
      await click('.title-col-1-title');

      await fillIn('.input-text-value', 'title 1');

      await click('.page-editors-container');
      await triggerEvent('.page-col-col-2 .title-with-buttons h1', 'mouseover');
      await click('.title-col-2-title');

      await fillIn('.input-text-value', 'title 2');
      await click('.btn-color-danger');

      await click('.page-editors-container');
      await triggerEvent('.page-col-col-1 .title-with-buttons h1', 'mouseover');
      await click('.title-col-1-title');

      expect(this.element.querySelector('.selected-value.bg-danger')).not.to.exist;
      expect(this.element.querySelector('.input-text-value').value).to.equal('title 1');

      await click('.page-editors-container');
      await triggerEvent('.page-col-col-2 .title-with-buttons h1', 'mouseover');
      await click('.title-col-2-title');

      expect(this.element.querySelector('.selected-value.bg-danger')).to.exist;
      expect(this.element.querySelector('.input-text-value').value).to.equal('title 2');
    });

    it('allows changing the background color', async function () {
      await click('.btn-config-container-0');

      await click('.btn-color-blue');
      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: +0,
          col: -1,
          row: -1,
          name: '0',
          type: 'background/color',
          data: { background: { color: 'blue' } },
        },
      ]);
    });

    it('allows setting a component as global', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await click('.btn-to-global-page-col');
      await fillIn('.input-global-page-col-id', 'main-title');
      await click('.btn-submit');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedSpace.space.cols).to.deep.equal({
        'main-title': {
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
      });

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
          gid: 'main-title',
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: +0,
          col: -1,
          row: -1,
          name: '0',
          type: 'background/color',
          data: { background: { color: 'primary' } },
        },
      ]);
    });

    it('allows setting a component as global using the the highlight', async function () {
      const element = this.element.querySelector('[data-editor=slot]');
      await triggerEvent(element, 'mouseover');

      let titles = this.element.querySelectorAll('.highlight-title');
      await click(titles[4]);

      await click('.btn-to-global-page-col');
      await fillIn('.input-global-page-col-id', 'main-title');
      await click('.btn-submit');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedSpace.space.cols).to.deep.equal({
        'main-title': {
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
      });

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
          gid: 'main-title',
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: +0,
          col: -1,
          row: -1,
          name: '0',
          type: 'background/color',
          data: { background: { color: 'primary' } },
        },
      ]);
    });

    it('allows unlinking a global component', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await click('.btn-to-global-page-col');
      await fillIn('.input-global-page-col-id', 'main-title');
      await click('.btn-submit');

      await click('.btn-remove-gid');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: +0,
          col: -1,
          row: -1,
          name: '0',
          type: 'background/color',
          data: {
            background: { color: 'primary' },
          },
        },
      ]);
    });

    it('shows an error when reusing a gid', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await click('.btn-to-global-page-col');
      await fillIn('.input-global-page-col-id', 'main-title');
      await click('.btn-submit');

      await click('.page-editors-container');

      columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[1], 'mouseover');
      await click('.title-col-2-slot');

      await click('.btn-to-global-page-col');
      await fillIn('.input-global-page-col-id', 'main-title');

      expect(
        this.element.querySelector('.input-manage-to-global-page-col .invalid-feedback').textContent.trim(),
      ).to.equal('the id is already used by another component');
    });

    it('can set the global id to the second col', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await click('.btn-to-global-page-col');
      await fillIn('.input-global-page-col-id', 'main-title');
      await click('.btn-submit');

      await click('.page-editors-container');

      columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[1], 'mouseover');
      await click('.title-col-2-slot');

      await selectSearch('.ember-power-select-trigger', 'main title');
      await selectChoose('.ember-power-select-trigger', 'main title');

      await click('.btn-save');

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
          gid: 'main-title',
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
          gid: 'main-title',
        },
        {
          container: +0,
          col: -1,
          row: -1,
          name: '0',
          type: 'background/color',
          data: { background: { color: 'primary' } },
        },
      ]);
    });

    it('can add a column after one was removed', async function () {
      let element = this.element.querySelector('[data-editor=slot]');
      await triggerEvent(element, 'mouseover');

      let titles = this.element.querySelectorAll('.title-col-1-slot');
      await click(titles[0]);

      expect(this.element.querySelector('.text-slot-name').value).to.equal('col-1');

      await click('.btn-none');

      expect(this.element.querySelector('.text-slot-name').value).to.equal('col-1');

      await selectSearch('.ember-power-select-trigger', 'heading');
      await selectChoose('.ember-power-select-trigger', 'heading');

      await click('.page-editors-container');

      await waitFor("h1[data-name='col-1']");

      element = this.element.querySelector("h1[data-name='col-1']");
      await triggerEvent(element, 'mouseover');

      await click('.title-col-1-text');

      await waitFor('.input-text-value');

      await fillIn('.input-text-value', 'some text');
      await click('.btn-save');

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'heading',
          data: {
            style: { color: '', classes: [], minLines: { sm: +0, md: +0, lg: +0 }, text: 'some text', heading: '1' },
          },
        },
        {
          container: +0,
          col: 1,
          row: +0,
          name: 'col-2',
          type: 'title-with-buttons',
          data: {
            title: { text: 'title', options: ['display-md-5'], heading: 1 },
            paragraph: { text: 'paragraph' },
            buttons: [],
          },
        },
        {
          container: +0,
          col: -1,
          row: -1,
          name: '0',
          type: 'background/color',
          data: {
            background: { color: 'primary' },
          },
        },
      ]);
    });
  });

  describe('a page with 3 empty cols', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      page.cols = [];

      await visit(`/manage/pages/${page._id}/content`);
    });

    it('can add a menu component', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await selectSearch('.ember-power-select-trigger', 'menu');
      await selectChoose('.ember-power-select-trigger', 'menu');

      await click('.page-editors-container');
      await triggerEvent('.page-col-menu', 'mouseover');
      await click('.title-col-1-menu');

      await click('.input-menu-show-logo');

      this.element.querySelector('.input-menu-align').value = 'center';
      await triggerEvent('.input-menu-align', 'change');

      await click('.btn-add-menu-item');

      await fillIn('.link-name', 'some name');

      await selectSearch('.manage-editors-properties-link .ember-power-select-trigger', 'http://giscollective.com');
      await selectChoose('.manage-editors-properties-link .ember-power-select-trigger', 'http://giscollective.com');

      await click('.btn-save');

      expect(receivedPage.page.cols[0].data).to.deep.equal({
        showLogo: true,
        logo: { style: {} },
        align: 'center',
        menu: { items: [{ name: 'some name', link: { url: 'http://giscollective.com' }, dropDown: [] }] },
      });
    });

    it('can add a title with buttons component', async function () {
      let columns = this.element.querySelectorAll('.page-col');
      await triggerEvent(columns[0], 'mouseover');
      await click('.title-col-1-slot');

      await selectSearch('.ember-power-select-trigger', 'title with buttons');
      await selectChoose('.ember-power-select-trigger', 'title with buttons');

      await click('.page-editors-container');
      await triggerEvent('.page-col .title-with-buttons', 'mouseover');
      await click('.title-col-1-title');

      await fillIn('.input-text-value', 'new title');

      await click('.page-editors-container');
      await triggerEvent('.page-col .title-with-buttons', 'mouseover');
      await click('.title-col-1-text');

      await fillIn('.input-text-value', 'some text');

      await click('.page-editors-container');
      await triggerEvent('.page-col .title-with-buttons', 'mouseover');
      await click('.title-col-1-button-list-text');

      await click('.btn-add-button');

      expect(this.element.querySelector('.page-col-col-1 h1').textContent.trim()).to.equal('new title');
      expect(this.element.querySelector('.page-col-col-1 p').textContent.trim()).to.equal('some text');
      expect(this.element.querySelectorAll('.col .button-list .btn')).to.have.length(1);

      await click('.page-editors-container');
      await triggerEvent('.col .button-list .btn', 'mouseover');
      await click('.title-col-1-button-1-button');

      await typeIn('.input-button-name', 'button name');
      await selectSearch('.manage-editors-properties-link .ember-power-select-trigger', 'https://giscollective.com');

      expect(this.element.querySelector('.page-col-col-1 .btn').textContent.trim()).to.equal('button name');
    });

    it('can add a picture component and upload a new picture and set a link', async function () {
      let element = this.element.querySelector('[data-editor=slot]');
      await triggerEvent(element, 'mouseover');

      await click('.title-col-1-slot');

      await selectSearch('.ember-power-select-trigger', 'picture');
      await selectChoose('.ember-power-select-trigger', 'picture');

      await click('.page-editors-container');
      await triggerEvent("[data-editor='source-record']", 'mouseover');
      await click('.title-col-1-data-source');

      this.element.querySelector('.model-name').value = 'picture';
      await triggerEvent('.model-name', 'change');

      const blob = server.testData.create.pngBlob();
      await triggerEvent("input[type='file']", 'change', { files: [blob] });

      await waitUntil(() => receivedPicture);
      await waitFor('.page-col-col-1 img');

      expect(this.element.querySelector('.page-col-col-1 img').attributes.getNamedItem('src').value).to.startWith(
        picture.picture,
      );

      await click('.page-editors-container');
      await triggerEvent('.page-col-col-1 img', 'mouseover');
      await click('.title-col-1-link');

      await selectSearch('.ember-power-select-trigger', 'https://giscollective.com');

      await PageElements.wait(500);

      await click('.btn-save');

      await waitUntil(() => receivedPage);

      expect(receivedPage.page.cols).to.deep.equal([
        {
          container: +0,
          col: +0,
          row: +0,
          name: 'col-1',
          type: 'picture',
          data: {
            source: {
              useSelectedModel: false,
              useDefaultModel: false,
              model: 'picture',
              id: '5cc8dc1038e882010061545a',
            },
            destination: { newTab: false, link: { url: 'https://giscollective.com' } },
          },
        },
      ]);
    });
  });

  describe('the page revisions', function (hooks) {
    hooks.beforeEach(async function () {
      authenticateSession();

      await visit(`/manage/pages/${page._id}/content`);
    });

    it('are rendered in the page settings', async function () {
      await click('.page-editors-container');

      expect(this.element.querySelector('.page-editors-properties .page-revisions')).to.exist;
    });

    it('restores an older content version when a button is pressed', async function () {
      await click('.page-editors-container');
      await click('.page-revisions .revision-item');

      await waitUntil(() => this.element.querySelector('.page-frame-body h1').textContent.trim() == 'restored title');

      expect(this.element.querySelector('.page-frame-body p').textContent.trim()).to.equal('restored description');

      expect(this.element.querySelector('.container-bg-color')).to.have.class('bg-green-100');
    });

    it('can get the latest content version after another one was restored', async function () {
      await click('.page-editors-container');
      await click('.page-revisions .revision-item');
      await click('.page-revisions .latest-version');

      await waitUntil((a) => this.element.querySelector('.page-editors-properties .page-name').value == 'test');

      expect(this.element.querySelector('.page-frame-body h1').textContent.trim()).to.equal('title');

      expect(this.element.querySelector('.page-frame-body p').textContent.trim()).to.equal('some content');

      expect(this.element.querySelector('.container-bg-color')).not.to.exist;
    });

    it('can save the page and content after restoration', async function () {
      await click('.page-editors-container');
      await click('.page-revisions .revision-item');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
    });
  });

  describe('the container visibility', function (hooks) {
    hooks.beforeEach(function () {
      page.layoutContainers = [
        {
          options: [],
          rows: [
            {
              options: [],
              cols: [
                {
                  name: 'col-1',
                  type: 'type',
                  data: {},
                  options: [],
                },
                {
                  name: 'col-2',
                  type: 'type',
                  data: {},
                  options: [],
                },
              ],
            },
          ],
        },
        {
          options: [],
          visibility: 'user:authenticated',
          rows: [
            {
              options: [],
              cols: [
                {
                  name: '1.0.0',
                  type: 'type',
                  data: {},
                  options: [],
                },
              ],
            },
          ],
        },
        {
          options: [],
          visibility: 'user:unauthenticated',
          rows: [
            {
              options: [],
              cols: [
                {
                  name: '2.0.0',
                  type: 'type',
                  data: {},
                  options: [],
                },
              ],
            },
          ],
        },
      ];

      page.cols.push({
        name: '1.0.0',
        type: 'blocks',
        data: {
          blocks: [
            {
              type: 'paragraph',
              data: {
                text: 'logged in',
              },
            },
          ],
        },
      });

      page.cols.push({
        name: '2.0.0',
        type: 'blocks',
        data: {
          blocks: [
            {
              type: 'paragraph',
              data: {
                text: 'logged out',
              },
            },
          ],
        },
      });

      authenticateSession();
    });

    it('shows the not authenticated visibility by default', async function () {
      await visit(`/manage/pages/${page._id}/content`);

      expect(this.element.querySelector('.btn-user-visibility .fa-user-slash')).to.exist;
      expect(this.element.querySelector('.page-frame-body').textContent).to.contain('logged out');
      expect(this.element.querySelector('.page-frame-body').textContent).not.to.contain('logged in');
    });

    it('shows the right props when the unauthenticated container is selected', async function () {
      await visit(`/manage/pages/${page._id}/content`);

      await click('.btn-config-container-2');

      expect(this.element.querySelector('.page-frame-body').textContent).not.to.contain('container 2');
    });

    it('shows the authenticated visibility when the button is toggled', async function () {
      await visit(`/manage/pages/${page._id}/content`);

      await click('.btn-user-visibility');

      expect(this.element.querySelector('.btn-user-visibility .fa-user')).to.exist;
      expect(this.element.querySelector('.page-frame-body').textContent).not.to.contain('logged out');
      expect(this.element.querySelector('.page-frame-body').textContent).to.contain('logged in');
    });
  });

  describe('global layout containers', function (hooks) {
    hooks.beforeEach(function () {
      space.layoutContainers = {
        'main-menu-container': {
          layers: { count: 1 },
          rows: [
            {
              options: [],
              name: 'row-1',
              cols: [
                { type: 'type', data: {}, name: 'col-1', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-2', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-3', componentCount: 1, options: [] },
              ],
            },
          ],
        },
      };

      page.layoutContainers = [
        {
          rows: [
            {
              name: 'row-1',
              cols: [
                { type: 'type', data: {}, name: 'col-1', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-2', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-3', componentCount: 1, options: [] },
              ],
              options: [],
              data: { container: {} },
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
          gid: 'main-menu-container',
        },
      ];

      page.cols = [
        {
          name: 'col-1',
          type: 'article',
          data: { source: { id: '5ca78e2160780601008f69e6', model: 'article' } },
        },
      ];
    });

    it('updates the space when a global col is used twice and it is updated', async function (a) {
      page.cols = [
        {
          gid: 'g-article',
          name: 'col-other',
          type: 'article',
          data: { source: { id: '5ca78e2160780601008f69e6', model: 'article' } },
        },
        {
          gid: 'g-article',
          name: 'col-1',
          type: 'article',
          data: { source: { id: '5ca78e2160780601008f69e6', model: 'article' } },
        },
      ];

      authenticateSession();
      await visit(`/manage/pages/${page._id}/content`);

      await triggerEvent('h1', 'mouseover');
      await click('.title-col-1-text');

      this.element.querySelector('.input-sm-text-size').value = '12';
      await triggerEvent('.input-sm-text-size', 'change');

      this.element.querySelector('.input-md-text-size').value = '13';
      await triggerEvent('.input-md-text-size', 'change');

      this.element.querySelector('.input-lg-text-size').value = '14';
      await triggerEvent('.input-lg-text-size', 'change');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      a.deepEqual(receivedPage.page.cols, [
        {
          container: 0,
          col: 0,
          row: 0,
          name: 'col-other',
          type: 'article',
          data: {
            source: { id: '5ca78e2160780601008f69e6', model: 'article' },
          },
          gid: 'g-article',
        },
        {
          container: 0,
          col: 0,
          row: 0,
          name: 'col-1',
          type: 'article',
          data: {
            source: { id: '5ca78e2160780601008f69e6', model: 'article' },
            style: {
              color: '',
              classes: ['text-size-12', 'text-size-md-13', 'text-size-lg-14'],
              minLines: { sm: 0, md: 0, lg: 0 },
              heading: '1',
              paragraphStyle: {
                color: '',
                classes: ['text-size-12', 'text-size-md-13', 'text-size-lg-14'],
                minLines: { sm: 0, md: 0, lg: 0 },
                heading: '1',
              },
            },
          },
          gid: 'g-article',
        },
      ]);

      a.deepEqual(receivedSpace.space.cols, {
        'g-article': {
          type: 'article',
          data: {
            source: { id: '5ca78e2160780601008f69e6', model: 'article' },
            style: {
              color: '',
              classes: ['text-size-12', 'text-size-md-13', 'text-size-lg-14'],
              minLines: { sm: 0, md: 0, lg: 0 },
              heading: '1',
              paragraphStyle: {
                color: '',
                classes: ['text-size-12', 'text-size-md-13', 'text-size-lg-14'],
                minLines: { sm: 0, md: 0, lg: 0 },
                heading: '1',
              },
            },
          },
        },
      });
    });

    it('can change the background of global col', async function () {
      page.cols = [];

      authenticateSession();
      await visit(`/manage/pages/${page._id}/content`);

      await triggerEvent('.page-col', 'mouseover');

      await click('.title-col-1-col');
      await click('.bs-color-picker .btn-color-secondary');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      expect(receivedPage.page.layoutContainers).to.deep.equal([
        {
          rows: [
            {
              name: 'row-1',
              cols: [
                {
                  type: 'type',
                  data: { container: { color: 'secondary' } },
                  name: 'col-1',
                  componentCount: 1,
                  options: [],
                },
                { type: 'type', data: {}, name: 'col-2', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-3', componentCount: 1, options: [] },
              ],
              options: [],
              data: {},
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
          gid: 'main-menu-container',
        },
      ]);

      expect(receivedSpace.space.layoutContainers).to.deep.equal({
        'main-menu-container': {
          rows: [
            {
              name: 'row-1',
              cols: [
                {
                  type: 'type',
                  data: { container: { color: 'secondary' } },
                  name: 'col-1',
                  componentCount: 1,
                  options: [],
                },
                { type: 'type', data: {}, name: 'col-2', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-3', componentCount: 1, options: [] },
              ],
              options: [],
              data: {},
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
          gid: 'main-menu-container',
        },
      });
    });

    it('can change the background of global row', async function (a) {
      page.cols = [];

      authenticateSession();
      await visit(`/manage/pages/${page._id}/content`);

      await triggerEvent('.page-col', 'mouseover');

      await click('.title-row-1-row');
      await click('.bs-color-picker .btn-color-secondary');

      await click('.btn-save');

      await waitUntil(() => receivedPage);
      await waitUntil(() => receivedSpace);

      a.deepEqual(receivedPage.page.layoutContainers, [
        {
          rows: [
            {
              name: 'row-1',
              cols: [
                { type: 'type', data: {}, name: 'col-1', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-2', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-3', componentCount: 1, options: [] },
              ],
              options: [],
              data: { container: { color: 'secondary' } },
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
          gid: 'main-menu-container',
        },
      ]);

      a.deepEqual(receivedSpace.space.layoutContainers, {
        'main-menu-container': {
          rows: [
            {
              name: 'row-1',
              cols: [
                { type: 'type', data: {}, name: 'col-1', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-2', componentCount: 1, options: [] },
                { type: 'type', data: {}, name: 'col-3', componentCount: 1, options: [] },
              ],
              options: [],
              data: { container: { color: 'secondary' } },
            },
          ],
          options: [],
          visibility: 'always',
          layers: { count: 1, effect: 'none', showContentAfter: 1 },
          gid: 'main-menu-container',
        },
      });
    });
  });
});
