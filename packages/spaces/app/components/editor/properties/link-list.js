/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { A } from "models/lib/array";

export default class ManageEditorsPropertiesLinkListComponent extends Component {
  @tracked _items;

  get items() {
    if (this._items) {
      return this._items;
    }

    if (!Array.isArray(this.args.value)) {
      return [];
    }

    return A(this.args.value ?? []);
  }

  @action
  dragItemEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList[sourceIndex];

    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    this._items = sourceList;

    this.triggerChange();
  }

  triggerChange() {
    this.args.onChange?.(this.items);
  }

  @action
  changeItem(index, value) {
    if (!this._items) {
      this._items = this.items;
    }
    this._items[index] = value;

    this.triggerChange();
  }

  @action
  deleteItem(index) {
    const newItems = [...this.items];

    newItems.splice(index, 1);
    this._items = newItems;

    this.triggerChange();
  }

  @action
  addItem() {
    const newItems = [...this.items];

    newItems.push({});

    this._items = newItems;
    this.triggerChange();
  }
}
