/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class MapMarkerFeatureComponent extends Component {
  @service store;
  @service space;

  @tracked feature;
  @tracked cover;
  @tracked pictures;
  @tracked isLoading;
  @tracked link;

  get pictureOptions() {
    return {
      size: {
        sizing: 'proportional',
        proportion: '4:3',
      },
    };
  }

  @action
  swiperSetup(instance) {
    this.swiper = instance;
  }

  @action
  async setup() {
    if (!this.args.featureId) {
      return;
    }

    this.link = this.space?.getLinkFor('Feature', this.args.featureId);
    this.feature = await this.store.findRecord('feature', this.args.featureId);
    const pictures = await this.feature.pictures;

    if (pictures.length == 1) {
      this.cover = pictures[0];
      this.pictures = [];
    }

    if (pictures.length > 1) {
      this.pictures = pictures;
      this.cover = null;
    }

    later(() => {
      this.swiper?.update?.();
      this.swiper?.slideTo?.(0, 0);
    });
  }
}
