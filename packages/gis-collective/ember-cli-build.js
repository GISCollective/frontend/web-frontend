/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const Funnel = require('broccoli-funnel');

const CleanCss = require('clean-css');
const CleanCSSFilter = require('broccoli-clean-css');
const inlineSourceMapComment = require('inline-source-map-comment');

CleanCSSFilter.prototype.processString = async function (str) {
  const cleanCss = new CleanCss(this.options);
  const result = await cleanCss.minify(str);

  if (result.sourceMap) {
    return result.styles + '\n' + inlineSourceMapComment(result.sourceMap, { block: true }) + '\n';
  }

  return result.styles;
};

module.exports = function (defaults) {
  const typefaces = [
    'lato',
    'crimson-pro',
    'league-gothic',
    'paytone-one',
    'fira-sans-condensed',
    'anton',
    'fjalla-one',
    'zen-kaku-gothic-new',
    'poppins',
    'fredoka-one',
    'merriweather',
    'zilla-slab',
    'tinos',
    'lobster',
    'freehand',
    'rubik-doodle-shadow',
    'fredericka-the-great',
    'londrina-sketch',
  ];
  const typefacesVariable = [
    'oswald',
    'inconsolata',
    'roboto-slab',
    'newsreader',
    'overpass',
    'figtree',
    'nunito',
    'comfortaa',
    'grandstander',
    'pixelify-sans',
    'big-shoulders-inline-display',
    'baloo-2',
  ];

  let app = new EmberApp(defaults, {
    '@embroider/macros': {
      setConfig: {
        '@ember-data/store': {
          polyfillUUID: true,
        },
      },
    },
    sassOptions: {
      includePaths: [
        '../../node_modules/bootstrap/scss',
        '../../node_modules/swiper',
        '../../node_modules/@fontsource',
        '../../node_modules/vanillajs-datepicker/sass',
        '../../node_modules/@fontsource-variable',
        '../spaces/app',
        '../spaces-view/app',
        '../map/app',
        '../manage/app',
        '../core/app',
        '.',
      ],
    },
    'ember-power-select': {
      theme: 'bootstrap',
    },
    sourcemaps: {
      enabled: true,
    },
    'ember-simple-auth': {
      useSessionSetupMethod: true,
    },
    'asset-cache': {
      include: ['assets/**/*', 'assets/*', 'img/*'],
    },
    'ember-service-worker': {
      versionStrategy: 'every-build',
    },
    'esw-index': {
      // Bypass esw-index and don't serve cached index file for matching URLs
      excludeScope: [
        /\/api(\/.*)?$/,
        /\/api-files(\/.*)?$/,
        /\/api-issues(\/.*)?$/,
        /\/stats(\/.*)?$/,
        /\/admin(\/.*)?$/,
        /\/auth(\/.*)?$/,
      ],
    },
    typefaceOptions: {
      disableAuto: true,
      typefaces: [...typefaces, ...typefacesVariable],
    },
    autoImport: {
      webpack: {
        devtool: 'eval',
        node: { global: true }, // Fix: "Uncaught ReferenceError: global is not defined"
      },
    },
    gisCollective: {
      version: process.env.VERSION || 'develop',
    },
    babel: {
      plugins: [],
    },
    emberData: {
      // debug: {
      //   LOG_PAYLOADS: false, // data store received to update cache with
      //   LOG_OPERATIONS: false, // updates to cache remote state
      //   LOG_MUTATIONS: true, // updates to cache local state
      //   LOG_NOTIFICATIONS: false,
      //   LOG_REQUESTS: true, // log Requests issued via the request manager
      //   LOG_REQUEST_STATUS: false,
      //   LOG_IDENTIFIERS: false,
      //   LOG_GRAPH: false, // relationship storage
      //   LOG_INSTANCE_CACHE: false, // instance creation/deletion
      // },
    },
  });

  app.import('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', {
    outputFile: 'assets/bootstrap.min.js',
  });
  app.import('node_modules/ol/ol.css', { outputFile: 'assets/ol.css' });
  app.import('node_modules/ol/ol.css', { outputFile: 'assets/ol.css' });

  const fontFunnels = typefaces.map(
    (a) =>
      new Funnel(`../../node_modules/@fontsource/${a}/files`, {
        srcDir: '/',
        include: ['**/*.*'],
        destDir: `/assets/files/${a}`,
      }),
  );

  const fontVariableFunnels = typefacesVariable.map(
    (a) =>
      new Funnel(`../../node_modules/@fontsource-variable/${a}/files`, {
        srcDir: '/',
        include: ['**/*.*'],
        destDir: `/assets/files/${a}`,
      }),
  );

  return app.toTree([...fontFunnels, ...fontVariableFunnels], { overwrite: true });
};
