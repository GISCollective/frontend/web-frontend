/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import PictureMeta from 'models/lib/picture-meta';

export default class AdminUsersEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service modal;
  @service store;
  @service router;

  @tracked isRoleLoading = false;
  @tracked isRoleError = false;

  get canRemoveAccount() {
    if (!this.model.user.isAdmin) {
      return true;
    }

    if (this.model.user.id == this.user.id) {
      return false;
    }

    return true;
  }

  get detailedLocation() {
    if (typeof this.model.detailedLocation?.value == 'string') {
      return this.model.detailedLocation?.value.toLowerCase() == 'true';
    }

    return this.model.detailedLocation?.value;
  }

  get breadcrumbs() {
    return [
      {
        route: 'admin.users',
        text: this.intl.t('all users'),
        capitalize: true,
      },
      {
        text: this.intl.t('edit-model.title', { name: this.model.user.email }),
      },
    ];
  }

  @action
  update() {
    return this.model.profile.save();
  }

  @action
  async updateAccount(joinedTime, email, userName) {
    this.model.user.email = email;
    this.model.user.username = userName;
    this.model.profile.joinedTime = joinedTime;

    await this.model.user.save();
    await this.model.profile.save();

    return true;
  }

  @action
  async createImage() {
    this.model.profile.picture = this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });

    return await this.model.profile.picture;
  }

  @action
  async changePassword(password, newPassword) {
    try {
      await this.model.user.changePassword(password, newPassword);

      this.notifications.showMessage(
        this.intl.t('change password'),
        this.intl.t('password-change-success', {
          name: this.model.user.email,
        })
      );
    } catch (err) {
      this.notifications.handleError(err);
    }
  }

  @action
  async delete() {
    let password;

    try {
      password = await this.modal.confirmWithPassword(
        this.intl.t('delete account', { size: 1 }),
        this.intl.t('Are you sure you want to delete this account?')
      );
    } catch (err) {
      return;
    }

    try {
      await this.model.user.removeUser(password);
      this.notifications.showMessage(
        this.intl.t('Remove user'),
        this.intl.t('You have successfully removed the user.')
      );
      this.router.transitionTo('admin.users');
    } catch (err) {
      this.notifications.handleError(err);
    }
  }

  @action
  async promote() {
    let password;

    try {
      password = await this.modal.confirmWithPassword(
        this.intl.t('promote user'),
        this.intl.t('promote-user-question', { name: this.model.user.email })
      );
    } catch (err) {
      return;
    }

    this.isRoleLoading = true;
    this.isRoleError = false;

    try {
      await this.model.user.promote(password);

      this.notifications.showMessage(
        this.intl.t('promote user'),
        this.intl.t('promote-user-success-message', {
          name: this.model.user.email,
        })
      );
      this.isRoleLoading = false;
      this.model.user.set('isAdmin', true);
    } catch (err) {
      this.isRoleLoading = false;
      this.isRoleError = true;
      this.notifications.handleError(err);
    }
  }

  @action
  async downgrade() {
    let password;

    try {
      password = await this.modal.confirmWithPassword(
        this.intl.t('downgrade user'),
        this.intl.t('downgrade-user-question', { name: this.model.user.email })
      );
    } catch (err) {
      return;
    }

    this.isRoleLoading = true;
    this.isRoleError = false;

    try {
      await this.model.user.downgrade(password);

      this.notifications.showMessage(
        this.intl.t('downgrade user'),
        this.intl.t('downgrade-user-success-message', {
          name: this.model.user.email,
        })
      );
      this.isRoleLoading = false;
      this.model.user.isAdmin = false;
    } catch (err) {
      this.isRoleLoading = false;
      this.isRoleError = true;
      this.notifications.handleError(err);
    }
  }
}
