/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageSpaceTemplateSelectorComponent extends Component {
  @service store;
  @tracked spaces;

  @action
  async setup() {
    this.spaces = await this.store.query('space', { tpl: true });

    const promises = this.spaces.map((a) => a?.cover).filter((a) => a);

    await Promise.all(promises);
  }

  @action
  select(value) {
    if (value.id == this.args.value) {
      return this.args.onChange?.();
    }

    return this.args.onChange?.(value.id, value);
  }
}
