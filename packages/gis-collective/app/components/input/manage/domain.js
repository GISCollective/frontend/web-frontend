/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class InputManageDomainComponent extends Component {
  @service space;
  @service preferences;

  @tracked _domain = null;
  @tracked availableDomains;

  get dns() {
    return `${this.args.value.domain} ALIAS ${this.space.defaultDomain}.`;
  }

  get dnsVerification() {
    return `_gis-collective-verification-code.${this.args.value.domain} TXT gis-collective-verification-code=${this.args.value.domainValidationKey}`;
  }

  get selectedDomain() {
    const pos = this.domain.indexOf('.') + 1;
    return this.domain.substr(pos);
  }

  get domain() {
    if (this._domain !== null) {
      return this._domain;
    }

    return this.args.value?.domain ?? '';
  }

  set domain(value) {
    this._domain = value;
  }

  get hasDomainOptions() {
    return this.availableDomains?.length > 1;
  }

  @action
  async setup() {
    const preference = await this.preferences.getPreference('spaces.domains');

    this.availableDomains = preference?.value?.split(',').map((a) => a.trim());
  }

  @action
  changeDomain(value) {
    this.domain = value;
  }

  @action
  verify() {
    return this.args.onVerify?.();
  }

  @action
  save() {
    return this.args.onSave?.(this.domain);
  }

  @action
  cancel() {
    this.domain = null;
    return this.args.onCancel?.();
  }
}
