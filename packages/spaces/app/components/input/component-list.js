/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class InputComponentListComponent extends Component {
  @tracked areComponentsVisible = false;

  get selected() {
    const name = this.args.value;

    return Object.values(this.args.groups)
      .flatMap((a) => a?.options ?? [])
      .find((a) => a.name == name);
  }

  @action
  change(value) {
    this.areComponentsVisible = false;
    this.args.onChange?.(value.name);
  }

  @action
  searchRecord(term) {
    const result = [];

    for (const group of this.args.groups) {
      const options = group.options.filter(
        (a) =>
          a.name.toLowerCase().includes(term.toLowerCase()) || `${a.label}`.toLowerCase().includes(term.toLowerCase()),
      );

      if (options.length > 0) {
        result.push({
          groupName: group.groupName,
          options,
        });
      }
    }

    return result;
  }
}
