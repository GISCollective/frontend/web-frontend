/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { getOwner } from '@ember/application';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export class Visibility {
  @tracked isPublic;
  @tracked isDefault;
  @tracked _team;
  @tracked teamId = '';

  constructor(store, isPublic, isDefault, team) {
    this.store = store;
    this.isPublic = isPublic;
    this.isDefault = isDefault;
    this.teamId = team;
  }

  fetchTeam() {
    if (this._team) {
      return this._team;
    }

    if (!this.teamId || this.fetchError) {
      return null;
    }

    return new Promise(async (resolve, reject) => {
      const tmp = this.store.peekRecord('team', this.teamId);

      if (tmp) {
        return later(() => {
          this._team = tmp;

          resolve(this._team);
        });
      }

      try {
        this._team = await this.store.findRecord('team', this.teamId);
      } catch (err) {
        this.fetchError = err;
        console.error(err.message);
      }

      return resolve(this._team);
    });
  }

  get team() {
    if (!this._team) {
      this.fetchTeam();
    }

    return this._team;
  }

  set team(value) {
    this._team = value;

    if (value?.id) {
      this.teamId = value.id;
    }
  }
}

export default class VisibilityTransform extends Transform {
  deserialize(serialized) {
    let applicationInstance = getOwner(this);
    const store = applicationInstance.lookup('service:store');

    return new Visibility(store, serialized.isPublic || false, serialized.isDefault || false, serialized.team);
  }

  serialize(deserialized) {
    if (!deserialized) {
      return;
    }

    let team = deserialized.teamId;

    if (!team && deserialized.team?.id) {
      team = deserialized.team?.id;
    }

    if (!team && typeof deserialized.team == 'string') {
      team = deserialized.team;
    }

    return {
      isPublic: deserialized.isPublic || false,
      isDefault: deserialized.isDefault || false,
      team,
    };
  }
}
