/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { A } from 'core/lib/array';
import { LinkMap } from 'models/transforms/link-map';

class Pair {
  @tracked source;
  @tracked destination;
}

export default class InputManageLinkMapComponent extends Component {
  @tracked pairs;

  factoryValue() {
    return new Pair();
  }

  @action
  createPairs() {
    let pairs = A();

    for (const key in this.args.value?.value) {
      const pair = new Pair();
      pair.source = key;
      pair.destination = this.args.value?.value?.[key];

      pairs.push(pair);
    }

    this.pairs = pairs;
  }

  get value() {
    if (this.pairs) {
      const result = {};

      for (const pair of this.pairs) {
        result[pair.source] = pair.destination;
      }

      return new LinkMap(result);
    }

    return this.args.value?.value ?? {};
  }

  @action
  change(value) {
    this.pairs = value;
  }

  @action
  cancel() {
    this.pairs = null;

    return this.args.onCancel();
  }

  @action
  save() {
    return this.args.onSave?.(this.value);
  }
}
