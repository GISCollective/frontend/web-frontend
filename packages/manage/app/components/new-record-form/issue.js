/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class NewRecordFormIssueComponent extends Component {
  @service store;
  @tracked title;
  @tracked description;

  get isInvalid() {
    return !this.title || !this.description;
  }

  createIssue() {
    return this.store.createRecord('issue', {
      id: null,
      title: this.title,
      description: this.description,
      type: 'none',
      feature: this.args.feature,
    });
  }

  @action
  async save() {
    const issue = await this.createIssue();

    await issue.save();

    return this.args.onSubmit?.();
  }
}
