/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddFeatureController extends Controller {
  @service notifications;
  @service intl;
  @service router;

  queryParams = ['all'];

  @tracked all = false;
  @tracked map;
  @tracked team;

  get breadcrumbs() {
    const result = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    if (this.team?.id) {
      result.push({
        route: 'manage.dashboards.dashboard',
        text: this.team.name,
        model: this.team.id,
      });
    }

    if (this.map) {
      result.push({
        route: 'manage.dashboards.map',
        text: this.map.name,
        model: this.map.id,
      });
    }

    result.push({
      text: this.intl.t('new feature'),
      capitalize: true,
    });

    return result;
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  async onMapSelect(value) {
    this.map = value;

    if (this.map) {
      this.team = await this.map?.visibility?.fetchTeam?.();
    }
  }

  @action
  async save(feature) {
    await feature.save();

    this.router.transitionTo('manage.features.edit', feature.id);
  }
}
