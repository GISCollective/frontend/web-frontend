/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | card/cover-title-description', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let store;
  let mapData;
  let picture;

  hooks.before(function () {
    server = new TestServer();
    mapData = server.testData.storage.addDefaultMap();
    picture = server.testData.storage.addDefaultPicture();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };

    store = this.owner.lookup('service:store');
  });

  it('renders', async function () {
    await render(hbs`<Card::CoverTitleDescription />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  describe('using map records', function () {
    it('renders the map title', async function () {
      const map = await store.findRecord('map', mapData._id);

      this.set('map', map);

      await render(hbs`<Card::CoverTitleDescription @record={{this.map}} />`);

      expect(this.element.querySelector('.name').textContent.trim()).to.equal('Harta Verde București');
    });

    it('does not render a link when the value is not set', async function () {
      const map = await store.findRecord('map', mapData._id);

      this.set('map', map);

      await render(hbs`<Card::CoverTitleDescription @record={{this.map}} />`);

      expect(this.element.querySelector('a')).not.to.exist;
    });

    it('does not render a link when the value has a destination to a different model type', async function () {
      const map = await store.findRecord('map', mapData._id);

      this.set('campaign', map);

      this.set('value', {
        data: {
          destination: { slug: '/campaigns/:campaign-id' },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} @value={{this.value}} />`);

      expect(this.element.querySelector('a')).not.to.exist;
    });

    it('renders a link when the value has a map destination', async function () {
      const map = await store.findRecord('map', mapData._id);

      this.set('campaign', map);

      this.set('value', {
        data: {
          destination: { slug: '/maps/:map-id' },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} @value={{this.value}} />`);

      expect(this.element.querySelector('a')).to.have.attribute('href', `/maps/${mapData._id}`);
    });

    it("uses the 'size.sizing' property when the 'sizing' is set", async function (a) {
      const map = await store.findRecord('map', mapData._id);

      this.set('campaign', map);

      this.set('value', {
        data: {
          picture: {
            sizing: 'proportional',
            size: {
              sizing: 'fixed height',
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} @value={{this.value}} />`);

      expect(this.element.querySelector('img')).to.have.attribute('class', ``);
    });

    it('uses the container properties for pictures', async function (a) {
      const map = await store.findRecord('map', mapData._id);

      this.set('campaign', map);

      this.set('value', {
        data: {
          picture: {
            size: {
              sizing: 'auto',
              customStyle: {
                md: {
                  height: '50px',
                  width: 'auto',
                },
                lg: {
                  height: '50px',
                  width: 'auto',
                },
                sm: {
                  height: '50px',
                  width: 'auto',
                },
              },
            },
            container: {
              style: {
                sm: {
                  height: '150px',
                  width: '',
                },
                md: {
                  height: '200px',
                },
                lg: {
                  height: '300px',
                },
              },
              classes: ['justify-content-start', 'align-items-start'],
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} @value={{this.value}} />`);

      const divElement = this.element.querySelector('.page-col-base-container');
      const height = document.defaultView.getComputedStyle(divElement).height;

      expect(height).to.equal('300px');

      const id = this.element.querySelector('.page-col-base-container').attributes.getNamedItem('id').textContent;
      expect(this.element.querySelector(`[data-selector*="#${id} img"]`)).not.to.exist;
    });

    it('applies the proportional size', async function (a) {
      const map = await store.findRecord('map', mapData._id);

      this.set('campaign', map);

      this.set('value', {
        data: {
          picture: {
            size: {
              sizing: 'proportional',
              proportion: '4:3',
              customStyle: {
                md: {
                  height: '50px',
                  width: 'auto',
                },
                lg: {
                  height: '50px',
                  width: 'auto',
                },
                sm: {
                  height: '50px',
                  width: 'auto',
                },
              },
            },
            container: {
              style: {
                sm: {
                  height: '150px',
                  width: '',
                },
                md: {
                  height: '200px',
                },
                lg: {
                  height: '300px',
                },
              },
              classes: ['justify-content-start', 'align-items-start'],
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} @value={{this.value}} />`);

      const divElement = this.element.querySelector('.page-col-base-container');

      const id = this.element.querySelector('.page-col-base-container').attributes.getNamedItem('id').textContent;
      expect(this.element.querySelector(`[data-selector*="#${id} img"]`)).not.to.exist;
      expect(this.element.querySelector(`[data-selector*="#${id} .picture-hash-container"]`)).not.to.exist;

      const height = document.defaultView.getComputedStyle(divElement).height;
      expect(height).to.equal('300px');
    });
  });

  describe('using campaign records', function () {
    it('renders the campaign title', async function () {
      this.set('campaign', {
        id: 'some-id',
        name: 'some name',
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      expect(this.element.querySelector('.name').textContent.trim()).to.equal('some name');
    });

    it('renders the campaign picture', async function () {
      this.set('campaign', {
        id: 'some-id',
        cover: {
          picture: 'some-picture',
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      expect(this.element.querySelector('img')).to.have.attribute('src', 'some-picture/lg');
    });

    it('renders the campaign first paragraph', async function () {
      this.set('campaign', {
        id: 'some-id',
        cover: {
          picture: 'some-picture',
        },
        firstParagraph: 'description',
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      expect(this.element.textContent).to.contain('description');
      expect(this.element.querySelector('.card-description')).to.exist;
    });

    it('does not render the campaign options', async function () {
      this.set('campaign', {
        id: 'some-id',
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      expect(this.element.querySelector('.campaign-options')).not.to.exist;
    });

    it('renders the campaign options when the campaign is editable', async function () {
      this.set('campaign', {
        id: 'some-id',
        canEdit: true,
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      expect(this.element.querySelector('.record-options')).to.exist;
    });

    it('allows to delete an editable campaign', async function () {
      this.set('campaign', {
        id: 'some-id',
        canEdit: true,
      });

      let deletedCampaign;
      this.set('onDelete', (val) => {
        deletedCampaign = val;
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} @onDelete={{this.onDelete}} />`);

      await click('.record-options');
      await click('.btn-delete');

      expect(deletedCampaign).deep.equal({
        id: 'some-id',
        canEdit: true,
      });
    });

    it('allows to edit an editable campaign', async function () {
      this.set('campaign', {
        id: 'some-id',
        canEdit: true,
        constructor: {
          modelName: 'campaign',
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      await click('.record-options');
      expect(this.element.querySelector('.dropdown-item-edit')).to.exist;
    });

    it('shows an unpublished icon when the campaign is not public', async function () {
      this.set('campaign', {
        id: 'some-id',
        visibility: {
          isPublic: false,
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      expect(this.element.querySelector('.icon-not-published')).to.exist;
    });

    it('does not show an unpublished icon when the campaign is public', async function () {
      this.set('campaign', {
        id: 'some-id',
        visibility: {
          isPublic: true,
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.campaign}} />`);

      expect(this.element.querySelector('.icon-not-published')).not.to.exist;
    });
  });

  describe('using event records', function (hooks) {
    let eventData;
    let pictureData;
    let calendarData;
    let calendarPictureData;
    let iconData;

    hooks.before(function () {
      server = new TestServer();
      eventData = server.testData.storage.addDefaultEvent();
      calendarData = server.testData.storage.addDefaultCalendar();
      iconData = server.testData.storage.addDefaultIcon();

      pictureData = server.testData.storage.addPicture({
        ...picture,
        name: 'default',
        _id: 'event-picture',
      });
      calendarPictureData = server.testData.storage.addPicture({
        ...picture,
        _id: 'calendar-picture',
        name: 'calendar-picture',
        picture: 'calendar-picture',
      });

      eventData.cover = pictureData._id;
      eventData.calendar = calendarData._id;
      eventData.icons = [iconData._id];
      calendarData.cover = calendarPictureData._id;
    });

    it('renders the calendar cover when the event has none and defaultCover (legacy) is "from parent"', async function () {
      const event = await store.findRecord('event', eventData._id);

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'from parent',
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(this.element.querySelector('img')).to.have.attribute('src', 'calendar-picture/lg');
    });

    it('renders the calendar cover when the event has none and defaultCover is "from parent"', async function () {
      const event = await store.findRecord('event', eventData._id);

      this.set('event', event);
      this.set('value', {
        data: {
          card: {
            cover: { defaultCover: 'from parent' },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(this.element.querySelector('img')).to.have.attribute('src', 'calendar-picture/lg');
    });

    it('does not render the calendar cover when the event has none and defaultCover is "default"', async function () {
      const event = await store.findRecord('event', eventData._id);

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(this.element.querySelector('img')).to.have.attribute('src', '/test/5d5aa72acac72c010043fb59.jpg.lg.jpg');
    });

    it('renders the entries when the extra field is selected (legacy)', async function (a) {
      const event = await store.findRecord('event', eventData._id);

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          extraFields: {
            entries: true,
            nextOccurrence: false,
            schedule: false,
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(this.element.querySelector('.card-entries').textContent.trim()).to.contain(
        'monthly between 01/01/2018 and 01/02/2018',
      );
    });

    it('renders the schedule when the extra field is selected (legacy)', async function (a) {
      const event = await store.findRecord('event', eventData._id);
      event.entries[0].repetition = 'weekly';

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          extraFields: {
            entries: false,
            nextOccurrence: false,
            schedule: true,
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(
        this.element
          .querySelector('.time-table-day')
          .textContent.split('\n')
          .map((a) => a.trim())
          .filter((a) => a)
          .join(' '),
      ).to.equal('Mon: 12:30 - 13:30');
    });

    it('renders the icons when the extra field is selected (legacy)', async function (a) {
      const event = await store.findRecord('event', eventData._id);
      await event.loadRelations();

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          extraFields: {
            entries: false,
            nextOccurrence: false,
            icons: true,
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(this.element.querySelector('.card-icons .icon')).to.have.attribute('alt', 'Healthy Dining');
    });

    it('renders the entries when the extra field is selected', async function (a) {
      const event = await store.findRecord('event', eventData._id);

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          card: {
            extraFields: {
              entries: true,
              nextOccurrence: false,
              schedule: false,
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(this.element.querySelector('.card-entries').textContent.trim()).to.contain(
        'monthly between 01/01/2018 and 01/02/2018',
      );
    });

    it('renders the schedule when the extra field is selected', async function (a) {
      const event = await store.findRecord('event', eventData._id);
      event.entries[0].repetition = 'weekly';

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          card: {
            extraFields: {
              entries: false,
              nextOccurrence: false,
              schedule: true,
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(
        this.element
          .querySelector('.time-table-day')
          .textContent.split('\n')
          .map((a) => a.trim())
          .filter((a) => a)
          .join(' '),
      ).to.equal('Mon: 12:30 - 13:30');
    });

    it('renders the icons when the extra field is selected', async function (a) {
      const event = await store.findRecord('event', eventData._id);
      await event.loadRelations();

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          card: {
            extraFields: {
              entries: false,
              nextOccurrence: false,
              icons: true,
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(this.element.querySelector('.card-icons .icon')).to.have.attribute('alt', 'Healthy Dining');
    });

    it('renders the next date with location when it is set', async function (a) {
      const event = await store.findRecord('event', eventData._id);
      await event.loadRelations();

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          card: {
            extraFields: {
              entries: false,
              nextOccurrence: false,
              icons: false,
              nextDateWithLocation: true,
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(
        this.element
          .querySelector('.card-next-date-with-location')
          .textContent.split('\n')
          .map((a) => a.trim())
          .filter((a) => a)
          .join(' '),
      ).to.equal('Monday, 1 January 2018 at some location');
    });

    it('renders the full description when is setup', async function (a) {
      const event = await store.findRecord('event', eventData._id);
      await event.loadRelations();
      event.article = {
        blocks: [
          { type: 'paragraph', data: { text: 'paragraph 1' } },
          { type: 'paragraph', data: { text: 'paragraph 2' } },
        ],
      };

      this.set('event', event);
      this.set('value', {
        data: {
          picture: {
            defaultCover: 'default',
          },
          card: {
            extraFields: {
              entries: false,
              nextOccurrence: false,
              icons: false,
              nextDateWithLocation: false,
              fullDescription: true,
            },
          },
        },
      });

      await render(hbs`<Card::CoverTitleDescription @record={{this.event}} @value={{this.value}} />`);

      expect(
        this.element
          .querySelector('.card')
          .textContent.split('\n')
          .map((a) => a.trim())
          .filter((a) => a)
          .join(' '),
      ).to.equal('test event paragraph 1 paragraph 2');
    });
  });
});
