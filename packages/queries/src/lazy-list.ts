/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { tracked } from "@glimmer/tracking";
import { Factories } from "./factories";
import { addObject, addObjects, lastObject, toArray } from "./array";
import { StoreType } from "./StoreType";

export class LazyList<T extends Object> {
  @tracked buckets: T[][];
  @tracked isLoading = false;
  @tracked lastError: null | string | Error = null;

  model: string = "";
  query: Record<string, string | number | boolean> = {};
  store: StoreType | undefined;

  itemsPerPage = 24;

  constructor(model: string | T[][], query: Record<string, string | number | boolean>, store: StoreType | undefined) {
    if (Array.isArray(model)) {
      this.buckets = Factories.instance.A(model);
      return;
    }

    this.model = model;
    this.query = query;
    this.store = store;
    this.buckets = Factories.instance.A();
  }

  get loadedAny() {
    return this.skip - this.deletedCount > 0;
  }

  get isEmpty() {
    return this.skip - this.deletedCount == 0;
  }

  get canLoadMore() {
    if (!this.store) {
      return false;
    }

    if (this.lastError) {
      return false;
    }

    if (this.query?.limit) {
      return this.buckets.length == 0;
    }

    if (this.buckets?.length == 0) {
      return true;
    }

    return lastObject(this.buckets)?.length == this.itemsPerPage;
  }

  get items(): T[] {
    const result: T[] = [];

    for (const list of this.buckets) {
      addObjects(result, list);
    }

    return result;
  }

  get skip() {
    return this.buckets.map((a) => a.length).reduce((a, b) => a + b, 0);
  }

  get deletedCount() {
    return this.buckets.map((a) => a.filter((b) => "isDeleted" in b && b.isDeleted).length).reduce((a, b) => a + b, 0);
  }

  limitTo(newLimit: number) {
    const items = this.items.slice(0, newLimit);

    const result = new LazyList([items], {}, this.store);
    result.model = this.model;
    result.query = { ...this.query, limit: newLimit };
    result.store = this.store;

    return result;
  }

  buildParams() {
    let limit = this.itemsPerPage;
    let skip = 0;

    if (this.query.limit) {
      limit = Number(this.query.limit);
    } else {
      skip = this.skip - this.deletedCount;
    }

    return {
      ...this.query,
      limit,
      skip,
    };
  }

  async loadNext() {
    if (!this.canLoadMore) {
      return;
    }

    this.isLoading = true;
    let results: T[] = [];

    try {
      results = (await this.store?.query(this.model, this.buildParams())) ?? [];
      results = toArray(results);

      await Promise.allSettled(
        results
          .map((a) => ("loadRelations" in a && typeof a.loadRelations == "function" ? a.loadRelations() : a))
          .filter((a) => a),
      );

    } catch (err) {
      console.error(err);
      this.lastError = err as Error;
    }

    addObject(this.buckets, results);
    this.isLoading = false;
  }
}
