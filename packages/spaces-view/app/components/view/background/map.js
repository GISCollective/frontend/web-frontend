/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { guidFor } from '@ember/object/internals';

export default class PageColBackgroundMapComponent extends Component {
  @tracked height;
  @tracked hasSize;

  get elementId() {
    return this.args?.id || `page-col-background-map-${guidFor(this)}`;
  }

  get style() {
    if (!this.height) {
      return htmlSafe(``);
    }

    return htmlSafe(`height: ${this.height}px;`);
  }

  @action
  watchSize(element) {
    this.container = element;

    this.onResize();
    this.spyResize();
  }

  spyResize() {
    try {
      this.sizeObserver?.disconnect?.();

      this.sizeObserver = new ResizeObserver(() => {
        window?.requestAnimationFrame(() => {
          this.onResize();
        });
      });
      this.sizeObserver.observe(this.container);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  }

  onResize() {
    const rect = this.container?.getBoundingClientRect?.();

    this.height = this.container?.scrollHeight;
    this.hasSize = !!this.height;
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.sizeObserver?.disconnect?.();
  }
}
