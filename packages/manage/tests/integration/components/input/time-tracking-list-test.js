import { module } from 'qunit';
import { setupRenderingTest, it } from 'dummy/tests/helpers';
import { click, fillIn, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | input/time-tracking-list', function (hooks) {
  setupRenderingTest(hooks);

  it('renders a value', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', [
      {
        hours: 1,
        date: new Date('2024-03-12T02:22:33Z'),
        details: 'some details',
      },
    ]);

    await render(hbs`<Input::TimeTrackingList @value={{this.value}} @onChange={{this.change}} />`);

    expect(this.element.querySelectorAll('.value-hours')).to.have.length(1);
    expect(this.element.querySelector('.value-hours').value).to.equal('1');
  });

  it('can add a value', async function (assert) {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('value', []);

    await render(hbs`<Input::TimeTrackingList @value={{this.value}} @onChange={{this.change}} />`);

    await click('.btn-add');

    await fillIn('.value-hours', '22');
    await fillIn('.value-details', 'new details');
    await fillIn('.datepicker-input', '2022-05-06');

    this.element.querySelector('.form-select.hour').value = '9';
    this.element.querySelector('.form-select.minute').value = '0';

    await triggerEvent('.form-select.hour', 'change');
    await triggerEvent('.form-select.minute', 'change');

    const list = value.map((a) => a.toJSON());

    expect(list).to.deep.equal([{ hours: 22, details: 'new details', date: '2022-05-06T07:00:00.000Z' }]);
  });
});
