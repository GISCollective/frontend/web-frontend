/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | custom-page-style', function (hooks) {
  setupRenderingTest(hooks);

  it('generates the styles for one column', async function () {
    this.set('value', [
      {
        rows: [
          {
            cols: [{ type: '', options: ['wpx-100', 'wpx-md-300', 'wpx-lg-600'] }],
            options: ['min-vh-60', 'gx-0'],
          },
        ],
        options: ['container-fluid', 'pt-0', 'pb-0', 'ps-0', 'pe-0'],
      },
    ]);

    await render(hbs`<CustomPageStyle @value={{this.value}} />`);

    const styles = this.element.textContent
      .trim()
      .split('\n')
      .filter((a) => a);

    expect(styles[0]).to.equal('.wpx-100 { flex: 0 0 auto; width: 100px; }');
    expect(styles[1]).to.equal('.enforce-sm-size .wpx-100, .wpx-100 { flex: 0 0 auto; width: 100px; }');
    expect(styles[2]).to.equal('@media (min-width: 768px) { .wpx-md-300 { flex: 0 0 auto; width: 300px; } }');
    expect(styles[3]).to.equal('.enforce-md-size .wpx-md-300, .wpx-300 { flex: 0 0 auto; width: 300px; }');
    expect(styles[4]).to.equal('@media (min-width: 992px) { .wpx-lg-600 { flex: 0 0 auto; width: 600px; } }');
    expect(styles[5]).to.equal('.enforce-lg-size .wpx-lg-600, .wpx-600 { flex: 0 0 auto; width: 600px; }');
  });

  it('generates the styles for 3 columns', async function () {
    this.set('value', [
      {
        rows: [
          {
            cols: [
              { type: '', options: ['wpx-200'] },
              { type: '', options: ['wpx-md-400'] },
              { type: '', options: ['wpx-lg-650'] },
            ],
            options: ['min-vh-60', 'gx-0'],
          },
        ],
        options: ['container-fluid', 'pt-0', 'pb-0', 'ps-0', 'pe-0'],
      },
    ]);

    await render(hbs`<CustomPageStyle @value={{this.value}} />`);

    const styles = this.element.textContent
      .trim()
      .split('\n')
      .filter((a) => a);

    expect(styles[0]).to.equal('.wpx-200 { flex: 0 0 auto; width: 200px; }');
    expect(styles[1]).to.equal('.enforce-sm-size .wpx-200, .wpx-200 { flex: 0 0 auto; width: 200px; }');
    expect(styles[2]).to.equal('@media (min-width: 768px) { .wpx-md-400 { flex: 0 0 auto; width: 400px; } }');
    expect(styles[3]).to.equal('.enforce-md-size .wpx-md-400, .wpx-400 { flex: 0 0 auto; width: 400px; }');
    expect(styles[4]).to.equal('@media (min-width: 992px) { .wpx-lg-650 { flex: 0 0 auto; width: 650px; } }');
    expect(styles[5]).to.equal('.enforce-lg-size .wpx-lg-650, .wpx-650 { flex: 0 0 auto; width: 650px; }');
  });

  it('generates the styles for 2 rows', async function () {
    this.set('value', [
      {
        rows: [
          {
            cols: [
              { type: '', options: ['wpx-md-400'] },
              { type: '', options: ['wpx-lg-650'] },
            ],
            options: ['min-vh-60', 'gx-0'],
          },
          {
            cols: [{ type: '', options: ['wpx-200'] }],
            options: ['min-vh-60', 'gx-0'],
          },
        ],
        options: ['container-fluid', 'pt-0', 'pb-0', 'ps-0', 'pe-0'],
      },
    ]);

    await render(hbs`<CustomPageStyle @value={{this.value}} />`);

    const styles = this.element.textContent
      .trim()
      .split('\n')
      .filter((a) => a);

    expect(styles[0]).to.equal('@media (min-width: 768px) { .wpx-md-400 { flex: 0 0 auto; width: 400px; } }');
    expect(styles[1]).to.equal('.enforce-md-size .wpx-md-400, .wpx-400 { flex: 0 0 auto; width: 400px; }');
    expect(styles[2]).to.equal('@media (min-width: 992px) { .wpx-lg-650 { flex: 0 0 auto; width: 650px; } }');
    expect(styles[3]).to.equal('.enforce-lg-size .wpx-lg-650, .wpx-650 { flex: 0 0 auto; width: 650px; }');
    expect(styles[4]).to.equal('.wpx-200 { flex: 0 0 auto; width: 200px; }');
    expect(styles[5]).to.equal('.enforce-sm-size .wpx-200, .wpx-200 { flex: 0 0 auto; width: 200px; }');
  });

  it('generates nothing when there is no row', async function () {
    this.set('value', [{}]);

    await render(hbs`<CustomPageStyle @value={{this.value}} />`);

    const styles = this.element.textContent.trim();

    expect(styles).to.equal('');
  });
});
