/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Service from '@ember/service';
import { service } from '@ember/service';

export default class TrackingService extends Service {
  @service preferences;
  @service fastboot;

  trackers = {};

  registerTracker(name, tracker) {
    if (this.trackers[name]) {
      return;
    }

    this.trackers[name] = tracker;
  }

  addPage() {
    for (const tracker of Object.values(this.trackers)) {
      tracker.addPage?.(...arguments);
    }
  }

  addMapView() {
    for (const tracker of Object.values(this.trackers)) {
      tracker.addMapView?.(...arguments);
    }
  }

  addView() {
    for (const tracker of Object.values(this.trackers)) {
      tracker.addView?.(...arguments);
    }
  }

  addLoad() {
    for (const tracker of Object.values(this.trackers)) {
      tracker.addLoad?.(...arguments);
    }
  }
}
