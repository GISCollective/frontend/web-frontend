/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class InputImageComponent extends Component {
  @tracked rnd;
  @tracked _isLoading;

  get isLoading() {
    if (this._isLoading) {
      return true;
    }

    if (this.args.picture && this.args.picture.progress > 0 && this.args.picture.progress < 100) {
      return true;
    }

    return false;
  }

  @action
  async remove() {
    this._isLoading = true;

    await this.args.onRemove();

    this._isLoading = false;
  }

  @action
  async rotate() {
    this._isLoading = true;
    try {
      await this.args.onRotate();
      // eslint-disable-next-line no-empty
    } catch (err) {}

    await later(() => {
      this.rnd = Math.random();
      this._isLoading = false;
    }, 200);
  }

  @action
  async toggle360() {
    this._isLoading = true;
    if (this.args.onToggle360) {
      await this.args.onToggle360();
    }
    this._isLoading = false;
  }

  get url() {
    if (!this.args.picture) {
      return '';
    }

    const isNew = this.args.picture.get ? this.args.picture?.get('isNew') : this.args.picture?.isNew;

    if (isNew) {
      return this.args.picture.picture;
    }

    const picture = this.args.picture.get ? this.args.picture?.get('picture') : this.args.picture?.picture;

    const size = `${picture}`.indexOf('.jpg') > 0 ? `.sm.jpg` : '/sm';

    return `${picture}${size}?rnd=${this.args.picture.rnd}`;
  }

  get hasActions() {
    if (this.args.disabled) {
      return false;
    }

    const owner = this.args.picture?.get ? this.args.picture?.get('owner') : this.args.picture?.owner;

    return owner != '@system';
  }
}
