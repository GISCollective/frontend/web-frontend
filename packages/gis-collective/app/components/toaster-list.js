/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class ToasterListComponent extends Component {
  @service toaster;

  @action
  close(index) {
    this.toaster.remove(index);
  }
}
