/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { SpaceMenu, SpaceMenuItem, SpaceMenuChildItem } from "../../transforms/space-menu";
import { tracked } from "@glimmer/tracking";
import { A } from "models/lib/array";

export default class InputMenuComponent extends Component {
  @tracked _value = null;

  get value() {
    if (this._value !== null) {
      return this._value;
    }

    if (this.args.value) {
      return new SpaceMenu(this.args.value);
    }

    return new SpaceMenu();
  }

  set value(v) {
    if (this._value === null) {
      this._value = v;
    }
  }

  @action
  setup() {
    this._value = new SpaceMenu(this.args.value);
  }

  @action
  addSubItem(index) {
    const value = this.value;

    value.items[index].dropDown.push(new SpaceMenuChildItem());

    return this.args.onChange?.(value);
  }
  @action
  deleteItem(index) {
    const value = this.value;
    value.items.splice(index, 1);
    this.value = value;

    return this.args.onChange?.(value);
  }

  @action
  dragItemEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;
    const value = this.value;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    value.items = A(sourceList);
    return this.args.onChange?.(value);
  }

  @action
  dragSubitemEnd(index, { sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;
    const value = this.value;

    const item = sourceList[sourceIndex];
    sourceList.splice(sourceIndex, 1);
    sourceList.splice(targetIndex, 0, item);

    value.items[index].dropDown = A(sourceList);
    return this.args.onChange?.(value);
  }

  @action
  addItem() {
    const item = new SpaceMenuItem({});

    const value = this.value;
    value.items.push(item);

    return this.args.onChange?.(value);
  }

  @action
  changeSubitem(index, subindex, item) {
    const value = this.value;
    value.items[index].dropDown[subindex].copyFields(item);
    this.value = value;

    return this.args.onChange?.(value);
  }

  @action
  deleteSubItem(index, subindex) {
    const value = this.value;
    value.items[index].dropDown.splice(subindex, 1);
    this.value = value;

    return this.args.onChange?.(value);
  }

  @action
  updateItemName(index, newName) {
    const value = this.value;
    value.items[index].name = newName;

    this.value = value;

    return this.args.onChange?.(value);
  }

  @action
  changeItem(index, item) {
    const value = this.value;
    value.items[index].copyFields(item);

    this.value = value;

    return this.args.onChange?.(value);
  }
}
