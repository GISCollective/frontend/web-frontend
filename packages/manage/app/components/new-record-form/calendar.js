/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { service } from '@ember/service';
import { set, action } from '@ember/object';
import { toContentBlocks } from 'models/lib/content-blocks';

export default class NewRecordFormCalendarComponent extends Component {
  @service intl;

  eventTypes = ['events', 'schedules'];

  get article() {
    const value = this.intl.t('article-placeholders.add-calendar');

    return toContentBlocks(value);
  }

  get canSubmit() {
    const model = this.args.model;

    return model?.name?.trim?.() && model?.visibility?.team && model.type;
  }

  @action
  save() {
    return this.args.onSubmit(this.args.model);
  }

  @action
  changeArticle(title, body) {
    return this.changeField('article', body);
  }

  @action
  changeField(name, value) {
    if (!this.args.model) {
      return;
    }

    set(this.args.model, name, value);

    if (name == 'visibility.team') {
      this.args.onChangeTeam?.(value);
    }
  }

  @action
  setup() {
    return this.changeField('article', this.article);
  }
}
