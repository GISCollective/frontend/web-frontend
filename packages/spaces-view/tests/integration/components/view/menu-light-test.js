import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import TestServer from 'models/test-support/gis-collective/test-server';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | view/menu-light', function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let pictureData;
  let store;
  let page;
  let space;
  let spaceData;
  let picture;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultIconSet();
    pictureData = server.testData.storage.addDefaultPicture();
    page = server.testData.storage.addDefaultPage();
    spaceData = server.testData.storage.addDefaultSpace();
  });

  hooks.after(function () {
    server.shutdown();
  });

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    const spaceService = this.owner.lookup('service:store');
    picture = await store.findRecord('picture', pictureData._id);

    space = await this.store.findRecord('space', spaceData._id);

    space.logo = picture;
    space.landingPageId = page._id;
    space.landingPagePath = '/';
    this.set('space', space);

    space._getPagesMap = {
      [page._id]: page.slug,
    };
    spaceService.currentSpace = space;
  });

  it('renders the logo with the provided size', async function (a) {
    this.set('value', {
      data: {
        components: {
          showLogo: true,
        },
        logo: {
          style: { classes: ['btn-primary'] },
          pictureOptions: {
            style: {
              lg: { height: '54px' },
              md: { height: '54px' },
              sm: { height: '54px' },
            },
          },
        },
      },
    });

    this.set('model', { space });

    await render(
      hbs`<View::MenuLight @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.logo-container a')).to.have.class(
      'btn-primary',
    );
    expect(
      this.element.querySelector('.logo-container a img').offsetHeight,
    ).to.equal(54);
  });

  it('renders the login button', async function (a) {
    this.set('value', {
      data: {
        components: {
          showLogin: true,
        },
        login: {
          style: { classes: ['btn-primary'] },
        },
      },
    });

    this.set('model', { space });

    await render(
      hbs`<View::MenuLight @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.login-container .btn-primary')).to
      .exist;
  });

  it('renders a button with image', async function () {
    this.set('value', {
      data: {
        components: {
          showLogin: true,
        },
        login: {
          style: { classes: ['btn-primary'] },
        },
        menu: {
          pictures: {
            0: { position: 'start', picture: pictureData._id },
          },
          items: [
            {
              link: { url: 'https://giscollective.com' },
              dropDown: [],
              name: 'test1',
            },
          ],
          styles: {
            0: { classes: ['btn-danger', 'size-btn-sm'] },
          },
        },
      },
    });

    this.set('model', { space });

    await render(
      hbs`<View::MenuLight @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.querySelector('.menu-content a')).to.have.class(
      'btn-danger',
    );
    expect(
      this.element.querySelector('.menu-content a .main-picture'),
    ).to.have.attribute('src', '/test/5d5aa72acac72c010043fb59.jpg');
  });
});
