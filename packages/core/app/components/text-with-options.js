/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TextWithOptionsComponent extends Component {
  elementId = `text-with-options-${guidFor(this)}`;
  @tracked currentElement;
  @tracked _value;
  @tracked isEmpty;

  @action
  checkEmptyValue() {
    if (!this.args.isEditor) {
      return;
    }

    this.isEmpty = this.currentElement.textContent.trim() == '';
  }

  @action
  setup(element) {
    this.currentElement = element;

    const links = [...this.currentElement.querySelectorAll('a')];

    for (const link of links) {
      link.setAttribute('target', '_blank');

      if (this.args.isEditor) {
        link.classList.add('disabled');
      }
    }

    this.checkEmptyValue();
  }

  get value() {
    return this._value ?? this.args.options;
  }

  get classes() {
    const classes = this.value?.classes ?? [];

    return [...classes, `text-${this.color} `];
  }

  get color() {
    return this.value?.color ?? 'dark';
  }

  get headingLevel() {
    return this.args.level ?? this.value?.heading ?? 1;
  }

  get style() {
    const minLines = this.value?.minLines ?? {};

    const result = {};
    const style = this.currentElement ? getComputedStyle?.(this.currentElement) : {};
    const lh = style?.lineHeight ?? '1.5em';

    if (minLines.sm) {
      result.sm = { 'min-height': `calc(${lh} * ${minLines.sm})` };
    }

    if (minLines.md) {
      result.md = { 'min-height': `calc(${lh} * ${minLines.md})` };
    }

    if (minLines.lg) {
      result.lg = { 'min-height': `calc(${lh} * ${minLines.lg})` };
    }

    return result;
  }

  @action
  async updateProps(event) {
    this._value = event.detail.value;

    this.checkEmptyValue();
  }
}
