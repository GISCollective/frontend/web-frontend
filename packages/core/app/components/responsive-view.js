/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { later, cancel } from '@ember/runloop';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { getSizeBsFromWidth } from '../lib/responsive';

export default class ResponsiveViewComponent extends Component {
  @tracked _hasSetup = false;
  @tracked _size;

  @service fastboot;

  oldW = 0;
  oldH = 0;
  resizeTimer = null;
  container = null;

  get hasSetup() {
    if (this.fastboot.isFastBoot) {
      return true;
    }

    return this._hasSetup;
  }

  get size() {
    if (this.fastboot.isFastBoot) {
      return 'lg';
    }

    return this._size;
  }

  get isSmall() {
    return this.size == 'sm';
  }

  spyResizeWithTimer(element) {
    this.oldW = element.offsetWidth;
    this.oldH = element.offsetHeight;
    cancel(this.resizeTimer);
    const _this = this;

    function sizeCheck() {
      _this.resizeTimer = later(() => {
        sizeCheck();
      }, 1000);

      if (_this.oldW == element.offsetWidth && _this.oldH == element.offsetHeight) {
        return;
      }

      _this.oldW = element.offsetWidth;
      _this.oldH = element.offsetHeight;

      _this.onResize();
    }

    _this.resizeTimer = later(() => {
      sizeCheck();
    }, 1000);
  }

  spyResize() {
    try {
      if (this.sizeObserver) {
        this.sizeObserver.disconnect();
      }

      this.sizeObserver = new ResizeObserver(() => {
        window?.requestAnimationFrame(() => {
          this.onResize();
        });
      });
      this.sizeObserver.observe(this.container);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      return this.spyResizeWithTimer(this.container);
    }
  }

  @action
  setupTimer(element) {
    this.container = element;

    this.onResize();
    this.spyResize();

    this._hasSetup = true;
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (this.resizeTimer) {
      cancel(this.resizeTimer);
    }

    this.sizeObserver?.disconnect();
  }

  onResize() {
    this._size = getSizeBsFromWidth(this.container.offsetWidth);
    this.args.onChange?.(this.size);
  }
}
