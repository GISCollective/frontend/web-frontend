/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import BrowseRoute from '../../base/browse';
import { hash } from 'rsvp';
import LazyList from 'models/lib/lazy-list';

export default class BrowseMapsRoute extends BrowseRoute {

  async model(params) {
    const mapsParams = {};

    this.mapsSetup(mapsParams, params);
    let team;
    let editableTeams;

    if (params.team) {
      team = this.store.findRecord('team', params.team);
    }

    if (this.user.id) {
      editableTeams = this.store.query('team', { edit: true });
    }

    const maps = new LazyList('map', mapsParams, this.store);
    await maps.loadNext();

    return hash({
      maps,
      area: params.area,
      editableTeams,
      team,
    });
  }

  afterModel(model) {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    const items = [];

    model.buckets.content.forEach((map) => {
      items.push(
        map.get('cover').catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        })
      );

      items.push(
        map.get('squareCover').catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        })
      );
    });

    return Promise.all(items);
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.setupFilters();
    controller.areaFilter.area = model.area;
    controller.teamFilter.team = model.team;
  }
}
