/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { FontStyles } from 'models/transforms/font-styles';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';

export default class ManageFontStylesComponent extends Component {
  elementId = 'color-palette-' + guidFor(this);
  @tracked previewValue = 'The quick brown fox jumps over the lazy dog';

  @tracked styles = [];

  get hasNoStyles() {
    const styleWithClasses = this.styles.find((a) => a.list.length > 0);

    return !styleWithClasses;
  }

  @action
  async save() {
    const newValue = new FontStyles();

    for (const style of this.styles) {
      newValue[style.name] = style.list;
    }

    const result = await this.args.onSave(newValue);

    this.styles = [];
    this.setup();

    return result;
  }

  @action
  change(name, value) {
    const style = this.styles.find((a) => a.name == name);
    style.list = value;
    style.cls = value.join(' ');
  }

  @action
  cancel() {
    this.setup();

    return this.args.onCancel?.();
  }

  @action
  setup() {
    const styles = [];

    for (const name of FontStyles.styles) {
      const list = this.args.value?.[name] ?? [];

      styles.push({
        name,
        list,
        cls: list.join(' '),
      });
    }

    this.styles = styles;
  }
}
