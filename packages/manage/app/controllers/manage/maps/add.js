/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageMapsAddController extends Controller {
  @service intl;
  @service router;
  @tracked _description;
  @tracked team;

  get breadcrumbs() {
    const pieces = [
      {
        route: 'manage.dashboards.index',
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
    ];

    let team = this.model.map.visibility?.team;

    if (this.team) {
      team = this.team;
    }

    if (team) {
      pieces.push({
        route: 'manage.dashboards.dashboard',
        text: team.name,
        model: team.id,
      });
    }

    pieces.push({
      text: this.intl.t('new map'),
      capitalize: true,
    });

    return pieces;
  }

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-map'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  @action
  changeTeam(team) {
    this.team = team;
    this.model.map.visibility.team = team;
  }

  @action
  changeDescriptionValue(title, description) {
    this.description = description;
  }

  @action
  async save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.map.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];

    this.model.map.description = {
      blocks,
    };

    const result = await this.model.map.save();

    this.router.transitionTo('manage.maps.edit', result.id);
  }
}
