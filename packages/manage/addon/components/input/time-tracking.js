import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputTimeTrackingComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? {};
  }

  get hours() {
    return this.value.hours;
  }

  set hours(value) {
    this.change('hours', value);
  }

  get date() {
    return this.value.date;
  }

  get details() {
    return this.value.details;
  }

  set details(value) {
    this.change('details', value);
  }

  @action
  changeDate(date) {
    return this.change('date', date);
  }

  @action
  change(name, value) {
    if (!this._value) {
      this._value = this.value;
    }

    this._value[name] = value;

    return this.args.onChange?.(this._value);
  }
}
