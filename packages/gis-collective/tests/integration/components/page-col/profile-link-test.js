/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, before, after } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import TestServer from "models/test-support/gis-collective/test-server";

describe ('Integration | Component | view/profile-link',  function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let profile;

  hooks.before(function () {
    server = new TestServer();
    profile = server.testData.storage.addDefaultProfile('1');
  });

  hooks.after(function () {
    server.shutdown();
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<View::ProfileLink />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the profile name and a link when the profile exists', async function () {
    this.set('value', profile._id);

    await render(hbs`<View::ProfileLink @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/browse/profiles/1'
    );
  });

  it('renders the profile id when it does not exist', async function () {
    this.set('value', 'other');

    await render(hbs`<View::ProfileLink @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal('other');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders the profile when it contains an @', async function () {
    this.set('value', '@other');

    await render(hbs`<View::ProfileLink @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal('@other');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders nothing when the value is an empty object', async function () {
    this.set('value', {});

    await render(hbs`<View::ProfileLink @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders the profile when the value has a fullname and id', async function () {
    this.set('value', { id: 'id', fullName: 'fullName' });

    await render(hbs`<View::ProfileLink @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.equal('fullName');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/browse/profiles/id'
    );
  });
});
