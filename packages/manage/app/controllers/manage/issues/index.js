/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class IssuesController extends Controller {
  @service modal;

  get issues() {
    const items = this.model.issues.items.map((item) => {
      if (item.type == 'newImage') {
        item.set('hasImage', true);
      }

      return item;
    });

    return items;
  }

  @action
  async resolve(item) {
    await item
      .resolve()
      .then(() => {
        item.reload();
      })
      .catch((error) => {
        var description = error.message;

        if (error.errors && error.errors.length > 0) {
          description = error.errors[0].description;
        }

        this.modal.alert('Resolve issue error', description);
      });

    this.reloadFeature();
  }

  @action
  async reject(item) {
    await item
      .reject()
      .then(() => {
        item.reload();
      })
      .catch((error) => {
        var description = error.message;

        if (error.errors && error.errors.length > 0) {
          description = error.errors[0].description;
        }

        this.modal.alert('Reject issue error', description);
      });

    this.reloadFeature();
  }

  reloadFeature() {
    this.model.feature.issueCount = this.model.feature.issueCount - 1;
    this.model.feature.reload();
  }
}
