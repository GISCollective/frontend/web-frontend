/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent, fillIn } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/properties/dimension", function (hooks) {
  setupRenderingTest(hooks);

  describe("without a device size", function () {
    describe("using the col type", function () {
      it("renders the right option", async function () {
        this.set("col", { options: ["col-4"] });
        await render(hbs`<Editor::Properties::Dimension @col={{this.col}}/>`);

        expect(this.element.querySelector(".input-dimension-col").value).to.equal("4");
      });

      it("renders the default value when a value for a different device size is set", async function () {
        this.set("col", { options: ["col-md-4"] });
        await render(hbs`<Editor::Properties::Dimension @col={{this.col}}/>`);

        expect(this.element.querySelector(".input-dimension-col").value).to.equal("default");
      });

      it("can change the width", async function () {
        let value;
        let rawValue;
        let unit;

        this.set("change", (v, r, u) => {
          value = v;
          rawValue = r;
          unit = u;
        });

        this.set("col", { options: ["col-4"] });

        await render(hbs`<Editor::Properties::Dimension @col={{this.col}} @onChangeOptions={{this.change}}/>`);

        expect(this.element.querySelector(".input-dimension-col").value).to.equal("4");

        this.element.querySelector(".input-dimension-col").value = "6";
        await triggerEvent(".input-dimension-col", "change");

        expect(value).to.deep.equal(["col-6"]);
        expect(rawValue).to.equal(6);
        expect(unit).to.equal("cols");
      });

      it("can change the type", async function () {
        let value;
        let rawValue;
        let unit;

        this.set("change", (v, r, u) => {
          value = v;
          rawValue = r;
          unit = u;
        });

        this.set("col", { options: ["col-4"] });

        await render(hbs`<Editor::Properties::Dimension @col={{this.col}} @onChangeOptions={{this.change}}/>`);

        this.element.querySelector(".input-dimension-unit").value = "px";
        await triggerEvent(".input-dimension-unit", "change");

        expect(this.element.querySelector(".input-dimension-col")).not.to.exist;
        expect(this.element.querySelector(".input-dimension-numeric")).to.exist;

        await fillIn(".input-dimension-numeric", "123");

        expect(value).to.deep.equal(["wpx-123"]);
        expect(rawValue).to.deep.equal(123);
        expect(unit).to.deep.equal("px");
      });
    });

    describe("using the px type", function () {
      it("renders the right px option", async function () {
        this.set("col", { options: ["wpx-43"] });
        await render(hbs`<Editor::Properties::Dimension @col={{this.col}}/>`);

        expect(this.element.querySelector(".input-dimension-numeric").value).to.equal("43");
      });

      it("renders the default value when a value for a different device size is set", async function () {
        this.set("col", { options: ["wpx-md-4"] });
        await render(hbs`<Editor::Properties::Dimension @col={{this.col}}/>`);

        expect(this.element.querySelector(".input-dimension-col").value).to.equal("default");
      });

      it("can change the type", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { options: ["wpx-4"] });

        await render(hbs`<Editor::Properties::Dimension @col={{this.col}} @onChangeOptions={{this.change}}/>`);

        this.element.querySelector(".input-dimension-unit").value = "cols";
        await triggerEvent(".input-dimension-unit", "change");

        expect(this.element.querySelector(".input-dimension-col")).to.exist;
        expect(this.element.querySelector(".input-dimension-numeric")).not.to.exist;

        this.element.querySelector(".input-dimension-col").value = "6";
        await triggerEvent(".input-dimension-col", "change");

        expect(value).to.deep.equal(["col-6"]);
      });

      it("can delete the value", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { options: ["wpx-4"] });

        await render(hbs`<Editor::Properties::Dimension @col={{this.col}} @onChangeOptions={{this.change}}/>`);

        await fillIn(".input-dimension-numeric", "");
        expect(this.element.querySelector(".input-dimension-numeric").value).to.equal("");

        expect(value).to.deep.equal(["wpx-"]);
      });
    });
  });

  describe("with a md device size", function () {
    describe("using the col type", function () {
      it("renders the right col option", async function () {
        this.set("col", { options: ["col-md-4"] });
        await render(hbs`<Editor::Properties::Dimension @deviceSize="md" @col={{this.col}}/>`);

        expect(this.element.querySelector(".input-dimension-numeric")).not.to.exist;
        expect(this.element.querySelector(".input-dimension-col").value).to.equal("4");
      });

      it("can change the width", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { options: ["col-md-4"] });

        await render(
          hbs`<Editor::Properties::Dimension @deviceSize="md" @col={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        expect(this.element.querySelector(".input-dimension-col").value).to.equal("4");

        this.element.querySelector(".input-dimension-col").value = "6";
        await triggerEvent(".input-dimension-col", "change");

        expect(value).to.deep.equal(["col-md-6"]);
      });

      it("can change the type", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { options: ["col-md-4"] });

        await render(
          hbs`<Editor::Properties::Dimension @deviceSize="md" @col={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        this.element.querySelector(".input-dimension-unit").value = "px";
        await triggerEvent(".input-dimension-unit", "change");

        expect(this.element.querySelector(".input-dimension-col")).not.to.exist;
        expect(this.element.querySelector(".input-dimension-numeric")).to.exist;

        await fillIn(".input-dimension-numeric", "123");

        expect(value).to.deep.equal(["wpx-md-123"]);
      });
    });

    describe("using the px type", function () {
      it("renders the right px option", async function () {
        this.set("col", { options: ["wpx-md-43"] });
        await render(hbs`<Editor::Properties::Dimension @deviceSize="md" @col={{this.col}}/>`);

        expect(this.element.querySelector(".input-dimension-numeric").value).to.equal("43");
      });

      it("renders the default value when a value for a different device size is set", async function () {
        this.set("col", { options: ["wpx-4"] });
        await render(hbs`<Editor::Properties::Dimension @deviceSize="md" @col={{this.col}}/>`);

        expect(this.element.querySelector(".input-dimension-col").value).to.equal("default");
      });

      it("can change the type", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { options: ["wpx-md-4"] });

        await render(
          hbs`<Editor::Properties::Dimension @deviceSize="md" @col={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        this.element.querySelector(".input-dimension-unit").value = "cols";
        await triggerEvent(".input-dimension-unit", "change");

        expect(this.element.querySelector(".input-dimension-col")).to.exist;
        expect(this.element.querySelector(".input-dimension-numeric")).not.to.exist;

        this.element.querySelector(".input-dimension-col").value = "6";
        await triggerEvent(".input-dimension-col", "change");

        expect(value).to.deep.equal(["col-md-6"]);
      });

      it("can set the default value", async function () {
        let value;

        this.set("change", (v) => {
          value = v;
        });

        this.set("col", { options: ["wpx-md-4"] });

        await render(
          hbs`<Editor::Properties::Dimension @deviceSize="md" @col={{this.col}} @onChangeOptions={{this.change}}/>`,
        );

        this.element.querySelector(".input-dimension-unit").value = "cols";
        await triggerEvent(".input-dimension-unit", "change");

        expect(value).to.deep.equal([]);
      });
    });
  });
});
