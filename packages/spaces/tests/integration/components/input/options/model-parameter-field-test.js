/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { click, fillIn, render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";
import TestServer from "models/test-support/gis-collective/test-server";
import { selectSearch, selectChoose } from "ember-power-select/test-support";

describe("Integration | Component | input/options/model-parameter-field", function (hooks) {
  setupRenderingTest(hooks);

  let server;
  let mapData;
  let icon1;
  let icon2;

  hooks.before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam("1");
    server.testData.storage.addDefaultTeam("2");

    server.testData.storage.addDefaultCampaign("1");
    server.testData.storage.addDefaultCampaign("2");
    server.testData.storage.addDefaultCampaign("3");

    server.testData.storage.addDefaultFeature("1");

    mapData = server.testData.storage.addDefaultMap();

    icon1 = server.testData.storage.addDefaultIcon();

    icon2 = server.testData.storage.addDefaultIcon("1");
    icon2.name = "test icon";
    icon2.localName = "test icon";
  });

  hooks.after(function () {
    server.shutdown();
  });

  it("sets the ignored option by default", async function () {
    await render(hbs`<Input::Options::ModelParameterField />`);

    expect(this.element.querySelector(".select-parameter-source").value).to.equal("ignored");
  });

  it("does not show the use current model option by default", async function () {
    await render(hbs`<Input::Options::ModelParameterField />`);

    const options = [...this.element.querySelectorAll(".select-parameter-source option")].map((a) => a.value);

    expect(options).to.deep.equal(["ignored", "default", "state", "fixed"]);
  });

  it("shows the use current model when the selected model = field model name", async function () {
    await render(hbs`<Input::Options::ModelParameterField @selectedModel="map" @modelName="feature" @field="map" />`);

    const options = [...this.element.querySelectorAll(".select-parameter-source option")].map((a) => a.value);

    expect(options).to.deep.equal(["ignored", "selected model", "default", "state", "fixed"]);
  });

  it("sets the selected model in the select when @value=selected-model", async function () {
    await render(
      hbs`<Input::Options::ModelParameterField @value="selected-model" @selectedModel="map" @modelName="feature" @field="map" />`,
    );

    expect(this.element.querySelector(".select-parameter-source").value).to.equal("selected model");
  });

  it("sets the state value in the select when @value=state-map", async function () {
    await render(
      hbs`<Input::Options::ModelParameterField @value="state-map" @selectedModel="map" @modelName="feature" @field="map" />`,
    );

    expect(this.element.querySelector(".select-parameter-source").value).to.equal("state");
  });

  it("sets the map fixed id for a feature map", async function (a) {
    this.set("value", mapData._id);

    await render(hbs`<Input::Options::ModelParameterField @value={{this.value}} @modelName="feature" @field="map" />`);

    expect(this.element.querySelector(".select-parameter-source").value).to.equal("fixed");

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal(
      "Harta Verde București",
    );
  });

  it("can change to the selected model value", async function (a) {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ModelParameterField @onChange={{this.onChange}} @value={{this.value}} @selectedModel="map" @modelName="map" @field="map" />`,
    );

    this.element.querySelector(".select-parameter-source").value = "selected model";
    await triggerEvent(".select-parameter-source", "change");

    a.deepEqual(value, {
      source: "selected-model",
      value: "",
    });
  });

  it("can change to the state value", async function (a) {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ModelParameterField @onChange={{this.onChange}} @value={{this.value}} @selectedModel="map" @modelName="map" @field="map" />`,
    );

    this.element.querySelector(".select-parameter-source").value = "state";
    await triggerEvent(".select-parameter-source", "change");

    a.deepEqual(value, { source: "state", value: "", state: "map" });
  });

  it("can change to an record id", async function (a) {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ModelParameterField @onChange={{this.onChange}} @value={{this.value}} @selectedModel="map" @modelName="feature" @field="map" />`,
    );

    this.element.querySelector(".select-parameter-source").value = "fixed";
    await triggerEvent(".select-parameter-source", "change");

    await selectSearch(".model-parameter-record-id", mapData.name);
    await selectChoose(".model-parameter-record-id", mapData.name);

    a.deepEqual(value, {
      source: "fixed",
      value: "5ca89e37ef1f7e010007f54c",
    });
  });

  it("can change to a text value", async function (a) {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ModelParameterField @onChange={{this.onChange}} @value={{this.value}} @selectedModel="map" @modelName="icon" @field="category" />`,
    );

    this.element.querySelector(".select-parameter-source").value = "fixed";
    await triggerEvent(".select-parameter-source", "change");

    await fillIn(".model-text", "some category");

    a.deepEqual(value, { source: "fixed", value: "some category" });
  });

  it("can change to an id list value", async function (a) {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ModelParameterField @onChange={{this.onChange}} @value={{this.value}} @selectedModel="map" @modelName="feature" @field="icons" />`,
    );

    this.element.querySelector(".select-parameter-source").value = "fixed";
    await triggerEvent(".select-parameter-source", "change");

    await click(".btn-add-item");
    await click(".btn-add-item");

    await selectSearch(".model-id-0", icon1.name);
    await selectChoose(".model-id-0", icon1.name);

    await selectSearch(".model-id-1", icon2.name);
    await selectChoose(".model-id-1", icon2.name);

    a.deepEqual(value, { source: "fixed", value: "5ca7bfc0ecd8490100cab980,1" });
  });

  it("can change to an options value", async function (a) {
    let value;
    this.set("onChange", (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ModelParameterField @onChange={{this.onChange}} @value={{this.value}} @selectedModel="article" @modelName="article" @field="sortBy" />`,
    );

    this.element.querySelector(".select-parameter-source").value = "fixed";
    await triggerEvent(".select-parameter-source", "change");

    this.element.querySelector(".model-options").value = "order";
    await triggerEvent(".model-options", "change");

    a.deepEqual(value, { source: "fixed", value: "order" });
  });
});
