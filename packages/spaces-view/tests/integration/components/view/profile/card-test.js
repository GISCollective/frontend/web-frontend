/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from 'models/test-support/gis-collective/test-server';

describe('Integration | Component | view/profile/card', function (hooks) {
  setupRenderingTest(hooks);
  let server;
  let store;
  let profileData;

  hooks.before(function () {
    server = new TestServer();
  });

  hooks.after(function () {
    server.shutdown();
  });

  let featureData;

  hooks.beforeEach(async function () {
    store = this.owner.lookup('service:store');
    server.testData.storage.addDefaultPreferences();
    const pictureData = server.testData.storage.addDefaultPicture();

    profileData = server.testData.storage.addDefaultProfile();
    profileData.picture = pictureData._id;
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<View::Profile::Card />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when the profile of the original author is not set', async function () {
    featureData = server.testData.storage.addDefaultFeature();
    featureData.info = {};

    const feature = await store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {},
      modelKey: 'feature',
    });

    this.set('model', { feature });

    await render(
      hbs`<View::Profile::Card @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when the profile of the original author can not be found', async function () {
    featureData = server.testData.storage.addDefaultFeature();
    featureData.info = { originalAuthor: 'missing' };

    const feature = await store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {},
      modelKey: 'feature',
    });

    this.set('model', { feature });

    await render(
      hbs`<View::Profile::Card @value={{this.value}} @model={{this.model}} />`,
    );

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the original author profile for a feature', async function () {
    featureData = server.testData.storage.addDefaultFeature();

    const feature = await store.findRecord('feature', featureData._id);

    this.set('value', {
      data: {},
      modelKey: 'feature',
    });

    this.set('model', { feature });

    await render(
      hbs`<View::Profile::Card @value={{this.value}} @model={{this.model}} />`,
    );
    expect(this.element.querySelector('img')).to.have.attribute(
      'src',
      '/test/5d5aa72acac72c010043fb59.jpg.sm.jpg',
    );
    expect(
      this.element.querySelector('.profile-name').textContent.trim(),
    ).to.equal('mr Bogdan Szabo');
    expect(
      this.element.querySelector('.profile-description').textContent.trim(),
    ).to.equal('This is me');
  });
});
