import { module, test } from 'qunit';
import { setupRenderingTest, it, describe } from 'dummy/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

module('Integration | Component | manage/survey-notifications', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders an info message when there are no notifications', async function () {
      await render(hbs`<Manage::SurveyNotifications  @title="notifications" />`);
      expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
        'The survey has no notifications set up.',
      );
    });

    it('renders a link to the notification when one is set', async function () {
      this.set('value', {
        add: '000000000000000000000015',
      });
      await render(hbs`<Manage::SurveyNotifications @value={{this.value}} @title="notifications" />`);
      expect(this.element.querySelector('p').textContent.trim()).to.contain(
        'Authenticated contributors will receive a notification on add.',
      );
      expect(this.element.querySelector('p').textContent.trim()).to.contain('edit notification.');
    });
  });

  describe('in edit mode', function (hooks) {
    it('renders an unchecked checkbox when the notifications are not set', async function () {
      await render(
        hbs`<Manage::SurveyNotifications @value={{this.value}} @title="notifications" @editablePanel="notifications" />`,
      );

      expect(this.element.querySelector('.btn-switch')).to.exist;
      expect(this.element.querySelector('.btn-switch').checked).to.equal(false);
    });

    it('renders an checked checkbox when the notifications are set', async function () {
      this.set('value', {
        add: '000000000000000000000015',
      });
      await render(
        hbs`<Manage::SurveyNotifications @value={{this.value}} @title="notifications" @editablePanel="notifications" />`,
      );

      expect(this.element.querySelector('.btn-switch')).to.exist;
      expect(this.element.querySelector('.btn-switch').checked).to.equal(true);
    });

    it('triggers on cancel when saving without a change', async function () {
      this.set('value', {
        add: '000000000000000000000015',
      });
      let saved = false;
      let canceled = false;

      this.set('save', () => {
        saved = true;
      });

      this.set('cancel', () => {
        canceled = true;
      });

      await render(
        hbs`<Manage::SurveyNotifications @value={{this.value}} @onSave={{this.save}} @onCancel={{this.cancel}} @title="notifications" @editablePanel="notifications" />`,
      );

      await click('.btn-submit');

      expect(saved).to.equal(false);
      expect(canceled).to.equal(true);
    });

    it('triggers on cancel when the checkbox is updated twice', async function () {
      this.set('value', {
        add: '000000000000000000000015',
      });
      let saved = false;
      let canceled = false;

      this.set('save', () => {
        saved = true;
      });

      this.set('cancel', () => {
        canceled = true;
      });

      await render(
        hbs`<Manage::SurveyNotifications @value={{this.value}} @onSave={{this.save}} @onCancel={{this.cancel}} @title="notifications" @editablePanel="notifications" />`,
      );

      await click('.btn-switch');
      await click('.btn-switch');
      await click('.btn-submit');

      expect(saved).to.equal(false);
      expect(canceled).to.equal(true);
    });

    it('triggers on save when the checkbox is updated once', async function () {
      this.set('value', {
        add: '000000000000000000000015',
      });
      let savedValue;
      let canceled = false;

      this.set('save', (v) => {
        savedValue = v;
      });

      this.set('cancel', () => {
        canceled = true;
      });

      await render(
        hbs`<Manage::SurveyNotifications @value={{this.value}} @onSave={{this.save}} @onCancel={{this.cancel}} @title="notifications" @editablePanel="notifications" />`,
      );

      await click('.btn-switch');
      await click('.btn-submit');

      expect(savedValue).to.equal(false);
      expect(canceled).to.equal(false);
    });
  });
});
