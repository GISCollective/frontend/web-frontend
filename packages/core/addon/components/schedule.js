/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';

import { scheduleGroup, beginSchedule, endSchedule } from 'models/transforms/calendar-entry-list';
import { DateTime } from 'luxon';

export default class ScheduleComponent extends Component {
  get begin() {
    return beginSchedule(this.args.value).toLocaleString();
  }

  get timezone() {
    const here = DateTime.now().zoneName;
    const tz = beginSchedule(this.args.value)?.zoneName;

    if (here == tz) {
      return '';
    }

    return tz;
  }

  get hasInterval() {
    return endSchedule(this.args.value).year < 2050;
  }

  get end() {
    return endSchedule(this.args.value).toLocaleString();
  }

  get days() {
    return scheduleGroup(this.args.value);
  }
}
