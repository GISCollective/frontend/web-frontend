/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, setupRenderingTest } from 'ogm/tests/helpers';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | model-actions/data-binding',  function (hooks) {
  setupRenderingTest(hooks);

  it('renders', async function () {
    this.set('record', {
      id: 'id',
      canEdit: true,
    });

    await render(hbs`<ModelActions::DataBinding @record={{this.record}} />`);

    expect(this.element.textContent.trim()).to.contain('Edit');
    expect(this.element.textContent.trim()).to.contain('Analyze');
    expect(this.element.textContent.trim()).to.contain('Trigger');
  });

  it('can trigger the job', async function () {
    let called;
    this.set('dataBinding', {
      id: 'some-id',

      handleTrigger: () => {
        called = true;
      },
    });

    this.set('onDelete', () => true);

    await render(
      hbs`<ModelActions::DataBinding @record={{this.dataBinding}} />`
    );

    await click('.btn-trigger');

    expect(called).to.equal(true);
  });

  it('can analyze the binding', async function () {
    let called;
    this.set('dataBinding', {
      id: 'some-id',

      handleAnalyze: () => {
        called = true;
      },
    });

    this.set('onDelete', () => true);

    await render(
      hbs`<ModelActions::DataBinding @record={{this.dataBinding}} />`
    );

    await click('.btn-analyze');

    expect(called).to.equal(true);
  });
});
