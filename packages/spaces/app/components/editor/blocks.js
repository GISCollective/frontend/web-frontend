/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { toContentBlocks } from "models/lib/content-blocks";
import { guidFor } from "@ember/object/internals";
import { tracked } from "@glimmer/tracking";

export default class ManageEditorsBlocksComponent extends Component {
  @tracked _value;

  elementId = `manage-editors-value-${guidFor(this)}`;

  @action
  reset() {
    this._value = undefined;
  }

  change() {
    if (this.args.isSimple) {
      return this.args.onChange?.({ blocks: this.value.blocks });
    }

    this.args.onChange?.(this.value.blocks);
  }

  get hasAdvancedOptions() {
    return !!this.args.value?.data;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    if (typeof this.args.value == "string") {
      return toContentBlocks(this.args.value);
    }

    if (Array.isArray(this.args.value)) {
      return {
        blocks: this.args.value ?? [],
      };
    }

    return this.args.value;
  }

  @action
  blocksChanged(title, value) {
    this._value = value;
    this.change();
  }
}
