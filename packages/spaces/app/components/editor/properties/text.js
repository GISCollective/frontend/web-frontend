/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { guidFor } from "@ember/object/internals";

export default class ManageEditorsPropertiesTextComponent extends Component {
  elementId = `text-${guidFor(this)}`;

  @tracked _classes;
  @tracked _color;
  @tracked _text;
  @tracked _minLines;

  get minLines() {
    if (this._minLines !== undefined) {
      return this._minLines;
    }

    return {
      sm: 0,
      md: 0,
      lg: 0,
      ...(this.args.value?.minLines ?? {}),
    };
  }

  get text() {
    if (this._text !== undefined) {
      return this._text;
    }

    return this.args.value?.text ?? "";
  }

  set text(value) {
    this._text = value;
    this.triggerChange();
  }

  get color() {
    if (this._color) {
      return this._color;
    }

    return this.args.value?.color ?? "";
  }

  get classes() {
    if (this._classes) {
      return this._classes;
    }

    return this.args.value?.classes ?? [];
  }

  get heading() {
    if (this._heading) {
      return this._heading;
    }

    return this.args.value?.heading ?? "1";
  }

  @action
  changeHeading(value) {
    this._heading = value;

    this.triggerChange();
  }

  @action
  changeMinLines(size, value) {
    if (isNaN(value)) {
      return;
    }

    if (this._minLines === undefined) {
      this._minLines = this.minLines;
    }

    this._minLines = {
      ...this.minLines,
      [size]: parseInt(value),
    };

    this.triggerChange();
  }

  @action
  changeClasses(value) {
    this._classes = value;

    this.triggerChange();
  }

  @action
  changeColor(value) {
    this._color = value;

    this.triggerChange();
  }

  triggerChange() {
    const newOptions = { color: this.color, classes: this.classes, minLines: this.minLines };

    if (this.args.enableTextValue) {
      newOptions.text = this.text;
    }

    if (this.args.isHeading) {
      newOptions.heading = this.heading;
    }

    return this.args.onChange?.(newOptions);
  }
}
