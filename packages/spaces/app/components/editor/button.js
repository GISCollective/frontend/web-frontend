/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { buildWaiter } from "@ember/test-waiters";
import { later } from "@ember/runloop";

let waiter = buildWaiter("dom-changed-waiter");

export default class ManageEditorsButtonComponent extends Component {
  @tracked _content;
  @tracked _style;
  @tracked _containerStyle;
  @tracked _pictureOptions;

  get model() {
    return this.args.element?.attributes?.getNamedItem?.("data-model")?.value;
  }

  get modelId() {
    return this.args.element?.attributes?.getNamedItem?.("data-model-id")?.value;
  }

  get value() {
    let result = {};

    if (typeof this.args.element == "object") {
      result = this.args.value ?? {};

      if (!result.content) {
        result.content = {
          name: result.name,
          type: result.type,
          link: result.link,
        };
      }

      if (!result.style) {
        result.style = {
          classes: result.classes,
        };
      }
    } else {
      result = this.args.value?.data ?? {};
    }

    return result;
  }

  get content() {
    if (this._content) {
      return this._content;
    }

    return this.value.content ?? {};
  }

  get style() {
    if (this._style) {
      return this._style;
    }

    return this.value.style ?? {};
  }

  get containerStyle() {
    if (this._containerStyle) {
      return this._containerStyle;
    }

    return this.value.containerStyle ?? {};
  }

  get pictureOptions() {
    if (this._pictureOptions) {
      return this._pictureOptions;
    }

    return this.value.pictureOptions ?? {};
  }

  get isAction() {
    return this.args.element?.dataset?.noLink == "true";
  }

  get hasPicture() {
    return this.content.enablePicture;
  }

  triggerChange() {
    return this.args.onChange?.({
      content: this.content,
      style: this.style,
      containerStyle: this.containerStyle,
      pictureOptions: this.pictureOptions,
    });
  }

  @action
  changePictures(value) {
    this._pictureOptions = value;
    return this.triggerChange();
  }

  @action
  changeContent(value) {
    let token = waiter.beginAsync();

    later(async () => {
      this._content = value;

      await this.triggerChange();

      later(() => {
        waiter.endAsync(token);
      });
    });
  }

  @action
  changeStyleClasses(value) {
    this._style = this.style;
    this._style.classes = value;

    return this.triggerChange();
  }

  @action
  changeContainerStyleClasses(value) {
    this._containerStyle = this.containerStyle;
    this._containerStyle.classes = value;

    return this.triggerChange();
  }
}
