/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { Tooltip } from 'bootstrap';
import { toPageState } from '../lib/page-state';

export default class IconComponent extends Component {
  @service fastboot;
  @tracked resolvedIcon;
  @tracked isLoading = true;
  @tracked isError = true;

  @action
  setupTooltip(element) {
    if (this.fastboot.isFastBoot || this.args.tooltip === false) {
      return;
    }

    this.tooltip = new Tooltip(element, {
      title: () => {
        return this.localName;
      },
    });
  }

  @action
  destroyTooltip() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.tooltip?.dispose();
  }

  @action
  async setup() {
    this.isLoading = true;
    this.isError = false;

    try {
      this.resolvedIcon = await this.args.src;
    } catch (err) {
      this.isError = true;
    }

    if (!this.resolvedIcon?.image?.value) {
      this.isError = true;
    }

    this.isLoading = false;
  }

  get hasIconVariable() {
    const destination = this.args.options?.destination;

    if (!destination || typeof destination != 'object') {
      return false;
    }

    return !!Object.values(destination).find((a) => a.indexOf(':icon-id') != -1);
  }

  get link() {
    return this.args.options?.destination;
  }

  get query() {
    if (this.hasIconVariable) {
      return null;
    }

    return 's=' + toPageState({ if: this.icon.id });
  }

  get variables() {
    return {
      icon: this.icon.id,
    };
  }

  get hasIcon() {
    return !this.isLoading && !this.isError && !!this.resolvedIcon;
  }

  get image() {
    if (!this.hasIcon) {
      return null;
    }

    return this.icon.image;
  }

  get name() {
    if (!this.hasIcon) {
      return '';
    }

    return this.icon.localName;
  }

  get icon() {
    if (this.resolvedIcon) {
      return this.resolvedIcon;
    }

    return this.args.src;
  }
}
