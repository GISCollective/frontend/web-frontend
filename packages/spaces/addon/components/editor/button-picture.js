import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";

export default class EditorButtonPicture extends Component {
  @tracked _value;

  get value() {
    return this._value ?? this.args.value;
  }

  get model() {
    return this.args.element?.attributes?.getNamedItem?.("data-model")?.value;
  }

  get modelId() {
    return this.args.element?.attributes?.getNamedItem?.("data-model-id")?.value;
  }
}
