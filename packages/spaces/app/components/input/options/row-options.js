/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class InputRowOptionsComponent extends Component {
  @tracked _options;

  get options() {
    return this._options || this.args.row?.options || [];
  }

  @action
  onChangeOptions(options) {
    this._options = options;

    this.args.onChangeOptions?.(this.options);
  }
}
