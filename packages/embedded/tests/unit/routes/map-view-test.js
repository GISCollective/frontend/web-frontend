import { module, test } from 'qunit';
import { setupTest } from 'embedded/tests/helpers';

module('Unit | Route | map-view', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:map-view');
    assert.ok(route);
  });
});
