/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputManageLicenseComponent extends Component {
  @tracked _name;
  @tracked _url;

  elementId = 'input-license-' + guidFor(this);

  get name() {
    if (typeof this._name == 'string') {
      return this._name;
    }

    if (!this.args.value || !this.args.value.name) {
      return '';
    }

    return this.args.value.name;
  }

  set name(value) {
    this._name = value;
  }

  get url() {
    if (typeof this._url == 'string') {
      return this._url;
    }

    if (!this.args.value || !this.args.value.url) {
      return '';
    }

    return this.args.value.url;
  }

  set url(value) {
    this._url = value;
  }

  @action
  save() {
    const newValue = {
      name: this.name,
      url: this.url,
    };

    this._name = null;
    this._value = null;

    if (this.args.onSave) {
      return this.args.onSave(newValue);
    }
  }

  @action
  cancel() {
    this._name = null;
    this._value = null;

    if (this.args.onCancel) {
      return this.args.onCancel();
    }
  }
}
