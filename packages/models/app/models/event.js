/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import PrimaryModel from './base/primary-model';
import { attr, belongsTo, hasMany } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from 'models/lib/content-blocks';

export default class CalendarModel extends PrimaryModel {
  @attr('string') name;

  @attr article;

  @belongsTo('calendar', { async: true, inverse: null }) calendar;
  @hasMany('icons', { async: true, inverse: null }) icons;

  @belongsTo('picture', { async: true, inverse: null })
  cover;

  @attr('calendar-entry-list') entries;
  @attr('event-location') location;

  @attr('time-interval') nextOccurrence;
  @attr('time-interval-list') allOccurrences;

  @attr attributes;

  get pictures() {
    if (!this.cover) {
      return [];
    }

    if (!this.cover.then) {
      return [this.cover];
    }

    return this.cover.then((cover) => [cover]);
  }

  get relatedModels() {
    return ['feature'];
  }

  get relatedIdsMap() {
    if (!this.calendar || (this.calendar.then && !this.calendar.content)) {
      return {};
    }

    const result = {
      calendar: this.calendar.get('id'),
      team: this.visibility?.teamId,
    };

    if (this.location?.type == 'Feature') {
      result['feature'] = this.location.value;
    }

    return result;
  }

  get parentCover() {
    return this.calendar?.get('cover');
  }

  get title() {
    return this.name;
  }

  get contentBlocks() {
    return toContentBlocks(this.article, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter((a) => !(a.type == 'header' && a.data.level == 1));

    return { blocks };
  }

  set descriptionContentBlocks(content) {
    this.description = toContentBlocks(content, this.name);
  }

  get articleContentBlocks() {
    return toContentBlocks(this.article);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  get categories() {
    return [this.calendar.get('id'), this.calendar.get('name')];
  }

  get cachedIcons() {
    return this.hasMany("icons").value();
  }

  replaceRelatedVar(v, path) {
    if (v == ':feature-id' && this.location.type == 'Feature') {
      path = path.replace(':feature-id', this.location.value);
    }

    return path;
  }

  async loadRelations() {
    await this.icons;
    await this.calendar;
    await this.calendar.get('cover');
    await this.cover;
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;

    try {
      const cover = await this.cover;
      metaInfo.imgSrc = cover?.picture;
      if (metaInfo.imgSrc) {
        metaInfo.imgSrc += '/lg';
      }
    } catch (err) {
      console.error(err);
    }

    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }
}
