/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Transform from '@ember-data/serializer/transform';
import { getOwner } from '@ember/application';
import { tracked } from '@glimmer/tracking';

export default class ReviewTransform extends Transform {
  deserialize(serialized) {
    let applicationInstance = getOwner(this);
    const store = applicationInstance.lookup('service:store');

    return new Review(serialized, store);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class Review {
  @tracked userId;
  @tracked user;
  @tracked profile;
  @tracked reviewedOn;

  constructor(value, store) {
    this.store = store;

    if (!value) {
      return;
    }

    if (value.reviewedOn) {
      this.reviewedOn = new Date(value.reviewedOn);
    }

    if (value.user) {
      this.userId = value.user;
    }
  }

  async fetch() {
    if (!this.userId || !this.store) {
      return;
    }

    this.user = this.store.peekRecord('user', this.userId);

    if (!this.user) {
      this.user = await this.store.findRecord('user', this.userId);
    }

    this.profile = this.store.peekRecord('user-profile', this.userId);

    if (!this.profile) {
      this.profile = await this.store.findRecord('user-profile', this.userId);
    }
  }

  toJSON() {
    const value = {};

    if (this.userId) {
      value.user = this.userId;
    }

    if (this.user) {
      value.user = this.user.id;
    }

    if (this.reviewedOn) {
      value.reviewedOn = this.reviewedOn.toISOString();
    }

    return value;
  }
}
