/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from "chai";
import { describe, it, setupRenderingTest } from "dummy/tests/helpers";
import { render, triggerEvent } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

describe("Integration | Component | editor/options", function (hooks) {
  setupRenderingTest(hooks);

  it("renders a value", async function () {
    this.set("options", { values: ["value1", "value2", "value3"] });
    this.set("value", "value1");

    await render(hbs`<Editor::Options @value={{this.value}} @options={{this.options}} />`);

    expect(this.element.querySelector("select").value).to.equal("value1");
  });

  it("can change a value", async function () {
    this.set("options", { values: ["value1", "value2", "value3"] });
    this.set("value", "value1");

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Editor::Options @value={{this.value}} @options={{this.options}} @onChange={{this.change}} />`);

    const select = this.element.querySelector("select");

    select.value = "value2";
    await triggerEvent(select, "change");

    expect(value).to.equal("value2");
  });
});
