import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { later } from '@ember/runloop';

export default class InputStreamDischarge extends Component {
  @tracked velocity;
  @tracked width;
  @tracked depth;

  get streamDischarge() {
    let result = parseFloat(this.velocity) * parseFloat(this.width) * parseFloat(this.depth);

    if (isNaN(result)) {
      return 0;
    }

    return result;
  }

  @action
  setup() {
    if (this.velocity != this.args.value?.data?.velocity) {
      later(() => {
        this.velocity = this.args.value?.data?.velocity ?? 0;
      });
    }

    if (this.width != this.args.value?.data?.width) {
      later(() => {
        this.width = this.args.value?.data?.width ?? 0;
      });
    }

    if (this.depth != this.args.value?.data?.depth) {
      later(() => {
        this.depth = this.args.value?.data?.depth ?? 0;
      });
    }
  }

  @action
  focusIn() {
    return this.args.onFocusIn?.();
  }

  @action
  focusOut() {
    return this.args.onFocusOut?.();
  }

  @action
  changeInput() {
    const newValue = {
      type: 'stream discharge',
      data: {
        velocity: parseFloat(this.velocity),
        width: parseFloat(this.width),
        depth: parseFloat(this.depth),
        result: this.streamDischarge,
      },
    };

    return this.args.onChange?.(newValue);
  }
}
