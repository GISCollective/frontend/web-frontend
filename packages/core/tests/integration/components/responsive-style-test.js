/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupRenderingTest, describe, it } from 'dummy/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | responsive-style', function (hooks) {
  setupRenderingTest(hooks);

  it('renders all styles when the device size is not set', async function (a) {
    this.set('value', {
      sm: {
        'font-size': '1px',
      },
      md: {
        'font-size': '2px',
      },
      lg: {
        'font-size': '3px',
      },
    });

    await render(hbs`<ResponsiveStyle @selector="#id" @style={{this.value}}/>`);
    expect(this.element.textContent.trim()).to.contain('#id {font-size: 1px}');
    expect(this.element.textContent.trim()).to.contain('#id {font-size: 2px}');
    expect(this.element.textContent.trim()).to.contain('#id {font-size: 3px}');
  });

  it('renders only the md size when the device size is md', async function () {
    this.set('value', {
      sm: {
        'font-size': '1px',
      },
      md: {
        'font-size': '2px',
      },
      lg: {
        'font-size': '3px',
      },
    });

    await render(hbs`<ResponsiveStyle @selector="#id" @deviceSize="md" @style={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.contain('#id {font-size: 2px}');

    expect(this.element.textContent.trim()).not.to.contain('#id {font-size: 1px}');
    expect(this.element.textContent.trim()).not.to.contain('#id {font-size: 3px}');
  });
});
