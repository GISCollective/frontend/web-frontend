/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it, beforeEach } from 'ogm/tests/helpers';
import { setupRenderingTest } from 'ogm/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe ('Integration | Component | browse/campaign/card',  function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };
  });

  it('renders', async function () {
    await render(hbs`<Browse::Campaign::Card />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the campaign title', async function () {
    this.set('campaign', {
      id: 'some-id',
      name: 'some name',
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.name').textContent.trim()).to.equal(
      'some name'
    );
  });

  it('renders the campaign picture', async function () {
    this.set('campaign', {
      id: 'some-id',
      cover: {
        picture: 'some-picture',
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.picture-bg')).to.have.attribute(
      'style',
      'background-image: url(some-picture/lg);aspect-ratio: 1;'
    );
  });

  it('does not render the description by default', async function () {
    this.set('campaign', {
      id: 'some-id',
      cover: {
        picture: 'some-picture',
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.card-description')).not.to.exist;
  });

  it('shows only the description when the option has description true and other false', async function () {
    this.set('campaign', {
      id: 'some-id',
      cover: {
        picture: 'some-picture',
      },
    });

    this.set('value', {
      data: {
        components: { cover: false, name: false, description: true },
      },
    });

    await render(
      hbs`<Browse::Campaign::Card @value={{this.value}} @campaign={{this.campaign}} />`
    );

    expect(this.element.querySelector('.card-description')).to.exist;
    expect(this.element.querySelector('.cpicture-bg')).not.to.exist;
    expect(this.element.querySelector('.name')).not.to.exist;
  });

  it('does not render the campaign options', async function () {
    this.set('campaign', {
      id: 'some-id',
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.campaign-options')).not.to.exist;
  });

  it('renders the campaign options when the campaign is editable', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.campaign-options')).to.exist;
  });

  it('allows to delete an editable campaign', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
    });

    let deletedCampaign;
    this.set('onDelete', (val) => {
      deletedCampaign = val;
    });

    await render(
      hbs`<Browse::Campaign::Card @campaign={{this.campaign}} @onDelete={{this.onDelete}} />`
    );

    await click('.campaign-options');
    await click('.btn-delete');

    expect(deletedCampaign).deep.equal({
      id: 'some-id',
      canEdit: true,
    });
  });

  it('allows to edit an editable campaign', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
      constructor: {
        modelName: 'campaign',
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    await click('.campaign-options');
    expect(this.element.querySelector('.dropdown-item-edit')).to.exist;
  });

  it('shows an unpublished icon when the campaign is not public', async function () {
    this.set('campaign', {
      id: 'some-id',
      visibility: {
        isPublic: false,
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.icon-not-published')).to.exist;
  });

  it('does not show an unpublished icon when the campaign is public', async function () {
    this.set('campaign', {
      id: 'some-id',
      visibility: {
        isPublic: true,
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.icon-not-published')).not.to.exist;
  });
});
