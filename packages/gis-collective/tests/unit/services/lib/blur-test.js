/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'ogm/tests/helpers';
import { setupTest } from 'ogm/tests/helpers';
import { decode } from 'blurhash';
import { getRoundedPercentageOf, pixelsToCss } from 'ogm/lib/blur';

describe ('Unit | Lib | blur',  function (hooks) {
  setupTest(hooks);

  describe ('pixelsToCss',  function (hooks) {
    it('can convert to ', function () {
      const hash = 'eCF6B#-:0JInxr?@s;nmIoWUIko1%NocRk.8xbIUaxR*^+s;RiWAWU';
      const pixels = decode(hash, 10, 10, 1);
      const result = pixelsToCss(pixels, 10, 10);

      expect(result).to.deep.equal({
        'background-image':
          'linear-gradient(90deg,rgb(143,156,98) 10%,rgb(152,161,113) 10% 20%,rgb(170,173,140) 20% 30%,rgb(182,183,156) 30% 40%,rgb(183,186,156) 40% 50%,rgb(173,181,143) 50% 60%,rgb(160,171,127) 60% 70%,rgb(147,160,113) 70% 80%,rgb(138,152,103) 80% 90%,rgb(134,149,97) 90% 100%),linear-gradient(90deg,rgb(132,147,87) 10%,rgb(140,151,101) 10% 20%,rgb(156,161,125) 20% 30%,rgb(168,170,141) 30% 40%,rgb(171,174,142) 40% 50%,rgb(162,170,131) 50% 60%,rgb(148,160,115) 60% 70%,rgb(134,148,101) 70% 80%,rgb(126,141,92) 80% 90%,rgb(125,139,88) 90% 100%),linear-gradient(90deg,rgb(108,128,61) 10%,rgb(113,130,73) 10% 20%,rgb(125,136,93) 20% 30%,rgb(139,144,109) 30% 40%,rgb(146,150,113) 40% 50%,rgb(141,147,107) 50% 60%,rgb(125,136,92) 60% 70%,rgb(106,123,77) 70% 80%,rgb(101,117,70) 80% 90%,rgb(109,120,71) 90% 100%),linear-gradient(90deg,rgb(103,118,57) 10%,rgb(104,119,66) 10% 20%,rgb(113,122,86) 20% 30%,rgb(128,132,103) 30% 40%,rgb(141,140,113) 40% 50%,rgb(139,140,111) 50% 60%,rgb(123,127,99) 60% 70%,rgb(101,111,82) 70% 80%,rgb(96,105,73) 80% 90%,rgb(108,111,73) 90% 100%),linear-gradient(90deg,rgb(120,126,82) 10%,rgb(123,127,93) 10% 20%,rgb(133,133,115) 20% 30%,rgb(149,145,135) 30% 40%,rgb(162,156,145) 40% 50%,rgb(162,156,143) 50% 60%,rgb(146,143,131) 60% 70%,rgb(126,125,114) 70% 80%,rgb(116,115,100) 80% 90%,rgb(122,119,93) 90% 100%),linear-gradient(90deg,rgb(132,134,100) 10%,rgb(137,136,112) 10% 20%,rgb(150,145,137) 20% 30%,rgb(167,159,158) 30% 40%,rgb(178,171,168) 40% 50%,rgb(178,172,166) 50% 60%,rgb(164,159,154) 60% 70%,rgb(144,139,135) 70% 80%,rgb(129,126,117) 80% 90%,rgb(126,124,104) 90% 100%),linear-gradient(90deg,rgb(122,128,96) 10%,rgb(128,131,108) 10% 20%,rgb(142,140,132) 20% 30%,rgb(160,155,154) 30% 40%,rgb(172,167,165) 40% 50%,rgb(173,168,164) 50% 60%,rgb(160,157,152) 60% 70%,rgb(139,137,132) 70% 80%,rgb(120,119,111) 80% 90%,rgb(110,113,93) 90% 100%),linear-gradient(90deg,rgb(100,112,73) 10%,rgb(104,113,83) 10% 20%,rgb(117,121,105) 20% 30%,rgb(134,133,125) 30% 40%,rgb(148,145,137) 40% 50%,rgb(151,148,138) 50% 60%,rgb(139,138,128) 60% 70%,rgb(118,118,108) 70% 80%,rgb(94,99,85) 80% 90%,rgb(80,90,63) 90% 100%),linear-gradient(90deg,rgb(93,102,40) 10%,rgb(94,101,50) 10% 20%,rgb(100,104,71) 20% 30%,rgb(114,112,91) 30% 40%,rgb(128,122,104) 40% 50%,rgb(132,125,106) 50% 60%,rgb(123,116,96) 60% 70%,rgb(103,100,77) 70% 80%,rgb(82,83,53) 80% 90%,rgb(70,75,27) 90% 100%),linear-gradient(90deg,rgb(108,106,15) 10%,rgb(107,104,33) 10% 20%,rgb(108,103,58) 20% 30%,rgb(116,106,77) 30% 40%,rgb(127,112,87) 40% 50%,rgb(131,114,87) 50% 60%,rgb(124,108,76) 60% 70%,rgb(108,95,59) 70% 80%,rgb(95,85,40) 80% 90%,rgb(90,82,25) 90% 100%)',
        'background-position':
          '0 0,0 11%,0 22%,0 33%,0 44%,0 56%,0 67%,0 78%,0 89%,0 100%',
        transform: 'scale(2.5)',
        'background-repeat': 'no-repeat',
        'background-size': '100% 10%',
        filter: 'blur(24px)',
      });
    });
  });

  describe ('getRoundedPercentageOf',  function (hooks) {
    it('returns the right percentages', function () {
      expect(getRoundedPercentageOf(10, 100)).to.equal('10%');
      expect(getRoundedPercentageOf(25, 200)).to.equal('13%');
    });
  });
});
