/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { setupTest, describe, it } from 'dummy/tests/helpers';

describe('Unit | Transform | optional icon image', function (hooks) {
  setupTest(hooks);

  it('exists', function () {
    let transform = this.owner.lookup('transform:optional-icon-image');
    expect(transform).to.be.ok;
  });

  it('deserialize a null to a optional-icon-image object', function () {
    let transform = this.owner.lookup('transform:optional-icon-image');
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({
      useParent: false,
      value: '',
    });
  });
});
