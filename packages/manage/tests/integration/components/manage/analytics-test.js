/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import { expect } from 'chai';
import { describe, it } from 'dummy/tests/helpers';
import { setupRenderingTest } from 'dummy/tests/helpers';
import { render, fillIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/analytics', function (hooks) {
  setupRenderingTest(hooks);

  describe('in view mode', function (hooks) {
    it('renders the current value', async function () {
      this.set('value', {
        matomoUrl: 'https://analytics.giscollective.com',
        matomoSiteId: '1',
      });

      await render(hbs`<Manage::Analytics @title="title" @value={{this.value}}/>`);

      expect(this.element.querySelector('.matomo-url').textContent.trim()).to.equal(
        'https://analytics.giscollective.com',
      );

      expect(this.element.querySelector('.side-id').textContent.trim()).to.equal('1');
    });

    it('renders a not set message when the value is not set', async function () {
      this.set('value', {});

      await render(hbs`<Manage::Analytics @title="title" @value={{this.value}}/>`);

      expect(this.element.querySelector('.matomo-url').textContent.trim()).to.equal('not set');

      expect(this.element.querySelector('.side-id').textContent.trim()).to.equal('not set');
    });
  });

  describe('in edit mode', function (hooks) {
    it('renders the current value', async function () {
      this.set('value', {
        matomoUrl: 'https://analytics.giscollective.com',
        matomoSiteId: '1',
      });

      await render(hbs`<Manage::Analytics @title="title" @value={{this.value}} @editablePanel="title"/>`);

      expect(this.element.querySelector('.input-matomo-url').value).to.equal('https://analytics.giscollective.com');

      expect(this.element.querySelector('.input-side-id').value).to.equal('1');
    });

    it('can change the values', async function () {
      this.set('value', {
        matomoUrl: 'https://analytics.giscollective.com',
        matomoSiteId: '1',
      });

      let url;
      let id;
      this.set('save', (a, b) => {
        url = a;
        id = b;
      });

      await render(
        hbs`<Manage::Analytics @title="title" @value={{this.value}} @onSave={{this.save}} @editablePanel="title"/>`,
      );

      await fillIn('.input-matomo-url', 'http://other.example.com');
      await fillIn('.input-side-id', '2');

      await click('.btn-submit');

      expect(url).to.equal('http://other.example.com');
      expect(id).to.equal('2');
    });

    it('can reset the values', async function () {
      this.set('value', {
        matomoUrl: 'https://analytics.giscollective.com',
        matomoSiteId: '1',
      });

      let called;
      this.set('cancel', () => {
        called = true;
      });

      await render(
        hbs`<Manage::Analytics @title="title" @value={{this.value}} @onCancel={{this.cancel}} @editablePanel="title"/>`,
      );

      await fillIn('.input-matomo-url', 'http://other.example.com');
      await fillIn('.input-side-id', '2');

      await click('.btn-cancel');

      expect(called).to.equal(true);

      expect(this.element.querySelector('.input-matomo-url').value).to.equal('https://analytics.giscollective.com');

      expect(this.element.querySelector('.input-side-id').value).to.equal('1');
    });
  });
});
