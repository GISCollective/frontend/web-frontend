/**
  Copyright: © 2015-2024 GISCollective
  License: Subject to the terms of the AFFERO GENERAL PUBLIC LICENSE, as written in the included COPYING file.
*/
import Controller from '@ember/controller';
import { htmlSafe } from '@ember/template';
import { service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class CampaignsIndexController extends Controller {
  @service intl;
  @service fastboot;
  @tracked s = '';

  queryParams = ['s'];

  get formPageCol() {
    return {
      modelKey: 'campaign',
      data: {
        submit: {
          classes: ["btn-primary", "btn-lg"]
        }
      },
    };
  }

  get contentBlocks() {
    const blocks = this.model.campaign.contentBlocks.blocks.filter((a) => a.type != 'header' && a.data?.level != 1);

    return {
      blocks,
    };
  }

  get title() {
    const value = this.model.campaign.contentBlocks.blocks.find((a) => a.type == 'header' && a.data?.level == 1)?.data?.text ?? '---';

    return htmlSafe(value);
  }

  get coverStyle() {
    const picture = this.model.cover?.picture;

    if (!picture) {
      return '';
    }

    return htmlSafe(`background-image: url('${picture}/md')`);
  }

  get breadcrumbs() {
    const result = [
      {
        route: 'campaigns',
        text: this.intl.t('campaigns'),
        capitalize: true,
      },
    ];

    result.push({
      text: this.model.campaign.name,
    });

    return result;
  }

  @action
  changeState(newState) {
    this.preserveScrollPosition = true;
    this.s = newState;
  }

  @action
  publish() {
    this.model.campaign.visibility.isPublic = true;
    return this.model.campaign.save();
  }

  @action
  unpublish() {
    this.model.campaign.visibility.isPublic = false;
    return this.model.campaign.save();
  }
}
